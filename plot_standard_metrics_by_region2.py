"""
2020/05/19
ywang254@utk.edu

Plot the performances of Mean, Median, best method of DOLCE, and best method 
of EM-LSM by
 - MODIS land cover type
 - continent
using the validation-period ISMN observations.
"""
from utils_management.constants import year_longest, depth, depth_cm, \
    lu_names, continent_legend
import utils_management as mg
from misc.ismn_utils import get_weighted_monthly_data
from misc.analyze_utils import calc_stats
import os
import pandas as pd
import matplotlib.pyplot as plt
import itertools as it
import numpy as np
import matplotlib as mpl
from matplotlib.cm import get_cmap


# Aggregation method to produce the grid-scale ISMN observations.
iam = 'lu_weighted'

# Subset of observations to compare on. Different periods produce similar
# metrics ("plot_standard_metrics_all.py") and have similar spatial
# availability ("ismn_summary.py").
opt = 'val'


mpl.rcParams['font.size'] = 8

stats = ['RMSE', 'Corr']
units = ['(m$^3$/m$^3$)', '(-)']

cmap = get_cmap('jet')
clist = [cmap((i+0.7)/7) for i in range(7)]
mlist = ['x', 'o', 's', 's', 's', 's', 's']
prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all']
prod_name = ['Mean ORS', 'OLC ORS', 'EC ORS', 'EC CMIP5', 'EC CMIP6',
             'EC CMIP5+6', 'EC ALL']

mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 6

for clas in ['continent', 'land_use_modis']:
    fig, axes = plt.subplots(nrows = 4, ncols = 2, sharex = True,
                             sharey = False, figsize = (6.5, 6.5))
    fig.subplots_adjust(hspace = 0., wspace = 0.15)

    for s_ind, d_ind in it.product(range(len(stats)), range(len(depth))):
        stat = stats[s_ind]
        d = depth[d_ind]
        dcm = depth_cm[np.where([x == d for x in depth])[0][0]]

        ax = axes[d_ind, s_ind]

        metrics_by_region = pd.read_csv(os.path.join(mg.path_out(), 
                                                     'standard_metrics',
                                                     'by_' + clas + '_' +\
                                                     d + '_1950-2016_' + \
                                                     iam + '_' + \
                                                     opt + '.csv'),
                                        index_col = 0, header = [0,1])
        metrics_by_region.sort_index(inplace=True)

        if clas == 'land_use_modis':
            # Remove water bodies & Permanent wetlands
            if d_ind != 0:
                metrics_by_region.drop([0], axis = 0, inplace = True)
            else:
                metrics_by_region.drop([0, 11], axis = 0, inplace = True)

        if (clas == 'continent') and (999 in metrics_by_region.index):
            metrics_by_region = metrics_by_region.drop(999, axis = 0)

        b = [None] * len(prod_list)
        for pind, prod in enumerate(prod_list):
            position = [(pind+0.5)/len(prod_list) + rr \
                        for rr in range(metrics_by_region.shape[0])]
            y = metrics_by_region.loc[:, (prod_name[pind].replace(' ','_'),
                                          stat)].values
            btmp = ax.bar(position, y, color = clist[pind],
                          width = 0.9/(2+len(prod_list)))
            b[pind] = btmp[0]

        if d_ind == 0:
            for x_ind, x in enumerate(metrics_by_region.index):
                h1 = ax.hlines(metrics_by_region.loc[x, 
                                                     ('ESA-CCI', stat)],
                               x_ind, x_ind + 1,
                               lw = 1.5, ls = '-', color = '#252525')
                h2 = ax.hlines(metrics_by_region.loc[x, 
                                                     ('GLEAM', stat)],
                               x_ind, x_ind + 1,
                               lw = 1.5, ls = '--', color = '#252525')
                h3 = ax.hlines(metrics_by_region.loc[x, ('ERA-Land', stat)],
                               x_ind, x_ind + 1,
                               lw = 1.5, ls = ':', color = '#252525')
        if d_ind == 1:
            for x_ind, x in enumerate(metrics_by_region.index):
                h3 = ax.hlines(metrics_by_region.loc[x, ('ERA-Land', stat)],
                               x_ind, x_ind + 1,
                               lw = 1.5, ls = ':', color = '#252525')

        if d_ind == 0:
            ax.set_title(stat + units[s_ind])

            xticks = np.arange(0., metrics_by_region.shape[0]+.5)
            xminorticks = np.arange(0.5, metrics_by_region.shape[0]+.5)
            if clas == 'land_use_modis':
                xticklabels = []
                for x in metrics_by_region.index:
                    tmp = lu_names[x].split(' ')
                    tmp = tmp[0] + '\n' + ' '.join(tmp[1:])
                    xticklabels.append(tmp)
            else:
                xticklabels = [continent_legend[int(x)].replace(' ','\n') \
                               for x in metrics_by_region.index]
            xlim = [-1/len(metrics_by_region.columns.levels[0]),
                    metrics_by_region.shape[0]]

        ax.set_xticks(xticks)
        ax.tick_params('x', length = 0, which = 'minor')
        ax.set_xticklabels([])
        ax.set_xticks(xminorticks, minor = True)
        ax.set_xlim(xlim)
        if clas == 'land_use_modis':
            ax.set_xticklabels(xticklabels, rotation = 90, minor = True)
            if (d_ind == (len(depth)-1)) & (stat == 'RMSE'):
                ax.legend([h1, h2, h3] + b,
                          ['ESA-CCI', 'GLEAMv3.3a', 'ERA-Land'] + prod_name,
                          ncol = 5, loc = [0, -1.])
            if stat == 'RMSE':
                ax.set_ylabel(dcm)
                ax.set_ylim([0., 0.26])
                ax.set_yticks(np.arange(0.05, 0.251, 0.05))
            else:
                ax.set_ylim([-0.9, 0.76])
                ax.set_yticks(np.arange(-.6, 0.75, 0.3))
        else:
            ax.set_xticklabels(xticklabels, rotation = 0, minor = True)
            if (d_ind == (len(depth)-1)) & (stat == 'RMSE'):
                ax.legend([h1, h2, h3] + b,
                          ['ESA-CCI', 'GLEAMv3.3a', 'ERA-Land'] + prod_name,
                          ncol = 5, loc = [0, -0.5])
            if (stat == 'RMSE'):
                ax.set_ylabel(dcm)
                ax.set_ylim([0., 0.22])
                ax.set_yticks(np.arange(0.05, 0.23, 0.05))
            else:
                ax.set_ylim([-0.25, 1.])
                ax.set_yticks(np.arange(-0.2, .81, 0.2))

    fig.savefig(os.path.join(mg.path_out(), 'standard_metrics', 
                             'by_' + clas + '_' + \
                             iam + '_' + opt + '.png'), 
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
                                                     
