"""
2019/07/24

ywang254@utk.edu

Draw the global maps of the emergent constraint slopes, when based on the range
 of land surface models, and the significance of the slopes.
"""
import numpy as np
import xarray as xr
import utils_management as mg
from utils_management.constants import depth, year_longest, year_shorter, \
    year_shorter2, year_shortest, depth_cm, target_lat, target_lon
import sys
import os
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from misc.plot_utils import plot_sm_map, plot_map_w_stipple
import itertools as it
from misc.plot_utils import cmap_gen
import multiprocessing as mp


cmap = cmap_gen('autumn', 'winter_r')
met_obs = 'CRU_v4.03'
em_method = ['month_anomaly_1grid', 'month_1grid']


for model_set in ['lsm', 'cmip5', 'cmip6']:
    if model_set == 'lsm':
        year_list = [year_longest, year_shorter, year_shorter2, year_shortest]
    else:
        year_list = [year_longest]

    for year, i, me in it.product(year_list, range(4), em_method):
        d = depth[i]
        dcm = depth_cm[i]

        year_str = str(year[0]) + '-' + str(year[-1])
        year_ind = list(np.round(np.linspace(0, 1., 10) * \
                                 (len(year)-1)).astype(int))

        # Make a plot for each individual month. The data contains
        # significant correlation coefficients.
        for reg, rname in zip([1,2], ['precipitation', 'temperature']):
            fig, axes = plt.subplots(nrows = 4, ncols = 3, 
                                     figsize = (25,15), 
                                     subplot_kw = {'projection': \
                                                   ccrs.PlateCarree()})
            for ax_ind, ax in enumerate(axes.flat):
                if reg == 1:
                    level = np.linspace(-0.4, 0.4, 11)
                else:
                    level = np.linspace(-0.1, 0.1, 11)
                    
                file_name = os.path.join(mg.path_out(), 
                                         'em_' + model_set + '_corr',
                                         met_obs + '_' + me + '_' + dcm + \
                                         '_' + year_str, 'corr_' + \
                                         str(ax_ind) + '.nc')
                data = xr.open_dataset(file_name)
                corr = data.corr.values[reg, :, :].copy()
                data.close()

                ax.coastlines()
                ax.set_extent([-180, 180, -60, 90])
                h = ax.contourf(data.lon.values, data.lat.values, 
                                corr, levels = level, cmap = cmap)
                plt.colorbar(h, ax = ax)
                ax.set_title('Month = ' + str(ax_ind), y = 1.08)
            fig.savefig(os.path.join(mg.path_out(), 
                                     'plot_em_significance', 
                                     model_set, met_obs + '_' + me + \
                                     '_' + dcm + '_' + year_str,
                                     'corr_' + rname + '.png'),
                        dpi=600., bbox_inches = 'tight')
            plt.close(fig)
