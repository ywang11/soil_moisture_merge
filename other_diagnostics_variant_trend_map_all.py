"""
2019/10/02

ywang254@utk.edu

The trend in soil moisture, 1950-2016. No reference to ERA-Interim/GLEAM
"""
import os
import itertools as it
import pandas as pd
import numpy as np
import xarray as xr
import utils_management as mg
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth, depth_cm
from misc.analyze_utils import calc_seasonal_trend
import time
import multiprocessing as mp


data_list = [x+'_'+y for x in ['mean', 'dolce', 'em'] \
             for y in ['lsm', 'cmip5', 'cmip6', '2cmip', 'all']]
year = year_longest
year_str = str(year[0]) + '-' + str(year[-1])


def calc(option):
    i, l = option

    d = depth[i]
    dcm = depth_cm[i]

    if 'mean' in l:
        fname = os.path.join(mg.path_out(), 'meanmedian_' + l.split('_')[1],
                             'mean_' + d + '_' + year_str + '.nc')
        data = xr.open_dataset(fname, decode_times = False)
        sm = data.sm
    elif (l == 'dolce_lsm') | (l == 'dolce_all'):
        fname = os.path.join(mg.path_out(), 'concat_' + l,
                             'concat_average_' + d + \
                             '_lu_weighted_ShrunkCovariance.nc')
        data = xr.open_dataset(fname, decode_times = False)
        sm = data.sm
    elif (l == 'dolce_cmip5') | (l == 'dolce_cmip6') | (l == 'dolce_2cmip'):
        fname = os.path.join(mg.path_out(), l + '_product',
                             'weighted_average_' + year_str + '_' + d + \
                             '_lu_weighted_ShrunkCovariance.nc')
        data = xr.open_dataset(fname, decode_times = False)
        sm = data.sm
    elif (l == 'em_lsm') | (l == 'em_all'):
        fname = os.path.join(mg.path_out(), 'concat_' + l,
                             'concat_CRU_v4.03_year_month_anomaly_9grid_' + \
                             dcm + '_predicted_' + year_str + '.nc')
        data = xr.open_dataset(fname, decode_times = False)
        sm = data.predicted
    elif (l == 'em_cmip5') | (l == 'em_cmip6') | (l == 'em_2cmip'):
        fname = os.path.join(mg.path_out(), l + '_corr',
                             'CRU_v4.03_year_month_positive_9grid_' + \
                             dcm + '_' + year_str, 'predicted.nc')
        data = xr.open_mfdataset(fname, decode_times = False,
                                 concat_dim = 'time')
        sm = data.predicted

    sm.load()
    sm['time'] = pd.date_range(str(year[0])+'-01-01',
                               str(year[-1])+'-12-31', freq = 'MS')

    #
    for s in ['All', 'Annual', 'DJF', 'MAM', 'JJA', 'SON']:
        g_map_trend, g_p_values, g_intercepts = calc_seasonal_trend(sm, s)

        xr.Dataset({'g_map_trend': (['lat','lon'], g_map_trend),
                    'g_p_values': (['lat','lon'], g_p_values),
                    'g_intercepts': (['lat','lon'], g_intercepts)},
                   coords = {'lon':data.lon, 'lat':data.lat} \
               ).to_netcdf(os.path.join(mg.path_out(),
                                        'other_diagnostics', l, 
                                        d + '_g_map_trend_' + s + '.nc'))
    data.close()


pool = mp.Pool(4)
pool.map_async(calc, list(it.product(range(4), data_list)))
pool.close()
pool.join()

#for i, d in it.product(range(4), data_list):
#    calc([i,d])
