"""
2019/08/05
ywang254@utk.edu

Plot the time series of global mean annual mean soil moisture of the 
 mean and median results. Show the GLEAMv3.3 and ERA-Land time series of 
 global mean annual mean soil moisture for comparison.

Note: ESA-CCI is not included in the global average envelope. 
      Trend calculation skips the 1st year for consistency with DJF.
"""
import matplotlib.pyplot as plt
import utils_management as mg
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth, depth_cm, lsm_list
from misc.plot_utils import plot_ts_shade, plot_ts_trend
from misc.standard_diagnostics import seasonal_avg
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability
import pandas as pd
import os
import numpy as np
import itertools as it


# MODIFY
model_set = '2cmip' # 'lsm', 'cmip5', 'cmip6', '2cmip', 'all'

if model_set == 'lsm': 
    title = 'ORS'
elif model_set == 'cmip5':
    title = 'CMIP5'
elif model_set == 'cmip6':
    title = 'CMIP6'
elif model_set == '2cmip':
    title = 'CMIP5 + CMIP6'
else:
    title = 'ORS + CMIP5 + CMIP6'


if (model_set == 'lsm') | (model_set == 'all'):
    year_list = [year_longest, year_shorter, year_shorter2, year_shortest]
else:
    year_list = [year_longest]


###############################################################################
# Global mean/median soil moisture.
###############################################################################
global_ts_all = {'mean': {}, 'median': {}}
for i, year in it.product(range(len(depth)), year_list):
    d = depth[i]
    dcm = depth_cm[i]
    year_str = str(year[0]) + '-' + str(year[-1])

    #
    mean = pd.read_csv(os.path.join(mg.path_out(), 
                                    'standard_diagnostics_meanmedian_' + \
                                    model_set, 
                                    'mean_' + year_str + '_' + d + \
                                    '_g_ts.csv'), index_col = 0, 
                       parse_dates = True).iloc[:, 0]
    median = pd.read_csv(os.path.join(mg.path_out(), 
                                      'standard_diagnostics_meanmedian_' + \
                                      model_set, 
                                      'median_' + year_str + '_' + d + \
                                      '_g_ts.csv'), index_col = 0,
                         parse_dates = True).iloc[:, 0]

    global_ts_all['mean'][(d, year_str)] = seasonal_avg(mean)
    global_ts_all['median'][(d, year_str)] = seasonal_avg(median)


###############################################################################
# Reference data.
###############################################################################
# GLEAM v3.3a is only available for 0-10cm.
gleam = pd.read_csv(os.path.join(mg.path_out(), 'standard_diagnostics_lsm', 
                                 'GLEAMv3.3a_0.00-0.10_g_ts.csv'),
                    index_col = 0, parse_dates = True).iloc[:, 0]
gleam_ts_all = seasonal_avg(gleam)

# ERA-Land is only available for the two shallow depths.
eraland_ts_all = {}
for i, d in enumerate(['0.00-0.10', '0.10-0.30']):
    temp = pd.read_csv(os.path.join(mg.path_out(),
                                    'standard_diagnostics_lsm',
                                    'ERA-Land_' + d + '_g_ts.csv'),
                       index_col = 0, parse_dates = True).iloc[:, 0]
    eraland_ts_all[d] = seasonal_avg(temp)


###############################################################################
# Build 5% to 95% intervals of the absolute values and the anomalies of 
# raw data.
###############################################################################
land_mask = 'vanilla'
raw_ts_all = {}
raw_ts_all_anomaly = {}
for i, year in it.product(range(len(depth)), year_list):
    d = depth[i]
    dcm = depth_cm[i]
    year_str = str(year[0]) + '-' + str(year[-1])

    temp_all = {}
    if model_set == 'lsm':
        model_list = lsm_list[(year_str, d)]

        if 'ESA-CCI' in model_list:
            model_list.remove('ESA-CCI')

        for m in model_list:
            temp_all[m] = seasonal_avg(pd.read_csv(os.path.join(mg.path_out(),
                'standard_diagnostics_' + model_set, m + '_' + d + \
                '_g_ts.csv'), index_col = 0, parse_dates = True).iloc[:, 0])
            temp_all[m] = temp_all[m].loc[year, :]
            temp_all[m] = temp_all[m].unstack()
    elif model_set == 'cmip5':
        model_list = cmip5_availability(dcm, land_mask)
        for m in model_list:
            temp_all[m] = seasonal_avg(pd.read_csv(os.path.join(mg.path_out(),
                'standard_diagnostics_' + model_set, m + '_' + dcm + \
                '_g_ts.csv'), index_col = 0, parse_dates = True).iloc[:, 0])
            temp_all[m] = temp_all[m].loc[year, :]
            temp_all[m] = temp_all[m].unstack()
    elif model_set == 'cmip6':
        cmip6_list_1 = mrsol_availability(dcm, land_mask, 'historical')
        cmip6_list_2 = mrsol_availability(dcm, land_mask, 'ssp585')
        cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) )
        model_list = [x for x in cmip6_list if 'r1i1p1f1' in x]
        for m in model_list:
            temp_all[m] = seasonal_avg(pd.read_csv(os.path.join(mg.path_out(),
                'standard_diagnostics_' + model_set, 'mrsol_' + \
                m + '_' + dcm + '_g_ts.csv'),  index_col = 0, 
                parse_dates = True).iloc[:, 0])
            temp_all[m] = temp_all[m].loc[year, :]
            temp_all[m] = temp_all[m].unstack()
    elif model_set == '2cmip':
        #
        model_list = cmip5_availability(dcm, land_mask)
        for m in model_list:
            temp_all[m] = seasonal_avg(pd.read_csv(os.path.join(mg.path_out(),
                'standard_diagnostics_cmip5', m + '_' + dcm + \
                '_g_ts.csv'), index_col = 0, parse_dates = True).iloc[:, 0])
            temp_all[m] = temp_all[m].loc[year, :]
            temp_all[m] = temp_all[m].unstack()
        #
        cmip6_list_1 = mrsol_availability(dcm, land_mask, 'historical')
        cmip6_list_2 = mrsol_availability(dcm, land_mask, 'ssp585')
        cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) )
        model_list = [x for x in cmip6_list if 'r1i1p1f1' in x]
        for m in model_list:
            temp_all[m] = seasonal_avg(pd.read_csv(os.path.join(mg.path_out(),
                'standard_diagnostics_cmip6', 'mrsol_' + \
                m + '_' + dcm + '_g_ts.csv'),  index_col = 0,
                parse_dates = True).iloc[:, 0])
            temp_all[m] = temp_all[m].loc[year, :]
            temp_all[m] = temp_all[m].unstack()
    elif model_set == 'all':
        #
        model_list = lsm_list[(year_str, d)]
        if 'ESA-CCI' in model_list:
            model_list.remove('ESA-CCI')
        for m in model_list:
            temp_all[m] = seasonal_avg(pd.read_csv(os.path.join(mg.path_out(),
                'standard_diagnostics_lsm', m + '_' + d + \
                '_g_ts.csv'), index_col = 0, parse_dates = True).iloc[:, 0])
            temp_all[m] = temp_all[m].loc[year, :]
            temp_all[m] = temp_all[m].unstack()
        #
        model_list = cmip5_availability(dcm, land_mask)
        for m in model_list:
            temp_all[m] = seasonal_avg(pd.read_csv(os.path.join(mg.path_out(),
                'standard_diagnostics_cmip5', m + '_' + dcm + \
                '_g_ts.csv'), index_col = 0, parse_dates = True).iloc[:, 0])
            temp_all[m] = temp_all[m].loc[year, :]
            temp_all[m] = temp_all[m].unstack()
        #
        cmip6_list_1 = mrsol_availability(dcm, land_mask, 'historical')
        cmip6_list_2 = mrsol_availability(dcm, land_mask, 'ssp585')
        cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) )
        model_list = [x for x in cmip6_list if 'r1i1p1f1' in x]
        for m in model_list:
            temp_all[m] = seasonal_avg(pd.read_csv(os.path.join(mg.path_out(),
                'standard_diagnostics_cmip6', 'mrsol_' + \
                m + '_' + dcm + '_g_ts.csv'),  index_col = 0,
                parse_dates = True).iloc[:, 0])
            temp_all[m] = temp_all[m].loc[year, :]
            temp_all[m] = temp_all[m].unstack()



    # absolute values
    temp_all = pd.DataFrame(temp_all)
    raw_ts_all[(d, year_str)] = {}
    raw_ts_all[(d, year_str)]['min'] = \
      pd.Series(np.percentile(temp_all, 2.5, axis = 1), 
                index = temp_all.index).unstack().T
    raw_ts_all[(d, year_str)]['max'] = \
      pd.Series(np.percentile(temp_all, 97.5, axis = 1),
                index = temp_all.index).unstack().T

    # anomalies
    temp_all2 = temp_all.reorder_levels([1, 0]).unstack()
    temp_all2 = temp_all2 - temp_all2.mean(axis = 0)
    temp_all2 = temp_all2.stack(dropna = False).reorder_levels([1,0])

    raw_ts_all_anomaly[(d, year_str)] = {}
    raw_ts_all_anomaly[(d, year_str)]['min'] = \
        pd.Series(np.percentile(temp_all2, 2.5, axis = 1),
                  index = temp_all.index).unstack().T
    raw_ts_all_anomaly[(d, year_str)]['max'] = \
        pd.Series(np.percentile(temp_all2, 97.5, axis = 1),
                  index = temp_all.index).unstack().T


###############################################################################
# Plotting.
###############################################################################
for stat, year, season in it.product(['mean', 'median'], year_list,
                                     ['annual', 'DJF', 'MAM', 'JJA', 'SON']):
    year_str = str(year[0]) + '-' + str(year[-1])

    fig, axes = plt.subplots(nrows = len(depth_cm), ncols = 2, sharex = True, 
                             sharey = False, figsize = (10, 6.5))
    fig.subplots_adjust(hspace = 0., wspace = 0.2)

    # (1) Soil Moisture
    for i,d in enumerate(depth):
        dcm = depth_cm[i]
        ax = axes[i, 0]

        h1, hfill = plot_ts_shade(ax, global_ts_all[stat][(d, year_str)].index,
            {'min': raw_ts_all[(d, year_str)]['min'].loc[:, season],
             'mean': global_ts_all[stat][(d, year_str)].loc[:, season],
             'max': raw_ts_all[(d, year_str)]['max'].loc[:, season]},
                                  ts_col = 'b')

        if (i == 0):
            h2, = ax.plot(gleam_ts_all.index, gleam_ts_all.loc[:, season],
                          '-r', linewidth = 2, zorder = 3)

        if (i == 0) | (i == 1):
            h3, = ax.plot(eraland_ts_all[d].index,
                          eraland_ts_all[d].loc[:, season],
                          '-k', linewidth = 2, zorder = 3)
        if (i == 0):
            ax.set_title('Soil Moisture (m$^3$/m$^3$)')
        ax.set_xlim([year[0], year[-1]])
        ax.set_xlabel('Year')
        ax.set_ylim([0., 0.5])
        ax.set_ylabel(dcm)
        ax.set_yticks(np.linspace(0.1, 0.4, 5))

    # (2) Soil Moisture Anomaly
    for i,d in enumerate(depth):
        dcm = depth_cm[i]
        ax = axes[i, 1]

        h1, hfill = plot_ts_shade(ax, global_ts_all[stat][(d, year_str)].index,
            {'min': raw_ts_all_anomaly[(d, year_str)]['min'].loc[:, season],
             'mean': global_ts_all[stat][(d, year_str)].loc[:, season] - \
                     global_ts_all[stat][(d, year_str)].loc[:, season].mean(),
             'max': raw_ts_all_anomaly[(d, year_str)]['max'].loc[:, season]},
                                  ts_col = 'b')

        # Skip 1st year in estimating trend for consistency with DJF.
        # But anomaly is wrt entire period.
        h1, hline = plot_ts_trend(ax, 
            global_ts_all[stat][(d, year_str)].index[1:],
            (global_ts_all[stat][(d, year_str)].loc[:,season].iloc[1:] - \
             global_ts_all[stat][(d, year_str)].loc[:,season].mean()),
                                  0.02, 0.05, ts_kw = {'color': 'b'})

        if (i == 0):
            h2, = ax.plot(gleam_ts_all.index, 
                          gleam_ts_all.loc[:, season] - \
                          gleam_ts_all.loc[:, season].mean(), '-r',
                          linewidth = 2, zorder = 3)

        if (i == 0) | (i == 1):
            h3, = ax.plot(eraland_ts_all[d].index, 
                          eraland_ts_all[d].loc[:, season] - \
                          eraland_ts_all[d].loc[:, season].mean(), 
                          '-k', linewidth = 2, zorder = 3)

        if (i == 0):
            ax.set_title('Anomaly (m$^3$/m$^3$)')
        ax.set_xlim([year[0], year[-1]])
        ax.set_xlabel('Year')
        ax.set_ylim([-0.007, 0.007])
        ax.set_yticks(np.linspace(-0.006, 0.006, 7))

    ax.legend([(h1, hfill), h2, h3],
              [stat.capitalize() + ' (95% CI)', 'GLEAMv3.3a', 'ERA-Land'],
              ncol = 3, loc = 'lower right', bbox_to_anchor = (0.3, -0.7))

    fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot', 
                             'ts_global', 'meanmedian', stat + '_' + \
                             season + '_' + \
                             model_set + '_' + year_str + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
