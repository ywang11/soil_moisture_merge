# -*- coding: utf-8 -*-
"""
Created on Fri Feb 22 20:42:37 2019

@author: ywang254

Examine the soil moisture data from GLDAS land surface model.
"""
import pandas as pd
import xarray as xr
import utils_management as mg
import os

path_mstmip = os.path.join(mg.path_data(), 'TRENDY')

flist = []


# The soil reference depth 
ds = xr.open_dataset(os.path.join(path_mstmip,
                                  'mstmip_driver_global_hd_soil_v1.nc4'))
ds.close()