"""
2019/07/05

ywang254@utk.edu

Plot the standard deviation of soil moisture at each grid.
"""
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth
import utils_management as mg
import numpy as np
import os
import itertools as it
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import cartopy.crs as ccrs
from cartopy.util import add_cyclic_point
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import xarray as xr
import multiprocessing as mp


year = year_longest
year_str = str(year[0]) + '-' + str(year[-1])


levels = np.linspace(0., 0.021, 21)
cmap = 'Spectral'
map_extent = [-180, 180, -60, 90]


for month in range(1,13):

    fig, axes = plt.subplots(nrows = 2, ncols = 2, figsize = (12, 12),
                             subplot_kw = {'projection': ccrs.PlateCarree()})
    fig.subplots_adjust(wspace = 0., hspace = 0.)

    for mind, dind in it.product(range(2), range(2)):
        if mind == 0:
            model = 'ERA-Land'
        else:
            model = 'GLEAMv3.3a'
        d = depth[dind]

        if (dind == 1) & (model == 'GLEAMv3.3a'):
            continue

        data = xr.open_dataset(os.path.join(mg.path_out(), 'other_diagnostics',
                                            'lsm', model + '_sd_' + d + '.nc'))
        ax = axes[mind, dind]
        ax.coastlines()
        ax.set_extent(map_extent)
        sd_cyc, lon_cyc = add_cyclic_point(data['sd'].values[month - 1,:,:], 
                                           coord=data.lon)
        h = ax.contourf(lon_cyc, data.lat, sd_cyc, cmap = cmap, 
                        levels = levels, extend = 'both')
        ax.set_title(d)
        ax.set_ylabel(model)
    cax = fig.add_axes([0.93, 0.1, 0.01, 0.8])
    fig.colorbar(h, cax = cax, boundaries = levels,
                 ticks = 0.5 * (levels[1:] + levels[:-1]))
    fig.savefig(os.path.join(mg.path_out(), 'other_diagnostics_plot', 'sd',
                             'ref_' + str(month) + '.png'), dpi = 600.)
    plt.close(fig)
