"""
2019/07/05

ywang254@utk.edu

Map the global mean and seasonal mean soil moisture for the mean & median of
  the land surface models/CMIP5/CMIP6.
"""
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth
import utils_management as mg
from misc.plot_standard_diagnostics_utils import plot_map
import numpy as np
import os
import multiprocessing as mp
import itertools as it


levels = np.linspace(0., 0.6, 15)
cmap = 'Spectral'
year = year_longest

data_list = ['lsm', 'cmip5', 'cmip6', '2cmip', 'all']


def plotter(option):
    data, stat = option
##for data, stat in it.product(data_list, ['mean', 'median']):
    prefix_in = os.path.join(mg.path_out(),
                             'standard_diagnostics_meanmedian_' + data,
                             'mean_' + str(year[0]) + '-' + str(year[-1]) + \
                             '_')
    path_out = os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                            'clim_meanmedian', data + '_mean_' + \
                            str(year[0]) + '-' + str(year[-1]) + '.png')
    plot_map(depth, prefix_in, path_out, levels, cmap)


pool = mp.Pool(3)
pool.map_async(plotter, list(it.product(data_list, ['mean', 'median'])))
pool.close()
pool.join()
