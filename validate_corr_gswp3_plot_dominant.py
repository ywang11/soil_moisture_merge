"""
2021/06/04
Plot the dominant factor in the GSWP3 regressions.
"""
import xarray as xr
import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import itertools as it
import utils_management as mg
from utils_management.constants import depth, depth_cm
from misc.plot_utils import stipple, cmap_div


mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 6
mpl.rcParams['hatch.linewidth'] = 0.5
cmap = 'jet'
lab = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
       'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
       'y', 'z', 'aa', 'bb', 'cc', 'dd', 'ee', 'ff', 'gg', 'hh',
       'ii', 'jj', 'kk', 'll', 'mm', 'nn', 'oo', 'pp', 'qq', 'rr',
       'ss', 'tt', 'uu', 'vv', 'ww', 'xx']


prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all']
prod_name_list = ['Mean ORS', 'OLC ORS', 'EC ORS', 'EC CMIP5', 'EC CMIP6',
                  'EC CMIP5+6', 'EC ALL']
season_list = ['Annual', 'DJF', 'MAM', 'JJA', 'SON']
var_list = ['pr', 'tas', 'rsds']
for season in season_list:
    fig, axes = plt.subplots(len(prod_list), 4, figsize = (6.5, 6.5),
                             subplot_kw = {'projection': ccrs.Miller()})
    fig.subplots_adjust(wspace = 0.01, hspace = 0.01)
    count = 0
    for pind, dind in it.product(range(len(prod_list)), range(len(depth))):
        prod = prod_list[pind]

        d = depth[dind]
        dcm = depth_cm[dind]

        collect = np.full([3, 360, 720], np.nan)
        collect_p = np.full([3, 360, 720], np.nan)
        for vind, var in enumerate(['pr', 'tas', 'rsds']):
            hr = xr.open_dataset(os.path.join(mg.path_out(), 'validate',
                                              'corr_gswp3_' + prod + '_' + \
                                              d + '_' + season + '.nc'))
            collect[vind, :, :] = hr[var].values
            collect_p[vind, :, :] = hr[var + '_p'].values
            hr.close()
        collect = np.where(~np.isnan(collect[0,:,:]),
                           np.argmax(np.abs(collect), axis = 0), np.nan)
        collect_p = np.where(collect == 0, collect_p[0,:,:],
                             np.where(collect == 1, collect_p[1,:,:], collect_p[2,:,:]))

        ax = axes.flat[count]
        ax.coastlines(lw = 0.5, color = 'grey')
        ax.set_extent([-180, 180, -60, 90], crs=ccrs.PlateCarree())

        cf = ax.contourf(hr['lon'], hr['lat'], collect, 
                         cmap = cmap, alpha = 0.5,
                         levels = np.arange(-0.5, 2.6),
                         transform = ccrs.PlateCarree(),
                         extend = 'neither')
        stipple(ax, hr['lat'].values, hr['lon'].values, 
                collect_p, transform = ccrs.PlateCarree())

        if pind == 0:
            ax.set_title(dcm.replace('cm', ' cm').replace('-', u'\u2212'))
        if dind == 0:
            ax.text(-0.1, 0.5, prod_name_list[pind], rotation = 90., 
                    verticalalignment = 'center',
                    transform = ax.transAxes)
        ax.text(0.05, 0.88, '(' + lab[count] + ')', transform = ax.transAxes)

        count += 1

    cax = fig.add_axes([0.1, 0.07, 0.8, 0.01])
    cb = plt.colorbar(cf, cax = cax, orientation = 'horizontal', ticks = [0, 1, 2])
    cb.ax.xaxis.set_ticklabels(['Precipitation', 'Temperature', 'Radiation'])

    fig.savefig(os.path.join(mg.path_out(), 'validate', 'corr_gswp3_df_' + season + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
