# -*- coding: utf-8 -*-
"""
Created on Tue Apr 23 10:27:53 2019

@author: ywang254

Plot the global maps of two normal and three problematic models.
"""
import xarray as xr
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
import utils_management as mg

# Which year to draw the models for
year = '1950'

# Range of the colorbars
vmin = 0.
vmax = 1.


#The normal model with highest global mean soil moisture
fig,ax = plt.subplots(nrows=2, ncols=2, figsize=(12, 12))
for d, depth in enumerate(['0-10cm', '10-30cm', '30-50cm', '50-100cm']):
    f2 = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'Interp_Merge', 
                                      'CLM5.0', 'CLM5.0_' + year + '_' + \
                                      depth + '.nc'), 
                         decode_times = False)
    np.mean(f2.sm[:12,:,:], axis=0).plot( \
            ax = ax.flat[d], vmin=vmin, vmax=vmax, cmap='RdBu')
    ax.flat[d].set_title(depth)
    f2.close()
fig.savefig('sm_CLM5.0_' + year + '_interp_annual_average.png')
plt.close(fig)


# The normal model with lowest global mean soil moisture
fig,ax = plt.subplots(nrows=1, ncols=2, figsize=(12,6))
for d, depth in enumerate(['0-10cm', '30-50cm']):
    f2 = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'Interp_Merge', 
                                      'JSBACH', 'JSBACH_' + year + '_' + \
                                      depth + '.nc'),
                         decode_times = False)
    np.mean(f2.sm[:12,:,:], axis=0).plot( \
            ax = ax.flat[d], vmin=vmin, vmax=vmax, cmap='RdBu')
    ax.flat[d].set_title(depth)
    f2.close()
fig.savefig('sm_JSBACH_' + year + '_interp_annual_average.png')
plt.close(fig)


# Abnormal models
mlist = ['CLASS-CTEM-N_BG1', 'CLM4_BG1', 'GTEC_SG3']

for model in mlist:
    if (model == 'CLM4_BG1') | (model == 'CLM4VIC_BG1'):
        # Soil layer thicknesses
        f = xr.open_dataset(os.path.join(mg.path_data(), 'MsTMIP', model + \
                            '_Monthly_z_bottom.nc4'), decode_times = False)
        z_bottom = f.z_bottom.values
        f.close()

        f = xr.open_dataset(os.path.join(mg.path_data(), 'MsTMIP', model + \
                            '_Monthly_z_top.nc4'), decode_times = False)
        z_top = f.z_top.values
        f.close()

        z_thickness = z_bottom - z_top

        if model=='CLM4_BG1':
            z_top = z_top * 0.01
            z_bottom = z_bottom * 0.01
            z_thickness = z_thickness * 0.01
            z_top = z_top[:12]
            z_bottom = z_bottom[:12]
            z_thickness = z_thickness[:12]
    elif (model == 'GTEC_SG3'):
        z_top = np.array([0, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.75, 1, 1.25, 1.5, 
                          2, 3, 4])
        z_bottom = np.array([0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.75, 1, 1.25, 1.5, 
                             2, 3, 4, 5])
        z_thickness = np.array([0.05, 0.05, 0.1, 0.1, 0.1, 0.1, 0.25, 0.25, 
                                0.25, 0.25, 0.5, 1, 1, 1])
    else:
        z_top = np.array([0., 0.1, 0.35])
        z_bottom = np.array([0.1, 0.35, 4.])
        z_thickness = np.array([0.1, 0.25, 3.65])

    # 
    f = xr.open_dataset(os.path.join(mg.path_data(), 'MsTMIP', model + \
                        '_Monthly_SoilMoist.nc4'))

    ##f.SoilMoist.mean(axis=0).plot(cmap='Blues')

    sm = f.SoilMoist

    # fig 1: kg/m2; fig 2: converted to m3/m3
    fig,ax = plt.subplots(nrows=3, ncols=4, figsize=(24,16))
    fig2,ax2 = plt.subplots(nrows=3, ncols=4, figsize=(24,16))
    for i in range(min(12, len(z_thickness))):
        # (limit to selected year)
        ind = np.where( (pd.to_datetime(f.time.values) > \
                         pd.to_datetime(year+'-01-01', format='%Y-%m-%d')) & \
                        (pd.to_datetime(f.time.values) < \
                         pd.to_datetime(year+'-12-31', format='%Y-%m-%d')) )[0]

        if model=='CLM4_BG1':
            (np.mean(sm[i,ind,:,:], axis=0)).plot( \
             ax = ax.flat[i], vmin=vmin, vmax=vmax, cmap='RdBu')
            (np.mean(sm[i,ind,:,:], axis=0) / z_thickness[i] * 0.001 \
             ).plot(ax = ax2.flat[i], vmin=vmin, vmax=vmax, cmap='RdBu')
        else:
            (np.mean(sm[ind,i,:,:], axis=0)).plot( \
             ax = ax.flat[i], vmin=vmin, vmax=vmax, cmap='RdBu')
            (np.mean(sm[ind,i,:,:], axis=0) / z_thickness[i] * 0.001 \
             ).plot(ax = ax2.flat[i], vmin=vmin, vmax=vmax, cmap='RdBu')
        ax.flat[i].set_title(('%.3f' % z_top[i]) + '-' + \
                              ('%.3f' % z_bottom[i]))
        ax2.flat[i].set_title(('%.3f' % z_top[i]) + '-' + \
                              ('%.3f' % z_bottom[i]))
    fig.savefig('sm_' + model + '_' + year + '_kgm-2_annual_average.png')
    fig2.savefig('sm_' + model + '_' + year + '_m3m-3_annual_average.png')

    f.close()

    #
    if model != 'CLASS-CTEM-N_BG1':
        fig,ax = plt.subplots(nrows=2, ncols=2, figsize=(12, 10))
        dlist = ['0-10cm', '10-30cm', '30-50cm', '50-100cm']
    else:
        fig,ax = plt.subplots(nrows=1, ncols=2, figsize=(12, 10))
        dlist = ['0-10cm', '30-50cm']
    for d, depth in enumerate(dlist):
        f2 = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                          model.split('_')[0], 
                                          model.split('_')[0] + '_' + year + \
                                          '_' + depth + '.nc'), 
                             decode_times = False)
        np.mean(f2.sm[:12,:,:], axis=0).plot( \
                ax = ax.flat[d], vmin=0., vmax=0.3, cmap='RdBu')
        ax.flat[d].set_title(depth)
        f2.close()
    fig.savefig('sm_' + model + '_' + year + '_interp_annual_average.png')

    plt.close('all')