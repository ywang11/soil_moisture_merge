"""
2019/07/06

ywang254@utk.edu

For each depth, annual/season, anomaly/absolute values, plot in 9 panels:
  - mean_lsm, with original models' envelop
  - mean_cmip5, with original models' envelop
  - mean_cmip6, with original models' envelop
  - concat_dolce_lsm with uncertainty (lu_weighted, ShrunkCovariance)
  - dolce_cmip5 with uncertainty (lu_weighted, ShrunkCovariance)
  - dolce_cmip6 with uncertainty (lu_weighted, ShrunkCovariance)
  - dolce_2cmip with uncertainty (lu_weighted, ShrunkCovariance)
  - dolce_all with uncertainty (lu_weighted, ShrunkCovariance)
  - em_lsm with uncertainty (CRU_v4.03, year_month_anomaly_9grid)
  - em_cmip5 with uncertainty (CRU_v4.03, year_month_anomaly_9grid)
  - em_cmip6 with uncertainty (CRU_v4.03, year_month_anomaly_9grid)
  - em_2cmip with uncertainty (CRU_v4.03, year_month_anomaly_9grid)
  - em_all with uncertainty (CRU_v4.03, year_month_anomaly_9grid)
"""
from utils_management.constants import year_longest, depth, depth_cm, \
    lsm_list, year_cmip5, year_cmip6
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import utils_management as mg
import numpy as np
import xarray as xr
import pandas as pd
import os
import sys
import matplotlib.pyplot as plt
from matplotlib import cm
from misc.plot_utils import plot_ts_trend, plot_ts_shade
import itertools as it
import multiprocessing as mp


year_str = str(year_longest[0]) + '-' + str(year_longest[-1])
period = pd.date_range(str(year_longest[0])+'-01-01', 
                       str(year_longest[-1])+'-12-31', freq = 'MS')
land_mask = 'vanilla'


data_list = list(it.product(['Mean_', 'OLC_', 'EC_'],
                            ['ORS', 'CMIP5', 'CMIP6', 'CMIP5+6', 'ALL']))

mapper_name = {'lsm': 'ORS', 'cmip5': 'CMIP5', 'cmip6': 'CMIP6', 
               '2cmip': 'CMIP5+6', 'all': 'ALL'}

season_list = ['Annual', 'DJF', 'MAM', 'JJA', 'SON']


def seasonal(data_frame, season, anomaly):
    if isinstance(season, str):
        if season == 'DJF':
            data_frame = data_frame.loc[data_frame.index.quarter == 1, :]
        elif season == 'MAM':
            data_frame = data_frame.loc[data_frame.index.quarter == 2, :]
        elif season == 'JJA':
            data_frame = data_frame.loc[data_frame.index.quarter == 3, :]
        elif season == 'SON':
            data_frame = data_frame.loc[data_frame.index.quarter == 4, :]
        else:
            data_frame = data_frame
    else:
        # Month
        data_frame = data_frame.loc[data_frame.index.month == season, :]
    data_frame = data_frame.groupby(data_frame.index.year).mean()
    if anomaly:
        data_frame = data_frame - data_frame.mean(axis = 0)
    return data_frame


# MODIFY
anomaly = True


for i in range(len(depth)):
    fig, axes = plt.subplots(nrows = len(data_list), ncols = len(season_list),
                             sharex = True, sharey = True, figsize = (16,22))
    fig.subplots_adjust(wspace = 0., hspace = 0.2)

    for sind, season in enumerate(season_list):
        d = depth[i]
        dcm = depth_cm[i]

        lsm = lsm_list[(year_str, d)]
        cmip5 = cmip5_availability(dcm, land_mask)
        cmip6 = list(set(mrsol_availability(dcm, land_mask, 'historical')) & \
                     set(mrsol_availability(dcm, land_mask, 'ssp585')))
        cmip6 = [x for x in cmip6 if 'r1i1p1f1' in x]

        #######################################################################
        # Read the datasets.
        #######################################################################
        collect_mean = pd.DataFrame(np.nan, index = np.unique(period.year), 
                                    columns = [x[0]+x[1] for x in data_list])
        collect_q95 = pd.DataFrame(np.nan, index = np.unique(period.year), 
                                   columns = [x[0]+x[1] for x in data_list])
        collect_q05 = pd.DataFrame(np.nan, index = np.unique(period.year),
                                   columns = [x[0]+x[1] for x in data_list])

        #########################
        # LSM
        #########################
        data_lsm = pd.DataFrame(np.nan, index = period, columns = lsm)
        for m in lsm:
            temp = pd.read_csv(os.path.join(mg.path_out(), 
                                            'standard_diagnostics_lsm',
                                            m + '_' + d + '_g_ts.csv'), 
                               index_col = 0, parse_dates = True)
            temp = temp.loc[(temp.index.year >= 1950) & \
                            (temp.index.year <= 2016), :]
            data_lsm.loc[temp.index, m] = temp.iloc[:, 0].values
        data_lsm = seasonal(data_lsm, season, anomaly)
        collect_mean.loc[:, 'Mean_ORS'] = data_lsm.mean(axis = 1)
        collect_q95.loc[:, 'Mean_ORS'] = np.nanpercentile(data_lsm.values, 95, 
                                                          axis = 1)
        collect_q05.loc[:, 'Mean_ORS'] = np.nanpercentile(data_lsm.values, 5, 
                                                          axis = 1)

        if i == 0:
            data_gleam = pd.read_csv(os.path.join(mg.path_out(),
                                                  'standard_diagnostics_lsm',
                                                  'GLEAMv3.3a_' + d + \
                                                  '_g_ts.csv'),
                                     index_col = 0, parse_dates = True)
            data_gleam = data_gleam.loc[(data_gleam.index.year >= 1950) & \
                                        (data_gleam.index.year <= 2016), :]
            data_gleam = seasonal(data_gleam, season, anomaly).iloc[:, 0]
            data_gleam = data_gleam.loc[1981:2016]
        if (i == 0) | (i == 1):
            data_era = pd.read_csv(os.path.join(mg.path_out(),
                                                'standard_diagnostics_lsm',
                                                'ERA-Land_' + d + '_g_ts.csv'),
                                   index_col = 0, parse_dates = True)
            data_era = data_era.loc[(data_era.index.year >= 1950) & \
                                    (data_era.index.year <= 2016), :]
            data_era = seasonal(data_era, season, anomaly).iloc[:, 0]

        #########################
        # CMIP5 & CMIP6
        #########################
        for name, model in zip(['cmip5', 'cmip6'], [cmip5, cmip6]):
            data = pd.DataFrame(np.nan, index = period, columns = cmip5)
            for m in model:
                if name == 'cmip5':
                    temp = pd.read_csv(os.path.join(mg.path_out(), 
                                                    'standard_diagnostics_' \
                                                    + name, m + '_' + dcm + \
                                                    '_g_ts.csv'), 
                                       index_col = 0, parse_dates = True)
                else:
                    temp = pd.read_csv(os.path.join(mg.path_out(), 
                                                    'standard_diagnostics_' \
                                                    + name, 'mrsol_' + m + \
                                                    '_' + dcm + '_g_ts.csv'), 
                                       index_col = 0, parse_dates = True)
                data.loc[:, m] = temp.iloc[:, 0].values
            data = seasonal(data, season, anomaly)
            collect_mean.loc[:, 'Mean_' + mapper_name[name]] = \
                data.mean(axis = 1)
            collect_q95.loc[:, 'Mean_' + mapper_name[name]] = \
                np.nanpercentile(data.values, 95, axis = 1)
            collect_q05.loc[:, 'Mean_' + mapper_name[name]] = \
                np.nanpercentile(data.values, 5, axis = 1)

        ##########################
        # 2CMIP
        ##########################
        data = pd.DataFrame(np.nan, index = period, columns = cmip5 + cmip6)
        for m in cmip5:
            temp = pd.read_csv(os.path.join(mg.path_out(),
                                            'standard_diagnostics_cmip5',
                                            m + '_' + dcm + '_g_ts.csv'),
                               index_col = 0, parse_dates = True)
            data.loc[:, m] = temp.iloc[:, 0].values
        for m in cmip6:
            temp = pd.read_csv(os.path.join(mg.path_out(),
                                            'standard_diagnostics_cmip6',
                                            'mrsol_' + m + '_' + dcm + \
                                            '_g_ts.csv'),
                               index_col = 0, parse_dates = True)
            data.loc[:, m] = temp.iloc[:, 0].values
        data = seasonal(data, season, anomaly)
        collect_mean.loc[:, 'Mean_CMIP5+6'] = data.mean(axis = 1)
        collect_q95.loc[:, 'Mean_CMIP5+6'] = \
            np.nanpercentile(data.values, 95, axis = 1)
        collect_q05.loc[:, 'Mean_CMIP5+6'] = \
            np.nanpercentile(data.values, 5, axis = 1)

        ##########################
        # All
        ##########################
        data = pd.DataFrame(np.nan, index = period,
                            columns = lsm + cmip5 + cmip6)
        for m in lsm:
            temp = pd.read_csv(os.path.join(mg.path_out(),
                                            'standard_diagnostics_lsm',
                                            m + '_' + d + '_g_ts.csv'),
                               index_col = 0, parse_dates = True)
            temp = temp.loc[(temp.index.year >= 1950) & \
                            (temp.index.year <= 2016), :]
            data.loc[temp.index, m] = temp.iloc[:, 0].values
        for m in cmip5:
            temp = pd.read_csv(os.path.join(mg.path_out(),
                                            'standard_diagnostics_cmip5',
                                            m + '_' + dcm + '_g_ts.csv'),
                               index_col = 0, parse_dates = True)
            data.loc[:, m] = temp.iloc[:, 0].values
        for m in cmip6:
            temp = pd.read_csv(os.path.join(mg.path_out(),
                                            'standard_diagnostics_cmip6',
                                            'mrsol_' + m + '_' + dcm + \
                                            '_g_ts.csv'),
                               index_col = 0, parse_dates = True)
            data.loc[:, m] = temp.iloc[:, 0].values
        data = seasonal(data, season, anomaly)
        collect_mean.loc[:, 'Mean_ALL'] = data.mean(axis = 1)
        collect_q95.loc[:, 'Mean_ALL'] = np.nanpercentile(data.values, 95,
                                                          axis = 1)
        collect_q05.loc[:, 'Mean_ALL'] = np.nanpercentile(data.values, 5,
                                                          axis = 1)

        ##########################
        # DOLCE
        ##########################
        for name in ['lsm', 'cmip5', 'cmip6', '2cmip', 'all']:
            if (name == 'lsm') & (name == 'all'):
                data = pd.read_csv(os.path.join(mg.path_out(),
                                                'standard_diagnostics_concat',
                                                'dolce_' + name, 
                                                'dolce_average_lu_weighted_' +\
                                                'ShrunkCovariance_' + d + '_'+\
                                                name + '_g_ts.csv'),
                                   index_col = 0, parse_dates = True)
            else:
                data = pd.read_csv(os.path.join(mg.path_out(),
                                                'standard_diagnostics_dolce_'+\
                                                name, 'lu_weighted_' + \
                                                'ShrunkCovariance_weighted_' +\
                                                'average_' + year_str + '_' + \
                                                d + '_g_ts.csv'),
                                   index_col = 0, parse_dates = True)
            data = seasonal(data, season, anomaly)
            collect_mean.loc[:, 'OLC_' + mapper_name[name]] = data.values[:, 0]
    
            if (name == 'lsm') & (name == 'all'):
                #temp = pd.read_csv(os.path.join(mg.path_out(),
                #                                'standard_diagnostics_concat',
                #                                'dolce_' + name, 
                #                                'dolce_uncertainty_' + \
                #                                'lu_weighted_' + \
                #                                'ShrunkCovariance_' +\
                #                                d + '_g_ts.csv'),
                #                   index_col = 0, parse_dates = True)
                continue
            else:
                temp = pd.read_csv(os.path.join(mg.path_out(),
                                                'standard_diagnostics_'
                                                'dolce_' + name,
                                                'lu_weighted_' + \
                                                'ShrunkCovariance_' +\
                                                'weighted_uncertainty_' + \
                                                year_str + '_' + d + \
                                                '_g_ts.csv'),
                                   index_col = 0, parse_dates = True)
            temp = seasonal(temp, season, anomaly)
            collect_q95.loc[:, 'OLC_' + mapper_name[name]] = \
                collect_mean.loc[:, 'OLC_' + mapper_name[name]] + \
                1.67 * temp.iloc[:, 0].values
            collect_q05.loc[:, 'OLC_' + mapper_name[name]] = \
                collect_mean.loc[:, 'OLC_'+ mapper_name[name]] - \
                1.67 * temp.iloc[:, 0].values

        ##########################
        # EC
        ##########################
        for name in ['lsm', 'cmip5', 'cmip6', '2cmip', 'all']:
            if (name == 'lsm') | (name == 'all'):
                begin = os.path.join(mg.path_out(), 
                                     'standard_diagnostics_concat', 
                                     'em_' + name,
                                     'CRU_v4.03_year_month_anomaly_9grid_')
                endd = '_' + d + '_g_ts.csv'
            else:
                begin = os.path.join(mg.path_out(),
                                     'standard_diagnostics_em_' + name,
                                     'CRU_v4.03_')
                endd = '_year_month_anomaly_9grid_' + year_str + '_' + \
                       d + '_g_ts.csv'
    
            temp0 = seasonal(pd.read_csv(begin + 'predicted' + endd,
                                        index_col = 0, parse_dates = True), 
                             season, False)
            collect_mean.loc[:, 'EC_' + mapper_name[name]] = temp0
            if anomaly:
                collect_mean.loc[:, 'EC_' + mapper_name[name]] -= \
                    np.mean(collect_mean.loc[:, 'EC_' + mapper_name[name]])
            temp = seasonal(pd.read_csv(begin + 'predicted_CI_upper' + endd, 
                                        index_col = 0, parse_dates = True), 
                            season, False)
            collect_q95.loc[:, 'EC_' + mapper_name[name]] = temp
            temp = seasonal(pd.read_csv(begin + 'predicted_CI_lower' + endd,
                                        index_col = 0, parse_dates = True),
                            season, False)
            collect_q05.loc[:, 'EC_' + mapper_name[name]] = temp
            if (name != 'lsm') & (name != 'all') & ~anomaly:
                collect_q95.loc[:, 'EC_' + mapper_name[name]] += \
                    collect_mean.loc[:, 'EC_' + mapper_name[name]] 
                collect_q05.loc[:, 'EC_' + mapper_name[name]] += \
                    collect_mean.loc[:, 'EC_' + mapper_name[name]]

        #######################################################################
        # Plot the result.
        #######################################################################
        for c_ind, c in enumerate(collect_mean.columns):
            ax = axes[c_ind, sind]
            plot_ts_trend(ax, collect_mean.index, collect_mean[c], 0.1, 0.1)
            if i == 0:
                h1, = ax.plot(data_gleam.index, data_gleam, 'b')
            if (i == 0) | (i == 1):
                h2, = ax.plot(data_era.index, data_era, 'r')
    
            if not anomaly:
                plot_ts_shade(ax, collect_mean.index, 
                              ts = {'min': collect_q05[c],
                                    'mean': collect_mean[c],
                                    'max': collect_q95[c]}, ts_col = 'b')
            if sind == 0:
                ax.set_ylabel(c)
            if c_ind == 0:
                ax.set_title(season)
            if (sind == 0) & (c_ind == 0):
                ax.legend([h1, h2], ['GLEAM', 'ERA-Land'], ncol = 2,
                          loc = 'center', bbox_to_anchor = (-16,3))
    
    if anomaly:
        fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                                 'global_compare', year_str + '_' + d + '_' + \
                                 'anomaly.png'), dpi = 600.)
    else:
        fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                                 'global_compare', year_str + '_' + d + \
                                 '.png'), dpi = 600.)
