# -*- coding: utf-8 -*-
"""
Created on Wed Feb 27 11:33:30 2019

@author: ywang254
"""
import os
import xarray as xr
import utils_management as mg

path_mstmip = os.path.join(mg.path_data(), 'Reanalysis', 'ERA-Interim')

ds = xr.open_dataset(os.path.join(path_mstmip, 
                                  'Monthly_means_of_daily_means_1970.nc'))

ds.close()