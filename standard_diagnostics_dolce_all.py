"""
Compute the following for every DOLCE-weighted result:

1. Global map (averaged over time): NetCDF
2. Global average time series.
3. Latitude bands average time series.
4. Continental average time series.
5. SREX region average time series.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth, depth_cm
from misc.dolce_utils import get_cov_method
from misc.ismn_utils import get_ismn_aggr_method
from misc.standard_diagnostics import standard_diagnostics
import os
import pandas as pd


year = REPLACE1

i = REPLACE2 # [0,1,2,3]
d = depth[i]

cov = REPLACE3 # range(4)
cov_method = get_cov_method(cov)


ismn_aggr_ind = REPLACE4
simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = get_ismn_aggr_method(simple[ismn_aggr_ind],
                                        dominance_lc[ismn_aggr_ind],
                                        dominance_threshold)


for val in ['weighted_average', 'weighted_uncertainty']:
    fi = os.path.join(mg.path_out(), 'dolce_all_product', 
                      val + '_' + str(year[0]) + '-' + str(year[-1]) + \
                      '_' + d + '_' + ismn_aggr_method + '_' + \
                      cov_method + '.nc')

    data = xr.open_dataset(fi, decode_times=False)

    time = pd.date_range(str(year[0])+'-01-01', str(year[-1])+'-12-31',
                         freq = 'MS')

    if val == 'weighted_uncertainty':
        values = data.sm_uncertainty.values
    else:
        values = data.sm.values

    standard_diagnostics(values, time, data.lat, data.lon, 
                         os.path.join(mg.path_out(), 
                                      'standard_diagnostics_dolce_all'),
                         ismn_aggr_method + '_' + cov_method + '_' + val + \
                         '_' + str(year[0]) + '-' + str(year[-1]) + '_' + d)

    data.close()
