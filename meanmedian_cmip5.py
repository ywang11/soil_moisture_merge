# -*- coding: utf-8 -*-
"""
Created on Tue May 14 11:40:17 2019

@author: ywang254

Due to memory issue, calculate the median across the CMIP5's for each year.
"""
import os
import xarray as xr
import numpy as np
import utils_management as mg
from utils_management.constants import depth, depth_cm, cmip5, \
    year_longest, year_cmip5, target_lat, target_lon
from misc.cmip5_availability import cmip5_availability
import itertools as it
import multiprocessing as mp


## MODIFY
##operation = 'mean' # 'mean', 'median'
##for i,d in enumerate(depth):

land_mask = 'vanilla'


def calc(option):
    operation, i = option
    d = depth[i]

    dcm = depth_cm[i]
    cmip5 = cmip5_availability(dcm, land_mask)

    collect_years = np.empty([12*len(year_longest), len(target_lat),
                              len(target_lon)])
    for y in year_longest:
        # Pool the land surface model soil moisture into an array
        collection = np.empty([12, len(target_lat), len(target_lon),
                               len(cmip5)])
        for j,model in enumerate(cmip5):
            if y <= year_cmip5[-1]:
                scn = 'historical'
            else:
                scn = 'rcp85'
            data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 
                                                'Interp_Merge',
                                                land_mask, 'CMIP5', model,
                                                'sm_' + scn + '_r1i1p1_' + \
                                                str(y) + '_' + dcm + '.nc'),
                                   decode_times=False)
            collection[:, :, :, j] = data.sm.values.copy()
            dim0 = data.sm.time.copy()
            data.close()

        # Calculate the median along the last dimension
        if operation == 'mean':
            model_merge = np.nanmean(collection, axis = 3)
        else:
            model_merge = np.nanmedian(collection, axis=3)

        del collection # free up memory

        # 
        collect_years[(12*(y-year_longest[0])):(12*(y-year_longest[0]+1)),
                      :, :] = model_merge

    dim_time = np.arange( (year_longest[0]-1900.)*12,
                          (year_longest[-1]-1900.+1)*12, 1. )
    dim1 = xr.DataArray(dim_time, coords={'time': dim_time}, 
                        dims=['time'], attrs=dim0.attrs)

    collect_year2 = xr.DataArray(collect_years, coords={'time': dim1, 
                                 'lat': target_lat, 'lon': target_lon},
                                 dims=['time','lat','lon'])

    del collect_years # free up memory

    collect_year2.to_dataset(name='sm').to_netcdf(os.path.join(mg.path_out(),
        'meanmedian_cmip5', operation + '_' + d + '_' + \
        str(year_longest[0]) + '-' + str(year_longest[-1]) + '.nc'))

p = mp.Pool(8)
p.map_async(calc, list(it.product(['mean', 'median'], range(4))))
p.close()
p.join()
