""" Perform the homogeneity test using station data. """
import utils_management as mg
from utils_management.constants import depth, year_longest, depth_cm
from misc.ismn_get_monthly import ismn_get_monthly
from misc.ismn_utils import get_ismn_aggr_method
from misc.homogeneity import calc_q_test
import os
import pandas as pd
import numpy as np
import time
import xarray as xr
from datetime import datetime
import itertools as it
from scipy.stats import pearsonr, linregress, ranksums, fligner


def get_ismn_list(path_lu, gr):
    f = open(os.path.join(path_lu, gr), 'r')
    grid_coords = [float(x) for x in f.readline().split(',')]
    f.close()
    stations_lu = pd.read_csv(os.path.join(path_lu, gr),
                              skiprows = 3)
    return grid_coords, stations_lu


def get_ismn_data(p):
    try:
        monthly_data = ismn_get_monthly(os.path.join(mg.path_data(),
                                                     'ISMN2', p['Network']),
                                        p['Station'], p['Depth-Actual'],
                                        p['Sensor'],
                                        pd.date_range(str(year_longest[0]) + '-01-01',
                                                      str(year_longest[-1]) + '-12-31',
                                                      freq = 'MS', tz = 'UTC'))
    except Exception as e:
        print(e)
        return pd.Series(dtype = float,
                         index = pd.date_range('1990-01-01', periods = 0, freq = 'MS'))

    if type(monthly_data) == type(None):
        raise IOError('The station has no good data. Why did it get in?')

    return monthly_data


def get_prod_data(prod):
    path_prod = mg.path_to_final_prod(prod, d, dcm)
    hr = xr.open_dataset(path_prod, decode_times = False)
    hr['time'] = pd.date_range('1950-01-01', '2016-12-31', freq = 'MS')
    if 'em' in prod:
        sm = hr['predicted'].copy(deep = True)
    else:
        sm = hr['sm'].copy(deep = True)
    sm = sm[sm['time'].to_index().year >= 1970, :, :]
    hr.close()
    return sm


# MODIFY
dind = REPLACE1
d = depth[dind]
dcm = depth_cm[dind]
prod = 'REPLACE2' # mean_lsm


#
path_lu = os.path.join(mg.path_intrim_out(), 'ismn_lu', d)
grids = os.listdir(path_lu)
sm = get_prod_data(prod)


#
time_ranges = [pd.date_range('1970-01-01', '1980-12-31', freq = 'MS'),
               pd.date_range('1981-01-01', '2010-12-31', freq = 'MS'),
               pd.date_range('2011-01-01', '2016-12-31', freq = 'MS')]


#
pval_collection = pd.DataFrame(np.nan, index = grids,
                               columns = pd.MultiIndex.from_product([['wilcoxon', 'fligner'],
                                                                     range(1,
                                                                           len(time_ranges))]))
for gr in grids:
    # Read the ISMN data in this grid.
    grid_coords, stations_lu = get_ismn_list(path_lu, gr)
    
    # Read the soil moisture time series for each ISMN station and 
    # aggregate to monthly time scale.
    for i,p in stations_lu.iterrows():
        monthly_data = get_ismn_data(p)

        if i==0:
            stations_monthly_data = monthly_data.to_frame(name=str(i))
        else:
            stations_monthly_data = pd.concat([stations_monthly_data, 
                                               monthly_data.to_frame(name=str(i))],
                                              join='outer', axis=1)
    if len(stations_monthly_data) == 0:
        print('There is no station for ' + gr)
        continue
    
    stations_monthly_data.dropna(axis=0, how='all', inplace=True)

    # Read the gridded data from product.
    xx = np.argmin(np.abs(sm['lat'].values - grid_coords[-1]))
    yy = np.argmin(np.abs(sm['lon'].values - grid_coords[0]))
    prod_monthly_data = pd.Series(sm[:, xx, yy].values, sm['time'].to_index())

    #
    matched = pd.concat([stations_monthly_data,
                         prod_monthly_data.to_frame(name = 'target')],
                        join = 'inner', axis = 1)

    for period in range(1, len(time_ranges)):
        # Check the overlapping period & calculate correlation
        a = time_ranges[period - 1]
        b = time_ranges[period]

        aa = matched.loc[(matched.index >= a[0]) & (matched.index <= a[-1]), :]
        bb = matched.loc[(matched.index >= b[0]) & (matched.index <= b[-1]), :]

        if len(aa) > 0 and len(bb) > 0:
            wx_pval, fl_pval = calc_q_test(matched, b[0].year)
            pval_collection.loc[gr, ('wilcoxon', period)] = wx_pval
            pval_collection.loc[gr, ('fligner', period)] = fl_pval
pval_collection.to_csv(os.path.join(mg.path_out(), 'homogeneity_test',
                                    'pval_' + dcm + '_' + prod + '.csv'))
