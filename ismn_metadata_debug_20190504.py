# -*- coding: utf-8 -*-
"""
Created on Sat May  4 10:45:14 2019

@author: ywang254
"""

from ismn.interface import ISMN_Interface
import utils_management as mg
import os
import pandas as pd
import numpy as np


_, dirnames, _ = next(os.walk(os.path.join(mg.path_data(), 'ISMN')))

target_depth_from = np.array([0., .1, .3, .5]) # unit: m
target_depth_to   = np.array([.1, .3, .5, 1.]) # unit: m

time_range = pd.date_range(start='1950-01-01', end='2016-12-31', tz='UTC')

time_range_by_month = pd.date_range(start='1950-01-01', end='2016-12-31', 
                                    tz='UTC', freq='MS')


##dirnames = ['RISMA']

multi_sensor_networks = []
multi_sensor_stations = []

for network in dirnames:
    path_to_ismn_data = os.path.join(mg.path_data(), 'ISMN', network)    
    ISMN_reader = ISMN_Interface(path_to_ismn_data)

    stations = ISMN_reader.list_stations(network = network)

    if isinstance(stations, type(None)):
        continue

    meta_list = ['Network', 'Station', 'Lat', 'Lon', 'Depth-Fit',
                 'Depth-Actual', 'Sensor', 'Land Cover 2000',
                 'Land Cover 2005', 'Land Cover 2010', 'Land Cover in situ',
                 'Climate Zone'] + list(time_range_by_month)
    network_metadata = pd.DataFrame(data = np.nan, index = [0], 
                                    columns = meta_list, dtype=object)

    for st in stations:
        print('Network = ' + network + ' Station = ' + st)

        station_obj = ISMN_reader.get_station(st)

        # get the variables that this station measures
        variables = station_obj.get_variables()

        if not 'soil moisture' in variables:
            print('Soil moisture is not measured at ' + network + ' *********')
            print('which measures:')
            print(variables)
            continue

        # to make sure the selected variable is not measured
        # by different sensors at the same depths
        # we also select the first depth and the first sensor
        # even if there is only one
        var = 'soil moisture'
        depths_from, depths_to = station_obj.get_depths(var)

        sensors = []
        for ii in range(len(depths_from)):
            temp = list(station_obj.get_sensors(var, 
                        depths_from[ii], depths_to[ii]))
            if len(temp) > 1:
                multi_sensor_networks.append(network)
                multi_sensor_stations.append(st)

multi_sensors = pd.DataFrame(np.array([multi_sensor_networks, multi_sensor_stations]).T,
                             index = range(len(multi_sensor_networks)),
                             columns = ['Network', 'Station'])
multi_sensors.to_csv('temp.csv')