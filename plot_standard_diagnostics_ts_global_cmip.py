"""
2019/08/05
ywang254@utk.edu

Plot the time series of global mean annual mean soil moisture of the 
 CMIP5/CMIP6 models.
"""
import matplotlib.pyplot as plt
import utils_management as mg
from utils_management.constants import year_longest, depth, depth_cm
from misc.plot_utils import plot_ts_shade, plot_ts_trend
from misc.standard_diagnostics import seasonal_avg
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability
import pandas as pd
import os
import numpy as np
import itertools as it
import multiprocessing as mp
from matplotlib.cm import get_cmap


year = year_longest
cmap = get_cmap('nipy_spectral')
land_mask = 'vanilla'


# MODIFY
model_set = 'cmip6' # 'cmip5', 'cmip6'


#
global_ts_all_depth = {}
for i,d in enumerate(depth):
    dcm = depth_cm[i]

    if model_set == 'cmip5':
        model_list = cmip5_availability(dcm, land_mask)
    else:
        cmip6_list_1 = mrsol_availability(dcm, land_mask, 'historical')
        cmip6_list_2 = mrsol_availability(dcm, land_mask, 'ssp585')
        cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) )
        model_list = [x for x in cmip6_list if 'r1i1p1f1' in x]

    for l in model_list:
        if model_set == 'cmip5':
            fname = os.path.join(mg.path_out(), 'standard_diagnostics_' + \
                                 model_set, l + '_' + dcm + '_g_ts.csv')
        else:
            fname = os.path.join(mg.path_out(), 'standard_diagnostics_' + \
                                 model_set, 'mrsol_' + l + '_' + dcm + \
                                 '_g_ts.csv')
        if not os.path.exists(fname):
            continue
        temp = pd.read_csv(fname, index_col = 0, parse_dates = True).iloc[:,0]
        global_ts_all_depth[(d, l)] = seasonal_avg(temp)

    if i == 0:
        model_list0 = model_list


#
def plotter(option):
    subset, subset_name = option
    clist = [cmap((ind+0.5)/len(subset)) for ind in range(len(subset))]

    for s in ['annual', 'DJF', 'MAM', 'JJA', 'SON']:
        fig, axes = plt.subplots(nrows = len(depth_cm), ncols = 2, 
                                 sharex = True, 
                                 sharey = False, figsize = (10, 6.5))
        fig.subplots_adjust(hspace = 0., wspace = 0.2)

        # (1) Soil Moisture
        for i,d in enumerate(depth):
            dcm = depth_cm[i]

            ax = axes[i, 0]
            h = [None] * len(subset)
            for ind, l in enumerate(subset):
                if not (d,l) in global_ts_all_depth.keys():
                    continue

                h[ind], = ax.plot(global_ts_all_depth[(d,l)].index, 
                                  global_ts_all_depth[(d,l)].loc[:, s], 
                                  color = clist[ind])

            if (i == 0):
                ax.set_title('Soil Moisture (m$^3$/m$^3$)')

            ax.set_xlim([year[0], year[-1]])
            ax.set_xlabel('Year')
            ax.set_ylabel(dcm)
            ax.set_ylim([0., 0.5])
            ax.set_yticks(np.linspace(0.1, 0.4, 5))

            if i == 0:
                ax.legend(h, subset, loc = [0.3, -4],
                          ncol = min(4, len(subset)))

        # (2) Soil Moisture Anomaly
        for i,d in enumerate(depth):
            dcm = depth_cm[i]

            ax = axes[i, 1]
            h = [None] * len(subset)
            for ind, l in enumerate(subset):
                if not (d,l) in global_ts_all_depth.keys():
                    continue
                h[ind], = ax.plot(global_ts_all_depth[(d,l)].index, 
                                  global_ts_all_depth[(d,l)].loc[:, s] - \
                                  global_ts_all_depth[(d,l)].loc[:, 
                                                                 s].mean(), 
                                  color = clist[ind])

            if (i == 0):
                ax.set_title('Anomaly (m$^3$/m$^3$)')

            ax.set_xlim([year[0], year[-1]])
            ax.set_xlabel('Year')
            ax.set_ylim([-0.01, 0.01])
            ax.set_yticks(np.linspace(-0.008, 0.008, 5))

        fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot', 
                                 'ts_global', model_set, s + '_' + \
                                 subset_name + '.png'),
                    dpi = 600., bbox_inches = 'tight')
        plt.close(fig)


plotter([model_list0, model_set])
