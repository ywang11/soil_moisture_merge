"""
2019/11/23
ywang254@utk.edu

Merge the mean of the land surface/CMIP5/CMIP6 models with the
"month_anomaly" predictions of the emergent constraint method.
"""
import xarray as xr
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, lsm_list, depth, depth_cm
from misc.ismn_utils import get_ismn_aggr_method
import os
import utils_management as mg
import numpy as np
import pandas as pd
import itertools as it


###############################################################################
# Setup of the emergent constraint method.
###############################################################################
# year_longest, year_shorter, year_shorter2, year_shortest
year = REPLACE1
year_str = str(year[0]) + '-' + str(year[-1])
period = pd.date_range(str(year[0]) + '-01-01', 
                       str(year[-1]) + '-12-31', freq = 'MS')

i = REPLACE2 #[0, 1, 2, 3]
d = depth[i]
dcm = depth_cm[i]

model = 'REPLACE3' # 'lsm', 'cmip5', 'cmip6', '2cmip', 'all'

opt = 'REPLACE4' # 'year_month_anomaly_9grid', 'year_month_anomaly_1grid'


###############################################################################
# Load the mean climatology - separate between months.
###############################################################################
data = xr.open_dataset(os.path.join(mg.path_out(), 'meanmedian_' + model,
                                    'mean_' + d + '_' + year_str + '.nc'),
                       decode_times = False)
# ---- subset to year_shortest
sm = data.sm[(period.year >= year_shortest[0]) & \
             (period.year <= year_shortest[-1]), :, :].values.copy()
data.close()
mean_climatology = np.empty([12, len(data.lat), len(data.lon)])
mean_climatology[:, :, :] = np.nan
for m in range(12):
    mean_climatology[m, :, :] = sm[m::12, :, :].mean(axis = 0)


###############################################################################
# Read the emergent constraint-based soil moisture anomaly.
# Add the climatology to the anomalies.
###############################################################################
for y, prefix in it.product(year, ['predicted']): # , 'predicted_CI_upper',
#                                   'predicted_CI_lower']):
    data = xr.open_dataset(os.path.join(mg.path_out(), 'em_' + model + '_corr',
                                        'CRU_v4.03_' + opt + '_' + dcm + \
                                        '_' + year_str, prefix + '_' + \
                                        str(y) + '.nc'))

    em_sm = data[prefix] + mean_climatology

    em_sm.to_dataset(name = prefix \
    ).to_netcdf(os.path.join(mg.path_out(),
                             'em_' + model + '_corr',
                             'CRU_v4.03_' + \
                             opt.replace('anomaly', 'restored') + '_' + dcm + \
                             '_' + year_str, prefix + '_' + str(y) + '.nc'))

    data.close()
