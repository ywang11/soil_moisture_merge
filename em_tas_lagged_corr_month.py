# -*- coding: utf-8 -*-
"""
20191118
ywang254@utk.edu

Estimate regression relationship between grid-scale precipitation, temperature,
 and modeled soil moisture at different depths with +/- six month lag.
"""
import utils_management as mg
from utils_management.constants import met_to_lsm, year_longest, \
    year_shorter, year_shorter2, year_shortest, lsm_list, depth, depth_cm, \
    target_lat, target_lon, year_cmip5, year_cmip6
from misc.spatial_utils import extract_grid
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import os
import xarray as xr
import pandas as pd
import numpy as np
import itertools as it
import time
import multiprocessing as mp
from scipy import stats


land_mask = 'vanilla'


###############################################################################
# Settings
###############################################################################
# Soil layer of interest.
i = REPLACE1 # [0, 1, 2, 3]
d = depth[i]
dcm = depth_cm[i]

# Land surface models relevant to this soil layer and target period.
model = 'REPLACE2' # 'lsm', 'cmip5', 'cmip6'
if model == 'lsm':
    model_list = lsm_list[(str(year_shortest[0]) + '-' + \
                           str(year_shortest[-1]), d)]
elif model == 'cmip5':
    model_list = cmip5_availability(dcm, land_mask)
elif model == 'cmip6':
    model_list = list(set(mrsol_availability(dcm, land_mask, 'historical')) &\
                      set(mrsol_availability(dcm, land_mask, 'ssp585')))
    model_list = sorted([x for x in model_list if 'r1i1p1f1' in x])


# Prefix of the precipitation and temperature data that are to be used in
# the regression.
if model == 'lsm':
    temp1 = mg.path_to_pr(land_mask)
    temp2 = mg.path_to_tas(land_mask)
    pr_list = []
    tas_list = []
    for l in model_list:
        for met, value in met_to_lsm.items():
            if l in value:
                pr_list.append(temp1[met])
                tas_list.append(temp2[met])
else:
    pr_list = []
    tas_list = []
    for m in model_list:
        pr_list.append(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                    land_mask, model.upper(), m, 'pr_'))
        tas_list.append(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                     land_mask, model.upper(), m, 'tas_'))


##DEBUG
##for m_ind, m in enumerate(model_list):
##lag = {-6..6}
##var = ['temperature', 'precipitation']
def corr(m_ind):
    ##m_ind, lag, var = option
    m = model_list[m_ind]

    ###########################################################################
    # Read the soil moisture data for regression.
    ###########################################################################
    if model == 'lsm':
        flist = [os.path.join(mg.path_intrim_out(), 'Interp_Merge', 
                              land_mask, m, m + '_' + str(y) + '_' + \
                              dcm + '.nc') for y in year_longest]
        # Skip years that do not exist for this model.
        year = [year_longest[i] for i in range(len(year_longest)) \
                if os.path.exists(flist[i])]
        flist = [f for f in flist if os.path.exists(f)]
    elif model == 'cmip5':
        f_hist = [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                               land_mask, model.upper(), m, 'sm_historical_' +\
                               'r1i1p1_' + str(y) + '_' + dcm + '.nc') \
                  for y in year_cmip5]
        f_85 = [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                             land_mask, model.upper(), m, 'sm_rcp85_' +\
                             'r1i1p1_' + str(y) + '_' + dcm + '.nc') \
                for y in range(year_cmip5[-1]+1, year_longest[-1]+1)]
        flist = f_hist + f_85
        year = year_longest
    elif model == 'cmip6':
        f_hist = [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                               land_mask, model.upper(), m, 
                               'mrsol_historical_' + str(y) + '_' + dcm + \
                               '.nc') for y in year_cmip6]
        f_85 = [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                             land_mask, model.upper(), m, 'mrsol_ssp585_' +\
                             str(y) + '_' + dcm + '.nc') \
                for y in range(year_cmip6[-1]+1, year_longest[-1]+1)]
        flist = f_hist + f_85
        year = year_longest

    data = xr.open_mfdataset(flist, decode_times = False)
    sm = data['sm'].values.copy()
    data.close()

    for var in ['precipitation', 'temperature']:
        #######################################################################
        # Read the precipitation or temperature data for regression.
        #######################################################################
        if var == 'precipitation':
            p = pr_list[m_ind]
        else:
            p = tas_list[m_ind]

        if model == 'lsm':
            flist = [p + str(y) + '.nc' for y in year]
        elif model == 'cmip5':
            f_hist = [p + 'historical_r1i1p1_' + str(y) + '.nc' \
                      for y in year_cmip5]
            f_85 = [p + 'rcp85_r1i1p1_' + str(y) + '.nc' \
                    for y in range(year_cmip5[-1]+1, year[-1]+1)]
            flist = f_hist + f_85
        elif model == 'cmip6':
            f_hist = [p + 'historical_' + str(y) + '.nc' \
                      for y in year_cmip6]
            f_85 = [p + 'ssp585_' + str(y) + '.nc' \
                    for y in range(year_cmip6[-1]+1, year[-1]+1)]
            flist = f_hist + f_85


        data2 = xr.open_mfdataset(flist, decode_times = False)
        if var == 'precipitation':
            met = data2['pr'].values.copy()
        else:
            met = data2['tas'].values.copy()
        data2.close()


        start = time.time()

        for month, lag in it.product(range(12), range(-6, 6)):
            print('month ' + str(month) + ' lag ' + str(lag))

            if (month + lag) < 0:
                # ---- skip the first year.
                sm_data = sm[(month+12)::12, :, :].copy()

                met_data = met[(month + lag + 12): \
                               (met.shape[0]-12):12, :, :].copy()

            elif (month + lag) > 11:
                # ---- skip the last year.
                sm_data = sm[month:(sm.shape[0] - 12):12, :, :].copy()

                met_data = met[(month + lag)::12, :, :].copy()

            else:
                sm_data = sm[month::12,:, :].copy()

                met_data = met[(month + lag)::12, :, :].copy()

            if data.lat.values[0] > -87:
                # Pad to global data
                temp = target_lat < data.lat.values[0]
                sm_data = np.concatenate([np.full([sm_data.shape[0], 
                                                   sum(temp), 
                                                   sm_data.shape[2]],
                                                  np.nan), sm_data], axis = 1)
                met_data = np.concatenate([np.full([met_data.shape[0], 
                                                    sum(temp),
                                                    met_data.shape[2]],
                                                   np.nan),met_data], axis = 1)

            # Convert to anomaly.
            sm_data = sm_data - sm_data.mean(axis = 0, keepdims = True)
            met_data = met_data - met_data.mean(axis = 0, keepdims = True)


            ###################################################################
            # Calculate the correlation coefficient.
            ###################################################################
            corr = np.full([len(target_lat), len(target_lon)], np.nan)
            for a, b in it.product(range(met_data.shape[1]), 
                                   range(met_data.shape[2])):
                if np.isnan(met_data[0, a, b]):
                    continue
                corr[a, b] = stats.pearsonr(met_data[:,a,b],
                                            sm_data[:,a,b])[0]


            xr.DataArray(corr, coords = {'lat': target_lat, 
                                         'lon': target_lon}, 
                         dims = ['lat', 'lon'] \
                     ).to_dataset(name = 'corr' \
                     ).to_netcdf(os.path.join(mg.path_intrim_out(), 
                                              'em_tas_lagged_corr_month', 
                                              model + '_' + m + '_' + \
                                              dcm + '_' + str(month) + \
                                              '_' + str(lag) + '_' + var + \
                                              '.nc'))
        end = time.time()
        print(var + ' ' + m + ': ' + str((end - start)/60) + ' minutes.')


pool = mp.Pool(6) # mp.cpu_count()
pool.map_async(corr, range(len(model_list)))
pool.close()
pool.join()

##corr(3)
