# -*- coding: utf-8 -*-
"""
2020/05/20

@author: ywang254

Calculate the standard deviations across the lsm's for each year.
"""
import os
import xarray as xr
import numpy as np
import utils_management as mg
from utils_management.constants import depth, depth_cm, lsm_list, \
    year_longest, year_shorter, year_shorter2, year_shortest, target_lat, \
    target_lon
import multiprocessing as mp
import itertools as it


land_mask = 'vanilla'
year = year_longest


for i,d in enumerate(depth):

    collect_years = np.empty([12*len(year), len(target_lat), len(target_lon)])
    for y in year:
        llist = lsm_list[(str(year[0]) + '-' + str(year[-1]), d)]

        # Pool the land surface model soil moisture into an array
        collection = np.empty([12, len(target_lat), len(target_lon),
                               len(llist)])
        for j,lsm in enumerate(llist):
            data = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                                'Interp_Merge', land_mask,
                                                lsm, lsm + '_' + \
                                                str(y) + '_' + depth_cm[i] + \
                                                '.nc'), decode_times=False)
            collection[:, :, :, j] = data.sm.values.copy()
            dim0 = data.sm.time.copy()
            data.close()

        # Calculate the std along the last dimension
        # ---- Use NaN std because ESA-CCI has missing values.
        lsm_merge = np.nanstd(collection, axis=3, ddof = 1)

        del collection # free up memory

        #
        collect_years[(12*(y-year[0])):(12*(y-year[0]+1)), :,
                      :] = lsm_merge

    dim_time = np.arange( (year[0]-1900.)*12, (year[-1]-1900.+1)*12, 1. )
    dim1 = xr.DataArray(dim_time, coords={'time': dim_time}, 
                        dims=['time'], attrs=dim0.attrs)

    collect_year2 = xr.DataArray(collect_years, coords={'time': dim1, 
                                 'lat': target_lat, 'lon': target_lon},
                                 dims=['time','lat','lon'])

    del collect_years # free up memory

    collect_year2.to_dataset(name='sm').to_netcdf(os.path.join(mg.path_out(),
        'meanmedian_lsm', 'std_' + d + '_' + str(year[0]) + '-' + \
        str(year[-1]) + '.nc'))
