""" Normalize each column by the maximum absolute value, and label the maximum absolute value
    on top. """

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec, GridSpecFromSubplotSpec
import utils_management as mg
import matplotlib as mpl
from matplotlib.colors import LogNorm, Normalize
import itertools as it


stat_list = ['climatology', 'seasonality', 'trend', 'anomalies']
stat2_list = ['Bias', 'RMSE', 'Corr']


prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5',
             'em_cmip6', 'em_2cmip', 'em_all']
prod_name_list = ['Mean ORS', 'OLC ORS', 'EC ORS', 'EC CMIP5',
                  'EC CMIP6', 'EC CMIP5+6', 'EC ALL']
eval_list = ['SMOS_L3', 'SoMo_0-10cm', 'SoMo_10-30cm', 'SMERGE_v2',
             'SoMo_30-50cm', 'SMOS_L4', 'GLEAMv3.3a_0-100cm']
eval_name_list = ['SMOS L3\n0-5cm', 'SoMo\n0-10cm', 'SoMo\n10-30cm',
                  'SMERGE v2\n0-40cm', 'SoMo\n30-50cm', 'SMOS L4\n0-100cm',
                  'GLEAMv3.3a\n0-100cm']


mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 6
mpl.rcParams['ytick.left'] = False
mpl.rcParams['ytick.right'] = False
mpl.rcParams['xtick.top'] = False
mpl.rcParams['xtick.bottom'] = False
mpl.rcParams['xtick.labelbottom'] = True
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'
fig, axes = plt.subplots(len(stat_list), len(stat2_list),
                         figsize = (6.5, 6.5))
for sind2, stat2 in enumerate(stat2_list):
    for sind, stat in enumerate(stat_list):
        #
        prod = pd.read_csv(os.path.join(mg.path_out(), 'validate', stat,
                                        'results_products.csv'),
                           index_col = [0,1], header = 0).loc[stat2, :]
        lsm = pd.read_csv(os.path.join(mg.path_out(), 'validate', stat,
                                       'results_lsm.csv'),
                          index_col = [0,1], header = 0).loc[stat2, :]
        cmip5 = pd.read_csv(os.path.join(mg.path_out(), 'validate', stat,
                                         'results_cmip5.csv'),
                            index_col = [0,1], header = 0).loc[stat2, :]
        cmip6 = pd.read_csv(os.path.join(mg.path_out(), 'validate', stat,
                                         'results_cmip6.csv'),
                            index_col = [0,1], header = 0).loc[stat2, :]

        #
        a = prod.loc[prod_list, :].values
        b = np.mean(lsm.values, axis = 0).reshape(1, -1)
        c = np.mean(cmip5.values, axis = 0).reshape(1, -1)
        d = np.mean(cmip6.values, axis = 0).reshape(1, -1)
        values = np.concatenate([a, b, c, d], axis = 0)
        values2 = np.zeros(values.shape, dtype = bool)
        for ii,jj in it.product(range(len(prod_list)),
                                range(lsm.values.shape[1])):
            if 'ORS' in prod_list[ii]:
                values2[ii,jj] = np.abs(a[ii,jj]) < np.abs(b[0,jj])
            elif prod_list[ii] == 'EC CMIP5':
                values2[ii,jj] = np.abs(a[ii,jj]) < np.abs(c[0,jj])
            elif prod_list[ii] == 'EC CMIP6':
                values2[ii,jj] = np.abs(a[ii,jj]) < np.abs(d[0,jj])
            elif prod_list[ii] == 'EC CMIP5+6':
                values2[ii,jj] = np.abs(a[ii,jj]) < \
                    np.mean([np.abs(c[0,jj]), np.abs(d[0,jj])])
            else:
                values2[ii,jj] = np.abs(a[ii,jj]) < \
                    np.mean([np.abs(b[0,jj]), np.abs(c[0,jj]),
                             np.abs(d[0,jj])])
            if stat2 == 'Corr':
                values2[ii,jj] = ~values2[ii,jj]

        #
        if (stat == 'trend') and ((stat2 == 'Bias') or (stat2 == 'RMSE')):
            values = values * 100 # annual to 100-year trend

        # normalize by column
        values_max = np.max(values, axis = 0)
        values = values / np.broadcast_to(values_max.reshape(1,-1), values.shape)

        #
        if stat2 == 'Corr':
            vmin = 0.; vmax = 1.
            norm = Normalize(vmin, vmax)
            unit = ' (-)'
        elif stat2 == 'Bias':
            vmin = -1.; vmax = 1.
            norm = Normalize(vmin, vmax)
            if stat == 'trend':
                unit = ' (m$^3/m^3$/100 years)'
            else:
                unit = ' (m$^3/m^3$)'
        elif stat2 == 'RMSE':
            vmin = 0.5; vmax = 1.
            norm = Normalize(vmin, vmax) # LogNorm(vmin, vmax)
            if stat == 'trend':
                unit = ' (m$^3/m^3$/100 years)'
            else:
                unit = ' (m$^3/m^3$)'

        ax = axes[sind, sind2]
        cf = ax.imshow(values[::-1, :], cmap = 'Spectral',
                       aspect = 0.47, norm = norm)
        for ii,jj in it.product(range(len(prod_list)),
                                range(lsm.values.shape[1])):
            if values2[ii,jj]:
                ax.scatter(jj, len(prod_list) + 2 - ii,
                           marker = 'x', c = 'k', s = 6)

        ax.set_ylim([-0.5, values.shape[0] - 0.5])
        ax.set_xlim([-0.5, values.shape[1] - 0.5])
        ax.set_yticks(np.arange(0., values.shape[0], 1))
        ax.set_xticks(np.arange(0., values.shape[1], 1))
        ax.tick_params('both', length = 1.)
        if sind == (len(stat_list)-1):
            ax.set_xticklabels(eval_name_list, rotation = 90)
        else:
            ax.set_xticklabels([])
        if sind == 0:
            ax.set_title(stat2)

        if sind2 == 0:
            ax.set_yticklabels((prod_name_list + \
                                ['ORS', 'CMIP5', 'CMIP6'])[::-1])
        else:
            ax.set_yticklabels([])
        ax.set_ylabel(stat.capitalize() + unit)
        ax.text(-0.08, 1.06, '(' + lab[sind * len(stat2_list) +
                                       sind2] + ')',
                transform = ax.transAxes)

        #
        ax2 = ax.twiny()
        #ax2.spines['top'].set_color('tab:blue')
        ax2.set_xlim([-0.5, values.shape[1] - 0.5])
        ax2.set_xticks(np.arange(0., values.shape[1], 1))
        ax2.set_xticklabels(['%.2f' % tt for tt in values_max])
        ax2.tick_params(axis = 'x', colors = 'tab:blue', length = 1., labelcolor = 'tab:blue')

    cax = fig.add_axes([0.15 + 0.26*sind2, -0.02, 0.2, 0.015])
    cbar = plt.colorbar(cf, cax = cax, orientation = 'horizontal')
    cbar.ax.set_xticklabels(['%d%%' % (tt*100) for tt in cbar.get_ticks()])

fig.savefig(os.path.join(mg.path_out(), 'validate', 'metric_plot.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)
