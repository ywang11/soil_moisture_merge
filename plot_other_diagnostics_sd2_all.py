"""
2020/05/20

ywang254@utk.edu

Plot the global STD in soil moisture of the selected products.
1981-2010 for compatibility with GLEAM and ERA-Land.
"""
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
import cartopy.crs as ccrs
import utils_management as mg
from utils_management.constants import depth_cm, depth, target_lat, \
    target_lon, year_longest
import os
import xarray as xr
from misc.plot_utils import cmap_gen
import statsmodels.api as sm
import numpy as np
import itertools as it
from scipy.stats import pearsonr


year = year_longest
iam = 'lu_weighted'
cov = 'ShrunkCovariance'
met = 'CRU_v4.03'
me = 'year_month_anomaly_9grid'


prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all']
prod_names = ['Mean ORS', 'OLC ORS', 'EC ORS', 'EC CMIP5', 'EC CMIP6',
              'EC CMIP5+6', 'EC ALL']


levels = np.linspace(0, 0.06, 21)
cmap = 'Spectral'
levels_diff = np.linspace(-0.06, 0.06, 21)
cmap_diff = cmap_gen('autumn', 'winter_r')
mpl.rcParams['font.size'] = 5
mpl.rcParams['axes.titlesize'] = 5
mpl.rcParams['hatch.linewidth'] = 0.5


fig, axes = plt.subplots(nrows = len(prod_list), ncols = 5,
                         figsize = (6.5,5.5),
                         subplot_kw = {'projection': ccrs.Miller()})
fig.subplots_adjust(hspace = 0., wspace = 0.)
for pind, prod in enumerate(prod_list):
    ###########################################################################
    # (1)/(2) 1981-2010 trend, 0-10cm/10-30cm
    ###########################################################################
    sm_collect = {}
    for i in range(len(depth)):
        d = depth[i]
        dcm = depth_cm[i]
    
        ax = axes[pind, i]
        ax.coastlines(lw = 0.5, color = 'grey')
        ax.set_extent([-180, 180, -60, 80])
        ax.outline_patch.set_visible(False)
        ax.spines['top'].set_visible(True)
        ax.spines['bottom'].set_visible(True)
        ax.spines['left'].set_visible(True)
        ax.spines['right'].set_visible(True)
        ax.spines['top'].set_lw(0.1)
        ax.spines['bottom'].set_lw(0.1)
        ax.spines['left'].set_lw(0.1)
        ax.spines['right'].set_lw(0.1)
    
        fname = os.path.join(mg.path_out(), 'other_diagnostics', prod,
                             'sd2_' + d + '.nc')
        hr = xr.open_dataset(fname)
        sm = hr['sd'].values.copy()
        hr.close()
        sm_collect[dcm] = sm

        cf1 = ax.contourf(hr.lon.values, hr.lat.values, sm, cmap = cmap,
                          levels = levels, extend = 'both',
                          transform = ccrs.PlateCarree())
        if pind == 0:
            ax.set_title('Standard deviation\n' + dcm)
        if i == 0:
            ax.text(-0.1, 0.5, prod_names[pind], verticalalignment='center',
                    rotation = 90, transform = ax.transAxes)

    ###########################################################################
    # (3)/(4)/(5) Difference from GLEAM/ERA-Land with hatch for the difference
    #             being within 95% CI
    ###########################################################################
    for rind, ref, d, dcm in zip(range(3),
                                 ['GLEAMv3.3a', 'ERA-Land', 'ERA-Land'],
                                 ['0.00-0.10','0.00-0.10','0.10-0.30'],
                                 ['0-10cm','0-10cm','10-30cm']):
        ax = axes[pind, 2 + rind]
        ax.coastlines(lw = 0.5, color = 'grey')
        ax.set_extent([-180, 180, -60, 80])
        ax.outline_patch.set_visible(False)
        ax.spines['top'].set_visible(True)
        ax.spines['bottom'].set_visible(True)
        ax.spines['left'].set_visible(True)
        ax.spines['right'].set_visible(True)
        ax.spines['top'].set_lw(0.1)
        ax.spines['bottom'].set_lw(0.1)
        ax.spines['left'].set_lw(0.1)
        ax.spines['right'].set_lw(0.1)

        fname = os.path.join(mg.path_out(), 'other_diagnostics', 'lsm',
                             ref + '_sd2_' + d + '.nc')
        hr = xr.open_dataset(fname)
        obs = hr['sd'].values.copy()
        hr.close()

        cf3 = ax.contourf(hr.lon.values, hr.lat.values,
                          sm_collect[dcm] - obs,
                          levels = levels_diff, cmap = cmap_diff,
                          extend = 'both', transform = ccrs.PlateCarree())

        # ---- Pearson correlation between sm and obs
        x = obs.reshape(-1)
        y = sm_collect[dcm].reshape(-1)
        temp = ~(np.isnan(x) | np.isnan(y))
        r, pval = pearsonr(x[temp], y[temp])
        ax.text(0.4, 0.03, r'$\rho$=' + ('%.3f' % r),
                transform = ax.transAxes)

        if pind == 0:
            ax.set_title('Product - ' + ref + '\n' + dcm)

cax = fig.add_axes([0.13, 0.08, 0.29, 0.01])
plt.colorbar(cf1, cax = cax, boundaries = levels, orientation = 'horizontal',
             ticks = levels[::4])
cax = fig.add_axes([0.45, 0.08, 0.43, 0.01])
plt.colorbar(cf3, cax = cax, boundaries = levels_diff,
             orientation = 'horizontal', ticks = levels_diff[2::4])

fig.savefig(os.path.join(mg.path_out(), 'other_diagnostics_plot',
                         'sd_all.png'), dpi = 600, bbox_inches = 'tight')
plt.close()
