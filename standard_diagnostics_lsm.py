"""
Compute the following for every land surface model:

1. Global map (averaged over time): NetCDF
2. Global average time series.
3. Latitude bands average time series.
4. Continental average time series.
5. SREX region average time series.

for different time periods.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import lsm_all, depth, depth_cm
from misc.standard_diagnostics import standard_diagnostics
import os
from glob import glob
import itertools as it
import pandas as pd
import numpy as np
import multiprocessing as mp


land_mask = 'vanilla'

i = REPLACE
d = depth[i]
dcm = depth_cm[i]

def calc(model):
    flist = sorted(glob(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                     land_mask, model, 
                                     model + '_*_' + dcm + '.nc')))

    year_list = sorted([int(x.split('_')[-2]) for x in flist])

    data = xr.open_mfdataset(flist, decode_times=False)

    if model == 'ESA-CCI':
        time = pd.date_range(str(year_list[0])+'-01-01', 
                             '2018-12-01', freq = 'MS')
    else:
        time = pd.date_range(str(year_list[0])+'-01-01', 
                             str(year_list[-1])+'-12-31', freq = 'MS')

    ##print('start')
    standard_diagnostics(data.sm.values, time, data.lat, data.lon, 
                         os.path.join(mg.path_out(), 
                                      'standard_diagnostics_lsm'),
                         model + '_' + d)
    ##print('end')

    data.close()


##for model in lsm:
pool = mp.Pool(mp.cpu_count())
pool.map_async(calc, lsm_all)
pool.close()
pool.join()
