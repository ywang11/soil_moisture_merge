"""
2019/10/06

Plot the by-latitude (and annual + monthly) climatology of the reference
 products (ESA-CCI, GLEAMv3.3a, ERA-Land), Mean-All, and Median-Products.
"""
from misc.plot_utils import cmap_gen
import matplotlib as mpl
import matplotlib.pyplot as plt
import utils_management as mg
from utils_management.constants import depth
import pandas as pd
import numpy as np
import os


mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 6


year_str = '1981-2010'
lat_median = np.arange(-89.75, 90., 0.5)


# Collect the relevant datasets.
i = 1 # 10-30cm
d = depth[i]
collection = {}
for model_set, dummy, name in zip(['meanmedian_all', 'meanmedian_products',
                                   'lsm'],
                                  ['mean', 'median', 'ERA-Land'],
                                  ['Mean-All', 'Median-Products', 'ERA-Land']):
    collection[name] = pd.read_csv(os.path.join(mg.path_out(),
                                                 'other_diagnostics',
                                                 model_set, 
                                                 'bylat_mean_' + dummy + \
                                                 '_' + year_str + '_' + d + \
                                                 '.csv'), index_col = 0)


# Drop the NaN; should be uniform on all the data b.c. common land mask.
not_nan = ~np.isnan(collection['ERA-Land'].loc['annual',:].values)
for k in collection.keys():
    collection[k] = collection[k].loc[:, not_nan]
lat_median = lat_median[not_nan]


# Plot the relevant datasets.
fig, axes = plt.subplots(nrows = 2, ncols = 3, figsize = (6.5, 4.5),
                         sharex = True, sharey = True)
fig.subplots_adjust(hspace = 0.1)
axes[0,0].axis('off')
# (1) Climatology of the reference datasets.
for ind, model in enumerate(['ERA-Land']):
    ax = axes[ind + 1, 0]
    h = ax.imshow(collection[model].loc[::-1, :], aspect = 15, 
                  cmap = 'Spectral', vmin = 0., vmax = 0.6)

    ax.set_xlim([-0.5, len(lat_median)-0.5])
    ax.set_ylim([-0.5, 12.5])
    ax.set_yticks(range(0, 13))
    ax.set_yticklabels(collection[model].index[::-1])
    ax.set_ylabel(model + ' 10-30cm (m$^3$/m$^3$)')
    ax.axvline((0 - np.min(lat_median)) * len(lat_median) / \
               (np.max(lat_median) - np.min(lat_median)),
               color = 'grey', linewidth = 1, zorder = 3)

    cb = plt.colorbar(h, ax = ax, boundaries = np.arange(0., 0.61, 0.1),
                      extend = 'both', shrink = 0.6)
# (2) Climatology of the Mean-All and Median-Products.
for ind, model in enumerate(['Mean-All', 'Median-Products']):
    ax = axes[0, ind + 1]
    h = ax.imshow(collection[model].loc[::-1, :], aspect = 15,
                  cmap = 'Spectral', vmin = 0., vmax = 0.6)

    ax.set_xlim([-0.5, len(lat_median)-0.5])
    ax.set_ylim([-0.5, 12.5])
    ax.set_title(model + ' 10-30cm (m$^3$/m$^3$)')
    ax.axvline((0 - np.min(lat_median)) * len(lat_median) / \
               (np.max(lat_median) - np.min(lat_median)),
               color = 'grey', linewidth = 1, zorder = 3)

    cb = plt.colorbar(h, ax = ax, boundaries = np.arange(0., 0.61, 0.1),
                      extend = 'both', shrink = 0.6)
# (3) Difference between climatologies
for row, i in enumerate(['ERA-Land']):
    for col, j in enumerate(['Mean-All', 'Median-Products']):
        ax = axes[row + 1, col + 1]
        h = ax.imshow(collection[j].loc[::-1, :] - \
                      collection[i].loc[::-1, :], aspect = 15, 
                      cmap = cmap_gen('autumn', 'winter_r'), 
                      vmin = -0.2, vmax = 0.2)

        ax.set_xlim([-0.5, len(lat_median)-0.5])
        ax.set_ylim([-0.5, 12.5])
        ax.set_title('Diff (Column-Row)')
        cb = plt.colorbar(h, ax = ax, boundaries = np.arange(-0.2, 0.21, 0.05),
                          extend = 'both', shrink = 0.6)
        if row == 0:
            ax.set_xticks((np.arange(-40, 80, 20) - np.min(lat_median)) * \
                          len(lat_median) / (np.max(lat_median) - \
                                             np.min(lat_median)))
            ax.set_xticklabels([str(x) for x in np.arange(-40, 80, 20)])
            ax.set_xlabel('Latitude')
        ax.axvline((0 - np.min(lat_median)) * len(lat_median) / \
                   (np.max(lat_median) - np.min(lat_median)),
                   color = 'grey', linewidth = 1, zorder = 3)

fig.savefig(os.path.join(mg.path_out(), 'other_diagnostics_plot',
                         'bylat_climatology_10-30cm.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)
