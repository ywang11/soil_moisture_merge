load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "shapefile_utils.ncl"


begin
  africa_shp = "$SCRATCHDIR/Soil_Moisture/data/World_Continents/" + \
               "World_Continents.shp"
  ;;f = addfile(africa_shp, "r")
  ;;print(f->CONTINENT)
  ;;print(f->x)
  ;;delete(f)

  depth = (/"0-10cm", "10-30cm", "30-50cm", "50-100cm"/)
  model = (/"CLM4","ERA-Interim","GLDAS_VIC","JSBACH","ORCHIDEE", \
            "CERA20C","CLM4VIC","JULES","ORCHIDEE-CNP","CFSR", \
            "CLM5.0","GLDAS_CLM","GTEC","LPX-Bern","CLASS-CTEM-N", \
            "ERA20C","GLDAS_Noah2.0","ISAM","MERRA2"/)
  category = (/"MsTMIP BG1","Reanalysis","GLDAS","TRENDY","TRENDY", \
               "Reanalysis","MsTMIP BG1","TRENDY","TRENDY","Reanalysis", \
               "TRENDY","GLDAS","MsTMIP SG3","TRENDY","MsTMIP BG1", \
               "Reanalysis","GLDAS_Noah2.0","TRENDY","Reanalysis"/)

  do m = 0,dimsizes(model)-1
    do d = 0,dimsizes(depth)-1
      files = systemfunc("ls $SCRATCHDIR/Soil_Moisture/" + \
                         "intermediate/Interp_Merge/" + model(m) + \
                         "/" + model(m) + "*" + depth(d) + ".nc")
      if .not. ismissing(files(0)) then
        f = addfiles(files, "r")
        sm = f[:]->sm(:, {-40:40}, {-20:60})
        sm = tofloat(sm)

        sm_dims = dimsizes(sm)

        Opt = True
        Opt@shape_var = "CONTINENT"
        Opt@shape_names = "Africa"
        Opt@return_mask = True
        sm_mask = shapefile_mask_data(sm(0,:,:), africa_shp, Opt)

        ;;print(sm_mask)
        sm_mask = where(sm_mask .eq. 1, sm_mask, \
                        default_fillvalue("integer"))
        sm_mask@_FillValue = default_fillvalue("integer")
        sm_mask := tofloat(sm_mask)
        ;;print(sm_mask)

        do n = 0,sm_dims(0)-1
          sm(n,:,:) = (/ sm(n,:,:) * sm_mask /)
        end do

        f2 = addfile("$SCRATCHDIR/Soil_Moisture/intermediate/" + \
                     "subset_Africa/" + model(m) + "_" + depth(d) + \
                     ".nc", "c")
        filedimdef(f2, "time", -1, True)
        f2->sm = sm

        global_attributes = True
        global_attributes@day_created = systemfunc("date '+%A %W %Y %X'")
        global_attributes@source = category(m)
        global_attributes@method = "Interpolated bilinearly in space and weighted by the depths of the original soil layers."
        global_attributes@contact = "Yaoping Wang <ywang254@utk.edu> Jiafu Mao <maoj@ornl.gov>"

        fileattdef(f2, global_attributes)

        delete(n)
        delete(sm_mask)
        delete(sm_dims)
        delete(f)
        delete(sm)
        delete(Opt)
        delete(f2)
      end if
      delete(files)
    end do
  end do

  delete(depth)
  delete(model)
  delete(africa_shp)
end