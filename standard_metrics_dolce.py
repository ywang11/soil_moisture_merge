# -*- coding: utf-8 -*-
"""
Created on Wed May  1 21:04:14 2019

@author: ywang254

Calculate the evaluation metrics between between the DOLCE-weighted results, 
and the observed soil moisture. Note: although the weighting factors use the
de-biased soil moisture, the final soil moisture product still uses the 
absolute soil moisture. Therfore, a bias metric can still be calculated.
"""
import os
import utils_management as mg
import pandas as pd
from utils_management.constants import depth, depth_cm, lsm_list, \
    year_longest, year_shorter, year_shorter2, year_shortest
from misc.ismn_utils import get_ismn_aggr_method, get_weighted_monthly_data, \
    get_weighted_monthly_data_USonly
from misc.dolce_utils import get_cov_method
from misc.analyze_utils import calc_stats
import numpy as np
import itertools as it
import time


# MODIFY:
# Whether the metric is limited to the performance in CONUS.
USonly = True # True, False
model_set = 'all' # 'lsm', 'cmip5', 'cmip6', '2cmip', 'all'


# Select the time range to calculate the metrics between observation and land
# surface models.
if (model_set == 'lsm') | (model_set == 'all'):
    year_list = [year_longest, year_shorter, year_shorter2, year_shortest]
else:
    year_list = [year_longest]
time_range_list = []
for year in year_list:
    time_range_list.append(pd.date_range(start=str(year[0])+'-01-01',
                                         end=str(year[-1])+'-12-31',
                                         freq='MS'))


# List the weighting methods that are used to generate the weighted
# averaged ISMN data.
simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = [get_ismn_aggr_method(simple[i], dominance_lc[i], 
                                         dominance_threshold) for i in \
                    range(len(simple))]


# List the methods to calculate the covariance when calculating the weights.
cov_method = [get_cov_method(i) for i in range(5)]


start = time.time()
for i, year in it.product(range(len(depth)), year_list):
    d = depth[i]
    dcm = depth_cm[i]

    # Collector for the calculated metrics in time and space.
    metrics_collect = pd.DataFrame(data = np.nan,
        index = pd.MultiIndex.from_product([ismn_aggr_method, cov_method], 
                                           names = ['ISMN Aggr',
                                                    'Cov Method']),
        columns = pd.MultiIndex.from_product([['RMSE','Bias','uRMSE','Corr'],
                                              ['all','cal','val']],
                                             names = ['Metric', 'ISMN Set']))

    for iam, cov, ismn_set in it.product(ismn_aggr_method, cov_method, 
                                         ['all', 'cal', 'val']):
        #######################################################################
        # Get the observed data.
        #######################################################################
        if USonly:
            grid_latlon, weighted_monthly_data, _ = \
                get_weighted_monthly_data_USonly( \
                    os.path.join(mg.path_intrim_out(), 
                                 'ismn_aggr'), iam, d, ismn_set)
        else:
            grid_latlon, weighted_monthly_data, _ = \
                get_weighted_monthly_data(os.path.join(mg.path_intrim_out(), 
                                                       'ismn_aggr'), iam, d, 
                                          ismn_set)

        #######################################################################
        # Performance of the weighted product.
        #######################################################################
        data = pd.read_csv(os.path.join(mg.path_out(),
                                        'at_obs_dolce_' + model_set,
                                        'weighted_average_' + str(year[0]) + \
                                        '-' + str(year[-1]) + '_' + d + '_' + \
                                        iam + '_' + cov + '.csv'),
                           index_col = 0, parse_dates = True)
        # ---- obtain values at the observed data points
        weighted_sm_at_data = data.loc[weighted_monthly_data.index, 
                                       weighted_monthly_data.columns]

        metrics_collect.loc[(iam, cov), ('RMSE',ismn_set)], \
            metrics_collect.loc[(iam, cov), ('Bias',ismn_set)], \
            metrics_collect.loc[(iam, cov), ('uRMSE',ismn_set)], \
            metrics_collect.loc[(iam, cov), ('Corr',ismn_set)] = \
            calc_stats(weighted_sm_at_data.values.reshape(-1),
                       weighted_monthly_data.values.reshape(-1))

    if USonly:
        metrics_collect.to_csv(os.path.join(mg.path_out(), 'standard_metrics',
                                            'dolce_' + model_set + \
                                            '_' + d + '_' + str(year[0]) + \
                                            '-' + str(year[-1]) + \
                                            '_USonly.csv'))
    else:
        metrics_collect.to_csv(os.path.join(mg.path_out(), 'standard_metrics',
                                            'dolce_' + model_set + \
                                            '_' + d + '_' + str(year[0]) + \
                                            '-' + str(year[-1]) + '.csv'))
end = time.time()
print('The script finished in ' + ('%.4f' % ((end-start)/3600) ) + ' hours.')
