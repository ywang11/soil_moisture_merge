# -*- coding: utf-8 -*-
"""
20190911

ywang254@utk.edu

Source: 
  Hobeichi et al. - 2018 - Derived Optimal Linear Combination 
    Evapotranspiration (DOLCE) A Global Gridded Synthesis Estimate

Use the weights derived from "dolce_cmip5_weights.py" to calculate the final
  weighted average of the land surface models. Also calculate the
  spatiotemporal uncertainty.
"""
import os
import utils_management as mg
import pandas as pd
from utils_management.constants import depth, depth_cm, year_longest, \
    year_cmip5, year_cmip6
from misc.ismn_utils import get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability
import xarray as xr


# Choose the weighting method that was used to generate the weighted averaged
# ISMN data.
ismn_aggr_ind = REPLACE1
simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = get_ismn_aggr_method(simple[ismn_aggr_ind],
                                        dominance_lc[ismn_aggr_ind],
                                        dominance_threshold)


year = year_longest
time_range = pd.date_range(start=str(year[0])+'-01-01',
                           end=str(year[-1])+'-12-31', freq='MS')


# Pick the covariance function based on which the weights are calculated.
cov_estimer = REPLACE2 ## [0,1,2,3,4]
method = get_cov_method(cov_estimer)

remove_mean = True

land_mask = 'vanilla'

i = REPLACE3 # Need this much disaggregation because takes a long time to
             # generate results.
d = depth[i]


#######################################################################
# Load the weights.
#######################################################################
w_file = os.path.join(mg.path_intrim_out(), 'dolce_2cmip_weights', 
                      'weights_' + ismn_aggr_method + '_' + \
                      str(year[0]) + '-' + str(year[-1]) + '_' + method + \
                      '_' + d)
if remove_mean:
    w_file = w_file + '_remove_mean.csv'
else:
    w_file = w_file + '.csv'
weights = pd.read_csv(w_file, index_col = 0)


#######################################################################
# Calculate the weighted average and save to file.
#######################################################################
def calc_weighted_avg(weights, year, dcm):
    cmip5 = cmip5_availability(dcm, land_mask)
    cmip6 = list(set(mrsol_availability(dcm, land_mask, 'historical')) &\
                 set(mrsol_availability(dcm, land_mask, 'ssp585')))
    cmip6 = sorted([x for x in cmip6 if 'r1i1p1f1' in x])

    for count, l in enumerate(cmip5 + cmip6):
        w = weights.loc[l, 'Weight']

        if count < len(cmip5):
            f = [os.path.join(mg.path_intrim_out(), 'Interp_Merge', land_mask, 
                              'CMIP5', l, 'sm_historical_r1i1p1_' + str(x) + \
                              '_' + dcm + '.nc') for x in year_cmip5] + \
                [os.path.join(mg.path_intrim_out(), 'Interp_Merge', land_mask, 
                              'CMIP5', l, 'sm_rcp85_r1i1p1_' + str(x) + '_' + \
                              dcm + '.nc') \
                 for x in sorted(list(set(year_longest) - set(year_cmip5)))]
        else:
            f = [os.path.join(mg.path_intrim_out(), 'Interp_Merge', land_mask,
                              'CMIP6', l, 'mrsol_historical_' + str(x) + \
                              '_' + dcm + '.nc') for x in year_cmip6] + \
                [os.path.join(mg.path_intrim_out(), 'Interp_Merge', land_mask,
                              'CMIP6', l, 'mrsol_ssp585_' + str(x) + '_' + \
                              dcm + '.nc') \
                 for x in sorted(list(set(year_longest) - set(year_cmip6)))]

        data = xr.open_mfdataset(f, decode_times = False)
        #if np.isnan(data.sm.values[-1, 70, 218]):
        #    print(l)
        #data.close()
        if count == 0:
            weighted_sm = data.sm.copy(deep=True).load() * w
        else:
            weighted_sm = data.sm.copy(deep=True).load() * w + weighted_sm
        data.close()
    return weighted_sm
weighted_sm = calc_weighted_avg(weights, year, depth_cm[i])
wsmfile = os.path.join(mg.path_out(), 'dolce_2cmip_product', 
                       'weighted_average_' + str(year[0]) + '-' + \
                       str(year[-1]) + '_' + d + '_' + \
                       ismn_aggr_method + '_' + method + '.nc')
# ---- convert from DataArray to Dataset and save to netcdf
weighted_sm.to_dataset(name = 'sm').to_netcdf(wsmfile)
