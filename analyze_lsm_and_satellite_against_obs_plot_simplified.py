# -*- coding: utf-8 -*-
"""
Created on Wed May 22 23:44:39 2019

@author: ywang254

Plot the summarized evaluation metrics by climate zone, land use, and season.

Gather together the individual gridded products.
"""
import pandas as pd
import utils_management as mg
from utils_management.constants import depth, year_longest, \
    lu_names, lsm_list
import os
import time
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np


def cal_vals_plot(ax, input_data, merged, lsm):
    """
    Convenience function for the first section.
    """
    input_data.columns = input_data.columns.droplevel([1,2])
    input_data.index = input_data.index.droplevel(2)
    input_data = input_data.stack().groupby(level=[0,2]).mean().unstack()

    cmap = cm.get_cmap('Spectral')
    color = [cmap(x) for x in np.arange(.5/4, 1., 1./4)]

    h = []
    for m, mod in enumerate(lsm):
        hl, = ax.plot(range(input_data.shape[0]), input_data.loc[:, mod], '--', 
                      color = [.8, .8, .8])
    h.append(hl)
    for m, mod in enumerate(merged):
        hl, = ax.plot(range(input_data.shape[0]), input_data.loc[:, mod], '-', 
                      color = color[m], linewidth=2)
        h.append(hl)
    ax.set_xticks(range(input_data.shape[0]))
    ax.set_xticklabels(input_data.index, rotation=90)
    ax.set_xlim([-0.5, input_data.shape[0]+0.5])
    ax.legend(h, ['LSM'] + merged)
    return h


start = time.time()


year = year_longest
season = ['DJF', 'MAM', 'JJA', 'SON']
metric_name = ['RMSE', 'Bias', 'uRMSE', 'Corr']
value = ['Max', 'Mean', 'Median', 'Min']
lu_list = list(lu_names.values())


###############################################################################
# Simple plot that separate the climate zones or land use, and each by season,
# but not the interactions between climate zones and land use.
###############################################################################
for i, d in enumerate(depth):

    if i == 0:
        merged = ['weighted', 'average', 'median', 'satellite']
    else:
        merged = ['weighted', 'average', 'median']
    lsm = lsm_list[(str(year[0])+'-'+str(year[-1]), d)]

    metric = pd.read_csv(os.path.join(mg.path_out(), 
                         'analyze_lsm_and_satellite_against_obs', 
                         'metrics_' + d + '.csv'), 
                         index_col=[0,1,2], header=[0,1,2])
    metric.sort_index(axis=0, level=2, inplace=True)
    metric.sort_index(axis=1, level=1, inplace=True)

    ncols = 2 ## 3
    nrows = 2 ## int(np.ceil(len(clim_zone)/ncols))
    fig, axes = plt.subplots(nrows=nrows, ncols=ncols, figsize=(12,10), 
                             sharex=True, sharey=True)
    for me in metric_name:
        separation = ['climate_zone', 'land_use']
        for sep in separation:
            for j,ss in enumerate(season):
                ax = axes.flat[j]
                ax.cla()
                input_data = metric.loc[(slice(None), slice(None), ss), 
                                        (slice(None), 'Mean', me)].copy()
                if sep == 'land_use':
                    input_data = input_data.swaplevel(i=1,j=0,axis=0)
                h = cal_vals_plot(ax, input_data, merged, lsm)
                ax.set_title(ss)
            fig.savefig(os.path.join(mg.path_out(),
                                     'analyze_lsm_and_satellite_against_obs',
                                     'plot_' + sep + '_' + me + '_' + d + \
                                     '.png'), dpi=600.)
            plt.close(fig)


end = time.time()
print('The script finished in ' + ('%.4f' % ((end-start)/3600) ) + ' hours.')