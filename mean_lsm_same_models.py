# -*- coding: utf-8 -*-
"""
20190514
ywang254@utk.edu

Due to memory issue, calculate the mean across the lsm's for each year.
"""
import os
import xarray as xr
import numpy as np
import utils_management as mg
from utils_management.constants import depth, depth_cm, lsm_list, \
    year_longest, year_shorter, year_shorter2, year_shortest, target_lat, \
    target_lon

# The intersected set of models for year_longest over all the depths.
llist = ['CLM5.0', 'ISAM', 'LPX-Bern', 'ORCHIDEE', 'ORCHIDEE-CNP']

year = year_longest ## REPLACE ## year_longest, year_shorter, year_shorter2, year_shortest
land_mask = 'vanilla'

for i,d in enumerate(depth):
    collect_years = np.empty([12*len(year), len(target_lat), len(target_lon)])
    for y in year:

        # Pool the land surface model soil moisture into an array
        collection = np.empty([12, len(target_lat), 
                               len(target_lon), len(llist)])
        for j,lsm in enumerate(llist):
            data = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                                'Interp_Merge', land_mask,
                                                lsm, lsm + '_' + \
                                                str(y) + '_' + depth_cm[i] + \
                                                '.nc'), decode_times=False)
            collection[:, :, :, j] = data.sm.values.copy()
            dim0 = data.sm.time.copy()
            data.close()

        # Calculate the mean along the last dimension
        # ---- Use NaN mean because ESA-CCI has missing values.
        lsm_mean = np.nanmean(collection, axis=3)
        del collection # free up memory

        # 
        collect_years[(12*(y-year[0])):(12*(y-year[0]+1)), :, :] = \
            lsm_mean

    dim_time = np.arange( (year[0]-1900.)*12, (year[-1]-1900.+1)*12, 1. )
    dim1 = xr.DataArray(dim_time, coords={'time': dim_time}, 
                        dims=['time'], attrs=dim0.attrs)

    collect_year2 = xr.DataArray(collect_years, coords={'time': dim1, 
                                 'lat': target_lat, 'lon': target_lon},
                                 dims=['time','lat','lon'])

    del collect_years # free up memory

    collect_year2.to_dataset(name='sm').to_netcdf(os.path.join(mg.path_out(),
        'meanmedian_lsm', 'same_models_average_' + d + '_' + \
        str(year[0]) + '-' + str(year[-1]) + '.nc'))
