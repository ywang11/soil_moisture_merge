"""
2020/12/10
Examine the results of GSWP3.
Missing values are when precipitation are static (= 0 in extremely dry region).
"""
import xarray as xr
import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import itertools as it
import utils_management as mg
from utils_management.constants import depth, depth_cm
from misc.plot_utils import stipple, cmap_div


mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 6
mpl.rcParams['hatch.linewidth'] = 0.5
cmap = cmap_div()
lab = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
       'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
       'y', 'z', 'aa', 'bb', 'cc', 'dd', 'ee', 'ff', 'gg', 'hh',
       'ii', 'jj', 'kk', 'll', 'mm', 'nn', 'oo', 'pp', 'qq', 'rr',
       'ss', 'tt', 'uu', 'vv', 'ww', 'xx']


prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all']
prod_name_list = ['Mean ORS', 'OLC ORS', 'EC ORS', 'EC CMIP5', 'EC CMIP6',
                  'EC CMIP5+6', 'EC ALL']
season_list = ['Annual', 'DJF', 'MAM', 'JJA', 'SON']
for var, season in it.product(['pr','tas','rsds'], season_list):

    fig, axes = plt.subplots(len(prod_list), 4, 
                             figsize = (6.5, 6.5),
                             subplot_kw = {'projection': ccrs.Miller()})
    fig.subplots_adjust(wspace = 0.01, hspace = 0.01)
    count = 0
    for pind, dind in it.product(range(len(prod_list)), range(len(depth))):
        prod = prod_list[pind]

        d = depth[dind]
        dcm = depth_cm[dind]

        hr = xr.open_dataset(os.path.join(mg.path_out(), 'validate',
                                          'corr_gswp3_' + prod + '_' + \
                                          d + '_' + season + '.nc'))

        ax = axes.flat[count]
        ax.coastlines(lw = 0.5, color = 'grey')
        ax.set_extent([-180, 180, -60, 90], crs=ccrs.PlateCarree())

        cf = ax.contourf(hr['lon'], hr['lat'], hr[var], 
                         cmap = cmap, alpha = 0.5,
                         levels = np.linspace(-1, 1, 41),
                         transform = ccrs.PlateCarree(),
                         extend = 'neither')

        stipple(ax, hr['lat'].values, hr['lon'].values, 
                hr[var + '_p'], transform = ccrs.PlateCarree())
#        lon2, lat2 = np.meshgrid(hr['lon'], hr['lat'])
#        lon2 = np.where(hr[var + '_p'].values <= 0.05, lon2, np.nan)
#        lat2 = np.where(hr[var + '_p'].values <= 0.05, lat2, np.nan)
#        x = lon2.reshape(-1)
#        y = lat2.reshape(-1)
#        temp = ~(np.isnan(x) | np.isnan(y))
#        x = x[temp]
#        y = y[temp]
#        ax.scatter(x, y, c = 'k', transform = ccrs.PlateCarree(), 
#                   s = 0.01, marker = ',')

        hr.close()

        if pind == 0:
            ax.set_title(dcm.replace('cm', ' cm').replace('-', u'\u2212'))
        if dind == 0:
            ax.text(-0.1, 0.5, prod_name_list[pind], rotation = 90., 
                    verticalalignment = 'center',
                    transform = ax.transAxes)
        ax.text(0.05, 0.88, '(' + lab[count] + ')', transform = ax.transAxes)

        count += 1

    cax = fig.add_axes([0.1, 0.07, 0.8, 0.01])
    plt.colorbar(cf, cax = cax, orientation = 'horizontal')

    fig.savefig(os.path.join(mg.path_out(), 'validate', 
                             'corr_gswp3_' + var + '_' + season + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
