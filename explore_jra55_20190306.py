# -*- coding: utf-8 -*-
"""
Created on Wed Mar  6 11:54:20 2019

@author: ywang254
"""
import utils_management as mg
import os
import xarray as xr

path_jra55 = os.path.join(mg.path_data(), 'Reanalysis', 'JRA55')

for yy in range(1958, 2018):
    ds = xr.open_dataset(os.path.join(path_jra55, \
                                      'anl_land.225_soilw.reg_tl319.' + \
                                      str(yy) + '010100_' + str(yy) + \
                                      '123118.nc'))
    ds.SoilW_GDS4_ULN

    ds.close()