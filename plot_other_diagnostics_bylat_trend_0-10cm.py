"""
2019/10/06

Plot the by-latitude (and annual + monthly) trend of the reference
 products (ESA-CCI, GLEAMv3.3a, ERA-Land), Mean-All, and Median-Products.
"""
from misc.plot_utils import cmap_gen
import matplotlib as mpl
import matplotlib.pyplot as plt
import utils_management as mg
from utils_management.constants import depth, depth_cm, lsm_list
import pandas as pd
import numpy as np
import os
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability, one_layer_availability


mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 6


year_str = '1981-2010'
lat_median = np.arange(-89.75, 90., 0.5)


cmap = cmap_gen('autumn', 'winter_r')


# Skip ESA-CCI because missing values.

###############################################################################
# Collect the relevant trends.
###############################################################################
i = 0 # 0-10cm
d = depth[i]
collection = {}
for model_set, dummy, name in zip(['meanmedian_all', 'meanmedian_products',
                                   'lsm', 'lsm'],
                                  ['mean', 'median', 
                                   'GLEAMv3.3a', 'ERA-Land'],
                                  ['Mean-All', 'Median-Products',
                                   'GLEAMv3.3a', 'ERA-Land']):
    collection[name] = pd.read_csv(os.path.join(mg.path_out(),
                                                'other_diagnostics',
                                                model_set, 
                                                'bylat_trend_' + dummy + \
                                                '_' + year_str + '_' + d + \
                                                '.csv'), index_col = 0).T

###############################################################################
# Collect the relevant significance values. 
# 1. For the GLEAMv3.3a and ERA-Land products, p value <= 0.05
# 2. For the Mean-All and Median-Products products, >66.7% of the source agree.
###############################################################################
collection_p = {}
for l in ['GLEAMv3.3a', 'ERA-Land']:
    collection_p[l] = pd.read_csv(os.path.join(mg.path_out(),
                                               'other_diagnostics', 'lsm',
                                               'bylat_p_' + l + '_' + \
                                               year_str + '_' + d + '.csv'),
                                  index_col = 0).T

# Calculate the pct agreement between the raw source datasets.
land_mask = 'vanilla'
dcm = depth_cm[i]
consistent_pct = pd.DataFrame(data = 0, 
                              index = collection['Mean-All'].index, 
                              columns = collection['Mean-All'].columns)
lsm = lsm_list[(year_str, d)]
cmip5 = cmip5_availability(dcm, land_mask)
cmip6_list_1 = mrsol_availability(dcm, land_mask, 'historical')
cmip6_list_2 = mrsol_availability(dcm, land_mask, 'ssp585')
cmip6_list_3 = one_layer_availability('pr', land_mask, 'historical')
cmip6_list_4 = one_layer_availability('pr', land_mask, 'ssp585')
cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) & \
                   set(cmip6_list_3) & set(cmip6_list_4) )
cmip6_list = [x for x in cmip6_list if 'r1i1p1f1' in x]

for group, model in zip(['lsm', 'cmip5', 'cmip6'],
                        [lsm, cmip5, cmip6_list]):
    for l in model:
        if l == 'ESA-CCI':
            continue # Skip because no global trend available.
        temp = pd.read_csv(os.path.join(mg.path_out(), 'other_diagnostics',
                                        group, 'bylat_trend_' + l + '_' + \
                                        year_str + '_' + d + '.csv'),
                           index_col = 0)
        consistent_pct += ((collection['Mean-All'].values > 0) & \
                           (temp.values.T > 0)) | \
            ((collection['Mean-All'].values < 0) & (temp.values.T < 0))
consistent_pct /= (len(lsm) + len(cmip5) + len(cmip6_list))
collection_p['Mean-All'] = consistent_pct

# Calculate the pct agreement between the producted products.
consistent_pct = pd.DataFrame(data = 0,
                              index = collection['Median-Products'].index,
                              columns = collection['Median-Products'].columns)
for group in ['dolce_lsm', 'dolce_cmip5', 'dolce_cmip6',
              'em_lsm', 'em_cmip5', 'em_cmip6']:
    temp = pd.read_csv(os.path.join(mg.path_out(), 'other_diagnostics',
                                    group, 'bylat_trend_' + \
                                    year_str + '_' + d + '.csv'),
                       index_col = 0)
    consistent_pct += ((collection['Median-Products'].values > 0) & \
                       (temp.values.T > 0)) | \
        ((collection['Median-Products'].values < 0) & (temp.values.T < 0))
consistent_pct /= 6
collection_p['Median-Products'] = consistent_pct


###############################################################################
# Drop the NaN; should be uniform on all the data b.c. common land mask.
###############################################################################
not_nan = ~np.isnan(collection['ERA-Land'].loc['annual',:].values)
for k in collection.keys():
    collection[k] = collection[k].loc[:, not_nan]
for k in collection_p.keys():
    collection_p[k] = collection_p[k].loc[:, not_nan]
lat_median = lat_median[not_nan]


###############################################################################
# Plot the relevant datasets.
###############################################################################
fig, axes = plt.subplots(nrows = 3, ncols = 3, figsize = (6.5, 5),
                         sharex = True, sharey = True)
fig.subplots_adjust(hspace = 0.1)
axes[0,0].axis('off')
# (1) Trend of the reference datasets.
for ind, model in enumerate(['GLEAMv3.3a', 'ERA-Land']):
    ax = axes[ind + 1, 0]
    h = ax.imshow(collection[model].loc[::-1, :], aspect = 15, 
                  cmap = cmap, vmin = -0.001, vmax = 0.001)

    # ---- add hatch when p <= 0.05
    for m_ind, m in enumerate(collection[model].index):
        for c_ind, c in enumerate(collection[model].columns):
            if collection_p[model].loc[m,c] <= 0.05:
                bbox = [c_ind-0.5, collection_p[model].shape[0]-m_ind-1.5]
                ax.plot([bbox[0], bbox[0]+1], [bbox[1]+0.4, bbox[1]+0.6],
                        '-', color = 'k', linewidth = 0.1)
                ax.plot([bbox[0], bbox[0]+1], [bbox[1]+0.6, bbox[1]+0.4],
                        '-', color = 'k', linewidth = 0.1)

    ax.set_xlim([-0.5, len(lat_median)-0.5])
    ax.set_ylim([-0.5, 12.5])
    ax.set_yticks(range(0, 13))
    ax.set_yticklabels(collection[model].index[::-1])
    ax.set_ylabel(model + ' 0-10cm (m$^3$/m$^3$)')
    ax.axvline((0 - np.min(lat_median)) * len(lat_median) / \
               (np.max(lat_median) - np.min(lat_median)),
               color = 'grey', linewidth = 1, zorder = 3)

    cb = plt.colorbar(h, ax = ax, boundaries = np.arange(-0.0012, 0.0013,
                                                         0.0004),
                      extend = 'both', shrink = 0.6)
# (2) Trend of the Mean-All and Median-Products.
for ind, model in enumerate(['Mean-All', 'Median-Products']):
    ax = axes[0, ind + 1]
    h = ax.imshow(collection[model].loc[::-1, :], aspect = 15,
                  cmap = cmap, vmin = -0.001, vmax = 0.001)

    # ---- add hatch when >66.7% source datasets agree
    for m_ind, m in enumerate(collection[model].index):
        for c_ind, c in enumerate(collection[model].columns):
            if collection_p[model].loc[m,c] > 0.666:
                bbox = [c_ind-0.5, collection_p[model].shape[0]-m_ind-1.5]
                ax.plot([bbox[0], bbox[0]+1], [bbox[1]+0.4, bbox[1]+0.6],
                        '-', color = 'k', linewidth = 0.1)
                ax.plot([bbox[0], bbox[0]+1], [bbox[1]+0.6, bbox[1]+0.4],
                        '-', color = 'k', linewidth = 0.1)

    ax.set_xlim([-0.5, len(lat_median)-0.5])
    ax.set_ylim([-0.5, 12.5])
    ax.set_title(model + ' 0-10cm (m$^3$/m$^3$)')
    ax.axvline((0 - np.min(lat_median)) * len(lat_median) / \
               (np.max(lat_median) - np.min(lat_median)),
               color = 'grey', linewidth = 1, zorder = 3)
    cb = plt.colorbar(h, ax = ax, boundaries = np.arange(-0.0012, 0.0013,
                                                         0.0004),
                      extend = 'both', shrink = 0.6)
# (3) Difference between climatologies
for row, i in enumerate(['GLEAMv3.3a', 'ERA-Land']):
    for col, j in enumerate(['Mean-All', 'Median-Products']):
        ax = axes[row + 1, col + 1]
        h = ax.imshow(collection[j].loc[::-1, :] - \
                      collection[i].loc[::-1, :], aspect = 15, 
                      cmap = cmap, vmin = -0.001, vmax = 0.001)

        ax.set_xlim([-0.5, len(lat_median)-0.5])
        ax.set_ylim([-0.5, 12.5])
        ax.set_title('Diff (Column-Row)')
        cb = plt.colorbar(h, ax = ax, boundaries = np.arange(-0.0012, 0.0013,
                                                             0.0004),
                          extend = 'both', shrink = 0.6)
        if row == 1:
            ax.set_xticks((np.arange(-40, 80, 20) - np.min(lat_median)) * \
                          len(lat_median) / (np.max(lat_median) - \
                                             np.min(lat_median)))
            ax.set_xticklabels([str(x) for x in np.arange(-40, 80, 20)])
            ax.set_xlabel('Latitude')
        ax.axvline((0 - np.min(lat_median)) * len(lat_median) / \
                   (np.max(lat_median) - np.min(lat_median)),
                   color = 'grey', linewidth = 1, zorder = 3)
fig.savefig(os.path.join(mg.path_out(), 'other_diagnostics_plot',
                         'bylat_trend_0-10cm.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)
