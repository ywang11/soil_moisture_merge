"""
2019/08/05

ywang254@utk.edu

The median, min, max of the mean and trend of global soil moisture of CMIP6
models. 
"""
import numpy as np
import utils_management as mg
from utils_management.constants import cmip6_list, target_lon, target_lat, \
    year_cmip6
import xarray as xr
import os
from misc.standard_diagnostics import get_summary_cmip6
import numpy.ma as ma


act = 'DAMIP' # ['CMIP', 'DAMIP', 'DAMIP']
expr = 'hist-nat' # ['historical', 'hist-GHG', 'hist-nat']
var = 'mrsos'
tid = 'Lmon'

model_version = cmip6_list(act, expr, var, tid)


mean_mean, mean_max, mean_min, _, _ = get_summary_cmip6(mg.path_out(),
                                                        model_version,
                                                        var + '_' + expr, 
                                                        target_lat, target_lon,
                                                        year_cmip6, 'mean')

xr.Dataset({'mean': xr.DataArray(mean_mean, dims = ['lat', 'lon'],
                                 coords = {'lat': target_lat,
                                           'lon': target_lon}),
            'min': xr.DataArray(mean_min, dims = ['lat', 'lon'],
                                coords = {'lat': target_lat,
                                          'lon': target_lon}),
            'max': xr.DataArray(mean_max, dims = ['lat', 'lon'],
                                coords = {'lat': target_lat,
                                          'lon': target_lon})} \
           ).to_netcdf(os.path.join(mg.path_out(), 
                                    'standard_diagnostics_cmip6',
                                    var + '_' + expr + '_' + \
                                    str(year_cmip6[0]) + '-' + \
                                    str(year_cmip6[-1]) + \
                                    '_summary_mean.nc'))


trend_mean, trend_max, trend_min, \
    trend_collect, trend_pvalue = get_summary_cmip6(mg.path_out(), 
                                                    model_version, 
                                                    var + '_' + expr,
                                                    target_lat, 
                                                    target_lon, year_cmip6,
                                                    'trend')

# Percent significant negative/positive trends
trend_collect_significant = ma.masked_array(trend_collect, 
                                            trend_pvalue > 0.05)
pct_sig_neg = np.sum(trend_collect_significant<0, axis=0) / \
              trend_collect.shape[0]
pct_sig_pos = np.sum(trend_collect_significant>0, axis=0) / \
              trend_collect.shape[0]


xr.Dataset({'mean': xr.DataArray(trend_mean, dims = ['lat', 'lon'],
                                 coords = {'lat': target_lat,
                                           'lon': target_lon}),
            'min': xr.DataArray(trend_min, dims = ['lat', 'lon'],
                                coords = {'lat': target_lat,
                                          'lon': target_lon}),
            'max': xr.DataArray(trend_max, dims = ['lat', 'lon'],
                                coords = {'lat': target_lat,
                                          'lon': target_lon}),
            'pct_sig_neg': xr.DataArray(pct_sig_neg, dims = ['lat', 'lon'],
                                        coords = {'lat': target_lat,
                                                  'lon': target_lon}),
            'pct_sig_pos': xr.DataArray(pct_sig_pos, dims = ['lat', 'lon'],
                                        coords = {'lat': target_lat,
                                                  'lon': target_lon})} \
).to_netcdf(os.path.join(mg.path_out(), 'standard_diagnostics_cmip6',
                         var + '_' + expr + '_' + \
                         str(year_cmip6[0]) + '-' + str(year_cmip6[-1]) + \
                         '_summary_trend.nc'))
