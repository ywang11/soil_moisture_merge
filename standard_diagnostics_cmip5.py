"""
Compute the following for every CMIP5 model:

1. Global maps (averaged over time, and by season over time).
2. Global trends (all months, annual mean, seasonal mean).
3. Global average time series.
4. Latitude bands average time series.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import year_longest, year_cmip5, depth_cm
from misc.cmip5_availability import cmip5_availability
from misc.standard_diagnostics import standard_diagnostics
import os
import pandas as pd
import multiprocessing as mp


i = REPLACE # [0, 1, 2, 3]
dcm = depth_cm[i]

land_mask = 'vanilla'

cmip5 = cmip5_availability(dcm, land_mask)


def calc(model):
    file1 = [os.path.join(mg.path_intrim_out(), 'Interp_Merge', land_mask, 
                          'CMIP5', model, 'sm_historical_r1i1p1_' + \
                          str(y) + '_' + dcm + '.nc') for y in year_cmip5]
    file2 = [os.path.join(mg.path_intrim_out(), 'Interp_Merge', land_mask, 
                          'CMIP5', model, 'sm_rcp85_r1i1p1_' + str(y) + '_' + \
                          dcm + '.nc') for y in \
             list(set(year_longest) - set(year_cmip5))]

    data = xr.open_mfdataset(file1 + file2, decode_times=False)

    time = pd.date_range(str(year_longest[0])+'-01-01',
                         str(year_longest[-1])+'-12-31', freq = 'MS')

    standard_diagnostics(data.sm.values, time, data.lat, data.lon, 
                         os.path.join(mg.path_out(), 
                                      'standard_diagnostics_cmip5'),
                         model + '_' + dcm)

    data.close()

#for model in cmip5:
pool = mp.Pool(mp.cpu_count())
pool.map_async(calc, cmip5)
pool.close()
pool.join()
