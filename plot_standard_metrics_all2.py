"""
2020/05/19

ywang254@utk.edu

Plot the RMSE & Corr of the different weighted results,
 the range of LSM, CMIP5, CMIP6 models, and highlight the ESA-CCI, 
 GLEAMv3.3a, and the ERA-Land results in the LSM models.

Use the best emergent constraint and DOLCE weighting setups.

Use a subset of the products that are used in the D&A.
"""
import os
import sys
import pandas as pd
import numpy as np
from utils_management.constants import depth, depth_cm, year_longest, \
    year_shorter, year_shorter2, year_shortest, lsm_list, year_cmip5, \
    year_cmip6
import utils_management as mg
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import cm
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability, one_layer_availability
from misc.ismn_utils import get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
import itertools as it


# MODIFY
USonly = False # if True, plot the evaluation results for CONUS
if USonly:
    suffix = '_USonly'
else:
    suffix = ''

#
pr = 'val'
year = year_longest # Only plot the concatenated LSM and ALL products.
year_str = str(year[0]) + '-' + str(year[-1])
iam = 'lu_weighted'
em = 'year_month_anomaly_9grid'
cov = 'ShrunkCovariance'
land_mask = 'vanilla'
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'

#
#clist = ['#e41a1c', '#e41a1c', '#e41a1c', '#377eb8', '#4daf4a', '#984ea3',
#         '#ff7f00']
cmap = cm.get_cmap('jet')
clist = [cmap((i+0.7)/7) for i in range(7)]
mlist = ['x', 'o', 's', 's', 's', 's', 's']
prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all']
prod_name = ['Mean ORS', 'OLC ORS', 'EC ORS', 'EC CMIP5', 'EC CMIP6',
             'EC CMIP5+6', 'EC ALL']

#
metric_list = ['RMSE', 'Corr']
unit_list = ['($m^3/m^3$)', '(-)']

#
mpl.rcParams.update({'font.size': 6})


#
fig, axes = plt.subplots(nrows = 2, ncols = 4, figsize = (6.5, 4.),
                         sharex = True, sharey = False)
fig.subplots_adjust(hspace = 0., wspace = 0.)
for mind, dind in it.product(range(len(metric_list)), range(len(depth))):
    metric = metric_list[mind]
    d = depth[dind]
    dcm = depth_cm[dind]

    ax = axes[mind, dind]

    #
    metric_raw = {}
    metric_raw['lsm'] = pd.read_csv(os.path.join(mg.path_out(), 
                                                 'standard_metrics', 
                                                 'lsm_' + d + '_' + \
                                                 year_str + \
                                                 suffix + '.csv'),
                                    index_col = [0,1], 
                                    header = [0,1]).loc[iam, (metric,pr)]
    metric_raw['cmip5'] = pd.read_csv(os.path.join(mg.path_out(), 
                                                   'standard_metrics',
                                                   'cmip5_' + d + \
                                                   suffix + '.csv'),
                                      index_col = [0,1], 
                                      header = [0,1]).loc[iam, (metric,pr)]
    metric_raw['cmip6'] = pd.read_csv(os.path.join(mg.path_out(),
                                                   'standard_metrics',
                                                   'cmip6_' + d + '.csv'),
                                      index_col = [0,1],
                                      header = [0,1]).loc[iam, (metric,pr)]

    #
    metric_prod = {}
    metric_prod[prod_list[0]] = pd.read_csv(os.path.join(mg.path_out(),
        'standard_metrics', 'meanmedian_lsm_' + d + '_' + year_str + \
        suffix + '.csv'), index_col = [0,1], header = [0,1]).loc[(iam,'mean'),
                                                                 (metric,pr)]
    metric_prod[prod_list[1]] = pd.read_csv(os.path.join(mg.path_out(),
        'standard_metrics', 'concat_dolce_lsm_' + d + '_' + year_str + \
        suffix + '.csv'), index_col = [0,1], header = [0,1]).loc[(iam,cov),
                                                                 (metric,pr)]
    for prod in ['em_lsm', 'em_all']:
        metric_prod[prod] = pd.read_csv(os.path.join(mg.path_out(), 
                                                     'standard_metrics',
                                                     'concat_' + prod + \
                                                     '_' + d + \
                                                     '_' + year_str + \
                                                     suffix + '.csv'),
                                        index_col = 0, 
                                        header = [0,1]).loc[iam, (metric,pr)]
    for prod in ['em_cmip5', 'em_cmip6', 'em_2cmip']:
        metric_prod[prod] = pd.read_csv(os.path.join(mg.path_out(),
                                                     'standard_metrics',
                                                     prod + '_' + d + '_' + \
                                                     year_str + \
                                                     suffix + '.csv'),
                                         index_col = [0,1],
                                         header = [0,1]).loc[(iam,em),
                                                             (metric,pr)]

    #######################################################################
    # Plot the individual source datasets.
    #######################################################################
    data_lsm_short = pd.read_csv(os.path.join(mg.path_out(),
                                              'standard_metrics',
                                              'lsm_' + d + '_' + \
                                              str(year_shortest[0]) + '-' \
                                              + str(year_shortest[-1]) + \
                                              suffix + '.csv'),
                                 index_col = [0,1],
                                 header = [0,1]).loc[iam, (metric,pr)]
    if (d == '0.00-0.10'):
        data_esa_cci = data_lsm_short.loc['ESA-CCI']
        data_gleam = data_lsm_short.loc['GLEAMv3.3a']
    else:
        data_esa_cci = np.nan
        data_gleam = np.nan
    if (d == '0.00-0.10') | (d == '0.10-0.30'):
        data_era_land = data_lsm_short.loc['ERA-Land']
    else:
        data_era_land = np.nan

    def plotter(ax, dl, pos, col):
        # ---- draw the boxplot. Outliers beyond range are not shown.
        hb = ax.boxplot(dl.reshape(-1,1), positions = [pos], 
                        widths = 0.4, whis = [0, 100], showfliers = False)
        #ax.text(pos-0.25, np.min(dl) * 0.6, 'n=' + str(dl.shape[0]),
        #        rotation = 90)
        hb['boxes'][0].set_color(col)
        return hb

    plotter(ax, metric_raw['lsm'].values, 1., 'k')
    if (d == '0.00-0.10'):
        h1, = ax.plot(1., data_esa_cci, 's', color = '#dd1c77', markersize = 4)
        h2, = ax.plot(1., data_gleam, '^', color = '#dd1c77', markersize = 4)
    if (d == '0.00-0.10') | (d == '0.10-0.30'):
        h3, = ax.plot(1., data_era_land, 'v', color = '#dd1c77',
                      markersize = 4)
    plotter(ax, metric_raw['cmip5'].values, 2, 'k')
    plotter(ax, metric_raw['cmip6'].values, 3, 'k')

    #######################################################################
    # Plot the products.
    #######################################################################
    np.random.seed(999)
    h4 = []
    for ind, prod in enumerate(prod_list):
        tmp, = ax.plot(4 + np.random.rand() * 0.4,
                       metric_prod[prod], mlist[ind],
                       color = clist[ind], markersize = 4,
                       markerfacecolor = "None")
        h4.append(tmp)

    ax.set_xlim([0.5, 4.5])
    ax.set_xticks([1, 2, 3, 4])
    ax.set_xticklabels(['ORS', 'CMIP5', 'CMIP6', 'Merged\nProducts'],
                       rotation = 40)

    if mind == 0:
        ax.set_title(dcm)
        ax.set_ylim([0.07, 0.2])
        ax.set_yticks(np.arange(0.08, 0.2, 0.02))
    else:
        ax.set_ylim([0.1, 0.7])
        ax.set_yticks(np.arange(0.2, 0.7, 0.1))
    if dind == 0:
        ax.set_ylabel(metric + ' ' + unit_list[mind])
    else:
        ax.tick_params('y', length = 0)
        ax.set_yticklabels([])
    ax.grid(True, axis = 'y')

ax.legend([h1, h2, h3] + h4, ['ESA-CCI', 'GLEAM v3.3a', 'ERA-Land'] + \
          prod_name, loc = 'center left', ncol = 5,
          bbox_to_anchor = [-2.5, -0.4])

fig.savefig(os.path.join(mg.path_out(), 'standard_metrics', 
                         'plot_' + pr + '_all_' + iam + \
                         suffix + '.png'), dpi = 600.,
            bbox_inches = 'tight')
plt.close(fig)
