"""
20190923
ywang254@utk.edu

Use CDF-matching merging on the uncertainty of the DOLCE products.
"""
import xarray as xr
import os
import pandas as pd
import numpy as np
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth_cm, target_lat, target_lon
import utils_management as mg
from misc.ismn_utils import get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
from misc.concat_utils import piecewise_cdf_create, piecewise_cdf_apply
import itertools as it
import multiprocessing as mp
from time import time


def scaler(vec_ref, tvec_ref, vec, tvec, order):
    ref = vec_ref[(tvec_ref >= time_range[0]) & \
                  (tvec_ref <= time_range[-1])]
    x = vec[(tvec >= time_range[0]) & \
            (tvec <= time_range[-1])]
    slope, threshold, intercept = piecewise_cdf_create(x, ref)
    vec_rescaled = piecewise_cdf_apply(slope, threshold, intercept, vec)
    return (vec_rescaled, order)


def restore(result):
    anomalies_rescaled = np.full([len(tvec),
                                  len(target_lat) * len(target_lon)],
                                 np.nan)
    for rr in result:
        rr2 = rr.get()
        anomalies_rescaled[:, retain[rr2[1]]] = rr2[0].reshape(-1)

    anomalies_rescaled = xr.DataArray(anomalies_rescaled.reshape(len(tvec),
        len(target_lat), len(target_lon)),
                                      dims = ['time','lat','lon'],
                                      coords = {'time': tvec,
                                                'lat': target_lat,
                                                'lon': target_lon})
    return anomalies_rescaled


#
dcm = depth_cm[REPLACE1]
prod = 'REPLACE2' # lsm, all


time_range = pd.date_range('1981-01-01', '2010-12-31', freq = 'MS')


# reference period
hr = xr.open_mfdataset([os.path.join(mg.path_out(),
                                     'em_' + prod + '_corr',
    'CRU_v4.03_year_month_anomaly_9grid_' + dcm + '_1981-2016',
    'predicted_std_' + str(yy) + '.nc') for yy in year_shorter2],
                       decode_times = False, concat_dim = 'time')
tvec_ref = pd.date_range(str(year_shorter2[0]) + '-01-01',
                         str(year_shorter2[-1]) + '-12-31',
                         freq = 'MS')
data_ref = hr['predicted_std'].values.copy()
hr.close()


#
retain = np.where((~np.isnan(data_ref[0, :, :])).reshape(-1))[0]
data_ref = np.array_split(data_ref.reshape(-1,
                                           data_ref.shape[1] * \
                                           data_ref.shape[2])[:, retain],
                          len(retain), axis = 1)


#
for year in [year_shorter, year_shortest]:
    hr = xr.open_mfdataset([os.path.join(mg.path_out(),
                                         'em_' + prod + '_corr',
        'CRU_v4.03_year_month_anomaly_9grid_' + dcm + '_' + \
        str(year[0]) + '-' + str(year[-1]),
        'predicted_std_' + str(yy) + '.nc') for yy in year],
                           decode_times = False, concat_dim = 'time')
    tvec = pd.date_range(str(year[0]) + '-01-01',
                         str(year[-1]) + '-12-31', freq = 'MS')
    anomalies = hr['predicted_std'].values.copy()
    anomalies = np.array_split(anomalies.reshape(-1,
                                                 anomalies.shape[1] * \
                                                 anomalies.shape[2])[:,retain],
                               len(retain), axis = 1)
    hr.close()

    p = mp.Pool(4)
    result = [p.apply_async(scaler,
                            args = (data_ref[ii], tvec_ref,
                                    anomalies[ii], tvec, ii))
              for ii in range(len(retain))]
    p.close()
    p.join()

    anomalies_rescaled = restore(result)

    anomalies_rescaled.to_dataset(name = 'predicted_std' \
    ).to_netcdf(os.path.join(mg.path_out(), 'concat_em_' + prod,
                             'uncertainty_scaled_CRU_v4.03_year_month_' \
                             + 'anomaly_9grid_' + dcm + '_' + \
                             str(year[0]) + '-' + \
                             str(year[-1]) + '.nc'))
