"""
2019/07/14

ywang254@utk.edu

Calculate the common land mask of the interpolated CMIP6 models after their
interpolation to 0.5oC. Also, intersect the common land mask with the 
land mask of e1m (land v.s. ocean) or SYNMAP (land, excluding snow and ice).
"""
import sys
import os
import xarray as xr
import numpy as np
import pandas as pd
from glob import glob
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import utils_management as mg
from utils_management.constants import year_cmip6, cmip6_list


target_lat = np.arange(-89.75, 89.76, 0.5)
target_lon = np.arange(-179.75, 179.76, 0.5)


expr = 'historical'
var = 'mrsol'

model_version = list(set(cmip6_list('historical', 'mrsol')) & \
                     set(cmip6_list('ssp585', 'mrsol')))
model_version = [x for x in model_version if 'r1i1p1f1' in x]


# Build the list of masks of interpolated, but un-masked soil moisture file.
mask_all = np.zeros([len(model_version), 360, 720], dtype = int)
for m_v_ind, m_v in enumerate(model_version):
    mrsos_path = os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                              'None', 'CMIP6', m_v, 
                              var + '_' + expr + '_' + \
                              str(year_cmip6[0]) + '*.nc')

    f = sorted(glob(mrsos_path))[0]
    data = xr.open_dataset(f, decode_times = False)
    mask_all[m_v_ind, :, :] = ~np.isnan(data.sm[0,:,:].values)
    data.close()


# Find the minimum mask, i.e. where all the mask == 1
mask_check = np.all(mask_all == 1, axis=0)


# Save the minimum mask to file.
mask_check = xr.DataArray(mask_check, dims = ['lat', 'lon'], 
                          coords = {'lat': target_lat, 'lon': target_lon})
mask_check.to_dataset(name = 'mask' \
).to_netcdf(os.path.join(mg.path_intrim_out(), 'land_mask_minimum',
                         'cmip6.nc'))


# Accompanying graph.
fig, ax = plt.subplots(figsize = (12, 12), 
                       subplot_kw = {'projection': ccrs.PlateCarree()})
ax.coastlines(color = 'red')
ax.gridlines(draw_labels = True)
mask_check.plot(cmap = 'Greys')
fig.savefig(os.path.join(mg.path_intrim_out(), 'land_mask_minimum', 
                         'cmip6.png'), bbox_inches = 'tight', dpi = 600.)
plt.close(fig)
