"""
20191120
ywang254@utk.edu

Compute the climatology of soil moisture, precipitation, and temperature 
 during the:
 1981-2010 overlapping period of all the LSMs, and
 1950-2016 overlapping period of all the CMIP5 & CMIP6 models.
"""
import utils_management as mg
from utils_management.constants import met_to_lsm, year_longest, \
    year_shortest, lsm_list, depth, depth_cm, target_lat, target_lon, \
    year_cmip5, year_cmip6
from misc.spatial_utils import extract_grid
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import os
import xarray as xr
import pandas as pd
import numpy as np
import itertools as it
import time
import multiprocessing as mp


land_mask = 'vanilla'


###############################################################################
# Loop through the models and calculate the climatology.
###############################################################################
##for m in met_list:
def clim(option):
    m, var = option

    if model == 'lsm':
        if var == 'tas':
            temp = mg.path_to_tas(land_mask)
        else:
            temp = mg.path_to_pr(land_mask)
        flist = [os.path.join(temp[m] + str(y) + '.nc') for y in year_shortest]
    elif model == 'cmip5':
        fhist = [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                              land_mask, 'CMIP5', m, var + \
                              '_historical_r1i1p1_' + str(y) + '.nc') \
                 for y in range(year_shortest[0], year_cmip5[-1]+1)]
        f85 = [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                            land_mask, 'CMIP5', m, var + '_rcp85_r1i1p1_' + \
                            str(y) + '.nc') for y in \
               range(year_cmip5[-1]+1, year_shortest[-1]+1)]
        flist = fhist + f85
    elif model == 'cmip6':
        flist = [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                              land_mask, 'CMIP6', m, var + '_historical_' + \
                              str(y) + '.nc') for y in year_shortest]

    data = xr.open_mfdataset(flist, decode_times = False)
    met = data[var].values.copy()
    data.close()

    climatology = np.full([12, len(target_lat), len(target_lon)], np.nan)
    for month in range(12):
        climatology[month, :, :] = np.mean(met[month::12, :, :], axis = 0)
    climatology =  xr.DataArray(climatology, coords = {'month': range(1,13),
                                                       'lat': target_lat,
                                                       'lon': target_lon},
                                dims = ['month', 'lat', 'lon'])
    climatology.to_dataset(name = var).to_netcdf(os.path.join( \
        mg.path_intrim_out(), 'em_tas_corr_climatology', var + '_' + \
        model + '_' + m + '.nc'))


# Land surface models relevant to this soil layer and target period.
# Soil layer of interest.
for model, i in it.product(['lsm', 'cmip5', 'cmip6'], range(4)):
    d = depth[i]
    dcm = depth_cm[i]

    if model == 'lsm':
        model_list = lsm_list[(str(year_shortest[0]) + '-' + \
                               str(year_shortest[-1]), '0.00-0.10')]
        met_list = []
        for met, value in met_to_lsm.items():
            for l in model_list:
                if l in value:
                    met_list.append(met)
        met_list = list(np.unique(met_list))
    elif model == 'cmip5':
        met_list = cmip5_availability('0-10cm', land_mask)
    elif model == 'cmip6':
        met_list = list(set(mrsol_availability('0-10cm', land_mask,
                                               'historical')) &\
                        set(mrsol_availability('0-10cm', land_mask, 'ssp585')))
        met_list = sorted([x for x in met_list if 'r1i1p1f1' in x])

    pool = mp.Pool(8)
    pool.map_async(clim, list(it.product(met_list, ['tas', 'pr'])))
    pool.close()
    pool.join()
