"""
2020/05/19
ywang254@utk.edu

Plot the performances of Mean, Median, best method of DOLCE, and best method 
of EM-LSM by
 - MODIS land cover type
 - continent
using the validation-period ISMN observations.
"""
from utils_management.constants import year_longest, depth, depth_cm, \
    lu_names, continent_legend
import utils_management as mg
from misc.ismn_utils import get_weighted_monthly_data
from misc.analyze_utils import calc_stats
import os
import pandas as pd
import matplotlib.pyplot as plt
import itertools as it
import numpy as np
import matplotlib as mpl
from matplotlib.cm import get_cmap
from matplotlib.gridspec import GridSpec, GridSpecFromSubplotSpec


def read_data(clas, d, iam, opt, stat):
    metric_raw = {}
    metric_raw['lsm'] = pd.read_csv(os.path.join(mg.path_out(),
                                                 'standard_metrics',
                                                 'by_' + clas + \
                                                 '_lsm_' + d + '_all_' + \
                                                 iam + '_' + opt + '.csv'),
                                    index_col = 0,
                                    header = [0,1]).loc[:, stat]
    metric_raw['cmip5'] = pd.read_csv(os.path.join(mg.path_out(),
                                                   'standard_metrics',
                                                   'by_' + clas + \
                                                   '_cmip5_' + d + '_' + \
                                                   iam + '_' + \
                                                   opt + '.csv'),
                                      index_col = 0,
                                      header = [0,1]).loc[:, stat]
    metric_raw['cmip6'] = pd.read_csv(os.path.join(mg.path_out(),
                                                   'standard_metrics',
                                                   'by_' + clas + \
                                                   '_cmip6_' + d + '_' + \
                                                   iam + '_' + \
                                                   opt + '.csv'),
                                      index_col = 0,
                                      header = [0,1]).loc[:, stat]
    metrics_by_region = pd.read_csv(os.path.join(mg.path_out(), 
                                                 'standard_metrics',
                                                 'by_' + clas + '_' +\
                                                 d + '_1950-2016_' + \
                                                 iam + '_' + \
                                                 opt + '.csv'),
                                    index_col = 0,
                                    header = [0,1]).loc[:,
                                                        ((slice(None),
                                                          stat))]
    metrics_by_region.sort_index(inplace=True)
    metrics_by_region.columns = metrics_by_region.columns.droplevel(1)

    #if clas == 'land_use_modis':
    #    # Remove water bodies & Permanent wetlands
    #    if d_ind != 0:
    #        metrics_by_region.drop([0], axis = 0, inplace = True)
    #    else:
    #        metrics_by_region.drop([0, 11], axis = 0, inplace = True)

    if (clas == 'continent') and (999 in metrics_by_region.index):
        metrics_by_region = metrics_by_region.drop(999, axis = 0)
    if (clas == 'land_use_modis'):
        if 255 in metrics_by_region.index:
            metrics_by_region = metrics_by_region.drop(255, axis = 0)
        if 11 in metrics_by_region.index: # 'Permanent Wetlands'
            metrics_by_region = metrics_by_region.drop(16, axis = 0)
        if 15 in metrics_by_region.index: # 'Permanent Snow and Ice'
            metrics_by_region = metrics_by_region.drop(15, axis = 0)
        if 6 in metrics_by_region.index: # 'Closed Shrublands'
            metrics_by_region = metrics_by_region.drop(15, axis = 0)

    return metric_raw, metrics_by_region


# Aggregation method to produce the grid-scale ISMN observations.
iam = 'lu_weighted'

# Subset of observations to compare on. Different periods produce similar
# metrics ("plot_standard_metrics_all.py") and have similar spatial
# availability ("ismn_summary.py").
opt = 'val'


stats = ['Bias','RMSE','Corr']
units = ['(m$^3$/m$^3$)','(m$^3$/m$^3$)', '(-)']
prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all']
prod_name = ['Mean ORS', 'OLC ORS', 'EC ORS', 'EC CMIP5', 'EC CMIP6',
             'EC CMIP5+6', 'EC ALL']


mpl.rcParams['font.size'] = 5.
mpl.rcParams['axes.titlesize'] = 5.
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'
cmap = get_cmap('jet')
clist = [cmap((i+0.7)/7) for i in range(7)]
#clist = ['#1b9e77', '#d95f02', '#7570b3', '#e7298a','#66a61e','#e6ab02',
#         '#a6761d']
mlist = ['x', 'o', 's', 's', 's', 's', 's']


for clas in ['continent', 'land_use_modis']:
    fig = plt.figure(figsize = (6., 8.))
    axes = np.empty(len(stats) * len(depth), dtype = object)
    gs = GridSpec(len(stats), 1, hspace = 0.05)
    for i in range(len(stats)):
        sub_gs = GridSpecFromSubplotSpec(len(depth), 1, gs[i], hspace = 0.)
        for j in range(len(depth)):
            if j == 0:
                axes[i*len(depth) + j] = fig.add_subplot(sub_gs[j])
            else:
                axes[i*len(depth) + j] = fig.add_subplot(sub_gs[j],
                    sharex = axes[i*len(depth) + 0],
                    sharey = axes[i*len(depth) + 0])

    for s_ind, d_ind in it.product(range(len(stats)), range(len(depth))):
        stat = stats[s_ind]
        d = depth[d_ind]
        dcm = depth_cm[np.where([x == d for x in depth])[0][0]]

        metric_raw, metrics_by_region = read_data(clas, d, iam, opt, stat)
        if clas == 'continent':
            regions = sorted(continent_legend.keys())
            regions.remove(999)
        else:
            regions = sorted(lu_names.keys())
            regions.remove(255)
            regions.remove(11)
            regions.remove(15)
            regions.remove(6)
#        ax = axes[d_ind + s_ind*len(depth)]

        ##print(stat, d)
        ##metrics_by_region.index = [lu_names[rr] for rr in \
        ##                           metrics_by_region.index]
        ##print(metrics_by_region.loc[:, [prd.replace(' ','_') for
        ##                                prd in prod_name]])

        for rind, rr in enumerate(regions):
            if not rr in metrics_by_region.index:
                continue

            anchor = rind * 4

            for pind, prod in enumerate(['lsm', 'cmip5', 'cmip6']):
                hb = ax.boxplot(metric_raw[prod].loc[:, str(rr)].values,
                                positions = [anchor + pind],
                                widths = 0.4, whis = [0, 100],
                                showfliers = False)
                hb['boxes'][0].set_color('k')
                hb['boxes'][0].set_linewidth(0.5)
                hb['medians'][0].set_linewidth(0.5)
                hb['whiskers'][0].set_linewidth(0.5)
                hb['whiskers'][1].set_linewidth(0.5)
                hb['caps'][0].set_linewidth(0.5)
                hb['caps'][1].set_linewidth(0.5)
                #dummy()

            count = 0; count2 = 0
            h = []
            for pind, prod in enumerate(prod_list):
                if ('lsm' in prod) | ('all' in prod):
                    x = anchor + (count - 1.5) * 0.2
                    count += 1
                elif 'cmip5' in prod:
                    x = anchor + 1
                else:
                    x = anchor + 2 + (count2 - 0.5) * 0.3
                    count2 += 1
                temp, = ax.plot(x, metrics_by_region.loc[rr,
                    prod_name[pind].replace(' ', '_')],
                                mlist[pind],
                                color = clist[pind], markersize = 2,
                                markerfacecolor = 'None', mew = 0.5)
                h.append(temp)

        ax.set_ylabel(dcm)
        if d_ind == 2:
            ax.text(-0.13, 0, stat + units[s_ind], rotation = 90,
                    transform = ax.transAxes)

        xticks = np.arange(-1., len(regions) * 4, 4)
        xminorticks = np.arange(0., len(regions) * 4, 1)
        ax.set_xticks(xticks)
        ax.set_xticklabels([])
        ax.set_xticks(xminorticks, minor = True)
        ax.grid(True, which = 'major', linewidth = 0.5)
        ax.set_xlim([-1, len(regions) * 4 - 1])
        ax.text(0.04, 0.83, '(' + lab[d_ind+s_ind*len(depth)] + ')',
                transform = ax.transAxes)

        if (d_ind != (len(depth) - 1)):
            ax.tick_params('x', length = 0.)
            ax.tick_params('x', which = 'minor', length = 0.)

        if (s_ind == (len(stats) - 1)) & (d_ind == (len(depth) - 1)):
            for rind, rr in enumerate(regions):
                if clas == 'continent':
                    rname = continent_legend[rr]
                else:
                    rname = lu_names[rr]
                if rname == 'Urban and Built-up Lands':
                    rname = 'Urban and\nBuilt-up Lands'
                elif rname == 'Cropland/Natural Vegetation Mosaics':
                    rname = 'Cropland/\nNatural\nVegetation\nMosaics'
                else:
                    rname = rname.replace(' ','\n')
                ax.text((rind + 0.5) / len(regions), -1.3, rname,
                        transform = ax.transAxes, rotation = 90,
                        horizontalalignment = 'center',
                        verticalalignment = 'center')
                for pind, prod in enumerate(['ORS', 'CMIP5', 'CMIP6']):
                    ax.text((rind + (pind+0.5) /4) / len(regions),
                            -.4, prod, transform = ax.transAxes,
                            rotation = 90, verticalalignment = 'center')

    ax.legend(h, prod_name, ncol = 5, loc = [0, -2.4])

    fig.savefig(os.path.join(mg.path_out(), 'standard_metrics', 
                             'by_' + clas + '_' + \
                             iam + '_' + opt + '.png'), 
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
