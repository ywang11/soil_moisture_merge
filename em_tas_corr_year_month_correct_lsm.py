"""
2020/01/10

Catch & correct negative soil moisture in the concatenated or produced
 emergent constraint soil moisture values.
Not including lsm & all, because these should correct concatenated values.
"""
import xarray as xr
import os
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth, depth_cm
import utils_management as mg
import numpy as np
import itertools as it
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import pandas as pd
from misc.em_utils import rm_oor


year_list = [year_longest, year_shorter, year_shorter2, year_shortest]
model_list = ['lsm', 'all']
prefix_list = ['predicted'] # , 'predicted_CI_lower', 'predicted_CI_upper']


###############################################################################
# Read the restored emergent constraint-based soil moisture. Find the 
# negative values. Plot. Set the negative values to 1e-16, and the values > 1.
# to 1 - 1e-16.
###############################################################################
for i, year, model, prefix in it.product(range(4), year_list, model_list, 
                                         prefix_list):
    d = depth[i]
    dcm = depth_cm[i]

    if model == 'lsm':
        opt_list = ['year_month_anomaly_9grid', 'year_month_9grid']
    elif model == 'all':
        opt_list = ['year_month_anomaly_9grid', 'year_month_9grid'] #,
#                    'year_month_anomaly_1grid', 'year_month_1grid']

    year_str = str(year[0]) + '-' + str(year[-1])
    period = pd.date_range(str(year[0]) + '-01-01',
                           str(year[-1]) + '-12-31', freq = 'MS')

    for opt in opt_list:
        data = xr.open_mfdataset([os.path.join(mg.path_out(),
                                               'em_' + model + '_corr',
                                               'CRU_v4.03_' + \
                                               opt.replace('anomaly',
                                                           'restored') + \
                                               '_' + dcm + '_' + year_str,
                                               prefix + '_' + str(y) + '.nc') \
                                  for y in year], decode_times = False,
                                 concat_dim = 'time')
        data[prefix].load()

        rm_oor(data[prefix], os.path.join(mg.path_out(),
                                          'em_' + model + '_corr',
                                          'CRU_v4.03_' + \
                                          opt.replace('anomaly', 'positive')\
                                          + '_' + dcm + '_' + year_str),
               '', prefix, period)

        data.close()
