"""
2019/07/09

ywang254@utk.edu

Plot the zonal and spatial trends of the soil moisture data of the CMIP6
models. Resample to annual values first.
"""
import os
import sys
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import utils_management as mg
from utils_management.constants import year_cmip6, cmip6_list
from misc.plot_utils import plot_ts_shade


year = year_cmip6 # [year_cmip5, year_cmip6]
time = pd.date_range(str(year[0])+'-01-01', str(year[-1])+'-12-31', 
                     freq = 'MS')

act = 'CMIP'
expr = 'historical'
var = 'mrsos'
tid = 'Lmon'
model_version = cmip6_list(act, expr, var, tid)


# Global time series with shade.
ts_collect = np.empty([len(year), len(model_version)])
for m_v_ind, m_v in enumerate(model_version):
    data = pd.read_csv(os.path.join(mg.path_out(),
                                    'standard_diagnostics_cmip6',
                                    var + '_' + expr + '_' + m_v + '_' + \
                                    str(year[0]) + '-' + str(year[-1]) + \
                                    '_g_ts.csv'), index_col = 0, 
                       parse_dates = True)
    data = data.groupby(data.index.year).mean()
    ts_collect[:, m_v_ind] = data.values.reshape(-1)
fig, ax = plt.subplots(figsize = (10, 10))
plot_ts_shade(ax, year, ts = {'min': np.min(ts_collect, axis=1), 
                              'mean': np.mean(ts_collect, axis=1),
                              'max': np.max(ts_collect, axis=1)}, 
              ts_col = 'k')
ax.set_ylim([0., 0.6])
fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot', 
                         'ts_cmip6', var + '_' + expr + '_' + \
                         str(year[0]) + '-' + str(year[-1]) + '_global.png'),
            dpi = 600.)
plt.close(fig)


# Time series by latitude/continent/SREX regions.
lat_bnds = np.concatenate([np.arange(-90., 81., 10.).reshape(-1,1),
                           np.arange(-80., 91., 10.).reshape(-1,1)],
                          axis = 1)
latitude = [('%d' % lat_bnds[i,0]) + '-' + ('%d' % lat_bnds[i,1]) for \
            i in range(lat_bnds.shape[0])]
lat_name = [('%d' % lat_bnds[i,0]) + ' ~ ' + ('%d' % lat_bnds[i,1]) for \
            i in range(lat_bnds.shape[0])]

continent = ['AF', 'AN', 'AS', 'EU', 'NA', 'OC', 'SA']
cont_name = ['Africa', 'Antarctica', 'Asia', 'Europe', 'North America', 
             'Oceania', 'South Africa']

srex = ['ALA','CGI','WNA','CNA','ENA','CAM','AMZ','NEB','WSA','SSA',
        'NEU','CEU','MED','SAH','WAF','EAF','SAF','NAS','WAS','CAS',
        'TIB','EAS','SAS','SEA','NAU','SAU']
srex_name = srex


for sep, sep_name, sep_suffix in zip([latitude, continent, srex], 
                                     [lat_name, cont_name, srex_name], 
                                     ['_g_lat_ts.csv', '_g_continent_ts.csv', 
                                      '_g_srex_ts.csv']):
    ncols = 3
    nrows = int(np.ceil(len(sep) / ncols))

    fig, axes = plt.subplots(nrows = nrows, ncols = ncols, 
                             figsize = (6 * ncols, 4 * nrows), 
                             sharex = True, sharey = True)

    for ssss_ind, ssss in enumerate(sep):
        ax = axes.flat[ssss_ind]

        ts_collect = np.empty([len(year), len(model_version)])
        for m_v_ind, m_v in enumerate(model_version):
            data = pd.read_csv(os.path.join(mg.path_out(),
                                            'standard_diagnostics_cmip6',
                                            var + '_' + expr + '_' + m_v + \
                                            '_' + str(year[0]) + '-' + \
                                            str(year[-1]) + sep_suffix), 
                               index_col = 0, parse_dates = True)[ssss]
            data = data.groupby(data.index.year).mean()
            ts_collect[:, m_v_ind] = data.values.reshape(-1)

        plot_ts_shade(ax, year, ts = {'min': np.min(ts_collect, axis=1),
                                      'mean': np.mean(ts_collect, axis=1),
                                      'max': np.max(ts_collect, axis=1)},
                      ts_col = 'k')
        ax.set_ylim([0., 0.6])
        ax.set_title(sep_name[ssss_ind])

    fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot', 
                             'ts_cmip6', var + '_' + expr + \
                             '_' + str(year[0]) + '-' + str(year[-1]) + \
                             '_' + sep_suffix.split('_')[2] + '.png'),
                dpi = 600.)
    plt.close(fig)
