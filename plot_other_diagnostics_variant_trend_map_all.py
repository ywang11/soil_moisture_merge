"""
2019/07/05

ywang254@utk.edu

Map the annual & seasonal mean soil moisture & trend for all the merged
products.
"""
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth
import utils_management as mg
import numpy as np
import os
import itertools as it
from misc.plot_utils import cmap_gen, stipple
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import cartopy.crs as ccrs
from cartopy.util import add_cyclic_point
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import xarray as xr
import multiprocessing as mp

data_list = [x+'_'+y for x in ['mean', 'dolce', 'em'] for y in \
             ['lsm', 'cmip5', 'cmip6', '2cmip', 'all']]

year = year_longest
year_str = str(year[0]) + '-' + str(year[-1])

season_list = ['Annual', 'DJF', 'MAM', 'JJA', 'SON']

levels = np.linspace(-0.0004, 0.0004, 21)
cmap = cmap_gen('autumn', 'winter_r')
map_extent = [-180, 180, -60, 90]

def plotter(d):
    fig, axes = plt.subplots(nrows = len(data_list), ncols = len(season_list),
                             figsize = (17, 26),
                             subplot_kw = {'projection': ccrs.PlateCarree()})
    fig.subplots_adjust(wspace = 0., hspace = 0.)
    for mind, sind in it.product(range(len(data_list)),
                                 range(len(season_list))):
        model = data_list[mind]
        ss = season_list[sind]

        data = xr.open_dataset(os.path.join(mg.path_out(), 'other_diagnostics',
                                            model, d + '_g_map_trend_' + \
                                            ss + '.nc'))
        ax = axes[mind, sind]
        ax.coastlines()
        ax.set_extent(map_extent)
        trend_cyc, lon_cyc = add_cyclic_point( \
            data['g_map_trend'].values, coord=data.lon)
        h = ax.contourf(lon_cyc, data.lat, trend_cyc, cmap = cmap,
                        levels = levels, extend = 'both')
        trend_p = data['g_p_values'].values
        stipple(ax, data.lat.values, data.lon.values, trend_p)

        if sind == 0:
            ax.text(-0.1, 0.5, model, rotation = 90, transform = ax.transAxes)
        if mind == 0:
            ax.set_title(ss)

    cax = fig.add_axes([0.93, 0.1, 0.01, 0.8])
    fig.colorbar(h, cax = cax, boundaries = levels,
                 ticks = 0.5 * (levels[1:] + levels[:-1]))
    fig.savefig(os.path.join(mg.path_out(), 'other_diagnostics_plot',
                             'variant_trend', d + '.png'), dpi = 600.)
    plt.close(fig)


for d in depth:
    plotter(d)

#p = mp.Pool(4)
#p.map_async(plotter, data_list)
#p.close()
#p.join()
