"""
20190918

ywang254@utk.edu

Test & remove the temporal inhomogeneity in the dataset.

Su et al. 2016 Homogeneity of a global multisatellite soil moisture climate data record. DOI: 10.1002/2016GL070458

T_{i}
t_{i,j}
T_{i}
T_{j}
"""
import numpy as np
