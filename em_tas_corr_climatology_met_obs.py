"""
20191120
ywang254@utk.edu

Compute the climatology of CRUv4.03 precipitation, and temperature 
 during the:
 1981-2010 overlapping period for use with the LSMs & CMIP5 & CMIP6 models.
"""
import utils_management as mg
from utils_management.constants import met_to_lsm, year_longest, \
    year_shortest, target_lat, target_lon
import os
import xarray as xr
import pandas as pd
import numpy as np
import itertools as it
import time


land_mask = 'vanilla'

for var, year in it.product(['tas', 'pr'], [year_shortest]):
    flist = [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                          land_mask, 'CRU_v4.03', var + '_' + str(y) + '.nc') \
             for y in year]
    data = xr.open_mfdataset(flist, decode_times = False)
    met = data[var].values.copy()
    data.close()

    climatology = np.full([12, len(target_lat), len(target_lon)], np.nan)
    for month in range(12):
        climatology[month, :, :] = np.mean(met[month::12, :, :], axis = 0)
    climatology =  xr.DataArray(climatology, coords = {'month': range(1,13),
                                                       'lat': target_lat,
                                                       'lon': target_lon},
                                dims = ['month', 'lat', 'lon'])
    climatology.to_dataset(name = var).to_netcdf(os.path.join( \
        mg.path_intrim_out(), 'em_tas_corr_climatology', var + \
        '_CRU_v4.03_' + str(year[0]) + '-' + str(year[-1]) + '.nc'))
