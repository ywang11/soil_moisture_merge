# -*- coding: utf-8 -*-
"""
Created on Wed May 15 16:39:13 2019

@author: ywang254

Compare the land surface models, and in situ observations, grid-by-grid. 
"""
import utils_management as mg
from utils_management.constants import depth, year_longest, clim_zone, lu_names
from misc.dolce_utils import get_ismn_aggr_method, get_weighted_monthly_data, \
    get_cov_method
from misc.analyze_utils import calc_stats
from misc.ismn_get_monthly import ismn_get_monthly
import os
import pandas as pd
import time
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np


start = time.time()


new = True
if new:
    for i,d in enumerate(depth):
        os.mkdir(os.path.join(mg.path_out(), 'examine_dolce_by_grid', d))
        for c in clim_zone:
            os.mkdir(os.path.join(mg.path_out(), 'examine_dolce_by_grid', d, c))


# Choose the weighting method that was used to generate the weighted averaged
# ISMN data.
##from ismn_aggr_dolce import simple, dominance_lc, dominance_threshold
# ---- default to LU-weighted, not cut off by minimum pct represented 
#      land cover
simple = False # [True, False]
dominance_lc = False # [True, False]
dominance_threshold = 40
ismn_aggr_method = get_ismn_aggr_method(simple, dominance_lc, dominance_threshold)


# Pick the covariance function based on which the weights are calculated
# ---- default to the simple empirical estimator
cov_estimer = 0
method = get_cov_method(cov_estimer)


# Select the time range to calculate the RMSE between observation and land
# surface models.
year = year_longest
time_range = pd.date_range(start=str(year[0])+'-01-01',
                           end=str(year[-1])+'-12-31', freq='MS', tz='UTC')

# 
cmap = cm.get_cmap('Spectral')

#
for i,d in enumerate(depth):

    grid_latlon, weighted_monthly_data, available_year = \
        get_weighted_monthly_data(os.path.join(mg.path_intrim_out(), 
                                  'ismn_aggr_dolce'), ismn_aggr_method, d)

    # Read in the gridded datasets at the observation points.
    if i==0:
        gridded_datasets = ['weighted', 'average', 'median', 'satellite']
    else:
        gridded_datasets = ['weighted', 'average', 'median']

    weighted_sm_at_data = {}
    for j in gridded_datasets:
        weighted_sm_at_data[j] = pd.read_csv(os.path.join(mg.path_out(), 
            'postprocess_grid_at_obs', j + '_' + d + '.csv'), index_col=0)
        weighted_sm_at_data[j].index = pd.to_datetime( \
            weighted_sm_at_data[j].index, format = '%Y-%m-%d')

    # 
    for k in grid_latlon.columns:
        if sum(~np.isnan(weighted_monthly_data.loc[:,k])) == 0:
            continue

        # Obtain the station information over this grid.
        def parse_grid(ddd, gr):
            fpath = os.path.join(mg.path_intrim_out(), 'ismn_lu', ddd, 
                                 gr + '.txt')
            f = open(fpath, 'r')
            # ---- grid lat lon
            grid_coords = [float(x) for x in f.readline().split(',')]
            f.readline() # remove 'Grid' label
            # ---- grid land cover percentages
            grid_lu = [float(n) for n in f.readline().split(',')]
            f.close()
            # ---- station land covers at this grid
            stations_lu = pd.read_csv(fpath, skiprows = 3)
            return grid_coords, grid_lu, stations_lu
        grid_coords, grid_lu, stations_lu = parse_grid(d, k)


        #######################################################################
        # Some preliminary quantities.
        # Grid climate zone.
        cz = stations_lu.iloc[0, -1]

        if not isinstance(cz, str):
            if np.isnan(cz):
                cz = 'Unknown'

        # Main land use type plus percentage of Water Bodies.
        mlu = np.argmax(grid_lu[1:])
        main_lu = ('%.2f' % grid_lu[mlu+1]) + '% ' + lu_names[mlu+1] + \
            ', ' + ('%.2f' % grid_lu[0]) + '% Water Bodies'

        # Timestamps.
        ts = weighted_monthly_data.index

        # Weighted averaged grid-scale observation.
        x = weighted_monthly_data.loc[:,k].values
        #######################################################################


        #
        fig, axes = plt.subplots(nrows = 2, ncols = 2, figsize=(12,12))
        # ---- scatterplot at grid scale and error statistics
        # ---- also the time series of the individual grid-scale data
        colors = cmap(np.arange(.1, 1., .2))
        for c,j in enumerate(gridded_datasets):
            # ---- grid-scale gridded data
            y = weighted_sm_at_data[j].loc[:,k].values

            # ---- skip if < 3 data points available
            non_nan = ~np.isnan(x) & ~np.isnan(y)
            if sum(non_nan) < 3:
                continue

            ax = axes.flat[0]
            ax.plot(x[non_nan], y[non_nan], 'o', color=colors[c,:], label = j)
            rmse, _, _, _ = calc_stats(y[non_nan], x[non_nan])
            ax.text(.05, .7+0.06*c, 'RMSE$_{'+j+'}$=' + ('%.3f' % rmse),
                    transform=ax.transAxes)
            ax.legend()
            ##ax.axis('equal')
            ##ax.set_aspect('equal', 'box')
            ax.set_xlim([.0, .5])
            ax.set_ylim([.0, .5])
            ax.set_title(main_lu)

            ax = axes.flat[2]
            ax.plot(ts, y, color=colors[c,:], linewidth=1, label = j)
            ax.legend()
            ax.set_ylim([.0, np.nanmax(x)*3])
        # ---- the time series of the grid average and individual stations
        ax = axes.flat[3]
        colors = cmap(np.arange(.5/stations_lu.shape[0], 1., 
                                1./stations_lu.shape[0]))
        ax.plot(ts, x, color=[.5, .5, .5], linewidth=2, label='Grid Avg')
        # ---- plot the individual stations
        for ind,p in stations_lu.iterrows():
            # ---- obtain the individual stations' data
            monthly_data = ismn_get_monthly(os.path.join(mg.path_data(),
                'ISMN', p['Network']), p['Station'], p['Depth-Actual'],
                p['Sensor'], time_range)
            # ---- plot the data
            ax.plot(monthly_data.index, monthly_data.values, '-', 
                    color=colors[ind,:], linewidth=1,
                    label=p['Network']+' '+str(p['Station'])+' '+ \
                          lu_names[p['Land Cover']])
        ax.set_ylim([.0, np.nanmax(x)*3])
        ax.legend()

        fig.savefig(os.path.join(mg.path_intrim_out(), 'examine_dolce_by_grid', 
                                 d, cz, '-'.join(lu_names[mlu+1 \
                                 ].replace('/',' ').split(' ')) + \
                                 '_' + k + '_' + d + '.png'))
        plt.close(fig)

end = time.time()
#The script finished in 9.2023 hours.
print('The script finished in ' + ('%.4f' % ((end-start)/3600) ) + ' hours.')