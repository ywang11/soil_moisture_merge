# -*- coding: utf-8 -*-
"""
2019/11/21

@author: ywang254

Copied and modified from "em_tas_corr_month.py"

Estimate regression relationship between grid-scale precipitation, temperature,
 and modeled soil moisture at different depths, for each month in each year,
 using the nearby 1x1 (i.e. single), 3x3, ... grid cells, and the nearby
 3 months of data (except for the beginning and the end).
"""
import utils_management as mg
from utils_management.constants import met_to_lsm, year_longest, \
    year_shorter, year_shorter2, year_shortest, lsm_list, depth, depth_cm, \
    target_lat, target_lon, year_cmip5, year_cmip6
from misc.spatial_utils import extract_grid
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import os
import xarray as xr
import pandas as pd
import numpy as np
import itertools as it
import time
import multiprocessing as mp
import statsmodels.api as stats
from statsmodels.sandbox.regression.predstd import wls_prediction_std


land_mask = 'vanilla'

start = time.time()


###############################################################################
# Settings
###############################################################################
# Target period of interest.
year = REPLACE1 # [year_longest, year_shorter, year_shorter2, year_shortest]
year_str = str(year[0]) + '-' + str(year[-1])
period = pd.date_range(str(year[0])+'-01-01', str(year[-1])+'-12-31',
                       freq = 'MS')
month = REPLACE2 # {0..11}

# Soil layer of interest.
i = REPLACE3 # [0, 1, 2, 3]
d = depth[i]
dcm = depth_cm[i]

# Land surface models relevant to this soil layer and target period.
model = 'REPLACE4' # 'lsm', 'cmip5', 'cmip6'
if model == 'lsm':
    model_list = lsm_list[(str(year[0])+'-'+str(year[-1]), d)]
elif model == 'cmip5':
    model_list = cmip5_availability(dcm, land_mask)
elif model == 'cmip6':
    model_list = list(set(mrsol_availability(dcm, land_mask, 'historical')) &\
                      set(mrsol_availability(dcm, land_mask, 'ssp585')))
    model_list = sorted([x for x in model_list if 'r1i1p1f1' in x])

# Temporal range of data to use in the regression.
# Always just 1 grid.
opt = 'REPLACE5' # ['year_mw_1grid', 'year_mw_9grid', 'year_mw_anomaly_1grid',
                 #  'year_mw_anomaly_9grid']




###############################################################################
# Preparations
###############################################################################
# Number x number nearby grids for the regression.
n_gr = int(opt.split('_')[-1][0])
n_sq_gr = int(np.sqrt(n_gr)) # [1, 3, ...]

# Observed precipitation and temperature dataset to use.
met_obs_name = 'CRU_v4.03'

# Prefix of the precipitation and temperature data that are to be used in
# the regression.
if model == 'lsm':
    temp1 = mg.path_to_pr(land_mask)
    temp2 = mg.path_to_tas(land_mask)
    pr_list = []
    tas_list = []
    met_name_list = []
    for l in model_list:
        for met, value in met_to_lsm.items():
            if l in value:
                pr_list.append(temp1[met])
                tas_list.append(temp2[met])
                met_name_list.append(met)
else:
    pr_list = []
    tas_list = []
    met_name_list = []
    for m in model_list:
        pr_list.append(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                    land_mask, model.upper(), m, 'pr_'))
        tas_list.append(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                     land_mask, model.upper(), m, 'tas_'))
        met_name_list.append(m)
pr_list_uniq, temp_ind = np.unique(pr_list, return_index = True)
pr_list_uniq = list(pr_list_uniq)
tas_list_uniq = [tas_list[i] for i in temp_ind]
met_name_list_uniq = [met_name_list[i] for i in temp_ind]


###############################################################################
# Read the soil moisture data for regression.
###############################################################################
sm_data = np.full([len(model_list), len(year), 3, len(target_lat), 
                   len(target_lon)], np.nan)
for m_ind, m in enumerate(model_list):
    if model == 'lsm':
        flist = [os.path.join(mg.path_intrim_out(), 'Interp_Merge', 
                              land_mask, m, m + '_' + str(y) + '_' + \
                              dcm + '.nc') for y in year]
    elif model == 'cmip5':
        f_hist = [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                               land_mask, model.upper(), m, 'sm_historical_' +\
                               'r1i1p1_' + str(y) + '_' + dcm + '.nc') \
                  for y in year_cmip5]
        f_85 = [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                             land_mask, model.upper(), m, 'sm_rcp85_' +\
                             'r1i1p1_' + str(y) + '_' + dcm + '.nc') \
                for y in range(year_cmip5[-1]+1, year[-1]+1)]
        flist = f_hist + f_85
    elif model == 'cmip6':
        f_hist = [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                               land_mask, model.upper(), m, 
                               'mrsol_historical_' + str(y) + '_' + dcm + \
                               '.nc') for y in year_cmip6]
        f_85 = [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                             land_mask, model.upper(), m, 'mrsol_ssp585_' +\
                             str(y) + '_' + dcm + '.nc') \
                for y in range(year_cmip6[-1]+1, year[-1]+1)]
        flist = f_hist + f_85

    data = xr.open_mfdataset(flist, decode_times = False)
    if data.lat.values[0] > -87:
        # Pad to global data
        temp = target_lat < data.lat.values[0]
        if month == 0:
            sm_data[m_ind, 1:, 0, temp, :] = data.sm.values[11:(-1):12, :, 
                                                            :].copy()
        else:
            sm_data[m_ind, :, 0, temp, :] = data.sm.values[(month-1)::12,
                                                           :, :].copy()
        sm_data[m_ind, :, 1, temp, :] = data.sm.values[month::12, :, :].copy()
        if month == 11:
            sm_data[m_ind, :(-1), 2, temp,
                    :] = data.sm.values[(month+1)::12, :, :].copy()
        else:
            sm_data[m_ind, :, 2, temp, :] = data.sm.values[(month+1)::12, :,
                                                           :].copy()
    else:
        if month == 0:
            sm_data[m_ind, 1:, 0, :, :] = data.sm.values[11:(-1):12,
                                                         :, :].copy()
        else:
            sm_data[m_ind, :, 0, :, :] = data.sm.values[(month-1)::12,
                                                        :, :].copy()
        sm_data[m_ind, :, 1, :, :] = data.sm.values[month::12, :, :].copy()
        if month == 11:
            sm_data[m_ind, :(-1), 2, :, :] = data.sm.values[(month+1)::12,
                                                            :, :].copy()
        else:
            sm_data[m_ind, :, 2, :, :] = data.sm.values[(month+1)::12, :,
                                                        :].copy()
    data.close()

    # Remove the climatology if specified.
    if 'anomaly' in opt:
        data = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                            'em_tas_corr_climatology',
                                            'sm_' + model + '_' + \
                                            m + '_' + dcm + '.nc'))
        if month == 0:
            sm_data[m_ind, :, 0, :, :] -= data['sm'].values[11, :, :]
            sm_data[m_ind, :, 1, :, :] -= data['sm'].values[0, :, :]
            sm_data[m_ind, :, 2, :, :] -= data['sm'].values[1, :, :]
        elif month == 11:
            sm_data[m_ind, :, 0, :, :] -= data['sm'].values[10, :, :]
            sm_data[m_ind, :, 1, :, :] -= data['sm'].values[11, :, :]
            sm_data[m_ind, :, 2, :, :] -= data['sm'].values[0, :, :]
        else:
            for lag in range(3):
                sm_data[m_ind, :, lag, :, 
                        :] -= data['sm'].values[month + lag - 1, :, :]
        data.close()


# Average over the different LSMs that have common forcing.
sm_data2 = np.full([len(pr_list_uniq), len(year), 3, len(target_lat),
                    len(target_lon)], np.nan)
for p_ind, p in enumerate(pr_list_uniq):
    p_ind_list = [i for i,x in enumerate(pr_list) if x == p]
    sm_data2[p_ind, :, :, :, :] = np.mean(sm_data[p_ind_list, :, :, :, :],
                                          axis = 0)
# ---- Keep the 95th and 5th percentiles of the source models for later use.
sm_data_mean = np.nanmean(sm_data[:, :, 1, :, :], axis = 0)
sm_data_p95 = np.nanpercentile(sm_data[:, :, 1, :, :], 95, axis = 0)
sm_data_p05 = np.nanpercentile(sm_data[:, :, 1, :, :], 5, axis = 0)
# ---- Now replace the sm_data with the averaged.
sm_data = sm_data2    
del sm_data2


###############################################################################
# Read the precipitation data for regression.
###############################################################################
pr_data = np.full([len(pr_list_uniq), len(year), 3, len(target_lat), 
                   len(target_lon)], np.nan)
for p_ind, p in enumerate(pr_list_uniq):
    if model == 'lsm':
        flist = [p + str(y) + '.nc' for y in year]
    elif model == 'cmip5':
        f_hist = [p + 'historical_r1i1p1_' + str(y) + '.nc' \
                  for y in year_cmip5]
        f_85 = [p + 'rcp85_r1i1p1_' + str(y) + '.nc' \
                for y in range(year_cmip5[-1]+1, year[-1]+1)]
        flist = f_hist + f_85
    elif model == 'cmip6':
        f_hist = [p + 'historical_' + str(y) + '.nc' \
                  for y in year_cmip6]
        f_85 = [p + 'ssp585_' + str(y) + '.nc' \
                for y in range(year_cmip6[-1]+1, year[-1]+1)]
        flist = f_hist + f_85

    p_ind_list = [i for i,x in enumerate(pr_list) if x == p]

    data = xr.open_mfdataset(flist, decode_times = False)
    if data.lat[0] > -87:
        # Pad to global data
        temp = target_lat < data.lat.values[0]
        if month == 0:
            pr_data[p_ind, 1:, 0, temp, :] = data.pr.values[11:(-1):12, :,
                                                            :].copy()
        else:
            pr_data[p_ind, :, 0, temp, :] = data.pr.values[(month-1)::12,
                                                           :, :].copy()
        pr_data[p_ind, :, 1, temp, :] = data.pr.values[month::12, :, :].copy()
        if month == 11:
            pr_data[p_ind, :(-1), 2, temp, 
                    :] = data.pr.values[(month+1)::12, :, :].copy()
        else:
            pr_data[p_ind, :, 2, temp, :] = data.pr.values[(month+1)::12, :,
                                                           :].copy()
    else:
        if month == 0:
            pr_data[p_ind, 1:, 0, :, :] = data.pr.values[11:(-1):12, :,
                                                         :].copy()
        else:
            pr_data[p_ind, :, 0, :, :] = data.pr.values[(month-1)::12,
                                                        :, :].copy()
        pr_data[p_ind, :, 1, :, :] = data.pr.values[month::12, :, :].copy()
        if month == 11:
            pr_data[p_ind, :(-1), 2, :, :] = data.pr.values[(month+1)::12, 
                                                            :, :].copy()
        else:
            pr_data[p_ind, :, 2, :, :] = data.pr.values[(month+1)::12, :,
                                                        :].copy()
    data.close()

    # Remove the climatology if specified.
    if 'anomaly' in opt:
        data = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                            'em_tas_corr_climatology',
                                            'pr_' + model + '_' + \
                                            met_name_list_uniq[p_ind] + '.nc'))
        if month == 0:
            pr_data[p_ind, :, 0, :, :] -= data['pr'].values[11, :, :]
            pr_data[p_ind, :, 1, :, :] == data['pr'].values[0, :, :]
            pr_data[p_ind, :, 2, :, :] == data['pr'].values[1, :, :]
        elif month == 11:
            pr_data[p_ind, :, 0, :, :] -= data['pr'].values[10, :, :]
            pr_data[p_ind, :, 1, :, :] == data['pr'].values[11, :, :]
            pr_data[p_ind, :, 2, :, :] == data['pr'].values[0, :, :]
        else:
            for lag in range(3):
                pr_data[p_ind, :, lag, :,
                        :] -= data['pr'].values[month + lag - 1, :, :]
        data.close()


###############################################################################
# Read the temperature data for regression.
###############################################################################
tas_data = np.full([len(tas_list_uniq), len(year), 3, len(target_lat),
                    len(target_lon)], np.nan)
for t_ind, t in enumerate(tas_list_uniq):
    if model == 'lsm':
        flist = [t + str(y) + '.nc' for y in year]
    elif model == 'cmip5':
        f_hist = [t + 'historical_r1i1p1_' + str(y) + '.nc' \
                  for y in year_cmip5]
        f_85 = [t + 'rcp85_r1i1p1_' + str(y) + '.nc' \
                for y in range(year_cmip5[-1]+1, year[-1]+1)]
        flist = f_hist + f_85
    elif model == 'cmip6':
        f_hist = [t + 'historical_' + str(y) + '.nc' \
                  for y in year_cmip6]
        f_85 = [t + 'ssp585_' + str(y) + '.nc' \
                for y in range(year_cmip6[-1]+1, year[-1]+1)]
        flist = f_hist + f_85

    tas_ind_list = [i for i,x in enumerate(tas_list) if x == t]

    data = xr.open_mfdataset(flist, decode_times = False)
    if data.lat[0] > -87:
        # Pad to global data
        temp = target_lat < data.lat.values[0]
        if month == 0:
            tas_data[t_ind, 1:, 0, temp, :] = data.tas.values[11:(-1):12, :,
                                                              :].copy()
        else:
            tas_data[t_ind, :, 0, temp, :] = data.tas.values[(month-1)::12,
                                                             :, :].copy()
        tas_data[t_ind, :, 1, temp, :] = data.tas.values[month::12, :, 
                                                         :].copy()
        if month == 11:
            tas_data[t_ind, :(-1), 2, temp, 
                     :] = data.tas.values[(month+1)::12, :, :].copy()
        else:
            tas_data[t_ind, :, 2, temp, :] = data.tas.values[(month+1)::12, :,
                                                             :].copy()
    else:
        # Pad to global data
        temp = target_lat < data.lat.values[0]
        if month == 0:
            tas_data[t_ind, 1:, 0, :, :] = data.tas.values[11:(-1):12, :,
                                                           :].copy()
        else:
            tas_data[t_ind, :, 0, :, :] = data.tas.values[(month-1)::12,
                                                          :, :].copy()
        tas_data[t_ind, :, 1, :, :] = data.tas.values[month::12, :, :].copy()
        if month == 11:
            tas_data[t_ind, :(-1), 2, :, :] = data.tas.values[(month+1)::12,
                                                              :, :].copy()
        else:
            tas_data[t_ind, :, 2, :, :] = data.tas.values[(month+1)::12, :,
                                                          :].copy()
    data.close()

    # Remove the climatology if specified.
    if 'anomaly' in opt:
        data = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                            'em_tas_corr_climatology',
                                            'tas_' + model + '_' + \
                                            met_name_list_uniq[t_ind] + '.nc'))
        if month == 0:
            tas_data[t_ind, :, 0, :, :] -= data['tas'].values[11, :, :]
            tas_data[t_ind, :, 1, :, :] == data['tas'].values[0, :, :]
            tas_data[t_ind, :, 2, :, :] == data['tas'].values[1, :, :]
        elif month == 11:
            tas_data[t_ind, :, 0, :, :] -= data['tas'].values[10, :, :]
            tas_data[t_ind, :, 1, :, :] == data['tas'].values[11, :, :]
            tas_data[t_ind, :, 2, :, :] == data['tas'].values[0, :, :]
        else:
            for lag in range(3):
                tas_data[t_ind, :, lag, :,
                        :] -= data['tas'].values[month + lag - 1, :, :]
        data.close()


###############################################################################
# Read the observed precipitation and temperature data.
###############################################################################
data = xr.open_mfdataset([os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                       land_mask, met_obs_name,
                                       'pr_' + str(y) + '.nc') for y in year],
                         decode_times = False)
pr_obs = data.pr.values[month::12, :, :].copy()
data.close()
if 'anomaly' in opt:
    if model == 'lsm':
        data = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                            'em_tas_corr_climatology',
                                            'pr_CRU_v4.03_1981-2010.nc'))
    else:
        data = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                            'em_tas_corr_climatology',
                                            'pr_CRU_v4.03_1950-2016.nc'))
    pr_obs -= data['pr'].values[month, :, :]
    data.close()


data = xr.open_mfdataset([os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                       land_mask, met_obs_name,
                                       'tas_' + str(y) + '.nc') for y in year],
                         decode_times = False)
tas_obs = data.tas.values[month::12, :, :].copy()
data.close()
if 'anomaly' in opt:
    if model == 'lsm':
        data = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                            'em_tas_corr_climatology',
                                            'tas_CRU_v4.03_1981-2010.nc'))
    else:
        data = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                            'em_tas_corr_climatology',
                                            'tas_CRU_v4.03_1950-2016.nc'))
    tas_obs -= data['tas'].values[month, :, :]
    data.close()


###########################################################################
# Prepare the output variables.
###########################################################################
# Correlation coefficients, p-values, and the 95% confidence intervals
# arranged in the order of: intercept, precipitation, temperature.
corr = np.full([3, len(year), len(target_lat), len(target_lon)],
               np.nan, dtype = float)
corr_p = corr.copy()
corr_CI_lower = corr.copy()
corr_CI_upper = corr.copy()

# Predicted values, using CRU_Merged precipitation as the constraint,
# and the 95% confidence intervals.
predicted = np.full([len(year), len(target_lat), len(target_lon)], 
                    np.nan, dtype = float)
predicted_CI_lower = predicted.copy()
predicted_CI_upper = predicted.copy()

# Indicator whether emergent constraint was applied on this grid.
# First slide is precipitation. Second slice is temperature.
# 0 - applied
# 1 - not applied due to insufficient variability in predictor
# 2 - not applied due to insigificant correlation coefficient
em_not_applied = np.full([2, len(year), len(target_lat), len(target_lon)],
                         np.nan, dtype = float)
for a, b in it.product(range(pr_obs.shape[1]), range(pr_obs.shape[2])):
    if np.isnan(pr_obs[0, a, b]):
        continue

    for t in range(len(year)):
        ##print(str(a) + ', ' + str(b))
        if n_sq_gr == 3:
            if a == 0:
                sub_a = range(a, a+2)
            elif a == (pr_obs.shape[1] - 1):
                sub_a = range(a-1, a+1)
            else:
                sub_a = range(a-1, a+2)
            if b == 0:
                sub_b = range(b, b+2)
            elif b == (pr_obs.shape[2] - 1):
                sub_b = range(b-1, b+1)
            else:
                sub_b = range(b-1, b+2)
            n_valid = len(sub_a) * len(sub_b) * 3 # number lags = 3
            X = np.concatenate([np.ones([pr_data.shape[0] * n_valid, 1]),
                                pr_data[:,t,:,:,
                                        :][:,:,sub_a,
                                           :][:,:,:,sub_b].reshape(-1,1),
                                tas_data[:,t,:,:,
                                         :][:,:,sub_a,
                                            :][:,:,:,sub_b].reshape(-1,1)],
                               axis = 1)
            y = sm_data[:,t,:,:,:][:,:,sub_a,:][:,:,:,sub_b].reshape(-1, 1)
            ymean = np.nanmean( sm_data_mean[t,:,:][sub_a,
                                                    :][:,sub_b].reshape(-1) )
            y95 = np.nanmean( sm_data_p95[t,:,:][sub_a,
                                                 :][:,sub_b].reshape(-1) )
            y05 = np.nanmean( sm_data_p05[t,:,:][sub_a,
                                                 :][:,sub_b].reshape(-1) )
        else:
            X = np.concatenate([np.ones([pr_data.shape[0] * 3,
                                         1]).reshape(-1,1),
                                pr_data[:,t,:,a,b].reshape(-1, 1),
                                tas_data[:,t,:,a,b].reshape(-1, 1)],
                               axis = 1)
            y = sm_data[:,t,:,a,b].reshape(-1, 1)
            ymean = sm_data_mean[t,a,b]
            y95 = sm_data_p95[t,a,b]
            y05 = sm_data_p05[t,a,b]
    
        ###############################
        # Regression cases.
        #
        # Logical structure of this section:
        #Exclude precipitation
        #    Exclude temperature
        #    Can include temperature
        #        Regression w/ temperature does not work
        #            Regression w/ temperature works
        #Can include precipitation
        #    Exclude temperature
        #        Regression w/ precipitation does not work
        #    Regression w/ precipitation works
        #    Can include temperature
        #Both significant
        #At least one not significant
        #Pr-only is significant, and more significant than Tas-only
        #Tas-only is significant, and more significant than Pr-only
        #Neither is significant
        ###############################
        not_nan = ~(np.isnan(X).any(axis = 1) | np.isnan(y.reshape(-1)))
        X = X[not_nan, :]
        y = y[not_nan]

        if np.std(X[:,1]) < 1e-3:
            em_not_applied[0, t, a, b] = 1
            if np.std(X[:,2]) < 1e-3:
                em_not_applied[1, t, a, b] = 1
                # Fill with mean, 5th, and 95th percentiles.
                predicted[t, a, b] = ymean
                predicted_CI_lower[t, a, b] = y05
                predicted_CI_upper[t, a, b] = y95
            else:
                # Try regression, excluding pr.
                mod = stats.OLS(y, X[:, [0,2]])
                result = mod.fit()

                if result.pvalues[1] > 0.05:
                    em_not_applied[1, t, a, b] = 2
                    # Fill with mean, 5th, and 95th percentile.
                    predicted[t, a, b] = ymean
                    predicted_CI_lower[t, a, b] = y05
                    predicted_CI_upper[t, a, b] = y95
                else:
                    em_not_applied[1, t, a, b] = 0
                    # Fill with regression results.
                    CI = result.conf_int()

                    corr[[0,2], t, a, b] = result.params
                    corr_p[[0,2], t, a, b] = result.pvalues
                    corr_CI_lower[[0,2], t, a, b] = CI[:, 0]
                    corr_CI_upper[[0,2], t, a, b] = CI[:, 1]
                    exog = np.array([1, tas_obs[t, a, b]]).reshape(1, 2)
                    predicted[t, a, b] = result.predict(exog)
                    prstd, iv_l, iv_u = wls_prediction_std(result,
                                                           exog = exog)
                    predicted_CI_lower[t, a, b] = iv_l
                    predicted_CI_upper[t, a, b] = iv_u
        else:
            if np.std(X[:,2]) < 1e-3:
                em_not_applied[1, t, a, b] = 1

                # Try regression, excluding tas.
                mod = stats.OLS(y, X[:, [0,1]])
                result = mod.fit()

                if result.pvalues[1] > 0.05:
                    em_not_applied[0, t, a, b] = 2
                    # Fill with mean, 5th, and 95th percentile.
                    predicted[t, a, b] = ymean
                    predicted_CI_lower[t, a, b] = y05
                    predicted_CI_upper[t, a, b] = y95
                else:
                    em_not_applied[0, t, a, b] = 0
                    # Fill with regression result.
                    CI = result.conf_int()

                    corr[[0,1], t, a, b] = result.params
                    corr_p[[0,1], t, a, b] = result.pvalues
                    corr_CI_lower[[0,1], t, a, b] = CI[:, 0]
                    corr_CI_upper[[0,1], t, a, b] = CI[:, 1]

                    exog = np.array([1, pr_obs[t, a, b]]).reshape(1, 2)
                    predicted[t, a, b] = result.predict(exog)
                    prstd, iv_l, iv_u = wls_prediction_std(result,
                                                           exog = exog)
                    predicted_CI_lower[t, a, b] = iv_l
                    predicted_CI_upper[t, a, b] = iv_u
            else:
                # Try regression.
                # (1) Both
                mod = stats.OLS(y, X)
                result = mod.fit()
                pvalues = result.pvalues

                if (pvalues[1] <= 0.05) & (pvalues[2] <= 0.05):
                    em_not_applied[0, t, a, b] = 0
                    em_not_applied[1, t, a, b] = 0

                    # Fill with regression results.
                    CI = result.conf_int()

                    corr[:, t, a, b] = result.params
                    corr_p[:, t, a, b] = pvalues
                    corr_CI_lower[:, t, a, b] = CI[:, 0]
                    corr_CI_upper[:, t, a, b] = CI[:, 1]

                    exog = np.array([1, pr_obs[t, a, b],
                                     tas_obs[t, a, b]]).reshape(1, 3)
                    predicted[t, a, b] = result.predict(exog)
                    prstd, iv_l, iv_u = wls_prediction_std(result,
                                                           exog = exog)
                    predicted_CI_lower[t, a, b] = iv_l
                    predicted_CI_upper[t, a, b] = iv_u
                else:
                    # (2) Pr-only
                    mod2 = stats.OLS(y, X[:, :2])
                    result2 = mod2.fit()
                    pvalues2 = result2.pvalues
                    # (3) Tas-only
                    mod3 = stats.OLS(y, X[:, [0,2]])
                    result3 = mod3.fit()
                    pvalues3 = result3.pvalues

                    pvalues_min = min(pvalues2[1], pvalues3[1])

                    if pvalues_min <= 0.05:
                        if pvalues2[1] < pvalues3[1]:
                            # Fit Pr-only
                            em_not_applied[0, t, a, b] = 0
                            em_not_applied[1, t, a, b] = 2

                            CI = result2.conf_int()

                            corr[[0,1], t, a, b] = result2.params
                            corr_p[[0,1], t, a, b] = pvalues2
                            corr_CI_lower[[0,1], t, a, b] = CI[:, 0]
                            corr_CI_upper[[0,1], t, a, b] = CI[:, 1]

                            exog = np.array([1, pr_obs[t, a, b]] \
                            ).reshape(1, 2)
                            predicted[t, a, b] = result2.predict(exog)
                            prstd, iv_l, iv_u = \
                                wls_prediction_std(result2, exog = exog)
                            predicted_CI_lower[t, a, b] = iv_l
                            predicted_CI_upper[t, a, b] = iv_u
                        else:
                            # Fit Tas-only
                            em_not_applied[0, t, a, b] = 2
                            em_not_applied[1, t, a, b] = 0

                            CI = result3.conf_int()

                            corr[[0,2], t, a, b] = result3.params
                            corr_p[[0,2], t, a, b] = pvalues3
                            corr_CI_lower[[0,2], t, a, b] = CI[:, 0]
                            corr_CI_upper[[0,2], t, a, b] = CI[:, 1]

                            exog = np.array([1, tas_obs[t, a, b]] \
                            ).reshape(1, 2)
                            predicted[t, a, b] = result3.predict(exog)
                            prstd, iv_l, iv_u = \
                                wls_prediction_std(result3, exog = exog)
                            predicted_CI_lower[t, a, b] = iv_l
                            predicted_CI_upper[t, a, b] = iv_u
                    else:
                        # Fill with mean, 5th, and 95th percentile.
                        em_not_applied[0, t, a, b] = 2
                        em_not_applied[1, t, a, b] = 2

                        predicted[t, a, b] = ymean
                        predicted_CI_lower[t, a, b] = y05
                        predicted_CI_upper[t, a, b] = y95

        ###############################
        # If soil moisture < 0. or > 1., set to the minimum or maximum
        # values in the training data.
        ###############################
        if predicted[t, a, b] < 0.:
            predicted_CI_lower[t, a, b] = predicted_CI_lower[t, a, b] - \
                                          predicted[t, a, b] + np.min(y)
            predicted_CI_upper[t, a, b] = predicted_CI_upper[t, a, b] - \
                                          predicted[t, a, b] + np.min(y)
            predicted[t, a, b] = np.min(y)
        if predicted[t, a, b] > 1.:
            predicted_CI_lower[t, a, b] = predicted_CI_lower[t, a, b] - \
                                          predicted[t, a, b] + np.max(y)
            predicted_CI_upper[t, a, b] = predicted_CI_upper[t, a, b] - \
                                          predicted[t, a, b] + np.max(y)
            predicted[t, a, b] = np.max(y)

end = time.time()
print('Total %.6f hours.' % ((end - start)/3600))


###########################################################################
# Save the correlation coefficients, confidence interval, and significance
# to netcdf file.
###########################################################################
path_folder = os.path.join(mg.path_out(), 'em_' + model + '_corr', 
                           met_obs_name + '_' + opt + '_' + dcm + '_' + \
                           year_str)
subperiod = period[period.month == (month+1)]

xr.DataArray(corr, coords = {'reg': range(3), 'time': subperiod,
                             'lat': target_lat, 'lon': target_lon}, 
             dims = ['reg', 'time', 'lat', 'lon']).to_dataset(name = 'corr' \
             ).to_netcdf(os.path.join(path_folder, 
                                      'corr_' + str(month) + '.nc'))
xr.DataArray(corr_p, coords = {'reg': range(3), 'time': subperiod,
                               'lat': target_lat, 'lon': target_lon}, 
             dims = ['reg', 'time', 'lat', 'lon']).to_dataset(name = 'corr_p' \
             ).to_netcdf(os.path.join(path_folder, 
                                      'corr_p_'+str(month)+'.nc'))
xr.DataArray(corr_CI_lower, coords = {'reg': range(3), 'time': subperiod,
                                      'lat': target_lat, 'lon': target_lon},
             dims = ['reg', 'time', 'lat',
                     'lon']).to_dataset(name = 'corr_CI_lower' \
             ).to_netcdf(os.path.join(path_folder, 'corr_CI_lower_' + \
                                      str(month) + '.nc'))
xr.DataArray(corr_CI_upper, coords = {'reg': range(3), 'time': subperiod,
                                      'lat': target_lat, 'lon': target_lon},
             dims = ['reg', 'time', 'lat',
                     'lon']).to_dataset(name = 'corr_CI_upper'\
             ).to_netcdf(os.path.join(path_folder, 'corr_CI_upper_' + \
                                      str(month) + '.nc'))
xr.DataArray(em_not_applied, coords = {'reg': range(1,3), 'time': subperiod,
                                       'lat': target_lat, 'lon': target_lon}, 
             dims = ['reg', 'time', 'lat',
                     'lon']).to_dataset(name = 'em_not_applied'\
            ).to_netcdf(os.path.join(path_folder, 'em_not_applied_' + \
                                     str(month) + '.nc'))
xr.DataArray(predicted, coords = {'time': subperiod, 
                                  'lat': target_lat, 'lon': target_lon}, 
             dims = ['time', 'lat', 'lon']).to_dataset(name = 'predicted' \
             ).to_netcdf(os.path.join(path_folder, 
                                      'predicted_' + str(month) + '.nc'))
xr.DataArray(predicted_CI_lower, coords = {'time': subperiod, 
                                           'lat': target_lat, 
                                           'lon': target_lon}, 
             dims = ['time', 'lat', 'lon'] \
).to_dataset(name = 'predicted_CI_lower' \
).to_netcdf(os.path.join(path_folder, 
                         'predicted_CI_lower_' + str(month) + '.nc'))
xr.DataArray(predicted_CI_upper, coords = {'time': subperiod,
                                           'lat': target_lat, 
                                           'lon': target_lon}, 
             dims = ['time', 'lat', 'lon'] \
).to_dataset(name = 'predicted_CI_upper' \
).to_netcdf(os.path.join(path_folder,
                         'predicted_CI_upper_' + str(month) + '.nc'))


end2 = time.time()
print('Total %.6f hours saving results.' % ((end2 - end)/3600))

# 20191123
# Total 0.914617 hours.
# Total 0.000412 hours saving results.
