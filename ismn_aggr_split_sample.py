# -*- coding: utf-8 -*-
"""
Created on Thu May 23 20:24:43 2019

@author: ywang254

Divide the aggregated ISMN station-level data into calibration and validation
    subsets.
"""
import pandas as pd
import utils_management as mg
from utils_management.constants import depth
from misc.ismn_utils import get_ismn_aggr_method
import os
import numpy as np
import random


# Choose the weighting method that was used to generate the weighted averaged
# ISMN data.
##from ismn_aggr_dolce import simple, dominance_lc, dominance_threshold
simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = [get_ismn_aggr_method(simple[k], dominance_lc[k], \
                                         dominance_threshold) for k in \
                    range(len(simple))]


# Percent data to use for validation.
pct_validation = 0.4


random.seed(999, version=2)


for d in depth:
    for i in ismn_aggr_method:
        data = pd.read_csv(os.path.join(mg.path_intrim_out(), 
                           'ismn_aggr', 'weighted_monthly_data_' + \
                           i + '_' + d + '.csv'), index_col=0, 
                           header=[0,1,2])


        # Cast random points.
        mask, = np.where((~np.isnan(data.values)).reshape(-1))
        mask = list(mask)
        ind_calibration = random.sample(mask, int(len(mask) * \
                                                 (1 - pct_validation)))


        # Unpack the calibration data.
        mask_calibration = np.empty_like(data.values)
        mask_calibration[:,:] = np.nan
        a, b = np.unravel_index(ind_calibration, mask_calibration.shape)
        for ia in range(len(a)):
            mask_calibration[a[ia], b[ia]] = 1.
        
        mask_validation = np.empty_like(data.values)
        mask_validation[:,:] = np.nan
        mask_validation[np.isnan(mask_calibration)] = 1.


        # 
        data_calibration = data * mask_calibration
        data_validation = data * mask_validation
        
        #
        data_calibration.to_csv(os.path.join(mg.path_intrim_out(), 
            'ismn_aggr', 'cal_weighted_monthly_data_'+i+'_'+d+'.csv'))
        data_validation.to_csv(os.path.join(mg.path_intrim_out(), 
            'ismn_aggr', 'val_weighted_monthly_data_'+i+'_'+d+'.csv'))
