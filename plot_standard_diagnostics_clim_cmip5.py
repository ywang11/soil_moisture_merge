"""
2019/07/05

ywang254@utk.edu

Map the global mean and seasonal mean soil moisture for the individual
  CMIP5 models.
"""
from utils_management.constants import year_longest, depth_cm, cmip5
import utils_management as mg
from misc.plot_standard_diagnostics_utils import plot_map
import numpy as np
import os
import multiprocessing as mp


levels = np.linspace(0., 0.6, 15)
cmap = 'coolwarm_r'
year = year_longest

def plotter(l):
    plot_map(depth_cm, os.path.join(mg.path_out(), 
                                    'standard_diagnostics_cmip5', l), 
             os.path.join(mg.path_out(), 'standard_diagnostics_plot', 
                          'mean_cmip5', l), 'map', levels, cmap)

# debug
##plotter(cmip5[0])

pool = mp.Pool(mp.cpu_count() - 1)
pool.map_async(plotter, cmip5)
pool.close()
pool.join()
