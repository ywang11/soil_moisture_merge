"""
20190923
ywang254@utk.edu

Concatenate the four time periods of the CDF-matching rescaled EM-LSM
 uncertainty products to produce a consecutive product.
"""
import xarray as xr
import os
import pandas as pd
import numpy as np
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth_cm, target_lat, target_lon
import utils_management as mg
from misc.ismn_utils import get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
import itertools as it
import multiprocessing as mp


args_list = []
for prod, dcm in it.product(['lsm', 'all'], depth_cm):
    args_list.append([prod, dcm])


for args in args_list:
    prod, dcm = args

    time_range = pd.date_range(str(year_longest[0]) + '-01-01',
                               str(year_longest[-1]) + '-12-31', freq = 'MS')

    concat = xr.DataArray(data = np.empty([len(time_range), 
                                           len(target_lat), len(target_lon)]),
                          dims = ['time', 'lat', 'lon'],
                          coords = {'time': time_range, 
                                    'lat': target_lat, 'lon': target_lon})

    time_segment = [pd.date_range('1950-01-01', '1980-12-31', freq = 'MS'),
                    pd.date_range('1981-01-01', '2010-12-31', freq = 'MS'),
                    pd.date_range('2011-01-01', '2016-12-31', freq = 'MS')]

    for year, ts in zip([year_shorter, year_shortest, year_shorter2], 
                        time_segment):
        if (year[0] == 1981) & (year[-1] == 2016):
            data = xr.open_mfdataset([os.path.join(mg.path_out(),
                                                   'em_' + prod + '_corr',
                'CRU_v4.03_year_month_anomaly_9grid_' + dcm + '_' + \
                str(year[0]) + '-' + str(year[-1]),
                                                   'predicted_std_' + \
                                                   str(yy) + '.nc') \
                                      for yy in year],
                                     decode_times = False, concat_dim = 'time')
        else:
            data = xr.open_dataset(os.path.join(mg.path_out(),
                                                'concat_em_' + prod,
                                                'uncertainty_scaled_' + \
                                                'CRU_v4.03_year_month_' + \
                                                'anomaly_9grid_' + dcm + \
                                                '_' + str(year[0]) + '-' + \
                                                str(year[-1]) + '.nc'),
                                   decode_times = False)
        data['time'] = pd.date_range(str(year[0]) + '-01-01',
                                     str(year[-1]) + '-12-31', freq = 'MS')
        concat.loc[ts, :, :] = data['predicted_std'].loc[ts,
                                                         :, :].values.copy()
        data.close()

    print(args)
    concat.to_dataset(name = 'predicted_std' \
    ).to_netcdf(os.path.join(mg.path_out(), 'concat_em_' + prod,
                             'concat_uncertainty_CRU_v4.03_year_month_' + \
                             'anomaly_9grid_' + dcm + '_' + \
                             str(year_longest[0]) + '-' + \
                             str(year_longest[-1]) + '.nc'))
