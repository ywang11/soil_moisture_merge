"""
2020/03/13

ywang254@utk.edu

Calculate the linear trend in the annual time series, and in the per month
  time series by latitude band during the full period for each land surface 
  model & CMIP5 & CMIP6 models, and the final product.
"""
import pandas as pd
from utils_management.constants import depth, depth_cm, year_longest, \
    year_shortest, lsm_list
import utils_management as mg
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability, one_layer_availability
from misc.other_diagnostics import get_bylat_trend
import itertools as it
import os
import numpy as np


year_str0 = '1950-2016'

# Use the common era
year = year_shortest
year_str = str(year[0]) + '-' + str(year[-1])
time_range = pd.date_range(start = str(year[0])+'-01-01',
                           end = str(year[-1])+'-12-31', freq = 'MS')

# Mean & Median
path_prefix = os.path.join(mg.path_out(), 
                           'standard_diagnostics_meanmedian_lsm',
                           'mean_' + year_str0)
for d in depth:
    trend, trend_p = get_bylat_trend(path_prefix, d, time_range)
    trend.to_csv(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                              'bylat_trend', 'mean_lsm_' + d + '_' + \
                              year_str + '.csv'))
    trend_p.to_csv(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                                'bylat_trend', 'p_mean_lsm' + d + '_' + \
                                year_str + '.csv'))

# DOLCE-LSM/DOLCE-ALL, concatenated
best = 'lu_weighted_ShrunkCovariance'
model_set = 'lsm'
path_prefix = os.path.join(mg.path_out(), 'standard_diagnostics_concat',
                           'dolce_' + model_set, 
                           'dolce_average_' + best)
for d in depth:
    trend, trend_p = get_bylat_trend(path_prefix, d, time_range)
    trend.to_csv(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                              'bylat_trend', 'dolce_' + model_set + '_' + \
                              d + '_' + year_str + '.csv'))
    trend_p.to_csv(os.path.join(mg.path_out(), 'standard_diagnostics_plot', 
                                'bylat_trend', 'p_dolce_' + model_set + '_' + \
                                d + '_' + year_str + '.csv'))

# Emergent Constraint
best = 'CRU_v4.03_predicted_year_month_anomaly_9grid'
for model_set in ['cmip5', 'cmip6', '2cmip']:
    path_prefix = os.path.join(mg.path_out(),
                               'standard_diagnostics_em_' + model_set,
                               best + '_' + year_str0)
    for d in depth:
        trend, trend_p = get_bylat_trend(path_prefix, d, time_range)
        trend.to_csv(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                                  'bylat_trend', 'em_' + model_set + '_' + \
                                  d + '_' + year_str + '.csv'))
        trend_p.to_csv(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                                    'bylat_trend', 'p_em_' + model_set + \
                                    '_' + d + '_' + year_str + '.csv'))

# Emergent Constraint-LSM/-ALL, concatenated
best = 'CRU_v4.03_year_month_anomaly_9grid_predicted'
for model_set in ['lsm', 'all']:
    path_prefix = os.path.join(mg.path_out(), 'standard_diagnostics_concat',
                               'em_' + model_set, best)
    for d in depth:
        trend, trend_p = get_bylat_trend(path_prefix, d, time_range)
        trend.to_csv(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                                  'bylat_trend', 'em_' + model_set + '_' + \
                                  d + '_' + year_str + '.csv'))
        trend_p.to_csv(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                                    'bylat_trend', 'p_em_' + model_set + '_' \
                                    + d + '_' + year_str + '.csv'))

# Individual LSMs
l = 'GLEAMv3.3a'
for d in depth[:1]:
    path_prefix = os.path.join(mg.path_out(),
                               'standard_diagnostics_lsm', l)
    trend, trend_p = get_bylat_trend(path_prefix, d, time_range)
    trend.to_csv(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                              'bylat_trend', l + '_' + d + \
                              '_' + year_str + '.csv'))
    trend_p.to_csv(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                                'bylat_trend', 'p_' + l + '_' + d + \
                                '_' + year_str + '.csv'))
l = 'ERA-Land'
for d in depth[:2]:
    path_prefix = os.path.join(mg.path_out(),
                               'standard_diagnostics_lsm', l)
    trend, trend_p = get_bylat_trend(path_prefix, d, time_range)
    trend.to_csv(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                              'bylat_trend', l + '_' + d + \
                              '_' + year_str + '.csv'))
    trend_p.to_csv(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                              'bylat_trend', 'p_' + l + '_' + d + \
                              '_' + year_str + '.csv'))
