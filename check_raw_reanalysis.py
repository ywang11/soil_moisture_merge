#Examine the original data of
# ---- GLDAS_CLM
# ---- GLDAS_VIC
# ---- ERA-Interim
# ---- ERA-Land
import xarray as xr
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import utils_management as mg
import os
import glob
##import iris

cmap = cm.get_cmap('Spectral')


###############################################################################
# (1) Plot the global time series of annual mean precipitation, with comparison
#     to CRU. Unit: conform to mm/day
##############################################################################
pr_collect = pd.DataFrame(data = np.nan, index = pd.date_range('1990-01-01', 
                                                               '2016-12-31',
                                                               freq = 'MS'),
                          columns = ['CRU', 'GLDAS_CLM', 'GLDAS_VIC', 
                                     'ERA-Interim', 'CFSR']) # 'ERA-Land'
###########
# CRU
###########
flist = [os.path.join(mg.path_data(), 'CRU_v4.03',
                      'cru_ts4.03.1981.1990.pre.dat.nc'),
         os.path.join(mg.path_data(), 'CRU_v4.03', 
                      'cru_ts4.03.1991.2000.pre.dat.nc'),
         os.path.join(mg.path_data(), 'CRU_v4.03',
                      'cru_ts4.03.2001.2010.pre.dat.nc'),
         os.path.join(mg.path_data(), 'CRU_v4.03',
                      'cru_ts4.03.2011.2018.pre.dat.nc')]
data = xr.open_mfdataset(flist)
pre = data['pre'].loc[(data['time'].indexes['time'].year >= 1990) & \
                      (data['time'].indexes['time'].year <= 2016), :, :]
# (weighted by cosine of latitude)
wt = np.broadcast_to(np.cos(data.lat.values / 180 * np.pi).reshape(1, -1, 1),
                     pre.shape)
wt = np.ma.masked_array(wt, mask = np.isnan(pre))
# ---- convert from mm/month to mm/day
Rainf = (pre * wt / wt.mean()).mean(dim = 'lat').mean(dim = 'lon')
Rainf.load()
dom = [31, 28.25, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
for m in range(12):
    Rainf[m::12] = Rainf.values[m::12] / dom[m]
pr_collect.loc[:, 'CRU'] = Rainf.values
data.close()
###########
# GLDAS_CLM
###########
flist = [os.path.join(mg.path_data(), 'GLDAS', 'CLM10_M_V001_pr', 
                      'GLDAS_CLM10_M.A' + \
                      str(y) + ('%02d' % m) + '.001.grb.SUB.nc4') \
         for y in range(1990,2017) for m in range(1,13)]
data = xr.open_mfdataset(flist)
# (weighted by cosine of latitude)
wt = np.broadcast_to(np.cos(data.lat.values / 180 * np.pi).reshape(1, -1, 1), 
                     data.Rainf.shape)
wt = np.ma.masked_array(wt, mask = np.isnan(data['Rainf']))
# ---- convert from kg/m^2/s to mm/day
Rainf = ((data.Rainf + data.Snowf) * wt / wt.mean() \
).mean(dim = 'lat').mean(dim = 'lon') * 86400
pr_collect.loc[:, 'GLDAS_CLM'] = Rainf.values
data.close()
###########
# GLDAS_VIC
###########
flist = [os.path.join(mg.path_data(), 'GLDAS', 'VIC10_M.001_pr',
                      'GLDAS_VIC10_M.A' + \
                      str(y) + ('%02d' % m) + '.001.grb.SUB.nc4') \
         for y in range(1990,2017) for m in range(1,13)]
data = xr.open_mfdataset(flist)
# (weighted by cosine of latitude)
wt = np.broadcast_to(np.cos(data.lat.values / 180 * np.pi).reshape(1, -1, 1),
                     data.Rainf.shape)
wt = np.ma.masked_array(wt, mask = np.isnan(data['Rainf']))
# ---- convert from kg/m^2/s to mm/day
Rainf = ((data.Rainf + data.Snowf) * wt / \
         wt.mean()).mean(dim = 'lat').mean(dim = 'lon') * 86400
pr_collect.loc[:, 'GLDAS_VIC'] = Rainf.values
data.close()
###########
# ERA-Interim
###########
# Need a land mask from soil moisture
data0 = xr.open_dataset(os.path.join(mg.path_data(), 'Reanalysis', 
                                     'ERA-Interim', 'soil_moisture_2018.nc'))
land = data0.swvl1.values[0, :, :] < 0

#
data = xr.open_mfdataset([os.path.join(mg.path_data(), 'Reanalysis', 
                                       'ERA-Interim',
                          'precipitation_' + str(y) + '.nc') \
                          for y in range(1990,2017)], concat_dim = 'time')
# (weighted by cosine of latitude)
wt = np.broadcast_to(np.cos(data.latitude.values / 180 * np.pi).reshape(-1, 1),
                     data.tp[0::3, :, :].shape)
wt = np.ma.masked_array(wt, mask = np.broadcast_to(land.reshape(-1, 241, 480),
                                                   data.tp[0::3, :, :].shape))
# ---- convert from m to mm/day
Rainf = ( (data.tp.values[0::3, :, :] + data.tp.values[1::3, :, :] \
           + data.tp.values[2::3, :, :]) / 3 * \
          wt / wt.mean() ).mean(axis = 2).mean(axis = 1) * 1000
pr_collect.loc[:, 'ERA-Interim'] = Rainf

data.close()
data0.close()
###########
# ERA-Land
###########
##pr_collect.loc[:, 'ERA-Land'] = Rainf

###########
# CFSR
###########
data0 = xr.open_dataset(os.path.join(mg.path_intrim_out(), 
                                     'land_mask_minimum', 'merge_vanilla.nc'))
land = data0.mask.values.copy()

#
flist = [os.path.join(mg.path_data(), 'Reanalysis', 'CFSR', 
                      'pgbh06.gdas.' + str(y) + '.grb2.nc') \
         for y in range(1990, 2011)]
data = xr.open_mfdataset(flist, concat_dim = 'time')

pre = (data.A_PCP_L1_AccumAvg[:, 1:, :] + \
       data.A_PCP_L1_AccumAvg[:, :-1, :].values) / 2

# (weighted by cosine of latitude)
wt = np.broadcast_to(np.cos(data0.lat.values / 180 * np.pi).reshape(1, -1, 1),
                     pre.shape)
wt = np.ma.masked_array(wt, mask = np.broadcast_to(land.reshape(-1, 360, 720),
                                                   pre.shape))
# ---- convert from 6-hour accumulation mm/month to mm/day
Rainf = ( pre.values * wt / wt.mean() ).mean(axis = 2).mean(axis = 1) * 4
pr_collect.loc[pr_collect.index.year <= 2010, 'CFSR'] = Rainf
data.close()
data0.close()

pr_collect.to_csv('PR_COLLECT.csv')


# (2) Plot the time series of global annual mean soil moisture
###########
# GLDAS_CLM
###########
depth_bnds = np.array([[0,0.018], [0.018,0.045], [0.045,0.091], [0.091,0.166],
                       [0.166,0.289], [0.289,0.493], [0.493,0.829],
                       [0.829,1.383], [1.383,2.296], [2.296,3.433]])

flist = [os.path.join(mg.path_data(), 'GLDAS', 'CLM10_M_V001', 
                      'GLDAS_CLM10_M.A' + \
                      str(y) + ('%02d' % m) + '.001.grb.SUB.nc4') \
         for y in range(1990,2017) for m in range(1,13)]
data = xr.open_mfdataset(flist)
# (weighted by cosine of latitude)
wt = np.broadcast_to(np.cos(data.lat.values / 180 * np.pi).reshape(1, -1, 1), 
                     [data.SoilMoist.shape[0], data.SoilMoist.shape[2],
                      data.SoilMoist.shape[3]])
wt = np.ma.masked_array(wt, mask = np.isnan(data['SoilMoist'][:, 0, :, :]))
# ---- convert from kg/m^2 to fraction
sm = data.SoilMoist
sm_collect = pd.DataFrame(data = np.nan, index = data['time'].values,
                          columns = range(10))
for d in range(data.SoilMoist.shape[1]):
    temp = (sm[:, d, :, :] * wt / wt.mean() \
    ).mean(dim = 'lat').mean(dim = 'lon')
    sm_collect.loc[:, d] = temp / (depth_bnds[d,1] - depth_bnds[d,0]) / 1000
sm_collect.to_csv('GLDAS_CLM.csv')
data.close()
###########
# GLDAS_VIC
###########
flist = [os.path.join(mg.path_data(), 'GLDAS', 'VIC10_M.001', 
                      'GLDAS_VIC10_M.A' + str(y) + ('%02d' % m) + \
                      '.001.grb.SUB.nc4') for y in range(1990,2017) \
         for m in range(1,13)]
data = xr.open_mfdataset(flist)
# (weighted by cosine of latitude)
wt = np.broadcast_to(np.cos(data.lat.values / 180 * np.pi).reshape(1, -1, 1),
                     [data.SoilMoist.shape[0], data.SoilMoist.shape[2],
                      data.SoilMoist.shape[3]])
wt = np.ma.masked_array(wt, mask = np.isnan(data['SoilMoist'][:, 0, :, :]))
# ---- convert from kg/m^2 to fraction
sm = data.SoilMoist
d = 0 # Only the first layer is 10 centimeters.
sm = (sm[:, d, :, :] * wt / wt.mean()).mean(dim = 'lat').mean(dim = 'lon')
sm = sm / 0.1 / 1000
pd.DataFrame(sm, index = data['time'].values,
             columns = ['Soil Moisture (m^3/m^3)']).to_csv('GLDAS_VIC.csv')
data.close()
###########
# ERA-Interim
###########
era_interim = pd.DataFrame(data = np.nan, index = pd.date_range('1990-01-01',
                                                                '2016-12-31',
                                                                freq = 'MS'),
                           columns = ['swvl1 (m^3/m^3)', 'swvl2 (m^3/m^3)',
                                      'swvl3 (m^3/m^3)', 'swvl4 (m^3/m^3)'])

flist = [os.path.join(mg.path_data(), 'Reanalysis', 'ERA-Interim',
                      'soil_moisture_' + \
                      str(y) + '.nc') for y in range(1990,2017)]
data = xr.open_mfdataset(flist)
# (weighted by cosine of latitude)
wt = np.broadcast_to(np.cos(data.latitude.values / 180 * np.pi \
                        ).reshape(1, -1, 1), data.swvl1.shape)
wt = np.ma.masked_array(wt, 
                        mask = np.broadcast_to(data['swvl1'].values[0, :, :] \
                                               < 1e-16, data.swvl1.shape))
wt = np.clip(wt, 0., 1.)
for sm_ind, sm in enumerate(['swvl1', 'swvl2', 'swvl3', 'swvl4']):
    sm2 = data[sm].where(data[sm][0, :, :] > 0) * wt / wt.mean()
    era_interim.loc[:, sm + ' (m^3/m^3)'] = \
        sm2.mean(dim = 'latitude').mean(dim = 'longitude').values
era_interim.to_csv('ERA-Interim.csv')
data.close()


###########
# ERA-Land
###########
#era_land = pd.DataFrame(data = np.nan, index = pd.date_range('1990-01-01', '2016-12-31', freq = 'MS'),
#                        columns = ['swvl1 (m^3/m^3)', 'swvl2 (m^3/m^3)', 'swvl3 (m^3/m^3)', 'swvl4 (m^3/m^3)'])
#flist = [os.path.join(mg.path_data(), 'Reanalysis', 'ERA-Land', str(y) + '.gr') for y in range(1990,2017)]
#for f in flist:
#    data = iris.load(f)


##########
# CFSR
##########
cfsr = pd.DataFrame(data = np.nan, index = pd.date_range('1990-01-01',
                                                         '2016-12-31',
                                                         freq = 'MS'),
                    columns = range(4))

flist = [os.path.join(mg.path_data(), 'Reanalysis', 'CFSR',
                      'flxf03.gdas.' + str(y) + '.grb2.nc') \
         for y in range(1990,2011)]
data = xr.open_mfdataset(flist)
# (weighted by cosine of latitude)
wt = np.broadcast_to(np.cos(data.lat.values / 180 * np.pi \
                        ).reshape(1, -1, 1), [252, 576, 1152])
wt = np.ma.masked_array(wt, mask = np.isnan(data.SOILW_Y106_Avg[:,0,:,:]))
wt = np.clip(wt, 0., 1.)
for i in range(4):
    sm = data['SOILW_Y106_Avg'][:, i, :, :] * wt / wt.mean()
    cfsr.loc[cfsr.index.year <= 2010, 
             i] = sm.mean(dim = 'lat').mean(dim = 'lon').values
cfsr.to_csv('CFSR.csv')
data.close()


###############################################################################
# Plotting
###############################################################################
fig, axes = plt.subplots(nrows = 5, ncols = 1, figsize = (8, 15))
fig.subplots_adjust(hspace = 0.3)
pr = pd.read_csv('PR_COLLECT.csv', index_col = 0, parse_dates = True)
pr = pr.groupby(pr.index.year).mean()
for c_ind, c in enumerate(pr.columns):
    axes[0].plot(pr.index, pr.loc[:, c].values, label = c,
                 color = cmap(c_ind / len(pr.columns)))
axes[0].legend()
axes[0].set_title('Precipitation')
for c_ind, c in enumerate(['GLDAS_CLM', 'GLDAS_VIC', 'ERA-Interim', 
                           'CFSR'], 1):
    sm = pd.read_csv(c + '.csv', index_col = 0, parse_dates = True)
    sm = sm.groupby(sm.index.year).mean()
    for r_ind in range(sm.shape[1]):
        axes[c_ind].plot(sm.index, sm.values[:, r_ind], label = str(r_ind))
    axes[c_ind].set_title(c)
    axes[c_ind].legend(ncol = 4)
    axes[c_ind].set_xlim([1989, 2017])
fig.savefig('pr_sm_collect.png', dpi = 600.)
plt.close()
