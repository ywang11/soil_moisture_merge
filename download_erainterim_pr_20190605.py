# -*- coding: utf-8 -*-
"""
Created on Wed Feb 27 21:15:31 2019

@author: ywang254
"""
from ecmwfapi import ECMWFDataServer
import utils_management as mg
import os
from time import sleep
import sys


if sys.platform == 'linux':
    os.system('export ECMWF_API_URL="https://api.ecmwf.int/v1"')
    os.system('export ECMWF_API_KEY="f93636d89d23e3990244d2486f6fdbcb"')
    os.system('export ECMWF_API_EMAIL="ywang254@utk.edu"')


def retrieve(yy):
    server = ECMWFDataServer(url="https://api.ecmwf.int/v1",
                             key="f93636d89d23e3990244d2486f6fdbcb",
                             email="ywang254@utk.edu")
    server.retrieve({
        "class": "ei",
        "dataset": "interim",
        "date": str(yy)+"0101/"+str(yy)+"0201/"+str(yy)+"0301/"+str(yy)+ \
                "0401/"+str(yy)+"0501/"+str(yy)+"0601/"+str(yy)+"0701/"+ \
                str(yy)+"0801/"+str(yy)+"0901/"+str(yy)+"1001/"+ \
                str(yy)+"1101/"+str(yy)+"1201",
        "expver": "1",
        "grid": "0.75/0.75",
        "levtype": "sfc",
        "param": "228.128",
        "step": "0-12/12-24/24-36",
        "stream": "mdfa",
        "type": "fc",
        "target": os.path.join(mg.path_data(), "Reanalysis", "ERA-Interim", 
                               "precipitation_"+str(yy)+".nc"),
    })


for yy in range(1979, 2019):
    retrieve(yy)
    sleep(1)
