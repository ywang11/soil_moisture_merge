"""
20191104
ywang254@utk.edu

Get the unique models in a folder.
"""
import os
from glob import glob
import numpy as np


# MODIFY
path = '/lustre/haven/user/ywang254/Soil_Moisture/data/CMIP5/rcp85/tas'

if 'CMIP6' in path:
    flist = glob(path + '/*/*r1i1p1f1*.nc')
elif 'CMIP5' in path:
    flist = glob(path + '/*r1i1p1*.nc')

print('\n'.join(np.unique([x.split('/')[-1].split('_')[2] for x in flist])))
