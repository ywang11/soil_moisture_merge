"""
2019/07/05

ywang254@utk.edu

Map the global mean and seasonal mean soil moisture for the individual
land surface models, CMIP5, and CMIP6 models. 1950-2016.
"""
from utils_management.constants import year_longest, depth, lsm_list, \
    depth_cm
import utils_management as mg
from misc.plot_standard_diagnostics_utils import plot_trend_map
import numpy as np
import os
import itertools as it
import matplotlib as mpl
from misc.plot_utils import cmap_gen
import multiprocessing as mp
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability


levels = np.linspace(-0.0004, 0.0004, 21)
cmap = cmap_gen('autumn', 'winter_r')
land_mask = 'vanilla'


def plotter(option):
    model_set, model, ddd = option

    prefix_in = os.path.join(mg.path_out(), 'other_diagnostics', model_set,
                             model + '_')
    prefix_out = os.path.join(mg.path_out(), 'other_diagnostics_plot', 
                              'variant_trend', model_set, model)

    plot_trend_map(ddd, prefix_in, prefix_out, levels, cmap)


for model_set in ['lsm', 'cmip5', 'cmip6']:
    if model_set == 'lsm':
        model_list = lsm_list[(str(year_longest[0]) + '-' + \
                               str(year_longest[-1]), depth[0])]
        ddd = depth
    elif model_set == 'cmip5':
        model_list = cmip5_availability(depth_cm[0], land_mask)
        ddd = depth
    else:
        cmip6_list_1 = mrsol_availability(depth_cm[0], land_mask, 'historical')
        cmip6_list_2 = mrsol_availability(depth_cm[0], land_mask, 'ssp585')
        cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) )
        model_list = [x for x in cmip6_list if 'r1i1p1f1' in x]
        ddd = depth

    p = mp.Pool(4)
    p.map_async(plotter, [(model_set, model, ddd) for model in model_list])
    p.close()
    p.join()
