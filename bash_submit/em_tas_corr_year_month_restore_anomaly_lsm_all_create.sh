create=0
if [ ${create} -eq 1 ]
then
    for year in year_longest year_shorter year_shorter2 year_shortest
    do
	for i in {0..3}
	do
	    m=lsm
	    k=year_month_anomaly_9grid
	    cat em_tas_corr_year_month_restore_anomaly_template.sh | sed s/REPLACE1/${year}/ | sed s/REPLACE2/${i}/ | sed s/REPLACE3/${m}/ | sed s/REPLACE4/${k}/ > em_tas_corr_year_month_restore_anomaly_${year}_${i}_${m}_${k}.sh
	    cat ../em_tas_corr_year_month_restore_anomaly.py | sed s/REPLACE1/${year}/ | sed s/REPLACE2/${i}/ | sed s/REPLACE3/${m}/ | sed s/REPLACE4/${k}/ > ../em_tas_corr_year_month_restore_anomaly_${year}_${i}_${m}_${k}.py

	    m=all
	    for k in year_month_anomaly_9grid # year_month_anomaly_1grid
	    do
		cat em_tas_corr_year_month_restore_anomaly_template.sh | sed s/REPLACE1/${year}/ | sed s/REPLACE2/${i}/ | sed s/REPLACE3/${m}/ | sed s/REPLACE4/${k}/ > em_tas_corr_year_month_restore_anomaly_${year}_${i}_${m}_${k}.sh
		cat ../em_tas_corr_year_month_restore_anomaly.py | sed s/REPLACE1/${year}/ | sed s/REPLACE2/${i}/ | sed s/REPLACE3/${m}/ | sed s/REPLACE4/${k}/ > ../em_tas_corr_year_month_restore_anomaly_${year}_${i}_${m}_${k}.py
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for year in year_longest year_shorter year_shorter2 year_shortest
    do
	for i in {0..3}
	do
	    m=lsm
	    k=year_month_anomaly_9grid
	    rm em_tas_corr_year_month_restore_anomaly_${year}_${i}_${m}_${k}.sh
	    rm ../em_tas_corr_year_month_restore_anomaly_${year}_${i}_${m}_${k}.py

	    m=all
	    for k in year_month_anomaly_9grid # year_month_anomaly_1grid
	    do
		rm em_tas_corr_year_month_restore_anomaly_${year}_${i}_${m}_${k}.sh
		rm ../em_tas_corr_year_month_restore_anomaly_${year}_${i}_${m}_${k}.py
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for year in year_longest year_shorter year_shorter2 year_shortest
    do
        for i in {0..3}
        do
	    m=lsm
	    k=year_month_anomaly_9grid
	    qsub em_tas_corr_year_month_restore_anomaly_${year}_${i}_${m}_${k}.sh

	    m=all
	    for k in year_month_anomaly_9grid #  year_month_anomaly_1grid
	    do
		qsub em_tas_corr_year_month_restore_anomaly_${year}_${i}_${m}_${k}.sh
	    done
        done
    done
fi
