#!/bin/bash
#SBATCH -A ccsi
#SBATCH -p batch
#SBATCH --mem=64g
#SBATCH -N 1
#SBATCH -n 16
#SBATCH -t 08:00:00
#SBATCH -o validate_seasonality.o%j
#SBATCH -e validate_seasonality.e%j

cd ~/Git/soil_moisture_merge

ipython -i validate_seasonality.py
