create=0
if [ ${create} -eq 1 ]
then
    for d in {0..3}
    do
	for cov in {0..4}
	do
	    for iam in {0..2}
	    do
		cat standard_diagnostics_dolce_cmip5_template.sh | sed s/REPLACE1/${d}/ | sed s/REPLACE2/${cov}/ | sed s/REPLACE3/${iam}/ > standard_diagnostics_dolce_cmip5_${d}_${cov}_${iam}.sh
		cat ../standard_diagnostics_dolce_cmip5.py | sed s/REPLACE1/${d}/ | sed s/REPLACE2/${cov}/ | sed s/REPLACE3/${iam}/ > ../standard_diagnostics_dolce_cmip5_${d}_${cov}_${iam}.py
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for d in {0..3}
    do
	for cov in {0..4}
	do
	    for iam in {0..2}
	    do
		rm standard_diagnostics_dolce_cmip5_${d}_${cov}_${iam}.sh
		rm ../standard_diagnostics_dolce_cmip5_${d}_${cov}_${iam}.py
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for d in {0..3}
    do
	for cov in {0..4}
	do
	    for iam in {0..2}
	    do
		qsub standard_diagnostics_dolce_cmip5_${d}_${cov}_${iam}.sh
	    done
	done
    done
fi
