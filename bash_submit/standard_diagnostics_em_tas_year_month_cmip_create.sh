create=0
if [ ${create} -eq 1 ]
then
    for d in {0..3}
    do
	for m in 2cmip cmip5 cmip6
	do
	    for opt in year_month_9grid year_month_anomaly_9grid # year_month_1grid year_month_anomaly_1grid
	    do
		cat standard_diagnostics_em_tas_year_month_cmip_template.sh | sed s/REPLACE2/${d}/ | sed s/REPLACE3/${m}/ | sed s/REPLACE4/${opt}/ > standard_diagnostics_em_tas_year_month_${d}_${m}_${opt}.sh
		cat ../standard_diagnostics_em_tas_year_month.py | sed s/REPLACE1/year_longest/ | sed s/REPLACE2/${d}/ | sed s/REPLACE3/${m}/ | sed s/REPLACE4/${opt}/ > ../standard_diagnostics_em_tas_year_month_${d}_${m}_${opt}.py
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for d in {0..3}
    do
	for m in 2cmip cmip5 cmip6
	do
	    for opt in year_month_9grid year_month_anomaly_9grid # year_month_1grid year_month_anomaly_1grid
	    do
		rm standard_diagnostics_em_tas_year_month_${d}_${m}_${opt}.sh
		rm ../standard_diagnostics_em_tas_year_month_${d}_${m}_${opt}.py
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for d in {0..3}
    do
	for m in 2cmip cmip5 cmip6
	do
	    for opt in year_month_9grid year_month_anomaly_9grid # year_month_1grid year_month_anomaly_1grid
	    do
		qsub standard_diagnostics_em_tas_year_month_${d}_${m}_${opt}.sh
	    done
	done
    done
fi
