create=1
if [ ${create} -eq 1 ]
then
    for i in {0..49} # In total 151 model ensemble members; 3 or 4 files each
    do
	cat standard_diagnostics_cmip6_mrsos_template.sh | sed s/REPLACE/${i}/g > standard_diagnostics_cmip6_mrsos_${i}.sh
	cat ../standard_diagnostics_cmip6_mrsos.py | sed s/REPLACE/${i}/g > ../standard_diagnostics_cmip6_mrsos_${i}.py
    done
fi


clean=0
if [ ${clean} -eq 1 ]
then
    for i in {0..49} # In total 151 model ensemble members; 3 or 4 files each
    do

	rm standard_diagnostics_cmip6_mrsos_${i}.sh
	rm ../standard_diagnostics_cmip6_mrsos_${i}.py
    done
fi


submit=1
if [ ${submit} -eq 1 ]
then
    for i in {0..49} # In total 151 model ensemble members; 3 or 4 files each
    do
        qsub standard_diagnostics_cmip6_mrsos_${i}.sh
    done
fi
