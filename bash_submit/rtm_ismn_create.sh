create=0
if [ ${create} -eq 1 ]
then
    for e in {0..4}
    do
	for m in mean_lsm dolce_lsm em_lsm em_cmip5 em_cmip6 em_2cmip em_2cmip em_all
	do
	    cat rtm_ismn_template.sh | sed s/REPLACE1/${e}/ | sed s/REPLACE2/${m}/ | sed s/REPLACE3/${e}/ > rtm_ismn_${e}_${m}.sh
	    cat ../rtm_ismn.py | sed s/REPLACE1/${e}/ | sed s/REPLACE2/${m}/ | sed s/REPLACE3/${e}/ > ../rtm_ismn_${e}_${m}.py
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for e in {0..4}
    do
	for m in mean_lsm dolce_lsm em_lsm em_cmip5 em_cmip6 em_2cmip em_2cmip em_all
	do
		rm rtm_ismn_${e}_${m}.sh
		rm ../rtm_ismn_${e}_${m}.py
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for e in {0..4}
    do
	for m in mean_lsm dolce_lsm em_lsm em_cmip5 em_cmip6 em_2cmip em_2cmip em_all
	do
	    qsub rtm_ismn_${e}_${m}.sh
	done
    done
fi
