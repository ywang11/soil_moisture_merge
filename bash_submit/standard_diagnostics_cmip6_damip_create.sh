create=0
if [ ${create} -eq 1 ]
then
    for i in {0..14} # In total 43/45 model ensemble members; 3/5 files each
    do
	cat standard_diagnostics_cmip6_damip_template.sh | sed s/REPLACE/${i}/g > standard_diagnostics_cmip6_damip_${i}.sh
	cat ../standard_diagnostics_cmip6_damip.py | sed s/REPLACE/${i}/g > ../standard_diagnostics_cmip6_damip_${i}.py
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..14} # In total 43/45 model ensemble members; 3/5 files each
    do

	rm standard_diagnostics_cmip6_damip_${i}.sh
	rm ../standard_diagnostics_cmip6_damip_${i}.py
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..14} # In total 43/45 model ensemble members; 3/5 files each
    do
        qsub standard_diagnostics_cmip6_damip_${i}.sh
    done
fi
