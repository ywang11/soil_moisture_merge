#!/bin/bash
#PBS -A ACF-UTK0011
#PBS -l nodes=1:ppn=16,walltime=6:00:00

conda activate myenv

cd ~/Git/soil_moisture_merge

ipython -i validate_gen_precip_ts.py
