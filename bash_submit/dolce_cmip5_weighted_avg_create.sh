create=0
if [ ${create} -eq 1 ]
then
    for xx in {0..2}
    do
	for cv in {0..4}
	do
	    for d in {0..3}
	    do
		cat dolce_cmip5_weighted_avg_template.sh | sed s/REPLACE1/${xx}/ | sed s/REPLACE2/${cv}/ | sed s/REPLACE3/${d}/ > dolce_cmip5_weighted_avg_${xx}_${cv}_${d}.sh
		cat ../dolce_cmip5_weighted_avg.py | sed s/REPLACE1/${xx}/ | sed s/REPLACE2/${cv}/ | sed s/REPLACE3/${d}/ > ../dolce_cmip5_weighted_avg_${xx}_${cv}_${d}.py
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for xx in {0..2}
    do
	for cv in {0..4}
	do
	    for d in {0..3}
	    do
		rm dolce_cmip5_weighted_avg_${xx}_${cv}_${d}.sh
		rm ../dolce_cmip5_weighted_avg_${xx}_${cv}_${d}.py
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for xx in {0..2}
    do
	for cv in {0..4}
	do
	    for d in {0..3}
	    do
		qsub dolce_cmip5_weighted_avg_${xx}_${cv}_${d}.sh
	    done
	done
    done
fi
