create=0
if [ ${create} -eq 1 ]
then
    for i in {0..3}
    do
	for k in year_month_9grid year_month_anomaly_9grid # year_month_anomaly_1grid year_month_1grid
	do
	    cat em_tas_corr_year_month_2cmip_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${k}/ > em_tas_corr_year_month_2cmip_${i}_${k}.sh
	    cat ../em_tas_corr_year_month_2cmip.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${k}/ > ../em_tas_corr_year_month_2cmip_${i}_${k}.py
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..3}
    do
	for k in year_month_anomaly_1grid year_month_1grid year_month_9grid year_month_anomaly_9grid
	do
	    rm em_tas_corr_year_month_2cmip_${i}_${k}.sh
	    rm ../em_tas_corr_year_month_2cmip_${i}_${k}.py
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..3}
    do
	for k in year_month_9grid year_month_anomaly_9grid # year_month_anomaly_1grid year_month_1grid 
	do
	    qsub em_tas_corr_year_month_2cmip_${i}_${k}.sh
        done
    done
fi
