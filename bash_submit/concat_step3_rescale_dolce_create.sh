create=0
if [ ${create} -eq 1 ]
then
    for pr in {0..1}
    do
	for d in {0..3}
	do
	    for iam in {0..2}
	    do
		for cov in {0..4}
		do
		    cat concat_step3_rescale_dolce_template.sh | sed s/REPLACE1/${pr}/ | sed s/REPLACE2/${d}/ | sed s/REPLACE3/${iam}/ | sed s/REPLACE4/${cov}/ > concat_step3_rescale_dolce_${pr}_${d}_${iam}_${cov}.sh
		    cat ../concat_step3_rescale_dolce.py | sed s/REPLACE1/${pr}/ | sed s/REPLACE2/${d}/ | sed s/REPLACE3/${iam}/ | sed s/REPLACE4/${cov}/ > ../concat_step3_rescale_dolce_${pr}_${d}_${iam}_${cov}.py
		done
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for pr in {0..1}
    do
	for d in {0..3}
	do
	    for iam in {0..2}
	    do
		for cov in {0..4}
		do
		    rm concat_step3_rescale_dolce_${pr}_${d}_${iam}_${cov}.sh
		    rm ../concat_step3_rescale_dolce_${pr}_${d}_${iam}_${cov}.py
		done
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for pr in {0..1}
    do
	for d in {0..3}
	do
	    for iam in {0..2}
	    do
		for cov in {0..4}
		do
		    qsub concat_step3_rescale_dolce_${pr}_${d}_${iam}_${cov}.sh
		done
	    done
	done
    done
fi
