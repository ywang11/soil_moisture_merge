create=0
if [ ${create} -eq 1 ]
then
    for i in year_longest year_shorter year_shorter2 year_shortest
    do
	for j in {0..3}
	do
	    cat em_tas_lsm_corr_test_plot_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ > em_tas_lsm_corr_test_plot_${i}_${j}.sh
	    cat ../em_tas_lsm_corr_test_plot.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ > ../em_tas_lsm_corr_test_plot_${i}_${j}.py
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in year_longest year_shorter year_shorter2 year_shortest
    do
	for j in {0..3}
	do
	    rm em_tas_lsm_corr_test_plot_${i}_${j}.sh
	    rm ../em_tas_lsm_corr_test_plot_${i}_${j}.py
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in year_longest year_shorter year_shorter2 year_shortest
    do
	for j in {0..3}
	do
	    qsub em_tas_lsm_corr_test_plot_${i}_${j}.sh
        done
    done
fi
