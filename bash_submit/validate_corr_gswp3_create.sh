create=0
if [ ${create} -eq 1 ]
then
    for i in {0..6}
    do
	for j in {0..3}
	do
	    cat validate_corr_gswp3_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ > validate_corr_gswp3_${i}_${j}.sh
	    cat ../validate_corr_gswp3.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ > ../validate_corr_gswp3_${i}_${j}.py
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..6}
    do
	for j in {0..3}
	do
	    rm validate_corr_gswp3_${i}_${j}.sh
	    rm ../validate_corr_gswp3_${i}_${j}.py
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..6}
    do
	for j in {1..3}
	do
	    qsub validate_corr_gswp3_${i}_${j}.sh
	done
    done
fi
