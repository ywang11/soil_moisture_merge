create=0
if [ ${create} -eq 1 ]
then
    for year in year_longest year_shorter year_shorter2 year_shortest
    do
	for d in {0..3}
	do
	    for opt in month_1grid month_anomaly_1grid
	    do
		cat standard_diagnostics_em_tas_month_lsm_template.sh | sed s/REPLACE1/${year}/ | sed s/REPLACE2/${d}/ | sed s/REPLACE4/${opt}/ > standard_diagnostics_em_tas_month_lsm_${year}_${d}_${opt}.sh
		cat ../standard_diagnostics_em_tas_month.py | sed s/REPLACE1/${year}/ | sed s/REPLACE2/${d}/ | sed s/REPLACE3/lsm/ | sed s/REPLACE4/${opt}/ > ../standard_diagnostics_em_tas_month_lsm_${year}_${d}_${opt}.py
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for year in year_longest year_shorter year_shorter2 year_shortest
    do
	for d in {0..3}
	do
	    for opt in month_1grid month_anomaly_1grid
	    do
		rm standard_diagnostics_em_tas_month_lsm_${year}_${d}_${opt}.sh
		rm ../standard_diagnostics_em_tas_month_lsm_${year}_${d}_${opt}.py
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for year in year_longest year_shorter year_shorter2 year_shortest
    do
	for d in {0..3}
	do
	    for opt in month_1grid month_anomaly_1grid
	    do
		qsub standard_diagnostics_em_tas_month_lsm_${year}_${d}_${opt}.sh
	    done
	done
    done
fi
