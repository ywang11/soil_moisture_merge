create=0
if [ ${create} -eq 1 ]
then
    for i in {0..3}
    do
	for j in lsm cmip5 cmip6
	do
	    cat em_tas_lagged_corr_month_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ > em_tas_lagged_corr_month_${i}_${j}.sh
	    cat ../em_tas_lagged_corr_month.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ > ../em_tas_lagged_corr_month_${i}_${j}.py
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..3}
    do
	for j in lsm cmip5 cmip6
	do
	    rm em_tas_lagged_corr_month_${i}_${j}.sh
	    rm ../em_tas_lagged_corr_month_${i}_${j}.py
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..3}
    do
	for j in lsm cmip5 cmip6
	do
	    qsub em_tas_lagged_corr_month_${i}_${j}.sh
        done
    done
fi
