create=0
if [ ${create} -eq 1 ]
then
    ##declare -a simple
    ##declare -a dominance_lc

    ##simple=(True False False)
    ##dominance_lc=(False False True)
    for i in year_longest year_shorter year_shorter2 year_shortest
    do
	for j in {0..3}
	do
  	    for k in {0..4}
  	    do
  		for l in {0..2}
  		do
  		    cat at_obs_dolce_all_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ | sed s/REPLACE4/${l}/ > at_obs_dolce_all_${i}_${j}_${k}_${l}.sh
  		    cat ../at_obs_dolce_all.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ | sed s/REPLACE4/${l}/ > ../at_obs_dolce_all_${i}_${j}_${k}_${l}.py
  		done
  	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in year_longest year_shorter year_shorter2 year_shortest
    do
	for j in {0..3}
	do
  	    for k in {0..4}
  	    do
  		for l in {0..2}
  		do
		    rm at_obs_dolce_all_${i}_${j}_${k}_${l}.sh
		    rm ../at_obs_dolce_all_${i}_${j}_${k}_${l}.py
		done
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in year_longest year_shorter year_shorter2 year_shortest
    do
	for j in {0..3}
	do
  	    for k in {0..4}
  	    do
  		for l in {0..2}
  		do
		    qsub at_obs_dolce_all_${i}_${j}_${k}_${l}.sh
		done
	    done
        done
    done
fi
