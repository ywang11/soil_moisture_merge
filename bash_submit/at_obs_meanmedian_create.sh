create=0
if [ ${create} -eq 1 ]
then
    for y in year_longest year_shorter year_shorter2 year_shortest
    do
	for iam in {0..2}
	do
	    for c in lsm cmip5 cmip6 2cmip all
	    do
		cat at_obs_meanmedian_template.sh | sed s/REPLACE1/${y}/ | sed s/REPLACE2/${iam}/ | sed s/REPLACE3/${c}/ > at_obs_meanmedian_${y}_${iam}_${c}.sh
		cat ../at_obs_meanmedian.py | sed s/REPLACE1/${y}/ | sed s/REPLACE2/${iam}/ | sed s/REPLACE3/${c}/ > ../at_obs_meanmedian_${y}_${iam}_${c}.py
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for y in year_longest year_shorter year_shorter2 year_shortest
    do
	for iam in {0..2}
	do
	    for c in lsm cmip5 cmip6 2cmip all
	    do
		rm at_obs_meanmedian_${y}_${iam}_${c}.sh
		rm ../at_obs_meanmedian_${y}_${iam}_${c}.py
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for y in year_longest year_shorter year_shorter2 year_shortest
    do
	for iam in {0..2}
	do
	    for c in lsm cmip5 cmip6 2cmip all
	    do
		qsub at_obs_meanmedian_${y}_${iam}_${c}.sh
	    done
	done
    done
fi
