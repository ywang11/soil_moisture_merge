create=0
if [ ${create} -eq 1 ]
then
    ##declare -a simple
    ##declare -a dominance_lc

    ##simple=(True False False)
    ##dominance_lc=(False False True)

    for i in {0..3}
    do
	for j in {0..4}
	do
	    for k in {0..2}
	    do
		cat at_obs_dolce_cmip6_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ > at_obs_dolce_cmip6_${i}_${j}_${k}.sh
		cat ../at_obs_dolce_cmip6.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ | sed s/REPLACE3/${k}/ > ../at_obs_dolce_cmip6_${i}_${j}_${k}.py
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..3}
    do
	for j in {0..4}
	do
	    for k in {0..2}
	    do
		rm at_obs_dolce_cmip6_${i}_${j}_${k}.sh
		rm ../at_obs_dolce_cmip6_${i}_${j}_${k}.py
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..3}
    do
	for j in {0..4}
	do
	    for k in {0..2}
	    do
		qsub at_obs_dolce_cmip6_${i}_${j}_${k}.sh
	    done
        done
    done
fi
