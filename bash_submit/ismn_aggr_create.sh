create=0
if [ ${create} -eq 1 ]
then
    for i in {0..2}
    do
	for j in {0..3}
	do
	    cat ismn_aggr_template.sh | sed s/REPLACE1/${i}/g | sed s/REPLACE2/${j}/g > ismn_aggr_${i}_${j}.sh
	    cat ../ismn_aggr.py | sed s/REPLACE1/${i}/g | sed s/REPLACE2/${j}/g > ../ismn_aggr_${i}_${j}.py
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..2}
    do
	for j in {0..3}
	do
	    rm ismn_aggr_${i}_${j}.sh
	    rm ../ismn_aggr_${i}_${j}.py
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..2}
    do
	for j in {0..3}
	do
            qsub ismn_aggr_${i}_${j}.sh
	done
    done
fi
