####!/bin/bash -l
####SBATCH --qos=regular
####SBATCH --time=1
####SBATCH --nodes=1
####SBATCH --constraint=haswell

#PBS -S /bin/bash
#PBS -A ACF-UTK0011
#PBS -l nodes=1,walltime=06:00:00

###source /global/project/projectdirs/cmip6/software/anaconda_envs/load_latest_e3sm_unified.sh

cd ~/Git/soil_moisture_merge/interpolation/

##module load ncl

conda activate base

# ncl interp_CMIP6_mrsol_REPLACE1_REPLACE2_REPLACE3.ncl
