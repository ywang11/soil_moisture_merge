#PBS -S /bin/bash
#PBS -A ACF-UTK0011
#PBS -l nodes=1,walltime=10:00:00
#PBS -o dolce_cmip6_weighted_uncertainty_REPLACE1_REPLACE2_REPLACE3.o$PBS_JOBID

cd $PBS_O_WORKDIR

python /nics/c/home/ywang254/Git/soil_moisture_merge/dolce_cmip6_weighted_uncertainty_REPLACE1_REPLACE2_REPLACE3.py
