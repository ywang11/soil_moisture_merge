create=0
if [ ${create} -eq 1 ]
then
    for i in {0..2}
    do
	for j in {0..3}
	do
	    cat at_obs_lsm_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ > at_obs_lsm_${i}_${j}.sh
	    cat ../at_obs_lsm.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ > ../at_obs_lsm_${i}_${j}.py
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..2}
    do
	for j in {0..3}   
	do
	    rm at_obs_lsm_${i}_${j}.sh
	    rm ../at_obs_lsm_${i}_${j}.py
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..2}
    do
	for j in {0..3}
	do
	    qsub at_obs_lsm_${i}_${j}.sh
        done
    done
fi
