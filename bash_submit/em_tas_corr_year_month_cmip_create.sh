create=0
if [ ${create} -eq 1 ]
then
    for i in {0..3}
    do
	for j in cmip5 cmip6
	do
	    for k in year_month_9grid year_month_anomaly_9grid # year_month_anomaly_1grid year_month_1grid
	    do
		cat em_tas_corr_year_month_cmip_template.sh | sed s/REPLACE2/${i}/ | sed s/REPLACE3/${j}/ | sed s/REPLACE4/${k}/ > em_tas_corr_year_month_${i}_${j}_${k}.sh
		cat ../em_tas_corr_year_month.py | sed s/REPLACE1/year_longest/ | sed s/REPLACE2/${i}/ | sed s/REPLACE3/${j}/ | sed s/REPLACE4/${k}/ > ../em_tas_corr_year_month_${i}_${j}_${k}.py
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..3}
    do
	for j in cmip5 cmip6
	do
	    for k in year_month_9grid year_month_anomaly_9grid # year_month_anomaly_1grid year_month_1grid
	    do
		rm em_tas_corr_year_month_${i}_${j}_${k}.sh
		rm ../em_tas_corr_year_month_${i}_${j}_${k}.py
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..3}
    do
	for j in cmip5 cmip6
	do
	    for k in year_month_9grid year_month_anomaly_9grid # year_month_anomaly_1grid year_month_1grid
	    do
		qsub em_tas_corr_year_month_${i}_${j}_${k}.sh
	    done
        done
    done
fi
