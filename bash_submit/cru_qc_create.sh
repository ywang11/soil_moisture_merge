create=0
if [ ${create} -eq 1 ]
then
    for year in year_longest year_shorter year_shorter2 year_shortest
    do
	cat cru_qc_template.sh | sed s/REPLACE/${year}/ > cru_qc_${year}.sh
	cat ../cru_qc.py | sed s/REPLACE/${year}/ > ../cru_qc_${year}.py
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for year in year_longest year_shorter year_shorter2 year_shortest
    do
	rm cru_qc_${year}.sh
	rm ../cru_qc_${year}.py
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for year in year_longest year_shorter year_shorter2 year_shortest
    do
        qsub cru_qc_${year}.sh
    done
fi
