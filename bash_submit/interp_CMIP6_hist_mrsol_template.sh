###!/bin/bash -l
###SBATCH --qos=regular
###SBATCH --time=1
###SBATCH --nodes=1
###SBATCH --constraint=haswell

#PBS -S /bin/bash
#PBS -A ACF-UTK0011
#PBS -l nodes=1,walltime=06:00:00

conda activate base

cd ~/Git/soil_moisture_merge/interpolation/

# ncl interp_CMIP6_historical_mrsol_REPLACE1_REPLACE2_REPLACE3.ncl
