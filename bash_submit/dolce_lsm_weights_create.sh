create=0
if [ ${create} -eq 1 ]
then
    for i in {0..2}
    do
        for year in year_longest year_shorter year_shorter2 year_shortest
        do
            cat dolce_lsm_weights_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${year}/ > dolce_lsm_weights_${i}_${year}.sh
            cat ../dolce_lsm_weights.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${year}/ > ../dolce_lsm_weights_${i}_${year}.py
        done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..2}
    do
        for year in year_longest year_shorter year_shorter2 year_shortest
        do
	    rm dolce_lsm_weights_${i}_${year}.sh
	    rm ../dolce_lsm_weights_${i}_${year}.py
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..2}
    do
	for year in year_longest year_shorter year_shorter2 year_shortest
	do
	    qsub dolce_lsm_weights_${i}_${year}.sh
	done
    done
fi
