create=0
if [ ${create} -eq 1 ]
then
    for i in {0..2}
    do
        cat dolce_2cmip_weights_template.sh | sed s/REPLACE/${i}/ > dolce_2cmip_weights_${i}.sh
        cat ../dolce_2cmip_weights.py | sed s/REPLACE/${i}/ > ../dolce_2cmip_weights_${i}.py
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..2}
    do
	rm dolce_2cmip_weights_${i}.sh
	rm ../dolce_2cmip_weights_${i}.py
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..2}
    do
	qsub dolce_2cmip_weights_${i}.sh
    done
fi
