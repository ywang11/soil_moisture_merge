create=0
if [ ${create} -eq 1 ]
then
    for xx in {0..2}
    do
	for year in year_longest year_shorter year_shorter2 year_shortest
	do
	    for cv in {0..4}
	    do
		for d in {0..3}
		do
		    cat dolce_lsm_weighted_avg_template.sh | sed s/REPLACE1/${xx}/ | sed s/REPLACE2/${year}/ | sed s/REPLACE3/${cv}/ | sed s/REPLACE4/${d}/ > dolce_lsm_weighted_avg_${xx}_${year}_${cv}_${d}.sh
		    cat ../dolce_lsm_weighted_avg.py | sed s/REPLACE1/${xx}/ | sed s/REPLACE2/${year}/ | sed s/REPLACE3/${cv}/ | sed s/REPLACE4/${d}/ > ../dolce_lsm_weighted_avg_${xx}_${year}_${cv}_${d}.py
		done
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for xx in {0..2}
    do
	for year in year_longest year_shorter year_shorter2 year_shortest
	do
	    for cv in {0..4}
	    do
		for d in {0..3}
		do
		    rm dolce_lsm_weighted_avg_${xx}_${year}_${cv}_${d}.sh
		    rm ../dolce_lsm_weighted_avg_${xx}_${year}_${cv}_${d}.py
		done
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for xx in {0..2}
    do
	for year in year_longest year_shorter year_shorter2 year_shortest
	do
	    for cv in {0..4}
	    do
		for d in {0..3}
		do
		    qsub dolce_lsm_weighted_avg_${xx}_${year}_${cv}_${d}.sh
		done
	    done
	done
    done
fi
