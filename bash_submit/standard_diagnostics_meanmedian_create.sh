create=0
if [ ${create} -eq 1 ]
then
    #
    for year in year_longest year_shorter year_shorter2 year_shortest
    do
	for model in lsm all
	do
	    cat standard_diagnostics_meanmedian_template.sh | sed s/REPLACE1/${year}/ | sed s/REPLACE2/${model}/ > standard_diagnostics_meanmedian_${year}_${model}.sh
	    cat ../standard_diagnostics_meanmedian.py | sed s/REPLACE1/${year}/| sed s/REPLACE2/${model}/ > ../standard_diagnostics_meanmedian_${year}_${model}.py
	done
    done

    #
    for year in year_longest
    do
	for model in cmip5 cmip6 2cmip
	do
	    cat standard_diagnostics_meanmedian_template.sh | sed s/REPLACE1/${year}/ | sed s/REPLACE2/${model}/ > standard_diagnostics_meanmedian_${year}_${model}.sh
	    cat ../standard_diagnostics_meanmedian.py | sed s/REPLACE1/${year}/| sed s/REPLACE2/${model}/ > ../standard_diagnostics_meanmedian_${year}_${model}.py
	done
    done

fi


clean=1
if [ ${clean} -eq 1 ]
then
    #
    for year in year_longest year_shorter year_shorter2 year_shortest
    do
	for model in lsm all
	do
	    rm standard_diagnostics_meanmedian_${year}_${model}.sh
	    rm ../standard_diagnostics_meanmedian_${year}_${model}.py
	done
    done

    #
    for year in year_longest
    do
	for model in cmip5 cmip6 2cmip
	do
	    rm standard_diagnostics_meanmedian_${year}_${model}.sh
	    rm ../standard_diagnostics_meanmedian_${year}_${model}.py
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    #
    for year in year_longest year_shorter year_shorter2 year_shortest
    do
	for model in lsm all
	do
	    qsub standard_diagnostics_meanmedian_${year}_${model}.sh
	done
    done

    #
    for year in year_longest
    do
	for model in cmip5 cmip6 2cmip
	do
	    qsub standard_diagnostics_meanmedian_${year}_${model}.sh
	done
    done
fi
