create=0
if [ ${create} -eq 1 ]
then
    for year in year_longest # year_shorter year_shorter2 year_shortest
    do
	for i in {0..3}
	do
	    for j in cmip5 cmip6 # lsm
	    do
		for k in True False
		do
		    cat em_tas_corr_test_area_all_template.sh | sed s/REPLACE1/${year}/ | sed s/REPLACE2/${i}/ | sed s/REPLACE3/${j}/ | sed s/REPLACE4/${k}/ > em_tas_corr_test_area_all_${year}_${i}_${j}_${k}.sh
		    cat ../em_tas_corr_test_area_all.py | sed s/REPLACE1/${year}/ | sed s/REPLACE2/${i}/ | sed s/REPLACE3/${j}/ | sed s/REPLACE4/${k}/ > ../em_tas_corr_test_area_all_${year}_${i}_${j}_${k}.py
		done
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for year in year_longest year_shorter year_shorter2 year_shortest
    do
	for i in {0..3}
	do
	    for j in lsm cmip5 cmip6
	    do
		for k in True False
		do
		    rm em_tas_corr_test_area_all_${year}_${i}_${j}_${k}.sh
		    rm ../em_tas_corr_test_area_all_${year}_${i}_${j}_${k}.py
		done
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for year in year_longest # year_shorter year_shorter2 year_shortest
    do
	for i in {0..3}
	do
	    for j in cmip5 cmip6 # lsm
	    do
		for k in True False
		do
		    qsub em_tas_corr_test_area_all_${year}_${i}_${j}_${k}.sh
		done
	    done
        done
    done
fi
