create=0
if [ ${create} -eq 1 ]
then
    for year in year_longest year_shorter year_shorter2 year_shortest
    do
	for o in mean median
	do
	    cat meanmedian_lsm_template.sh | sed s/REPLACE1/${year}/ | sed s/REPLACE2/${o}/ > meanmedian_lsm_${year}_${o}.sh
	    cat ../meanmedian_lsm.py | sed s/REPLACE1/${year}/ | sed s/REPLACE2/${o}/ > ../meanmedian_lsm_${year}_${o}.py
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for year in year_longest year_shorter year_shorter2 year_shortest
    do
	for o in mean median
	do
	    rm meanmedian_lsm_${year}_${o}.sh
	    rm ../meanmedian_lsm_${year}_${o}.py
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for year in year_longest year_shorter year_shorter2 year_shortest
    do
	for o in mean median
	do
            qsub meanmedian_lsm_${year}_${o}.sh
	done
    done
fi
