create=1
if [ ${create} -eq 1 ]
then
    for i in {0..6}
    do
	cat validate_corr_gswp3_template.sh | sed s/REPLACE/${i}/ > validate_corr_gswp3_${i}.sh
	cat ../validate_corr_gswp3.py | sed s/REPLACE/${i}/ > ../validate_corr_gswp3_${i}.py
    done
fi


clean=0
if [ ${clean} -eq 1 ]
then
    for i in {0..6}
    do
	rm validate_corr_gswp3_${i}.sh
	rm ../validate_corr_gswp3_${i}.py
    done
fi


submit=1
if [ ${submit} -eq 1 ]
then
    for i in {0..6}
    do
	sbatch validate_corr_gswp3_${i}.sh
    done
fi
