create=0
if [ ${create} -eq 1 ]
then
    for d in {0..3}
    do
	for no in True False
	do
	    for k in lsm all
	    do
		cat standard_diagnostics_concat_em_template.sh | sed s/REPLACE1/${d}/ | sed s/REPLACE2/${no}/ | sed s/REPLACE3/${k}/ > standard_diagnostics_concat_em_${d}_${no}_${k}.sh
		cat ../standard_diagnostics_concat_em.py | sed s/REPLACE1/${d}/ | sed s/REPLACE2/${no}/ | sed s/REPLACE3/${k}/ > ../standard_diagnostics_concat_em_${d}_${no}_${k}.py
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for d in {0..3}
    do
	for no in True False
	do
	    for k in lsm all
	    do
		rm standard_diagnostics_concat_em_${d}_${no}_${k}.sh
		rm ../standard_diagnostics_concat_em_${d}_${no}_${k}.py
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for d in {0..3}
    do
	for no in True False
	do
	    for k in lsm all
	    do
		qsub standard_diagnostics_concat_em_${d}_${no}_${k}.sh
	    done
	done
    done
fi
