create=0
if [ ${create} -eq 1 ]
then
    for i in {0..3}
    do
	for model in cmip5 cmip6 2cmip
	do
	    for j in year_month_anomaly_9grid year_month_9grid # year_month_anomaly_1grid year_month_1grid
	    do
		cat em_tas_corr_year_month_correct_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${model}/ | sed s/REPLACE3/${j}/ > em_tas_corr_year_month_correct_${i}_${model}_${j}.sh
		cat ../em_tas_corr_year_month_correct_cmip.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${model}/ | sed s/REPLACE3/${j}/ > ../em_tas_corr_year_month_correct_${i}_${model}_${j}.py
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..3}
    do
	for model in cmip5 cmip6 2cmip
	do
	    for j in year_month_anomaly_9grid year_month_9grid # year_month_anomaly_1grid year_month_1grid
	    do
		rm em_tas_corr_year_month_correct_${i}_${model}_${j}.sh
		rm ../em_tas_corr_year_month_correct_${i}_${model}_${j}.py
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..3}
    do
	for model in cmip5 cmip6 2cmip
	do
	    for j in year_month_anomaly_9grid year_month_9grid # year_month_anomaly_1grid year_month_1grid
	    do
		qsub em_tas_corr_year_month_correct_${i}_${model}_${j}.sh
	    done
        done
    done
fi
