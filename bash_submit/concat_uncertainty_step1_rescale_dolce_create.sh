create=0
if [ ${create} -eq 1 ]
then
    for d in {0..3}
    do
	for iam in 1 # {0..2}
	do
	    for cov in 1 # {0..4}
	    do
		for prod in lsm all
		do
		    cat concat_uncertainty_step1_rescale_dolce_template.sh | sed s/REPLACE1/${d}/ | sed s/REPLACE2/${iam}/ | sed s/REPLACE3/${cov}/ | sed s/REPLACE4/${prod}/ > concat_uncertainty_step1_rescale_dolce_${d}_${iam}_${cov}_${prod}.sh
		    cat ../concat_uncertainty_step1_rescale_dolce.py | sed s/REPLACE1/${d}/ | sed s/REPLACE2/${iam}/ | sed s/REPLACE3/${cov}/ | sed s/REPLACE4/${prod}/ > ../concat_uncertainty_step1_rescale_dolce_${d}_${iam}_${cov}_${prod}.py
		done
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for d in {0..3}
    do
	for iam in {0..2}
	do
	    for cov in {0..4}
	    do
		for prod in lsm all
		do
		    rm concat_uncertainty_step1_rescale_dolce_${d}_${iam}_${cov}_${prod}.sh
		    rm ../concat_uncertainty_step1_rescale_dolce_${d}_${iam}_${cov}_${prod}.py
		done
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for d in {2..3}
    do
	for iam in {0..2}
	do
	    for cov in {0..4}
	    do
		for prod in lsm all
		do
		    qsub concat_uncertainty_step1_rescale_dolce_${d}_${iam}_${cov}_${prod}.sh
		done
	    done
	done
    done
fi
