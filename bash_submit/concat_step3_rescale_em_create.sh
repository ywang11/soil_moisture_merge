create=0
if [ ${create} -eq 1 ]
then
    for i in {0..3}
    do
	for fx in 0
	do
	    for prod in {0..1}
	    do
		cat concat_step3_rescale_em_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${fx}/ | sed s/REPLACE3/${prod}/ > concat_step3_rescale_em_${i}_${fx}_${prod}.sh
		cat ../concat_step3_rescale_em.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${fx}/ | sed s/REPLACE3/${prod}/ > ../concat_step3_rescale_em_${i}_${fx}_${prod}.py
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..3}
    do
	for fx in 0
	do
	    for prod in {0..1}
	    do
		rm concat_step3_rescale_em_${i}_${fx}_${prod}.sh
		rm ../concat_step3_rescale_em_${i}_${fx}_${prod}.py
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..3}
    do
	for fx in 0
	do
	    for prod in {0..1}
	    do
		qsub concat_step3_rescale_em_${i}_${fx}_${prod}.sh
	    done
	done
    done
fi
