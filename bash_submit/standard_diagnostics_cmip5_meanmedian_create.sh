create=1
if [ ${create} -eq 1 ]
then
    for year in year_longest year_shorter year_shorter2 year_shortest
    do
	cat standard_diagnostics_cmip5_meanmedian_template.sh | sed s/REPLACE/${year}/ > standard_diagnostics_cmip5_meanmedian_${year}.sh
	cat ../standard_diagnostics_cmip5_meanmedian.py | sed s/REPLACE/${year}/ > ../standard_diagnostics_cmip5_meanmedian_${year}.py
    done
fi


clean=0
if [ ${clean} -eq 1 ]
then
    for year in year_longest year_shorter year_shorter2 year_shortest
    do
	rm standard_diagnostics_cmip5_meanmedian_${year}.sh
	rm ../standard_diagnostics_cmip5_meanmedian_${year}.py
    done
fi


submit=1
if [ ${submit} -eq 1 ]
then
    for year in year_longest year_shorter year_shorter2 year_shortest
    do
	qsub standard_diagnostics_cmip5_meanmedian_${year}.sh
    done
fi
