create=0
if [ ${create} -eq 1 ]
then
    for i in {0..3}
    do
	cat standard_diagnostics_cmip6_template.sh | sed s/REPLACE/${i}/g > standard_diagnostics_cmip6_${i}.sh
	cat ../standard_diagnostics_cmip6.py | sed s/REPLACE/${i}/g > ../standard_diagnostics_cmip6_${i}.py
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..3}
    do

	rm standard_diagnostics_cmip6_${i}.sh
	rm ../standard_diagnostics_cmip6_${i}.py
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..3}
    do
        qsub standard_diagnostics_cmip6_${i}.sh
    done
fi
