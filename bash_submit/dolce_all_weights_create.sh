create=0
if [ ${create} -eq 1 ]
then
    for i in {0..2}
    do
	for j in year_longest year_shorter year_shorter2 year_shortest
	do
            cat dolce_all_weights_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ > dolce_all_weights_${i}_${j}.sh
            cat ../dolce_all_weights.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ > ../dolce_all_weights_${i}_${j}.py
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..2}
    do
	for j in year_longest year_shorter year_shorter2 year_shortest
	do
	    rm dolce_all_weights_${i}_${j}.sh
	    rm ../dolce_all_weights_${i}_${j}.py
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..2}
    do
	for j in year_longest year_shorter year_shorter2 year_shortest
	do
	    qsub dolce_all_weights_${i}_${j}.sh
	done
    done
fi
