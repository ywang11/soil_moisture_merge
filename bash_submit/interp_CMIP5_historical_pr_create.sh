create=0
if [ ${create} -eq 1 ]
then
    # The models are intersection between the available soil moisture and
    # the available precipitation models.
    for model in ACCESS1-0 ACCESS1-3 CanESM2 CNRM-CM5 CNRM-CM5-2 FGOALS-g2 FGOALS-s2 GFDL-CM3 GFDL-ESM2G GFDL-ESM2M GISS-E2-H GISS-E2-H-CC GISS-E2-R GISS-E2-R-CC HadCM3 HadGEM2-CC HadGEM2-ES inmcm4 MIROC5 MIROC-ESM MIROC-ESM-CHEM NorESM1-ME
    do
	cat interp_CMIP5_historical_pr_template.sh | sed s/REPLACE/${model}/ > interp_CMIP5_historical_pr_${model}.sh
	cat ../interpolation/interp_CMIP5_historical_pr_template.ncl | sed s/REPLACE/${model}/ > ../interpolation/interp_CMIP5_historical_pr_${model}.ncl
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for model in ACCESS1-0 ACCESS1-3 CanESM2 CNRM-CM5 CNRM-CM5-2 FGOALS-g2 FGOALS-s2 GFDL-CM3 GFDL-ESM2G GFDL-ESM2M GISS-E2-H GISS-E2-H-CC GISS-E2-R GISS-E2-R-CC HadCM3 HadGEM2-CC HadGEM2-ES inmcm4 MIROC5 MIROC-ESM MIROC-ESM-CHEM NorESM1-ME
    do
	rm interp_CMIP5_historical_pr_${model}.sh
	rm ../interpolation/interp_CMIP5_historical_pr_${model}.ncl
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for model in ACCESS1-0 ACCESS1-3 CanESM2 CNRM-CM5 CNRM-CM5-2 FGOALS-g2 FGOALS-s2 GFDL-CM3 GFDL-ESM2G GFDL-ESM2M GISS-E2-H GISS-E2-H-CC GISS-E2-R GISS-E2-R-CC HadCM3 HadGEM2-CC HadGEM2-ES inmcm4 MIROC5 MIROC-ESM MIROC-ESM-CHEM NorESM1-ME
    do
        qsub interp_CMIP5_historical_pr_${model}.sh
    done
fi
