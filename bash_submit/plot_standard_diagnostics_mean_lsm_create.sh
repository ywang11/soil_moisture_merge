create=0
if [ ${create} -eq 1 ]
then
    for year in year_longest year_shorter year_shorter2 year_shortest
    do
        cat plot_standard_diagnostics_mean_lsm_template.sh | sed s/REPLACE/${year}/ > plot_standard_diagnostics_mean_lsm_${year}.sh
        cat ../plot_standard_diagnostics_mean_lsm.py | sed s/REPLACE/${year}/ > ../plot_standard_diagnostics_mean_lsm_${year}.py
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for year in year_longest year_shorter year_shorter2 year_shortest
    do
	rm plot_standard_diagnostics_mean_lsm_${year}.sh
	rm ../plot_standard_diagnostics_mean_lsm_${year}.py
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for year in year_longest year_shorter year_shorter2 year_shortest
    do
	qsub plot_standard_diagnostics_mean_lsm_${year}.sh
    done
fi
