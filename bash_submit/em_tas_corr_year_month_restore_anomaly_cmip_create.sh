create=0
if [ ${create} -eq 1 ]
then
    for year in year_longest
    do
	for i in {0..3}
	do
	    for model in cmip5 cmip6 2cmip
	    do
		for j in year_month_anomaly_9grid # year_month_anomaly_1grid
		do
		    cat em_tas_corr_year_month_restore_anomaly_template.sh | sed s/REPLACE1/${year}/ | sed s/REPLACE2/${i}/ | sed s/REPLACE3/${model}/ | sed s/REPLACE4/${j}/ > em_tas_corr_year_month_restore_anomaly_${year}_${i}_${model}_${j}.sh
		    cat ../em_tas_corr_year_month_restore_anomaly.py | sed s/REPLACE1/${year}/ | sed s/REPLACE2/${i}/ | sed s/REPLACE3/${model}/ | sed s/REPLACE4/${j}/ > ../em_tas_corr_year_month_restore_anomaly_${year}_${i}_${model}_${j}.py
		done
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for year in year_longest
    do
	for i in {0..3}
	do
	    for model in cmip5 cmip6 2cmip
	    do
		for j in year_month_anomaly_9grid # year_month_anomaly_1grid
		do
		    rm em_tas_corr_year_month_restore_anomaly_${year}_${i}_${model}_${j}.sh
		    rm ../em_tas_corr_year_month_restore_anomaly_${year}_${i}_${model}_${j}.py
		done
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for year in year_longest
    do
        for i in {0..3}
        do
	    for model in cmip5 cmip6 2cmip
	    do
		for j in year_month_anomaly_9grid # year_month_anomaly_1grid
		do
		    qsub em_tas_corr_year_month_restore_anomaly_${year}_${i}_${model}_${j}.sh
		done
	    done
        done
    done
fi
