create=0
if [ ${create} -eq 1 ]
then
    for i in {0..3}
    do
	cat standard_diagnostics_cmip5_template.sh | sed s/REPLACE/${i}/g > standard_diagnostics_cmip5_${i}.sh
	cat ../standard_diagnostics_cmip5.py | sed s/REPLACE/${i}/g > ../standard_diagnostics_cmip5_${i}.py
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..3}
    do

	rm standard_diagnostics_cmip5_${i}.sh
	rm ../standard_diagnostics_cmip5_${i}.py
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..3}
    do
        qsub standard_diagnostics_cmip5_${i}.sh
    done
fi
