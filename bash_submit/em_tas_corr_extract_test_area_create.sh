create=0
if [ ${create} -eq 1 ]
then
    for year in year_longest # year_shorter year_shorter2 year_shortest
    do
	for i in {0..3}
	do
	    for j in cmip5 cmip6 # lsm
	    do
		cat em_tas_corr_extract_test_area_template.sh | sed s/REPLACE1/${year}/ | sed s/REPLACE2/${i}/ | sed s/REPLACE3/${j}/ > em_tas_corr_extract_test_area_${year}_${i}_${j}.sh
		cat ../em_tas_corr_extract_test_area.py | sed s/REPLACE1/${year}/ | sed s/REPLACE2/${i}/ | sed s/REPLACE3/${j}/ > ../em_tas_corr_extract_test_area_${year}_${i}_${j}.py
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for year in year_longest year_shorter year_shorter2 year_shortest
    do
	for i in {0..3}
	do
	    for j in lsm cmip5 cmip6
	    do
		rm em_tas_corr_extract_test_area_${year}_${i}_${j}.sh
		rm ../em_tas_corr_extract_test_area_${year}_${i}_${j}.py
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for year in year_longest # year_shorter year_shorter2 year_shortest
    do
	for i in {0..3}
	do
	    for j in cmip5 cmip6 # lsm
	    do
		qsub em_tas_corr_extract_test_area_${year}_${i}_${j}.sh
	    done
        done
    done
fi
