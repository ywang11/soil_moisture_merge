create=0
if [ ${create} -eq 1 ]
then
    for d in {0..3}
    do
	for m in mean_lsm dolce_lsm em_lsm em_cmip5 em_cmip6 em_2cmip em_2cmip em_all
	do
	    cat rtg_ismn_template.sh | sed s/REPLACE1/${d}/ | sed s/REPLACE2/${m}/ > rtg_ismn_${d}_${m}.sh
	    cat ../rtg_ismn.py | sed s/REPLACE1/${d}/ | sed s/REPLACE2/${m}/ > ../rtg_ismn_${d}_${m}.py
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for d in {0..3}
    do
	for m in mean_lsm dolce_lsm em_lsm em_cmip5 em_cmip6 em_2cmip em_2cmip em_all
	do
	    rm rtg_ismn_${d}_${m}.sh
	    rm ../rtg_ismn_${d}_${m}.py
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for d in {0..3}
    do
	for m in mean_lsm dolce_lsm em_lsm em_cmip5 em_cmip6 em_2cmip em_2cmip em_all
	do
	    qsub rtg_ismn_${d}_${m}.sh
	done
    done
fi
