create=0
if [ ${create} -eq 1 ]
then
    for d in {0..3}
    do
	for no in True False
	do
	    for un in True False
	    do
		for pr in all lsm
		do
		    cat standard_diagnostics_concat_dolce_template.sh | sed s/REPLACE1/${d}/ | sed s/REPLACE2/${no}/ | sed s/REPLACE3/${un}/ | sed s/REPLACE4/${pr}/ > standard_diagnostics_concat_dolce_${d}_${no}_${un}_${pr}.sh
		    cat ../standard_diagnostics_concat_dolce.py | sed s/REPLACE1/${d}/ | sed s/REPLACE2/${no}/ | sed s/REPLACE3/${un}/ | sed s/REPLACE4/${pr}/ > ../standard_diagnostics_concat_dolce_${d}_${no}_${un}_${pr}.py
		done
	    done
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for d in {0..3}
    do
	for no in True False
	do
	    for un in True False
	    do
		for pr in all lsm
		do
		    rm standard_diagnostics_concat_dolce_${d}_${no}_${un}_${pr}.sh
		    rm ../standard_diagnostics_concat_dolce_${d}_${no}_${un}_${pr}.py
		done
	    done
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for d in {0..3}
    do
	for no in True False
	do
	    for un in True False
	    do
		for pr in all lsm
		do
		    qsub standard_diagnostics_concat_dolce_${d}_${no}_${un}_${pr}.sh
		done
	    done
	done
    done
fi
