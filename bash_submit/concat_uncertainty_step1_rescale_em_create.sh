create=0
if [ ${create} -eq 1 ]
then
    for d in {0..3}
    do
	for prod in lsm all
	do
	    cat concat_uncertainty_step1_rescale_em_template.sh | sed s/REPLACE1/${d}/ | sed s/REPLACE2/${prod}/ > concat_uncertainty_step1_rescale_em_${d}_${prod}.sh
	    cat ../concat_uncertainty_step1_rescale_em.py | sed s/REPLACE1/${d}/ | sed s/REPLACE2/${prod}/ > ../concat_uncertainty_step1_rescale_em_${d}_${prod}.py
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for d in {0..3}
    do
	for prod in lsm all
	do
	    rm concat_uncertainty_step1_rescale_em_${d}_${prod}.sh
	    rm ../concat_uncertainty_step1_rescale_em_${d}_${prod}.py
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for d in {0..3}
    do
	for prod in lsm all
	do
	    qsub concat_uncertainty_step1_rescale_em_${d}_${prod}.sh
	done
    done
fi
