create=0
if [ ${create} -eq 1 ]
then
    for i in {0..3}
    do
	for j in True False
	do
	    cat em_tas_corr_test_area_all_merge_cmip_template.sh | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ > em_tas_corr_test_area_all_merge_cmip_${i}_${j}.sh
	    cat ../em_tas_corr_test_area_all_merge_cmip.py | sed s/REPLACE1/${i}/ | sed s/REPLACE2/${j}/ > ../em_tas_corr_test_area_all_merge_cmip_${i}_${j}.py
	done
    done
fi


clean=1
if [ ${clean} -eq 1 ]
then
    for i in {0..3}
    do
	for j in True False
	do
	    rm em_tas_corr_test_area_all_merge_cmip_${i}_${j}.sh
	    rm ../em_tas_corr_test_area_all_merge_cmip_${i}_${j}.py
	done
    done
fi


submit=0
if [ ${submit} -eq 1 ]
then
    for i in {0..3}
    do
	for j in True False
	do
	    qsub em_tas_corr_test_area_all_merge_cmip_${i}_${j}.sh
        done
    done
fi
