#PBS -S /bin/bash
#PBS -A ACF-UTK0011
#PBS -l nodes=1:ppn=16,walltime=14:00:00

cd ~/Git/soil_moisture_merge

ipython -i validate_anomalies.py
