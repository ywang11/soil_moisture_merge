#PBS -S /bin/bash
#PBS -A ACF-UTK0011
#PBS -l nodes=1,walltime=06:00:00
#PBS -o interp_CMIP5_historical_REPLACE.o$PBS_JOBID

cd $PBS_O_WORKDIR

cd ../interpolation

conda activate base

ncl interp_CMIP5_historical_REPLACE.ncl
