"""
20191209
ywang254@utk.edu

Indicate the consistency in sign and magnitude with mean_cmip6 by
 [mean_lsm, mean_cmip5, dolce_products]
 and
 [dolce, em] x [lsm, cmip5, cmip6]
"""
import xarray as xr
import matplotlib.pyplot as plt
import os
from utils_management.constants import depth, depth_cm
import cartopy.crs as ccrs
import utils_management as mg
import numpy as np
import itertools as it


product_list = ['mean_lsm', 'mean_cmip5', 'dolce_products',
                'concat_dolce_lsm', 'dolce_cmip5', 'dolce_cmip6',
                'concat_em_lsm', 'em_cmip5', 'em_cmip6']
year_str = '1950-2016'


for i, season in it.product(range(4), ['Annual', 'DJF', 'JJA', 'MAM', 'SON']):
    d = depth[i]
    dcm = depth_cm[i]

    data = xr.open_dataset(os.path.join(mg.path_out(), 
                                        'other_diagnostics_variant_trend',
                                        'mean_cmip6_' + year_str + '_' + \
                                        d + '_g_map_trend_' + season + '.nc'))
    ref = data['g_map_trend'].values.copy()
    data.close()
    
    
    xand = {}
    for pp in product_list:
        # Consistency in sign with mean_cmip6
        data2 = xr.open_dataset(os.path.join(mg.path_out(),
                                             'other_diagnostics_variant_trend',
                                             pp + '_' + year_str + '_' + d + \
                                             '_g_map_trend_' + season + '.nc'))
        xand[pp] = np.logical_or(np.logical_and(data2['g_map_trend'].values>0.,
                                                ref > 0.),
                                 np.logical_and(data2['g_map_trend'].values<0.,
                                                ref < 0.) )
        data2.close()
    
    
    
    # Each product agreeing on the trend.
    fig, axes = plt.subplots(ncols = 3, nrows = 3, figsize = (20, 12),
                             subplot_kw = {'projection': ccrs.PlateCarree()})
    for ppi,pp in enumerate(product_list):
        ax = axes.flat[ppi]
    
        ax.coastlines()
        h = ax.contourf(data.lon.values, data.lat.values, 
                        xand[pp].astype(float),
                        levels = [-0.5, 0.5, 1.5], cmap = 'Greys')
        #plt.colorbar(h, ax = ax, boundaries = [-0.5, 0.5, 1.5],
        #             ticks = [0, 1])
        ax.set_title(pp)
    fig.savefig(os.path.join(mg.path_out(), 'other_diagnostics_variant_trend',
                             'same_sign_as_mean_cmip6_' + season + '_' + \
                             dcm + '.png'), dpi = 600.)
    plt.close(fig)


    # All products aggreeing on the trend. Note the mean_* are not products.
    for pp_ind, pp in enumerate(product_list):
        if pp_ind == 0:
            xand_all = xand[pp]
        else:
            xand_all = np.logical_and( xand[pp], xand_all )
    
    
    fig, ax = plt.subplots(figsize = (20, 12),
                           subplot_kw = {'projection': ccrs.PlateCarree()})
    ax.coastlines()
    ax.contourf(data.lon.values, data.lat.values, xand_all.astype(float),
                levels = [-0.5, 0.5, 1.5], cmap = 'Greys')
    fig.savefig(os.path.join(mg.path_out(), 'other_diagnostics_variant_trend',
                             'same_sign_as_mean_cmip6_all_' + season + \
                             '_' + dcm + '.png'), dpi = 600.)
    plt.close(fig)
