"""
2019/08/09
ywang254@utk.edu

Interpolate all the available CMIP6 files for the layer-wise soil moisture
 (mrsol)
"""
import utils_management as mg
import os
import pandas as pd


create = False
clean = True
submit = False


land_mask = 'None'
template_ncl_name = './interpolation/interp_CMIP6_hist_mrsol_template.ncl'
template_sh_name = './bash_submit/interp_CMIP6_hist_mrsol_template.sh'

expr = 'historical'

# Get the available CMIP6 models.
cmip6_model_path = mg.path_to_cmip6(expr, 'mrsol')
v = 'r1i1p1f1'

if create:
    ###########################################################################
    # Modify the NCL template file.
    ###########################################################################
    f = open(template_ncl_name, 'r')
    template = f.read()
    for model in cmip6_model_path.keys():
        #for v in cmip6_model_path[model].keys():
        path_out = os.path.join(mg.path_intrim_out(),
                                'Interp_Merge', land_mask, 'CMIP6',
                                model + '_' + v)
        if not os.path.exists(path_out):
            os.mkdir(path_out)

        template_p = template.replace('REPLACE1', model \
        ).replace('REPLACE2', v).replace('REPLACE3', expr)

        f2 = open(template_ncl_name.replace('template', model + '_' + \
                                            expr + '_' + v), 'w')
        f2.write(template_p)
        f2.close()
    f.close()


    ###########################################################################
    # Modify the .sh template file. Because the server does not allow many
    # jobs, put everything into the same script.
    ###########################################################################
    f = open(template_sh_name, 'r')
    template = f.read()
    for model in cmip6_model_path.keys():
        #for v in cmip6_model_path[model].keys():
        template2 = template + '\n' + 'ncl interp_CMIP6_hist_mrsol_' + \
                    model + '_' + expr + '_' + v + '.ncl\n'
        f2 = open(template_sh_name.replace('template', model + '_' + \
                                           expr + '_' + v), 'w')
        f2.write(template2)
        f2.close()
    f.close()


if clean:
    for model in cmip6_model_path.keys():
        #for v in cmip6_model_path[model].keys():
        os.remove(template_ncl_name.replace('template',
                                            model + '_' + expr + '_' + v))
        os.remove(template_sh_name.replace('template', model + '_' + \
                                           expr + '_' + v))


if submit:
    for model in cmip6_model_path.keys():
        #for v in cmip6_model_path[model].keys():
        os.system('qsub ' + \
                  template_sh_name.replace('template', 
                                           model + '_' + expr + '_' + v))
