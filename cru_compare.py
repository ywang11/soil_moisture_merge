"""
2019/06/05

ywang254@utk.edu

The TRENDY and MsTMIP driven precipitation use CRUv3.2, which is only until
2010. 

This script compares the global mean and latitude-band time series between
CRUv3.2 and CRUv4.03 between 1950 and 2011.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import path_to_pr
import os
import numpy as np
import sys

if sys.platform == 'linux':
    os.environ['QT_QPA_PLATFORM']='offscreen'

import matplotlib.pyplot as plt
from matplotlib import cm
from datetime import datetime


lat_bnds = np.arange(-60., 91., 10.).reshape(-1,1)
lat_bnds = np.concatenate([lat_bnds[:-1, :], 
                           lat_bnds[1:, :]], axis=1)


# CRU v3.2
cru_v3 = xr.open_mfdataset(path_to_pr['CRU_v3.2'])

pre_v3 = cru_v3.pre[(cru_v3.time >= np.datetime64('1950-01-01')) & \
                    (cru_v3.time <= np.datetime64('2011-12-31')), :, :]

# ----- extract the time period
pre_time = pre_v3.time.values.copy()

# ----- extract the mean
pre_v3_mean = pre_v3.mean(dim='time')

# ----- extract by 10deg latitude bands
pre_v3_sre = np.empty([len(pre_time), lat_bnds.shape[0]], dtype=float)
for i in range(lat_bnds.shape[0]):
    pre_v3_sre[:,i] = np.nanmean(np.nanmean(pre_v3.values[:,
        ((cru_v3.lat >= lat_bnds[i,0]) & (cru_v3.lat <= lat_bnds[i,1])), :],
                                            axis=2), axis=1)

cru_v3.close()


# CRU v4.03
cru_v4 = xr.open_mfdataset(path_to_pr['CRU_v4.03'])

pre_v4 = cru_v4.pre[(cru_v4.time >= np.datetime64('1950-01-01')) & \
                    (cru_v4.time <= np.datetime64('2011-12-31')), :, :]

# ----- extract the mean
pre_v4_mean = pre_v4.mean(dim='time')

# ----- extract by 10deg latitude bands
pre_v4_sre = np.empty([len(pre_time), lat_bnds.shape[0]], dtype=float)
for i in range(lat_bnds.shape[0]):
    pre_v4_sre[:,i] = np.nanmean(np.nanmean(pre_v4.values[:,
        ((cru_v4.lat >= lat_bnds[i,0]) & (cru_v4.lat <= lat_bnds[i,1])), :],
                                            axis=2), axis=1)

cru_v4.close()



# Plot the comparison.
# ---- global map
fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(12,12))
ax = axes[0,0]
hl = ax.contourf(pre_v3_mean)
plt.colorbar(hl, ax=ax)
ax.set_title('CRUv3.2 (mm/month)')
ax = axes[0,1]
hl = ax.contourf(pre_v4_mean)
ax.set_title('CRUv4.03 (mm/month)')
plt.colorbar(hl, ax=ax)
ax = axes[1,0]
hl = ax.contourf(pre_v4_mean - pre_v3_mean)
ax.set_title('CRUv4.03 - CRUv3.2 (mm/month)')
plt.colorbar(hl, ax=ax)
fig.savefig(os.path.join(mg.path_intrim_out(), 'em_compare_cru',
                         'map.png'), dpi=600.)


# ----- time series
fig, axes = plt.subplots(nrows=5, ncols=3, figsize=(12,20))
fig.subplots_adjust(hspace=0.3)
for i in range(15):
    ax = axes.flat[i]
    ##ax.plot(pre_time.astype(datetime), pre_v3_sre[:, i], '-r')
    ##ax.plot(pre_time.astype(datetime), pre_v4_sre[:, i], '-b')
    ax.plot(pre_v3_sre[:, i], pre_v4_sre[:, i], 'ob')
    ax.set_xlabel('CRUv3.2 (mm/month)')
    ax.set_ylabel('CRUv4.03 (mm/month)')
    ax.set_title( ('%.1f' % lat_bnds[i,0]) + ' - ' + \
                  ('%.1f' % lat_bnds[i,1]) )
fig.savefig(os.path.join(mg.path_intrim_out(), 'cru_compare', 
                         'ts.png'), dpi=600.)
