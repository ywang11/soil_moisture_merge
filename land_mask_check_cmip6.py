"""
2019/07/14

ywang254@utk.edu

Combine the land mask of the interpolated CMIP6 data to see 
if they are the same between different CMIP6 models and the interpolated
precipitation & temperature data.

Switch the land mask between None, SYNAMP, and e1m to check either interpolated
data before applying the land mask, or after applying different land masks.
"""
import sys
import os
import xarray as xr
import numpy as np
import pandas as pd
from glob import glob
import utils_management as mg
from utils_management.constants import year_cmip6, cmip6_list


expr = 'historical'
var = 'mrsol'
model_version = cmip6_list(expr, var)


if (var == 'mrsos') | (var == 'mrsol') | (var == 'mrlsl'):
    nc_var = 'sm'
else:
    nc_var = var


path_merge = os.path.join(mg.path_intrim_out(), 'Interp_Merge')


target_lat = np.arange(-89.75, 89.76, 0.5)
target_lon = np.arange(-179.75, 179.76, 0.5)


land_mask = 'vanilla'


mask_all = np.zeros([len(model_version), 360, 720], dtype = int)
for m_v_ind, m_v in enumerate(model_version):
    path_model = os.path.join(path_merge, land_mask, 'CMIP6', m_v)
    file_list = glob(path_model + '/' + var + '_' + expr + '*.nc')
    f = os.path.join(os.path.join(path_model, file_list[0]))
    data = xr.open_dataset(f, decode_times = False)
    mask_all[m_v_ind, :, :] = ~np.isnan(data[nc_var][0,:,:])
    data.close()


# Check if all the models have the same mask.
mask_check = np.max(mask_all, axis=0) - np.min(mask_all, axis=0)


# Check at which grid cells the models differ. 
mask_differ = np.where(mask_check > 1e-6)


# Check the model values at the different grid cells.
for i,j in zip(mask_differ[0], mask_differ[1]):
    data = pd.Series(data = mask_all[:,i,j], index = model_all)
    print(data)
    input('Press Enter to go to the next grid cell.')
