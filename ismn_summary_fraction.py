"""
Number of ISMN stations by land use and comparison to global land use fraction.
# Number of ISMN stations in SREX regions.
"""
from misc.ismn_utils import get_weighted_monthly_data, get_ismn_aggr_method
from utils_management.constants import depth, depth_cm, lu_names
import utils_management as mg
import os
import itertools as it
import xarray as xr
import numpy.ma as ma
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import colors, cm


simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = [get_ismn_aggr_method(simple[iam], dominance_lc[iam],
                                         dominance_threshold) \
                    for iam in range(len(simple))]


###############################################################################
# Land cover count
###############################################################################
# Load land cover base dataset.
data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'lu_aggr', 
                                    'mcd12c1_0.05_major.nc'))
land_use_modis = data.__xarray_dataarray_variable__.values.copy()
modis_lat = data.lat.values
modis_lon = data.lon.values
data.close()


# Bin count of the global land cover.
land_use_freq_global = np.bincount(land_use_modis.reshape(-1))
land_use_id = np.where(land_use_freq_global > 0)[0]

#
ismn_land_use_freq = pd.DataFrame(data = np.nan, index = \
                                  ['MODIS'] + \
                                  [x1+'_'+x2+'_'+x3+'_'+x4 for x1,x2,x3,x4 in \
                                   it.product(['cal','val','all'],
                                              ismn_aggr_method, depth,
                                              ['weighted', 'unweighted'])],
                                  columns = ['%d' % x for x in land_use_id])
ismn_land_use_freq.loc['MODIS', :] = land_use_freq_global[land_use_id] / \
                                     sum(land_use_freq_global)

#
for opt, iam, d in it.product(['cal','val','all'], ismn_aggr_method, depth):
    grid_latlon, weighted_monthly_data, _ = get_weighted_monthly_data( \
        os.path.join(mg.path_intrim_out(), 'ismn_aggr'), iam, d, opt)

##    if (iam == 'lu_weighted') & (opt == 'all'):
##        print(d)
##        print(np.sum(~np.isnan(weighted_monthly_data.values)))

    # Create a mask: 0 where there is no ISMN station, 
    #               >0 number indicating the total year-month of 
    #                  observations at the grid cell.
    ismn_mask = np.zeros([len(modis_lat), len(modis_lon)], dtype=int)

    for c in weighted_monthly_data.columns:
        ismn_mask[np.argmin(np.abs(modis_lat - \
                                   grid_latlon.loc['Lat',c])),
                  np.argmin(np.abs(modis_lon - \
                                   grid_latlon.loc['Lon',c]))] = \
        sum(~np.isnan(weighted_monthly_data[c].values))

    # Determine the bin count of the land use at the ISMN stations.
    temp = np.bincount(land_use_modis[ismn_mask > 0].reshape(-1))
    if len(temp) < len(land_use_freq_global):
        temp = np.concatenate([temp, np.zeros(len(land_use_freq_global) - \
                                              len(temp))])
    ismn_land_use_freq.loc[opt+'_'+iam+'_'+d+'_unweighted', :] = \
        temp[land_use_id] / sum(temp)

    # ---- consider the number of months of available data.
    temp = np.bincount(land_use_modis[ismn_mask > 0].reshape(-1),
                       ismn_mask[ismn_mask > 0].reshape(-1))
    if len(temp) < len(land_use_freq_global):
        temp = np.concatenate([temp, np.zeros(len(land_use_freq_global) - \
                                              len(temp))])
    ismn_land_use_freq.loc[opt+'_'+iam+'_'+d+'_weighted', :] = \
        temp[land_use_id] / sum(temp)

# Save to file.
ismn_land_use_freq.to_csv(os.path.join(mg.path_intrim_out(), 'ismn_summary',
                                       'ismn_land_use_freq.csv'))
#ismn_srex_freq.to_csv(os.path.join(mg.path_intrim_out(), 'ismn_summary',
#                                   'ismn_srex_freq.csv'))


###############################################################################
# Visualize the distribution.
###############################################################################
ismn_land_use_freq = pd.read_csv(os.path.join(mg.path_intrim_out(), 
                                              'ismn_summary',
                                              'ismn_land_use_freq.csv'),
                                 index_col = 0)


# Land use tye
fig, ax = plt.subplots(figsize = (10,10))
h = ax.imshow(ismn_land_use_freq.T, cmap = 'RdYlBu', aspect = 2,
              norm = colors.LogNorm())
plt.colorbar(h, ax = ax, shrink = 0.3)
ax.set_yticks(range(ismn_land_use_freq.shape[1]))
ax.set_yticklabels([lu_names[int(i)] for i in ismn_land_use_freq.columns])
ax.set_xticks([0, 35])
ax.set_xticklabels(['MODIS', 'ISMN Station - Different Subsets'])
ax.set_xlim([-1, ismn_land_use_freq.shape[0]])
fig.savefig(os.path.join(mg.path_intrim_out(), 'ismn_summary',
                         'ismn_land_use_freq.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)

# ----- focusing on all stations, weighted by the number of months.
data = ismn_land_use_freq.loc[['MODIS'] + ['all_lu_weighted_' + d + \
                                           '_weighted' for d in depth], :]
# ----- drop permanent snow and ice
data.drop('15', axis = 1, inplace = True)
# ----- rename Barren to 15
data.columns = [str(x) for x in range(16)]

mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 6
fig, axes = plt.subplots(ncols = 1, nrows = 2, figsize = (6.5, 3))
fig.subplots_adjust(hspace = 0.08)
cmap = cm.get_cmap('hsv')
clist = [cmap((i+0.5)/(len(depth))) for i in range(len(depth))]
x = np.arange(1, 16)
width = 0.1
# ---- Bottom part
ax = axes[1]
lab = 'MODIS'
ax.bar(x, data.loc[lab, :].values[1:], width, label = 'MODIS', 
       fc = [0.5, 0.5, 0.5])
for lind, lab in enumerate(data.index[1:], 1):
    # Skip ocean
    ax.bar(x + 1.5 * lind * width, data.loc[lab, :].values[1:], width,
           label = depth_cm[lind-1], fc = clist[lind-1])
ax.spines['top'].set_visible(False)
ax.set_ylim([0., 0.08])
ax.set_xticks(np.arange(0.8, 16.8))
ax.set_xticks(np.arange(1.3, 16.3, 1), minor = True)
ax.set_xlim([0.8, 15.8])
xticks = []
for i in range(1,16):
    if i == 15:
        tmp = ['Barren']
    else:
        tmp = lu_names[i].split(' ')
    xticks.append( tmp[0] + '\n' + ' '.join(tmp[1:]) )
ax.set_xticklabels([])
ax.tick_params('x', which = 'minor', length = 0)
ax.set_xticklabels(xticks, rotation = 90, minor = True)
# ---- Top part
ax = axes[0]
lab = 'MODIS'
ax.bar(x, data.loc[lab, :].values[1:], width, label = 'MODIS',
       fc = [0.5, 0.5, 0.5])
for lind, lab in enumerate(data.index[1:], 1):
    # Skip ocean
    ax.bar(x + 1.5 * lind * width, data.loc[lab, :].values[1:], width,
           label = depth_cm[lind-1], fc = clist[lind-1])
ax.set_ylim([0.25, 0.45])
ax.spines['bottom'].set_visible(False)
ax.set_xticklabels([])
ax.set_xticks([])
ax.set_xlim([0.8, 15.8])

# ---- Separator
d = .015  # how big to make the diagonal lines in axes coordinates
# arguments to pass to plot, just so we don't keep repeating them
ax = axes[0]
kwargs = dict(transform=ax.transAxes, color='k', clip_on=False)
ax.plot((-d, +d), (-d, +d), **kwargs)        # top-left diagonal
ax.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal

ax = axes[1]
kwargs.update(transform=ax.transAxes)  # switch to the bottom axes
ax.plot((-d, +d), (1 - d, 1 + d), **kwargs)  # bottom-left diagonal
ax.plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs)  # bottom-right diagonal

axes[0].legend(loc = 'upper left', ncol = 3)
fig.savefig(os.path.join(mg.path_intrim_out(), 'ismn_summary',
                         'ismn_land_used_freq_1.png'), 
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)
