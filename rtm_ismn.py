""" Perform the homogeneity test using independent evaluation data. """
import utils_management as mg
from utils_management.constants import depth, year_longest, depth_cm
from misc.ismn_get_monthly import ismn_get_monthly
from misc.ismn_utils import get_ismn_aggr_method
from misc.homogeneity import calc_q_test
import os
import pandas as pd
import numpy as np
import time
import xarray as xr
from datetime import datetime
import itertools as it
from scipy.stats import pearsonr, linregress, ranksums, fligner
from glob import glob
from dateutil.relativedelta import relativedelta


def decode_month_since(time):
    ref = pd.Timestamp(time.attrs['units'].split(' ')[2])
    start = ref + relativedelta(months = time.values[0])
    start = start.replace(day = 1, hour = 0, minute = 0, second = 0)
    return pd.date_range(start, periods = len(time), freq = 'MS')


def get_eval_data(name):
    land_mask = 'vanilla'
    if 'SoMo' in name:
        flist = sorted(glob(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                         land_mask, name.split('_')[0],
                                         name.split('_')[0] + '_*_' + \
                                         name.split('_')[1] + '.nc')))
    elif name == 'GLEAMv3.3a_0-100cm':
        flist = sorted(glob(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                         land_mask, 'GLEAMv3.3a', 'GLEAMv3.3a_*_0-100cm.nc')))
    elif name == 'SMERGE_v2':
        flist = sorted(glob(os.path.join(mg.path_intrim_out(),
                                         'Interp_Merge',
                                         land_mask, name, name + '*.nc')))
    ##print(flist)
    hr = xr.open_mfdataset(flist, decode_times = False)
    if 'month' in hr['time'].attrs['units']:
        tvec = decode_month_since(hr['time'])
    else:
        tvec = xr.decode_cf(hr)['time'].to_index()
    sm = hr['sm'].copy(deep = True)
    sm['time'] = tvec
    hr.close()
    return sm


def get_prod_data(prod, d, dcm):
    path_prod = mg.path_to_final_prod(prod, d, dcm)
    hr = xr.open_dataset(path_prod, decode_times = False)
    hr['time'] = pd.date_range('1950-01-01', '2016-12-31', freq = 'MS')
    if 'em' in prod:
        sm = hr['predicted'].copy(deep = True)
    else:
        sm = hr['sm'].copy(deep = True)
    sm = sm[sm['time'].to_index().year >= 1970, :, :]
    hr.close()
    return sm


def point_q_test(pp, ee, tvec, year_of_break):
    if (len(pp) == 0) | (len(ee) == 0):
        return np.nan, np.nan
    elif np.all(np.isnan(pp)) | np.all(np.isnan(ee)):
        return np.nan, np.nan
    elif (tvec[0].year >= year_of_break) | (tvec[-1].year <= year_of_break):
        return np.nan, np.nan
    else:
        matched = pd.DataFrame(np.vstack((pp, ee)).T,
                               index = tvec, columns = ['target', '0'])
        ##print(matched.iloc[:10, :])
        wx_pval, fl_pval = calc_q_test(matched, year_of_break)
        return wx_pval, fl_pval

#
eval_list = ['SoMo_0-10cm', 'SoMo_10-30cm', 'SMERGE_v2',
             'SoMo_30-50cm', 'GLEAMv3.3a_0-100cm']
evl = eval_list[0] # eval_list[REPLACE1]
eval_data = get_eval_data(evl)


prod = 'mean_lsm' # 'REPLACE2'
if 'SoMo' in evl:
    dcm = evl.split('_')[1]
    d = depth[np.where([dd == dcm for dd in depth_cm])[0][0]]
    prod_data = get_prod_data(prod, d, dcm)
elif evl == 'SMERGE_v2':
    a = get_prod_data(prod, '0.00-0.10', '0-10cm')
    b = get_prod_data(prod, '0.10-0.30', '10-30cm')
    c = get_prod_data(prod, '0.30-0.50', '30-50cm')
    prod_data = (a * 0.1 + b * 0.2 + c * 0.1) / 0.4
else:
    a = get_prod_data(prod, '0.00-0.10', '0-10cm')
    b = get_prod_data(prod, '0.10-0.30', '10-30cm')
    c = get_prod_data(prod, '0.30-0.50', '30-50cm')
    d = get_prod_data(prod, '0.30-0.50', '50-100cm')
    prod_data = a * 0.1 + b * 0.2 + c * 0.2 + d * 0.5


#
time_ranges = [pd.date_range('1970-01-01', '1980-12-31', freq = 'MS'),
               pd.date_range('1981-01-01', '2010-12-31', freq = 'MS'),
               pd.date_range('2011-01-01', '2016-12-31', freq = 'MS')]

#
for period in range(1, len(time_ranges)):
    prod_data_temp = prod_data[(prod_data['time'].to_index().year >= \
                                max(time_ranges[period-1][0].year,
                                    eval_data['time'].to_index().year[0])) & \
                               (prod_data['time'].to_index().year <= \
                                min(time_ranges[period][-1].year,
                                    eval_data['time'].to_index().year[-1])), :, :]
    eval_data_temp = eval_data[(eval_data['time'].to_index().year >= \
                                max(time_ranges[period-1][0].year,
                                    prod_data['time'].to_index().year[0])) & \
                               (eval_data['time'].to_index().year <= \
                                min(time_ranges[period][-1].year,
                                    prod_data['time'].to_index().year[-1])), :, :]

    #
    wx_pval, fl_pval = xr.apply_ufunc(point_q_test,
                                      prod_data_temp.chunk({'time':-1}),
                                      eval_data_temp.chunk({'time':-1}),
                                      input_core_dims = [['time'], ['time']],
                                      output_core_dims = [[], []],
                                      kwargs = {'tvec': prod_data_temp['time'].to_index(), 
                                                'year_of_break': time_ranges[period][0].year},
                                      dask = 'parallelized',
                                      vectorize = True,
                                      output_dtypes = [float, float])
    wx_pval.compute()
    fl_pval.compute()

    #
    ds = xr.Dataset()
    ds.update({'wx_pval': wx_pval, 'fl_pval': fl_pval})
    ds['lat'].encoding['_FillValue'] = None
    ds['lon'].encoding['_FillValue'] = None
    ds['wx_pval'].encoding['_FillValue'] = -1e32
    ds['fl_pval'].encoding['_FillValue'] = -1e32
    ds.to_netcdf(os.path.join(mg.path_out(), 'homogeneity_test',
                              'rtm_' + str(period) + '_' + \
                              prod + '_' + evl + '.nc'))
    
