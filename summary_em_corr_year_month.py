"""
2020/05/19

ywang254@utk.edu

Draw the global maps of the emergent constraint slopes, when based on the range
 of land surface models, and the significance of the slopes.
"""
import numpy as np
import xarray as xr
import utils_management as mg
from utils_management.constants import depth, year_longest, year_shorter, \
    year_shorter2, year_shortest, depth_cm, target_lat, target_lon
import sys
import os
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from misc.plot_utils import plot_sm_map, plot_map_w_stipple
import itertools as it
from misc.plot_utils import cmap_gen
import multiprocessing as mp


cmap = cmap_gen('autumn', 'winter_r')
met_obs = 'CRU_v4.03'

me = 'year_month_anomaly_9grid'
year = year_longest
year_str = str(year[0]) + '-' + str(year[-1])


year_str_list1 = [str(year_shorter[0]) + '-' + str(year_shorter[-1]),
                  str(year_shortest[0]) + '-' + str(year_shortest[-1]),
                  str(year_shorter2[0]) + '-' + str(year_shorter2[-1])]
year_used_list1 = [range(1950,1981), range(1981, 2011), range(2011, 2017)]


for model, i in it.product(['lsm', 'cmip5', 'cmip6', '2cmip', 'all'],
                           range(4)):
    d = depth[i]
    dcm = depth_cm[i]

    if (model == 'lsm') | (model == 'all'):
        corr_p = np.full([len(year), 12, len(target_lat), len(target_lon)],
                         np.nan)
        corr_t = np.full([len(year), 12, len(target_lat), len(target_lon)],
                         np.nan)
        em_not_applied_p = np.full([len(year), 12, len(target_lat), 
                                    len(target_lon)], np.nan)
        em_not_applied_t = np.full([len(year), 12, len(target_lat),
                                    len(target_lon)], np.nan)
        for yind, yy in enumerate(year):
            if yy in year_used_list1[0]:
                which = 0
            elif yy in year_used_list1[1]:
                which = 1
            else:
                which = 2
            hr = xr.open_dataset(os.path.join(mg.path_out(),
                'em_' + model + '_corr', met_obs + '_' + me + '_' + \
                dcm + '_' + year_str_list1[which], 'corr_' + str(yy) + '.nc'))
            # ---- precipitation
            corr_p[yind, :, :, :] = hr['corr'][1,:,:,:].values.copy()
            # ---- temperature
            corr_t[yind, :, :, :] = hr['corr'][2,:,:,:].values.copy()
            hr.close()

            hr = xr.open_dataset(os.path.join(mg.path_out(),
                'em_' + model + '_corr', met_obs + '_' + me + '_' + \
                dcm + '_' + year_str_list1[which],
                'em_not_applied_' + str(yy) + '.nc'))
            # ---- precipitation
            em_not_applied_p[yind, :, :, :] = \
                hr['em_not_applied'][0,:,:,:].copy(deep = True)
            # ---- temperature
            em_not_applied_t[yind, :, :, :] = \
                hr['em_not_applied'][1,:,:,:].copy(deep = True)
            hr.close()
        corr_p = corr_p.reshape(len(year) * 12, len(target_lat),
                                len(target_lon))
        corr_t = corr_t.reshape(len(year)* 12, len(target_lat),
                                len(target_lon))
        em_not_applied_p = em_not_applied_p.reshape(len(year) * 12,
                                                    len(target_lat),
                                                    len(target_lon))
        em_not_applied_t = em_not_applied_t.reshape(len(year) * 12,
                                                    len(target_lat),
                                                    len(target_lon))
    else:
        #
        hr = xr.open_mfdataset([os.path.join(mg.path_out(),
                                             'em_' + model + '_corr',
                                             met_obs + '_' + me + '_' + \
                                             dcm + '_' + year_str,
                                             'corr_' + str(yy) + '.nc') \
                                for yy in year], decode_times = False,
                               concat_dim = 'time')
        # ---- precipitation
        corr_p = hr['corr'][1,:,:,:].values.copy()
        # ---- temperature
        corr_t = hr['corr'][2,:,:,:].values.copy()
        hr.close()

        #
        hr = xr.open_mfdataset([os.path.join(mg.path_out(),
                                             'em_' + model + '_corr',
                                             met_obs + '_' + me + '_' + \
                                             dcm + '_' + year_str,
                                             'em_not_applied_' + \
                                             str(yy) + '.nc') \
                                for yy in year])
        # ---- precipitation
        em_not_applied_p = hr['em_not_applied'][0,:,:,:].values.copy()
        # ---- temperature
        em_not_applied_t = hr['em_not_applied'][1,:,:,:].values.copy()
        hr.close()


    ###########################################################################
    # Separately plot the percentage applied and not applied
    # summed over all years and month for brevity.
    ###########################################################################
    corr_p = np.where(em_not_applied_p < 0.5, corr_p, np.nan)
    corr_t = np.where(em_not_applied_t < 0.5, corr_t, np.nan)

    # ---- Average over years and months
    corr_p = np.nanmean(corr_p, axis = 0)
    corr_t = np.nanmean(corr_t, axis = 0)

    # ---- The percentage applied over years and months
    em_applied_p = np.sum(em_not_applied_p < 0.5, axis = 0) / len(year) / 12
    em_applied_t = np.sum(em_not_applied_t < 0.5, axis = 0) / len(year) / 12

    xr.Dataset({'corr_p': (['lat','lon'], corr_p),
                'corr_t': (['lat','lon'], corr_t),
                'em_applied_p': (['lat','lon'], em_applied_p),
                'em_applied_t': (['lat','lon'], em_applied_t)},
               coords = {'lat': target_lat, 'lon': target_lon} \
    ).to_netcdf(os.path.join(mg.path_out(), 'summary_em_corr',
                             model + '_' + dcm + '.nc'))
