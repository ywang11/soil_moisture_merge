"""
2020/12/10
Examine the results of standard_diagnostics2.

Use the month with maximum soil moisture to indicate seasonality, if there 
 are multiple peaks (i.e. greater than the two adjacent months), hatch 
 with dot.


Still need to double check the correctness. 

"""
import xarray as xr
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import cartopy.crs as ccrs
import itertools as it
import utils_management as mg
from utils_management.constants import depth, depth_cm
from misc.plot_utils import stipple, cmap_div


def seasonal_metric(data):
    mmax = xr.apply_ufunc(np.argmax, data,
                          input_core_dims = [['month']],
                          dask = 'allowed', vectorize = True)
    mmax = mmax.where(~np.isnan(data[0,:,:]))

    local_max = np.full(data.shape, False)
    for ii in range(12):
        if ii == 0:
            local_max[ii, :, :] = (data.values[ii,:,:] > \
                                   data.values[ii+1,:,:]) & \
                (data.values[ii,:,:] > data.values[11,:,:])
        elif ii == 11:
            local_max[ii, :, :] = (data.values[ii,:,:] > \
                                   data.values[0,:,:]) & \
                (data.values[ii,:,:] > data.values[ii-1,:,:])
        else:
            local_max[ii, :, :] = (data.values[ii,:,:] > \
                                   data.values[ii+1,:,:]) & \
                (data.values[ii,:,:] > data.values[ii-1,:,:])
    nmax = xr.DataArray(np.sum(local_max, axis = 0),
                        dims = ['lat', 'lon'],
                        coords = {'lat': data['lat'],
                                  'lon': data['lon']})
    nmax = nmax.where(~np.isnan(data[0,:,:]))
    return mmax, nmax


prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all']
prod_name_list = ['Mean ORS', 'OLC ORS', 'EC ORS', 'EC CMIP5', 'EC CMIP6',
                  'EC CMIP5+6', 'EC ALL']
lab = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
       'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
       'y', 'z', 'aa', 'bb', 'cc', 'dd', 'ee', 'ff', 'gg', 'hh',
       'ii', 'jj', 'kk', 'll', 'mm', 'nn', 'oo', 'pp', 'qq', 'rr',
       'ss', 'tt', 'uu', 'vv', 'ww', 'xx']


mpl.rcParams['font.size'] = 6.
mpl.rcParams['font.size'] = 6.
mpl.rcParams['hatch.linewidth'] = 0.5
cmap = cmap_div()
for stat in ['climatology', 'seasonal', 'trend', 'anomalies_std']:
    fig, axes = plt.subplots(len(prod_list), 4, figsize = (6.5, 6.5),
                             subplot_kw = {'projection': ccrs.Miller()})
    fig.subplots_adjust(wspace = 0.01, hspace = 0.01)
    count = 0
    for pind, dind in it.product(range(len(prod_list)), range(len(depth))):
        prod = prod_list[pind]
        d = depth[dind]
        dcm = depth_cm[dind]

        ax = axes.flat[count]
        ax.coastlines(lw = 0.5)
        ax.set_extent([-180, 180, -60, 90])

        #
        hr = xr.open_dataset(os.path.join(mg.path_out(), 
                                          'standard_diagnostics2',
                                          prod + '_' + dcm + '_1970-2016.nc'))
        if stat == 'climatology':
            cf = ax.contourf(hr['lon'], hr['lat'], 
                             hr['seasonal'].mean(dim = 'month'),
                             cmap = 'Spectral',
                             levels = np.linspace(0., 0.45, 41),
                             extend = 'max',
                             transform = ccrs.PlateCarree())
        elif stat == 'seasonal':
            mmax, nmax = seasonal_metric(hr['seasonal'])

            cf = ax.contourf(hr['lon'], hr['lat'], mmax + 1, 
                             levels = np.arange(0.5, 12.6),
                             cmap = 'hsv', extend = 'neither',
                             alpha = 0.5, 
                             transform = ccrs.PlateCarree())
            stipple(ax, hr['lat'], hr['lon'], - nmax, -1,
                    ccrs.PlateCarree())
        elif stat == 'trend':
            cf = ax.contourf(hr['lon'], hr['lat'], hr['trend'],
                             cmap = cmap, 
                             levels = np.linspace(-4e-5, 4e-5, 41),
                             extend = 'both', alpha =.5,
                             transform = ccrs.PlateCarree())
            stipple(ax, hr['lat'], hr['lon'], hr['trend_p'].values,
                    0.05, ccrs.PlateCarree())
        else:
            cf = ax.contourf(hr['lon'], hr['lat'], hr['anomalies_std'],
                             cmap = 'Spectral',
                             levels = np.linspace(0., 0.03, 41),
                             extend = 'max',
                             transform = ccrs.PlateCarree())

        hr.close()

        ax.text(0.05, 0.85, '(' + lab[count] + ')',
                transform = ax.transAxes)
        if dind == 0:
            ax.text(-0.1, 0.5, prod_name_list[pind], rotation = 90,
                    verticalalignment = 'center',
                    transform = ax.transAxes)
        if pind == 0:
            ax.set_title(dcm.replace('-', u'\u2212').replace('cm', ' cm'))

        count += 1
    cax = fig.add_axes([0.1, 0.07, 0.8, 0.01])
    if stat == 'seasonal':
        cbar = plt.colorbar(cf, cax = cax, orientation = 'horizontal',
                            ticks = np.arange(1,13))
        cbar.ax.set_xticklabels(['Jan','Feb','Mar','Apr','May','Jun',
                                 'Jul','Aug','Sep','Oct','Nov','Dec'])
    else:
        cbar = plt.colorbar(cf, cax = cax, orientation = 'horizontal')

    fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics2',
                             stat + '.png'), dpi = 600., 
                bbox_inches = 'tight')
    plt.close(fig)
