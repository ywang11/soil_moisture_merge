# -*- coding: utf-8 -*-
"""
20190911

ywang254@utk.edu

Source: 
  Hobeichi et al. - 2018 - Derived Optimal Linear Combination 
    Evapotranspiration (DOLCE) A Global Gridded Synthesis Estimate

Use the weights derived from "cmip6_weights_dolce.py" to calculate the final
  weighted spatiotemporal uncertainty of the CMIP6 models.
"""
import os
import utils_management as mg
import pandas as pd
from utils_management.constants import depth, depth_cm, year_longest, \
    year_cmip6
from misc.ismn_utils import get_weighted_monthly_data, get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import xarray as xr
import numpy as np
import subprocess


# Choose the weighting method that was used to generate the weighted averaged
# ISMN data.
##from ismn_aggr_dolce import simple, dominance_lc, dominance_threshold
ismn_aggr_ind = REPLACE1
simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = get_ismn_aggr_method(simple[ismn_aggr_ind], 
                                        dominance_lc[ismn_aggr_ind],
                                        dominance_threshold)

year = year_longest


# pick the covariance function based on which the weights are calculated
cov_estimer = REPLACE2 ## [0,1,2,3,4]
method = get_cov_method(cov_estimer)

remove_mean = True

land_mask = 'vanilla'

i = REPLACE3
d = depth[i]
dcm = depth_cm[i]

###############################################################################
# Load the weights.
###############################################################################
w_file = os.path.join(mg.path_intrim_out(), 'dolce_cmip6_weights',
                      'weights_' + ismn_aggr_method + '_' + \
                      str(year[0]) + '-' + str(year[-1]) + '_' + method + \
                      '_' + d)
if remove_mean:
    w_file = w_file + '_remove_mean.csv'
else:
    w_file = w_file + '.csv'
weights = pd.read_csv(w_file, index_col = 0)


###############################################################################
# Calculate the uncertainty window.
###############################################################################
#******************************************************************************
# Preliminary data
#******************************************************************************
# ---- observed soil moisture
grid_latlon, weighted_monthly_data, _ = \
    get_weighted_monthly_data(os.path.join(mg.path_intrim_out(), 'ismn_aggr'),
                              ismn_aggr_method, d, opt='cal')
# ---- subset to year_range
weighted_monthly_data = weighted_monthly_data.loc[ \
    (weighted_monthly_data.index >= np.datetime64(str(year[0])+'-01-01')) & \
    (weighted_monthly_data.index <= np.datetime64(str(year[-1])+'-12-31')), :]


# ---- weighted soil moisture at observational data points
weighted_sm_at_data = pd.read_csv(os.path.join(mg.path_out(),
                                               'at_obs_dolce_cmip6',
                                               'weighted_average_' +\
                                               str(year[0]) + '-' +\
                                               str(year[-1]) + '_' + d + '_' +\
                                               ismn_aggr_method + '_' +\
                                               method + '.csv'),
                                  index_col = 0, parse_dates = True)
weighted_sm_at_data = weighted_sm_at_data.loc[weighted_monthly_data.index, :]


# ---- soil moisture of the individual CMIP6 models at the 
#      observational data locations
cmip6_list_1 = mrsol_availability(dcm, land_mask, 'historical')
cmip6_list_2 = mrsol_availability(dcm, land_mask, 'ssp585')
cmip6_list_3 = one_layer_availability('pr', land_mask, 'historical')
cmip6_list_4 = one_layer_availability('pr', land_mask, 'ssp585')
cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) & \
                   set(cmip6_list_3) & set(cmip6_list_4) )
cmip6 = [x for x in cmip6_list if 'r1i1p1f1' in x]

cmip6_sm_at_data = {}
for l in cmip6:
    cmip6_sm_at_data[l] = pd.read_csv(os.path.join( \
        mg.path_intrim_out(), 'at_obs_cmip6', ismn_aggr_method + '_' + l + \
        '_' + d + '.csv'), index_col = 0, parse_dates = True)
    cmip6_sm_at_data[l] = cmip6_sm_at_data[l].loc[weighted_monthly_data.index,
                                                  :]

cmip6_mean_at_data = pd.read_csv(os.path.join(mg.path_out(), \
                                              'at_obs_meanmedian_cmip6',
                                              'mean_' + d + '_' + \
                                              str(year[0]) + '-' + \
                                              str(year[-1]) + '_' + \
                                              ismn_aggr_method + '.csv'),
                                 index_col = 0, parse_dates = True)
cmip6_mean_at_data = cmip6_mean_at_data.loc[weighted_monthly_data.index, :]

# ---- average of the CMIP6 models at all data points
data = xr.open_dataset(os.path.join(mg.path_out(), 'meanmedian_cmip6',
                                    'mean_' + d + '_' + str(year[0]) + '-' + \
                                    str(year[-1]) + '.nc'),
                       decode_times = False)
mean_sm = data.sm.copy(deep = True)
data.close()

# ---- weighted average results
data = xr.open_dataset(os.path.join(mg.path_out(), 'dolce_cmip6_product',
                                    'weighted_average_' + str(year[0]) + \
                                    '-' + str(year[-1]) + '_' + d + '_' + \
                                    ismn_aggr_method + '_' + method + '.nc'),
                       decode_times = False)
weighted_sm = data.sm.copy(deep = True)
data.close()


#******************************************************************************
# Calculation
#******************************************************************************
# ---- K: number of merged original products
K = weights.shape[0]

# ---- J: nubmer of observational points
J = np.sum(~(np.isnan(weighted_monthly_data.values.reshape(-1))))

# ---- alpha: adjustment parameter
if np.min(weights.values) >= 0:
    alpha = 1
else:
    alpha = 1 - K * np.min(weights.values)

# ---- adjusted weight vector
w_tilda = (weights.values.reshape(-1) + (alpha - 1) * \
           np.ones(weights.shape[0], dtype=float) / K) / alpha

# ---- the discrepancy of the weighted data from observation at
#      observed locations
se_sq = np.nansum(np.power((weighted_sm_at_data - \
                            weighted_monthly_data).values.reshape(-1), 
                           2)) / (J-1)

# ---- the beta parameter
beta_denom_inner = np.zeros(K, float)
count = 0
for l in weights.index:
    beta_denom_inner[count] = \
        np.nanmean(np.power(cmip6_mean_at_data.values.reshape(-1) + \
                            alpha*(cmip6_sm_at_data[l].values.reshape(-1) - \
                                   cmip6_mean_at_data.values.reshape(-1)) - \
                            weighted_sm_at_data.values.reshape(-1), 2))
    count += 1
beta = np.sqrt(se_sq / np.dot(w_tilda, beta_denom_inner))

# ---- calculate the weighted uncertainty
for k,l in enumerate(weights.index):
    # ---- reload the CMIP6 model data
    file1 = [os.path.join(mg.path_intrim_out(), 'Interp_Merge', land_mask,
                          'CMIP6', l, 'mrsol_historical_' + str(x) + '_' + \
                          depth_cm[i] + '.nc') for x in year_cmip6]
    file2 = [os.path.join(mg.path_intrim_out(), 'Interp_Merge', land_mask,
                          'CMIP6', l, 'mrsol_ssp585_' + str(x) + '_' + \
                          depth_cm[i] + '.nc') for x in \
             list(set(year_longest) - set(year_cmip6))]
    data = xr.open_mfdataset(file1 + file2, decode_times = False)
    cmip6_sm_adjusted = weighted_sm + beta * (mean_sm + alpha * \
        (data.sm.copy(deep=True) - mean_sm) - weighted_sm)
    data.close()

    # ---- calculate the to-sum
    if k==0:
        weighted_uncertainty = w_tilda[k] * np.power( \
            cmip6_sm_adjusted - weighted_sm, 2)
    else:
        weighted_uncertainty += w_tilda[k] * np.power( \
            cmip6_sm_adjusted - weighted_sm, 2)

# ---- convert from variance to standard deviation
weighted_uncertainty = np.sqrt(weighted_uncertainty)

wsufile = os.path.join(mg.path_out(), 'dolce_cmip6_product',
                       'weighted_uncertainty_' + str(year[0]) + '-' + \
                       str(year[-1]) + '_' + d + '_' + \
                       ismn_aggr_method + '_' + method + '.nc')
weighted_uncertainty.to_dataset(name = 'sm_uncertainty').to_netcdf(wsufile)
