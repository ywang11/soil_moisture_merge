"""
2020/08/06
ywang254@utk.edu

Size of SREX regions, masked by the merged mask.
"""
import regionmask as rmk
import xarray as xr
import os
import pandas as pd
import utils_management as mg
import numpy as np


#
hr = xr.open_dataset(os.path.join(mg.path_data(), 'Global_Masks',
                                  'elm_half_degree_grid_negLon.nc'))
area = hr['area'].copy(deep = True)
hr.close()

#
hr = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                  'land_mask_minimum', 'merge_vanilla.nc'))
area = area.where(hr['mask'])
hr.close()

#
ds = rmk.defined_regions.srex.mask(area)

#
srex_area = pd.DataFrame([[np.nan, ' '*10]], index = [1], 
                         columns = ['Area', 'Unit'])
for sid in range(1,27):
    srex_area.loc[sid, 'Area'] = area.where(np.abs(ds - sid) < 1e-6).sum()
    srex_area.loc[sid, 'Unit'] = 'km^2'
#srex_area.index.name = 'SREX'
#srex_area.to_csv(os.path.join(mg.path_out(), 'srex_size.csv'))

print('Smallest SREX region = ' + ('%d' % srex_area['Area'].min()) + ' km^2.')
print('Smallest SREX region = ' + ('%d' % srex_area['Area'].max()) + ' km^2.')
