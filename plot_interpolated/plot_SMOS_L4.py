"""
2020/12/05
ywang254@utk.edu
"""
import os
import xarray as xr
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from matplotlib.gridspec import GridSpec
import numpy as np
import pandas as pd


path_root = '/lustre/haven/proj/UTK0134/Soil_Moisture/intermediate/' + \
    'Interp_Merge'
mask = 'None'


hr = xr.open_mfdataset(os.path.join(path_root, mask, 'SMOS_L4',
                                    'SMOS_L4*.nc'))
sm = hr['sm'].copy(deep = True)
hr.close()

fig = plt.figure(figsize = (6.5, 6.5))
gs = GridSpec(2, 1, hspace = 0.1)
axes = np.empty(2, dtype = object)
axes[0] = fig.add_subplot(gs[0], projection = ccrs.PlateCarree())
axes[1] = fig.add_subplot(gs[1])

ax = axes[0]
cf = ax.contourf(sm.lon, sm.lat, sm.mean(dim = 'time'),
                 cmap = 'Spectral')
plt.colorbar(cf, ax = ax, orientation = 'horizontal', shrink = 0.7)

ax = axes[1]
ax.plot(sm['time'].to_index(), sm.mean(dim = ['lon', 'lat']).values)

fig.savefig(os.path.join(path_root, mask, 'SMOS_L4',
                         'SMOS_L4.png'), dpi = 600.)
plt.close(fig)
