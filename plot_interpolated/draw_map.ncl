load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "srex_regions.ncl"
load "shapefile_utils.ncl"


undef ("draw_map")
procedure draw_map(sm, model, depth)
local sm_avg, wks, res, plot
begin
  ;-----------------------------------------------------------
  ; Average over time
  ;-----------------------------------------------------------
  sm_avg = dim_avg_n_Wrap(sm, 0)
  sm_dims = dimsizes(sm)

  wks = gsn_open_wks("png", "$SCRATCHDIR/Soil_Moisture/" + \
                     "intermediate/plot_interp_by_model/Map/global_map_" + \
                     model + "_" + depth)

  res = True
  res@gsnMaximize = True
  res@cnFillOn = True
  res@cnLinesOn = False
  res@cnLevelSelectionMode = "ManualLevels"
  res@cnMinLevelValF = 0.
  res@cnMaxLevelValF = 1.
  res@cnLevelSpacingF = 0.1; 10.
  res@tiMainString = sm_avg@long_name + " " + sm_avg@units
  
  plot = gsn_csm_contour_map(wks, sm_avg, res)

  delete(sm_avg)
  delete(wks)
  delete(res)
  delete(plot)
end