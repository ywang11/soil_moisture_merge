# -*- coding: utf-8 -*-
"""
Created on Wed May  1 21:04:14 2019

@author: ywang254

Calculate the metrics between between the individual land surface models, and
the observed soil moisture.
"""
import os
import utils_management as mg
import pandas as pd
from utils_management.constants import depth, depth_cm
from misc.ismn_utils import get_ismn_aggr_method, get_weighted_monthly_data, \
    get_weighted_monthly_data_USonly
from misc.analyze_utils import calc_stats
from misc.cmip5_availability import cmip5_availability
import numpy as np
import itertools as it
import time


# List the weighting methods that are used to generate the weighted
# averaged ISMN data.
simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = [get_ismn_aggr_method(simple[i], dominance_lc[i], 
                                         dominance_threshold) for i in \
                    range(len(simple))]
ismn_set = 'val'
iam = 'lu_weighted'
land_mask = 'vanilla'


start = time.time()
for i,d in enumerate(depth):
    dcm = depth_cm[i]
    cmip5 = cmip5_availability(dcm, land_mask)

    #
    ismn_info = pd.read_csv(os.path.join(mg.path_intrim_out(), 'ismn_summary',
                                         'ismn_landuse_continent_' + d + \
                                         '_' + iam + '_' + ismn_set + '.csv'),
                            index_col = 0)

    #
    for clas_abbr, clas in zip(['land_use_modis','continent'],
                               ['MODIS Land Use','Continent']):
        region = ismn_info.loc[~np.isnan(ismn_info[clas]), clas].unique()

        metrics_collect = pd.DataFrame(data = np.nan, index = cmip5,
            columns = pd.MultiIndex.from_product( \
                [['RMSE','Bias','uRMSE','Corr'],
                 ['%d' % c for c in region]], names = ['Metric',
                                                       'CMIP5']))

        #
        for l,c in it.product(cmip5, region):
            ###################################################################
            # Get the observed data.
            ###################################################################
            grid_latlon, weighted_monthly_data, _ = \
                get_weighted_monthly_data(os.path.join(mg.path_intrim_out(),
                                                       'ismn_aggr'), iam, d, 
                                          ismn_set)

            c_station = ismn_info.index[np.abs(ismn_info[clas] - c) < 1e-6]
            obs0 = weighted_monthly_data.loc[:, c_station].values.reshape(-1)

            ###################################################################
            # Performance of the weighted product.
            ###################################################################
            data = pd.read_csv(os.path.join(mg.path_intrim_out(),
                                            'at_obs_cmip5',
                                            iam + '_' + l + '_' + d + '.csv'), 
                               index_col = 0, parse_dates = True)
            # ---- obtain values at the observed data points
            weighted_sm_at_data = data.loc[weighted_monthly_data.index, 
                                           c_station]

            metrics_collect.loc[l, ('RMSE','%d' % c)], \
                metrics_collect.loc[l, ('Bias','%d' % c)], \
                metrics_collect.loc[l, ('uRMSE','%d' % c)], \
                metrics_collect.loc[l, ('Corr','%d' % c)] = \
                    calc_stats(weighted_sm_at_data.values.reshape(-1),
                               obs0)

        metrics_collect.to_csv(os.path.join(mg.path_out(),
                                            'standard_metrics', 
                                            'by_' + clas_abbr + \
                                            '_cmip5_' + d + '_' + iam + \
                                            '_' + ismn_set + '.csv'))
end = time.time()
# The script finished in 5.2126 hours.
print('The script finished in ' + ('%.4f' % ((end-start)/3600) ) + ' hours.')
