"""
2019/10/04
ywang254@utk.edu

Calculate the correlation map between the ESA-CCI, GLEAMv3.3a, ERA-Land, and 
 the results.
"""
import xarray as xr
import os
from utils_management.constants import depth, depth_cm
import utils_management as mg
import itertools as it
import pandas as pd
import numpy as np


d = depth[0]
dcm = depth_cm[0]


# Median-Products
year_str = '1950-2016'
time = pd.date_range('1950-01-01', '2016-12-31', freq = 'MS')
data = xr.open_mfdataset(os.path.join(mg.path_out(), 'meanmedian_products',
                                      'median_' + d + '_' + year_str + '.nc'),
                         decode_times = False)
product = data.sm.values[time.year >= 1981, :, :].copy()
data.close()


# Source products
land_mask = 'vanilla'
for s in ['ESA-CCI', 'GLEAMv3.3a']:
    file_list = [os.path.join(mg.path_intrim_out(), 'Interp_Merge', 
                              land_mask, s, s + '_' + str(y) + \
                              '_' + dcm + '.nc') for y in range(1981,2017)]
    data = xr.open_mfdataset(file_list, decode_times = False)
    ref = data.sm.values.copy()

    # Calculate correlation.
    corr = xr.DataArray(data = np.full([product.shape[1], product.shape[2]],
                                       np.nan, dtype = float),
                        coords = {'lat': data.lat, 'lon': data.lon},
                        dims = ['lat', 'lon'])
    for a, b in it.product(range(len(data.lat)), range(len(data.lon))):
        if sum(~np.isnan(ref[:, a, b])) < 3:
            continue

        x = ref[:, a, b]
        y = product[:, a, b]

        temp = ~(np.isnan(x) | np.isnan(y))
        x = x[temp]
        y = y[temp]

        corr[a, b] = np.corrcoef(x, y)[1,0]

    # Save to file
    corr.to_dataset(name = 'corr').to_netcdf(os.path.join(mg.path_out(),
        'other_diagnostics', 'meanmedian_products', 
        'corr_with_' + s + '_' + dcm + '.nc'))

    data.close()


# ERA-Land, two depths
time = time[time.year >= 1981]
product = product[time.year <= 2010, :, :]
for dcm in depth_cm[:2]:
    file_list = [os.path.join(mg.path_intrim_out(), 'Interp_Merge', 
                              land_mask, 'ERA-Land', 'ERA-Land_' + str(y) + \
                              '_' + dcm + '.nc') for y in range(1981,2011)]
    data = xr.open_mfdataset(file_list, decode_times = False)
    ref = data.sm.values.copy()

    # Calculate correlation.
    corr = xr.DataArray(data = np.full([product.shape[1], product.shape[2]],
                                       np.nan, dtype = float),
                        coords = {'lat': data.lat, 'lon': data.lon},
                        dims = ['lat', 'lon'])
    for a, b in it.product(range(len(data.lat)), range(len(data.lon))):
        if np.isnan(ref[0, a, b]):
            continue

        if sum(~np.isnan(ref[:, a, b])) < 3:
            continue

        x = ref[:, a, b]
        y = product[:, a, b]

        temp = ~(np.isnan(x) | np.isnan(y))
        x = x[temp]
        y = y[temp]

        corr[a, b] = np.corrcoef(x, y)[1,0]

    # Save to file
    corr.to_dataset(name = 'corr').to_netcdf(os.path.join(mg.path_out(),
        'other_diagnostics', 'meanmedian_products', 
        'corr_with_ERA-Land_' + dcm + '.nc'))

    data.close()
