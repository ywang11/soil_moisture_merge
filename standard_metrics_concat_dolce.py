# -*- coding: utf-8 -*-
"""
Created on Wed May  1 21:04:14 2019

@author: ywang254

Calculate the evaluation metrics between between the DOLCE-weighted results, 
and the observed soil moisture. Note: although the weighting factors use the
de-biased soil moisture, the final soil moisture product still uses the 
absolute soil moisture. Therfore, a bias metric can still be calculated.
"""
import os
import utils_management as mg
import pandas as pd
from utils_management.constants import depth, depth_cm, year_longest
from misc.ismn_utils import get_ismn_aggr_method, get_weighted_monthly_data, \
    get_weighted_monthly_data_USonly
from misc.dolce_utils import get_cov_method
from misc.analyze_utils import calc_stats
import numpy as np
import itertools as it
import time


year = year_longest
time_range = pd.date_range(start=str(year[0])+'-01-01',
                           end=str(year[-1])+'-12-31', freq='MS')


# List the weighting methods that are used to generate the weighted
# averaged ISMN data.
simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = [get_ismn_aggr_method(simple[i], dominance_lc[i], 
                                         dominance_threshold) for i in \
                    range(len(simple))]


# List the methods to calculate the covariance when calculating the weights.
cov_method = [get_cov_method(i) for i in range(5)]


# Whether the metric is limited to the performance in CONUS.
# Whether the result is merged or simply concatenated.
start = time.time()
for no_merge, USonly, prod, i in it.product([True, False], [True, False], 
                                            ['lsm', 'all'], range(len(depth))):
    d = depth[i]
    dcm = depth_cm[i]

    # Collector for the calculated metrics in time and space.
    metrics_collect = pd.DataFrame(data = np.nan,
        index = pd.MultiIndex.from_product([ismn_aggr_method, cov_method], 
                                           names = ['ISMN Aggr', 
                                                    'Cov Method']),
        columns = pd.MultiIndex.from_product([['RMSE','Bias','uRMSE','Corr'],
                                              ['all','cal','val']],
                                             names = ['Metric', 'ISMN Set']))

    for iam, cov, ismn_set in it.product(ismn_aggr_method, cov_method,
                                         ['all', 'cal', 'val']):
        #######################################################################
        # Get the observed data.
        #######################################################################
        if USonly:
            grid_latlon, weighted_monthly_data, _ = \
                get_weighted_monthly_data_USonly( \
                    os.path.join(mg.path_intrim_out(), 
                                 'ismn_aggr'), iam, d, ismn_set)
        else:
            grid_latlon, weighted_monthly_data, _ = \
                get_weighted_monthly_data(os.path.join(mg.path_intrim_out(), 
                                                       'ismn_aggr'), iam, d, 
                                          ismn_set)

        #######################################################################
        # Performance of the weighted product.
        #######################################################################
        if no_merge:
            data = pd.read_csv(os.path.join(mg.path_out(), 
                                            'at_obs_concat_dolce_' + prod,
                                            'no_merge_' + d + '_' + \
                                            iam + '_' + cov + '.csv'),
                               index_col = 0, parse_dates = True)
        else:
            data = pd.read_csv(os.path.join(mg.path_out(), 
                                            'at_obs_concat_dolce_' + prod,
                                            d + '_' + iam + '_' + cov + \
                                            '.csv'), index_col = 0,
                               parse_dates = True)
        # ---- obtain values at the observed data points
        weighted_sm_at_data = data.loc[weighted_monthly_data.index, 
                                       weighted_monthly_data.columns]

        metrics_collect.loc[(iam, cov), ('RMSE',ismn_set)], \
            metrics_collect.loc[(iam, cov), ('Bias',ismn_set)], \
            metrics_collect.loc[(iam, cov), ('uRMSE',ismn_set)], \
            metrics_collect.loc[(iam, cov), ('Corr',ismn_set)] = \
            calc_stats(weighted_sm_at_data.values.reshape(-1),
                       weighted_monthly_data.values.reshape(-1))

    if USonly:
        if no_merge:
            metrics_collect.to_csv(os.path.join(mg.path_out(), 
                                                'standard_metrics',
                                                'concat_no_merge_dolce_' + \
                                                prod + '_' + d + '_' + \
                                                str(year[0]) + \
                                                '-' + str(year[-1]) + \
                                                '_USonly.csv'))
        else:
            metrics_collect.to_csv(os.path.join(mg.path_out(), 
                                                'standard_metrics',
                                                'concat_dolce_' + prod + '_' \
                                                + d + '_' + str(year[0]) + \
                                                '-' + str(year[-1]) + \
                                                '_USonly.csv'))
    else:
        if no_merge:
            metrics_collect.to_csv(os.path.join(mg.path_out(), 
                                                'standard_metrics',
                                                'concat_no_merge_dolce_' + \
                                                prod + '_' + d + '_' + \
                                                str(year[0]) + \
                                                '-' + str(year[-1]) + '.csv'))
        else:
            metrics_collect.to_csv(os.path.join(mg.path_out(), 
                                                'standard_metrics',
                                                'concat_dolce_' + prod + '_' \
                                                + d + '_' + str(year[0]) + \
                                                '-' + str(year[-1]) + '.csv'))
end = time.time()
print('The script finished in ' + ('%.4f' % ((end-start)/3600) ) + ' hours.')
