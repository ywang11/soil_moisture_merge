"""
2019/08/05
ywang254@utk.edu

Plot the time series of global mean annual mean soil moisture of the 
 emergent constraint results, with the upper CI and lower CI.
Show the GLEAMv3.3 and ERA-Land time series of global mean annual mean
 soil moisture for comparison.
Compare between the 1950-2016 simple product (which has fewer models), 
 concatenated product no merge, and merged product.
"""
import matplotlib.pyplot as plt
import utils_management as mg
from utils_management.constants import year_longest, depth, depth_cm
from misc.plot_utils import plot_ts_shade, plot_ts_trend
import pandas as pd
import os
import numpy as np


# MODIFY
pr = 'GPCC' # ['CRU_v4.03', 'GPCC']
em = 'year_month_9grid' # ['month_1grid', 'month_anomaly_1grid', 
                        #  'year_month_9grid']
year = year_longest


#
year_str = str(year[0]) + '-' + str(year[-1])


#
global_ts_all_depth = {}
for i,d in enumerate(depth):
    dcm = depth_cm[i]

    # 
    # (1) 1950-2016 continuous dataset with fewer models
    simple = pd.read_csv(os.path.join(mg.path_out(), 
                                      'standard_diagnostics_em_lsm',
                                      pr + '_predicted_' + em + '_' + \
                                      year_str + '_' + d + '_g_ts.csv'), 
                         index_col = 0, parse_dates = True).iloc[:, 0]
    simple_CI_lower = pd.read_csv(os.path.join(mg.path_out(),
        'standard_diagnostics_em_lsm',
        pr + '_predicted_CI_lower_' + em + '_' + year_str + '_' + \
        d + '_g_ts.csv'), index_col = 0, parse_dates = True).iloc[:, 0]
    simple_CI_upper = pd.read_csv(os.path.join(mg.path_out(),
        'standard_diagnostics_em_lsm', pr + '_predicted_CI_upper_' + \
        em + '_' + year_str + '_' + d + '_g_ts.csv'),
        index_col = 0, parse_dates = True).iloc[:, 0]

    # (2) Concatenated dataset, no merge
    concat = pd.read_csv(os.path.join(mg.path_out(), 
                                      'standard_diagnostics_concat',
                                      'no_merge_em',
                                      'no_merge_' + pr + '_predicted_' + \
                                      em + '_' + d + '_g_ts.csv'),
                         index_col = 0, parse_dates = True).iloc[:, 0]
    concat_CI_lower = pd.read_csv(os.path.join(mg.path_out(), 
                                               'standard_diagnostics_concat',
                                               'no_merge_em',
                                               'no_merge_' + pr + \
                                               '_predicted_CI_lower_' + \
                                               em + '_' + d + '_g_ts.csv'),
                                  index_col = 0, parse_dates = True).iloc[:, 0]
    concat_CI_upper = pd.read_csv(os.path.join(mg.path_out(),
                                               'standard_diagnostics_concat',
                                               'no_merge_em',
                                               'no_merge_' + pr + \
                                               '_predicted_CI_upper_' + \
                                               em + '_' + d + '_g_ts.csv'),
                                  index_col = 0, parse_dates = True).iloc[:, 0]

    # (3)
    merge = pd.read_csv(os.path.join(mg.path_out(), 
                                     'standard_diagnostics_concat',
                                     'em', pr + '_predicted_' + \
                                     em + '_' + d + '_g_ts.csv'),
                        index_col = 0, parse_dates = True).iloc[:, 0]
    merge_CI_lower = pd.read_csv(os.path.join(mg.path_out(), 
                                              'standard_diagnostics_concat',
                                              'em', 
                                              pr + '_predicted_CI_lower_' + \
                                              em + '_' + d + '_g_ts.csv'),
                                 index_col = 0, parse_dates = True).iloc[:, 0]
    merge_CI_upper = pd.read_csv(os.path.join(mg.path_out(),
                                              'standard_diagnostics_concat',
                                              'em',
                                              pr + '_predicted_CI_upper_' + \
                                              em + '_' + d + '_g_ts.csv'),
                                 index_col = 0, parse_dates = True).iloc[:, 0]

    # Annual average
    simple = simple.groupby(simple.index.year).mean()
    simple_CI_lower = simple_CI_lower.groupby( \
        simple_CI_lower.index.year).mean()
    simple_CI_upper = simple_CI_upper.groupby( \
        simple_CI_upper.index.year).mean()
    concat = concat.groupby(concat.index.year).mean()
    ##concat_CI_lower = concat.groupby(concat_CI_lower.index.year).mean()
    ##concat_CI_upper = concat.groupby(concat_CI_upper.index.year).mean()
    merge = merge.groupby(merge.index.year).mean()
    ##merge_CI_lower = merge_CI_lower.groupby(merge_CI_lower.index.year).mean()
    ##merge_CI_upper = merge_CI_upper.groupby(merge_CI_upper.index.year).mean()

    # Gather the data
    global_ts_all_depth[d] = {}

    global_ts = {}
    global_ts['mean'] = simple
    global_ts['min'] = simple_CI_lower # 95% CI
    global_ts['max'] = simple_CI_upper # 95% CI
    global_ts_all_depth[d]['simple'] = global_ts

    global_ts = {}
    global_ts['mean'] = concat
    global_ts['min'] = concat ###concat_CI_lower # 95% CI
    global_ts['max'] = concat ###concat_CI_upper # 95% CI
    global_ts_all_depth[d]['concat'] = global_ts

    global_ts = {}
    global_ts['mean'] = merge
    global_ts['min'] = merge # merge_CI_lower # 95% CI
    global_ts['max'] = merge # merge_CI_upper # 95% CI
    global_ts_all_depth[d]['merge'] = global_ts


# 
fig, axes = plt.subplots(nrows = len(depth_cm), ncols = 2, sharex = True, 
                         sharey = False, figsize = (10, 6.5))
fig.subplots_adjust(hspace = 0., wspace = 0.2)

# (1) Soil Moisture
for i,d in enumerate(depth):
    dcm = depth_cm[i]

    ax = axes[i, 0]
    h1 = plot_ts_shade(ax, 
                       global_ts_all_depth[d]['simple']['mean'].index,
                       global_ts_all_depth[d]['simple'], ts_col = 'b')
    h2 = plot_ts_shade(ax, 
                       global_ts_all_depth[d]['concat']['mean'].index,
                       global_ts_all_depth[d]['concat'], ts_col = 'r')
    h3 = plot_ts_shade(ax, 
                       global_ts_all_depth[d]['merge']['mean'].index,
                       global_ts_all_depth[d]['merge'], ts_col = 'g')

    if i == 0:
        ax.set_title('Soil Moisture (m$^3$/m$^3$)')

    ax.set_xlim([year[0], year[-1]])
    ax.set_ylabel(dcm) # '(m$^3$/m$^3$)'
    ax.set_ylim([0., 0.5])
    ax.set_yticks(np.linspace(0.1, 0.4, 5))
ax.set_xlabel('Year')

ax.legend([h1, h2, h3], ['1950-2016', 'Concat', 'Merged'], 
          ncol = 3, loc = 'lower right', 
          bbox_to_anchor = (1.5, -0.7))

# (2) Soil Moisture Anomaly
for i,d in enumerate(depth):
    dcm = depth_cm[i]

    ax = axes[i, 1]
    h1, = ax.plot(global_ts_all_depth[d]['simple']['mean'].index,
                  global_ts_all_depth[d]['simple']['mean'] - \
                  global_ts_all_depth[d]['simple']['mean'].mean(), '-b',
                  linewidth = 2, zorder = 3)
    h2, = ax.plot(global_ts_all_depth[d]['concat']['mean'].index,
                  global_ts_all_depth[d]['concat']['mean'] - \
                  global_ts_all_depth[d]['concat']['mean'].mean(), '-r',
                  linewidth = 2, zorder = 3)
    h3, = ax.plot(global_ts_all_depth[d]['merge']['mean'].index, 
                  global_ts_all_depth[d]['merge']['mean'] - \
                  global_ts_all_depth[d]['merge']['mean'].mean(), '-g', 
                  linewidth = 2, zorder = 3)

    if i == 0:
        ax.set_title('Anomaly (m$^3$/m$^3$)')

    ax.set_xlim([year[0], year[-1]])
    ax.set_ylim([-0.005, 0.005])
    ax.set_yticks(np.linspace(-0.004, 0.004, 5))
ax.set_xlabel('Year')

fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                         'ts_annual', 'compare_concat_em_' + \
                         pr + '_' + em + '_global_annual_ts.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)
