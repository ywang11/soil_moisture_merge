"""
2019/07/05

ywang254@utk.edu

Map the global mean and seasonal mean soil moisture for the emergent constraint
  result of the land surface models/CMIP5/CMIP6.
"""
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth
import utils_management as mg
from misc.plot_standard_diagnostics_utils import plot_trend_map
import numpy as np
import os
import itertools as it
from misc.plot_utils import cmap_gen
import multiprocessing as mp


cmap = cmap_gen('autumn', 'winter_r')


met_obs_name = 'CRU_v4.03'


# MODIFY
model_set = 'cmip6' # 'lsm', 'cmip5', 'cmip6'
if model_set == 'lsm':
    method_list = ['year_month_9grid', 'year_month_anomaly_9grid',
                   'month_1grid', 'month_anomaly_1grid',
                   'year_mw_9grid', 'year_mw_anomaly_9grid',
                   'year_mw_1grid', 'year_mw_anomaly_1grid']
    year_list = [year_longest, year_shorter, year_shorter2, year_shortest]
else:
    method_list = ['year_mw_anomaly_9grid', 'year_mw_anomaly_1grid']
                  #'year_month_9grid', 'year_month_anomaly_9grid',
                  #'year_month_1grid', 'year_month_anomaly_1grid',
                  #'month_1grid', 'month_anomaly_1grid',
                  #'year_mw_9grid', 'year_mw_anomaly_9grid',
                  #'year_mw_1grid', 'year_mw_anomaly_1grid']
    year_list = [year_longest]
                             # 'month_1grid', 'month_anomaly_1grid'

#DEBUG
#for year, me in it.product(year_list, method_list):
def plotter(option):
    year, me = option

    prefix_in = os.path.join(mg.path_out(),
                             'standard_diagnostics_em_' + model_set,
                             met_obs_name + '_predicted_' + me + '_' + \
                             str(year[0]) + '-' + str(year[-1]))
    prefix_out = os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                              'trend_map_em_' + model_set, 
                              met_obs_name + '_' + me + '_' + \
                              str(year[0]) + '-' + str(year[-1]))

    levels = np.linspace(-0.002, 0.002, 9)

    plot_trend_map(depth, prefix_in, prefix_out, levels, cmap)


pool = mp.Pool(mp.cpu_count())
pool.map_async(plotter, list(it.product(year_list, method_list)))
pool.close()
pool.join()
