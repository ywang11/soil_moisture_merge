"""
2019/07/05

ywang254@utk.edu

Map the global mean and seasonal mean soil moisture for the emergent constraint
  result of the land surface models.
"""
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth
import utils_management as mg
from misc.plot_standard_diagnostics_utils import plot_map
import numpy as np
import os
import itertools as it


levels = np.linspace(0., 0.6, 15)
cmap = 'RdYlBu'

year = year_longest

pr_obs = ['CRU_v4.03', 'GPCC']

model_set = ['lsm', 'cmip5', 'cmip6']

for pr,model in it.product(pr_obs, model_set):
    if model == 'lsm':
        method = ['month_1grid', 'month_anomaly_1grid', 'year_month_9grid']
    else:
        method = ['month_1grid', 'month_anomaly_1grid', 'year_month_1grid',
                  'year_month_9grid']

    for me in method:
        plot_map(depth, os.path.join(mg.path_out(),
                                     'standard_diagnostics_em_' + model,
                                     pr + '_predicted_' + me + '_' + \
                                     str(year[0]) + '-' + str(year[-1])),
                 os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                              'mean_em_' + model, pr + '_' + me + '_' + \
                              str(year[0]) + '-' + str(year[-1])),
                 'map', levels, cmap)
