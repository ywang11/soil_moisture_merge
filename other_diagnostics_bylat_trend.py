"""
2020/03/13

ywang254@utk.edu

Calculate the linear trend in the annual time series, and in the per month
  time series by latitude band during the full period for each land surface 
  model & CMIP5 & CMIP6 models, and the final product.
"""
import pandas as pd
from utils_management.constants import depth, depth_cm, year_longest, \
    year_shortest, lsm_list
import utils_management as mg
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability, one_layer_availability
from misc.other_diagnostics import get_bylat_trend
import itertools as it
import os
import numpy as np


year_str0 = '1950-2016'


# Time period to calculate the trend on - MODIFY, which changes the 
# associated set of land surface models.
year = year_longest # year_shortest
year_str = str(year[0]) + '-' + str(year[-1])
time_range = pd.date_range(start = str(year[0])+'-01-01',
                           end = str(year[-1])+'-12-31', freq = 'MS')

# Mean & Median
for model_set in ['lsm', 'cmip5', 'cmip6', '2cmip', 'all']:
    path_prefix = os.path.join(mg.path_out(), 
                               'standard_diagnostics_meanmedian_' + \
                               model_set, 'mean_' + year_str0)
    for d in depth:
        trend, trend_p = get_bylat_trend(path_prefix, d, time_range)
        trend.to_csv(os.path.join(mg.path_out(), 'other_diagnostics', 
                                  'mean_' + model_set, 
                                  'bylat_trend_' + d + '_' + year_str + \
                                  '.csv'))
        trend_p.to_csv(os.path.join(mg.path_out(), 'other_diagnostics', 
                                    'mean_' + model_set, 
                                    'bylat_trend_p_' + d + '_' + year_str + \
                                    '.csv'))

# DOLCE
best = 'lu_weighted_ShrunkCovariance'
for model_set in ['cmip5', 'cmip6', '2cmip']:
    path_prefix = os.path.join(mg.path_out(), 
                               'standard_diagnostics_dolce_' + model_set,
                               best + '_weighted_average_' + year_str0)
    for d in depth:
        trend, trend_p = get_bylat_trend(path_prefix, d, time_range)
        trend.to_csv(os.path.join(mg.path_out(), 'other_diagnostics', 
                                  'dolce_' + model_set, 'bylat_trend_' + \
                                  d + '_' + year_str + '.csv'))
        trend_p.to_csv(os.path.join(mg.path_out(), 'other_diagnostics', 
                                    'dolce_' + model_set, 'bylat_trend_p_' + \
                                    d + '_' + year_str + '.csv'))

# DOLCE-LSM/DOLCE-ALL, concatenated
best = 'lu_weighted_ShrunkCovariance'
for model_set in ['lsm', 'all']:
    path_prefix = os.path.join(mg.path_out(), 'standard_diagnostics_concat',
                               'dolce_' + model_set, 
                               'dolce_average_' + best)
    for d in depth:
        trend, trend_p = get_bylat_trend(path_prefix, d, time_range)
        trend.to_csv(os.path.join(mg.path_out(), 'other_diagnostics', 
                                  'dolce_' + model_set, 'bylat_trend_' + \
                                  d + '_' + year_str + '.csv'))
        trend_p.to_csv(os.path.join(mg.path_out(), 'other_diagnostics', 
                                    'dolce_' + model_set, 'bylat_trend_p_' + \
                                    d + '_' + year_str + '.csv'))

# Emergent Constraint
best = 'CRU_v4.03_predicted_year_month_anomaly_9grid'
for model_set in ['cmip5', 'cmip6', '2cmip']:
    path_prefix = os.path.join(mg.path_out(),
                               'standard_diagnostics_em_' + model_set,
                               best + '_' + year_str0)
    for d in depth:
        trend, trend_p = get_bylat_trend(path_prefix, d, time_range)
        trend.to_csv(os.path.join(mg.path_out(), 'other_diagnostics',
                                  'em_' + model_set, 'bylat_trend_' + \
                                  d + '_' + year_str + '.csv'))
        trend_p.to_csv(os.path.join(mg.path_out(), 'other_diagnostics',
                                    'em_' + model_set, 'bylat_trend_p_' + \
                                    d + '_' + year_str + '.csv'))

# Emergent Constraint-LSM/-ALL, concatenated
best = 'CRU_v4.03_year_month_anomaly_9grid_predicted'
for model_set in ['lsm', 'all']:
    path_prefix = os.path.join(mg.path_out(), 'standard_diagnostics_concat',
                               'em_' + model_set, best)
    for d in depth:
        trend, trend_p = get_bylat_trend(path_prefix, d, time_range)
        trend.to_csv(os.path.join(mg.path_out(), 'other_diagnostics',
                                  'em_' + model_set, 'bylat_trend_' + \
                                  d + '_' + year_str + '.csv'))
        trend_p.to_csv(os.path.join(mg.path_out(), 'other_diagnostics',
                                    'em_' + model_set, 'bylat_trend_p_' + \
                                    d + '_' + year_str + '.csv'))

# Individual LSMs
for d in depth:
    lsm = lsm_list[(year_str, d)]
    for l in lsm:
        path_prefix = os.path.join(mg.path_out(),
                                   'standard_diagnostics_lsm', l)
        trend, trend_p = get_bylat_trend(path_prefix, d, time_range)
        trend.to_csv(os.path.join(mg.path_out(), 'other_diagnostics',
                                  'lsm', l + '_bylat_trend_' + d + \
                                  '_' + year_str + '.csv'))
        trend_p.to_csv(os.path.join(mg.path_out(), 'other_diagnostics',
                                    'lsm', l + '_bylat_trend_p_' + d + \
                                    '_' + year_str + '.csv'))

# Individual CMIP5
land_mask = 'vanilla'
for i,dcm in enumerate(depth_cm):
    d = depth[i]

    cmip5 = cmip5_availability(dcm, land_mask)
    for l in cmip5:
        path_prefix = os.path.join(mg.path_out(), 
                                   'standard_diagnostics_cmip5', l)
        trend, trend_p = get_bylat_trend(path_prefix, dcm, time_range)
        trend.to_csv(os.path.join(mg.path_out(), 'other_diagnostics',
                                  'cmip5', l + '_bylat_trend_' + d + \
                                  '_' + year_str + '.csv'))
        trend_p.to_csv(os.path.join(mg.path_out(), 'other_diagnostics',
                                    'cmip5', l + '_bylat_trend_p_' + \
                                    d + '_' + year_str + '.csv'))

# Individual CMIP6
land_mask = 'vanilla'
for i,dcm in enumerate(depth_cm):
    d = depth[i]

    # CMIP6 relevant to this soil layer.
    # ---- the models that have precipitation.
    cmip6_list_1 = mrsol_availability(dcm, land_mask, 'historical')
    cmip6_list_2 = mrsol_availability(dcm, land_mask, 'ssp585')
    # ---- further subset to the given depth.
    cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) )
    # ---- further subset to the first ensemble member.
    cmip6_list = [x for x in cmip6_list if 'r1i1p1f1' in x]

    for l in cmip6_list:
        path_prefix = os.path.join(mg.path_out(), 
                                   'standard_diagnostics_cmip6', 
                                   'mrsol_' + l)
        trend, trend_p = get_bylat_trend(path_prefix, dcm, time_range)
        trend.to_csv(os.path.join(mg.path_out(), 'other_diagnostics', 'cmip6', 
                                  l + '_bylat_trend_' + d + \
                                  '_' + year_str + '.csv'))
        trend_p.to_csv(os.path.join(mg.path_out(), 'other_diagnostics', 
                                    'cmip6', l + '_bylat_trend_p_' + d + \
                                    '_' + year_str + '.csv'))
