"""
20191218
ywang254@utk.edu

Find the negative soil moisture in the year_month emergent constraint data.
Ensure that the only difference from restored anomalies are the negative 
 values.
"""
import xarray as xr
import os
import utils_management as mg
from utils_management.constants import depth_cm, year_longest
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import numpy as np
import itertools as it
import pandas as pd


year_str = str(year_longest[0]) + '-' + str(year_longest[-1])
period = pd.date_range(str(year_longest[0]) + '-01-01',
                       str(year_longest[-1]) + '-12-31', freq = 'MS')


# 'lsm',
for dcm, model in it.product(depth_cm, ['cmip5', 'cmip6']):
    #
    if model == 'lsm':
        best = 'year_month_anomaly_9grid'
        data = xr.open_dataset(os.path.join(mg.path_out(), 'concat_em_lsm',
                                            'concat_CRU_v4.03_' + best + '_' +\
                                            dcm + '_predicted_' + year_str + \
                                            '.nc'), decode_times = False)
    else:
        best = 'year_month_anomaly_9grid'.replace('anomaly', 'restored')
        data = xr.open_mfdataset([os.path.join(mg.path_out(),
                                               'em_' + model + '_corr',
                                               'CRU_v4.03_' + best + '_' + \
                                               dcm + '_' + year_str,
                                               'predicted_' + str(y) + '.nc') \
                                  for y in year_longest], decode_times = False,
                                 concat_dim = 'time')
    sm_anomaly = data['predicted'].values.copy()
    data.close()

    #
    if model == 'lsm':
        best = 'year_month_anomaly_9grid'.replace('anomaly', 'positive')
        data = xr.open_dataset(os.path.join(mg.path_out(), 'concat_em_lsm',
                                            'concat_CRU_v4.03_' + best + '_' +\
                                            dcm + '_predicted_' + year_str + \
                                            '.nc'), decode_times = False)
    else:
        best = 'year_month_anomaly_9grid'.replace('anomaly', 'positive')
        data = xr.open_dataset(os.path.join(mg.path_out(), 
                                            'em_' + model + '_corr',
                                            'CRU_v4.03_' + best + '_' + \
                                            dcm + '_' + year_str, 
                                            'predicted.nc'), 
                               decode_times = False)
    sm_correct = data['predicted'].values.copy()
    data.close()


    anomaly_invalid = (sm_anomaly < 0.) | (sm_anomaly > 1.)
    correct_invalid = (sm_correct < 0.) | (sm_correct > 1.)

    #
    anomaly_ts_invalid = np.sum(np.sum(anomaly_invalid, axis = 2), axis = 1)
    anomaly_map_invalid = np.sum(anomaly_invalid, axis = 0)

    correct_ts_invalid = np.sum(np.sum(correct_invalid, axis = 2), axis = 1)
    correct_map_invalid = np.sum(correct_invalid, axis = 0)


    #
    fig = plt.figure(figsize = (12, 12))
    ax = fig.add_subplot(221)
    ax.plot(period.year.values + (period.month.values-0.5)/12, 
            anomaly_ts_invalid)
    ax.set_xticks(period.year[::60])
    ax.set_title('Anomaly # invalids per time step')

    ax = fig.add_subplot(222)
    ax.plot(period.year.values + (period.month.values-0.5)/12, 
            correct_ts_invalid)
    ax.set_xticks(period.year[::60])
    ax.set_title('Correct # invalids per time step')

    ax = fig.add_subplot(223, projection = ccrs.PlateCarree())
    ax.coastlines()
    ax.set_extent([-180, 180, -60, 90])
    h = ax.contourf(data.lon.values, data.lat.values, anomaly_map_invalid,
                    corner_mask = False)
    plt.colorbar(h, ax = ax, shrink = 0.7)

    ax = fig.add_subplot(224, projection = ccrs.PlateCarree())
    ax.coastlines()
    ax.set_extent([-180, 180, -60, 90])
    h = ax.contourf(data.lon.values, data.lat.values, correct_map_invalid,
                    corner_mask = False)
    plt.colorbar(h, ax = ax, shrink = 0.7)


    # num different grid cells
    temp = (sm_anomaly - sm_correct)[(~anomaly_invalid) & (~correct_invalid)]
    #print(np.nanmax(temp))
    #print(np.nanmin(temp))

    fig.text(0.5, 0.95, '# Max Diff = (%.ff, %.4f)' % (np.nanmax(temp),
                                                       np.nanmin(temp)))

    fig.savefig(os.path.join(mg.path_out(), 'em_best_check_negatives',
                             'corrected_' + model + '_' + dcm + '.png'), 
                dpi = 600.)
    plt.close(fig)
