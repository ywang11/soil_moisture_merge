"""
2020/12/07
ywang254@utk.edu

Compare the validataion dataset to the products and raw source datasets.
"""
import os
import pandas as pd
import numpy as np
import xarray as xr
import utils_management as mg
from utils_management.constants import depth, depth_cm, lsm_list, \
    year_shorter2
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability
from misc.analyze_utils import calc_stats
import itertools as it


def get_raw_list():
    land_mask = 'vanilla'
    for dind, d in enumerate(depth):
        dcm = depth_cm[dind]
        lsm_temp = lsm_list[(str(year_shorter2[0]) + '-' + \
                             str(year_shorter2[-1]), d)]
        cmip5_temp = cmip5_availability(dcm, land_mask)
        cmip6_temp = sorted(set(mrsol_availability(dcm, land_mask,
                                                   'historical')) \
                            & set(mrsol_availability(dcm, land_mask,
                                                     'ssp585')))
        if dind == 0:
            lsm = lsm_temp
            cmip5 = cmip5_temp
            cmip6 = cmip6_temp
        else:
            lsm = sorted(set(lsm) & set(lsm_temp))
            cmip5 = sorted(set(cmip5) & set(cmip5_temp))
            cmip6 = sorted(set(cmip6) & set(cmip6_temp))
    return lsm, cmip5, cmip6


year_list = {'SMOS_L3': range(2010, 2017),
             'SMOS_L4': range(2010, 2017),
             'SMERGE_v2': range(1981, 2017), # for compatibility with shorter2
             'GLEAMv3.3a_0-100cm': range(1981, 2017), # for compatibility with shorter2
             'SoMo_0-10cm': range(2000,2017),
             'SoMo_10-30cm': range(2000,2017),
             'SoMo_30-50cm': range(2000,2017)}
unit_list = {'SMOS_L3': '0-10cm',
             'SMOS_L4': '0-100cm',
             'SMERGE_v2': '0-40cm',
             'GLEAMv3.3a_0-100cm': '0-100cm',
             'SoMo_0-10cm': '0-10cm',
             'SoMo_10-30cm': '10-30cm',
             'SoMo_30-50cm': '30-50cm'}
eval_list = ['SMOS_L3', 'SoMo_0-10cm', 'SoMo_10-30cm', 'SMERGE_v2',
             'SoMo_30-50cm', 'SMOS_L4', 'GLEAMv3.3a_0-100cm']
prod_list = {'products': ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5',
                          'em_cmip6', 'em_2cmip', 'em_all']}
lsm, cmip5, cmip6 = get_raw_list()
prod_list['lsm'] = lsm
prod_list['cmip5'] = cmip5
prod_list['cmip6'] = cmip6


for stat, prod in it.product(['climatology', 'seasonality', 'trend',
                              'anomalies'],
                             sorted(prod_list.keys())):
    prod_inner_list = prod_list[prod]

    result = pd.DataFrame(np.nan,
                          index = pd.MultiIndex.from_product([['Bias','Corr',
                                                               'RMSE','uRMSE'],
                          prod_inner_list]),
                          columns = eval_list)
    for ev, pr in it.product(eval_list, prod_inner_list):
        #
        hr = xr.open_dataset(os.path.join(mg.path_out(), 'validate',
                                          stat, ev + '.nc'))
        if stat == 'trend':
            ev_data = hr['trend'].values.copy()
        else:
            ev_data = hr['sm'].values.copy()
        if stat == 'seasonality':
            # need to remove climatology's influence in the calculation
            hr2 = xr.open_dataset(os.path.join(mg.path_out(), 'validate',
                                               'climatology', ev + '.nc'))
            ev_data = ev_data - hr2['sm'].values.copy()
            hr2.close()
        hr.close()

        #
        if (prod == 'lsm') | (prod == 'cmip5') | (prod == 'cmip6'):
            prod_name = prod + '_' + pr + '_' + unit_list[ev] + \
                '_' + str(year_list[ev][0]) + '-' + \
                str(year_list[ev][-1])
        else:
            prod_name = pr + '_' + pr + '_' + unit_list[ev] + '_' + \
                str(year_list[ev][0]) + '-' + str(year_list[ev][-1])

        hr = xr.open_dataset(os.path.join(mg.path_out(), 'validate',
                                          stat, prod_name + '.nc'))
        if stat == 'trend':
            pr_data = hr['trend'].values.copy()
            pr_pvalue = hr['trend_p'].values.copy()
            pr_data[pr_pvalue > 0.1] = 0.
        else:
            pr_data = hr['sm'].values.copy()
        if stat == 'seasonality':
            # need to remove climatology's influence in the calculation
            hr2 = xr.open_dataset(os.path.join(mg.path_out(), 'validate',
                                               'climatology',
                                               prod_name + '.nc'))
            pr_data = pr_data - hr2['sm'].values.copy()
            hr2.close()
        hr.close()

        #
        sim = pr_data.reshape(-1)
        obs = ev_data.reshape(-1)
        temp = ~(np.isnan(sim) | np.isnan(obs))
        sim = sim[temp]
        obs = obs[temp]

        rmse, bias, urmse, corr = calc_stats(sim, obs)

        #
        result.loc[('Bias', pr), ev] = bias
        result.loc[('Corr', pr), ev] = corr
        result.loc[('RMSE', pr), ev] = rmse
        result.loc[('uRMSE', pr), ev] = urmse

    result.to_csv(os.path.join(mg.path_out(), 'validate',
                               stat, 'results_' + prod + '.csv'))
