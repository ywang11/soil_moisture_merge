"""Plot the wx_pval & fl_pval.
   Note that white areas exist because they do not satisfy the 
   correlation >= 0.8 and significant criteria.
"""
import utils_management as mg
from utils_management.constants import *
import matplotlib as mpl
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import os
import numpy as np
import xarray as xr
import itertools as it


prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6', 'em_2cmip', 'em_all']
prod_names_list = ['Mean ORS', 'OLC ORS', 'EC ORS', 'EC CMIP5', 'EC CMIP6', 'EC CMIP5+6',
                   'EC ALL']

#
mpl.rcParams['font.size'] = 5.
mpl.rcParams['axes.titlesize'] = 5.
lab = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
       'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
       'y', 'z', 'aa', 'bb', 'cc', 'dd', 'ee', 'ff', 'gg', 'hh',
       'ii', 'jj', 'kk', 'll', 'mm', 'nn', 'oo', 'pp', 'qq', 'rr',
       'ss', 'tt', 'uu', 'vv', 'ww', 'xx']


# Period 1 - does not have SoMo
eval_list = ['SMERGE_v2', 'GLEAMv3.3a_0-100cm']
period = 1

count = 0
fig, axes = plt.subplots(len(prod_list), len(eval_list) * 2, figsize = (6.5, 6.),
                         subplot_kw = {'projection': ccrs.PlateCarree()})
fig.subplots_adjust(hspace = 0.01, wspace = 0.01)
for sind, stat in enumerate(['wx_pval', 'fl_pval']):
    if stat == 'wx_pval':
        stat_name = 'Wilcoxon p-value'
    else:
        stat_name = 'Fligner-Killeen p-value'

    for pind, eind in it.product(range(len(prod_list)), range(len(eval_list))):
        evl = eval_list[eind]
        prod = prod_list[pind]

        #
        hr = xr.open_dataset(os.path.join(mg.path_out(), 'homogeneity_test',
                                          'rtm_' + str(period) + '_' + \
                                          prod + '_' + evl + '.nc'))
        data_stat = hr[stat].copy(deep = True)
        hr.close()

        #
        ax = axes[pind, eind + sind * len(eval_list)]
        ax.coastlines(lw = 0.5)
        cf = ax.contourf(data_stat['lon'], data_stat['lat'], data_stat, cmap = 'gray',
                         levels = [0., 0.05, 1])
        if pind == 0:
            ax.set_title(evl)
            if eind == 0:
                ax.text(1., 1.4, stat_name.split(' ')[0], transform = ax.transAxes,
                        horizontalalignment = 'center')
        if eind == 0 and sind == 0:
            ax.text(-0.1, 0.5, prod_names_list[pind], rotation = 90, transform = ax.transAxes,
                    verticalalignment = 'center')

        #
        pct = np.sum(data_stat <= 0.05) / np.sum(~np.isnan(data_stat))
        ax.text(0.1, -0.22, '%.2f%%' % (pct * 100),
                transform = ax.transAxes, color = 'r')
        ax.text(0.03, 0.08, '(' + str(lab[count]) + ')', transform = ax.transAxes)
        count += 1
cax = fig.add_axes([0.1, 0.06, 0.8, 0.01])
cbar = plt.colorbar(cf, cax = cax, ticks = [0., 0.05, 1], label = 'p-value',
                    orientation = 'horizontal')
fig.savefig(os.path.join(mg.path_out(), 'homogeneity_test',
                         'rtm_' + str(period) + '.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)


# Period 2
period = 2
eval_list = ['SoMo_0-10cm', 'SoMo_10-30cm', 'SMERGE_v2', 'SoMo_30-50cm', 'GLEAMv3.3a_0-100cm']
for stat in ['wx_pval', 'fl_pval']:
    if stat == 'wx_pval':
        stat_name = 'Wilcoxon p-value'
    else:
        stat_name = 'Fligner-Killeen p-value'

    count = 0
    fig, axes = plt.subplots(len(prod_list), len(eval_list), figsize = (6.5, 6.),
                             subplot_kw = {'projection': ccrs.PlateCarree()})
    fig.subplots_adjust(hspace = 0.01, wspace = 0.01)
    for pind, eind in it.product(range(len(prod_list)), range(len(eval_list))):
        evl = eval_list[eind]
        prod = prod_list[pind]

        #
        hr = xr.open_dataset(os.path.join(mg.path_out(), 'homogeneity_test',
                                          'rtm_' + str(period) + '_' + \
                                          prod + '_' + evl + '.nc'))
        data_stat = hr[stat].copy(deep = True)
        hr.close()

        #
        ax = axes[pind, eind]
        ax.coastlines(lw = 0.5)
        cf = ax.contourf(data_stat['lon'], data_stat['lat'], data_stat, cmap = 'gray',
                         levels = [0., 0.05, 1])
        if pind == 0:
            ax.set_title(evl)
        if eind == 0:
            ax.text(-0.1, 0.5, prod_names_list[pind], rotation = 90, transform = ax.transAxes,
                    verticalalignment = 'center')

        #
        pct = np.sum(data_stat <= 0.05) / np.sum(~np.isnan(data_stat))
        ax.text(0.1, -0.18, '%.2f%%' % (pct * 100), transform = ax.transAxes, color = 'r')
        ax.text(0.03, 0.08, '(' + str(lab[count]) + ')', transform = ax.transAxes)
        count += 1
    cax = fig.add_axes([0.1, 0.1, 0.8, 0.01])
    cbar = plt.colorbar(cf, cax = cax, ticks = [0., 0.05, 1], label = stat_name,
                        orientation = 'horizontal')
    fig.savefig(os.path.join(mg.path_out(), 'homogeneity_test',
                             'rtm_' + stat + '_' + str(period) + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
