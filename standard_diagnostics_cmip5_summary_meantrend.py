"""
2019/08/05

ywang254@utk.edu

The mean, min, max of the mean and trend of global soil moisture of CMIP5
models. 
"""
import numpy as np
import utils_management as mg
from utils_management.constants import year_cmip5, cmip5, depth_cm, \
    target_lat, target_lon
import xarray as xr
import os
from misc.standard_diagnostics import get_summary_cmip5
import numpy.ma as ma


i = 0 # [0, 1, 2, 3]
dcm = depth_cm[i]


mean_mean, mean_max, mean_min, _, _ = get_summary_cmip5(mg.path_out(), 
                                                        target_lat, 
                                                        target_lon,
                                                        dcm, 'mean')

xr.Dataset({'mean': xr.DataArray(mean_mean, dims = ['lat', 'lon'],
                                 coords = {'lat': target_lat,
                                           'lon': target_lon}),
            'min': xr.DataArray(mean_min, dims = ['lat', 'lon'],
                                 coords = {'lat': target_lat,
                                           'lon': target_lon}),
            'max': xr.DataArray(mean_max, dims = ['lat', 'lon'],
                                 coords = {'lat': target_lat,
                                           'lon': target_lon})} \
           ).to_netcdf(os.path.join(mg.path_out(), 
                                    'standard_diagnostics_cmip5',
                                    'sm_historical_' + \
                                    str(year_cmip5[0]) + '-' + \
                                    str(year_cmip5[-1]) + '_' + dcm + \
                                    '_summary_mean.nc'))


trend_mean, trend_max, trend_min, \
    trend_collect, trend_pvalue = get_summary_cmip5(mg.path_out(), target_lat, 
                                                    target_lon, dcm, 'trend')

# NO. significant negative/positive trends
trend_collect_significant = ma.masked_array(trend_collect,
                                            trend_pvalue > 0.05)
pct_sig_neg = np.sum(trend_collect_significant<0, axis=0) / \
              trend_collect.shape[0]
pct_sig_pos = np.sum(trend_collect_significant>0, axis=0) / \
              trend_collect.shape[0]


xr.Dataset({'mean': xr.DataArray(trend_mean, dims = ['lat', 'lon'],
                                 coords = {'lat': target_lat,
                                           'lon': target_lon}),
            'min': xr.DataArray(trend_min, dims = ['lat', 'lon'],
                                 coords = {'lat': target_lat,
                                           'lon': target_lon}),
            'max': xr.DataArray(trend_max, dims = ['lat', 'lon'],
                                 coords = {'lat': target_lat,
                                           'lon': target_lon}),
            'pct_sig_neg': xr.DataArray(pct_sig_neg, dims = ['lat', 'lon'],
                                        coords = {'lat': target_lat,
                                                  'lon': target_lon}),
            'pct_sig_pos': xr.DataArray(pct_sig_pos, dims = ['lat', 'lon'],
                                        coords = {'lat': target_lat,
                                                  'lon': target_lon})} \
       ).to_netcdf(os.path.join(mg.path_out(),
                                'standard_diagnostics_cmip5',
                                'sm_historical_' + \
                                str(year_cmip5[0]) + '-' + \
                                str(year_cmip5[-1]) + '_' + dcm + \
                                '_summary_trend.nc'))
