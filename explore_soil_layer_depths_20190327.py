# -*- coding: utf-8 -*-
"""
Created on Wed Mar 27 13:00:16 2019

@author: ywang254

Black vertical lines: soil layer interfaces of the land surface models
Grey lines: potential soil layers interfaces to interpolate to

A soil layer can be interpolated to only under two conditions:
    (1) two black lines exactly match two grey lines, i.e. the soil layer
        defined by two grey lines exactly matches a soil layer defined in the
        land surface model
    (2) there is one or more black line between two grey lines, i.e. the 
        soil layer defined by two grey lines can be interpolated to from 
        several soil layers defined in the land surface model

Because (2) is satisfied more often between 10cm and 30cm, than between 
10cm and 20cm, or 10cm and 25cm, the second soil layer is set to be 10-30cm. 
"""

import numpy as np
import matplotlib.pyplot as plt

soil_interface = {'ESA CCI': np.cumsum([0., 0.1]), # actually surface depth
    'BIOME-BGC': np.cumsum([0., 2.]), # actually reference depth
    'CLASS-CTEM-N': np.cumsum([0.1, 0.25, 3.65]), 
    'CLM4VIC': np.array([0.00175, 0.0451, 0.0906, 0.1655, 0.2891, 0.4929, 
                         0.8289, 1.383, 2.2961, 3.8019, 6.2845, 10.3775, 
                         17.1259, 28.2520, 42.1031]), 
    'CLM4': np.array([0.00175, 0.0451, 0.0906, 0.1655, 0.2891, 0.4929, 
                      0.8289, 1.383, 2.2961, 3.8019, 6.2845, 10.3775, 
                      17.1259, 28.2520, 42.1031]), 
    'GTEC': np.array([0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.75, 1., 1.25, 1.5, 
                      2., 3., 4., 5]), 
    'LPJ-wsl': np.cumsum([0., 0.5, 1.]), 
    'TRIPLEX-GHG': np.cumsum([0., 1.]), 
    'MsTMIP_VISIT': np.cumsum([0., 1.]), 
    'CABLE-POP': np.cumsum([0., 0.022, 0.058, 0.154, 0.409, 1.805, 2.872]), 
    'CLM5.0': np.cumsum([0.02, 0.06, 0.12, 0.2, 0.32, 0.48, 0.68, 0.92, 
                         1.2, 1.52, 1.88, 2.28, 2.72, 3.26, 3.9, 4.64, 
                         5.48, 6.42, 7.46, 8.6, 10.99, 15.666, 23.301, 
                         34.441, 49.556]), 
    'ISAM': np.cumsum([0.017513, 0.027579, 0.04547, 0.074967, 0.1236, 
                       0.203783, 0.335981, 0.553938, 0.91329, 1.136972]), 
    'JSBACH': np.cumsum([0.065, 0.254, 0.913, 2.902, 5.7]), 
    'JULES': np.cumsum([0.1, 0.25, 0.65, 2]), 
    'LPJ': np.cumsum([0., 0.5, 1.]), 
    'LPJ-GUESS': np.cumsum([0., 0.5, 1.]), 
    'LPX-Bern': np.cumsum([0, 0.5, 1.]), 
    'OCN': np.cumsum([0., 1.]), 
    'ORCHIDEE': np.cumsum([0.001, 0.003, 0.006, 0.012, 0.023, 0.047, 0.094,
                           0.188, 0.375, 0.750, 0.500]), 
    'ORCHIDEE-CNP': np.cumsum([0.001, 0.003, 0.006, 0.012, 0.023, 0.047, 0.094,
                               0.188, 0.375, 0.750, 0.500]), 
    'TRENDY_VISIT': np.cumsum([0., 0.3, 1.2]),
    'GLEAMv3.2a': np.cumsum([0., 0.1]), 
    'GLDAS_CLM': np.array([0., 0.018, 0.045, 0.091, 0.166, 0.289, 0.493, 
                           0.829, 1.383, 2.296, 3.433]), 
    'GLDAS_Noahv2.0': np.array([0., 0.10, 0.40, 1., 2.]), 
    'GLDAS_Noahv2.1': np.array([0., 0.10, 0.40, 1., 2.]), 
    'GLDAS_VIC': np.array([0., 0.1, 1.6, 1.9]), 
    'ERA-Interim': np.array([0., 0.07, 0.21, 0.72, 1.89]),
    'MERRA2': np.array([0., 1.]), # 1m or bedrock depth if shallower
    'CFSR': np.array([0., 0.1, 0.4, 1., 2.]), 
    'CERA-20C': np.cumsum([0., 0.07, 0.21, 0.72, 1.89]), 
    'ERA-20C': np.cumsum([0., 0.07, 0.21, 0.72, 1.89])}


soil_models = np.sort(list(soil_interface.keys()))
row_height = 5

fig,ax = plt.subplots(figsize=(10,8))
rect = plt.Rectangle((0.,0), 2.51, len(soil_models) * row_height, color = 'yellow')
# add background
ax.add_patch(rect)
# add the soil interfaces in each model
for mm,md in enumerate(soil_models):
    for x in soil_interface[md]:
        ax.plot([x, x], [mm*row_height, (mm+1)*row_height], '-k')
for mm in range(len(soil_models)):
    ax.axhline(y = mm*row_height, color='k')
for hh in [0.1, 0.2, 0.25, 0.3, 0.5, 1.]:
    ax.axvline(hh, color='grey', linestyle='--')
ax.set_xlim(0., 2.5)
ax.set_xticks(np.arange(0., 2.51, 0.05))
ax.set_xticklabels(labels = ['%.2f' % x for x in np.arange(0., 2.51, 0.05)], 
                             rotation = 90)
ax.set_xlabel('Soil Layer Depth (m)')
ax.set_ylim([0., len(soil_models) * row_height])
ax.set_yticks(row_height/2 + \
              np.arange(0., len(soil_models) * row_height, row_height))
ax.tick_params('both', length = 0.)
ax.set_yticklabels(soil_models)