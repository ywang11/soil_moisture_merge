"""
2019/08/05
ywang254@utk.edu

Plot the time series of global mean annual mean soil moisture of the 
 emergent constraint results, with the upper CI and lower CI.
Show the GLEAMv3.3 and ERA-Land time series of global mean annual mean
 soil moisture for comparison.

The purpose is to determine which setup's trend and temporal variability is
 the most reasonable.
"""
import matplotlib.pyplot as plt
import utils_management as mg
from utils_management.constants import year_cmip5, year_longest, \
    year_shorter, year_shorter2, year_shortest, depth, depth_cm
from misc.plot_utils import plot_ts_shade, plot_ts_trend
import pandas as pd
import os
import numpy as np
import itertools as it
import multiprocessing as mp


# MODIFY
model_set = 'cmip5' # 'lsm', 'cmip5', 'cmip6'
if model_set == 'lsm':
    method_list = ['year_month_9grid', 'year_month_anomaly_9grid',
                   'month_1grid', 'month_anomaly_1grid',
                   'year_mw_9grid', 'year_mw_anomaly_9grid',
                   'year_mw_1grid', 'year_mw_anomaly_1grid']
    year_list = [year_longest, year_shorter, year_shorter2, year_shortest]
else:
    method_list = ['year_month_9grid', 'year_month_anomaly_9grid',
                   'year_month_1grid', 'year_month_anomaly_1grid',
                   'month_1grid', 'month_anomaly_1grid',
                   'year_mw_9grid', 'year_mw_anomaly_9grid',
                   'year_mw_1grid', 'year_mw_anomaly_1grid']
    year_list = [year_longest]


pr_obs = 'CRU_v4.03'
season_list = ['annual', 'DJF', 'MAM', 'JJA', 'SON']


if model_set == 'lsm': 
    title = 'EC-ORS'
elif model_set == 'cmip5':
    title = 'EC-CMIP5'
else:
    title = 'EC-CMIP6'


#
##DEBUG
##for method, year in [['year_month_9grid', year_longest]]:
def plotter(option):
    method, year = option

    print(method, year)

    year_str = str(year[0]) + '-' + str(year[-1])

    global_ts_all = {}
    for i,season in it.product(range(len(depth)), season_list):
        d = depth[i]
        dcm = depth_cm[i]

        #
        CI_lower = pd.read_csv(os.path.join(mg.path_out(), 
                                            'standard_diagnostics_em_' + \
                                            model_set, pr_obs + \
                                            '_predicted_CI_lower_' + \
                                            method + '_' + year_str + '_' + \
                                            d + '_g_ts.csv'),
                               index_col = 0, parse_dates = True).iloc[:, 0]
        CI_upper = pd.read_csv(os.path.join(mg.path_out(), 
                                            'standard_diagnostics_em_' + \
                                            model_set, pr_obs + \
                                            '_predicted_CI_upper_' + \
                                            method + '_' + year_str + '_' + \
                                            d + '_g_ts.csv'),
                               index_col = 0, parse_dates = True).iloc[:, 0]
        point = pd.read_csv(os.path.join(mg.path_out(),
                                         'standard_diagnostics_em_' + \
                                         model_set, pr_obs + \
                                         '_predicted_' + method + \
                                         '_' + year_str + '_' + d + \
                                         '_g_ts.csv'),
                            index_col = 0, parse_dates = True).iloc[:, 0]

        #
        if 'anomaly' in method:
            CI_lower = CI_lower + point
            CI_upper = CI_upper + point

        # ---- convert to seasonal results
        global_ts = pd.concat([CI_lower, CI_upper, point], axis = 1)
        global_ts.columns = ['min', 'max', 'mean']
        if season == 'annual':
            global_ts = global_ts.groupby(global_ts.index.year).mean()
        elif season == 'DJF':
            global_ts = global_ts.loc[(global_ts.index.month == 12) | \
                                      (global_ts.index.month == 1) | \
                                      (global_ts.index.month == 2)]
            global_ts = global_ts.groupby(global_ts.index.year).mean()
        elif season == 'MAM':
            global_ts = global_ts.loc[(global_ts.index.month == 3) | \
                                      (global_ts.index.month == 4) | \
                                      (global_ts.index.month == 5)]
            global_ts = global_ts.groupby(global_ts.index.year).mean()
        elif season == 'JJA':
            global_ts = global_ts.loc[(global_ts.index.month == 6) | \
                                      (global_ts.index.month == 7) | \
                                      (global_ts.index.month == 8)]
            global_ts = global_ts.groupby(global_ts.index.year).mean()
        else:
            global_ts = global_ts.loc[(global_ts.index.month == 9) | \
                                      (global_ts.index.month == 10) | \
                                      (global_ts.index.month == 11)]
            global_ts = global_ts.groupby(global_ts.index.year).mean()

        # ---- convert to anomalies by removing the climatology of point
        #      estimate.
        global_ts = global_ts - global_ts['mean'].mean()

        global_ts_all[(d, season)] = global_ts


    # GLEAM v3.3a is only available for 0-10cm.
    gleam = pd.read_csv(os.path.join(mg.path_out(), 
                                     'standard_diagnostics_lsm', 
                                     'GLEAMv3.3a_0.00-0.10_g_ts.csv'),
                        index_col = 0, parse_dates = True)
    gleam_all = {}
    for season in season_list:
        if season == 'annual':
            gleam2= gleam.groupby(gleam.index.year).mean()
        elif season == 'DJF':
            gleam2 = gleam.loc[(gleam.index.month == 12) | \
                               (gleam.index.month == 1) | \
                               (gleam.index.month == 2)]
            gleam2 = gleam2.groupby(gleam2.index.year).mean()
        elif season == 'MAM':
            gleam2 = gleam.loc[(gleam.index.month == 3) | \
                               (gleam.index.month == 4) | \
                               (gleam.index.month == 5)]
            gleam2 = gleam2.groupby(gleam2.index.year).mean()
        elif season == 'JJA':
            gleam2 = gleam.loc[(gleam.index.month == 6) | \
                               (gleam.index.month == 7) | \
                               (gleam.index.month == 8)]
            gleam2 = gleam2.groupby(gleam2.index.year).mean()
        else:
            gleam2 = gleam.loc[(gleam.index.month == 9) | \
                               (gleam.index.month == 10) | \
                               (gleam.index.month == 11)]
            gleam2 = gleam2.groupby(gleam2.index.year).mean()
        gleam_all[season] = gleam2 - gleam2.mean()


    # ERA-Land is only available for the two shallow depths.
    eraland_all = {}
    for i,d in enumerate(['0.00-0.10', '0.10-0.30']):
        eraland = pd.read_csv(os.path.join(mg.path_out(),
                                           'standard_diagnostics_lsm', 
                                           'ERA-Land_' + d + '_g_ts.csv'),
                              index_col = 0, parse_dates = True)
        for season in season_list:
            if season == 'annual':
                eraland0 = eraland.groupby(eraland.index.year).mean()
            elif season == 'DJF':
                eraland0 = eraland.loc[(eraland.index.month == 12) | \
                                       (eraland.index.month == 1) | \
                                       (eraland.index.month == 2)]
                eraland0 = eraland0.groupby(eraland0.index.year).mean()
            elif season == 'MAM':
                eraland0 = eraland.loc[(eraland.index.month == 3) | \
                                       (eraland.index.month == 4) | \
                                       (eraland.index.month == 5)]
                eraland0 = eraland0.groupby(eraland0.index.year).mean()
            elif season == 'JJA':
                eraland0 = eraland.loc[(eraland.index.month == 6) | \
                                       (eraland.index.month == 7) | \
                                       (eraland.index.month == 8)]
                eraland0 = eraland0.groupby(eraland0.index.year).mean()
            else:
                eraland0 = eraland.loc[(eraland.index.month == 9) | \
                                       (eraland.index.month == 10) | \
                                       (eraland.index.month == 11)]
                eraland0 = eraland0.groupby(eraland0.index.year).mean()
            eraland_all[(d, season)] = eraland0 - eraland0.mean()


    # 
    fig, axes = plt.subplots(nrows = len(depth_cm), 
                             ncols = len(season_list), sharex = True, 
                             sharey = True, figsize = (16., 8.))
    fig.subplots_adjust(hspace = 0., wspace = 0.)
    for i, sn in it.product(range(len(depth)), range(len(season_list))):
        d = depth[i]
        dcm = depth_cm[i]
        season = season_list[sn]
    
        ax = axes[i, sn]
        # Do not display the uncertainty envelop because it is too big
        # to be effectively visualized with the other data.
        ##plot_ts_shade(ax, global_ts_all[(d, season)].index, 
        ##              global_ts_all[(d, season)], ts_col = 'k')

        h1, _ = plot_ts_trend(ax, global_ts_all[(d, season)].index, 
                              global_ts_all[(d, season)]['mean'], 0.1, 0.1)
        if i == 0:
            h2, = ax.plot(gleam_all[season].index, 
                          gleam_all[season].values, '-r', 
                          linewidth = 1, zorder = 3)
        if (i == 0) | (i == 1):
            h3, = ax.plot(eraland_all[(d, season)].index, 
                          eraland_all[(d, season)], '-b',
                          linewidth = 1, zorder = 3)

        ax.set_xlim([year[0], year[-1]])
        ax.set_xlabel('Year')
        #ax.set_ylim([-0.005, 0.005])
        #if sn == 0:
        #    ax.set_yticks(np.linspace(-0.004, 0.004, 5))
        #else:
        #    ax.set_yticks([])

        if i == 0:
            ax.set_title(season)
        if sn == 0:
            ax.set_ylabel(dcm)
        if (i == 0) & (sn == 0):
            ax.legend([h1, h2, h3], [title, 'GLEAMv3.3a', 'ERA-Land'],
                      loc = (3, -3), ncol = 3)

    fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                             'ts_global', 'em_anomaly', model_set + '_' + \
                             method + '_' + year_str + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)


pool = mp.Pool(4)
pool.map_async(plotter, list(it.product(method_list, year_list)))
pool.close()
pool.join()
