"""
20190923
ywang254@utk.edu

Concatenate the four time periods of the CDF-matching rescaled DOLCE-LSM 
 products to produce a consecutive product.

The ALL product was still problematic. Many NaN's in the Evergreen
Broadleaf Forests!!!
"""
import xarray as xr
import os
import pandas as pd
import numpy as np
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth, target_lat, target_lon
import utils_management as mg
from misc.ismn_utils import get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
import itertools as it


simple = [True, False, False]
lu_weighted = [False, False, True]
lu_threshold = 40
ismn_aggr_method = [get_ismn_aggr_method(simple[i], lu_weighted[i], 
                                         lu_threshold) for i in range(3)]
cov_method = [get_cov_method(i) for i in range(5)]
prod_list = ['all', 'lsm']


for d, iam, cov, prod in it.product(depth, ismn_aggr_method, cov_method,
                                    prod_list):
    time_range = pd.date_range(str(year_longest[0]) + '-01-01',
                               str(year_longest[-1]) + '-12-31', freq = 'MS')

    concat = xr.DataArray(data = np.empty([len(time_range), 
                                           len(target_lat), len(target_lon)]),
                          dims = ['time', 'lat', 'lon'],
                          coords = {'time': time_range, 
                                    'lat': target_lat, 'lon': target_lon})

    time_segment = [pd.date_range('1950-01-01', '1980-12-31', freq = 'MS'),
                    pd.date_range('1981-01-01', '2010-12-31', freq = 'MS'),
                    pd.date_range('2011-01-01', '2016-12-31', freq = 'MS')]

    for year, ts in zip([year_shorter, year_shortest, year_shorter2], 
                        time_segment):
        if (year[0] == 1981) & (year[-1] == 2016):
            data = xr.open_dataset(os.path.join(mg.path_out(), 
                                                'dolce_' + prod + '_product',
                                                'weighted_average_' + \
                                                str(year[0]) + '-' + \
                                                str(year[-1]) + '_' + d + \
                                                '_' + iam + '_' + cov + \
                                                '.nc'),
                                   decode_times = False)
        else:
            data = xr.open_dataset(os.path.join(mg.path_out(),
                                                'concat_dolce_' + prod,
                                                'restored_' + str(year[0]) + \
                                                '-' + str(year[-1]) + '_' + \
                                                d + '_' + iam + '_' + cov + \
                                                '.nc'), decode_times = False)
        data['time'] = pd.date_range(str(year[0]) + '-01-01',
                                     str(year[-1]) + '-12-31', freq = 'MS')
        concat.loc[ts, :, :] = data.sm.loc[ts, :, :].values.copy()
        data.close()

    concat.to_dataset(name = 'sm').to_netcdf(os.path.join(mg.path_out(),
        'concat_dolce_' + prod, 
        'concat_average_' + d + '_' + iam + '_' + cov + '.nc'))
