"""
20190923
ywang254@utk.edu

Step 3 of the CDF-matching merging:
 Apply the CDF matching on the anomalies.
"""
import xarray as xr
import os
import pandas as pd
import numpy as np
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth_cm, target_lat, target_lon
import utils_management as mg
from misc.ismn_utils import get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
from misc.concat_utils import piecewise_cdf_create, piecewise_cdf_apply
import itertools as it
import multiprocessing as mp


met_obs = 'CRU_v4.03'
em = 'year_month_anomaly_9grid' # Need to modify the read script if not
                                # this method
prefix = ['predicted']
prod_list = ['lsm', 'all']


i = REPLACE1
fx = prefix[REPLACE2]
prod = prod_list[REPLACE3]


#args_list = []
#for i, fx, prod in it.product(range(len(depth_cm)), prefix, prod_list):
#    args_list.append([i, fx, prod])
#
#for args in [args_list[0]]:
#def rescale(args):
#    i, fx, prod = args
dcm = depth_cm[i]

time_range = pd.date_range('1981-01-01', '2010-12-31', freq = 'MS')

data_ref = xr.open_dataset(os.path.join(mg.path_out(), 'concat_em_' + prod,
                                        'anomaly_' + met_obs + '_' + em + \
                                        '_' + dcm + '_' + fx + '_' + \
                                        str(year_shorter2[0]) + '-' + \
                                        str(year_shorter2[-1]) + '.nc'),
                           decode_times = False)
data_ref['time'] = pd.date_range(str(year_shorter2[0]) + '-01-01',
                                 str(year_shorter2[-1]) + '-12-31',
                                 freq = 'MS')
data_ref[fx].load()

for year in [year_shorter, year_shortest]:
    year_str = str(year[0]) + '-' + str(year[-1])

    data = xr.open_dataset(os.path.join(mg.path_out(), 'concat_em_' + prod,
                                        'anomaly_' + met_obs + '_' + em + \
                                        '_' + dcm + '_' + fx + '_' + \
                                        year_str + '.nc'),
                           decode_times = False)
    data['time'] = pd.date_range(str(year[0]) + '-01-01',
                                 str(year[-1]) + '-12-31', freq = 'MS')
    data[fx].load()

    anomalies = data[fx].values.copy()

    for a,b in it.product(range(len(data.lat)), range(len(data.lon))):
        if np.isnan(anomalies[0,a,b]):
            continue
        ##else:
        ##    #DEBUG
        ##    break

        # Inter-calibration functions.
        ref = data_ref[fx][(data_ref.time.indexes['time'] >= \
                            time_range[0]) & \
                           (data_ref.time.indexes['time'] <= \
                            time_range[-1]), a, b].values
        x = data[fx][(data.time.indexes['time'] >= time_range[0]) & \
                     (data.time.indexes['time'] <= time_range[-1]),
                     a, b].values

        slope, threshold, intercept = piecewise_cdf_create(x, ref)
        anomalies[:,a,b] = piecewise_cdf_apply(slope, threshold, intercept,
                                               anomalies[:,a,b])

        if np.isnan(np.sum(anomalies[:,a,b])):
            break

    anomalies = xr.DataArray(anomalies, dims = ['time','lat','lon'],
                             coords = {'time': data.time,
                                       'lat': data.lat,  
                                       'lon': data.lon})

    anomalies.to_dataset(name = fx \
    ).to_netcdf(os.path.join(mg.path_out(), 'concat_em_' + prod,
                             'scaled_' + met_obs + '_' + em + '_' + \
                             dcm + '_' + fx + '_' + year_str + '.nc'))
    data.close()


#pool = mp.Pool(4)
#pool.map_async(rescale, args_list)
#pool.close()
#pool.join()
