# -*- coding: utf-8 -*-
"""
Created on Tue Apr 16 12:59:05 2019

@author: ywang254

Display the number of good monthly soil moisture points in each grid a global
map of 0.5 degrees. If multiple stations exist in the same grid, use the
overlapping time period between them.
"""
import numpy as np
import subprocess
from utils_management.constants import depth
import utils_management as mg
import os
import pandas as pd
import cartopy
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER


lat_grid = np.arange(-89.75, 89.75, 0.5)
lon_grid = np.arange(-179.75, 179.75, 0.5)

lat_parallel = np.arange(-90., 90., 0.5)
lon_parallel = np.arange(-180., 180., 0.5)

n_months = np.zeros([len(lat_grid), len(lon_grid)])

# Use the aggregated soil moisture data to determine the number of months
# that have soil moisture data for calibration and validation.
# Sum all depths.
for i,d in enumerate(depth):
    data = pd.read_csv(os.path.join(mg.path_intrim_out(), 'ismn_aggr',
                                    'weighted_monthly_data_lu_weighted_' + \
                                    d + '.csv'), index_col = 0)
    for ind, p in data.iteritems():
        which_lat = np.argmin(np.abs(lat_grid - p['Lat']))
        which_lon = np.argmin(np.abs(lon_grid - p['Lon']))
        n_months[which_lat,which_lon] += sum(p.iloc[2:] > 0)

n_months[n_months == 0.] = np.nan

##fig, axes = plt.subplots(nrows = 2, ncols = 3, figsize=(12,10), 
##                         subplot_kw = {'projection': \
##                                       cartopy.crs.PlateCarree()})
fig = plt.figure(figsize = (10, 8))
axes = [None] * 6
axes[0] = fig.add_axes([0., 0.5, 0.5, 0.5],
                       projection = cartopy.crs.PlateCarree())
axes[1] = fig.add_axes([0., 0.05, 0.28, 0.5],
                       projection = cartopy.crs.PlateCarree())
axes[2] = fig.add_axes([0.345, 0.05, 0.28, 0.5],
                       projection = cartopy.crs.PlateCarree())
axes[3] = fig.add_axes([0.57, 0.7, 0.36, 0.23],
                       projection = cartopy.crs.PlateCarree())
axes[4] = fig.add_axes([0.57, 0.54, 0.3, 0.13],
                       projection = cartopy.crs.PlateCarree())
axes[5] = fig.add_axes([0.6, 0.1, 0.32, 0.37],
                       projection = cartopy.crs.PlateCarree())
for ax_ind, ax in enumerate(axes):
    ax.coastlines(color='grey')
    #ax.set_yticks(np.arange(-80., 90., 20))
    #ax.set_xticks(np.arange(-180., 180., 20))

    h = ax.pcolormesh(lon_grid, lat_grid, n_months, vmin=1, vmax=500, 
                      transform=cartopy.crs.PlateCarree(), 
                      cmap=plt.cm.get_cmap('jet'))

    if ax_ind == 0:
        ax.set_extent([-150, -60, 25, 65])
        ax.set_title('North America')
    elif ax_ind == 1:
        ax.set_extent([80, 140, 0., 55])
        ax.set_title('Asia')
    elif ax_ind == 2:
        ax.set_extent([-10, 40, 35, 70])
        ax.set_title('Europe')
    elif ax_ind == 3:
        ax.set_extent([-20, 40, 0, 20])
        ax.set_title('Africa')
    elif ax_ind == 4:
        ax.set_extent([140, 160, -40, -30])
        ax.set_title('Oceania')
    elif ax_ind == 5:
        ax.set_extent([-80, -60, -58, -20])
        ax.set_title('South America')

    gl = ax.gridlines(crs=cartopy.crs.PlateCarree(), linewidth=2,
                      color='black', lw = 0,
                      linestyle='--', draw_labels=True)
    gl.xlabels_top = False
    gl.ylabels_right = False
    if ax_ind == 3:
        gl.xlocator = mticker.FixedLocator(range(-180, 181, 10))
        gl.ylocator = mticker.FixedLocator(range(-90, 91, 10))
    elif ax_ind == 4:
        gl.xlocator = mticker.FixedLocator(range(-180, 181, 5))
        gl.ylocator = mticker.FixedLocator(range(-90, 91, 5))
    elif ax_ind == 5:
        gl.xlocator = mticker.FixedLocator(range(-180, 181, 5))
        gl.ylocator = mticker.FixedLocator(range(-90, 91, 5))
    else:
        gl.xlocator = mticker.FixedLocator(range(-180, 181, 20))
        gl.ylocator = mticker.FixedLocator(range(-90, 91, 10))
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'color': 'k', 'weight': 'normal'}

# add colorbar
cbar_ax = fig.add_axes([0.97, 0.1, 0.01, 0.8])
cbar = fig.colorbar(h, ax = ax, cax=cbar_ax)
cbar.ax.tick_params(labelsize=12)
cbar.set_label('Number of Monthly Observations', fontsize=12)
fig.savefig(os.path.join(mg.path_intrim_out(), 'ismn_metadata', 
                         'ismn_display_map.png'), bbox_inches = 'tight',
            dpi = 600.)


# Stations with only one month is excluded
n_months2 = n_months.copy()
n_months2[n_months2 == 1.] = np.nan

fig = plt.figure(figsize=(20,15))
ax = fig.add_subplot(111, projection=cartopy.crs.PlateCarree())
ax.set_extent([-180, 180, -70, 90])
ax.coastlines(color='black')
h = ax.pcolormesh(lon_grid, lat_grid, n_months2, vmin=0, vmax=500, 
                  transform=cartopy.crs.PlateCarree(), 
                  cmap=plt.cm.get_cmap('rainbow'))
#ax.set_yticks(np.arange(-90., 90., 15))
#ax.set_xticks(np.arange(-180., 180., 30))
#ax.set_extent([130,170,-45,-25])
gl = ax.gridlines(crs=cartopy.crs.PlateCarree(), linewidth=2, color='black',
                  lw = 0, alpha=0.5, linestyle='--', draw_labels=True)
gl.xlabels_top = False
gl.ylabels_left = False
gl.xlocator = mticker.FixedLocator(range(-180, 181, 20))
gl.ylocator = mticker.FixedLocator(range(-90, 91, 10))
gl.xformatter = LONGITUDE_FORMATTER
gl.yformatter = LATITUDE_FORMATTER
gl.xlabel_style = {'color': 'k', 'weight': 'normal'}
# add colorbar
axpos = ax.get_position()
cbar_ax = fig.add_axes([axpos.x1+0.05,axpos.y0,0.03,axpos.height])
cbar = fig.colorbar(h, cax=cbar_ax)
cbar.ax.tick_params(labelsize=12)
cbar.set_label('Number of Monthly Observations', fontsize=12)
fig.savefig(os.path.join(mg.path_intrim_out(), 'ismn_metadata', 
                         'ismn_display_map_exclude1.png'), 
            bbox_inches = 'tight')
