"""
2019/08/05
ywang254@utk.edu

Plot the time series of global mean annual mean soil moisture of the 
 final product (Median of the DOLCE and EM results), with the following
 two sets of confidence intervals: 5th and 95th percentiles of the source
 LSM+CMIP5+CMIP6 models, and the min and max of the individual products.
Show the GLEAMv3.3 and ERA-Land time series of global mean annual mean
 soil moisture for comparison.
"""
import matplotlib.pyplot as plt
import utils_management as mg
from utils_management.constants import year_longest, year_shortest, \
    depth, depth_cm, lsm_list, lsm_all
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability, one_layer_availability
from misc.plot_utils import plot_ts_shade2, plot_ts_trend
import pandas as pd
import os
import numpy as np


# MODIFY
season = 'JJA' # 'annual', 'DJF', 'MAM', 'JJA', 'SON'


#
year = year_longest
year_str = str(year[0]) + '-' + str(year[-1])
year_range = pd.date_range(str(year[0]) + '-01-01', 
                           str(year[-1]) + '-12-31', freq = 'MS')
year_climatology = year_shortest
year_range_climatology = pd.date_range('1981-01-01', 
                                       '2010-12-31', freq = 'MS')

iam = 'lu_weighted'
cov_method = 'ShrunkCovariance'
land_mask = 'vanilla'

#
lsm_all.remove('ESA-CCI') # skip this model's surface data

#
global_ts_all_depth = {}
global_ts_all_depth_anomaly = {}
for i,d in enumerate(depth):
    dcm = depth_cm[i]

    global_ts = pd.DataFrame(data = np.nan, index = year,
                             columns = ['mean', 'bottom', 'lower', 
                                        'upper', 'top'])
    global_ts_anomaly = pd.DataFrame(data = np.nan, index = year,
                                     columns = ['mean', 'bottom', 'lower', 
                                                'upper', 'top'])

    if season == 'DJF':
        subset = (year_range.month == 12) | (year_range.month == 1) | \
                 (year_range.month == 2)
    elif season == 'MAM':
        subset = (year_range.month == 3) | (year_range.month == 4) | \
                 (year_range.month == 5)
    elif season == 'JJA':
        subset = (year_range.month == 6) | (year_range.month == 7) | \
                 (year_range.month == 8)
    elif season == 'SON':
        subset = (year_range.month == 9) | (year_range.month == 10) | \
                 (year_range.month == 11)
    elif season == 'annual':
        subset = np.full(len(year_range), True, dtype=bool)


    ###########################################################################
    # Median estimate of the products.
    ###########################################################################
    data = pd.read_csv(os.path.join(mg.path_out(), 
                                    'standard_diagnostics_meanmedian_products',
                                    'median_' + year_str + '_' + d + \
                                    '_g_ts.csv'),
                       index_col = 0, parse_dates = True).iloc[:, 0]
    data = data.loc[subset]
    data = data.groupby(data.index.year).mean()

    global_ts.loc[:, 'mean'] = data
    global_ts_anomaly.loc[:, 'mean'] = data - data.loc[year_climatology].mean()


    ###########################################################################
    # Range of the source datasets.
    ###########################################################################
    cmip5 = cmip5_availability(dcm, land_mask)
    cmip6_list_1 = mrsol_availability(dcm, land_mask, 'historical')
    cmip6_list_2 = mrsol_availability(dcm, land_mask, 'ssp585')
    cmip6_list_3 = one_layer_availability('pr', land_mask, 'historical')
    cmip6_list_4 = one_layer_availability('pr', land_mask, 'ssp585')
    cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) & \
                       set(cmip6_list_3) & set(cmip6_list_4) )
    cmip6_list = [x for x in cmip6_list if 'r1i1p1f1' in x]

    data = pd.DataFrame(data = np.nan, index = year_range, 
                        columns = lsm_all + cmip5 + cmip6_list)
    for l in lsm_all:
        try:
            dummy = pd.read_csv(os.path.join(mg.path_out(),
               'standard_diagnostics_lsm', l + '_' + d + '_g_ts.csv'),
                index_col = 0, parse_dates = True).iloc[:, 0]
        except FileNotFoundError:
            continue
        overlap = sorted(list(set(dummy.index) & set(data.index)))
        data.loc[overlap, l] = dummy.loc[overlap]
    for l in cmip5:
        data.loc[:, l] = pd.read_csv(os.path.join(mg.path_out(),
            'standard_diagnostics_cmip5',
            l + '_' + dcm + '_g_ts.csv'),
            index_col = 0, parse_dates = True).iloc[:, 0]
    for l in cmip6_list:
        data.loc[:, l] = pd.read_csv(os.path.join(mg.path_out(),
            'standard_diagnostics_cmip6',
            'mrsol_' + l + '_' + dcm + '_g_ts.csv'),
            index_col = 0, parse_dates = True).iloc[:, 0]

    data = data.loc[subset]
    data = data.groupby(data.index.year).mean()
    data_anomaly = data - data.loc[year_climatology, :].mean(axis=0)

    # Clean up some LSM data for consistency with the final product.
    # ---- 1950-1980: uses the lsm of 1950-2010
    # ---- 1981-2010: uses the lsm of 1981-2010
    # ---- 2010-2016: uses the lsm of 1981-2016
    for yy,yy2 in zip([('1950','2010'), ('1981','2010'), ('1981','2016')],
                       [(1950,1980), (1981,2010), (2011,2016)]):
        lsm = lsm_list[(yy[0] + '-' + yy[1], d)]
        if (d == '0.00-0.10') & (yy[0] == '1981'):
            lsm.remove('ESA-CCI')
        cols = lsm + cmip5 + cmip6_list
        rows = range(yy2[0],  yy2[1]+1)
        global_ts.loc[rows, 'bottom'] = \
            np.percentile(data.loc[rows, cols], 5, axis = 1)
        global_ts.loc[rows, 'top'] = \
            np.percentile(data.loc[rows, cols], 95, axis = 1)
        global_ts_anomaly.loc[rows, 'bottom'] = \
            np.percentile(data_anomaly.loc[rows, cols], 5, axis = 1)
        global_ts_anomaly.loc[rows, 'top'] = \
            np.percentile(data_anomaly.loc[rows, cols], 95, axis = 1)


    ###########################################################################
    # Range of the products.
    ###########################################################################
    data = pd.DataFrame(data = np.nan, index = year_range, 
                        columns = ['dolce_lsm', 'dolce_cmip5', 'dolce_cmip6',
                                   'em_lsm', 'em_cmip5', 'em_cmip6'])
    data.loc[:, 'dolce_lsm'] = pd.read_csv(os.path.join(mg.path_out(),
        'standard_diagnostics_concat', 'dolce',
        'dolce_average_lu_weighted_ShrunkCovariance_' + d + '_g_ts.csv'),
        index_col = 0, parse_dates = True).iloc[:, 0]
    for model in ['cmip5', 'cmip6']:
        data.loc[:, 'dolce_' + model] = pd.read_csv(os.path.join(mg.path_out(),
            'standard_diagnostics_dolce_' + model, 
            'lu_weighted_ShrunkCovariance_weighted_average_' + year_str + \
            '_' + d + '_g_ts.csv'), index_col = 0, 
            parse_dates = True).iloc[:, 0]
    data.loc[:, 'em_lsm'] = ( \
        pd.read_csv(os.path.join(mg.path_out(),
                                 'standard_diagnostics_em_lsm', 
                                 'CRU_v4.03_predicted_year_month_9grid_' + \
                                 year_str + '_' + d + '_g_ts.csv'), \
                                 index_col = 0, 
                    parse_dates = True).iloc[:, 0] + \
        pd.read_csv(os.path.join(mg.path_out(),
                                 'standard_diagnostics_em_lsm', 
                                 'GPCC_predicted_year_month_9grid_' + \
                                 year_str + '_' + d + '_g_ts.csv'), \
                    index_col = 0, parse_dates = True).iloc[:, 0]) * 0.5
    for model in ['cmip5', 'cmip6']:
        data.loc[:, 'em_' + model] = ( \
        pd.read_csv(os.path.join(mg.path_out(),
                                 'standard_diagnostics_em_' + model, 
                                 'CRU_v4.03_predicted_month_anomaly_1grid_' + \
                                 year_str + '_' + d + '_g_ts.csv'), \
                    index_col = 0, parse_dates = True).iloc[:, 0] + \
        pd.read_csv(os.path.join(mg.path_out(),
                                 'standard_diagnostics_em_' + model,
                                 'GPCC_predicted_month_anomaly_1grid_' + \
                                 year_str + '_' + d + '_g_ts.csv'), \
                    index_col = 0, parse_dates = True).iloc[:, 0] ) * 0.5

    data = data.loc[subset]
    data = data.groupby(data.index.year).mean()
    data_anomaly = data - data.loc[year_climatology, :].mean(axis=0)

    global_ts.loc[:, 'lower'] = np.min(data, axis = 1)
    global_ts.loc[:, 'upper'] = np.max(data, axis = 1)

    global_ts_anomaly.loc[:, 'lower'] = np.min(data_anomaly, axis = 1)
    global_ts_anomaly.loc[:, 'upper'] = np.max(data_anomaly, axis = 1)

    # Place into the larger data container.
    global_ts_all_depth[d] = global_ts
    global_ts_all_depth_anomaly[d] = global_ts_anomaly


# GLEAM v3.3a is only available for 0-10cm.
gleam = pd.read_csv(os.path.join(mg.path_out(), 
                                 'standard_diagnostics_lsm', 
                                 'GLEAMv3.3a_0.00-0.10_g_ts.csv'),
                    index_col = 0, parse_dates = True)
if season == 'DJF':
    subset = (gleam.index.month == 12) | (gleam.index.month == 1) | \
             (gleam.index.month == 2)
elif season == 'MAM':
    subset = (gleam.index.month == 3) | (gleam.index.month == 4) | \
             (gleam.index.month == 5)
elif season == 'JJA':
    subset = (gleam.index.month == 6) | (gleam.index.month == 7) | \
             (gleam.index.month == 8)
elif season == 'SON':
    subset = (gleam.index.month == 9) | (gleam.index.month == 10) | \
             (gleam.index.month == 11)
elif season == 'annual':
    subset = np.full(len(gleam.index), True, dtype=bool)
gleam = gleam.loc[subset, :]
gleam = gleam.groupby(gleam.index.year).mean()


# ERA-Land is only available for the two shallow depths.
eraland_all_depth = {}
for i,d in enumerate(['0.00-0.10', '0.10-0.30']):
    eraland = pd.read_csv(os.path.join(mg.path_out(),
                                       'standard_diagnostics_lsm',
                                       'ERA-Land_' + d + '_g_ts.csv'),
                          index_col = 0, parse_dates = True)

    if season == 'DJF':
        subset = (eraland.index.month == 12) | (eraland.index.month == 1) | \
                 (eraland.index.month == 2)
    elif season == 'MAM':
        subset = (eraland.index.month == 3) | (eraland.index.month == 4) | \
                 (eraland.index.month == 5)
    elif season == 'JJA':
        subset = (eraland.index.month == 6) | (eraland.index.month == 7) | \
                 (eraland.index.month == 8)
    elif season == 'SON':
        subset = (eraland.index.month == 9) | (eraland.index.month == 10) | \
                 (eraland.index.month == 11)
    elif season == 'annual':
        subset = np.full(len(eraland.index), True, dtype=bool)

    eraland_all_depth[d] = eraland.loc[subset, :]
    eraland_all_depth[d] = eraland_all_depth[d \
    ].groupby(eraland_all_depth[d].index.year).mean()


# 
fig, axes = plt.subplots(nrows = len(depth_cm), ncols = 2, sharex = True, 
                         sharey = False, figsize = (10, 6.5))
fig.subplots_adjust(hspace = 0., wspace = 0.2)

# (1) Soil Moisture
for i,d in enumerate(depth):
    dcm = depth_cm[i]

    ax = axes[i, 0]
    h1,h2,h3 = plot_ts_shade2(ax, global_ts_all_depth[d].index.values, 
                              global_ts_all_depth[d], ts_col = 'b')
    if i == 0:
        h4, = ax.plot(gleam.index, gleam.values.reshape(-1), '-r', 
                      linewidth = 1.5, zorder = 3)
    if (i == 0) | (i == 1):
        h5, = ax.plot(eraland_all_depth[d].index.values,
                      eraland_all_depth[d], '-k', 
                      linewidth = 1.5, zorder = 3)
    if (i == 0):
        ax.set_title('Soil Moisture (m$^3$/m$^3$)')

    if i == len(depth)-1:
        ax.legend([h1, h2, h3, h4, h5], 
                  ['Median-Products', '5$^{th}$-95$^{th}$ Raw',
                   'Min-Max Products', 'GLEAMv3.3a', 'ERA-Land'], 
                  ncol = 5, loc = 'lower right',
                  bbox_to_anchor = (2.2, -0.7))

    ax.set_xlim([year[0], year[-1]])
    ax.set_xlabel('Year')
    ax.set_ylabel(dcm) # '(m$^3$/m$^3$)'
    ax.set_ylim([0.08, 0.48])
    ax.set_yticks(np.arange(0.15, 0.451, 0.05))

# (2) Soil Moisture Anomaly
for i,d in enumerate(depth):
    dcm = depth_cm[i]

    ax = axes[i, 1]

    h1,h2,h3 = plot_ts_shade2(ax, 
                              global_ts_all_depth_anomaly[d].index.values, 
                              global_ts_all_depth_anomaly[d],
                              ts_col = 'b')

    if i == 0:
        h4, = ax.plot(gleam.index.values, gleam['global_mean'] - \
                      gleam['global_mean'].loc[ \
                          year_climatology].mean(), '-r',
                      linewidth = 1.5)
    if (i == 0) | (i == 1):
        h5, = ax.plot(eraland_all_depth[d].index.values, 
                      eraland_all_depth[d]['global_mean'] - \
                      eraland_all_depth[d]['global_mean' \
                          ].loc[year_climatology].mean(), '-k',
                      linewidth = 1.5)

    # Whole-period trend
    plot_ts_trend(ax, global_ts_all_depth_anomaly[d].index.values,
                  global_ts_all_depth_anomaly[d]['mean'].values, 0.05, 0.05,
                  ts_kw = {'color': 'b', 'marker': '.', 'markersize': 3.},
                  ln_kw = {'color': '#993404', 'linestyle': '--', 
                           'linewidth': 1.5, 'zorder': 3})
    # 1981-onwards trend
    plot_ts_trend(ax, global_ts_all_depth_anomaly[d].index.values[31:],
                  global_ts_all_depth_anomaly[d]['mean'].values[31:], 
                  0.2, 0.85,
                  ts_kw = {'color': 'b', 'marker': '.', 'markersize': 3.},
                  ln_kw = {'color': '#2ca25f', 'linestyle': '--', 
                           'linewidth': 1.5, 'zorder': 3})

    if (i == 0):
        ax.set_title('Anomaly (m$^3$/m$^3$)')

    ax.set_xlim([year[0], year[-1]])
    ax.set_xlabel('Year')
    ax.set_ylim([-0.01, 0.01])
    ax.set_yticks(np.arange(-0.008, 0.009, 0.002))

fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot', 
                         'ts_annual', 'products_w_trend_' + \
                         iam + '_' + cov_method + '_' + year_str + '_' + \
                         season + '.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)
