# -*- coding: utf-8 -*-
"""
Created on Wed Feb 27 21:22:41 2019

@author: ywang254
"""
from ecmwfapi import ECMWFDataServer
import utils_management as mg
import os
from time import sleep

def retrieve(yy):
    server = ECMWFDataServer()
    server.retrieve({
        "class": "ep",
        "dataset": "cera20c",
        "date": str(yy)+"0101/"+str(yy)+"0201/"+str(yy)+"0301/"+str(yy)+ \
                "0401/"+str(yy)+"0501/"+str(yy)+"0601/"+str(yy)+"0701/"+ \
                str(yy)+"0801/"+str(yy)+"0901/"+str(yy)+"1001/"+ \
                str(yy)+"1101/"+str(yy)+"1201",
        "expver": "1",
        "levtype": "sfc",
        "number": "0/1/2/3/4/5/6/7/8/9",
        "param": "39.128/40.128/41.128/42.128",
        "stream": "edmo",
        "type": "an",
        "target": os.path.join(mg.path_data(), "Reanalysis", "CERA20C", 
                               "Monthly_means_of_daily_means_"+str(yy)+".nc"),
    })


for yy in range(1990, 2011):
    retrieve(yy)

    sleep(1)