"""
20190923
ywang254@utk.edu

Concatenate the four time periods of the CDF-matching rescaled EM-LSM
 products to produce a consecutive product.
"""
import xarray as xr
import os
import pandas as pd
import numpy as np
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth_cm, target_lat, target_lon
import utils_management as mg
from misc.ismn_utils import get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
import itertools as it
import multiprocessing as mp


met_obs = 'CRU_v4.03'
em = 'year_month_anomaly_9grid' # Need to modify the read script if not
                                # this method
prefix = ['predicted']
prod_list = ['lsm', 'all']


args_list = []
for i, fx, prod in it.product(range(len(depth_cm)), prefix, prod_list):
    args_list.append([i, fx, prod])


def merge(args):
    i, fx, prod = args

    dcm = depth_cm[i]

    time_range = pd.date_range(str(year_longest[0]) + '-01-01',
                               str(year_longest[-1]) + '-12-31', freq = 'MS')

    concat = xr.DataArray(data = np.empty([len(time_range), 
                                           len(target_lat), len(target_lon)]),
                          dims = ['time', 'lat', 'lon'],
                          coords = {'time': time_range, 
                                    'lat': target_lat, 'lon': target_lon})

    time_segment = [pd.date_range('1950-01-01', '1980-12-31', freq = 'MS'),
                    pd.date_range('1981-01-01', '2010-12-31', freq = 'MS'),
                    pd.date_range('2011-01-01', '2016-12-31', freq = 'MS')]

    for year, ts in zip([year_shorter, year_shortest, year_shorter2], 
                        time_segment):
        year_str = str(year[0]) + '-' + str(year[-1])

        if (year[0] == 1981) & (year[-1] == 2016):
            data = xr.open_mfdataset([os.path.join(mg.path_out(),
                                                   'em_' + prod + '_corr',
                                                   met_obs + '_' + \
                                                   em.replace('anomaly',
                                                              'restored')+\
                                                   '_' + dcm + '_' + \
                                                   year_str,
                                                   fx + '_' + str(yr) + \
                                                   '.nc') for yr in year],
                                     decode_times = False, concat_dim = 'time')
        else:
            data = xr.open_dataset(os.path.join(mg.path_out(), 
                                                'concat_em_' + prod,
                                                'restored_' + met_obs + '_' + \
                                                em + '_' + dcm + '_' + fx + \
                                                '_' + year_str + '.nc'),
                                   decode_times = False)
        data[fx].load()
        data['time'] = pd.date_range(str(year[0]) + '-01-01',
                                     str(year[-1]) + '-12-31', freq = 'MS')
        concat.loc[ts, :, :] = data[fx].loc[ts, :, :].copy(deep = True)
        data.close()

    concat.to_dataset(name = fx).to_netcdf(os.path.join(mg.path_out(),
        'concat_em_' + prod,
        'concat_' + met_obs + '_' + em + '_' + dcm + '_' + \
        fx + '_' + str(year_longest[0]) + '-' + str(year_longest[-1]) + '.nc'))


#
pool = mp.Pool(6)
pool.map(merge, args_list)
pool.close()
pool.join()
