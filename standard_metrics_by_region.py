"""
2019/08/07
ywang254@utk.edu

Calculate the metrics between grid-averaged soil moisture observation
and different merged products by:
 - MODIS land cover type
 - continent
using the validation-period ISMN observations. 
"""
from utils_management.constants import year_longest, depth, depth_cm
import utils_management as mg
from misc.ismn_utils import get_weighted_monthly_data
from misc.analyze_utils import calc_stats
import os
import pandas as pd
import numpy as np
import itertools as it

# Subset of observations to compare on. Different periods produce similar
# metrics ("plot_standard_metrics_all.py") and have similar spatial
# availability ("ismn_summary.py").
opt = 'val'

# Other setups
iam = 'lu_weighted'

year = year_longest
year_str = str(year[0]) + '-' + str(year[-1])


map_name = {'lsm': 'ORS', 'cmip5': 'CMIP5', 'cmip6': 'CMIP6',
            '2cmip': 'CMIP5+6', 'all': 'ALL'}


for dcm, d in zip(depth_cm, depth):
    ###########################################################################
    # Load land cover & continent & SREX region information of the ISMN 
    # dataset.
    ###########################################################################
    ismn_info = pd.read_csv(os.path.join(mg.path_intrim_out(), 'ismn_summary',
                                         'ismn_landuse_continent_' + d + \
                                         '_' + iam + '_' + opt + '.csv'),
                            index_col = 0)

    ###########################################################################
    # Load the weighted products.
    ###########################################################################
    _, weighted_monthly_data, _ = get_weighted_monthly_data( \
        os.path.join(mg.path_intrim_out(), 'ismn_aggr'), iam, d, opt)

    pred_data = {}

    if (d == '0.00-0.10'):
        pred_data['ESA-CCI'] = pd.read_csv(os.path.join(mg.path_intrim_out(),
            'at_obs_lsm', iam + '_ESA-CCI_' + d + '.csv'),
            index_col = 0, parse_dates = True \
        ).loc[weighted_monthly_data.index, :]

        pred_data['GLEAM'] = pd.read_csv(os.path.join( \
            mg.path_intrim_out(), 'at_obs_lsm', iam + \
            '_GLEAMv3.3a_' + d + '.csv'),
            index_col = 0, parse_dates = True \
        ).loc[weighted_monthly_data.index, :]

    if (d == '0.00-0.10') | (d == '0.10-0.30'):
        pred_data['ERA-Land'] = pd.read_csv(os.path.join(mg.path_intrim_out(), 
            'at_obs_lsm', iam + '_ERA-Land_' + d + '.csv'),
            index_col = 0, parse_dates = True \
        ).loc[weighted_monthly_data.index, :]


    # Mean
    for src in ['lsm', 'cmip5', 'cmip6', '2cmip', 'all']:
        pred_data['Mean_' + map_name[src]] = \
            pd.read_csv(os.path.join(mg.path_out(), 'at_obs_meanmedian_' + src,
                                     'mean_' + d + '_' + year_str + \
                                     '_' + iam + '.csv'), index_col = 0, 
                        parse_dates = True).loc[weighted_monthly_data.index, :]

    # OLC
    for src in ['lsm', 'cmip5', 'cmip6', '2cmip', 'all']:
        if (src == 'lsm') | (src == 'all'):
            pred_data['OLC_' + map_name[src]] = \
                pd.read_csv(os.path.join(mg.path_out(),
                                         'at_obs_concat_dolce_' + src, 
                                         d + '_' + iam + \
                                         '_ShrunkCovariance.csv'),
                            index_col = 0, parse_dates = True \
                ).loc[weighted_monthly_data.index, :]
        else:
            pred_data['OLC_' + map_name[src]] = \
                pd.read_csv(os.path.join(mg.path_out(), 
                                         'at_obs_dolce_' + src, 
                                         'weighted_average_' + year_str + '_' \
                                         + d + '_' + iam + \
                                         '_ShrunkCovariance.csv'),
                            index_col = 0, parse_dates = True \
                ).loc[weighted_monthly_data.index, :]

    # EC
    for src in ['lsm', 'cmip5', 'cmip6', '2cmip', 'all']:
        if (src == 'lsm') | (src == 'all'):
            pred_data['EC_' + map_name[src]] = \
                pd.read_csv(os.path.join(mg.path_out(),
                                         'at_obs_concat_em_' + src, 
                                         'CRU_v4.03_year_month_anomaly_9grid_'\
                                         + dcm + '_' + iam + '.csv'),
                            index_col = 0, parse_dates = True \
                ).loc[weighted_monthly_data.index, :]
        else:
            pred_data['EC_' + map_name[src]] = \
                pd.read_csv(os.path.join(mg.path_out(), 
                                         'at_obs_em_' + src, 
                                         'CRU_v4.03_year_month_anomaly_9grid_'\
                                         + d + '_' + year_str + '_' + \
                                         iam + '.csv'),
                            index_col = 0, parse_dates = True \
                ).loc[weighted_monthly_data.index, :]


    ###########################################################################
    # Calculate the statistics by region.
    ###########################################################################
    for clas_abbr, clas in zip(['land_use_modis','continent'],
                               ['MODIS Land Use','Continent']):
        region = ismn_info.loc[~np.isnan(ismn_info[clas]), clas].unique()
        
        metrics_by_region = pd.DataFrame(data = np.nan, 
            index = ['%d' % c for c in region], 
            columns = pd.MultiIndex.from_product([['ESA-CCI', 'GLEAM',
                                                   'ERA-Land'] + \
                                                  [a+'_'+b for a in \
                                                   ['Mean','OLC','EC'] \
                                                   for b in \
                                                   ['ORS','CMIP5','CMIP6',
                                                    'CMIP5+6','ALL']],
                                                  ['Bias','uRMSE','RMSE',
                                                   'Corr']]))

        for c in region:
            c_station = ismn_info.index[ np.abs(ismn_info[clas] - c) < 1e-6 ]

            obs0 = weighted_monthly_data.loc[:, c_station].values.reshape(-1)

            for m_ind, m in enumerate(pred_data.keys()):
                sim = pred_data[m].loc[:, c_station].values.reshape(-1)
                #print(sim)

                retain = ~(np.isnan(obs0) | np.isnan(sim))
                obs = obs0[retain]
                sim = sim[retain]

                #if (d == '0.00-0.10') & (int(c) == 2) &
                # (clas == 'MODIS Land Use'):
                #    print(m)
                #    dummy()

                metrics_by_region.loc[('%d' % c), (m, 'RMSE')], \
                    metrics_by_region.loc[('%d' % c), (m, 'Bias')], \
                    metrics_by_region.loc[('%d' % c), (m, 'uRMSE')], \
                    metrics_by_region.loc[('%d' % c), (m, 'Corr')] = \
                    calc_stats(sim, obs)

        metrics_by_region.to_csv(os.path.join(mg.path_out(),
                                              'standard_metrics',
                                              'by_' + clas_abbr + '_' + d + \
                                              '_' + str(year[0]) + '-' + \
                                              str(year[-1]) + '_' + \
                                              iam + '_' + opt + '.csv'))
