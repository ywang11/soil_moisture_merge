# -*- coding: utf-8 -*-
"""
2020/08/04
ywang254@utk.edu

Plot the relationship between grid-scale precipitation & temperature and
 modeled soil moisture at different depths, for each month in each year,
 using selected individual grid boxes. Insignificant relationships, which
 do not contribute to the slope of the emergent constriant, are not plotted.
"""
import utils_management as mg
from utils_management.constants import met_to_lsm, year_longest, \
    year_shorter, year_shorter2, year_shortest, lsm_list, depth, depth_cm, \
    target_lat, target_lon
from misc.spatial_utils import extract_grid
import os
import xarray as xr
import sys

if sys.platform == 'linux':
    os.environ['QT_QPA_PLATFORM']='offscreen'

import matplotlib.pyplot as plt
plt.switch_backend('agg') # solve the display problem

import matplotlib as mpl
from matplotlib import gridspec
import pandas as pd
import numpy as np
import itertools as it
from scipy.stats import linregress
import multiprocessing as mp


npts = 200 # Number of random spatial points to choose to plot the regression
           # relationship.

# [year_longest, year_shorter, year_shorter2, year_shortest]
year = REPLACE1
year_str = str(year[0]) + '-' + str(year[-1])
period = pd.date_range(str(year[0])+'-01-01', str(year[-1])+'-12-31',
                       freq = 'MS')

# [0,1,2,3]
i = REPLACE2
d = depth[i]
dcm = depth_cm[i]

model_list = lsm_list[(str(year[0])+'-'+str(year[-1]), d)]

opt = 'year_month_anomaly_9grid'
n_gr = int(opt.split('_')[-1][0])
n_sq_gr = int(np.sqrt(n_gr)) # [1, 3, ...]

land_mask = 'vanilla'
path_to_pr = mg.path_to_pr(land_mask)
met_obs_name = 'CRU_v4.03'


###############################################################################
# Prefix of the precipitation and temperature data that are to be used in
# the regression.
###############################################################################
temp1 = mg.path_to_pr(land_mask)
temp2 = mg.path_to_tas(land_mask)
pr_list = []
tas_list = []
met_name_list = []
for l in model_list:
    for met, value in met_to_lsm.items():
        if l in value:
            pr_list.append(temp1[met])
            tas_list.append(temp2[met])
            met_name_list.append(met)
pr_list_uniq, temp_ind = np.unique(pr_list, return_index = True)
pr_list_uniq = list(pr_list_uniq)
tas_list_uniq = [tas_list[i] for i in temp_ind]
met_name_list_uniq = [met_name_list[i] for i in temp_ind]


mpl.rcParams['font.size'] = 5
mpl.rcParams['axes.titlesize'] = 5
marker = {'CERA20C': '>', 'CRU_v3.26': 'o', 'CRU_v3.20': 's', 'CRU_v4.03': 'D',
          'ERA-Interim': 'v', 'ERA20C': '^', 'ERA5': 'x',
          'GLDAS_Noah2.0': '<'}
mlist = [marker[met] for met in met_name_list_uniq]
cmap = mpl.cm.get_cmap('Spectral')
clist = [cmap((i+0.5)/len(met_name_list_uniq)) \
         for i in range(len(met_name_list_uniq))]


#for yr in year:
def calc(yr):
    np.random.seed(999 + yr)

    ###########################################################################
    # Read the soil moisture data for regression.
    ###########################################################################
    for m_ind, m in enumerate(model_list):
        fname = os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                             land_mask, m, m + '_' + str(yr) + '_' + \
                             dcm + '.nc')

        data = xr.open_dataset(fname, decode_times = False)
        # Guard against non-global data
        sm = np.full([12, len(target_lat), len(target_lon)], np.nan)
        if data.lat.values[0] > -87:
            temp = target_lat >= data.lat.values[0]
            sm[:, temp, :] = data.sm.values.copy()
        else:
            sm[:, :, :] = data.sm.values.copy()
        data.close()

        # Remove the climatology if specified.
        if 'anomaly' in opt:
            data = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                                'em_tas_corr_climatology',
                                                'sm_lsm_' + m + '_' + \
                                                dcm + '.nc'))
            sm -= data['sm'].values
            data.close()

        # Randomly sample the points with soil moisture data.
        if m_ind == 0:
            lat_ind, lon_ind = np.where(~np.isnan(data.sm.values[0, :, :]))
            which = np.random.choice(range(len(lat_ind)), npts)
            sm_data = np.full([len(model_list), 12, npts, n_gr], np.nan)

        for wind, ww in enumerate(which):
            gcount = 0
            for mm, nn in it.product(range(n_sq_gr), range(n_sq_gr)):
                sm_data[m_ind, :, wind, gcount] = \
                    sm[:, lat_ind[ww]-1+mm, lon_ind[ww]-1+nn]
                gcount += 1

    # Average over the different LSMs that have common forcing.
    sm_data2 = np.full([len(pr_list_uniq), 12, npts, n_gr], np.nan)
    for p_ind, p in enumerate(pr_list_uniq):
        p_ind_list = [i for i,x in enumerate(pr_list) if x == p]
        sm_data2[p_ind, :, :, :] = np.mean(sm_data[p_ind_list, :, :, :],
                                           axis = 0)
    # ---- Now replace the sm_data with the averaged.
    sm_data = sm_data2
    del sm_data2

    ###########################################################################
    # Read the precipitation data for regression.
    ###########################################################################
    pr_data = np.full([len(pr_list_uniq), 12, npts, n_gr], np.nan)
    for p_ind, p in enumerate(pr_list_uniq):
        fname = p + str(yr) + '.nc'

        data = xr.open_dataset(fname, decode_times = False)
        # Guard against non-global data
        pr = np.full([12, len(target_lat), len(target_lon)], np.nan)
        if data.lat.values[0] > -87:
            temp = target_lat >= data.lat.values[0]
            pr[:, temp, :] = data.pr.values.copy()
        else:
            pr[:, :, :] = data.pr.values.copy()
        data.close()

        # Remove the climatology if specified.
        if 'anomaly' in opt:
            data = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                                'em_tas_corr_climatology',
                                                'pr_lsm_' + \
                                                met_name_list_uniq[p_ind] + \
                                                '.nc'))
            pr -= data['pr'].values
            data.close()

        for wind, ww in enumerate(which):
            gcount = 0
            for mm, nn in it.product(range(n_sq_gr), range(n_sq_gr)):
                pr_data[p_ind, :, wind, gcount] = \
                    pr[:, lat_ind[ww]-1+mm, lon_ind[ww]-1+nn]
                gcount += 1

    ###########################################################################
    # Read the temperature data for regression.
    ###########################################################################
    tas_data = np.full([len(tas_list_uniq), 12, npts, n_gr], np.nan)
    for p_ind, p in enumerate(tas_list_uniq):
        fname = p + str(yr) + '.nc'

        data = xr.open_dataset(fname, decode_times = False)
        # Guard against non-global data
        tas = np.full([12, len(target_lat), len(target_lon)], np.nan)
        if data.lat.values[0] > -87:
            temp = target_lat >= data.lat.values[0]
            tas[:, temp, :] = data.tas.values.copy()
        else:
            tas[:, :, :] = data.tas.values.copy()
        data.close()

        # Remove the climatology if specified.
        if 'anomaly' in opt:
            data = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                                'em_tas_corr_climatology',
                                                'tas_lsm_' + \
                                                met_name_list_uniq[p_ind] + \
                                                '.nc'))
            tas -= data['tas'].values
            data.close()

        for wind, ww in enumerate(which):
            gcount = 0
            for mm, nn in it.product(range(n_sq_gr), range(n_sq_gr)):
                tas_data[p_ind, :, wind, gcount] = \
                    tas[:, lat_ind[ww]-1+mm, lon_ind[ww]-1+nn]
                gcount += 1

    ###########################################################################
    # Make the graphs: 40 precipitation / temperature per graph per year
    #                  & month.
    ###########################################################################
    coef = pd.DataFrame(data = np.nan,
                        index = range(npts),
                        columns = pd.MultiIndex.from_product([['pr', 'tas'],
                                                              range(12)]))
    for var, mon, wind in it.product(['pr', 'tas'], range(12), range(5)):
        fig, axes = plt.subplots(8, 5, figsize = (13, 13))
        fig.subplots_adjust(hspace = 0.4, wspace = 0.3)
        for wind2 in range(40):
            ww = wind*40 + wind2

            ax = axes.flat[wind2]
            h = [None] * len(met_name_list_uniq)
            for hind in range(len(met_name_list_uniq)):
                if var == 'pr':
                    x = pr_data[hind, mon, ww, :]
                else:
                    x = tas_data[hind, mon, ww, :]
                y = sm_data[hind, mon, ww, :]
                h[hind], = ax.plot(x, y, lw = 0, marker = mlist[hind],
                                   color = clist[hind], ms = 5)
            x = pr_data[:, mon, ww, :].reshape(-1)
            y = sm_data[:, mon, ww, :].reshape(-1)
            slope, intercept, rval, pval, stderr = linregress(x, y)
            if pval <= 0.05:
                coef.loc[ww, (var, mon)] = slope
            if wind2 >= 35:
                ax.set_xlabel(var)
            if np.mod(wind2, 5) == 0:
                ax.set_ylabel('sm')
            ax.set_title(('%.2f' % target_lat[lat_ind[which[ww]]]) + \
                         ', ' + \
                         ('%.2f' % target_lon[lon_ind[which[ww]]]))
            ax.ticklabel_format(axis = 'y', scilimits = (-2, 2))
        ax.legend(h, met_name_list_uniq, ncol = len(met_name_list_uniq),
                  loc = (-4, -0.6))
        fig.savefig(os.path.join(mg.path_intrim_out(),
                                 'em_tas_corr_test_plot', year_str,
                                 dcm, var + '_' + ('%02d' % (mon+1)) + \
                                 '_' + str(yr) + '-' + str(wind) + \
                                 '.png'), dpi = 600.,
                    bbox_inches = 'tight')
        plt.close(fig)
    coef.to_csv(os.path.join(mg.path_intrim_out(),
                             'em_tas_corr_test_plot', year_str + '_' + dcm + \
                             '_coef.csv'))

p = mp.Pool(6) # memory issue
p.map_async(calc, list(year))
p.close()
p.join()

#calc(year[0])
