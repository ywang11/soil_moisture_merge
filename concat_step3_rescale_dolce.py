"""
20190923
ywang254@utk.edu

Step 3 of the CDF-matching merging:
 Apply the CDF matching on the anomalies.
"""
import xarray as xr
import os
import pandas as pd
import numpy as np
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth, target_lat, target_lon
import utils_management as mg
from misc.ismn_utils import get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
from misc.concat_utils import piecewise_cdf_create, piecewise_cdf_apply
import itertools as it
import multiprocessing as mp


simple = [True, False, False]
lu_weighted = [False, False, True]
lu_threshold = 40
ismn_aggr_method = [get_ismn_aggr_method(simple[i], lu_weighted[i],
                                         lu_threshold) for i in range(3)]
cov_method = [get_cov_method(i) for i in range(5)]
prod_list = ['all', 'lsm']


prod = prod_list[REPLACE1]
d = depth[REPLACE2]
iam = ismn_aggr_method[REPLACE3]
cov = cov_method[REPLACE4]


#def rescale(prod, d, iam, cov):
time_range = pd.date_range('1981-01-01', '2010-12-31', freq = 'MS')

data_ref = xr.open_dataset(os.path.join(mg.path_out(), 
                                        'concat_dolce_' + prod,
                                        'anomaly_' + \
                                        str(year_shorter2[0]) + '-' + \
                                        str(year_shorter2[-1]) + '_' + \
                                        d + '_' + iam + '_' + cov + '.nc'),
                           decode_times = False)
data_ref['time'] = pd.date_range(str(year_shorter2[0]) + '-01-01',
                                 str(year_shorter2[-1]) + '-12-31',
                                 freq = 'MS')

for year in [year_shorter, year_shortest]:
    data = xr.open_dataset(os.path.join(mg.path_out(),
                                        'concat_dolce_' + prod,
                                        'anomaly_' + str(year[0]) + '-' + \
                                        str(year[-1]) + '_' + d + '_' + \
                                        iam + '_' + cov + '.nc'),
                           decode_times = False)
    data['time'] = pd.date_range(str(year[0]) + '-01-01',
                                 str(year[-1]) + '-12-31', freq = 'MS')

    anomalies = data.sm.values.copy()

    for a,b in it.product(range(len(data.lat)), 
                          range(len(data.lon))):
        if np.isnan(anomalies[0,a,b]):
            continue
        ##else:
        ##    #DEBUG
        ##    break

        # Inter-calibration functions.
        ref = data_ref.sm[(data_ref.time.indexes['time'] >= \
                           time_range[0]) & \
                          (data_ref.time.indexes['time'] <= \
                           time_range[-1]), a, b].values
        x = data.sm[(data.time.indexes['time'] >= time_range[0]) & \
                    (data.time.indexes['time'] <= time_range[-1]),
                    a, b].values

        slope, threshold, intercept = piecewise_cdf_create(x, ref)
        anomalies[:,a,b] = piecewise_cdf_apply(slope, threshold, intercept,
                                               anomalies[:,a,b])

    anomalies = xr.DataArray(anomalies, dims = ['time','lat','lon'],
                             coords = {'time': data.time,
                                       'lat': data.lat,  
                                       'lon': data.lon})

    anomalies.to_dataset(name = 'sm' \
    ).to_netcdf(os.path.join(mg.path_out(), 'concat_dolce_' + prod,
                             'scaled_' + str(year[0]) + '-' + \
                             str(year[-1]) + '_' + d + '_' + iam + \
                             '_' + cov + '.nc'))
    data.close()


#pool = mp.Pool(2)
#[pool.apply_async(rescale, args = (prod, d, iam, cov)) \
# for prod, d, iam, cov in it.product([prod_list[1]], depth, ismn_aggr_method,
#                                     cov_method)]
#pool.close()
#pool.join()

#for prod, d, iam, cov in it.product(prod_list, depth, ismn_aggr_method, 
#                                    cov_method):
#    rescale(prod, d, iam, cov)
