import utils_management as mg
from utils_management.constants import depth, depth_cm, year_longest
import os
import xarray as xr
from misc.standard_diagnostics2 import get_prod_list, get_sm
from datetime import datetime


def get_flist2(name, d, dcm):
    if name == 'mean_lsm':
        fname = os.path.join(mg.path_out(), 'meanmedian_lsm',
                             'mean_uncertainty_' + d + '_1950-2016.nc')
    elif name == 'dolce_lsm':
        fname = os.path.join(mg.path_out(), 'concat_dolce_lsm',
                             'concat_uncertainty_' + d + \
                             '_lu_weighted_ShrunkCovariance.nc')
    elif (name == 'em_lsm') | (name == 'em_all'):
        fname = os.path.join(mg.path_out(), 'concat_' + name,
                             'concat_uncertainty_CRU_v4.03_year_month_' + \
                             'anomaly_9grid_'+ dcm + '_1950-2016.nc')
    else:
        fname = [os.path.join(mg.path_out(), name + '_corr',
                              'CRU_v4.03_year_month_' + \
                              'anomaly_9grid_' + dcm + '_1950-2016',
                              'predicted_std_' + str(yy) + '.nc') \
                 for yy in range(1950, 2017)]
    return fname



prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all']
prod_list_name = ['Mean ORS', 'OLC ORS', 'EC ORS', 'EC CMIP5', 'EC CMIP6',
                  'EC CMIP5+6', 'EC ALL']
for pind, prod in enumerate(prod_list):
    for dind, dcm in enumerate(depth_cm):
        d = depth[dind]

        flist = get_prod_list(prod, d, dcm)
        sm = get_sm(flist, year_longest)

        fname = get_flist2(prod, d, dcm)
        if type(fname) == str:
            hr = xr.open_dataset(fname, decode_times = False)
        else:
            hr = xr.open_mfdataset(fname, decode_times = False,
                                   concat_dim = 'time')
        if 'em' in prod:
            uncertainty = hr['predicted_std'].copy(deep = True)
        else:
            uncertainty = hr['sm_uncertainty'].copy(deep = True)
        hr.close()
        uncertainty['time'] = sm['time']
        

        xr.Dataset({'sm': (('time','lat','lon'), sm,
                           {'long_name': 'soil moisture estimate',
                            'units': 'm^3/m^3'}),
                    'std': (('time','lat','lon'), uncertainty,
                            {'long_name': 'uncertainty estimate',
                             'unit': 'm^3/m^3',
                             'note': 'in the form of standard deviation'})},
                   coords = sm.coords,
                   attrs = {'contact': 'ywang254@utk.edu; maoj@ornl.gov',
                            'created on': datetime.now( \
                            ).strftime('%Y-%m-%d %H:%M:%S'),
                            'note': prod_list_name[pind]} \
        ).to_netcdf(os.path.join(mg.path_out(), 'figshare',
                                 prod_list_name[pind].lower( \
                                 ).replace(' ','_') + '_' + dcm + '.nc'),
                    format = 'NETCDF4',
                    encoding = {'sm': {'_FillValue': -1e35},
                                'std': {'_FillValue': -1e35},
                                'time': {'_FillValue': None},
                                'lat': {'_FillValue': None},
                                'lon': {'_FillValue': None}})
