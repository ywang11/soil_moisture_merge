"""
Compute the following for every CMIP6 model:

1. Global map (averaged over time): NetCDF
2. Global average time series.
3. Latitude bands average time series.
4. Continental average time series.
5. SREX region average time series.

Because the CMIP5 models ends 2005, do two sets of calculations: 
1950-2005, and 1950-2014
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import year_cmip5, year_cmip6, cmip6_list
from misc.standard_diagnostics import standard_diagnostics
import os
import pandas as pd


year = year_cmip6 # [year_cmip5, year_cmip6]

act = 'CMIP'
expr = 'historical'
var = 'mrsos'
tid = 'Lmon'

if (var == 'mrsos') | (var == 'mrsol') | (var == 'mrlsl'):
    nc_var = 'sm'
else:
    nc_var = var

model_version = cmip6_list(act, expr, var, tid)

land_mask = 'SYNMAP'

# A subset of 3-4 models
model_version = model_version[(REPLACE*3):(REPLACE*3+3)]


for m_v in model_version:
    flist = [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                          land_mask, 'CMIP6', m_v,
                          var + '_' + expr + '_' + str(y) + '.nc') \
             for y in year]

    data = xr.open_mfdataset(flist, decode_times=False)

    time = pd.date_range(str(year[0])+'-01-01', str(year[-1])+'-12-31',
                         freq = 'MS')

    standard_diagnostics(data[nc_var].values, time, data.lat, data.lon, 
                         os.path.join(mg.path_out(), 
                                      'standard_diagnostics_cmip6'),
                         var + '_' + expr + '_' + m_v + '_' + str(year[0]) + \
                         '-' + str(year[-1]))
    data.close()
