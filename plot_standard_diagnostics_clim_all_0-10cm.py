"""
2020/05/20

ywang254@utk.edu

Plot the global mean soil moisture of the selected products.
1981-2010 climatology.
"""
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from misc.plot_utils import stipple
import cartopy.crs as ccrs
import utils_management as mg
from utils_management.constants import depth_cm, depth, target_lat, \
    target_lon, year_longest
import os
import xarray as xr
from misc.plot_utils import cmap_gen
import statsmodels.api as sm
import numpy as np
import itertools as it
from scipy.stats import pearsonr


year = year_longest
iam = 'lu_weighted'
cov = 'ShrunkCovariance'
met = 'CRU_v4.03'
me = 'year_month_anomaly_9grid'


prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all']
prod_names = ['Mean ORS', 'OLC ORS', 'EC ORS', 'EC CMIP5', 'EC CMIP6',
              'EC CMIP5+6', 'EC ALL']


levels = np.linspace(0., 0.6, 21)
cmap = 'RdYlBu'
levels_diff = np.linspace(-0.3, 0.3, 21)
cmap_diff = cmap_gen('autumn', 'winter_r')
mpl.rcParams['font.size'] = 5
mpl.rcParams['axes.titlesize'] = 5
mpl.rcParams['hatch.linewidth'] = 0.5


which = 'annual'
i = 0
d = depth[i]
dcm = depth_cm[i]


fig, axes = plt.subplots(nrows = len(prod_list), ncols = 4, figsize = (6.5,6),
                         subplot_kw = {'projection': ccrs.Miller()})
fig.subplots_adjust(hspace = 0., wspace = 0.)
for pind, prod in enumerate(prod_list):
    ###########################################################################
    # (1) 1981-2010 climatology
    ###########################################################################
    ax = axes[pind, 0]
    ax.coastlines(lw = 0.5, color = 'grey')
    ax.set_extent([-180, 180, -60, 80])
    ax.outline_patch.set_visible(False)
    ax.spines['top'].set_visible(True)
    ax.spines['bottom'].set_visible(True)
    ax.spines['left'].set_visible(True)
    ax.spines['right'].set_visible(True)
    ax.spines['top'].set_lw(0.1)
    ax.spines['bottom'].set_lw(0.1)
    ax.spines['left'].set_lw(0.1)
    ax.spines['right'].set_lw(0.1)

    if (prod == 'em_lsm') | (prod == 'em_all'):
        fname = os.path.join(mg.path_out(), 'standard_diagnostics_concat',
                             prod, met + '_' + me + '_predicted_' + d + \
                             '_g_map_' + which + '.nc')
    elif 'em' in prod:
        fname = os.path.join(mg.path_out(), 'standard_diagnostics_' + prod,
                             met + '_predicted_' + me + '_1950-2016_' + d + \
                             '_g_map_' + which + '.nc')
    elif prod == 'dolce_lsm':
        fname = os.path.join(mg.path_out(), 'standard_diagnostics_concat',
                             prod, 'dolce_average_' + iam + '_' + cov + \
                             '_' + d + '_g_map_' + which + '.nc')
    elif prod == 'mean_lsm':
        fname = os.path.join(mg.path_out(),
                             'standard_diagnostics_meanmedian_lsm',
                             'mean_1950-2016_' + d + '_g_map_' + which + '.nc')
    hr = xr.open_dataset(fname)
    sm = hr['g_map'].values.copy()
    hr.close()

    cf1 = ax.contourf(hr.lon.values, hr.lat.values, sm, cmap = cmap,
                      levels = levels, extend = 'both',
                      transform = ccrs.PlateCarree())
    if pind == 0:
        ax.set_title('Climatology')
    ax.text(-0.1, 0.5, prod_names[pind], verticalalignment='center',
            rotation = 90, transform = ax.transAxes)

    ###########################################################################
    # (2) Size of 95% CI, simply averaged, unshrunk by the number of timesteps
    ###########################################################################
    ax = axes[pind, 1]
    ax.coastlines(lw = 0.5, color = 'grey')
    ax.set_extent([-180, 180, -60, 80])
    ax.outline_patch.set_visible(False)
    ax.spines['top'].set_visible(True)
    ax.spines['bottom'].set_visible(True)
    ax.spines['left'].set_visible(True)
    ax.spines['right'].set_visible(True)
    ax.spines['top'].set_lw(0.1)
    ax.spines['bottom'].set_lw(0.1)
    ax.spines['left'].set_lw(0.1)
    ax.spines['right'].set_lw(0.1)

    if (prod == 'em_lsm') | (prod == 'em_all'):
        f1 = os.path.join(mg.path_out(), 'standard_diagnostics_concat', \
                          prod, met + '_' + me + '_predicted_CI_lower_' + \
                          d + '_g_map_' + which + '.nc')
        f2 = os.path.join(mg.path_out(), 'standard_diagnostics_concat', \
                          prod, met + '_' + me + '_predicted_CI_upper_' + \
                          d + '_g_map_' + which + '.nc')
    elif 'em' in prod:
        f1 = os.path.join(mg.path_out(), 'standard_diagnostics_' + prod, \
                          met + '_predicted_CI_lower_' + me + '_1950-2016_' + \
                          d + '_g_map_' + which + '.nc')
        f2 = os.path.join(mg.path_out(), 'standard_diagnostics_' + prod, \
                          met + '_predicted_CI_upper_' + me + '_1950-2016_' + \
                          d + '_g_map_' + which + '.nc')
    elif prod == 'dolce_lsm':
        fname = os.path.join(mg.path_out(), 'standard_diagnostics_concat',
                             prod, 'dolce_uncertainty_' + iam + '_' + cov + \
                             '_' + d + '_g_map_' + which + '.nc')
    else:
        # Problematic! Need to generate STD files!!!
        fname = os.path.join(mg.path_out(),
                             'standard_diagnostics_meanmedian_lsm',
                             'std_1950-2016_' + d + '_g_map_' + which + '.nc')

    if 'em' in prod:
        hr1 = xr.open_dataset(f1)
        hr2 = xr.open_dataset(f2)
        sm_lower = hr1['g_map'].values.copy()
        sm_upper = hr2['g_map'].values.copy()
        hr2.close()
        hr1.close()
    else:
        hr = xr.open_dataset(fname)
        sm_lower = -1.96 * hr['g_map'].values.copy() + sm
        sm_upper = 1.96 * hr['g_map'].values.copy() + sm
        hr.close()

    cf2 = ax.contourf(hr.lon.values, hr.lat.values, sm_upper - sm_lower,
                      levels = levels, cmap = cmap, extend = 'both',
                      transform = ccrs.PlateCarree())
    if pind == 0:
        ax.set_title('95% CI')

    ###########################################################################
    # (3)/(4) Difference from GLEAM/ERA-Land with hatch for the difference
    #         being within 95% CI
    ###########################################################################
    for rind, ref in zip(range(2), ['GLEAMv3.3a', 'ERA-Land']):
        ax = axes[pind, 2 + rind]
        ax.coastlines(lw = 0.5, color = 'grey')
        ax.set_extent([-180, 180, -60, 80])
        ax.outline_patch.set_visible(False)
        ax.spines['top'].set_visible(True)
        ax.spines['bottom'].set_visible(True)
        ax.spines['left'].set_visible(True)
        ax.spines['right'].set_visible(True)
        ax.spines['top'].set_lw(0.1)
        ax.spines['bottom'].set_lw(0.1)
        ax.spines['left'].set_lw(0.1)
        ax.spines['right'].set_lw(0.1)

        fname = os.path.join(mg.path_out(), 'standard_diagnostics_lsm',
                             ref + '_' + d + '_g_map_' + which + '.nc')
        hr = xr.open_dataset(fname)
        obs = hr['g_map'].values.copy()
        hr.close()

        cf3 = ax.contourf(hr.lon.values, hr.lat.values, sm - obs,
                          levels = levels_diff, cmap = cmap_diff,
                          extend = 'both', transform = ccrs.PlateCarree())

        # ---- Pearson correlation between sm and obs
        x = obs.reshape(-1)
        y = sm.reshape(-1)
        temp = ~(np.isnan(x) | np.isnan(y))
        r, pval = pearsonr(x[temp], y[temp])
        ax.text(0.01, 0.03, r'$\rho$=' + ('%.3f' % r),
                transform = ax.transAxes)

        # ---- stipple if values are below thresh; need to inverse
        stipple(ax, hr.lat.values, hr.lon.values,
                ~(np.abs(obs - sm) < (sm_upper - sm_lower)), 0.05,
                transform = ccrs.PlateCarree())
        if pind == 0:
            ax.set_title('Product - ' + ref)

cax = fig.add_axes([0.14, 0.08, 0.36, 0.01])
plt.colorbar(cf1, cax = cax, boundaries = levels, orientation = 'horizontal',
             ticks = levels[::2])
cax = fig.add_axes([0.52, 0.08, 0.36, 0.01])
plt.colorbar(cf3, cax = cax, boundaries = levels_diff,
             orientation = 'horizontal', ticks = levels_diff[::2])

fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                         'clim_all_0-10cm.png'), dpi = 600,
            bbox_inches = 'tight')
plt.close()
