"""
2019/08/07
ywang254@utk.edu

Plot the performances of Mean, Median, best method of DOLCE, and best method 
of EM-LSM by
 - MODIS land cover type
 - continent
using the validation-period ISMN observations.
"""
from utils_management.constants import year_longest, depth, depth_cm, \
    lu_names, continent_legend
import utils_management as mg
from misc.ismn_utils import get_weighted_monthly_data
from misc.analyze_utils import calc_stats
import os
import pandas as pd
import matplotlib.pyplot as plt
import itertools as it
import numpy as np
import matplotlib as mpl


# Aggregation method to produce the grid-scale ISMN observations.
iam = 'lu_weighted'

# Subset of observations to compare on. Different periods produce similar
# metrics ("plot_standard_metrics_all.py") and have similar spatial
# availability ("ismn_summary.py").
opt = 'val'


mpl.rcParams['font.size'] = 8

stats = ['RMSE', 'Corr']

methods = ['Mean','OLC','EC']

sourcedata = ['ORS','CMIP5','CMIP6','CMIP5+6','ALL']


cn2 = [methods[i] + '_' + sourcedata[j] for i in range(len(methods)) \
       for j in range(len(sourcedata))]
cmap_list = [mpl.cm.get_cmap('Blues_r'), mpl.cm.get_cmap('Greens_r'),
             mpl.cm.get_cmap('Reds_r')]
col_list = dict([(methods[i] + '_' + sourcedata[j],
                  cmap_list[i](j/len(sourcedata))) \
                 for i in range(len(methods)) for j in range(len(sourcedata))])


for clas, stat in it.product(['continent', 'land_use_modis'], stats):

    fig, axes = plt.subplots(nrows = 4, ncols = 1, sharex = True,
                             sharey = False, figsize = (8, 8))
    fig.subplots_adjust(hspace = 0.)
    for d_ind in range(len(depth)):
        ax = axes[d_ind]

        d = depth[d_ind]
        dcm = depth_cm[np.where([x == d for x in depth])[0][0]]

        col_names = list(col_list.keys())

        metrics_by_region = pd.read_csv(os.path.join(mg.path_out(), 
                                                     'standard_metrics',
                                                     'by_' + clas + '_' +\
                                                     d + '_1950-2016_' + \
                                                     iam + '_' + \
                                                     opt + '.csv'),
                                        index_col = 0, header = [0,1])
        metrics_by_region.sort_index(inplace=True)

        if clas == 'land_use_modis':
            # Remove water bodies & Permanent wetlands
            if d_ind != 0:
                metrics_by_region.drop([0], axis = 0, inplace = True)
            else:
                metrics_by_region.drop([0, 11], axis = 0, inplace = True)

        if (clas == 'continent') and (999 in metrics_by_region.index):
            metrics_by_region = metrics_by_region.drop(999, axis = 0)

        b = [None] * len(metrics_by_region.columns.levels[0])
        for m_ind, method in enumerate(col_names):
            position = [x + m_ind / (2 + len(col_names)) \
                        for x in range(metrics_by_region.shape[0])]
            y = metrics_by_region.loc[:, (method, stat)].values
            ax.bar(position, y, color = col_list[method], label = method,
                   width = 0.9/(2+len(col_names)))

        if d_ind == 0:
            for x_ind, x in enumerate(metrics_by_region.index):
                h1 = ax.hlines(metrics_by_region.loc[x, 
                                                     ('ESA-CCI', stat)],
                               x_ind, x_ind + 1 - 1/len(sourcedata), 
                               lw = 1.5, ls = '-', color = '#252525')
                h2 = ax.hlines(metrics_by_region.loc[x, 
                                                     ('GLEAM', stat)],
                               x_ind, x_ind + 1 - 1/len(sourcedata),
                               lw = 1.5, ls = '--', color = '#252525')
                h3 = ax.hlines(metrics_by_region.loc[x, ('ERA-Land', stat)],
                               x_ind, x_ind + 1 - 1/len(sourcedata),
                               lw = 1.5, ls = ':', color = '#252525')
            if clas == 'land_use_modis':
                ax.legend([h1, h2, h3], ['ESA-CCI', 'GLEAM', 'ERA-Land'],
                          ncol = 3, loc = [0, -3.8], frameon = False)
            else:
                ax.legend([h1, h2, h3], ['ESA-CCI', 'GLEAM', 'ERA-Land'],
                          ncol = 3, loc = [0, -3.35], frameon = False)
        if d_ind == 1:
            for x_ind, x in enumerate(metrics_by_region.index):
                h3 = ax.hlines(metrics_by_region.loc[x, ('ERA-Land', stat)],
                               x_ind, x_ind + 1 - 1/len(sourcedata), 
                               lw = 1.5, ls = ':', color = '#252525')

        if d_ind == 0:
            ax.set_title(stat)

            xticks = np.arange(.3, metrics_by_region.shape[0]+.5)
            if clas == 'land_use_modis':
                xticklabels = [lu_names[int(x)].replace(' ','\n') \
                               for x in metrics_by_region.index]
            else:
                xticklabels = [continent_legend[int(x)]
                               for x in metrics_by_region.index]
            xlim = [-1/len(metrics_by_region.columns.levels[0]),
                    metrics_by_region.shape[0]]

        ax.set_xticks(xticks)
        ax.set_xlim(xlim)
        if clas == 'land_use_modis':
            ax.set_xticklabels(xticklabels, rotation = 90)
            if d_ind == (len(depth)-1):
                ax.legend(ncol = 5, loc = (0., -1.1), frameon = False)
        else:
            ax.set_xticklabels(xticklabels, rotation = 0)
            if d_ind == (len(depth)-1):
                ax.legend(ncol = 5, loc = (0., -0.7), frameon = False)
        if (stat == 'RMSE'):
            ax.set_ylabel(dcm)
            ax.set_ylim([0., 0.23])
            ax.set_yticks(np.arange(0.02, 0.24, 0.04))
        else:
            ax.set_ylim([-1, 1])

    fig.savefig(os.path.join(mg.path_out(), 'standard_metrics', 
                             'by_' + clas + '_' + \
                             iam + '_' + opt + '_' + stat + '.png'), 
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
                                                     
