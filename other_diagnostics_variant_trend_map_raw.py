"""
20200109

Calculate the 1950-2016 trend of selected LSMs.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import lsm_list, depth, depth_cm, year_cmip5,\
    year_cmip6, year_longest
from misc.analyze_utils import calc_seasonal_trend
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability
import os
from glob import glob
import itertools as it
import pandas as pd
import numpy as np
import multiprocessing as mp


land_mask = 'vanilla'
year = year_longest
year_str = str(year[0]) + '-' + str(year[-1])
period = pd.date_range(str(year[0])+'-01-01', str(year[-1])+'-12-31',
                       freq = 'MS')


def calc(option):
    model_set, model, d, dcm = option

    if model_set == 'lsm':
        flist = [os.path.join(mg.path_intrim_out(), 'Interp_Merge', land_mask,
                              model, model + '_' + str(y) + '_' + \
                              dcm + '.nc') for y in year]
    elif model_set == 'cmip5':
        file1 = [os.path.join(mg.path_intrim_out(), 'Interp_Merge', land_mask,
                              'CMIP5', model, 'sm_historical_r1i1p1_' + \
                              str(y) + '_' + dcm + '.nc') for y in year_cmip5]
        file2 = [os.path.join(mg.path_intrim_out(), 'Interp_Merge', land_mask,
                              'CMIP5', model, 'sm_rcp85_r1i1p1_' + \
                              str(y) + '_' + dcm + '.nc') for y in \
                 list(set(year) - set(year_cmip5))]
        flist = file1 + file2
    else:
        file1 = [os.path.join(mg.path_intrim_out(), 'Interp_Merge', land_mask,
                              'CMIP6', model, 'mrsol_historical_' + \
                              str(y) + '_' + dcm + '.nc') for y in year_cmip6]
        file2 = [os.path.join(mg.path_intrim_out(), 'Interp_Merge', land_mask,
                              'CMIP6', model, 'mrsol_ssp585_' + \
                              str(y) + '_' + dcm + '.nc') \
                 for y in list(set(year) - set(year_cmip6))]
        flist = file1 + file2

    data = xr.open_mfdataset(flist, decode_times=False)
    sm = data.sm.load()
    sm['time'] = period

    for s in ['Annual', 'DJF', 'MAM', 'JJA', 'SON']:
        g_map_trend, g_p_values, g_intercepts = calc_seasonal_trend(sm, s)

        xr.Dataset({'g_map_trend': (['lat','lon'], g_map_trend),
                    'g_p_values': (['lat','lon'], g_p_values),
                    'g_intercepts': (['lat','lon'], g_intercepts)},
                   coords = {'lon':data.lon, 'lat':data.lat} \
               ).to_netcdf(os.path.join(mg.path_out(),
                                        'other_diagnostics', model_set,
                                        model + '_' + d + \
                                        '_g_map_trend_' + s + '.nc'))
    data.close()


for i, model_set in it.product(range(4), ['lsm', 'cmip5', 'cmip6']):
    d = depth[i]
    dcm = depth_cm[i]

    if model_set == 'lsm':
        model_list = lsm_list[(year_str, d)]
    elif model_set == 'cmip5':
        model_list = cmip5_availability(dcm, land_mask)
    else:
        cmip6_list_1 = mrsol_availability(dcm, land_mask, 'historical')
        cmip6_list_2 = mrsol_availability(dcm, land_mask, 'ssp585')
        cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) )
        model_list = [x for x in cmip6_list if 'r1i1p1f1' in x]

    #pool = mp.Pool(min(4, len(model_list)))
    #pool.map_async(calc, [(model_set, mm, d, dcm) for mm in model_list])
    #pool.close()
    #pool.join()

    for mm in model_list:
        calc([model_set, mm, d, dcm])
