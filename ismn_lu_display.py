import numpy as np
import utils_management as mg
from utils_management.constants import depth, target_lat, target_lon, \
    modis_legend
import xarray as xr
import os
import pandas as pd
import time
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
import cartopy.crs as ccrs
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import matplotlib as mpl


cmap = get_cmap('gist_ncar')

mpl.rcParams['font.size'] = 6

fig, ax = plt.subplots(figsize = (6.5, 4),
                       subplot_kw = {'projection': ccrs.PlateCarree()})


hr = xr.open_dataset(os.path.join(mg.path_intrim_out(), 
                                  'lu_aggr', 'mcd12c1_0.5.nc'))
lu = hr.__xarray_dataarray_variable__

lu2 = xr.apply_ufunc(np.argmax, lu, input_core_dims = [['class']],
                     vectorize = True, dask = 'allowed')
##lu2 = lu2.where(~np.isnan(lu[:, :, 0]))


gl = ax.gridlines(crs=ccrs.PlateCarree(), linewidth=1, color='black',
                  alpha=0., linestyle='--', draw_labels=True)
gl.xlabels_top = False
gl.ylabels_right = False
gl.xlocator = mticker.FixedLocator(np.arange(-180, 181, 40))
gl.ylocator = mticker.FixedLocator(np.arange(-80, 81, 20))
gl.xformatter = LONGITUDE_FORMATTER
gl.yformatter = LATITUDE_FORMATTER
gl.xlabel_style = {'color': 'black', 'weight': 'normal'}

ax.set_extent([-180, 180, -60., 80])
ax.coastlines()
cf = ax.contourf(lu.lon, lu.lat, lu2, cmap = cmap,
                 levels = np.arange(-0.5, 16.6, 1.))
cbar = plt.colorbar(cf, ax = ax, orientation = 'horizontal',
                    boundaries = np.arange(-0.5, 16.6, 1.),
                    ticks = np.arange(17))
cbar.ax.set_xticklabels([modis_legend[ii] for ii in range(17)],
                        rotation = 90)

fig.savefig(os.path.join(mg.path_out(), 'lu_map.png'),
            dpi = 600., bbox_inches = 'tight')

plt.close(fig)
hr.close()
