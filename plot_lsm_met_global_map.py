"""
2019/06/12
ywang254@utk.edu

Plot the global maps of interpolated soil moisture at each depth (if exist).
"""
import xarray as xr
import sys
import os
import matplotlib.pyplot as plt
from utils_management.paths import path_intrim_out
from utils_management.constants import depth_cm, lsm_all
import cartopy.crs as ccrs
from glob import glob
import multiprocessing as mp
##import numpy as np


land_mask = 'vanilla' # 'None', 'vanilla'


pr_all  = ['CRU_v4.03', 'CRU_v3.20', 'CRU_v3.26', 'CERA20C_pr',
           'ERA20C_pr', 'ERA-Interim_pr', 'ERA5_pr', 'GLDAS_Noah2.0_pr']
tas_all  = ['CRU_v4.03', 'CRU_v3.20', 'CRU_v3.26', 'CERA20C_tas',
            'ERA20C_tas', 'ERA-Interim_tas', 'ERA5_tas', 'GLDAS_Noah2.0_tas']

#var = 'pr'
#for model in pr_all:
def plotter(model, var):
    fig, ax = plt.subplots(figsize = (10, 6),
                           subplot_kw = {'projection': ccrs.PlateCarree()})

    filename = sorted(glob(os.path.join(path_intrim_out(), 'Interp_Merge',
                                        land_mask, model, '*' + var + \
                                        '*.nc')))
    if len(filename) == 0:
        return None
        #continue

    data = xr.open_mfdataset(filename, decode_times = False,
                             concat_dim = 'time')
    var_data = data[var].mean(dim='time').copy(deep=True).load()
    data.close()

    ##print(np.where(var_data.values))

    ax.coastlines()
    h = ax.contourf(var_data.lon.values, var_data.lat.values,
                    var_data.values)
    plt.colorbar(h, ax = ax, shrink = 0.7)
    ax.set_title(var)
    fig.savefig(os.path.join(path_intrim_out(), 'Interp_Merge_plot', 
                             land_mask, 'lsm_met', 
                             'global_map_' + model + '_' + var + '.png'),
                dpi = 600.)
    plt.close(fig)


p = mp.Pool(8)
[p.apply_async(plotter, args = (model, 'pr')) for model in pr_all]
[p.apply_async(plotter, args = (model, 'tas')) for model in tas_all]
p.close()
p.join()
