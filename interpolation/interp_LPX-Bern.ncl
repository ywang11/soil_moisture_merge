load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"
load "compute_soil_wts_bnds.ncl"
load "compute_horizontal_regrid.ncl"
load "interp_landfrac_conserve.ncl"

begin
  mask_opt = "None"; "e1m", "SYNMAP"

  ; soil layer bounds (unit: m)
  depth_bnds = (/(/0., 0.1/), (/0.1, 0.2/), (/0.2, 0.3/), \
                 (/0.3, 0.5/), (/0.5, 0.7/), (/0.7, 1./), \
                 (/1., 1.5/), (/1.5, 2./)/)

  sfile = addfiles(systemfunc("ls $SCRATCHDIR/Soil_Moisture/" + \
                              "data/TRENDY/LPX-Bern/S3/" + \
                              "LPX_S3_msl.nc"),"r")

  ; convert the time from seconds since to days since, bc the values
  ; are too large for integer. Add calendar attribute.
  sm_original = sfile[:]->msl({73000:}, :, :, :)

  ; convert kg/m^2 to m^3/m^3
  nlev = dimsizes(depth_bnds)
  do nn = 0,nlev(0)-1
    sm_original(:, nn, :, :) = sm_original(:, nn, :, :) / \
        (depth_bnds(nn,1) - depth_bnds(nn,0)) * 0.001
  end do

  ; latitude is already S->N

  ; change the dimension names
  sm_original!2 = "lat"
  sm_original!3 = "lon"

  ; homogenize the data type of the coordinates
  sm_original&lon := dble2flt(sm_original&lon)

  ;----------------------------------------------
  ; mask the ocean grid cells
  ;----------------------------------------------
  lat_original = sfile[0]->latitude
  lon_original = sfile[0]->longitude
  landfrac_new = interp_landfrac_conserve(lat_original, lon_original, \
                                          mask_opt)
  ntime = dimsizes(sm_original)
  do t = 0, ntime(0)-1
    do d = 0, ntime(1)-1
      sm_original(t,d,:,:) = where(landfrac_new .gt. 0., \
                                   sm_original(t,d,:,:), \
                                   sm_original@_FillValue)
    end do
  end do
  delete(lat_original)
  delete(lon_original)
  delete(landfrac_new)
  delete(ntime)
  delete(t)
  delete(d)

  ;----------------------------------------------
  ; 0-10cm, 10-30cm, 30-50cm, 50-100cm, 0.5 deg, monthly
  ;----------------------------------------------
  depth_to = (/ (/0., 0.1/), (/0.1, 0.3/), (/0.3, 0.5/), (/0.5, 1./) /)
  depth_to_names = (/ "0-10cm", "10-30cm", "30-50cm", "50-100cm" /)
  do d = 0,dimsizes(depth_to_names)-1
    sm_avg = sm_original(:, d, :, :)

    sm_avg@long_name = "Volumetric soil water"
    sm_avg@units = "m3/m3"
    sm_avg@depth_bnds = depth_to_names(d)

    ; interpolate from 0.5 deg to 0.5 deg
    ; ---- is already on the satisfactory grid, and longitude
    ;      is already -180. to 180.
    ;; sm = compute_horizontal_regrid(sm_avg, "LPX-Bern")
    sm = sm_avg
    ;;temp = ind(sm&lon .ge. 180.)
    ;;printVarSummary(sm)
    ;;print(sm&lon)
    ;;print(sm&lat)
    ;;print(temp)
    ;;sm&lon(temp) = sm&lon(temp) - 360.

    ; reset the time unit
    sm&time := todouble(sm&time)
    sm&time@units = "days since 1700-01-01 00:00:00"
    sm&time@calendar = "noleap"
    ;;print(sm&time)
    sm&time := cd_convert(sm&time, "months since 1900-01-01 00:00")
    ;;print(sm&time)
    do tt = 0,dimsizes(sm&time)-1
      sm&time(tt) = floor(sm&time(tt))
    end do
    ;;print(sm&time)

    ; write to file by year
    do yy = 1900,2017
      fout = "$SCRATCHDIR/Soil_Moisture/intermediate/Interp_Merge/" + \
             mask_opt + "/LPX-Bern/LPX-Bern_" + tostring(yy) + "_" + \
             depth_to_names(d) + ".nc"
      system("rm -f " + fout)
      ncdf = addfile(fout, "c")
      filedimdef(ncdf, "time", -1, True)

      ;;print(yy)

      y2 = flt2dble(int2flt( (yy-1900)*12 ))
      y2@units = "months since 1900-01-01 00:00"
      y2@calendar = sm&time@calendar

      ;;print(y2)

      printVarSummary( sm({y2:(y2+11)}, :, :) )

      ncdf->sm = sm({y2:(y2+11)}, :, :)

      delete(fout)
      delete(ncdf)
      delete(y2)
    end do
    delete(sm)
    delete(sm_avg)
    delete(tt)
    delete(yy)
  end do
  delete(depth_bnds)
  delete(sfile)
  delete(sm_original)
  delete(nlev)
  delete(depth_to)
  delete(depth_to_names)
  delete(d)
end