###############################################################################
# udunits error workaround
# https://github.com/r-quantities/units/issues/1#issuecomment-330435512
###############################################################################
mkdir $HOME/local/udunits
mkdir $HOME/local/udunits/local
cd $HOME/local/udunits
wget ftp://ftp.unidata.ucar.edu/pub/udunits/udunits-2.2.26.tar.gz
tar xzvf udunits-2.2.26.tar.gz
cd udunits-2.2.26
./configure --prefix=$HOME/local/udunits/local
make
make install

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/local/udunits/local/lib

install.packages("units", configure.args='--with-udunits2-lib=/nics/c/home/ywang254/local/udunits/local/lib --with-udunits2-include=/nics/c/home/ywang254/local/udunits/local/include/')


###############################################################################
# rgdal error workaround
###############################################################################
# Install SQLite: do not load anaconda on startup because it causes curl error
cd $HOME/local
mkdir sqlite_install
tar xzvf sqlite-autoconf-3320300.tar.gz
cd sqlite-autoconf-3320300
./configure --prefix=$HOME/local/sqlite_install
make
make install


# Install PROJ4
cd $HOME/local
mkdir proj_install
tar xzvf proj-7.1.0.tar.gz
cd proj-7.1.0
./configure --prefix=$HOME/local/proj_install SQLITE3_CFLAGS=-I$HOME/local/sqlite_install/include/ SQLITE3_LIBS="-L$HOME/local/sqlite_install/lib/ -lsqlite3"
make
make install


# Install GDAL
cd $HOME/local
mkdir gdal_install
tar xvzf gdal-3.1.2.tar.gz
cd gdal-3.1.2
./configure --prefix=$HOME/local/gdal_install --with-proj=$HOME/local/proj_install
make
make install


# Install rgdal
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/nics/c/home/ywang254/local/gdal_install/lib:/nics/c/home/ywang254/local/proj_install/lib
export PROJ_LIB=/nics/c/home/ywang254/local/proj_install/share/proj

install.packages("rgdal", configure.args = c("--with-gdal-config=/nics/c/home/ywang254/local/gdal_install/bin/gdal-config",
                 "--with-proj-include=/nics/c/home/ywang254/local/proj_install/include",
                 "--with-proj-lib=/nics/c/home/ywang254/local/proj_install/lib",
                 "CFLAGS=-I/nics/c/home/ywang254/local/proj_install/include"))



# Do not load anaconda3 on start up
install.packages("sf", configure.args = c("--with-gdal-config=/nics/c/home/ywang254/local/gdal_install/bin/gdal-config",
                 "--with-proj-include=/nics/c/home/ywang254/local/proj_install/include",
                 "--with-proj-lib=/nics/c/home/ywang254/local/proj_install/lib",
                 "CFLAGS=-I/nics/c/home/ywang254/local/proj_install/include"))


install.packages("lwgeom")