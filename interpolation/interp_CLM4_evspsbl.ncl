load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"
load "compute_soil_wts_bnds.ncl"
load "compute_horizontal_regrid.ncl"
load "interp_landfrac_conserve.ncl"

begin
  mask_opt = "None"; "e1m", "SYNMAP"

  ; read soil moisture data
  sfile = addfiles(systemfunc("ls $PROJDIR/Soil_Moisture/" + \
                              "data/MsTMIP/" + \
                              "CLM4_BG1_Monthly_Evap.nc4"), "r")
  et = sfile[:]->Evap

  ; unit conversion kg m-2 s-1 to mm/day
  et = et * 86400
  et@units = "mm/day"

  ; the latitude is already S->N

  ; interpolate 0.5o to 0.5o because different nodes
  et2 = compute_horizontal_regrid(et, "CLM4")

  ; reset the time unit
  et2&time := cd_convert(et2&time, "months since 1900-01-01 00:00")
  do tt = 0,dimsizes(et2&time)-1
    et2&time(tt) = floor(et2&time(tt))
  end do
  print(et2&time)

  ; write to file by year
  do yy = 1950,2010
    fout = "$PROJDIR/Soil_Moisture/intermediate/Interp_Merge/" + \
           mask_opt + "/CLM4_evspsbl/CLM4_" + tostring(yy) + ".nc"
    system("rm -f " + fout)
    ncdf = addfile(fout, "c")
    filedimdef(ncdf, "time", -1, True)

    y2 = flt2dble(int2flt( (yy-1900)*12 ))
    y2@units = "months since 1900-01-01 00:00"
    y2@calendar = "gregorian"

    printVarSummary( et2({y2:(y2+11)}, :, :) )

    ncdf->evspsbl = et2({y2:(y2+11)}, :, :)

    delete(fout)
    delete(ncdf)
    delete(y2)
  end do
  delete(et2)
  delete(sfile)
  delete(et)
  delete(mask_opt)
end