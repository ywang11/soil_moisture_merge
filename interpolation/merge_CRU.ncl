; CRU v3.2 is used to force the TRENDY and MsTMIP simulations, but
; is not available after 2011. Use CRUv4.03 for the afterwards years.
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"
load "interp_landfrac_conserve.ncl"


begin
  mask_opt = "None"

  sfiles = addfiles(systemfunc("ls $SCRATCHDIR/Soil_Moisture/data/" + \
                               "CRU_v3.20/*pre.dat.nc"), "r")
  pr = sfiles[:]->pre
  ; reset the time unit
  pr&time := cd_convert(pr&time, "months since 1900-01-01 00:00")
  ; change the unit name, because later mm/month is converted into mm/day
  pr@units = "mm/day"

  sfiles2 = addfiles(systemfunc("ls $SCRATCHDIR/Soil_Moisture/data/" + \
                                "CRU_v4.03/*pre.dat.nc"), "r")
  pr2 = sfiles2[:]->pre
  ; reset the time unit
  pr2&time := cd_convert(pr2&time, "months since 1900-01-01 00:00")
  ; change the unit name, because later mm/month is converted into mm/day
  pr2@units = "mm/day"

  ; apply the land mask
  lat_original = sfiles[0]->lat
  lon_original = sfiles[0]->lon
  landfrac_new = interp_landfrac_conserve(lat_original, lon_original, \
                                          mask_opt)
  ntime = dimsizes(pr)
  do t = 0, ntime(0)-1
    pr(t,:,:) = where(landfrac_new .gt. 0., pr(t,:,:), \
                      pr@_FillValue)
  end do
  ntime := dimsizes(pr2)
  do t = 0, ntime(0)-1
    pr2(t,:,:) = where(landfrac_new .gt. 0., pr2(t,:,:), \
                       pr2@_FillValue)
  end do
  delete(lat_original)
  delete(lon_original)
  delete(landfrac_new)
  delete(ntime)
  delete(t)

  ; write to file by year
  do yy = 1950,2016
    fout = "$SCRATCHDIR/Soil_Moisture/intermediate/Interp_Merge/" + \
           mask_opt + "/CRU_Merged/CRU_Merged_pr_" + tostring(yy) + ".nc"
    system("rm -f " + fout)
    ncdf = addfile(fout, "c")
    filedimdef(ncdf, "time", -1, True)

    y2 = flt2dble(int2flt( (yy-1900)*12 ))
    y2@units = "months since 1900-01-01 00:00"
    y2@calendar = "gregorian"

    if yy .le. 2011 then
      ; convert to mm/day from monthly total
      do m = 0,11
        pr({y2+m}, :, :) = pr({y2+m}, :, :) /   days_in_month(yy, m+1)
      end do
      ncdf->pr = pr({y2:(y2+11)}, :, :)
    else
      ; convert to mm/day from monthly total
      do m = 0,11
        pr2({y2+m}, :, :) = pr2({y2+m}, :, :) /   days_in_month(yy, m+1)
      end do
      ncdf->pr = pr2({y2:(y2+11)}, :, :)
    end if

    delete(fout)
    delete(ncdf)
    delete(y2)
  end do

  delete(sfiles)
  delete(sfiles2)
  delete(pr)
  delete(pr2)
  delete(yy)
end