load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"
load "compute_soil_wts_bnds.ncl"
load "compute_horizontal_regrid.ncl"
load "interp_landfrac_conserve.ncl"

begin
  mask_opt = "None"; "e1m", "SYNMAP"

  ; soil layer bounds (unit: m)
  depth_bnds = (/(/0., 0.065/), (/0.065, 0.319/), (/0.319, 1.232/), \
                 (/1.232, 4.134/), (/4.134, 9.834/)/)

  sfile = addfiles(systemfunc("ls $SCRATCHDIR/Soil_Moisture/" + \
                              "data/TRENDY/JSBACH/S3/" + \
                              "JSBACH_S3_msl.nc"),"r")

  ; convert the time from seconds since to days since, bc the values
  ; are too large for integer. Add calendar attribute.
  sm_original = sfile[:]->msl({72666:}, :, :, :)

  ; convert kg/m^2 to m^3/m^3
  nlev = dimsizes(depth_bnds)
  do nn = 0,nlev(0)-1
    sm_original(:, nn, :, :) = sm_original(:, nn, :, :) / \
        (depth_bnds(nn,1) - depth_bnds(nn,0)) * 0.001
  end do

  ; latitue is already S->N

  ; apply the ocean mask
  ; (for the models that do not have ocean values, appears to have the
  ;  no effect)
  lat_original = sfile[0]->lat
  lon_original = sfile[0]->lon
  landfrac_new = interp_landfrac_conserve(lat_original, lon_original, \
                                          mask_opt)
  ntime = dimsizes(sm_original)
  do t = 0, ntime(0)-1
    do d = 0, ntime(1)-1
      sm_original(t,d,:,:) = where(landfrac_new .gt. 0., \
                                   sm_original(t,d,:,:), \
                                   sm_original@_FillValue)
    end do
  end do
  delete(lat_original)
  delete(lon_original)
  delete(landfrac_new)
  delete(ntime)
  delete(t)
  delete(d)

  ;----------------------------------------------
  ; 0-10cm, 30-50cm, ~1.875deg -> 0.5 deg, monthly
  ;----------------------------------------------
  depth_to = (/ (/0., 0.1/), (/0.3, 0.5/) /)
  depth_to_names = (/ "0-10cm", "30-50cm" /)

  do d=0,dimsizes(depth_to_names)-1
    ; calculate the weighted average
    wgts = compute_soil_wts_bnds(depth_bnds, depth_to(d,0), depth_to(d,1))
    sm_avg = dim_avg_wgt_n_Wrap(sm_original, wgts, 0, 1)

    sm_avg@long_name = "Volumetric soil water"
    sm_avg@units = "m3/m3"
    sm_avg@depth_bnds = depth_to_names(d)

    ; horizontal interpolation
    sm = compute_horizontal_regrid(sm_avg, "JSBACH")

    ; reset the time unit
    sm&time := cd_convert(sm&time, "months since 1900-01-01 00:00")
    ;;print(sm&time)
    do tt = 0,dimsizes(sm&time)-1
      sm&time(tt) = floor(sm&time(tt))
    end do
    ;;print(sm&time)

    ; write to file by year
    do yy = 1900,2017
      fout = "$SCRATCHDIR/Soil_Moisture/intermediate/Interp_Merge/" + \
             mask_opt + "/JSBACH/JSBACH_" + tostring(yy) + "_" + \
             depth_to_names(d) + ".nc"
      system("rm -f " + fout)
      ncdf = addfile(fout, "c")
      filedimdef(ncdf, "time", -1, True)

      y2 = flt2dble(int2flt( (yy-1900)*12 ))
      y2@units = "months since 1900-01-01 00:00"
      y2@calendar = sm&time@calendar
      ;;print(y2)
      printVarSummary( sm({y2:(y2+11)}, :, :) )

      ncdf->sm = sm({y2:(y2+11)}, :, :)

      delete(fout)
      delete(ncdf)
      delete(y2)
    end do
    delete(tt)
    delete(sm)
    delete(sm_avg)
    delete(wgts)
    delete(yy)
  end do
  delete(depth_bnds)
  delete(sfile)
  delete(sm_original)
  delete(nlev)
  delete(nn)
  delete(depth_to)
  delete(depth_to_names)
  delete(d)
end