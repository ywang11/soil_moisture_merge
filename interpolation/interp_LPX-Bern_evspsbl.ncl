load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"
load "compute_soil_wts_bnds.ncl"
load "compute_horizontal_regrid.ncl"
load "interp_landfrac_conserve.ncl"

begin
  mask_opt = "None"; "e1m", "SYNMAP"

  sfile = addfiles(systemfunc("ls $PROJDIR/Soil_Moisture/" + \
                              "data/TRENDY/LPX-Bern/S3/" + \
                              "LPX_S3_evapotrans.nc"),"r")

  ; convert the time from seconds since to days since, bc the values
  ; are too large for integer. Add calendar attribute.
  et = sfile[:]->evapotrans

  ; convert kg m-2 s-1 to mm/day
  et = et * 86400

  ; latitude is already S->N

  ; change the dimension names
  et!1 = "lat"
  et!2 = "lon"

  ; homogenize the data type of the coordinates
  et&lon := dble2flt(et&lon)

  ;; et2 = compute_horizontal_regrid(et, "LPX-Bern")
  et2 = et

  ; reset the time unit
  et2&time := todouble(et2&time)
  et2&time@units = "days since 1700-01-01 00:00:00"
  et2&time@calendar = "noleap"
  ;;print(et2&time)
  et2&time := cd_convert(et2&time, "months since 1900-01-01 00:00")
  ;;print(et2&time)
  do tt = 0,dimsizes(et2&time)-1
    et2&time(tt) = floor(et2&time(tt))
  end do
  ;;print(et2&time)

  ; write to file by year
  do yy = 1900,2017
    fout = "$PROJDIR/Soil_Moisture/intermediate/Interp_Merge/" + \
           mask_opt + "/LPX-Bern_evspsbl/LPX-Bern_" + tostring(yy) + ".nc"
    system("rm -f " + fout)
    ncdf = addfile(fout, "c")
    filedimdef(ncdf, "time", -1, True)

    ;;print(yy)

    y2 = flt2dble(int2flt( (yy-1900)*12 ))
    y2@units = "months since 1900-01-01 00:00"
    y2@calendar = et2&time@calendar

    ;;print(y2)

    printVarSummary( et2({y2:(y2+11)}, :, :) )

    ncdf->evspsbl = et2({y2:(y2+11)}, :, :)

    delete(fout)
    delete(ncdf)
    delete(y2)
  end do
  delete(et)
  delete(et2)
  delete(sfile)
  delete(mask_opt)
end