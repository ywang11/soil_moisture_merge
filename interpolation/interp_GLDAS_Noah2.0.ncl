load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"
load "compute_soil_wts_bnds.ncl"
load "compute_horizontal_regrid.ncl"
load "interp_landfrac_conserve.ncl"

begin
  mask_opt = "None"; "e1m", "SYNMAP"

  do n=1948,2010
    sfile = addfiles(systemfunc("ls $SCRATCHDIR/Soil_Moisture/" + \
                                "data/GLDAS/NOAH025_M2.0/" + \
                                "GLDAS_NOAH025_M.A" + tostring(n) + \
                                "*.020.nc4.SUB.nc4"),"r")

    depth_bnds = (/(/0,0.1/), (/.1,.4/), (/.4,1./), (/1.,2./)/)
    msl1 = sfile[:]->SoilMoi0_10cm_inst
    msl2 = sfile[:]->SoilMoi10_40cm_inst
    msl3 = sfile[:]->SoilMoi40_100cm_inst
    msl4 = sfile[:]->SoilMoi100_200cm_inst

    db = dimsizes(depth_bnds)
    nlayers = db(0)
    delete(db)

    ; convert from kg/m2 to m3/m3
    msl1 = msl1 * 0.0010 / (depth_bnds(0,1) - depth_bnds(0,0))
    msl2 = msl2 * 0.0010 / (depth_bnds(1,1) - depth_bnds(1,0))
    msl3 = msl3 * 0.0010 / (depth_bnds(2,1) - depth_bnds(2,0))
    msl4 = msl4 * 0.0010 / (depth_bnds(3,1) - depth_bnds(3,0))

    ;---------------------------------------------------
    ; apply the ocean mask
    ;---------------------------------------------------
    lat_original = sfile[0]->lat
    lon_original = sfile[0]->lon
    landfrac_new = interp_landfrac_conserve(lat_original, lon_original, \
                                            mask_opt)
    ntime = dimsizes(msl1)
    do t = 0, ntime(0)-1
      msl1(t,:,:) = where(landfrac_new .gt. 0., msl1(t,:,:), \
                          msl1@_FillValue)
      msl2(t,:,:) = where(landfrac_new .gt. 0., msl2(t,:,:), \
                          msl2@_FillValue)
      msl3(t,:,:) = where(landfrac_new .gt. 0., msl3(t,:,:), \
                          msl3@_FillValue)
      msl4(t,:,:) = where(landfrac_new .gt. 0., msl4(t,:,:), \
                          msl4@_FillValue)
    end do
    delete(lat_original)
    delete(lon_original)
    delete(landfrac_new)
    delete(ntime)
    delete(t)

    ;---------------------------------------------------
    ; 0-10cm, 10-30cm, 30-50cm, 50-100cm, 1 deg -> 0.5 deg, monthly
    ;---------------------------------------------------
    depth_to = (/ (/0., 0.1/), (/0.1, 0.3/), (/0.3, 0.5/), (/0.5, 1./) /)
    depth_to_names = (/ "0-10cm", "10-30cm", "30-50cm", "50-100cm" /)

    do d=0,dimsizes(depth_to_names)-1
      wgts = compute_soil_wts_bnds(depth_bnds, depth_to(d,0), \
                                   depth_to(d,1))
      ; exchange variable name
      sm_avg = msl1 * wgts(0) + msl2 * wgts(1) + msl3 * wgts(2) + \
           msl4 * wgts(3)

      sm_avg!0 = "time"
      sm_avg&time = msl1&time
      sm_avg!1 = "lat"
      sm_avg&lat = msl1&lat
      sm_avg!2 = "lon"
      sm_avg&lon = msl1&lon

      sm_avg@long_name = "Volumetric soil water"
      sm_avg@units = "m3/m3"
      sm_avg@depth_bnds = depth_to_names(d)

      sm = compute_horizontal_regrid(sm_avg, \
                                     "GLDAS_Noah2.0_" + depth_to_names(d))

      ; reset the time unit
      ;;print(sm&time)
      sm&time = cd_convert(sm&time, "months since 1900-01-01 00:00")
      ;;print(sm&time)
      ; write to file
      fout = "$SCRATCHDIR/Soil_Moisture/intermediate/Interp_Merge/" + \
             mask_opt + "/GLDAS_Noah2.0/GLDAS_Noah2.0_" + \
             tostring(n) + "_" + depth_to_names(d) + ".nc"
      system("rm -f " + fout)
      ncdf = addfile(fout, "c")
      filedimdef(ncdf, "time", -1, True)
      ncdf->sm = sm

      delete(wgts)
      delete(sm)
      delete(fout)
      delete(ncdf)
    end do
    delete(depth_to)
    delete(depth_to_names)
    delete(msl1)
    delete(msl2)
    delete(msl3)
    delete(msl4)
    delete(depth_bnds)
    delete(sfile)
  end do
end