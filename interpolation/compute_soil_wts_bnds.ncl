function compute_soil_wts_bnds (depth_bnds, min_depth, max_depth)

begin

  ; Set depth_bnds, nlev, max_depth
  nlev = dimsizes(depth_bnds)

  ;;print(depth_bnds)
  ;;print(nlev)

  nlev := nlev(0)
  ;max_depth = 1.0 ; m
  ; Create top, bottom, thick, wt
  top = depth_bnds(:,0)
  bottom = depth_bnds(:,1)
  thick = bottom - top
  wt = new(nlev,float)
  ; Construct weights for soil levels down to max_depth m
  do lev=0,nlev-1
    if (top(lev) .ge. min_depth) then
      if (top(lev) .lt. max_depth) then
        if (bottom(lev) .le. max_depth) then
          wt(lev) = thick(lev)
        else
          wt(lev) = max_depth - top(lev)
        end if
      else
        wt(lev) = 0.0
      end if
    else
      if (bottom(lev) .gt. min_depth) then
        if (bottom(lev) .le. max_depth) then
          wt(lev) = bottom(lev) - min_depth
        else
          print("The min_depth and max_depth is entirely contained within " + \
                tostring(top(lev)) + " " + tostring(bottom(lev)))
          exit()
        end if
      else
        wt(lev) = 0.0
      end if
    end if
  end do


  ; Normalize weights
  wt = wt/sum(wt)
  ;
  ;printVarSummary(top)
  ;printVarSummary(bottom)
  ;printVarSummary(thick)
  ;printVarSummary(wt)

  return(wt)
end