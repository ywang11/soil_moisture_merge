load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"
load "compute_soil_wts_bnds.ncl"
load "compute_horizontal_regrid.ncl"
load "interp_landfrac_conserve.ncl"

begin
  mask_opt = "None"; "e1m", "SYNMAP"

  do n=1979,2018
    sfile = addfiles(systemfunc("ls $SCRATCHDIR/Soil_Moisture/" + \
                                "data/Reanalysis/ERA-Interim/" + \
                                "tas_" + tostring(n) + ".gr"),"r")

    ; convert the unit from Kelvin to Celsius
    tas0 = sfile[:]->2T_GDS0_SFC_S123

    tas = tas0 - 273.15
    copy_VarCoords(tas0, tas)
    delete(tas0)

    tas!0 = "time"
    tas!1 = "lat"
    tas!2 = "lon"

    tas@units = "mm/day"

    ; reverse latitude from N->S to S->N
    tas = tas(:, ::-1, :)

    ; mask out ocean regions
    lat_original = sfile[0]->g0_lat_1(::-1)
    lon_original = sfile[0]->g0_lon_2
    landfrac_new = interp_landfrac_conserve(lat_original, lon_original, \
                                            mask_opt)
    ntime = dimsizes(tas)
    do d=0,ntime(0)-1
      tas(d,:,:) = where(landfrac_new .gt. 0., tas(d,:,:), tas@_FillValue)
    end do
    delete(ntime)
    delete(landfrac_new)
    delete(lat_original)
    delete(lon_original)

    ; interpolate from 0.75o to 1o
    tas2 = compute_horizontal_regrid(tas, "ERA-Int_tas_")

    ; reset the time unit
    tas2&time = cd_convert(tas2&time, "months since 1900-01-01 00:00")

    ; write to file
    fout = "$SCRATCHDIR/Soil_Moisture/intermediate/Interp_Merge/" + \
           mask_opt + "/ERA-Interim_tas/ERA-Interim_tas_" + tostring(n) + ".nc"
    system("rm -f " + fout)
    ncdf = addfile(fout, "c")
    filedimdef(ncdf, "time", -1, True)
    ncdf->tas = tas2

    delete(fout)
    delete(ncdf)
    delete(tas)
    delete(sfile)
  end do
end