#Date: 2017-01-01
#IBG-3,FZJ
#script to read rotated grid data and reprojected to WGS84 or other projection systems
rm(list=ls())
library(sp)
library(maps)
library(rgeos)
library(mapdata)
library(ncdf4)
library(raster)
library(maptools)
library(sf)
library(lwgeom)

#path = "/home/b.naz/FZ-researc/EoCoE/CLM3k/CLM_gridfiles"
path = "/lustre/haven/proj/UTK0134/Soil_Moisture/data/ESSMRA"
wrfFilename="geo_em.d03_MODIS3km1kmlakes_geo_cutdowntocosmo.nc"

for (yr in seq(2015,2015,1)){
for (mm in seq(1,12,1)){
  SMFilename=sprintf("EU_ESSMRA_daily_SSM_DA_ensmean_FZJ-IBG3_CLM-PDAF_3Km_v1.%d%02d.nc", yr, mm)
  SMfilePath=sprintf("%s/%s",path,SMFilename)
  WRFfilePath=sprintf("%s/%s",path,wrfFilename)
  var='H2OSOI'

  ncid=nc_open(WRFfilePath)
  
  #From WRF geo_em file:
  #For MAP_PROJ=6, rotated projection:
  #:STAND_LON = -18.f - lon0
  #:POLE_LAT = 39.25f - poleLat
  #:POLE_LON = 18.f - poleLon
  # projected grid resolution
  
  poleLat=ncatt_get(ncid,0)$POLE_LAT
  poleLon=ncatt_get(ncid,0)$POLE_LON
  
  write(poleLon, stdout())
  lon_0=-1*ncatt_get(ncid,0)$STAND_LON
  write(lon_0, stdout())
  cen_lat=ncatt_get(ncid,0)$CEN_LAT
  cen_lon=ncatt_get(ncid,0)$CEN_LON
  # the coordinates are on a sphere in longlat WGS84 this is standard not rotated
  
  proj1=CRS('+proj=longlat +ellps=WGS84 +datum=WGS84 +units=m +no_defs')
  # this gives the best answer:
  # see: https://fossies.org/linux/fimex/src/coordSys/WRFCoordSysBuilder.cc
  # I added: ellps=WGS84 +datum=WGS84
  rotProj=sprintf("+proj=ob_tran +datum=WGS84 +o_proj=longlat +lon_0=%s +o_lon_p=%s +o_lat_p=%s +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=km +no_defs",lon_0,poleLon,poleLat)
  nameProj="rotProj"
  wrfproj=CRS(rotProj)

  # domain corner grids. see http://www2.mmm.ucar.edu/wrf/users/docs/user_guide_V3/users_guide_chap3.htm
  cornerlat=ncatt_get(ncid,0)$corner_lats
  cornerlong=ncatt_get(ncid,0)$corner_lons
  write(cornerlong, stdout())
  write(cornerlat, stdout())
  # projected grid resolution
  dx=ncatt_get(ncid,0)$DX
  dy=ncatt_get(ncid,0)$DY
  
  # create spatial points
  # using the extreme corners of the data since raster uses extreme coordinates. page 3, https://cran.r-project.org/web/packages/raster/vignettes/Raster.pdf
  # there are 16 corner coordinates. 1:4 are cell centers. 13:16 are cellextremes (see wrf user_guide linked above)
  # these coordinates are in longlat so assign geographic system from above
  spext <-
    SpatialPoints(
      list( x=cornerlong[13:16],
            y=cornerlat[13:16]),
      proj4string = proj1)
  
  # convert to projected space
  spext.wrf <- spTransform(spext,wrfproj)
  spext.wrf
  
  #clmsm <- raster(SMfilePath,varname=var)
  clmsm <- stack(SMfilePath,varname=var)
  
  # assign projection detailed in the geo file (defined above as wrfproj)
  projection(clmsm)=wrfproj
  # change extent to that of the projected spatialpoints
  extent(clmsm) <- extent(spext.wrf)
  #xend = 0.003122283 + 4.823773e-07*1544
  #xend
  
  # project the raster
  r <- projectRaster(clmsm,crs=CRS("+proj=longlat +datum=WGS84"))
  
  # check the raster.
  #r <- projectRaster(topo12,res=1,crs=CRS("+proj=laea +lat_0=52 +lon_0=10 +x_0=4321000 +y_0=3210000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs"))
  data(wrld_simpl, package = "maptools")
  w = st_as_sf(wrld_simpl)

  png(sprintf("%s/proj_%s_%s_towgs84_%d%02d.png",path,var,nameProj,yr,mm))
  plot(r[[1]],col=topo.colors(25))
  plot(st_geometry(w),axes=F, add=T)
  #save to tiff
  dev.off()
  
  writeRaster(r, filename=sprintf("%s/proj_%s_%s_towgs84_%d%02d.nc",path,var,nameProj,yr,mm), format="CDF", overwrite=TRUE)
  cat("Returning projected matrices\n")

  nc_close(ncid)
}
}
