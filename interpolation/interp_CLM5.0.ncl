load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"
load "compute_soil_wts_bnds.ncl"
load "compute_horizontal_regrid.ncl"
load "interp_landfrac_conserve.ncl"

begin
  mask_opt = "None"; "e1m", "SYNMAP"

  ; soil layer bounds (unit: m)
  depth_bnds = (/(/0,0.02/),(/0.02,0.06/),(/0.06,0.12/),(/0.12,0.2/), \
                 (/0.2,0.32/),(/0.32,0.48/),(/0.48,0.68/),(/0.68,0.92/), \
                 (/0.92,1.2/),(/1.2,1.52/),(/1.52,1.88/),(/1.88,2.28/), \
                 (/2.28,2.72/),(/2.72,3.26/),(/3.26,3.9/),(/3.9,4.64/), \ 
                 (/4.64,5.48/),(/5.48,6.42/),(/6.42,7.46/),(/7.46,8.6/)/)
               ;;(/8.6,10.99/),(/10.99,15.666/),(/15.666,23.301/), \
               ;;(/23.301,34.441/), (/34.441,49.556/)/)
  db = dimsizes(depth_bnds)
  nlayers = db(0)
  delete(db)

  ;----------------------------------------------
  ; read soil moisture data
  ;----------------------------------------------
  sfile = addfiles(systemfunc("ls $SCRATCHDIR/Soil_Moisture/" + \
                              "data/TRENDY/" + \
                              "CLM5.0/S3/CLM5.0_S3_msl.nc"),"r")
  msl = sfile[:]->mrso
  ; convert kg/m^2 to m^3/m^3
  print(nlayers)
  printVarSummary(msl)
  do d=0,nlayers-1
    msl(:,d,:,:) = msl(:,d,:,:) / (depth_bnds(d,1) - depth_bnds(d,0)) * 0.001
  end do

  ;----------------------------------------------
  ; 0-10cm, 10-30cm, 30-50cm, 50-100cm, 1 deg -> 0.5 deg, monthly
  ;----------------------------------------------
  depth_to = (/ (/0., 0.1/), (/0.1, 0.3/), (/0.3, 0.5/), (/0.5, 1./) /)
  depth_to_names = (/ "0-10cm", "10-30cm", "30-50cm", "50-100cm" /)
  ; calculate the weighted average
  do d=0,dimsizes(depth_to_names)-1
    wgts = compute_soil_wts_bnds(depth_bnds, depth_to(d,0), depth_to(d,1))
    sm_avg = dim_avg_wgt_n_Wrap(msl, wgts, 0, 1)

    sm_avg!0 = "time"
    sm_avg&time = msl&time
    sm_avg!1 = "lat"
    sm_avg&lat = msl&lat
    sm_avg!2 = "lon"
    sm_avg&lon = msl&lon

    sm_avg@long_name = "Volumetric soil water"
    sm_avg@units = "m3/m3"
    sm_avg@depth_bnds = depth_to_names(d)

    ; reverse the latitude from N->S to S->N
    sm_avg = sm_avg(:, ::-1, :)
    ;;printVarSummary(sm_avg)

    ; mask out ocean grids
    lat_original = sfile[0]->lat(::-1)
    lon_original = sfile[0]->lon
    landfrac_new = interp_landfrac_conserve(lat_original, lon_original, \
                                            mask_opt)
    ntime = dimsizes(sm_avg)
    do t = 0, ntime(0)-1
      sm_avg(t,:,:) = where(landfrac_new .gt. 0., sm_avg(t,:,:), \
                            sm_avg@_FillValue)
    end do
    delete(lat_original)
    delete(lon_original)
    delete(landfrac_new)
    delete(ntime)
    delete(t)

    ; interpolate 1deg to 0.5deg
    sm = compute_horizontal_regrid(sm_avg, "CLM5.0")

    ; reset the time unit
    sm&time := cd_convert(sm&time, "months since 1900-01-01 00:00")
    ;;print(sm&time)
    do tt = 0,dimsizes(sm&time)-1
      sm&time(tt) = floor(sm&time(tt))
    end do
    ;;print(sm&time)

    ; write to file by year
    do yy = 1900,2017
      fout = "$SCRATCHDIR/Soil_Moisture/intermediate/Interp_Merge/" + \
             mask_opt + "/CLM5.0/CLM5.0_" + tostring(yy) + "_" + \
             depth_to_names(d) + ".nc"
      system("rm -f " + fout)
      ncdf = addfile(fout, "c")
      filedimdef(ncdf, "time", -1, True)

      y2 = flt2dble(int2flt( (yy-1900)*12 ))
      y2@units = "months since 1900-01-01 00:00"
      y2@calendar = "gregorian"

      ;;print(y2)
      printVarSummary( sm({y2:(y2+11)}, :, :) )

      ncdf->sm = sm({y2:(y2+11)}, :, :)

      delete(fout)
      delete(ncdf)
      delete(y2)
    end do
    delete(sm_avg)
    delete(sm)
    delete(yy)
  end do
  delete(depth_bnds)
  delete(sfile)
  delete(wgts)
  delete(msl)
end