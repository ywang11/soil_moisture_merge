import xarray as xr
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import numpy as np
import os


fig, axes = plt.subplots(nrows = 2, ncols = 2, figsize = (16, 8),
                         subplot_kw = {'projection': ccrs.PlateCarree()})

for i, dcm in enumerate(['0-10cm', '10-30cm']):
    for j, test in enumerate(['original', 'unmasked']):
        ax = axes[i, j]

        data = xr.open_mfdataset([os.environ['SCRATCHDIR'] + '/Soil_Moisture/'\
                                  + 'interp_CMIP6_hist_mrsol_test' \
                                  + '/' + test + '/' + \
                                  'mrsol_EC-Earth3_r1i1p1f1_historical_' + \
                                  str(y) + '_' + dcm + '.nc' for \
                                  y in range(1950, 2015)],
                                 decode_times = False)
        ax.coastlines()
        h = ax.contourf(data.lon.values, data.lat.values,
                        data['sm'].values.mean(axis = 0),
                        cmap = 'Spectral')
        plt.colorbar(h, ax = ax, shrink = 0.5)

        ax.set_title(dcm + ' ' + test)

        data.close()

fig.savefig(os.environ['SCRATCHDIR'] + '/Soil_Moisture/' + \
            'interp_CMIP6_hist_mrsol_test/test.png', dpi = 600.)
