"""
YW 20190403

Identify the models that exist for each depth and year.
"""
import numpy as np
import pandas as pd
import os
import itertools as it


depths = ["0-10cm", "10-30cm", "30-50cm", "50-100cm"]
## CABLE-POP data is wrong
models = ['CERA20C', 'CFSR', 'CLASS-CTEM-N', 'CLM4', 'CLM4VIC',
          'CLM5.0', 'ERA20C', 'ERA-Interim', 'ERA-Land', 'ESA-CCI',
          'GLDAS_CLM', 'GLDAS_Noah2.0', 'GLDAS_VIC', 'GLEAMv3.3a',
          'GTEC', 'ISAM', 'JSBACH', 'JULES', 'LPX-Bern',
          'ORCHIDEE', 'ORCHIDEE-CNP', 'SiBCASA']
years = [str(x) for x in np.arange(1950, 2017, 1)]


intersect_table = pd.DataFrame(data=False, index = \
    pd.MultiIndex.from_product((depths, years), sortorder=1, \
    names = ['Depth', 'Year']), columns=models)


land_mask = 'SYNMAP'
path_merged = os.path.join(os.environ['SCRATCHDIR'], 'Soil_Moisture',
                           'intermediate', 'Interp_Merge', land_mask)


for k in range(len(models)):
    files = os.listdir(os.path.join(path_merged, models[k]))
    for i in files:
        i_temp = i.split('_')
        if i_temp[-2] in years:
            intersect_table.loc[(i_temp[-1].split('.')[0],i_temp[-2]), 
                                models[k]] = True
intersect_table.to_csv(os.path.join(path_merged, "intersect_table.csv"))


# (make a list of the available model by time period and depth
periods = ["1950-2016", "1950-2010", "1981-2010", "1981-2016"]

intersect_table.sort_index(level=[0,1], inplace=True)

f = open(os.path.join(path_merged,
                      "intersect_table_summary.csv"),'w')

for i,j in it.product(depths, periods):
    yr_start = j.split("-")[0]
    yr_end   = j.split("-")[1]

    temp = intersect_table.loc[(i, slice(yr_start, yr_end)), :].copy()

    mtemp = temp.columns[temp.all(axis=0).values]

    f.write('(' + j + ',' + i + '):' + '[' + ','.join(mtemp) + ']\n')


f.close()
