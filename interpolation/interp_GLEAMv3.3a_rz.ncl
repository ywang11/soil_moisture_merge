load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"
load "compute_soil_wts_bnds.ncl"
load "compute_horizontal_regrid.ncl"
load "interp_landfrac_conserve.ncl"

begin
  mask_opt = "None"; "e1m", "SYNMAP"

  ;;root_path = "$SCRATCHDIR/Soil_Moisture/"
  root_path = "$PROJDIR/Soil_Moisture/"

  ;----------------------------------------------
  ; read surface soil moisture data: 0-10cm
  ;----------------------------------------------
  sfile = addfiles(systemfunc("ls " + root_path + \
                              "data/GLEAM/v3.3a/monthly/" + \
                              "SMroot_1980_2018_GLEAM_v3.3a_MO.nc"), "r")
  ; unit is already m3/m3
  sm1 = sfile[:]->SMroot
  sm1@long_name = "Volumetric soil water"
  sm1@units = "m3/m3"
  sm1@depth_bnds = "0-100cm"

  ; reorder the lat & lon
  sm1 := sm1(time|:, lat|:, lon|:)

  sm1 = sm1(:, ::-1, :)

  ; mask the ocean grids after the reversal of latitude
  lat_original = sfile[0]->lat(::-1)
  lon_original = sfile[0]->lon
  landfrac_new = interp_landfrac_conserve(lat_original, lon_original, \
                                          mask_opt)
  ntime = dimsizes(sm1)
  do t = 0, ntime(0)-1
    sm1(t,:,:) = where(landfrac_new .gt. 0., sm1(t,:,:), \
                       sm1@_FillValue)
  end do
  delete(lat_original)
  delete(lon_original)
  delete(landfrac_new)
  delete(ntime)
  delete(t)

  ;----------------------------------------------
  ; 0-100cm, 0.5 deg, monthly
  ;----------------------------------------------
  ; reverse the latitude from N->S to S->N
  sm1 = sm1(:, ::-1, :)
  ;;printVarSummary(sm1)

  ; interpolate 0.25o to 0.5
  sm = compute_horizontal_regrid(sm1, "GLEAM")

  ; reset the time unit
  sm&time = cd_convert(sm&time, "months since 1900-01-01 00:00")
  ;;print(sm&time)
  do tt = 0,dimsizes(sm&time)-1
    sm&time(tt) = floor(sm&time(tt))
  end do
  print(sm&time)

  ; write to file by year
  do yy = 1980,2018
    fout = root_path + "intermediate/Interp_Merge/" + \
           mask_opt + "/GLEAMv3.3a/GLEAMv3.3a_" + tostring(yy) + "_0-100cm.nc"
    system("rm -f " + fout)
    ncdf = addfile(fout, "c")
    filedimdef(ncdf, "time", -1, True)

    y2 = flt2dble(int2flt( (yy-1900)*12 ))
    y2@units = "months since 1900-01-01 00:00"
    y2@calendar = "gregorian"

    ;;print(y2)

    printVarSummary( sm({y2:(y2+11)}, :, :) )

    ncdf->sm = sm({y2:(y2+11)}, :, :)

    delete(fout)
    delete(ncdf)
    delete(y2)
  end do

  delete(sfile)
  delete(sm1)
  delete(sm)
  delete(tt)
  delete(yy)
end