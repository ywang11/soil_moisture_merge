load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"
load "compute_soil_wts_bnds.ncl"
load "compute_horizontal_regrid_pr.ncl"
load "interp_landfrac_conserve.ncl"

begin
  mask_opt = "None"; "e1m", "SYNMAP"

  sfile = addfile("$SCRATCHDIR/Soil_Moisture/data/Reanalysis/ERA5/" + \
                  "monthly_averaged_reanalysis_pr_1979-2018.nc", "r")

  ; convert the unit from m per 24-hour to mm/day
  pr0 = short2flt(sfile->tp)

  pr = pr0 * 1000.
  copy_VarCoords(pr0, pr)
  delete(pr0)

  pr!0 = "time"
  pr!1 = "lat"
  pr!2 = "lon"

  pr@units = "mm/day"

  ; reverse latitude from N->S to S->N
  pr = pr(:, ::-1, :)

  ; mask out ocean regions
  landfrac_new = interp_landfrac_conserve(pr&lat, pr&lon, mask_opt)
  ntime = dimsizes(pr)
  do d=0,ntime(0)-1
    pr(d,:,:) = where(landfrac_new .gt. 0., pr(d,:,:), pr@_FillValue)
  end do
  delete(ntime)
  delete(landfrac_new)

  ; interpolate from 0.75o to 1o
  pr2 = compute_horizontal_regrid_pr(pr, "ERA5_pr_")

  ; reset the time unit
  pr2&time := cd_convert(pr2&time, "months since 1900-01-01 00:00")

  do n=1979,2018
    ; write to file
    fout = "$SCRATCHDIR/Soil_Moisture/intermediate/Interp_Merge/" + \
           mask_opt + "/ERA5_pr/ERA5_pr_" + tostring(n) + ".nc"
    system("rm -f " + fout)
    ncdf = addfile(fout, "c")
    filedimdef(ncdf, "time", -1, True)

    y2 = flt2dble(int2flt( (n-1900)*12 ))
    y2@units = "months since 1900-01-01 00:00"
    y2@calendar = "gregorian"

    printVarSummary( pr2({y2:(y2+11)}, :, :) )

    ncdf->pr = pr2({y2:(y2+11)}, :, :)

    delete(fout)
    delete(ncdf)
    delete(y2)
  end do

  delete(sfile)
  delete(pr2)
  delete(pr)
end