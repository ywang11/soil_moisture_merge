"""
YW 20190403

Identify the CMIP5 models that exist for each depth.
"""
import numpy as np
import pandas as pd
import os
import itertools as it

depths = ["0-10cm", "10-30cm", "30-50cm", "50-100cm"]

years = [str(x) for x in np.arange(1950, 2017, 1)]

models = ['ACCESS1-0','ACCESS1-3','CanESM2', 'CNRM-CM5', 'CNRM-CM5-2',
          'FGOALS-g2', 'FGOALS-s2', 'GFDL-CM3','GFDL-ESM2G','GFDL-ESM2M',
          'GISS-E2-H-CC','GISS-E2-H','GISS-E2-R-CC','GISS-E2-R',
          'HadCM3', 'HadGEM2-ES','HadGEM2-CC','inmcm4','MIROC5',
          'MIROC-ESM','MIROC-ESM-CHEM','NorESM1-ME']


intersect_table = pd.DataFrame(data=False, index = depths, columns=models)


land_mask = 'vanilla' # 'None', 'vanilla'
path_merged = os.path.join(os.environ['SCRATCHDIR'], 'Soil_Moisture', 
                           'intermediate', 'Interp_Merge', land_mask)


for d,k in it.product(depths, range(len(models))):
    file1 = os.path.join(path_merged, 'CMIP5', models[k],
                         'sm_historical_r1i1p1_' + \
                         str(years[0]) + '_' + d + '.nc')
    file2 = os.path.join(path_merged, 'CMIP5', models[k],
                         'sm_rcp85_r1i1p1_' + \
                         str(years[-1]) + '_' + d + '.nc')
    file3 = os.path.join(path_merged, 'CMIP5', models[k],
                         'tas_historical_r1i1p1_' + \
                         str(years[0]) + '.nc')
    file4 = os.path.join(path_merged, 'CMIP5', models[k],
                         'tas_rcp85_r1i1p1_' + \
                         str(years[-1]) + '.nc')
    file5 = os.path.join(path_merged, 'CMIP5', models[k],
                         'pr_historical_r1i1p1_' + \
                         str(years[0]) + '.nc')
    file6 = os.path.join(path_merged, 'CMIP5', models[k],
                         'pr_rcp85_r1i1p1_' + \
                         str(years[-1]) + '.nc')

    if os.path.exists(file1) & os.path.exists(file2) & \
       os.path.exists(file3) & os.path.exists(file4) & \
       os.path.exists(file5) & os.path.exists(file6):
        intersect_table.loc[d, models[k]] = True

intersect_table.to_csv(os.path.join(path_merged, 'intersect_cmip5_table.csv'))
