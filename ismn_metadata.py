# -*- coding: utf-8 -*-
"""
Created on Thu Apr  4 16:09:42 2019

@author: ywang254

Calculate the number of stations in each 0.5x0.5 grid cell that satisfy the
following requirements:
  (1) Within the depth range (0-10cm, 10-30cm, 30-50cm, 50-100cm)
  (2) Following the first three criteria in "Hobeichi et al. 2018. Derived
      Optimal Linear Combination Evapotranspiration (DOLCE) A Global Gridded 
      Synthesis Estimate":
    (2.1) omit the observations that do not fall within the temporal coverage
          of the gridded soil moisture datasets
    (2.2) omit the daily soil moisture if <50% hourly soil moisture is 
          observed on that day
    (2.3) omit the monthly soil moisture average if <15 daily soil moisture
          values exist
    (2.4) keep only the stations within the spatial coverage of the soil
          moisture data (will do in the actual weighting step using ocean
          or NDVI masks)
    (2.5) exclude irrigated sites (how to do, or perhaps unecessary?) or 
          those that have only one monthly record
  (3) not flagged as problematic in ISMN
"""
import sys
import os
if sys.platform == 'linux':
    os.environ['QT_QPA_PLATFORM']='offscreen'
from ismn.interface import ISMN_Interface
from ismn.interface import ISMNError
import utils_management as mg
from misc.ismn_availability import ismn_availability
import os
import pandas as pd
import numpy as np


###############################################################################
# Function to loop through all network to collect the metadata information.
# ISMN networks are updated using the version accessed on 2019/07/17 ('ISMN2')
# The 'ISMN' folder was downloaded in 2018.
# https://ismn.geo.tuwien.ac.at/en/
###############################################################################
##def get_metadata(network):
##Too difficult to debug within a function.


###############################################################################
# Main loop.
###############################################################################
# Obtain the list of ISMN soil moisture network folders.
_, dirnames, _ = next(os.walk(os.path.join(mg.path_data(), 'ISMN')))

target_depth_from = np.array([0., .1, .3, .5]) # unit: m
target_depth_to   = np.array([.1, .3, .5, 1.]) # unit: m

time_range = pd.date_range(start='1950-01-01', end='2016-12-31', tz='UTC')

time_range_by_month = pd.date_range(start='1950-01-01', end='2016-12-31', 
                                    tz='UTC', freq='MS')


##dirnames = ['RISMA']

count_outer = 0
for network in dirnames:
    ##network_metadata = get_metadata(network)

    ###########################################################################
    # Things in this bracket is taken from the get_metadata() function.
    ###########################################################################
    path_to_ismn_data = os.path.join(mg.path_data(), 'ISMN2', network)

    ISMN_reader = ISMN_Interface(path_to_ismn_data)

    stations = ISMN_reader.list_stations(network = network)

    if isinstance(stations, type(None)):
        ##return None ## use this line if inside a function
        continue ## use this line if direct

    meta_list = ['Network', 'Station', 'Lat', 'Lon', 'Depth-Fit',
                 'Depth-Actual', 'Sensor', 'Land Cover 2000',
                 'Land Cover 2005', 'Land Cover 2010', 'Land Cover in situ',
                 'Climate Zone'] + list(time_range_by_month)
    network_metadata = pd.DataFrame(data = np.nan, index = [0], 
                                    columns = meta_list, dtype=object)

    count_inner = 0
    for st in stations:
        print('Network = ' + network + ' Station = ' + st)

        station_obj = ISMN_reader.get_station(st)

        # get the variables that this station measures
        variables = station_obj.get_variables()

        if not 'soil moisture' in variables:
            print('Soil moisture is not measured at ' + network + ' *********')
            print('which measures:')
            print(variables)
            continue

        # to make sure the selected variable is not measured
        # by different sensors at the same depths
        # we also select the first depth and the first sensor
        # even if there is only one
        var = 'soil moisture'
        ##depths_from, depths_to = station_obj.get_depths(var)
        depths_from = station_obj.depth_from
        depths_to = station_obj.depth_to

        # retrieve multiple sensors if multiple are used
        ##sensors = []
        ##for ii in range(len(depths_from)):
        ##    temp = list(station_obj.get_sensors(var, 
        ##                depths_from[ii], depths_to[ii]))
        ##    
        ##    if len(temp) == 1 or len(temp) == len(depths_from): 
        ##        sensors.append(temp)
        ##    else:
        ##        print(depths_from)
        ##        print(temp)
        ##        raise Exception('What happened here?')
        ##    ##sensors.append(temp[0])
        sensors = station_obj.sensors
        if len(depths_from) != len(depths_to):
            print(depths_from)
            print(depths_to)
            raise Exception('Unequal length of top and bottom depths.')

        if len(sensors) != len(depths_from):
            print(sensors)
            print(depths_from)
            raise Exception('Unequal length of sensors and depths.')

        # Exclude negative depths.
        temp = (depths_from > 0.) & (depths_to > 0.)
        depths_from = depths_from[temp]
        depths_to = depths_to[temp]
        sensors = sensors[temp]

        # Loop through the depths that are measured at this station.
        for ii in range(len(depths_from)):
            # exclude the station if the depth range is not within the desired
            # depth ranges
            if not ((depths_from[ii] >= target_depth_from[0] and \
                     depths_from[ii] <= target_depth_to[-1]) and \
                    (depths_to[ii] >= target_depth_from[0] and \
                     depths_to[ii] <= target_depth_to[-1])):
                continue

            ##if len(sensors[ii]) == 1:
            ##    srs = sensors[ii]
            ##else:
            ##    srs = sensors[ii][ii]
            srs = sensors[ii]

            # get the soil moisture data and flags
            try:
                time_series = station_obj.read_variable(var, 
                    depth_from=depths_from[ii], depth_to=depths_to[ii], 
                                                        sensor=srs)
            except ISMNError as e:
                if e.args[0] == 'There is no data for this combination of variable, depth_from, depth_to and sensor. Please check.':
                    continue

            # implement criteria (2.1) (2.2) (2.3) and (3)
            start_time = time_series.data.index[0]
            end_time = time_series.data.index[-1]

            if (start_time > time_range[-1]) | \
               (end_time < time_range[0]):
                continue
            else:
                # subset to the time_range
                time_series.data = time_series.data.loc[ \
                    (time_series.data.index >= time_range[0]) & \
                    (time_series.data.index <= time_range[-1]), :]
                # remove problematic flagged points and check criteria (2.2)
                # (2.3)
                months_avail2 = ismn_availability(time_series.data,
                                                  aggregate = False)

                if type(months_avail2) == type(None):
                    continue # No quality data.

                if len(months_avail2) <= 1:
                    continue # Too little data fits the criteria.

                # make index "time aware"
                months_avail2.index = months_avail2.index.tz_localize(tz='UTC')
                months_avail = pd.Series(False, index=time_range_by_month)
                months_avail.loc[months_avail2.index] = months_avail2

                # exclude stations that have zero or only one monthly record
                if sum(months_avail) <= 1:
                    continue

            # Fill in the metadata table.
            network_metadata.loc[count_inner, 'Network'] = network
            network_metadata.loc[count_inner, 'Station'] = st

            network_metadata.loc[count_inner, 'Lat'] = time_series.latitude
            network_metadata.loc[count_inner, 'Lon'] = time_series.longitude

            # ---- Need to address the situation where depths_from[ii] 
            #      equals depths_to[ii] equals a target depth.
            #      Attach to the shallower set of depths.
            a_ind = max(np.where(target_depth_from <= depths_from[ii])[0])
            b_ind = min(np.where(target_depth_to >= depths_to[ii])[0])
            if target_depth_from[a_ind] == target_depth_to[b_ind]:
                a_ind = a_ind -1
            network_metadata.loc[count_inner, 'Depth-Fit'] = '%.2f-%.2f' % \
                (target_depth_from[a_ind], target_depth_to[b_ind])

            network_metadata.loc[count_inner, 'Depth-Actual'] = \
                '%.2f-%.2f' % (depths_from[ii], depths_to[ii])
            network_metadata.loc[count_inner, 'Sensor'] = srs

            network_metadata.loc[count_inner, 'Land Cover 2000'] = \
                station_obj.landcover_2000
            network_metadata.loc[count_inner, 'Land Cover 2005'] = \
                station_obj.landcover_2005
            network_metadata.loc[count_inner, 'Land Cover 2010'] = \
                station_obj.landcover_2010
            network_metadata.loc[count_inner, 'Land Cover in situ'] = \
                station_obj.landcover_insitu
            network_metadata.loc[count_inner, 'Climate Zone'] = \
                station_obj.climate

            network_metadata.loc[count_inner, months_avail.index] = \
                months_avail

            count_inner += 1

    if isinstance(network_metadata.iloc[0,0], float) and \
       np.isnan(network_metadata.iloc[0,0]):
        # no data filled
        network_metadata = None
    else:
        # edit the time label for better human readability
        network_metadata.columns =  ['Network', 'Station', 'Lat', 'Lon', 
            'Depth-Fit', 'Depth-Actual', 'Sensor', 'Land Cover 2000',
            'Land Cover 2005', 'Land Cover 2010', 'Land Cover in situ',
            'Climate Zone'] + list(time_range_by_month.strftime('%Y-%m-%d'))

    ##return network_metadata ## Uncomment this line if in function.
    ###########################################################################
    # End of bracket.
    ###########################################################################


    ###########################################################################
    # Write the metadata to file.
    ###########################################################################
    if not isinstance(network_metadata, type(None)):
        # append if not the first visited network
        if count_outer==0:
            f = open(os.path.join(mg.path_intrim_out(), 'ismn_metadata', 
                                  'metadata.csv'), 'w')
            network_metadata.to_csv(f, header=True, index=False)
            f.close()
        else:
            f = open(os.path.join(mg.path_intrim_out(), 'ismn_metadata', 
                                  'metadata.csv'), 'a')
            network_metadata.to_csv(f, header=False, index=False)
            f.close()
        count_outer += 1
