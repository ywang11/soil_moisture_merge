# -*- coding: utf-8 -*-
"""
Created on Thu May 2 15:57:49 2019

@author: ywang254

Source: 
  Hobeichi et al. - 2018 - Derived Optimal Linear Combination 
    Evapotranspiration (DOLCE) A Global Gridded Synthesis Estimate

Use the weights derived from "dolce_products_weights.py" to calculate the final
  weighted average of the products.
"""
import os
import utils_management as mg
import pandas as pd
from utils_management.constants import depth, depth_cm, year_longest
from misc.ismn_utils import get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
import xarray as xr


# Choose the weighting method that was used to generate the weighted averaged
# ISMN data.
##from ismn_aggr_dolce import simple, dominance_lc, dominance_threshold
ismn_aggr_ind = REPLACE1
simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = get_ismn_aggr_method(simple[ismn_aggr_ind],
                                        dominance_lc[ismn_aggr_ind],
                                        dominance_threshold)


year = year_longest
time_range = pd.date_range(start=str(year[0])+'-01-01',
                           end=str(year[-1])+'-12-31', freq='MS')
year_str = str(year[0]) + '-' + str(year[-1])


# pick the covariance function based on which the weights are calculated
cov_estimer = REPLACE2 ## [0,1,2,3,4]
cov_method = get_cov_method(cov_estimer)

remove_mean = True

land_mask = 'vanilla'

i = REPLACE3 # Need this much disaggregation because takes a long time to
             # generate results.
d = depth[i]
dcm = depth_cm[i]


# The source products to merge. Note: the lsm uses the concated product.
dolce_method = {'dolce_lsm': os.path.join(mg.path_out(), 
                                          'concat_dolce_lsm',
                                          'concat_average_' + d + '_' + \
                                          ismn_aggr_method + \
                                          '_ShrunkCovariance.nc'),
                'dolce_cmip5': os.path.join(mg.path_out(), 
                                            'dolce_cmip5_product',
                                            'weighted_average_' + \
                                            year_str + '_' + d + '_' + \
                                            ismn_aggr_method + \
                                            '_ShrunkCovariance.nc'),
                'dolce_cmip6': os.path.join(mg.path_out(),
                                            'dolce_cmip6_product',
                                            'weighted_average_' + \
                                            year_str + '_' + d + '_' + \
                                            ismn_aggr_method + \
                                            '_ShrunkCovariance.nc')}


em_method = {'em_lsm': os.path.join(mg.path_out(), 'concat_em_lsm',
                                    'concat_CRU_v4.03_year_month_' + \
                                    'anomaly_9grid_' + dcm + '_' + \
                                    'predicted_' + year_str + '.nc'),
             'em_cmip5': [os.path.join(mg.path_out(), 'em_cmip5_corr',
                                       'CRU_v4.03_year_month_restored_9grid'+ \
                                       '_' + dcm + '_' + year_str, 
                                       'predicted_' + str(yr) + '.nc') for \
                          yr in year], 
             'em_cmip6': [os.path.join(mg.path_out(), 'em_cmip6_corr',
                                       'CRU_v4.03_year_month_restored_9grid'+ \
                                       '_' + dcm + '_' + year_str, 
                                       'predicted_' + str(yr) + '.nc') for \
                          yr in year]}


#######################################################################
# Load the weights.
#######################################################################
w_file = os.path.join(mg.path_intrim_out(), 'dolce_products_weights',
                      'weights_' + ismn_aggr_method + '_' + year_str + \
                      '_' + cov_method + '_' + d)
if remove_mean:
    w_file = w_file + '_remove_mean.csv'
else:
    w_file = w_file + '.csv'
weights = pd.read_csv(w_file, index_col = 0)


#######################################################################
# Calculate the weighted average and save to file.
#######################################################################
for l_ind, l in enumerate(dolce_method.keys()):
    data = xr.open_dataset(dolce_method[l], decode_times = False)
    if l_ind == 0:
        weighted_sm = data.sm.copy(deep=True)*weights.loc[l,'Weight']
    else:
        weighted_sm = data.sm.copy(deep=True)*weights.loc[l,'Weight'] + \
                      weighted_sm.values
    data.close()
for l_ind, l in enumerate(em_method.keys()):
    if l == 'em_lsm':
        data = xr.open_dataset(em_method[l], decode_times = False)
    else:
        data = xr.open_mfdataset(em_method[l], decode_times = False,
                                 concat_dim = 'time')

    weighted_sm = weighted_sm + data['predicted'].values * \
                  weights.loc[l, 'Weight']
    data.close()

wsmfile = os.path.join(mg.path_out(), 'dolce_products_product', 
                       'weighted_average_' + year_str + '_' + d + '_' + \
                       ismn_aggr_method + '_' + cov_method + '.nc')
# ---- convert from DataArray to Dataset and save to netcdf
weighted_sm.to_dataset(name = 'sm').to_netcdf(wsmfile)
