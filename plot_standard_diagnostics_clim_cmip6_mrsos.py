"""
Draw the global mean soil moisture map of the CMIP6 models at the 
interpolated 0.5 degrees resolution.
"""
import os
import pandas as pd
from utils_management.constants import year_cmip6, cmip6_list
import utils_management as mg
import xarray as xr
import numpy as np
from misc.plot_utils import plot_sm_map
import matplotlib.pyplot as plt
import cartopy.crs as ccrs


year = year_cmip6 # [year_cmip5, year_cmip6]

land_mask = 'SYNMAP'

act = 'CMIP'
expr = 'historical'
var = 'mrsos'
tid = 'Lmon'

model_version = cmip6_list(act, expr, var, tid)


# A subset of 3-4 models
##model_version = model_version[(REPLACE*3):(REPLACE*3+3)]


# Graph setup.
levels = np.linspace(0., 0.6, 15)
cmap = 'RdYlBu'



for m_v in model_version[149:]:
    print(m_v)

    prefix = var + '_' + expr + '_' + m_v + '_' + str(year[0]) + '-' + \
             str(year[-1])

    data = xr.open_dataset(os.path.join(mg.path_out(),
                                        'standard_diagnostics_cmip6', 
                                        prefix + '_g_map_annual.nc'))

    # Plot the climatology
    fig, ax = plt.subplots(subplot_kw = {'projection': \
                                         ccrs.PlateCarree()},
                           figsize = (8,6))
    plot_sm_map(ax, data['g_map'].values, data.lat, data.lon, add_cyc = True,
                levels = levels)
    fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                             'mean_cmip6_mrsos', prefix + '.png'), dpi = 600.)
    data.close()
    plt.close(fig)
