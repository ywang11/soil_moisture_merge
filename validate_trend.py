"""
2020/12/06

Trend of the products, evaluation datasets, raw datasets.
"""
import os
import xarray as xr
import pandas as pd
import numpy as np
import utils_management as mg
from utils_management.constants import depth, depth_cm, lsm_list, \
    year_longest, year_shorter2, year_shorter, year_shortest
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability, one_layer_availability
from glob import glob
import itertools as it
from scipy.stats import linregress
from time import time
import multiprocessing as mp
from dateutil.relativedelta import relativedelta


def decode_month_since(time):
    ref = pd.Timestamp(time.attrs['units'].split(' ')[2])
    start = ref + relativedelta(months = time.values[0])
    start = start.replace(day = 1, hour = 0, minute = 0, second = 0)
    return pd.date_range(start, periods = len(time), freq = 'MS')


def get_sm(flist):
    hr = xr.open_mfdataset(flist, decode_times = False)
    try:
        sm = hr['sm']
    except:
        sm = hr['predicted']
    if ('CRU_v4.03_year_month_positive_9grid_0-10cm_1950-2016' in flist[0]) |\
       ('CRU_v4.03_year_month_positive_9grid_10-30cm_1950-2016' in flist[0]) |\
       ('CRU_v4.03_year_month_positive_9grid_30-50cm_1950-2016' in flist[0]) |\
       ('CRU_v4.03_year_month_positive_9grid_50-100cm_1950-2016' in flist[0]):
        sm['time'].attrs['units'] = 'months since 1950-01-01'
    if 'month' in sm['time'].attrs['units']:
        tvec = decode_month_since(sm['time'])
    else:
        tvec = xr.decode_cf(hr)['time'].to_index()
    hr.close()
    return sm, tvec


def calc_trend(vector, order):
    x = np.arange( len(vector) )
    y = vector
    temp = ~(np.isnan(x) | np.isnan(y))
    x = x[temp]
    y = y[temp]
    slope, _, _, slope_p, _ = linregress(x, y)
    return [order, slope, slope_p]


def get_trend(sm, tvec, yrng):
    start = time()

    lat = sm['lat']
    lon = sm['lon']
    sm = sm[(tvec.year >= yrng[0]) & (tvec.year <= yrng[-1]), :, :].values

    retain = np.where((~np.isnan(np.nanmean(sm, axis = 0))).reshape(-1))[0]
    sm_temp = np.array_split(sm.reshape(-1, sm.shape[1]*sm.shape[2] \
    )[:, retain], len(retain), axis = 1)

    p = mp.Pool(4)
    result = [p.apply_async(calc_trend, args = (smt.reshape(-1), order)) \
              for order,smt in enumerate(sm_temp)]
    p.close()
    p.join()

    result = [rr.get() for rr in result]

    trend = np.full(sm.shape[1] * sm.shape[2], np.nan)
    trend_p = np.full(sm.shape[1] * sm.shape[2], np.nan)
    for rr in range(len(result)):
        trend[retain[result[rr][0]]] = result[rr][1]
        trend_p[retain[result[rr][0]]] = result[rr][2]

    trend = xr.DataArray(trend.reshape(sm.shape[1], sm.shape[2]),
                         dims = ['lat', 'lon'],
                         coords = {'lat': lat, 'lon': lon})
    trend_p = xr.DataArray(trend_p.reshape(sm.shape[1], sm.shape[2]),
                           dims = ['lat', 'lon'],
                           coords = {'lat': lat, 'lon': lon})
    end = time()

    print(end - start)

    return trend, trend_p


#
land_mask = 'vanilla'
year_list = {'SMOS_L3': range(2010, 2017),
             'SMOS_L4': range(2010, 2017),
             'SMERGE_v2': range(1981, 2017), # for compatibility with shorter2
             'GLEAMv3.3a': range(1981, 2017), # for compatibility with shorter2
             'SoMo': range(2000,2017)}

# 'GLEAMv3.3a', 'SMOS_L3', 'SMOS_L4', 'SMERGE_v2', 'SoMo'
for name in []:
    if name == 'SoMo':
        for layer in ['0-10cm', '10-30cm', '30-50cm']:
            flist = sorted(glob(os.path.join(mg.path_intrim_out(),
                                             'Interp_Merge',
                                             land_mask, name,
                                             name + '_*_' + layer + '.nc')))
            sm, tvec = get_sm(flist)
            sm_trend, sm_trend_p = get_trend(sm, tvec, year_list[name])
            xr.Dataset({'trend': sm_trend, 'trend_p': sm_trend_p} \
            ).to_netcdf(os.path.join(mg.path_out(), 'validate',
                                     'trend',
                                     name + '_' + layer + '.nc'))
    else:
        if name == 'GLEAMv3.3a':
            flist = sorted(glob(os.path.join(mg.path_intrim_out(),
                                             'Interp_Merge',
                                             land_mask, name,
                                             name + '_*_0-100cm.nc')))
            sm, tvec = get_sm(flist)
            sm_trend, sm_trend_p = get_trend(sm, tvec, year_list[name])
        elif name == 'SMOS_L3':
            # ascending orbit
            flist = sorted(glob(os.path.join(mg.path_intrim_out(),
                                             'Interp_Merge',
                                             land_mask, name,
                                             name + '_*_CLF3MA.nc')))
            sm_a, tvec = get_sm(flist)
            # descending orbit
            flist = sorted(glob(os.path.join(mg.path_intrim_out(),
                                             'Interp_Merge',
                                             land_mask, name,
                                             name + '_*_CLF3MD.nc')))
            sm_d, tvec = get_sm(flist)
            #
            sm = (sm_a + sm_d) / 2
            sm_trend, sm_trend_p = get_trend(sm, tvec, year_list[name])
        elif (name == 'SMOS_L4') | (name == 'SMERGE_v2'):
            flist = sorted(glob(os.path.join(mg.path_intrim_out(),
                                             'Interp_Merge',
                                             land_mask, name, name + '*.nc')))
            sm, tvec = get_sm(flist)
            sm_trend, sm_trend_p = get_trend(sm, tvec, year_list[name])

        if name == 'GLEAMv3.3a':
            xr.Dataset({'trend': sm_trend, 'trend_p': sm_trend_p} \
            ).to_netcdf(os.path.join(mg.path_out(), 'validate',
                                     'trend', name + '_0-100cm.nc'))
        else:
            xr.Dataset({'trend': sm_trend, 'trend_p': sm_trend_p} \
            ).to_netcdf(os.path.join(mg.path_out(), 'validate',
                                     'trend', name + '.nc'))


#
for dind, d in enumerate(depth):
    dcm = depth_cm[dind]
    lsm_temp = lsm_list[(str(year_shorter2[0]) + '-' + \
                         str(year_shorter2[-1]), d)]
    cmip5_temp = cmip5_availability(dcm, land_mask)
    cmip6_temp = sorted(set(mrsol_availability(dcm, land_mask, 'historical')) \
                        & set(mrsol_availability(dcm, land_mask, 'ssp585')))

    if dind == 0:
        lsm = lsm_temp
        cmip5 = cmip5_temp
        cmip6 = cmip6_temp
    else:
        lsm = sorted(set(lsm) & set(lsm_temp))
        cmip5 = sorted(set(cmip5) & set(cmip5_temp))
        cmip6 = sorted(set(cmip6) & set(cmip6_temp))


# range(2010, 2017), 
year_list_list = [range(1981, 2017), range(2000, 2017)]
# 'lsm', 'cmip5', 'cmip6', 'mean_lsm', 'dolce_lsm', 'em_lsm',
#             'em_cmip5', 'em_cmip6', 'em_2cmip', 'em_all'
for mset in ['em_lsm', 'em_cmip5', 'em_cmip6', 'em_2cmip', 'em_all']:
    if mset == 'lsm':
        model_list = lsm
    elif mset == 'cmip5':
        model_list = cmip5
    elif mset == 'cmip6':
        model_list = cmip6
    else:
        model_list = [mset]
    for model, yind in it.product(model_list, range(len(year_list_list))):
        for dind, dcm in enumerate(depth_cm):
            d = depth[dind]
            year = year_list_list[yind]
            year_str = str(year[0]) + '-' + str(year[-1])

            if mset == 'lsm':
                flist = sorted(glob(os.path.join(mg.path_intrim_out(),
                                                 'Interp_Merge', land_mask,
                                                 model, model + \
                                                 '_*_' + dcm + '.nc')))
            elif mset == 'cmip5':
                flist = [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                      land_mask, 'CMIP5', model,
                                      'sm_historical_r1i1p1_' + \
                                      str(yy) + '_' + dcm + '.nc') \
                         for yy in range(year[0], 2006)] + \
                        [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                      land_mask, 'CMIP5', model,
                                      'sm_rcp85_r1i1p1_' + str(yy) +  '_' + \
                                      dcm + '.nc') \
                         for yy in range(2006, year[-1] + 1)]
            elif mset == 'cmip6':
                flist = [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                      land_mask, 'CMIP6', model,
                                      'mrsol_historical_' + str(yy) + '_' + \
                                      dcm + '.nc') \
                         for yy in range(year[0], 2015)] + \
                        [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                      land_mask, 'CMIP6', model,
                                      'mrsol_ssp585_' + str(yy) + '_' + \
                                      dcm + '.nc') \
                         for yy in range(2015, year[-1] + 1)]
            elif mset == 'mean_lsm':
                flist = [os.path.join(mg.path_out(),
                                      'meanmedian_lsm',
                                      'mean_' + d + '_' + \
                                      str(year_longest[0]) + '-' + \
                                      str(year_longest[-1]) + '.nc')]
            elif mset == 'dolce_lsm':
                flist = [os.path.join(mg.path_out(),
                                      'concat_dolce_lsm',
                                      'positive_average_' + d + \
                                      '_lu_weighted_ShrunkCovariance.nc')]
            elif (mset == 'em_all') | (mset == 'em_lsm'):
                flist = [os.path.join(mg.path_out(),
                                      'concat_' + mset,
                                      'positive_CRU_v4.03' + \
                                      '_year_month_anomaly_9grid_' + \
                                      dcm + '_predicted_' + \
                                      str(year_longest[0]) + '-' + \
                                      str(year_longest[-1]) + '.nc')]
            else:
                flist = [os.path.join(mg.path_out(),
                                      model + '_corr',
                                      'CRU_v4.03_year_month_positive_' + \
                                      '9grid_' + dcm + '_' + \
                                      str(year_longest[0]) + \
                                      '-' + str(year_longest[-1]),
                                      'predicted.nc')]
            sm, tvec = get_sm(flist)
            if dcm != '50-100cm':
                sm_trend, sm_trend_p = get_trend(sm, tvec, year)
                xr.Dataset({'trend': sm_trend, 'trend_p': sm_trend_p} \
                ).to_netcdf(os.path.join(mg.path_out(), 'validate',
                                         'trend', mset + '_' + \
                                         model + '_' + dcm + '_' + \
                                         year_str + '.nc'))

            if dind == 0:
                sm_tot = sm * 0.1
            elif (dind == 1) | (dind == 2):
                sm_tot += sm * 0.2
            else:
                sm_tot += sm * 0.5
                sm_trend, sm_trend_p = get_trend(sm_tot, tvec, year)
                xr.Dataset({'trend': sm_trend, 'trend_p': sm_trend_p} \
                ).to_netcdf(os.path.join(mg.path_out(), 'validate',
                                         'trend', mset + '_' + model + \
                                         '_0-100cm_' + \
                                         year_str + '.nc'))

            if dind == 0:
                sm_tot2 = sm * 0.1
            elif dind == 1:
                sm_tot2 += sm * 0.2
            elif dind == 2:
                sm_tot2 += sm * 0.1
                sm_tot2 = sm_tot2 / 0.4
                sm_trend, sm_trend_p = get_trend(sm_tot2, tvec, year)
                xr.Dataset({'trend': sm_trend, 'trend_p': sm_trend_p} \
                ).to_netcdf(os.path.join(mg.path_out(), 'validate',
                                         'trend', mset + '_' + model + \
                                         '_0-40cm_' + year_str + '.nc'))
