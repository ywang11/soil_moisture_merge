"""
2020/08/04
ywang254@utk.edu

Overlap the merged minimum land mask of the soil moisture product with the 
 individual validation products.
"""
import sys
import os
import xarray as xr
import numpy as np
from glob import glob
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import utils_management as mg
from utils_management.constants import year_cmip6, val_list, val_depth


land_mask = 'vanilla'
hr = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'land_mask_minimum',
                                  'merge_' + land_mask + '.nc'))
mask_min = hr['mask'].copy(deep = True)
hr.close()


for vind, vv in enumerate(val_list):
    for dd in val_depth[vv]:
        if vv == 'scPDSI':
            nout = vv
        else:
            nout = vv + '_' + dd
        if (vv == 'GLEAMv3.3a') & (dd == '0-10cm'):
            continue # already created elsewhere

        hr = xr.open_mfdataset(os.path.join(mg.path_intrim_out(),
                                            'Interp_Merge', 'None', vv,
                                            '*' + dd + '.nc'),
                               decode_times = False, concat_dim = 'time')
        original = hr['sm'].copy(deep = True)
        merged = original.where(mask_min)
        hr.close()

        merged.to_dataset(name = 'sm' \
        ).to_netcdf(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                 land_mask, vv, nout + '.nc'))
    
        fig, axes = plt.subplots(2, 1,
            subplot_kw = {'projection': ccrs.PlateCarree()})
        ax = axes[0]
        cf = ax.contourf(original.lon.values, original.lat.values,
                         original.mean(dim = 'time'), cmap = 'Spectral',
                         vmin = np.nanmin(original.mean(dim = 'time').values),
                         vmax = np.nanmax(original.mean(dim = 'time').values))
        ax = axes[1]
        ax.contourf(merged.lon.values, merged.lat.values,
                    merged.mean(dim = 'time'), cmap = 'Spectral',
                    vmin = np.nanmin(original.mean(dim = 'time').values),
                    vmax = np.nanmax(original.mean(dim = 'time').values))
    
        cax = fig.add_axes([0.1, 0., 0.8, 0.01])
        plt.colorbar(cf, cax = cax, orientation = 'horizontal')
    
        fig.savefig(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                 land_mask, vv, nout + '.png'), dpi = 300.,
                    bbox_inches = 'tight')
        plt.close(fig)
