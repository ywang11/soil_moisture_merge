# -*- coding: utf-8 -*-
"""
Created on Tue May 21 17:58:33 2019

@author: ywang254

Plot the summarized evaluation metrics by climate zone, land use, and season.
Show the uncertainty envelope.
"""
import pandas as pd
import utils_management as mg
from utils_management.constants import depth, year_longest, clim_zone, \
    lu_names, lsm_list
import os
import itertools as it
import time
import matplotlib.pyplot as plt
##from matplotlib import cm
import numpy as np


def cal_vals_plot(ax, input_data):
    """
    Convenience function for the first section.
    """
    input_data.columns = input_data.columns.droplevel([0,2])
    input_data.index = input_data.index.droplevel(2)
    input_data = input_data.stack().unstack().unstack()
    val_max = np.nanmax(input_data.loc[:, ('Max', slice(None))], axis=1)
    val_min = np.nanmin(input_data.loc[:, ('Min', slice(None))], axis=1)
    val_mean = np.nanmax(input_data.loc[:, ('Mean', slice(None))], axis=1)
    val_median = np.nanmax(input_data.loc[:, ('Median', slice(None))], axis=1)
    h = [None] * 4
    h[0], = ax.plot(range(val_max.shape[0]), val_max, '--', 
                    color='black', markerfacecolor='black',
                    label='Max')
    h[1], = ax.plot(range(val_mean.shape[0]), val_mean, '-', 
                    color='blue', markerfacecolor='blue',
                    label='Mean')
    h[2], = ax.plot(range(val_min.shape[0]), val_min, '--', 
                    color='black', markerfacecolor='black',
                    label='Min')
    h[3], = ax.plot(range(val_median.shape[0]), val_median, '-o', 
                    color='black', markerfacecolor='black',
                    label='Median')
    return h


def cal_vals_plot2(ax, input_data):
    """
    Convenience function for the second section.
    """
    input_data.columns = input_data.columns.droplevel([0,2])
    input_data.index = input_data.index.droplevel(0)
    input_data = input_data.unstack()
    val_max = np.nanmax(input_data.loc[:, ('Max', slice(None))], axis=1)
    val_min = np.nanmin(input_data.loc[:, ('Min', slice(None))], axis=1)
    val_mean = np.nanmax(input_data.loc[:, ('Mean', slice(None))], axis=1)
    val_median = np.nanmax(input_data.loc[:, ('Median', slice(None))], axis=1)
    h = [None] * 4
    h[0], = ax.plot(range(val_max.shape[0]), val_max, '--', 
                    color='black', markerfacecolor='black',
                    label='Max')
    h[1], = ax.plot(range(val_mean.shape[0]), val_mean, '-', 
                    color='blue', markerfacecolor='blue',
                    label='Mean')
    h[2], = ax.plot(range(val_min.shape[0]), val_min, '--', 
                    color='black', markerfacecolor='black',
                    label='Min')
    h[3], = ax.plot(range(val_median.shape[0]), val_median, '-o', 
                    color='black', markerfacecolor='black',
                    label='Median')
    return h


start = time.time()


year = year_longest
season = ['DJF', 'MAM', 'JJA', 'SON']
metric_name = ['RMSE', 'Bias', 'uRMSE', 'Corr']
value = ['Max', 'Mean', 'Median', 'Min']
lu_list = list(lu_names.values())

##cmap = cm.get_cmap('Spectral')
##color = [cmap(x) for x in np.arange(.5/len(value), 1., 1./len(value))]
color = ['gray', 'blue', 'black', 'gray']


###############################################################################
# Simple plot that separate the climate zones or land use, and each by season,
# but not the interactions between climate zones and land use.
###############################################################################
for i, d in enumerate(depth):
    # Preliminary constants.
    if i==0:
        gridded_datasets = ['weighted', 'average', 'median', 'satellite'] + \
                           lsm_list[(str(year[0])+'-'+str(year[-1]), d)]
    else:
        gridded_datasets = ['weighted', 'average', 'median'] + \
                           lsm_list[(str(year[0])+'-'+str(year[-1]), d)]


    metric = pd.read_csv(os.path.join(mg.path_out(), 
                         'analyze_lsm_and_satellite_against_obs', 
                         'metrics_' + d + '.csv'), 
                         index_col=[0,1,2], header=[0,1,2])
    metric.sort_index(axis=0, level=2, inplace=True)
    metric.sort_index(axis=1, level=1, inplace=True)


    ###########################################################################
    ncols = 2 ## 3
    nrows = 2 ## int(np.ceil(len(clim_zone)/ncols))
    fig, axes = plt.subplots(nrows=nrows, ncols=ncols, figsize=(12,12), 
                             sharex=True, sharey=True)
    for me, ds in it.product(metric_name, gridded_datasets):
        ylim = [np.nanmin(metric.loc[:, (ds, slice(None), me) \
                                    ].values.reshape(-1)), 
                np.nanmax(metric.loc[:, (ds, slice(None), me) \
                                    ].values.reshape(-1))]

        separation = ['climate_zone', 'land_use']
        for sep in separation:
            for j,ss in enumerate(season):
                ax = axes.flat[j]
                ax.cla()
                input_data = metric.loc[(slice(None), slice(None), ss), 
                                        (ds, slice(None), me)].copy()
                if sep == 'land_use':
                    input_data = input_data.swaplevel(i=1,j=0,axis=0)
                h = cal_vals_plot(ax, input_data)
                if sep == 'climate_zone':
                    ax.set_xticks(range(len(clim_zone)))
                    if (len(season)-j) <= ncols:
                        ax.set_xticklabels(clim_zone, rotation=90)
                    else:
                        ax.set_xticklabels([])
                    ax.set_xlim([-0.5, len(clim_zone)+0.5])
                else:
                    ax.set_xticks(range(len(lu_list)))
                    if (len(season)-j) <= ncols:
                        ax.set_xticklabels(lu_list, rotation=90)
                    else:
                        ax.set_xticklabels([])
                    ax.set_xlim([-0.5, len(lu_list)+0.5])

                ax.set_title(ss)
                ax.set_ylim(ylim)
            ax.legend(h, ncol = 1) # bbox_to_anchor = [0., -0.5], 
            fig.savefig(os.path.join(mg.path_out(),
                                     'analyze_lsm_and_satellite_against_obs',
                                     'plot_' + sep, me + '_' + d + '_' + \
                                     ds + '.png'), dpi=600.)
            plt.close(fig)


###############################################################################
# Simple plot that separate the climate zones and land use, with interactions, 
# but not separate by season.
###############################################################################
for i, d in enumerate(depth):
    # Preliminary constants.
    if i==0:
        gridded_datasets = ['weighted', 'average', 'median', 'satellite'] + \
                           lsm_list[(str(year[0])+'-'+str(year[-1]), d)]
    else:
        gridded_datasets = ['weighted', 'average', 'median'] + \
                           lsm_list[(str(year[0])+'-'+str(year[-1]), d)]

    metric = pd.read_csv(os.path.join(mg.path_out(), 
                          'analyze_lsm_and_satellite_against_obs', 
                          'metrics_' + d + '.csv'), 
                          index_col=[0,1,2], header=[0,1,2])
    metric.sort_index(axis=0, level=2, inplace=True)
    metric.sort_index(axis=1, level=1, inplace=True)

    ###########################################################################
    ncols = 3
    nrows = int(np.ceil(len(clim_zone)/ncols))
    fig, axes = plt.subplots(nrows=nrows, ncols=ncols, figsize=(12,16), 
                             sharex=True, sharey=True)
    for me, ds in it.product(metric_name, gridded_datasets):
        ylim = [np.nanmin(metric.loc[:, (slice(None), slice(None), 
                                         me)].values.reshape(-1)), 
                np.nanmax(metric.loc[:, (slice(None), slice(None), 
                                         me)].values.reshape(-1))]
        for j, cz in enumerate(clim_zone):
            ax = axes.flat[j]
            ax.cla()
            h = cal_vals_plot2(ax,
                               metric.loc[(cz, slice(None), slice(None)), 
                                          (ds, slice(None), me)])
            ax.set_xticks(range(len(lu_list)))
            ##if  (len(clim_zone)-j) <= ncols:
            if (len(season)-j) <= ncols:
                ax.set_xticklabels(lu_list, rotation=90)
            else:
                ax.set_xticklabels([])
            ax.set_title(cz)
            ax.set_ylim(ylim)
            ax.set_xlim([-0.5, len(lu_list)+0.5])
        for k in range(j+1, nrows*ncols):
            axes.flat[k].axis('off')
        ax.legend(h, ncol = 1, bbox_to_anchor = [1.8, -0.5])
        fig.savefig(os.path.join(mg.path_out(),
                                 'analyze_lsm_and_satellite_against_obs',
                                 'plot_climate_zone_x_land_use', 
                                 me + '_' + d + '_' + ds + \
                                 '.png'), dpi=600.)
        plt.close(fig)


###############################################################################
# Detailed plot that separate the climate zones, land use, and season.
###############################################################################
for i, d in enumerate(depth):
    # Preliminary constants.
    if i==0:
        gridded_datasets = ['weighted', 'average', 'median', 'satellite'] + \
                           lsm_list[(str(year[0])+'-'+str(year[-1]), d)]
    else:
        gridded_datasets = ['weighted', 'average', 'median'] + \
                           lsm_list[(str(year[0])+'-'+str(year[-1]), d)]


    metric = pd.read_csv(os.path.join(mg.path_out(), 
                          'analyze_lsm_and_satellite_against_obs', 
                          'metrics_' + d + '.csv'), 
                          index_col=[0,1,2], header=[0,1,2])
    metric.sort_index(axis=0, level=2, inplace=True)
    metric.sort_index(axis=1, level=1, inplace=True)


    ###########################################################################
    ###########################################################################
    ncols = 2 ## 3
    nrows = 2 ## int(np.ceil(len(clim_zone)/ncols))
    fig, axes = plt.subplots(nrows=nrows, ncols=ncols, figsize=(12,12), 
                             sharex=True, sharey=True)
    for me, ds, cz in it.product(metric_name, gridded_datasets, clim_zone):
        ylim = [np.nanmin(metric.loc[:, (slice(None), slice(None), 
                                         me)].values.reshape(-1)), 
                np.nanmax(metric.loc[:, (slice(None), slice(None), 
                                         me)].values.reshape(-1))]
        for j,ss in enumerate(season):
            ax = axes.flat[j]
            ax.cla()
            for k,lu in enumerate(lu_list):
                h = []
                for r,v in enumerate(value):
                    hl, = ax.plot(k, metric.loc[(cz, lu, ss), (ds, v, me)], 
                                                'o', color=color[r], 
                                                markerfacecolor=color[r],
                                  label=v)
                    h.append(hl)
            ax.set_xticks(range(k))
            ##if  (len(clim_zone)-j) <= ncols:
            if (len(season)-j) <= ncols:
                ax.set_xticklabels(lu_list, rotation=90)
            else:
                ax.set_xticklabels([])
            ax.set_title(ss)
            ax.set_ylim(ylim)
            ax.set_xlim([-0.5, len(lu_list)+0.5])
        ##for k in range(j+1, nrows*ncols):
        ##    axes.flat[k].axis('off')
        ax.legend(h, ncol = 1, bbox_to_anchor = [1.8, -0.5])
        fig.savefig(os.path.join(mg.path_out(),
                                 'analyze_lsm_and_satellite_against_obs',
                                 'plot_' + me, 'metric_' + d + '_' + ds \
                                 + '_' + cz + '.png'), dpi=600.)
    plt.close(fig)


end = time.time()
print('The script finished in ' + ('%.4f' % ((end-start)/3600) ) + ' hours.')