"""
2019/07/06

ywang254@utk.edu

Plot the by-latitude mean, annual time series for all the methods:
    - Mean
    - Median
    - DOLCE_LSM (lu_weighted, ShrunkCovariance)
    - EM_LSM (CRU_Merged, month_anomaly_1grid)
    - EM_CMIP5 (CRU_Merged, month_anomaly_1grid)
    - EM_CMIP6 (CRU_Merged, month_anomaly_1grid)
    - EM_DOLCE (tbc!!!)

with stipples that indicate > 50% models agree.
"""
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth, depth_cm, lsm_list, year_cmip5, \
    year_cmip6
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import utils_management as mg
import numpy as np
import xarray as xr
import pandas as pd
import os
import sys
import matplotlib.pyplot as plt
from matplotlib import cm
from misc.plot_utils import plot_ts_trend, plot_ts_shade, stipple
from misc.plot_standard_diagnostics_utils import gather_ts
import itertools as it


# MODIFY
model_set = 'lsm'
method = 'year_month_9grid' # year_month_1grid, year_month_9grid,
                            # month_1grid, month_anomaly_1grid
pr_obs = 'CRU_Merged' # CRU_v4.03, GPCC, UDEL
year = year_longest # year_longest, year_shorter, year_shorter2, year_shortest,
                    # year_cmip5, year_cmip6
year_str = str(year[0]) + '-' + str(year[-1])
land_mask = 'vanilla'


# Read the source dataset.
ts_product = {}
for i,d in enumerate(depth):
    dcm = depth_cm[i]

    # Get data.
    data = pd.read_csv(os.path.join(mg.path_out(), 
                                    'standard_diagnostics_em_' + \
                                    model_set, pr_obs + '_predicted_' + \
                                    method + '_' + year_str + '_' + d + \
                                    '_g_lat_ts.csv'), 
                       index_col = 0, parse_dates = True)

    # Convert to annual average and remove the by-latitude climatology.
    data = data.groupby(data.index.year).mean()
    data = (data - data.mean(axis = 0))

    # Remove NaN latitudes.
    data.dropna(axis = 1, how = 'all', inplace = True)

    ts_product[dcm] = data.T.loc[::-1, :].stack()
ts_product = pd.DataFrame.from_dict(ts_product)
# ---- Some manuvers to make the correct format.
ts_product = ts_product.reorder_levels([1,0]).unstack()
# ---- Remove climatology.
ts_product = (ts_product - ts_product.mean(axis = 0)).T


# Plot the results.
fig, axes = plt.subplots(figsize = (6, 8), nrows = len(depth_cm), 
                         ncols = 1)
fig.subplots_adjust(hspace = 0.)

for d_ind,dcm in enumerate(depth_cm):
    ax = axes.flat[d_ind]
    h = ax.imshow(ts_product.loc[dcm, :], 
                  cmap = 'coolwarm', vmin = -0.01, 
                  vmax = 0.01, aspect=0.1)
    ax.set_xticks(range(0, ts_product.shape[1], 10))
    ax.set_xlim([-0.5, ts_product.shape[1]-0.5])

    if d_ind == len(depth_cm)-1:
        ax.set_xticklabels(ts_product.columns[::10])
    else:
        ax.set_xticklabels([])

    ax.set_yticks(range(60, ts_product.shape[0] // len(depth_cm), 60))
    ax.set_yticklabels([('%.2f' % x) for x in \
                        np.arange(-17.75, 76.25, 0.5*60)])
    ax.set_ylim([-0.5, ts_product.shape[0] // len(depth_cm) - 0.5])
    ax.set_ylabel(dcm)
cb_ax = fig.add_axes([0.2, 0.02, 0.6, 0.02])
cbar = fig.colorbar(h, cax = cb_ax, boundaries = np.linspace(-0.01, 0.01, 11), 
                    orientation = 'horizontal', pad = 0.8)
ax.set_xlabel('Year')
fig.text(-0.01, 0.5, 'Latitude', rotation = 90)
fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot', 
                         'ts_annual', 'by_lat_em_' + model_set + '_' + \
                         pr_obs + '_' + method + '_' + year_str + '.png'), 
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)
