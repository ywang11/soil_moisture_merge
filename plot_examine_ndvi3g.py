import utils_management as mg
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import cartopy.crs as ccrs
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import os
import pandas as pd
import numpy as np


# See if the last year being 2006 has any effect
year = range(1982, 2007)

flist = [os.path.join(mg.path_data(), 'NDVI3g', 
                      'ndvi3g_geo_v1_' + str(y) + '_' + str(j) + '.nc4') \
         for y in year for j in ['0106', '0712']]


data = xr.open_mfdataset(flist, decode_times = False)
# ---- remove _fill_val (-32768.) and convert to float
var = data['ndvi'].where(np.abs(data['ndvi'] + 32768.) > 1e-3) / 10000.
var = var.copy(deep = True)
data.close()


###############################################################################
# Calculate and plot the global mean time series.
###############################################################################
# ---- resample to the mean of each month
var_ts = var.mean(dim = ['lat', 'lon'])
var_ts = 0.5 * (var_ts[::2] + var_ts.values[1::2])
time = pd.date_range(str(year[0]) + '-01-01',
                     str(year[-1]) + '-12-31', freq = 'MS')


fig, ax = plt.subplots(figsize = (10, 10))
ax.plot(time, var_ts.values, '-', color = 'k')
ax.set_xlabel('Year')
ax.set_ylabel('NDVI')
fig.savefig(os.path.join(mg.path_data(), 'NDVI3g',
                         'examine_global_mean_ts.png'), 
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)


###############################################################################
# Calculate and plot the global climatology.
###############################################################################
var_mean = var.mean(dim = ['time'])

# Save the climatology for the purpose of re-gridding and creating land
# masks.
var_mean = var_mean.fillna(-1e20)
var_mean.attrs['_FillValue'] = -1e20
var_mean.to_dataset(name = 'ndvi').to_netcdf(os.path.join( \
    mg.path_intrim_out(), 'land_mask_minimum', 'ndvi_climatology.nc'))


# Create the figure, get the panel (ax)
fig, ax = plt.subplots(figsize = (10, 6), 
                       subplot_kw = {'projection': ccrs.PlateCarree()})


# Options - Change here
cmap = 'Spectral'
levels = np.linspace(-.3, 1.0, 14)
map_extent = [-180, 180, -60, 90]
grid_on = True # True, False


# Generic module: var - some xr.DataArray
ax.coastlines()
ax.set_extent(map_extent)
h = ax.contourf(var.lon, var.lat, var_mean, cmap = cmap, levels = levels)
plt.colorbar(h, ax = ax, boundaries = levels, shrink = 0.7)
if grid_on:
    gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True,
                      linewidth=1, color='gray', alpha=0.5, linestyle='--')
    gl.xlabels_top = False
    gl.ylabels_right = False
    gl.xlocator = mticker.FixedLocator(np.arange(-180, 180, 20.))
    gl.ylocator = mticker.FixedLocator(np.arange(-90., 90., 10.))
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'color': 'black', 'weight': 'bold', 'size': 10}
    gl.ylabel_style = {'color': 'black', 'weight': 'bold', 'size': 10}


fig.savefig(os.path.join(mg.path_data(), 'NDVI3g', 
                         'examine_global_mean_climatology.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)
