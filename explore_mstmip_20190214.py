# -*- coding: utf-8 -*-
"""
Created on Thu Feb 14 15:28:15 2019

@author: ywang254

Examine the soil moisture data from MstMIP land surface models.
"""
import pandas as pd
import xarray as xr
import utils_management as mg
import os

path_mstmip = os.path.join(mg.path_data(), 'MsTMIP')

flist = ['BIOME-BGC_BG1_Monthly_SoilWet.nc4',
         'CLASS-CTEM-N_BG1_Monthly_SoilMoist.nc4',
         'CLM4VIC_BG1_Monthly_SoilMoist.nc4',
         'CLM4_BG1_Monthly_SoilMoist.nc4',
         'GTEC_SG3_Monthly_SoilMoist.nc4',
         'LPJ-wsl_SG3_Monthly_SoilMoist.nc4',
         'ORCHIDEE-LSCE_SG3_Monthly_SoilWet_Lower.nc4',
         'ORCHIDEE-LSCE_SG3_Monthly_SoilWet_Upper.nc4',
         'SiB3_SG3_Monthly_SoilWet.nc4',
         'SiBCASA_SG3_Monthly_SoilMoist.nc4',
         'TRIPLEX-GHG_BG1_Monthly_SoilWet.nc4',
         'VISIT_SG3_Monthly_SoilMoist.nc4']

model = []
simulation = []
var = []
n_soil_layers = []
contact = []
email = []
ref_list = []

for f in flist:
    model.append(f.split('_')[0])
    simulation.append(f.split('_')[1])
    var.append(f.split('_')[3].split('.')[0])

    ds = xr.open_dataset(os.path.join(path_mstmip, f))    

    try:
        nsoil = len(ds.nsoil)
    except:
        nsoil = 1
    n_soil_layers.append(nsoil)

    try: 
        ref = ds.references
    except:
        ref = 'unknown'
    ref_list.append(ref)
    
    try:
        c = ds.contact
    except:
        c = 'unknown'
    contact.append(c)
        
    try:
        e = ds.email
    except:
        e = 'unknown'
    email.append(e)

    ds.close()


model_description = pd.DataFrame.from_dict({'model': model, 
                                            'simulation': simulation, 
                                            'var': var, 
                                            'n_soil_layers': n_soil_layers,
                                            'contact': contact, 
                                            'email': email, 
                                            'ref': ref_list})
model_description.to_csv(os.path.join(mg.path_out(), 'explore_20190214.csv'))


# Extra: the soil layer depths of CLM
clm_soil_depths = {}
for m in ['CLM4_BG1', 'CLM4VIC_BG1']:
    clm_soil_depths[m] = {}
    for f in ['z_bottom', 'z_node', 'z_top']:
        ds = xr.open_dataset(os.path.join(mg.path_data(), 'MsTMIP',
                                          m + '_Monthly_' + f + '.nc4'))
        clm_soil_depths[m][f] = ds[f].values
        ds.close()


# The soil reference depth 
ds = xr.open_dataset(os.path.join(path_mstmip,
                                  'mstmip_driver_global_hd_soil_v1.nc4'))
ds.close()