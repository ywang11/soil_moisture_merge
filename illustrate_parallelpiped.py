"""
2019/08/23
ywang254@utk.edu

https://stackoverflow.com/questions/44881885/python-draw-parallelepiped
"""
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection, Line3DCollection
import itertools as it
import utils_management as mg
import os


def plot_cube(cube_definition, ax, fc):
    cube_definition_array = [
        np.array(list(item))
        for item in cube_definition
    ]

    points = []
    points += cube_definition_array
    vectors = [
        cube_definition_array[1] - cube_definition_array[0],
        cube_definition_array[2] - cube_definition_array[0],
        cube_definition_array[3] - cube_definition_array[0]
    ]

    points += [cube_definition_array[0] + vectors[0] + vectors[1]]
    points += [cube_definition_array[0] + vectors[0] + vectors[2]]
    points += [cube_definition_array[0] + vectors[1] + vectors[2]]
    points += [cube_definition_array[0] + vectors[0] + vectors[1] + vectors[2]]

    points = np.array(points)

    edges = [
        [points[0], points[3], points[5], points[1]],
        [points[1], points[5], points[7], points[4]],
        [points[4], points[2], points[6], points[7]],
        [points[2], points[6], points[3], points[0]],
        [points[0], points[2], points[4], points[1]],
        [points[3], points[6], points[7], points[5]]
    ]


    faces = Poly3DCollection(edges, linewidths=1, edgecolors='k')
    faces.set_facecolor(fc)

    ax.add_collection3d(faces)

    # Plot the points themselves to force the scaling of the axes
    ax.scatter(points[:,0], points[:,1], points[:,2], s=0)

    ##ax.set_aspect('equal')
    ax.auto_scale_xyz([0, 2], [0, 2], [0, 1.5])
    ax.axis('off')

# Color 1 cube
fig, axes = plt.subplots(nrows = 1, ncols = 3, figsize = (15, 5),
                         subplot_kw = {'projection': '3d'})
fig.subplots_adjust(wspace = -0.1)

##ax = fig.add_subplot(111, projection='3d')

ax = axes.flat[0]
for x,y,z in it.product(np.arange(0., 1., 0.1), 
                        np.arange(0., 2., 0.1), 
                        np.arange(0., 1., 0.1)):
    # origin, y-size, x-size, z-size
    cube_definition = [
        (x,y,z), (x,y+0.1,z), (x+0.1,y,z), (x,y,z+0.1)
    ]

    if (x == 0.4) & (y==0.) & (z == 0.9):
        fc = (0.9921875, 0.6953125, 0.296875, 1.)
    else:
        fc = (1, 1, 1, 1.)
    plot_cube(cube_definition, ax, fc)

ax.set_xlabel('Lon')
ax.set_ylabel('Time')
ax.set_zlabel('Lat')

ax.set_title('(a) Year-month 1grid')

##fig.savefig(os.path.join(mg.path_out(), 'parallelpiped-1.png'), dpi=600.)


# Color 9 cubes
##fig = plt.figure()
##ax = fig.add_subplot(111, projection='3d')

ax = axes.flat[1]
for x,y,z in it.product(np.arange(0., 1., 0.1), 
                        np.arange(0., 2., 0.1), 
                        np.arange(0., 1., 0.1)):
    # origin, y-size, x-size, z-size
    cube_definition = [
        (x,y,z), (x,y+0.1,z), (x+0.1,y,z), (x,y,z+0.1)
    ]

    if (x >= 0.3) & (x <= 0.5) & (y==0.) & (z >= 0.7) & (z <= 0.9):
        fc = (0.9921875, 0.6953125, 0.296875, 1)
    else:
        fc = (1, 1, 1, 1.)
    plot_cube(cube_definition, ax, fc)

ax.set_xlabel('Lon')
ax.set_ylabel('Time')
ax.set_zlabel('Lat')

ax.set_title('(b) Year-month 9grid')

##fig.savefig(os.path.join(mg.path_out(), 'parallelpiped-2.png'), dpi=600.)


# Color 1 row
##fig = plt.figure()
##ax = fig.add_subplot(111, projection='3d')

ax = axes.flat[2]
for x,y,z in it.product(np.arange(0., 1., 0.1), 
                        np.arange(0., 2., 0.1), 
                        np.arange(0., 1., 0.1)):
    # origin, y-size, x-size, z-size
    cube_definition = [
        (x,y,z), (x,y+0.1,z), (x+0.1,y,z), (x,y,z+0.1)
    ]

    if (x == 0.9) & (z == 0.9):
        fc = (0.9921875, 0.6953125, 0.296875, 1)
    else:
        fc = (1, 1, 1, 1.)
    plot_cube(cube_definition, ax, fc)

ax.set_xlabel('Lon')
ax.set_ylabel('Time')
ax.set_zlabel('Lat')

ax.set_title('(c/d) Month/Month-anomaly 1grid')

##fig.savefig(os.path.join(mg.path_out(), 'parallelpiped-3.png'), dpi=600.)
fig.savefig(os.path.join(mg.path_out(), 'parallelpiped.png'), dpi=600., 
            bbox_inches = 'tight')
