"""
20200805
ywang254@utk.edu

Plot the merged products for global srex regions.

20200526
ywang254@utk.edu

Plot only the main regions, use spectrum density
"""
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
import matplotlib as mpl
import regionmask as rmk
import utils_management as mg
from utils_management.constants import depth, depth_cm, val_list, val_depth, \
    val2_list
from scipy import signal


def small_panel(a_gs, prod_by_depth, source_by_depth):
    sub_gs = gridspec.GridSpecFromSubplotSpec(4, 1, a_gs, hspace = 0.)
    axes = np.empty(4, dtype = object)
    for i in range(4):
        if i == 0:
            axes[i] = plt.subplot(sub_gs[i])
        else:
            axes[i] = plt.subplot(sub_gs[i], sharex = axes[0],
                                  sharey = axes[0])
        ax = axes[i]

        h = [None] * prod_by_depth[i].shape[1]
        for j in range(prod_by_depth[i].shape[1]):
            freqs, psd = signal.welch(prod_by_depth[i].values[:,j],
                                      fs = 12) # sampling frequency = 12

            h[j], = ax.plot(1/freqs, psd, '-', color = clist[j], lw = 0.6, zorder = 3)
        if i == 0:
            h0 = h # has all the products

        # Calculate the envelop of the PSD of the source datasets.
        source_psd = np.full([len(freqs), source_by_depth[i].shape[1]], np.nan)
        for j in range(source_by_depth[i].shape[1]):
            temp = source_by_depth[i].values[:,j]
            temp = temp[~np.isnan(temp)]
            if len(temp) == 0:
                continue
            freqs, source_psd[:, j] = signal.welch(temp,
                                                   fs = 12) # sampling frequency = 12
        hf = ax.fill_between(1/freqs, np.nanmax(source_psd, axis = 1),
                             np.nanmin(source_psd, axis = 1), zorder = 2,
                             edgecolor = '#636363', facecolor = '#f7f7f7',
                             lw = 0.5)

        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')
        #ax.ticklabel_format(axis = 'y', scilimits = (-2,2))
        ax.tick_params('both', length = 1.5)
        ax.set_xscale('log')
        ax.set_yscale('log')
        ax.set_ylim([1e-9, 1])
        ax.set_yticks([1e-6, 1e-4, 1e-2])
        ax.set_xlim([0.1666667, 21.33333])
        #ylim = ax.get_ylim()
        #ax.set_ylim([ylim[0], ylim[1]*1.2])
    return h0, hf, axes, sub_gs


prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all']
prod_names = ['Mean ORS', 'OLC ORS', 'EC ORS', 'EC CMIP5', 'EC CMIP6',
              'EC CMIP5+6', 'EC ALL']


mpl.rcParams['font.size'] = 5
mpl.rcParams['axes.titlesize'] = 5
mpl.rcParams['axes.titleweight'] = 'bold'
mpl.rcParams['axes.titlepad'] = 1.5
mpl.rcParams['axes.linewidth'] = 0.5
lab = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
       'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
       'y', 'z', 'aa', 'bb', 'cc', 'dd', 'ee', 'ff', 'gg', 'hh',
       'ii', 'jj', 'kk', 'll', 'mm', 'nn', 'oo', 'pp', 'qq', 'rr',
       'ss', 'tt', 'uu', 'vv', 'ww', 'xx']
cmap = mpl.cm.get_cmap('jet')
clist = [cmap( (i+0.5)/len(prod_list) ) for i in range(len(prod_list))]


#
if __name__ == '__main__':
    scaler = 1. # multiple the soil moisture values
    # Read dataset
    prod_set = pd.read_csv(os.path.join(mg.path_out(),'validate',
                                        'prod_srex_all.csv'),
                           index_col = 0, parse_dates = True,
                           header = [0,1,2]) * scaler
    source_set = pd.read_csv(os.path.join(mg.path_out(), 'validate',
                                          'source_srex_all.csv'),
                             index_col = 0, parse_dates = True,
                             header = [0,1,2]) * scaler
    
    
    fig = plt.figure(figsize = (6.5, 7))
    #gs = gridspec.GridSpec(3, 4, hspace = 0.13, wspace = 0.25)
    gs = gridspec.GridSpec(2, 3, hspace = 0.08, wspace = 0.08)
    for ind, sid in enumerate(['3', '7', '13', '15', '22', '25']):
        prod_by_depth = {}
        for dind, dcm in enumerate(depth_cm):
            prod_by_depth[dind] = prod_set.loc[:, (slice(None), dcm, str(sid))]
            prod_by_depth[dind].columns = prod_by_depth[dind].columns.droplevel([1,2])
    
        source_by_depth = {}
        for dind, dcm in enumerate(depth_cm):
            source_by_depth[dind] = source_set.loc[:, (slice(None), dcm, str(sid))]
            source_by_depth[dind].columns = source_by_depth[dind].columns.droplevel([1,2])
    
        gs_panel = gs[ind // 3, np.mod(ind, 3)]
        h, hf, axes, sub_gs = small_panel(gs_panel, prod_by_depth, source_by_depth)
        axes[0].set_title(rmk.defined_regions.srex.names[int(sid)-1])
    
        for i in range(4):
            axes[i].text(0.02, 0.8, '(' + lab[ind] + str(i) + ') ' + \
                         depth_cm[i].replace('-', u'\u2212').replace('cm',' cm'),
                         transform = axes[i].transAxes)
        if ind >= 3:
            for i in range(4):
                plt.setp(axes[i].get_xticklabels(), visible=True)
            axes[3].set_xlabel('Period (year)')
        else:
            for i in range(4):
                plt.setp(axes[i].get_xticklabels(), visible=False)
    
        if ind == 0:
            axes[1].text(-0.26, 0., 'Power', horizontalalignment = 'center', rotation = 90,
                         transform = axes[1].transAxes)
        elif ind == 3:
            axes[1].text(-0.26, 0., 'Power', horizontalalignment = 'center', rotation = 90,
                         transform = axes[1].transAxes)
        else:
            for i in range(4):
                plt.setp(axes[i].get_yticklabels(), visible=False)
        
        if ind == 3:
            axes[3].legend(h + [hf], prod_names + ['Source datasets'],
                           loc = (0.3, -0.9), ncol = 6)
    fig.savefig(os.path.join(mg.path_out(), 'validate',
                             'srex_psd_fewer.png'), dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
