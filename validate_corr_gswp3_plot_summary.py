"""
2020/12/10

Color the map with the factor that has largest annual partial correlations.
"""
import xarray as xr
import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import itertools as it
import utils_management as mg
from utils_management.constants import depth, depth_cm
from misc.plot_utils import stipple, cmap_div


mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 6
mpl.rcParams['hatch.linewidth'] = 0.5
cmap = 'jet'
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'


prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all']
prod_name_list = ['Mean ORS', 'OLC ORS', 'EC ORS', 'EC CMIP5', 'EC CMIP6',
                  'EC CMIP5+6', 'EC ALL']
season_list = ['Annual', 'DJF', 'MAM', 'JJA', 'SON']
for season in season_list:
    fig, axes = plt.subplots(len(prod_list), 4, figsize = (6.5, 6.5),
                             subplot_kw = {'projection': ccrs.Miller()})
    fig.subplots_adjust(wspace = 0.01, hspace = 0.01)
    count = 0
    for pind, dind in it.product(range(len(prod_list)), range(len(depth))):
        prod = prod_list[pind]

        d = depth[dind]
        dcm = depth_cm[dind]

        hr = xr.open_dataset(os.path.join(mg.path_out(), 'validate',
                                          'corr_gswp3_' + prod + '_' + \
                                          d + '_' + season + '.nc'))
        ax = axes.flat[count]
        ax.coastlines(lw = 0.5, color = 'grey')
        ax.set_extent([-180, 180, -60, 90], crs=ccrs.PlateCarree())

        a = np.where(hr['pr_p'].values > 0.05, 0, np.abs(hr['pr'].values))
        b = np.where(hr['tas_p'].values > 0.05, 0, np.abs(hr['tas'].values))
        c = np.where(hr['rsds_p'].values > 0.05, 0, np.abs(hr['rsds'].values))
        temp = np.stack([a, b, c], axis = 0)
        temp = np.argmax(temp, axis = 0)
        temp = np.where(np.isnan(hr['pr'].values), np.nan, temp)
        temp = np.where((hr['pr_p'].values > 0.05) & \
                        (hr['tas_p'].values > 0.05) & \
                        (hr['rsds_p'].values > 0.05), np.nan, temp)
        cf = ax.contourf(hr['lon'], hr['lat'], temp, 
                         cmap = cmap, 
                         levels = [-0.5, 0.5, 1.5, 2.5],
                         transform = ccrs.PlateCarree(),
                         extend = 'neither')
        hr.close()

        if pind == 0:
            ax.set_title(dcm)
        if dind == 0:
            ax.text(-0.1, 0.5, prod_name_list[pind], rotation = 90., 
                    verticalalignment = 'center',
                    transform = ax.transAxes)

        ax.text(0.05, 0.85, '(' + lab[count] + ')', transform = ax.transAxes)
        count += 1

    cax = fig.add_axes([0.1, 0.07, 0.8, 0.01])
    cbar = plt.colorbar(cf, cax = cax, orientation = 'horizontal',
                        boundaries = [-0.5, 0.5, 1.5, 2.5],
                        ticks = [0,1,2])
    cbar.ax.set_xticklabels(['pr', 'tas', 'rsds'])

    fig.savefig(os.path.join(mg.path_out(), 'validate', 
                             'corr_gswp3_argmax_' + \
                             season + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
