"""
20210604

Plot the trend of the regional time series.
"""
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
import matplotlib as mpl
import regionmask as rmk
import utils_management as mg
from utils_management.constants import depth, depth_cm, val_list, val_depth, val2_list
from scipy.stats import linregress


prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all']
prod_names = ['Mean ORS', 'OLC ORS', 'EC ORS', 'EC CMIP5', 'EC CMIP6',
              'EC CMIP5+6', 'EC ALL']


mpl.rcParams['font.size'] = 5
mpl.rcParams['axes.titlesize'] = 5
mpl.rcParams['axes.titleweight'] = 'bold'
mpl.rcParams['axes.titlepad'] = 1.5
mpl.rcParams['axes.linewidth'] = 0.5
lab = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
       'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
       'y', 'z', 'aa', 'bb', 'cc', 'dd', 'ee', 'ff', 'gg', 'hh',
       'ii', 'jj', 'kk', 'll', 'mm', 'nn', 'oo', 'pp', 'qq', 'rr',
       'ss', 'tt', 'uu', 'vv', 'ww', 'xx']
clist = ['#1b9e77', '#d95f02', '#7570b3', '#e7298a', '#66a61e', '#e6ab02',
         '#a6761d']
scaler = 100.


#
if __name__ == '__main__':
    # Read dataset
    prod_set = pd.read_csv(os.path.join(mg.path_out(),'validate', 'prod_srex_all.csv'),
                           index_col = 0, parse_dates = True,
                           header = [0,1,2])
    source_set = pd.read_csv(os.path.join(mg.path_out(), 'validate',
                                          'source_srex_all.csv'),
                             index_col = 0, parse_dates = True,
                             header = [0,1,2])

    mpl.rcParams['font.size'] = 6
    mpl.rcParams['axes.titlesize'] = 6

    fig, axes = plt.subplots(4, 1, figsize = (6.5, 7), sharex = True, sharey = True)
    for dind, dcm in enumerate(depth_cm):
        ax = axes.flat[dind]
        ax.text(0.025, 1.05, '(' + lab[dind] + ') ' + dcm, transform = ax.transAxes)

        # Fill the trend & p-values
        prod_trend = np.full((26, len(prod_set.columns.levels[0])), np.nan)
        prod_trend_p = np.full((26, len(prod_set.columns.levels[0])), np.nan)
        source_trend = np.full((26, len(source_set.columns.levels[0])), np.nan)
        source_trend_p = np.full((26, len(source_set.columns.levels[0])), np.nan)
        for sid in range(1, 27):
            prod = prod_set.loc[:, (slice(None), dcm, str(sid))]
            source = source_set.loc[:, (slice(None), dcm, str(sid))]

            for pind, pp in enumerate(prod.columns):
                res = linregress(np.arange(prod.shape[0]), prod[pp])
                prod_trend[sid-1, pind] = res.slope * scaler
                prod_trend_p[sid-1, pind] = res.pvalue
            for pind, pp in enumerate(source.columns):
                res = linregress(np.arange(source.shape[0]), source[pp])
                source_trend[sid-1, pind] = res.slope * scaler
                source_trend_p[sid-1, pind] = res.pvalue

        # Plot
        x0 = np.arange(0.6, 52, 2)
        h = [None] * len(prod_set.columns.levels[0])
        for ii in range(len(prod_set.columns.levels[0])):
            h[ii], = ax.plot(x0[prod_trend_p[:,ii] <= 0.05],
                             prod_trend[prod_trend_p[:,ii] <= 0.05, ii],
                             'o', color = clist[ii], markerfacecolor = clist[ii],
                             markersize = 2.5)
        h2 = [None] * len(prod_set.columns.levels[0])
        for ii in range(len(prod_set.columns.levels[0])):
            h2[ii], = ax.plot(x0[prod_trend_p[:,ii] > 0.05],
                              prod_trend[prod_trend_p[:,ii] > 0.05, ii],
                              'o', color = clist[ii], markerfacecolor = 'none',
                              markersize = 2.5)

        #source_trend_sig = [source_trend[ii, source_trend_p[ii,:] <= 0.05] \
        #                    for ii in range(source_trend.shape[0])]
        #b = ax.boxplot(source_trend_sig, positions = np.arange(1.,53,2),
        #               patch_artist = True, whis = [5, 95], notch = True,
        #               showfliers = False, widths = 0.3)
        #plt.setp(b['boxes'], facecolor = '#525252')
        #
        #source_trend_insig = [source_trend[ii, source_trend_p[ii,:] > 0.05] \
        #                      for ii in range(source_trend.shape[0])]
        #bp = ax.boxplot(source_trend_insig, positions = np.arange(1.4,53,2),
        #                patch_artist = True, whis = [5, 95], notch = True,
        #                showfliers = False, widths = 0.3)
        #plt.setp(bp['boxes'], color = '#bdbdbd')

        b = ax.boxplot([source_trend[ii,~np.isnan(source_trend[ii,:])] \
                        for ii in range(source_trend.shape[0])],
                       positions = np.arange(1.4,53, 2),
                       patch_artist = True, whis = [0, 100], notch = False,
                       showfliers = False, widths = 0.3)

        ax.set_xticks(x0 + 0.4)
        ax.set_xticklabels(rmk.defined_regions.srex.names, rotation = 90)
        ax.set_xlim([-0.5, 53])
    ax.text(-0.13, 2., r'Trend (m$^3$/m$^3$/100 years)', transform = ax.transAxes,
            rotation = 90)

    ax.legend(h + [b['boxes'][0]], prod_names + ['Source datasets'], ncol = 6,
              loc = (0., -1.3))
    fig.savefig(os.path.join(mg.path_out(), 'validate', 'srex_trend.png'), dpi = 600.,
                bbox_inches = 'tight')
    #fig.savefig('srex_trend.png', dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
