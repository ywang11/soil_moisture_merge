"""
2020/12/10

Draw the annual average, 1971-2016 average size of the 90% 
 uncertainty intervals of the merged products.

(-1.96, 1.96), if the former is <0 & latter >1, clip.
"""
import xarray as xr
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import matplotlib as mpl
import cartopy.crs as ccrs
import itertools as it
import utils_management as mg
from utils_management.constants import depth, depth_cm
from misc.plot_utils import stipple, cmap_div
from dateutil.relativedelta import relativedelta


def flist(name, d, dcm):
    if name == 'mean_lsm':
        fname = os.path.join(mg.path_out(), 'meanmedian_lsm',
                             'mean_uncertainty_' + d + '_1950-2016.nc')
    elif name == 'dolce_lsm':
        fname = os.path.join(mg.path_out(), 'concat_dolce_lsm',
                             'concat_uncertainty_' + d + \
                             '_lu_weighted_ShrunkCovariance.nc')
    elif (name == 'em_lsm') | (name == 'em_all'):
        fname = os.path.join(mg.path_out(), 'concat_' + name,
                             'concat_uncertainty_CRU_v4.03_year_month_' + \
                             'anomaly_9grid_'+ dcm + '_1950-2016.nc')
    else:
        fname = [os.path.join(mg.path_out(), name + '_corr',
                              'CRU_v4.03_year_month_' + \
                              'anomaly_9grid_' + dcm + '_1950-2016',
                              'predicted_std_' + str(yy) + '.nc') \
                 for yy in range(1950, 2017)]
    return fname


def decode_month_since(time):
    ref = pd.Timestamp(time.attrs['units'].split(' ')[2])
    start = ref + relativedelta(months = time.values[0])
    start = start.replace(day = 1, hour = 0, minute = 0, second = 0)
    return pd.date_range(start, periods = len(time), freq = 'MS')


prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all']
prod_name_list = ['Mean ORS', 'OLC ORS', 'EC ORS', 'EC CMIP5', 'EC CMIP6',
                  'EC CMIP5+6', 'EC ALL']
lab = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
       'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
       'y', 'z', 'aa', 'bb', 'cc', 'dd', 'ee', 'ff', 'gg', 'hh',
       'ii', 'jj', 'kk', 'll', 'mm', 'nn', 'oo', 'pp', 'qq', 'rr',
       'ss', 'tt', 'uu', 'vv', 'ww', 'xx']


mpl.rcParams['font.size'] = 6.
mpl.rcParams['font.size'] = 6.
mpl.rcParams['hatch.linewidth'] = 0.5
cmap = cmap_div()


fig, axes = plt.subplots(len(prod_list), 4, figsize = (6.5, 6.5),
                         subplot_kw = {'projection': ccrs.Miller()})
fig.subplots_adjust(wspace = 0.01, hspace = 0.01)
count = 0
for pind, dind in it.product(range(len(prod_list)), range(len(depth))):
    prod = prod_list[pind]
    d = depth[dind]
    dcm = depth_cm[dind]

    ax = axes.flat[count]
    ax.coastlines(lw = 0.5)
    ax.set_extent([-180, 180, -60, 90])

    #
    fname = flist(prod, d, dcm)
    if type(fname) == str:
        hr = xr.open_dataset(fname, decode_times = False)
    else:
        hr = xr.open_mfdataset(fname, decode_times = False,
                               concat_dim = 'time')
    if 'em' in prod:
        uncertainty = hr['predicted_std'].copy(deep = True)
    else:
        uncertainty = hr['sm_uncertainty'].copy(deep = True)
    hr.close()

    uncertainty = uncertainty.mean(dim = 'time')

    #
    norm = LogNorm(vmin = 1e-3, vmax = 1.)
    levels = np.power(10, np.linspace(-3, 0, 31))
    cf = ax.contourf(hr['lon'], hr['lat'], 
                     uncertainty, 
                     cmap = 'Spectral',
                     norm = norm, levels = levels,
                     extend = 'both',
                     transform = ccrs.PlateCarree())

    ax.text(0.05, 0.85, '(' + lab[count] + ')',
            transform = ax.transAxes)
    if dind == 0:
        ax.text(-0.1, 0.5, prod_name_list[pind], rotation = 90,
                verticalalignment = 'center',
                transform = ax.transAxes)
    if pind == 0:
        ax.set_title(dcm.replace('-', u'\u2212').replace('cm', ' cm'))

    count += 1
cax = fig.add_axes([0.1, 0.07, 0.8, 0.01])
cbar = plt.colorbar(cf, cax = cax, orientation = 'horizontal',
                    ticks = [1e-3, 1e-2, 1e-1, 1.])
cbar.ax.set_xticklabels([1e-3, 1e-2, 1e-1, 1.])

fig.savefig(os.path.join(mg.path_out(), 'validate',
                         'uncertainty.png'), dpi = 600., 
            bbox_inches = 'tight')
plt.close(fig)
