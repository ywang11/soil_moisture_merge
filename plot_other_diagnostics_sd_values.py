"""
2019/07/05

ywang254@utk.edu

Plot the standard deviation of soil moisture at each grid.
"""
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth
import utils_management as mg
import numpy as np
import os
import itertools as it
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import cartopy.crs as ccrs
from cartopy.util import add_cyclic_point
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import xarray as xr
import multiprocessing as mp


model_set = [x+'_'+y for x in ['mean', 'dolce', 'em'] for y in \
             ['lsm', 'cmip5', 'cmip6', '2cmip', 'all']]
year = year_longest
year_str = str(year[0]) + '-' + str(year[-1])


levels = np.linspace(0., 0.02, 21)
cmap = 'Spectral'
map_extent = [-180, 180, -60, 90]


def plotter(month):
    fig, axes = plt.subplots(nrows = len(model_set), ncols = len(depth),
                             figsize = (17, 26),
                             subplot_kw = {'projection': ccrs.PlateCarree()})
    fig.subplots_adjust(wspace = 0., hspace = 0.)
    for mind, dind in it.product(range(len(model_set)), range(len(depth))):
        model = model_set[mind]
        d = depth[dind]

        data = xr.open_dataset(os.path.join(mg.path_out(), 'other_diagnostics',
                                            model, 'sd_' + d + '.nc'))
        ax = axes[mind, dind]
        ax.coastlines()
        ax.set_extent(map_extent)
        sd_cyc, lon_cyc = add_cyclic_point(data['sd'].values[month-1,:,:], 
                                           coord=data.lon)
        h = ax.contourf(lon_cyc, data.lat, sd_cyc, cmap = cmap, 
                        levels = levels, extend = 'both')
        if dind == 0:
            ax.text(-0.1, 0.5, model, rotation = 90, transform = ax.transAxes)
        if mind == 0:
            ax.set_title(d)

    cax = fig.add_axes([0.93, 0.1, 0.01, 0.8])
    fig.colorbar(h, cax = cax, boundaries = levels,
                 ticks = 0.5 * (levels[1:] + levels[:-1]))
    fig.savefig(os.path.join(mg.path_out(), 'other_diagnostics_plot', 'sd',
                             str(month) + '.png'), dpi = 600.)
    plt.close(fig)


for month in range(1,13):
    plotter(month)
#p = mp.Pool(4)
#p.map_async(plotter, range(1,13))
#p.close()
#p.join()
