"""
2019/06/12
ywang254@utk.edu

Plot the global maps of interpolated soil moisture at each depth (if exist).
"""
import xarray as xr
import sys
import os
import matplotlib.pyplot as plt
from utils_management.paths import path_intrim_out
from utils_management.constants import cmip5, depth_cm
import cartopy.crs as ccrs
from glob import glob
import multiprocessing as mp


land_mask = 'vanilla' # None


#for model in cmip5:
def plotter(model):
    fig, axes = plt.subplots(nrows = 2, ncols = 2, figsize = (20, 12),
                             subplot_kw = {'projection': ccrs.PlateCarree()})
    for d in range(4):
        filename = sorted(glob(os.path.join(path_intrim_out(), 'Interp_Merge',
                                            land_mask, 'CMIP5', model,
                                            'sm_*' + depth_cm[d] + '.nc')))
        if len(filename) == 0:
            continue

        data = xr.open_mfdataset(filename, decode_times = False,
                                 concat_dim = 'time')
        sm_data = data['sm'].mean(dim='time').copy(deep=True).load()
        data.close()

        ax = axes.flat[d]
        ax.coastlines()
        h = ax.contourf(sm_data.lon.values, sm_data.lat.values,
                        sm_data.values)
        plt.colorbar(h, ax = ax, shrink = 0.7)
        ax.set_title('Soil moisture ' + depth_cm[d] + ' (m$^3$/m$^3$)')
    fig.savefig(os.path.join(path_intrim_out(), 'Interp_Merge_plot', 
                             land_mask, 'cmip5', 
                             'global_map_' + model + '.png'),
                dpi = 600.)
    plt.close(fig)


p = mp.Pool(8)
p.map_async(plotter, cmip5)
p.close()
p.join()
