import os
import numpy as np
import pandas as pd
import xarray as xr
import utils_management as mg
import matplotlib.pyplot as plt
import cartopy.crs as ccrs


# Test runs:
#dolce_all_weighted_uncertainty_0_year_shortest_0_0.py
#dolce_all_weighted_uncertainty_0_year_shortest_0_3.py
#dolce_all_weighted_uncertainty_0_year_shorter_0_1.py


# Make sure that the script produces identical results to the older script.
hr = xr.open_dataset(os.path.join(mg.path_out(), 'dolce_all_product',
                                  'weighted_uncertainty_1950-2010_' + \
                                  '0.10-0.30_simple_EmpiricalCovariance.nc'),
                     decode_times = False)
hr2 = xr.open_dataset(os.path.join(mg.path_out(), 'dolce_all_product',
                                   'backup',
                                   'weighted_uncertainty_1950-2010_' + \
                                   '0.10-0.30_simple_EmpiricalCovariance.nc'),
                      decode_times = False)
diff = hr['sm_uncertainty'].values - hr2['sm_uncertainty'].values

print(np.nanmax(diff))
print(np.nanmin(diff))

hr.close()
hr2.close()

# ---- check for another year
hr = xr.open_dataset(os.path.join(mg.path_out(), 'dolce_all_product',
                                  'weighted_uncertainty_1981-2010_' + \
                                  '0.50-1.00_simple_EmpiricalCovariance.nc'),
                     decode_times = False)
hr2 = xr.open_dataset(os.path.join(mg.path_out(), 'dolce_all_product',
                                   'backup',
                                   'weighted_uncertainty_1981-2010_' + \
                                   '0.50-1.00_simple_EmpiricalCovariance.nc'),
                      decode_times = False)
diff = hr['sm_uncertainty'].values - hr2['sm_uncertainty'].values

print(np.nanmax(diff))
print(np.nanmin(diff))

hr.close()
hr2.close()


# Make sure that the 0.00-0.10 products are fully filled.
hr = xr.open_dataset(os.path.join(mg.path_out(), 'dolce_all_product',
                                  'weighted_uncertainty_1981-2010_' + \
                                  '0.00-0.10_simple_EmpiricalCovariance.nc'),
                     decode_times = False)
print(np.sum(np.sum(~np.isnan(hr['sm_uncertainty'].values), axis = 2),
             axis = 1))
#print(np.sum(np.sum(~np.isnan(diff), axis = 2), axis = 1))


fig, ax = plt.subplots(figsize = (10, 6))
cf = ax.contourf(hr.lon, hr.lat, hr['sm_uncertainty'].values.mean(axis = 0),
                 cmap = 'Spectral')
plt.colorbar(cf)
fig.savefig(os.path.join(mg.path_out(),
                         'dolce_all_weighted_uncertainty_debug.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)


hr.close()
