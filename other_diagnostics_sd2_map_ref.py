"""
2020/05/20

ywang254@utk.edu

The standard deviation in soil moisture, 1981-2010. w/ reference to
 ERA-Interim & GLEAM. Not distinguishing between monthly and interannual
 variations.
"""
import os
import itertools as it
import pandas as pd
import numpy as np
import xarray as xr
import utils_management as mg
from utils_management.constants import depth, depth_cm
from misc.analyze_utils import calc_seasonal_trend
import time
from glob import glob


land_mask = 'vanilla'


for l, i in it.product(['ERA-Land', 'GLEAMv3.3a'], range(4)):
    d = depth[i]
    dcm = depth_cm[i]

    if i > 1:
        continue
    elif (i == 1) & (l == 'GLEAMv3.3a'):
        continue

    fname = sorted(glob(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                     land_mask, l, l + '_*_' + dcm + '.nc')))
    data = xr.open_mfdataset(fname, decode_times = False)
    sm = data.sm

    year = [int(f.split('_')[-2]) for f in fname]

    sm.load()
    sm['time'] = pd.date_range(str(year[0])+'-01-01',
                               str(year[-1])+'-12-31', freq = 'MS')
    sm = sm.loc[(sm['time'].to_index().year >= 1981) & \
                (sm['time'].to_index().year <= 2010), :, :]
    sm_sd = sm.std(axis = 0)

    #
    sm_sd.to_dataset(name = 'sd' \
    ).to_netcdf(os.path.join(mg.path_out(), 'other_diagnostics', 'lsm', 
                             l + '_sd2_' + d + '.nc'))
    data.close()
