"""
2019/07/05

ywang254@utk.edu

Map the global mean and seasonal mean soil moisture for the individual
 land surface models.
"""
from utils_management.constants import lsm_all, year_longest, year_shorter, \
    year_shorter2, year_shortest, depth
import utils_management as mg
from misc.plot_standard_diagnostics_utils import plot_map
import numpy as np
import os
import itertools as it


levels = np.linspace(0., 0.6, 15)
cmap = 'RdYlBu'

# Mean of the land surface models.
for l in lsm_all:
    plot_map(depth, os.path.join(mg.path_out(), 'standard_diagnostics_lsm', l),
             os.path.join(mg.path_out(), 'standard_diagnostics_plot', 
                          'mean_lsm', l), 'map', levels, cmap)
