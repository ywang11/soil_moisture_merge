"""
20191118
ywang254@utk.edu

Plot the lag that has the maximum correlation.
"""
import utils_management as mg
from utils_management.constants import depth, depth_cm, lsm_list, year_shortest
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import os
import xarray as xr
import pandas as pd
import numpy as np
import itertools as it
import multiprocessing as mp
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import cartopy.crs as ccrs
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER


land_mask = 'vanilla'


def plotter(option):
    # MODIFY
    #met = 'temperature' # 'temperature', 'precipitation'
    #i = 0 # [0, 1, 2, 3]
    #model = 'lsm' # 'lsm', 'cmip5', 'cmip6'
    #month = 1
    
    model, met, i, month = option
    
    d = depth[i]
    dcm = depth_cm[i]
    
    if model == 'lsm':
        model_list = lsm_list[(str(year_shortest[0]) + '-' + \
                               str(year_shortest[-1]), d)]
    elif model == 'cmip5':
        model_list = cmip5_availability(dcm, land_mask)
    elif model == 'cmip6':
        model_list = list(set(mrsol_availability(dcm, land_mask,
                                                 'historical')) &\
                          set(mrsol_availability(dcm, land_mask, 'ssp585')))
        model_list = sorted([x for x in model_list if 'r1i1p1f1' in x])
    
    
    #
    data = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                        'em_tas_lagged_corr_month',
                                        'max_lag_' + model + '_' + dcm + \
                                        '_' + str(month) + '_' + met + '.nc'))
    cmap = 'Spectral'
    levels = np.linspace(-6.5, 6.5, 13)
    map_extent = [-180, 180, -60, 90]
    
    nrows = int(np.ceil(len(model_list) / 3))
    fig, axes = plt.subplots(nrows = nrows, ncols = 3, 
                             figsize = (14, 2.5*nrows),
                             subplot_kw = {'projection': ccrs.PlateCarree()})
    for m_ind, m in enumerate(model_list):
        ax = axes.flat[m_ind]
    
        var = data['max_lag'][m_ind, :, :]
    
        ax.coastlines()
        ax.set_extent(map_extent)
        h = ax.contourf(var.lon, var.lat, var, cmap = cmap, levels = levels)
        plt.colorbar(h, ax = ax, boundaries = levels, 
                     ticks = np.arange(-6., 7.), shrink = 0.75)
        ax.set_title(m)
    fig.savefig(os.path.join(mg.path_intrim_out(), 
                             'plot_em_tas_lagged_corr_month_max', model, 
                             met + '_' + dcm + '_' + str(month) + '.png'), 
                dpi = 600.)
    data.close()



option_list = list(it.product(['lsm', 'cmip5', 'cmip6'], 
                              ['temperature', 'precipitation'], 
                              range(4), range(12)))
pool = mp.Pool(4)
pool.map_async(plotter, option_list)
pool.close()
pool.join()
