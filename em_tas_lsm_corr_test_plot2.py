"""
20200805
ywang254@utk.edu

Examine the results of em_tas_lsm_corr_test_plot.py
"""
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import utils_management as mg
import itertools as it
from utils_management.constants import depth_cm, year_longest, year_shorter, \
    year_shorter2, year_shortest


mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 6
bounds = np.insert(np.append(np.insert(np.linspace(-0.1, 0.1, 21), 0, -1),
                             1), 0, -100)
cmap = mpl.cm.get_cmap('Spectral')
cmap = mpl.colors.ListedColormap(['gray'] + \
                                 [cmap((i+0.5)/(len(bounds)-2)) \
                                  for i in range(len(bounds)-2)])
norm = mpl.colors.BoundaryNorm(boundaries=bounds, ncolors=len(bounds)-1)


def hm(matrix, ax):
    matrix = np.where(np.isnan(matrix), -999, matrix)
    h = ax.pcolormesh(matrix, norm = norm, cmap = cmap)
    ax.set_yticks(range(matrix.shape[0]))
    ax.set_xticks(range(matrix.shape[1]))
    return h


year_list = [year_longest, year_shorter, year_shorter2, year_shortest]
for var in ['pr', 'tas']:
    fig, axes = plt.subplots(4, 4, sharex = True, sharey = True,
                             figsize = (6.5, 10))
    fig.subplots_adjust(hspace = 0.01, wspace = 0.01)

    for yind, dind in it.product(range(4), range(len(depth_cm))):
        year = year_list[yind]
        year_str = str(year[0]) + '-' + str(year[-1])
        dcm = depth_cm[dind]
        data = pd.read_csv(os.path.join(mg.path_intrim_out(),
                                        'em_tas_corr_test_plot',
                                        year_str + '_' + dcm + '_coef.csv'),
                           index_col = 0, header = [0,1])
        matrix = data.loc[:, var].values

        ax = axes[dind, yind]
        h = hm(matrix, ax)
        if yind == 0:
            ax.set_ylabel(dcm)
        else:
            ax.tick_params('y', length = 0)
            ax.set_yticklabels([])
        if dind == 0:
            ax.set_title(year_str)
        if dind != 3:
            ax.tick_params('x', length = 0)
            ax.set_xticklabels([])
        else:
            ax.set_xticks([mon+0.5 for mon in range(12)])
            ax.set_xticklabels([str(mon+1) for mon in range(12)])
        ax.set_ylim([0, matrix.shape[0]])
    cax = fig.add_axes([0.1, 0.05, 0.8, 0.01])
    plt.colorbar(h, cax = cax, orientation = 'horizontal',
                 boundaries = bounds, ticks = bounds[1::2])
    fig.savefig(os.path.join(mg.path_intrim_out(),
                             'em_tas_corr_test_plot',
                             'summary_coef_' + var + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
