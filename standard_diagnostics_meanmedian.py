"""
Compute the following for the mean and median of the land surface models:

1. Global map (averaged over time): NetCDF
2. Global average time series.
3. Latitude bands average time series.
4. Continental average time series.
5. SREX region average time series.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth
from misc.standard_diagnostics import standard_diagnostics
import os
import itertools as it
import pandas as pd


year = REPLACE1
model = 'REPLACE2' # lsm, cmip5, cmip6, all, products


for i, d in enumerate(depth):
    prefix = [('median', os.path.join(mg.path_out(), 'meanmedian_' + model,
                                      'median_' + d + '_' + str(year[0]) + \
                                      '-' + str(year[-1]) + '.nc')),
              ('mean', os.path.join(mg.path_out(), 'meanmedian_' + model,
                                    'mean_' + d + '_' + str(year[0]) + \
                                    '-' + str(year[-1]) + '.nc'))]
    for x in prefix:
        data = xr.open_dataset(x[1], decode_times=False)
        time = pd.date_range(str(year[0])+'-01-01', str(year[-1])+'-12-31',
                             freq = 'MS')
        standard_diagnostics(data.sm.values, time, data.lat, data.lon, 
                             os.path.join(mg.path_out(), 
            'standard_diagnostics_meanmedian_' + model),
                             x[0]+'_'+str(year[0])+'-'+str(year[-1])+ \
                             '_'+d)
        data.close()
