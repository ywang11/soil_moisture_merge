"""
2019/08/05
ywang254@utk.edu

Plot the time series of global mean annual/seasonal mean soil moisture of the
 dolce and em products, not including uncertainty.
"""
import matplotlib.pyplot as plt
import utils_management as mg
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth, depth_cm
from misc.plot_utils import plot_ts_shade, plot_ts_trend
import pandas as pd
import os
import numpy as np
import itertools as it
import multiprocessing as mp
from matplotlib.cm import get_cmap


cmap = get_cmap('Spectral')
clist = [cmap(i/5) for i in range(1, 5)]


#
def plotter(option):
    year, me = option

    global_ts_all_depth = {}
    for i,d in enumerate(depth):
        dcm = depth_cm[i]
        global_ts_all_depth[d] = pd.read_csv(os.path.join(mg.path_out(),
            'standard_diagnostics_em_' + model_set, 'CRU_v4.03_predicted_' + \
            me + '_' + str(year[0]) + '-' + str(year[-1]) + '_' + d + \
            '_g_ts.csv'), index_col = 0, parse_dates = True).iloc[:, 0]

    fig, axes = plt.subplots(nrows = len(depth_cm), ncols = 2, sharex = True, 
                             sharey = False, figsize = (10, 6.5))
    fig.subplots_adjust(hspace = 0., wspace = 0.2)

    for s in ['annual', 'DJF', 'MAM', 'JJA', 'SON']:
        # (1) Soil Moisture
        for i,d in enumerate(depth):
            dcm = depth_cm[i]

            ts = global_ts_all_depth[d]

            if s == 'annual':
                ts = ts.groupby(ts.index.year).mean()
            elif s == 'DJF':
                ts = ts.loc[(ts.index.month == 12) | \
                            (ts.index.month == 1) | (ts.index.month == 2)]
                ts = ts.groupby(ts.index.year).mean()
            elif s == 'MAM':
                ts = ts.loc[(ts.index.month == 3) | \
                            (ts.index.month == 4) | (ts.index.month == 5)]
                ts = ts.groupby(ts.index.year).mean()
            elif s == 'JJA':
                ts = ts.loc[(ts.index.month == 6) | \
                            (ts.index.month == 7) | (ts.index.month == 8)]
                ts = ts.groupby(ts.index.year).mean()
            elif s == 'SON':
                ts = ts.loc[(ts.index.month == 9) | \
                            (ts.index.month == 10) | (ts.index.month == 11)]
                ts = ts.groupby(ts.index.year).mean()

            ax = axes[i, 0]
            h1 = ax.plot(ts.index, ts.values, clist[i])
            if (i == 0):
                ax.set_title('Soil Moisture (m$^3$/m$^3$)')
                
            ax.set_xlim([year_longest[0], year_longest[-1]])
            ax.set_xlabel('Year')
            ax.set_ylabel(dcm) # '(m$^3$/m$^3$)'
            ax.set_ylim([0., 0.5])
            ax.set_yticks(np.linspace(0.1, 0.4, 5))

        # (2) Soil Moisture Anomaly
        for i,d in enumerate(depth):
            dcm = depth_cm[i]

            ts = global_ts_all_depth[d]

            if s == 'annual':
                ts = ts.groupby(ts.index.year).mean()
            elif s == 'DJF':
                ts = ts.loc[(ts.index.month == 12) | \
                            (ts.index.month == 1) | (ts.index.month == 2)]
                ts = ts.groupby(ts.index.year).mean()
            elif s == 'MAM':
                ts = ts.loc[(ts.index.month == 3) | \
                            (ts.index.month == 4) | (ts.index.month == 5)]
                ts = ts.groupby(ts.index.year).mean()
            elif s == 'JJA':
                ts = ts.loc[(ts.index.month == 6) | \
                            (ts.index.month == 7) | (ts.index.month == 8)]
                ts = ts.groupby(ts.index.year).mean()
            elif s == 'SON':
                ts = ts.loc[(ts.index.month == 9) | \
                            (ts.index.month == 10) | (ts.index.month == 11)]
                ts = ts.groupby(ts.index.year).mean()

            ax = axes[i, 1]
            h1 = ax.plot(ts.index, ts.values - ts.mean(), clist[i])
            if (i == 0):
                ax.set_title('Anomaly (m$^3$/m$^3$)')

            ax.set_xlim([year_longest[0], year_longest[-1]])
            ax.set_xlabel('Year')
            ax.set_ylim([-0.01, 0.01])
            ax.set_yticks(np.linspace(-0.01, 0.01, 5))

    fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot', 
                             'ts_global', 'em', model_set + '_' + me + '_' + \
                             str(year[0]) + '-' + str(year[-1]) + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)


for model_set in ['lsm', 'cmip5', 'cmip6', '2cmip', 'all']:
    if model_set == 'lsm':
        year_list = [year_longest, year_shorter, year_shorter2, year_shortest]
        method = ['year_month_9grid', 'year_month_anomaly_9grid']
    else:
        year_list = [year_longest]
        method = ['year_month_1grid', 'year_month_anomaly_1grid',
                  'year_month_9grid', 'year_month_anomaly_9grid']
    p = mp.Pool(4)
    p.map_async(plotter, list(it.product(year_list, method)))
    p.close()
    p.join()
