# -*- coding: utf-8 -*-
"""
2019/11/23

@author: ywang254

Extract the soil moisture from the emergent constraint weighted results.
"""
import utils_management as mg
from utils_management.constants import depth, depth_cm, lsm_list, \
    year_longest, year_shorter, year_shorter2, year_shortest, target_lat, \
    target_lon
import os
import xarray as xr
from misc.ismn_utils import get_weighted_monthly_data, \
    get_ismn_aggr_method
from misc.dolce_utils import get_weighted_sm_points
from glob import glob
import pandas as pd
import time
import itertools as it
import multiprocessing as mp


start = time.time()


def extract(option):
    ismn_aggr_method, year, i, model, opt = option

    year_str = str(year[0]) + '-' + str(year[-1])
    time = pd.date_range(str(year[0])+'-01-01', str(year[-1])+'-12-31',
                         freq = 'MS')

    d = depth[i]
    dcm = depth_cm[i]

    grid_latlon, _, _ = get_weighted_monthly_data(os.path.join( \
        mg.path_intrim_out(), 'ismn_aggr'), ismn_aggr_method, d, 'all')

    if 'anomaly' in opt:
        data = xr.open_dataset(os.path.join(mg.path_out(), 
                                            'em_' + model + '_corr', 
                                            'CRU_v4.03_' + \
                                            opt.replace('anomaly', 
                                                        'positive') + \
                                            '_' + dcm + '_' + year_str, 
                                            'predicted.nc'))
    else:
        data = xr.open_mfdataset([os.path.join(mg.path_out(),
                                               'em_' + model + '_corr',
                                               'CRU_v4.03_' + opt + \
                                               '_' + dcm + '_' + year_str,
                                               'predicted_' + str(y) + '.nc') \
                                  for y in year])
    sm = xr.DataArray(data.predicted.values.copy(), 
                      coords = {'time': time, 'lat': target_lat,
                                'lon': target_lon},
                      dims = ['time', 'lat', 'lon'])
    data.close()


    # ---- obtain values at the observed data points (do not subset
    #      the dates).
    sm_at_data = get_weighted_sm_points(grid_latlon, sm, year)
    sm_at_data.to_csv(os.path.join(mg.path_out(), 'at_obs_em_' + model, \
                                   'CRU_v4.03_' + opt + '_' + d + \
                                   '_' + year_str + '_' + ismn_aggr_method + \
                                   '.csv'))

pool = mp.Pool(4)
pool.map_async(extract, list(it.product(['simple', 'lu_weighted',
                                         'lu_weighted_above_40'],
                                        [year_longest, year_shorter, 
                                         year_shorter2, year_shortest],
                                        range(4), ['lsm'],
                                        ['year_month_9grid',
                                         'year_month_anomaly_9grid'])))
pool.map_async(extract, list(it.product(['simple', 'lu_weighted', 
                                         'lu_weighted_above_40'],
                                        [year_longest], range(4),
                                        ['cmip5', 'cmip6', '2cmip'],
                                        ['year_month_9grid',
                                         'year_month_anomaly_9grid'])))
                                         ##'year_month_1grid', 
                                         ##'year_month_anomaly_1grid'])))
pool.map_async(extract, list(it.product(['simple', 'lu_weighted',
                                         'lu_weighted_above_40'],
                                        [year_longest, year_shorter, 
                                         year_shorter2, year_shortest],
                                        range(4), ['all'],
                                        ['year_month_9grid',
                                         'year_month_anomaly_9grid'])))
                                         ##'year_month_1grid', 
                                         ##'year_month_anomaly_1grid'])))
pool.close()
pool.join()


# DEBUG
# The script finished in 0.6533 hours.
# extract([year_longest, 1, 'lsm', 'year_month_9grid'])


end = time.time()
print('The script finished in ' + ('%.4f' % ((end-start)/3600) ) + ' hours.')
