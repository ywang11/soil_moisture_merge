# -*- coding: utf-8 -*-
"""
Created on Thu May 30 16:25:28 2019

@author: ywang254

Estimate regression relationship between grid-scale precipitation, temperature,
 and modeled soil moisture at different depths, for each month in each year,
 or for each month in all years, using the nearby 1x1 (i.e. single), 3x3, 
 ... grid cells. 

Input data is the CMIP5 and CMIP6 Earth System Models together.

The by-month-by-year regression using a single grid cell does not have enough
data points, as suggested by "em_corr_test.plot.py". But pooling the nearby
grid cells, assuming that the precipitation-soil moisture relationship is
similar, might solve the issue and lead to some reasonable results.

Skip certain grid cells that do not have enough nearby variance to conduct
regression on the fly.

2019/10/28

Try parallel. Add the temperature predictor.

2019/11/21

Add the anomaly option.

2020/12/25

Changed the calculation of uncertainty interval to standard deviation-like.
"""
import utils_management as mg
from utils_management.constants import met_to_lsm, year_longest, \
    year_shorter, year_shorter2, year_shortest, lsm_list, depth, depth_cm, \
    target_lat, target_lon, year_cmip5, year_cmip6
from misc.spatial_utils import extract_grid
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import os
import xarray as xr
import pandas as pd
import numpy as np
import itertools as it
import time
import multiprocessing as mp
import statsmodels.api as stats
from statsmodels.sandbox.regression.predstd import wls_prediction_std


land_mask = 'vanilla'


###############################################################################
# Settings
###############################################################################
# Target period of interest.
year = year_longest
year_str = str(year[0]) + '-' + str(year[-1])
period = pd.date_range(str(year[0])+'-01-01', str(year[-1])+'-12-31',
                       freq = 'MS')

# Soil layer of interest.
i = REPLACE1 # [0, 1, 2, 3]
d = depth[i]
dcm = depth_cm[i]

# Earth system models relevant to this soil layer and target period.
model_cmip5 = cmip5_availability(dcm, land_mask)
model_cmip6 = list(set(mrsol_availability(dcm, land_mask, 'historical')) &\
                   set(mrsol_availability(dcm, land_mask, 'ssp585')))
model_cmip6 = sorted([x for x in model_cmip6 if 'r1i1p1f1' in x])

# Temporal range of data to use in the regression.
opt = 'REPLACE2' # ['year_month_9grid', 'year_month_1grid',
                 #  'year_month_anomaly_9grid', 'year_month_anomaly_1grid']


###############################################################################
# Preparations
###############################################################################
# Number x number nearby grids for the regression.
n_gr = int(opt.split('_')[-1][0])
n_sq_gr = int(np.sqrt(n_gr)) # [1, 3, ...]

# Observed precipitation and temperature dataset to use.
met_obs_name = 'CRU_v4.03'

# Prefix of the precipitation and temperature data that are to be used in
# the regression.
pr_list = []
tas_list = []
met_name_list = []
for m in model_cmip5:
    pr_list.append(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                land_mask, 'CMIP5', m, 'pr_'))
    tas_list.append(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                 land_mask, 'CMIP5', m, 'tas_'))
    met_name_list.append(m)
for m in model_cmip6:
    pr_list.append(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                land_mask, 'CMIP6', m, 'pr_'))
    tas_list.append(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                 land_mask, 'CMIP6', m, 'tas_'))
    met_name_list.append(m)
pr_list_uniq, temp_ind = np.unique(pr_list, return_index = True)
pr_list_uniq = list(pr_list_uniq)
tas_list_uniq = [tas_list[i] for i in temp_ind]
met_name_list_uniq = [met_name_list[i] for i in temp_ind]


###############################################################################
# Conduct the regression.
###############################################################################
def corr(yr):
## DEBUG
##for yr in range(2009,2011):
    ###########################################################################
    # Read the soil moisture data for regression, split into lat_bnd.
    ###########################################################################
    sm_data0 = np.full([len(model_cmip5) + len(model_cmip6), 12,
                       len(target_lat), len(target_lon)], np.nan)
    for m_ind, m in enumerate(model_cmip5 + model_cmip6):
        if m_ind < len(model_cmip5):
            if yr <= year_cmip5[-1]:
                fname = os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                     land_mask, 'CMIP5', m,
                                     'sm_historical_r1i1p1_' + str(yr) + \
                                     '_' + dcm + '.nc')
            else:
                fname = os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                     land_mask, 'CMIP5', m,
                                     'sm_rcp85_r1i1p1_' + str(yr) + \
                                     '_' + dcm + '.nc')
        else:
            if yr <= year_cmip6[-1]:
                fname = os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                     land_mask, 'CMIP6', m,
                                     'mrsol_historical_' + str(yr) + \
                                     '_' + dcm + '.nc')
            else:
                fname = os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                     land_mask, 'CMIP6', m,
                                     'mrsol_ssp585_' + str(yr) + \
                                     '_' + dcm + '.nc')
        data = xr.open_dataset(fname, decode_times = False)
        if data.lat.values[0] > -87:
            # Guard against non-global data
            temp = target_lat >= data.lat.values[0]
            sm_data0[m_ind, :, temp, :] = data.sm.values.copy()
        else:
            sm_data0[m_ind, :, :, :] = data.sm.values.copy()
        data.close()

        # Remove the climatology if specified.
        if 'anomaly' in opt:
            if m_ind < len(model_cmip5):
                data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 
                                                    'em_tas_corr_climatology',
                                                    'sm_cmip5_' + m + '_' + \
                                                    dcm + '.nc'))
                sm_data0[m_ind, :, :, :] -= data['sm'].values
                data.close()
            else:
                data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 
                                                    'em_tas_corr_climatology',
                                                    'sm_cmip6_' + m + '_' + \
                                                    dcm + '.nc'))
                sm_data0[m_ind, :, :, :] -= data['sm'].values
                data.close()

    # Average over the different LSMs that have common forcing.
    sm_data = np.full([len(pr_list_uniq), 12, len(target_lat),
                        len(target_lon)], np.nan)
    for p_ind, p in enumerate(pr_list_uniq):
        p_ind_list = [i for i,x in enumerate(pr_list) if x == p]
        sm_data[p_ind, :, :, :] = np.mean(sm_data0[p_ind_list, :, :, :],
                                          axis = 0)

    ###########################################################################
    # Read the precipitation data for regression.
    ###########################################################################
    pr_data = np.full([len(pr_list_uniq), 12, len(target_lat),
                       len(target_lon)], np.nan)
    for p_ind, p in enumerate(pr_list_uniq):
        if 'CMIP5' in p:
            if yr <= year_cmip5[-1]:
                fname = p + 'historical_r1i1p1_' + str(yr) + '.nc'
            else:
                fname = p + 'rcp85_r1i1p1_' + str(yr) + '.nc'
        elif 'CMIP6' in p:
            if yr <= year_cmip6[-1]:
                fname = p + 'historical_' + str(yr) + '.nc'
            else:
                fname = p + 'ssp585_' + str(yr) + '.nc'

        ##p_ind_list = [i for i,x in enumerate(pr_list) if x == p]

        data = xr.open_dataset(fname, decode_times = False)
        if data.lat.values[0] > -87:
            # Guard against non-global data
            temp = target_lat >= data.lat.values[0]
            ##for pp in p_ind_list:
            pr_data[p_ind, :, temp, :] = data.pr.values.copy()
        else:
            ##for pp in p_ind_list:
            pr_data[p_ind, :, :, :] = data.pr.values.copy()
        data.close()

        # Remove the climatology if specified.
        if 'anomaly' in opt:
            if 'CMIP5' in p:
                data = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                                    'em_tas_corr_climatology',
                                                    'pr_cmip5_' + \
                                                    met_name_list_uniq[p_ind]\
                                                    + '.nc'))
            elif 'CMIP6' in p:
                data = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                                    'em_tas_corr_climatology',
                                                    'pr_cmip6_' + \
                                                    met_name_list_uniq[p_ind]\
                                                    + '.nc'))
            pr_data[p_ind, :, :, :] -= data['pr'].values
            data.close()


    ###########################################################################
    # Read the temperature data for regression.
    ###########################################################################
    tas_data = np.full([len(tas_list_uniq), 12, len(target_lat), 
                        len(target_lon)], np.nan)
    for p_ind, p in enumerate(tas_list_uniq):
        if 'CMIP5' in p:
            if yr <= year_cmip5[-1]:
                fname = p + 'historical_r1i1p1_' + str(yr) + '.nc'
            else:
                fname = p + 'rcp85_r1i1p1_' + str(yr) + '.nc'
        elif 'CMIP6' in p:
            if yr <= year_cmip6[-1]:
                fname = p + 'historical_' + str(yr) + '.nc'
            else:
                fname = p + 'ssp585_' + str(yr) + '.nc'

        ##t_ind_list = [i for i,x in enumerate(tas_list) if x == p]

        data = xr.open_dataset(fname, decode_times = False)
        if data.lat.values[0] > -87:
            # Guard against non-global data
            temp = target_lat >= data.lat.values[0]
            temp2 = data.tas.values.copy()
            ##for tt in t_ind_list:
            tas_data[p_ind, :, temp, :] = temp2
        else:
            temp = data.tas.values.copy()
            ##for tt in t_ind_list:
            tas_data[p_ind, :, :, :] = temp
        data.close()

        # Remove the climatology if specified.
        if 'anomaly' in opt:
            if 'CMIP5' in p:
                data = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                                    'em_tas_corr_climatology',
                                                    'tas_cmip5_' + \
                                                    met_name_list_uniq[p_ind] \
                                                    + '.nc'))
            elif 'CMIP6' in p:
                data = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                                    'em_tas_corr_climatology',
                                                    'tas_cmip6_' + \
                                                    met_name_list_uniq[p_ind] \
                                                    + '.nc'))
            tas_data[p_ind, :, :, :] -= data['tas'].values
            data.close()


    ###########################################################################
    # Read the observed precipitation and temperature data.
    ###########################################################################
    data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                        land_mask, met_obs_name, 
                                        'pr_' + str(yr) + '.nc'),
                           decode_times = False)
    if data.lat.values[0] > -87:
        pr_obs = np.full([12, len(target_lat), len(target_lon)])
        temp = target_lat >= data.lat.values[0]
        pr_obs[:, temp, :] = data.pr.values.copy()
    else:
        pr_obs = data.pr.values.copy()
    data.close()

    # Remove the climatology if specified.
    if 'anomaly' in opt:
        data = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                            'em_tas_corr_climatology',
                                            'pr_CRU_v4.03_1981-2010.nc'))
        pr_obs -= data['pr'].values
        data.close()

    data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                        land_mask, met_obs_name, 
                                        'tas_' + str(yr) + '.nc'),
                           decode_times = False)
    if data.lat.values[0] > -87:
        tas_obs = np.full([12, len(target_lat), len(target_lon)])
        temp = target_lat >= data.lat.values[0]
        tas_obs[:, temp, :] = data.tas.values.copy()
    else:
        tas_obs = data.tas.values.copy()
    data.close()

    # Remove the climatology if specified.
    if 'anomaly' in opt:
        data = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                            'em_tas_corr_climatology',
                                            'tas_CRU_v4.03_1981-2010.nc'))
        tas_obs -= data['tas'].values
        data.close()


    ###########################################################################
    # Prepare the output variables.
    ###########################################################################
    # Correlation coefficients, p-values, and the 95% confidence intervals
    # arranged in the order of: intercept, precipitation, temperature.
    corr = np.full([3, 12, len(target_lat), len(target_lon)],
                   np.nan, dtype = float)
    corr_p = corr.copy()
    corr_CI_lower = corr.copy()
    corr_CI_upper = corr.copy()

    # Predicted values, using CRU_Merged precipitation as the constraint,
    # and the 95% confidence intervals.
    predicted = np.full([12, len(target_lat), len(target_lon)], 
                        np.nan, dtype = float)
    predicted_std = predicted.copy()

    # Indicator whether emergent constraint was applied on this grid.
    # First slide is precipitation. Second slice is temperature.
    # 0 - applied
    # 1 - not applied due to insufficient variability in predictor
    # 2 - not applied due to insigificant correlation coefficient
    em_not_applied = np.full([2, 12, len(target_lat), len(target_lon)],
                             np.nan, dtype = float)

    for a, b in it.product(range(pr_obs.shape[1]), range(pr_obs.shape[2])):
        if np.isnan(pr_obs[0, a, b]):
            continue

        ##print(str(a) + ', ' + str(b))

        for t in range(12):
            if n_sq_gr == 3:
                if a == 0:
                    sub_a = range(a, a+2)
                elif a == (pr_obs.shape[1] - 1):
                    sub_a = range(a-1, a+1)
                else:
                    sub_a = range(a-1, a+2)
                if b == 0:
                    sub_b = range(b, b+2)
                elif b == (pr_obs.shape[2] - 1):
                    sub_b = range(b-1, b+1)
                else:
                    sub_b = range(b-1, b+2)
                n_valid = len(sub_a) * len(sub_b)
                X = np.concatenate([np.ones([pr_data.shape[0] * n_valid, 1]), 
                                    pr_data[:,t,:,:][:,sub_a,:][:, 
                                            :,sub_b].reshape(-1,1),
                                    tas_data[:,t,:,:][:,sub_a,:][:,
                                             :,sub_b].reshape(-1,1)],
                                   axis = 1)
                y = sm_data[:,t,:,:][:,sub_a,:][:,:,sub_b].reshape(-1, 1)
                ymean = np.nanmean( sm_data[:,t,:,
                                            :][:,sub_a,
                                               :][:,:,sub_b].reshape(-1) )
                ystd = np.nanstd( sm_data0[:,t,:,
                                           :][:,sub_a,
                                              :][:,:,sub_b].reshape(-1) )
            else:
                X = np.concatenate([np.ones([pr_data.shape[0],
                                             1]).reshape(-1,1),
                                    pr_data[:,t,a,b].reshape(-1, 1), 
                                    tas_data[:,t,a,b].reshape(-1, 1)],
                                   axis = 1)
                y = sm_data[:,t,a,b].reshape(-1, 1)
                ymean = np.nanmean(sm_data[:,t,a,b])
                ystd = np.nanstd(sm_data0[:,t,a,b])

            ###############################
            # Regression cases.
            #
            # Logical structure of this section:
            #Exclude precipitation
            #    Exclude temperature
            #    Can include temperature
            #        Regression w/ temperature does not work
            #            Regression w/ temperature works
            #Can include precipitation
            #    Exclude temperature
            #        Regression w/ precipitation does not work
            #    Regression w/ precipitation works
            #    Can include temperature
            #Both significant
            #At least one not significant
            #Pr-only is significant, and more significant than Tas-only
            #Tas-only is significant, and more significant than Pr-only
            #Neither is significant
            ###############################
            not_nan = ~(np.isnan(X).any(axis = 1) | np.isnan(y.reshape(-1)))
            X = X[not_nan, :]
            y = y[not_nan]

            if np.std(X[:,1]) < 1e-3:
                em_not_applied[0, t, a, b] = 1
                if np.std(X[:,2]) < 1e-3:
                    em_not_applied[1, t, a, b] = 1
                    # Fill with mean and standard deviation.
                    predicted[t, a, b] = ymean
                    predicted_std[t, a, b] = ystd
                else:
                    # Try regression, excluding pr.
                    mod = stats.OLS(y, X[:, [0,2]])
                    result = mod.fit()

                    if result.pvalues[1] > 0.05:                        
                        em_not_applied[1, t, a, b] = 2
                        # Fill with mean and standard deviation.
                        predicted[t, a, b] = ymean
                        predicted_std[t, a, b] = ystd
                    else:
                        em_not_applied[1, t, a, b] = 0
                        # Fill with regression results.
                        CI = result.conf_int()

                        corr[[0,2], t, a, b] = result.params
                        corr_p[[0,2], t, a, b] = result.pvalues
                        corr_CI_lower[[0,2], t, a, b] = CI[:, 0]
                        corr_CI_upper[[0,2], t, a, b] = CI[:, 1]

                        exog = np.array([1, tas_obs[t, a, b]]).reshape(1, 2)
                        predicted[t, a, b] = result.predict(exog)
                        prstd, iv_l, iv_u = wls_prediction_std(result, 
                                                               exog = exog)
                        predicted_std[t, a, b] = prstd
            else:
                if np.std(X[:,2]) < 1e-3:
                    em_not_applied[1, t, a, b] = 1

                    # Try regression, excluding tas.
                    mod = stats.OLS(y, X[:, [0,1]])
                    result = mod.fit()

                    if result.pvalues[1] > 0.05:
                        em_not_applied[0, t, a, b] = 2
                        # Fill with mean and standard deviation.
                        predicted[t, a, b] = ymean
                        predicted_std[t, a, b] = ystd
                    else:
                        em_not_applied[0, t, a, b] = 0
                        # Fill with regression result.
                        CI = result.conf_int()

                        corr[[0,1], t, a, b] = result.params
                        corr_p[[0,1], t, a, b] = result.pvalues
                        corr_CI_lower[[0,1], t, a, b] = CI[:, 0]
                        corr_CI_upper[[0,1], t, a, b] = CI[:, 1]

                        exog = np.array([1, pr_obs[t, a, b]]).reshape(1, 2)
                        predicted[t, a, b] = result.predict(exog)
                        prstd, iv_l, iv_u = wls_prediction_std(result, 
                                                               exog = exog)
                        predicted_std[t, a, b] = prstd
                else:
                    # Try regression.
                    # (1) Both
                    mod = stats.OLS(y, X)
                    result = mod.fit()
                    pvalues = result.pvalues

                    if (pvalues[1] <= 0.05) & (pvalues[2] <= 0.05):
                        em_not_applied[0, t, a, b] = 0
                        em_not_applied[1, t, a, b] = 0

                        # Fill with regression results.
                        CI = result.conf_int()

                        corr[:, t, a, b] = result.params
                        corr_p[:, t, a, b] = pvalues
                        corr_CI_lower[:, t, a, b] = CI[:, 0]
                        corr_CI_upper[:, t, a, b] = CI[:, 1]

                        exog = np.array([1, pr_obs[t, a, b],
                                         tas_obs[t, a, b]]).reshape(1, 3)
                        predicted[t, a, b] = result.predict(exog)
                        prstd, iv_l, iv_u = wls_prediction_std(result, 
                                                               exog = exog)
                        predicted_std[t, a, b] = prstd
                    else:
                        # (2) Pr-only
                        mod2 = stats.OLS(y, X[:, :2])
                        result2 = mod2.fit()
                        pvalues2 = result2.pvalues
                        # (3) Tas-only
                        mod3 = stats.OLS(y, X[:, [0,2]])
                        result3 = mod3.fit()
                        pvalues3 = result3.pvalues

                        pvalues_min = min(pvalues2[1], pvalues3[1])

                        if pvalues_min <= 0.05:
                            if pvalues2[1] < pvalues3[1]:
                                # Fit Pr-only
                                em_not_applied[0, t, a, b] = 0
                                em_not_applied[1, t, a, b] = 2

                                CI = result2.conf_int()

                                corr[[0,1], t, a, b] = result2.params
                                corr_p[[0,1], t, a, b] = pvalues2
                                corr_CI_lower[[0,1], t, a, b] = CI[:, 0]
                                corr_CI_upper[[0,1], t, a, b] = CI[:, 1]

                                exog = np.array([1, pr_obs[t, a, b]] \
                                ).reshape(1, 2)
                                predicted[t, a, b] = result2.predict(exog)
                                prstd, iv_l, iv_u = \
                                    wls_prediction_std(result2, exog = exog)
                                predicted_std[t, a, b] = prstd
                            else:
                                # Fit Tas-only
                                em_not_applied[0, t, a, b] = 2
                                em_not_applied[1, t, a, b] = 0

                                CI = result3.conf_int()

                                corr[[0,2], t, a, b] = result3.params
                                corr_p[[0,2], t, a, b] = pvalues3
                                corr_CI_lower[[0,2], t, a, b] = CI[:, 0]
                                corr_CI_upper[[0,2], t, a, b] = CI[:, 1]

                                exog = np.array([1, tas_obs[t, a, b]] \
                                ).reshape(1, 2)
                                predicted[t, a, b] = result3.predict(exog)
                                prstd, iv_l, iv_u = \
                                    wls_prediction_std(result3, exog = exog)
                                predicted_std[t, a, b] = prstd
                        else:
                            # Fill with mean and standard deviation.
                            em_not_applied[0, t, a, b] = 2
                            em_not_applied[1, t, a, b] = 2

                            predicted[t, a, b] = ymean
                            predicted_std[t, a, b] = ystd


    ###########################################################################
    # Save the correlation coefficients, confidence interval, and significance
    # to netcdf file.
    ###########################################################################
    path_folder = os.path.join(mg.path_out(), 'em_2cmip_corr',
                               met_obs_name + '_' + opt + '_' + dcm + '_' + \
                               year_str)
    if not os.path.exists(path_folder):
        os.mkdir(path_folder)
    subperiod = period[period.year == yr]

    xr.DataArray(corr, coords = {'reg': range(3), 'time': subperiod,
                                 'lat': target_lat, 'lon': target_lon}, 
                 dims = ['reg', 'time', 'lat',
                         'lon']).to_dataset(name = 'corr' \
                 ).to_netcdf(os.path.join(path_folder, 
                                          'corr_' + str(yr) + '.nc'))
    xr.DataArray(corr_p, coords = {'reg': range(3), 'time': subperiod,
                                   'lat': target_lat, 'lon': target_lon}, 
                 dims = ['reg', 'time', 'lat', 
                         'lon']).to_dataset(name = 'corr_p' \
                 ).to_netcdf(os.path.join(path_folder, 
                                          'corr_p_'+str(yr)+'.nc'))
    xr.DataArray(corr_CI_lower, coords = {'reg': range(3), 'time': subperiod,
                                          'lat': target_lat,
                                          'lon': target_lon},
                 dims = ['reg', 'time', 'lat', 
                         'lon']).to_dataset(name = 'corr_CI_lower' \
                 ).to_netcdf(os.path.join(path_folder, 'corr_CI_lower_' + \
                                          str(yr) + '.nc'))
    xr.DataArray(corr_CI_upper, coords = {'reg': range(3), 'time': subperiod,
                                          'lat': target_lat,'lon': target_lon},
                 dims = ['reg', 'time', 'lat',
                         'lon']).to_dataset(name = 'corr_CI_upper'\
                 ).to_netcdf(os.path.join(path_folder, 'corr_CI_upper_' + \
                                          str(yr) + '.nc'))
    xr.DataArray(em_not_applied, coords = {'reg': range(1,3),
                                           'time': subperiod, 
                                           'lat': target_lat,
                                           'lon': target_lon},
                 dims = ['reg', 'time', 'lat',
                         'lon']).to_dataset(name = 'em_not_applied'\
                ).to_netcdf(os.path.join(path_folder, 'em_not_applied_' + \
                                         str(yr) + '.nc'))
    xr.DataArray(predicted, coords = {'time': subperiod,
                                      'lat': target_lat, 'lon': target_lon}, 
                 dims = ['time', 'lat', 'lon']).to_dataset(name = 'predicted' \
                 ).to_netcdf(os.path.join(path_folder, 
                                          'predicted_' + str(yr) + '.nc'))
    xr.DataArray(predicted_std, coords = {'time': subperiod, 
                                               'lat': target_lat, 
                                               'lon': target_lon}, 
                 dims = ['time', 'lat', 'lon'] \
    ).to_dataset(name = 'predicted_std' \
    ).to_netcdf(os.path.join(path_folder, 
                             'predicted_std_' + str(yr) + '.nc'))


###############################################################################
# Collect the results of regression.
###############################################################################
pool = mp.Pool(6)
pool.map_async(corr, year) 
pool.close()
pool.join()
