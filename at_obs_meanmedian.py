# -*- coding: utf-8 -*-
"""
Created on Thu May 16 19:19:40 2019

@author: ywang254

Extract the soil moisture from the unweighted mean, and median of the 
land surface model datasets.
"""
import utils_management as mg
from utils_management.constants import depth, depth_cm, year_longest, \
    year_shorter, year_shorter2, year_shortest, lsm_list
import os
import xarray as xr
from misc.ismn_utils import get_weighted_monthly_data, \
    get_ismn_aggr_method
from misc.dolce_utils import get_cov_method, get_weighted_sm_points
from glob import glob
import pandas as pd
import time


start = time.time()


# Select the time range, which affects the set of land surface models that
# the mean or median build upon.
year = REPLACE1 ## year_longest, year_shorter, year_shorter2, year_shortest
time_range = pd.date_range(start=str(year[0])+'-01-01',
                           end=str(year[-1])+'-12-31', freq='MS')


# Choose the set of observational data points to compare to.
iam = REPLACE2
simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = get_ismn_aggr_method(simple[iam],
                                        dominance_lc[iam],
                                        dominance_threshold)


#
model_set = 'REPLACE3'


for i,d in enumerate(depth):
    gridded_datasets = dict([('mean', 
                              os.path.join(mg.path_out(),
                                           'meanmedian_' + model_set, 
                                           'mean_' + d + '_' + \
                                           str(year[0]) + '-' + \
                                           str(year[-1]) + '.nc')),
                             ('median', 
                              os.path.join(mg.path_out(), 
                                           'meanmedian_' + model_set,
                                           'median_' + d + '_' + \
                                           str(year[0]) + '-' + \
                                           str(year[-1]) + '.nc'))])

    for j,f in gridded_datasets.items():
        data = xr.open_dataset(f, decode_times=False)
        weighted_sm = data.sm.copy(deep=True)
        data.close()

        grid_latlon, _, _ = get_weighted_monthly_data( \
            os.path.join(mg.path_intrim_out(), 'ismn_aggr'),
            ismn_aggr_method, d, 'all')

        # ---- obtain values at the observed data points
        weighted_sm_at_data = get_weighted_sm_points(grid_latlon, 
                                                     weighted_sm, year)
        weighted_sm_at_data.to_csv(os.path.join(mg.path_out(),
                                                'at_obs_meanmedian_' + \
                                                model_set,
                                                j + '_' + d + '_' + \
                                                str(year[0]) + '-' + \
                                                str(year[-1]) + '_' + \
                                                ismn_aggr_method + '.csv'))

end = time.time()
print('The script finished in ' + ('%.4f' % ((end-start)/3600) ) + ' hours.')
