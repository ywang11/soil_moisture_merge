"""
2019/08/01

ywang254@utk.edu

Combine the individual minimum land masks (land surface models, CMIP5 models,
precipitation data, etc.) and create a merged minimum land mask.
"""
import sys
import os
import xarray as xr
import numpy as np
from glob import glob
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import utils_management as mg
from utils_management.constants import year_cmip6


target_lat = np.arange(-89.75, 89.76, 0.5)
target_lon = np.arange(-179.75, 179.76, 0.5)

land_mask = 'vanilla'

to_merge = ['cmip6_da', 'cmip6', 'cmip5', 'lsm']


# Build the list of masks of interpolated, but un-masked precipitation file.
mask_all = np.zeros([len(to_merge), 360, 720], dtype = bool)
for ds_ind, ds in enumerate(to_merge):
    data = xr.open_dataset(os.path.join(mg.path_intrim_out(),
                                        'land_mask_minimum', 
                                        ds + '.nc'))
    mask_all[ds_ind, :, :] = data.mask.values
    data.close()


# Intersect with the land_mask: SYNMAP or e1m.
if land_mask == 'SYNMAP':
    f = os.path.join(mg.path_data(), 'LULC',
                     'SYNMAP Global Potential Vegetation', 'data',
                     '112a942ec4294e5284e63d5e6bf14b29.nc')
    data = xr.open_dataset(f)
    # Need to reverse the latitude
    mask_temp = (data.biome_type[::-1,:].values != 47) & \
                (data.biome_type[::-1,:].values != 0)
    mask_all = np.concatenate([mask_all,
                               mask_temp.reshape(1, mask_temp.shape[0],
                                                 mask_temp.shape[1])],
                              axis = 0)
    data.close()
elif land_mask == 'elm':
    f = os.path.join(mg.path_data(), 'Global_Masks',
                     'elm_half_degree_grid_negLon.nc')
    data = xr.open_dataset(f)
    mask_temp = data.landfrac.values > 0
    mask_all = np.concatenate([mask_all,
                               mask_temp.reshape(1, mask_temp.shape[0],
                                                 mask_temp.shape[1])],
                              axis = 0)
    data.close()
elif land_mask == 'vanilla':
    # Use the output of "lu_aggr.py"
    f = os.path.join(mg.path_intrim_out(), 'lu_aggr', 'mcd12c1_0.5.nc')
    data = xr.open_dataset(f)

    # ---- >50% water body or >50% snow and ice
    mask_temp = ~((data.__xarray_dataarray_variable__.values[:,:,0] > 50) | \
                  (data.__xarray_dataarray_variable__.values[:,:,15] > 50))
    mask_all = np.concatenate([mask_all,
                               mask_temp.reshape(1, mask_temp.shape[0],
                                                 mask_temp.shape[1])],
                              axis = 0)
    data.close()
elif land_mask == 'vanilla':
    pass
else:
    raise('Unrecognized land mask ' + land_mask)


# Find the minimum mask, i.e. where all the mask == 1
mask_check = np.all(mask_all, axis=0)


# Save the minimum mask to file.
mask_check = xr.DataArray(mask_check, dims = ['lat', 'lon'], 
                          coords = {'lat': target_lat, 'lon': target_lon})
mask_check.to_dataset(name = 'mask' \
).to_netcdf(os.path.join(mg.path_intrim_out(), 'land_mask_minimum',
                         'merge_' + land_mask + '.nc'))


# Accompanying graph.
fig, ax = plt.subplots(figsize = (12, 12), 
                       subplot_kw = {'projection': ccrs.PlateCarree()})
ax.coastlines(color='r')
ax.gridlines(draw_labels = True)
mask_check.plot(cmap = 'Greys')
fig.savefig(os.path.join(mg.path_intrim_out(), 'land_mask_minimum', 
                         'merge_' + land_mask + '.png'), bbox_inches = 'tight',
            dpi = 600.)
plt.close(fig)
