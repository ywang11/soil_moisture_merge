# -*- coding: utf-8 -*-
"""
Created on Wed May  1 21:04:14 2019

@author: ywang254

Calculate the metrics between between the individual land surface models, and
the observed soil moisture.
"""
import os
import utils_management as mg
import pandas as pd
from utils_management.constants import depth, depth_cm, lsm_list, \
    year_longest, year_shorter, year_shorter2, year_shortest
from misc.ismn_utils import get_ismn_aggr_method, get_weighted_monthly_data, \
    get_weighted_monthly_data_USonly
from misc.analyze_utils import calc_stats
import numpy as np
import itertools as it
import time


# List the weighting methods that are used to generate the weighted
# averaged ISMN data.
simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = [get_ismn_aggr_method(simple[i], dominance_lc[i], 
                                         dominance_threshold) for i in \
                    range(len(simple))]
year_list = [year_longest, year_shorter, year_shorter2, year_shortest]
ismn_set = 'val'
iam = 'lu_weighted'
land_mask = 'vanilla'


start = time.time()
for i,year in it.product(range(len(depth)), year_list):
    d = depth[i]
    dcm = depth_cm[i]

    # Check existence.
    lsm = lsm_list[(str(year[0]) + '-' + str(year[-1]), d)]

    #
    ismn_info = pd.read_csv(os.path.join(mg.path_intrim_out(), 'ismn_summary',
                                         'ismn_landuse_continent_' + d + \
                                         '_' + iam + '_' + ismn_set + '.csv'),
                            index_col = 0)

    #
    for clas_abbr, clas in zip(['land_use_modis','continent'],
                               ['MODIS Land Use','Continent']):
        region = ismn_info.loc[~np.isnan(ismn_info[clas]), clas].unique()

        metrics_collect = pd.DataFrame(data = np.nan, index = lsm,
            columns = pd.MultiIndex.from_product( \
                [['RMSE','Bias','uRMSE','Corr'],
                 ['%d' % c for c in region]], names = ['Metric', 'ORS']))

        #
        for l,c in it.product(lsm, region):
            ###################################################################
            # Get the observed data.
            ###################################################################
            grid_latlon, weighted_monthly_data, _ = \
                get_weighted_monthly_data(os.path.join(mg.path_intrim_out(),
                                                       'ismn_aggr'), iam, d, 
                                          ismn_set)

            c_station = ismn_info.index[np.abs(ismn_info[clas] - c) < 1e-6]
            obs0 = weighted_monthly_data.loc[:, c_station].values.reshape(-1)

            ###################################################################
            # Performance of the weighted product.
            ###################################################################
            data = pd.read_csv(os.path.join(mg.path_intrim_out(),
                                            'at_obs_lsm',
                                            iam + '_' + l + '_' + d + '.csv'), 
                               index_col = 0, parse_dates = True)
            data = data.loc[(data.index >= str(year[0])+'-01-01') & \
                            (data.index <= str(year[-1])+'-12-31'), :]
            # ---- obtain values at the observed data points
            weighted_sm_at_data = data.loc[weighted_monthly_data.index, 
                                           c_station]

            metrics_collect.loc[l, ('RMSE',c)], \
                metrics_collect.loc[l, ('Bias',c)], \
                metrics_collect.loc[l, ('uRMSE',c)], \
                metrics_collect.loc[l, ('Corr',c)] = \
                    calc_stats(weighted_sm_at_data.values.reshape(-1),
                               obs0)

        metrics_collect.to_csv(os.path.join(mg.path_out(),
                                            'standard_metrics', 
                                            'by_' + clas_abbr + \
                                            '_lsm_' + d + '_' + \
                                            str(year[0]) + '-' + \
                                            str(year[-1]) + '.csv'))
end = time.time()
print('The script finished in ' + ('%.4f' % ((end-start)/3600) ) + ' hours.')
