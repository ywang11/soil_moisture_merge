"""
2019/07/23

ywang254@utk.edu

Compare the standard metrics between the un-merged 1950-2016 results, 
 the merged results, and the concatenated un-merged results.
"""
import os
import sys
import pandas as pd
import numpy as np
from utils_management.constants import depth, year_longest, year_shorter, \
    year_shorter2, year_shortest
import utils_management as mg
import matplotlib.pyplot as plt
from misc.ismn_utils import get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
import itertools as it


# MODIFY
USonly = False
if USonly:
    suffix = '_USonly'
else:
    suffix = ''

year = year_longest


# Covariance methods
cov_method = [get_cov_method(i) for i in range(5)]


# Separate plot for each metric.
for metric in ['RMSE', 'Bias', 'uRMSE', 'Corr']:
    fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(10,10),
                             sharex=True, sharey=True)
    for d_ind, d in enumerate(depth):
        ax = axes.flat[d_ind]

        for cov_ind, cov in enumerate(cov_method):
            original = pd.read_csv(os.path.join(mg.path_out(), 
                                                'standard_metrics', 
                                                'dolce_lsm_' + d + '_' + \
                                                str(year[0]) + '-' + \
                                                str(year[-1]) + suffix + \
                                                '.csv'),
                                   header = [0,1], index_col = [0,1] \
            ).loc[(slice(None), cov), (metric, slice(None))].values

            concat = pd.read_csv(os.path.join(mg.path_out(), 
                                              'standard_metrics', 
                                              'concat_no_merge_dolce_lsm_' + \
                                              d + '_' + str(year[0]) + '-' + \
                                              str(year[-1]) + suffix + '.csv'),
                                 header = [0,1], index_col = [0,1] \
            ).loc[(slice(None), cov), (metric, slice(None))].values

            merge = pd.read_csv(os.path.join(mg.path_out(), 
                                             'standard_metrics', 
                                             'concat_dolce_lsm_' + d + '_' + \
                                             str(year[0]) + '-' + \
                                             str(year[-1]) + suffix + '.csv'),
                                header = [0,1], index_col = [0,1] \
            ).loc[(slice(None), cov), (metric, slice(None))].values

            h1 = ax.plot([cov_ind - 0.2]*3, original, '.b')
            h2 = ax.plot([cov_ind]*3, concat, '.g')
            h3 = ax.plot([cov_ind + 0.2]*3, merge, '.r')

        ax.set_xticks(range(len(cov_method)))
        ax.set_xticklabels(cov_method, rotation = 30.)
        ax.set_title('Depth: ' + d)
        ax.set_ylabel(metric)

    ax.legend([h1[0], h2[0], h3[0]], ['Original', 'Concat', 'Merged'], 
              loc = 'lower center', bbox_to_anchor = (-1., -0.5))

    fig.savefig(os.path.join(mg.path_out(), 'standard_metrics', 
                             'plot_compare_concat_dolce_lsm_' + metric + \
                             suffix + '.png'), dpi = 600., bbox_inches='tight')
    plt.close(fig)
