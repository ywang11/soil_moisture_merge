"""
2019/07/05

ywang254@utk.edu

Map the global mean and seasonal mean soil moisture for the mean and median
  of the land surface models/CMIP5/CMIP6.
"""
from utils_management.constants import year_longest, depth
import utils_management as mg
from misc.plot_standard_diagnostics_utils import plot_trend_map
import numpy as np
import os
import itertools as it
from misc.plot_utils import cmap_gen
import multiprocessing as mp


model_set = ['all', 'lsm', 'cmip5', 'cmip6', '2cmip']
levels = np.linspace(-0.0004, 0.0004, 21)
cmap = cmap_gen('autumn', 'winter_r')
year = year_longest

def plotter(model):
    prefix_in = os.path.join(mg.path_out(),
                             'standard_diagnostics_meanmedian_' + model,
                             'mean_' + str(year[0]) + '-' + str(year[-1]) \
                             + '_')
    prefix_out = os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                              'trend_map_meanmedian', model + '_mean')
    plot_trend_map(depth, prefix_in, prefix_out, levels, cmap)


    prefix_in = os.path.join(mg.path_out(),
                             'standard_diagnostics_meanmedian_' + model,
                             'median_' + str(year[0]) + '-' + str(year[-1]) \
                             + '_')
    prefix_out = os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                              'trend_map_meanmedian', model + '_median')
    plot_trend_map(depth, prefix_in, prefix_out, levels, cmap)


p = mp.Pool(4)
p.map_async(plotter, model_set)
p.close()
p.join()
