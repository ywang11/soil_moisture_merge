# -*- coding: utf-8 -*-
"""
Created on Wed May  8 16:18:58 2019

@author: ywang254
"""
import xarray as xr
import os
import utils_management as mg
from utils_management.constants import depth, depth_cm, year_cmip6
from misc.cmip6_utils import mrsol_availability, one_layer_availability


land_mask = 'vanilla'


# ---- function to get the unweighted soil moisture 
def calc_cmip6_sm_mean(cmip6, year, dcm):
    count = 0
    for l in cmip6:
        data = xr.open_mfdataset([os.path.join(mg.path_intrim_out(), 
                                               'Interp_Merge', land_mask, 
                                               'CMIP6', l, 
                                               'mrsol_historical_' + \
                                               str(x) + '_' + dcm + '.nc') \
                                  for x in year], decode_times = False)
        if count == 0:
            mean_sm = data.sm.copy(deep=True)
        else:
            mean_sm = data.sm + mean_sm
        data.close()
        count += 1
    mean_sm = mean_sm / count
    return mean_sm


for i,d in enumerate(depth):
    dcm = depth_cm[i]

    # ---- the models that have precipitation.
    cmip6_list_1 = mrsol_availability(dcm, land_mask, 'historical')
    cmip6_list_2 = one_layer_availability('pr', land_mask, 'historical')
    # ---- further subset to the given depth.
    cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) )
    # ---- further subset to the first ensemble member.
    cmip6_list = [x for x in cmip6_list if 'r1i1p1f1' in x]

    mean_sm = calc_cmip6_sm_mean(cmip6_list, year_cmip6, dcm)

    mean_sm.attrs['Source Models'] = ' '.join(cmip6_list)

    mean_sm.to_dataset(name='sm').to_netcdf( \
        os.path.join(mg.path_out(), 'cmip6_unweighted_mean',
                     'average_' + d + '.nc'))
