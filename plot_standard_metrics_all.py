"""
2019/07/23

ywang254@utk.edu

Plot the RMSE, Bias, uRMSE, and Corr of the different weighted results,
 the range of LSM, CMIP5, CMIP6 models, and highlight the ESA-CCI, 
 GLEAMv3.3a, and the ERA-Land results in the LSM models.

Use the best emergent constraint and DOLCE weighting setups.
"""
import os
import sys
import pandas as pd
import numpy as np
from utils_management.constants import depth, depth_cm, year_longest, \
    year_shorter, year_shorter2, year_shortest, lsm_list, year_cmip5, \
    year_cmip6
import utils_management as mg
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import cm
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability, one_layer_availability
from misc.ismn_utils import get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
import itertools as it


# MODIFY
USonly = False # if True, plot the evaluation results for CONUS
if USonly:
    suffix = '_USonly'
else:
    suffix = ''


land_mask = 'vanilla'
model_list = ['lsm', 'cmip5', 'cmip6', '2cmip', 'all']

#cmap = cm.get_cmap('Spectral')
#clist = [cmap((i+0.1)/len(model_list)) for i in range(len(model_list))]
clist = ['#e41a1c', '#377eb8', '#4daf4a', '#984ea3', '#ff7f00']

metric_list = ['RMSE', 'Bias', 'uRMSE', 'Corr']
unit_list = ['($m^3/m^3$)', '($m^3/m^3$)', '($m^3/m^3$)', '(-)']


###############################################################################
# Separate plots for the following factors and depth
###############################################################################
period = ['all', 'cal', 'val']


###############################################################################
# Separate panels for the following factors
###############################################################################
# Time periods
year = year_longest # Only plot the concatenated LSM and ALL products.
year_str = str(year[0]) + '-' + str(year[-1])


# ISMN data setup options
simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = [get_ismn_aggr_method(simple[i], dominance_lc[i], 
                                         dominance_threshold) for i in \
                    range(len(simple))]


###############################################################################
# Weighting method setups: use the best that are identified in the 
# "standard_metrics_compare_*.py"
###############################################################################
em = 'year_month_anomaly_9grid'
cov = 'ShrunkCovariance'


###############################################################################
# Plot loop.
###############################################################################
mpl.rcParams.update({'font.size': 10})
for pr, metric, iam in it.product(period, metric_list, ismn_aggr_method):
    fig, axes = plt.subplots(nrows = 2, ncols = 2, figsize = (6., 6.),
                             sharex = True, sharey = True)

    for count, d in enumerate(depth):
        ax = axes.flat[count]
        dcm = depth_cm[count]

        #######################################################################
        # All the land surface, CMIP5, CMIP6 models.
        #######################################################################
        metric_raw = {}
        metric_raw['lsm'] = pd.read_csv(os.path.join(mg.path_out(), 
                                                     'standard_metrics', 
                                                     'lsm_' + d + '_' + \
                                                     year_str + \
                                                     suffix + '.csv'),
                                        index_col = [0,1], 
                                        header = [0,1]).loc[iam, (metric,pr)]
        metric_raw['cmip5'] = pd.read_csv(os.path.join(mg.path_out(), 
                                                       'standard_metrics',
                                                       'cmip5_' + d + \
                                                       suffix + '.csv'),
                                          index_col = [0,1], 
                                          header = [0,1]).loc[iam, (metric,pr)]
        metric_raw['cmip6'] = pd.read_csv(os.path.join(mg.path_out(),
                                                       'standard_metrics',
                                                       'cmip6_' + d + '.csv'),
                                          index_col = [0,1],
                                          header = [0,1]).loc[iam, (metric,pr)]

        #######################################################################
        # Meanmedian
        #######################################################################
        metric_mean = {}
        for model in model_list:
            metric_mean[model] = pd.read_csv(os.path.join(mg.path_out(),
                                                          'standard_metrics',
                                                          'meanmedian_' + \
                                                          model + '_' + d + \
                                                          '_' + year_str + \
                                                          suffix + '.csv'),
                                             index_col = [0,1], 
                                             header = [0,1]).loc[(iam,'mean'),
                                                                 (metric,pr)]

        #######################################################################
        # DOLCE
        #######################################################################
        metric_dolce = {}
        for model in ['lsm', 'all']:
            metric_dolce[model] = pd.read_csv(os.path.join(mg.path_out(),
                                                           'standard_metrics',
                                                           'concat_dolce_' + \
                                                           model + '_' + d + \
                                                           '_' + year_str + \
                                                           suffix + '.csv'),
                                              index_col = [0,1], 
                                              header = [0,1]).loc[(iam,cov),
                                                                  (metric,pr)]
        for model in ['cmip5', 'cmip6', '2cmip']:
            metric_dolce[model] = pd.read_csv(os.path.join(mg.path_out(),
                                                           'standard_metrics',
                                                           'dolce_' + model + \
                                                           '_' + d + '_' + \
                                                           year_str + \
                                                           suffix + '.csv'),
                                              index_col = [0,1], 
                                              header = [0,1]).loc[(iam,cov),
                                                                  (metric,pr)]

        #######################################################################
        # Emergent constraint
        #######################################################################
        metric_em = {}
        for model in ['lsm', 'all']:
            metric_em[model] = pd.read_csv(os.path.join(mg.path_out(), 
                                                        'standard_metrics',
                                                        'concat_em_' + \
                                                        model + '_' + d + \
                                                        '_' + year_str + \
                                                        suffix + '.csv'),
                                           index_col = 0, 
                                           header = [0,1]).loc[iam, 
                                                               (metric,pr)]
        for model in ['cmip5', 'cmip6', '2cmip']:
            metric_em[model] = pd.read_csv(os.path.join(mg.path_out(),
                                                        'standard_metrics',
                                                        'em_' + \
                                                        model + '_' + d + \
                                                        '_' + year_str + \
                                                        suffix + '.csv'),
                                           index_col = [0,1],
                                           header = [0,1]).loc[(iam,em),
                                                               (metric,pr)]


        #######################################################################
        # Plot the individual source datasets.
        #######################################################################
        data_lsm_short = pd.read_csv(os.path.join(mg.path_out(),
                                                  'standard_metrics',
                                                  'lsm_' + d + '_' + \
                                                  str(year_shortest[0]) + '-' \
                                                  + str(year_shortest[-1]) + \
                                                  suffix + '.csv'),
                                     index_col = [0,1],
                                     header = [0,1]).loc[iam, (metric,pr)]
        if (d == '0.00-0.10'):
            data_esa_cci = data_lsm_short.loc['ESA-CCI']
            data_gleam = data_lsm_short.loc['GLEAMv3.3a']
        else:
            data_esa_cci = np.nan
            data_gleam = np.nan
        if (d == '0.00-0.10') | (d == '0.10-0.30'):
            data_era_land = data_lsm_short.loc['ERA-Land']
        else:
            data_era_land = np.nan

        def plotter(ax, dl, pos, col):
            # ---- draw the boxplot. Outliers beyond range are not shown.
            hb = ax.boxplot(dl.reshape(-1,1), positions = [pos], 
                            widths = 0.4, whis = [0, 100], showfliers = False)
            #ax.text(pos-0.25, np.min(dl) * 0.6, 'n=' + str(dl.shape[0]),
            #        rotation = 90)
            hb['boxes'][0].set_color(col)
            return hb

        plotter(ax, metric_raw['lsm'].values, 0.5, clist[0])
        if (d == '0.00-0.10'):
            h1, = ax.plot(0.5, data_esa_cci, 'sb', markersize = 4)
            h2, = ax.plot(0.5, data_gleam, '^b', markersize = 4)
        if (d == '0.00-0.10') | (d == '0.10-0.30'):
            h3, = ax.plot(0.5, data_era_land, 'vb', markersize = 4)
        plotter(ax, metric_raw['cmip5'].values, 1, clist[1])
        plotter(ax, metric_raw['cmip6'].values, 1.5, clist[2])

        #######################################################################
        # Plot the products.
        #######################################################################
        h4 = []
        for ind, model in enumerate(model_list):
            tmp, = ax.plot(2.4 + ind/25, metric_mean[model], 'o',
                           color = clist[ind], markersize = 4)
            h4.append(tmp)
        for ind, model in enumerate(model_list):
            ax.plot(3.4 + ind/25, metric_dolce[model], 'o', 
                    color = clist[ind], markersize = 4)
        for ind, model in enumerate(model_list):
            ax.plot(4.4 + ind/25, metric_em[model], 'o', color = clist[ind],
                    markersize = 4)

        ax.set_xlim([0., 5])
        ax.set_xticks([1, 2.5, 3.5, 4.5])
        ax.set_xticklabels(['Raw', 'Mean', 'OLC', 'EC'])
        ax.set_ylabel(dcm)
        if count < 2:
            ax.set_title(metric + ' ' + unit_list[count])
        ax.grid(True, axis = 'y')

    ax.legend([h1, h2, h3] + h4, ['ESA-CCI', 'GLEAM v3.3a', 'ERA-Land',
                                  'ORS', 'CMIP5', 'CMIP6', 'CMIP5+6', 'ALL'],
              loc = 'center left', ncol = 4, bbox_to_anchor = [-1.4, -0.35])

    fig.savefig(os.path.join(mg.path_out(), 'standard_metrics', 
                             'plot_' + pr + '_' + metric + '_' + iam + \
                             suffix + '.png'), dpi = 600.,
                bbox_inches = 'tight')
    plt.close(fig)
