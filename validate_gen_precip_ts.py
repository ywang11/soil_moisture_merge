"""
20210713
ywang254@utk.edu

Create the SREX time series of the precipitation drivers.
"""
import regionmask as rmk
import matplotlib.pyplot as plt
import os
import pandas as pd
import xarray as xr
import numpy as np
import utils_management as mg
from utils_management.constants import depth, depth_cm, val_list, val_depth, \
    val2_list, val_depth, lsm_all, cmip5, met_to_lsm
from misc.analyze_utils import decode_month_since
from misc.cmip6_utils import *
from time import time


# Grid area
hr = xr.open_dataset(os.path.join(mg.path_data(), 'Global_Masks',
                                    'elm_half_degree_grid_negLon.nc'))
area = hr.area.copy(deep = True)
hr.close()


#
srex_mask = rmk.defined_regions.srex.mask(area)
def avg_srex(sm, which):
    """Average within SREX regions (all are over land)"""
    sm_srex = pd.DataFrame(data = np.nan,
                           index = sm['time'].to_index(),
                           columns = [str(sid) for sid in which])
    for sid in which:
        keep = (np.abs(srex_mask.values - sid) < 1e-6) & \
            (~np.isnan(sm.values[0,:,:]))
        sm_r = (sm.where(keep) * area.where(keep)).sum(dim = ['lat','lon']) \
            / np.sum(area.where(keep))
        sm_srex.loc[:, str(sid)] = sm_r.values
    return sm_srex


# Sahara & West Africa
sid_list = ['14', '19']
met_list = sorted(met_to_lsm.keys())


# Drivers of the products
"""
met_srex_all = pd.DataFrame(data = np.nan,
                            index = pd.date_range('1970-01-01', '2016-12-31', freq = 'MS'),
                            columns = pd.MultiIndex.from_product([met_list, sid_list]))
for met in met_list:
    path_met = mg.path_to_pr('vanilla')
    flist = [os.path.join(path_met[met] + str(yy) + '.nc') for yy in range(1970, 2017)]
    flist = [temp for temp in flist if os.path.exists(temp)]
    ylist = [int(temp.split('_')[-1][:4]) for temp in flist]

    hr = xr.open_mfdataset(flist, decode_times = False)
    var = hr['pr'].copy(deep = True)
    var['time'] = pd.date_range(str(ylist[0]) + '-01-01', str(ylist[-1]) + '-12-31', freq = 'MS')
    hr.close()

    temp = avg_srex(var, [int(x) for x in sid_list])
    for sid in sid_list:
        met_srex_all.loc[:, (met, sid)] = temp.loc[:, sid]
met_srex_all.to_csv(os.path.join(mg.path_out(), 'validate',
                                 'met_srex_precip_all.csv'))
"""

# Drivers of the source datasets
cmip6_list = list(set(mrsol_availability('0-10cm', 'vanilla', 'historical')) &\
                  set(mrsol_availability('0-10cm', 'vanilla', 'ssp585')))
cmip6_list = sorted([x for x in cmip6_list if 'r1i1p1f1' in x])


met_source_all = pd.DataFrame(data = np.nan,
                              index = pd.date_range('1970-01-01', '2016-12-31', freq = 'MS'),
                              columns = pd.MultiIndex.from_product([met_list+cmip5+cmip6_list,
                                                                    sid_list]))
for source, source_list in zip(['ORS', 'CMIP5', 'CMIP6'],
                               [met_list, cmip5, cmip6_list]):
    for nn in source_list:
        start = time()

        print(nn)

        if source == 'ORS':
            path_met = mg.path_to_pr('vanilla')
            flist = [os.path.join(path_met[nn] + str(yy) + '.nc') \
                     for yy in range(1970, 2017)]
            flist = [temp for temp in flist if os.path.exists(temp)]
            ylist = [int(temp.split('_')[-1][:4]) for temp in flist]
        elif source == 'CMIP5':
            flist = sorted(glob(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                                 'vanilla', source, nn, 'pr_*.nc')))
            ylist = [int(yy.split('/')[-1].split('_')[-1][:4]) for yy in flist]
        elif source == 'CMIP6':
            flist = sorted(glob(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                             'vanilla', source, nn, 'pr_*.nc')))
            ylist = [int(yy.split('/')[-1].split('_')[-1][:4]) for yy in flist]

        if len(flist) == 0:
            continue

        hr = xr.open_mfdataset(flist, decode_times = False)
        var = hr['pr'].copy(deep = True)
        var['time'] = pd.date_range(str(ylist[0]) + '-01-01',
                                    str(ylist[-1]) + '-12-31', freq = 'MS')
        var = var[(var['time'].to_index().year >= 1970) & \
                  (var['time'].to_index().year <= 2016), :, :]
        hr.close()

        temp = avg_srex(var, [int(x) for x in sid_list])
        for sid in sid_list:
            met_source_all.loc[var['time'].to_index(), (nn, sid)] = temp.loc[:, sid]

        print(time() - start)
met_source_all.to_csv(os.path.join(mg.path_out(), 'validate',
                                   'source_srex_precip_all.csv'))
