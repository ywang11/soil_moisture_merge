"""
2019/01/07
ywang254@utk.edu

Plot the monthly climatology and trend (w/ sig.) of latitudial mean 
 soil moisture, 1950-2016. No reference to ERA-Interim/GLEAM
"""
import pandas as pd
from utils_management.constants import depth, depth_cm, year_longest, \
    year_shortest
import utils_management as mg
import itertools as it
import os
import numpy as np
from misc.plot_utils import stipple, cmap_gen
import matplotlib.pyplot as plt
import matplotlib as mpl


# Time period to calculate the climatology and trend on: 1950-2016.
year = year_longest
year_str = str(year[0]) + '-' + str(year[-1])
year_str2 = str(year_shortest[0]) + '-' + str(year_shortest[-1])
time_range = pd.date_range(start = str(year[0])+'-01-01',
                           end = str(year[-1])+'-12-31', freq = 'MS')

             
#data_list = [x+'_'+y for x in ['mean', 'dolce', 'em'] \
#             for y in ['lsm', 'cmip5', 'cmip6', '2cmip', 'all']]
data_list = ['mean_lsm', 'dolce_lsm',
             'em_lsm', 'em_cmip5', 'em_cmip6', 'em_2cmip', 'em_all']

#data_name = [x+'_'+y for x in ['Mean','OLC','EC'] \
#             for y in ['ORS', 'CMIP5', 'CMIP6', 'CMIP5+6', 'ALL']]
data_name = ['Mean-ORS', 'OLC-ORS', 'EC-ORS', 'EC-CMIP5', 'EC-CMIP6',
             'EC-2CMIP', 'EC-ALL']


lat_median = np.arange(78.75, -54.76, -0.5)
levels_clim = np.linspace(0.05, 0.46, 41)
levels_trend = np.linspace(-2., 2., 41)
cmap = 'Spectral'
cmap_anomaly = cmap_gen('autumn', 'winter_r')
months = [str(i) for i in range(1,13)]

mpl.rcParams['axes.titlesize'] = 10
for which in ['clim', 'trend']:
    fig, axes = plt.subplots(nrows = len(depth), ncols = len(data_list), 
                             figsize = (2 * len(data_list), 12))
    fig.subplots_adjust(hspace = 0.03, wspace = 0.01)

    if which == 'clim':
        axes[0, len(data_list) // 2].text(0, -46, # -32,
                        'Soil Moisture (m$^3$/m$^3$)', fontsize = 16)
    else:
        axes[0, len(data_list) // 2].text(0, -46, # -32,
                        'Trend (10$^{-4}$ m$^3$/m$^3$/year)', fontsize = 16)

    for i, d in enumerate(depth):
        for dind, data in enumerate(data_list):
            d = depth[i]
            dcm = depth_cm[i]
    
            ax = axes[i, dind]

            if which == 'clim':
                clim = pd.read_csv(os.path.join(mg.path_out(), 
                                                'other_diagnostics', data,
                                                'bylat_clim_' + d + '_' + \
                                                year_str2 + '.csv'),
                                   index_col = 0 \
                ).dropna(axis = 1, how = 'all').T.loc[::-1, months]
                h = ax.imshow(clim, aspect = 0.075, vmin = levels_clim[0],
                              vmax = levels_clim[-1], cmap = cmap)
            else:
                trend = pd.read_csv(os.path.join(mg.path_out(),
                                                 'other_diagnostics', data,
                                                 'bylat_trend_' + d + '_' + \
                                                 year_str + '.csv'),
                                    index_col = 0 \
                ).dropna(axis = 0, how = 'all').loc[::-1, months]
                trend_p = pd.read_csv(os.path.join(mg.path_out(), 
                                                   'other_diagnostics', data,
                                                   'bylat_trend_p_' + d + \
                                                   '_' + year_str + '.csv'), 
                                      index_col = 0 \
                ).dropna(axis = 0, how = 'all').loc[::-1, months]
                h = ax.imshow(trend * 1e4, aspect = 0.075,
                              vmin = levels_trend[0],
                              vmax = levels_trend[-1], cmap = cmap_anomaly)
                stipple(ax, range(len(lat_median)), range(12), trend_p)

            if dind == 0:
                ax.set_ylabel(dcm)
            if i == 0:
                ax.set_title(data_name[dind])
            if i == (len(depth)-1):
                ax.set_xlabel('Month')

            ax.set_yticks(range(0, len(lat_median), 40))
            if dind == 0:
                ax.set_yticklabels(['%.2f' % f for f in lat_median[::40]])
            else:
                ax.set_yticklabels([])
            ax.set_xticks(range(0, 12, 3))
            if i == (len(depth)-1):
                ax.set_xticklabels(range(1, 12, 3))
            else:
                ax.set_xticklabels([])

    if which == 'clim':
        cax = fig.add_axes([0.1, 0.05, 0.8, 0.02])
        fig.colorbar(h, cax = cax, boundaries = levels_clim,
                     orientation = 'horizontal')
    else:
        cax = fig.add_axes([0.1, 0.05, 0.8, 0.02])
        fig.colorbar(h, cax = cax, boundaries = levels_trend,
                     orientation = 'horizontal')

    fig.savefig(os.path.join(mg.path_out(), 'other_diagnostics_plot',
                             'bylat', which + '.png'), dpi = 600.)
    plt.close(fig)
