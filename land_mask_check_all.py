"""
2019/07/14

ywang254@utk.edu

Combine the land mask of the interpolated CMIP6 data to see 
if they are the same between different CMIP6 models and the interpolated
precipitation & temperature data.

Switch the land mask between None, SYNAMP, and e1m to check either interpolated
data before applying the land mask, or after applying different land masks.
"""
import sys
import os
import xarray as xr
import numpy as np
import pandas as pd
from glob import glob
from utils_management.constants import cmip5, lsm_all
import utils_management as mg


path_out = os.path.join(mg.path_out(), 'check_land_mask')
path_merge = os.path.join(mg.path_intrim_out(), 'Interp_Merge')


lsm_all.remove('ESA-CCI')
pr_all  = ['CERA20C_pr', 'CRU_v3.20', 'CRU_v3.26', 'CRU_v4.03', 'ERA20C_pr',
           'ERA-Interim_pr', 'ERA5_pr', 'GLDAS_Noah2.0_pr']
cmip5_all = cmip5
model_all = lsm_all + pr_all + cmip5_all + cmip5_all


target_lat = np.arange(-89.75, 89.76, 0.5)
target_lon = np.arange(-179.75, 179.76, 0.5)


land_mask = 'vanilla' # e1m, SYNMAP, vanilla


mask_all = np.zeros([len(model_all), 360, 720], dtype = int)
for model_ind, model in enumerate(lsm_all):
    path_model = os.path.join(path_merge, land_mask, model)
    file_list = glob(path_model + '/' + model + '*.nc')
    f = os.path.join(os.path.join(path_model, file_list[0]))
    data = xr.open_dataset(f, decode_times = False)
    mask_all[model_ind, :, :] = ~np.isnan(data.sm[0,:,:])
    data.close()
for model_ind, model in enumerate(pr_all, len(lsm_all)):
    path_model = os.path.join(path_merge, land_mask, model)
    file_list = glob(path_model + '/' + model + '*.nc')
    f = os.path.join(os.path.join(path_model, file_list[0]))
    data = xr.open_dataset(f, decode_times = False)
    mask_all[model_ind, :, :] = ~np.isnan(data.pr[0,:,:])
    data.close()
for model_ind, model in enumerate(cmip5_all, len(lsm_all) + len(pr_all)):
    path_model = os.path.join(path_merge, land_mask, 'CMIP5', model)
    file_list = glob(path_model + '/sm_historical_*.nc')
    f = os.path.join(os.path.join(path_model, file_list[0]))
    data = xr.open_dataset(f, decode_times = False)
    mask_all[model_ind, :, :] = ~np.isnan(data.sm[0,:,:])
    data.close()
for model_ind, model in enumerate(cmip5_all, len(lsm_all) + len(pr_all) + \
                                  len(cmip5_all)):
    path_model = os.path.join(path_merge, land_mask, 'CMIP5', model)
    file_list = glob(path_model + '/pr_historical_*.nc')
    f = os.path.join(os.path.join(path_model, file_list[0]))
    data = xr.open_dataset(f, decode_times = False)
    mask_all[model_ind, :, :] = ~np.isnan(data.pr[0,:,:])
    data.close()


# Check if all the models have the same mask.
mask_check = np.max(mask_all, axis=0) - np.min(mask_all, axis=0)


# Check at which grid cells the models differ. 
mask_differ = np.where(mask_check > 1e-6)


# Check the model values at the different grid cells.
for i,j in zip(mask_differ[0], mask_differ[1]):
    data = pd.Series(data = mask_all[:,i,j], index = model_all)
    print(data)
    input('Press Enter to go to the next grid cell.')
