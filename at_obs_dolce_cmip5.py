# -*- coding: utf-8 -*-
"""
Created on Thu May 16 19:19:40 2019

@author: ywang254

Extract the soil moisture from the DOLCE-weighted soil moisture data.
"""
import utils_management as mg
from utils_management.constants import depth, depth_cm, year_longest
import os
import xarray as xr
from misc.ismn_utils import get_weighted_monthly_data, \
    get_ismn_aggr_method
from misc.dolce_utils import get_cov_method, get_weighted_sm_points
from glob import glob
import pandas as pd
import time


start = time.time()


year = year_longest


i = REPLACE1 # [0,1,2,3]
d = depth[i]

cov = REPLACE2 # range(5)
cov_method = get_cov_method(cov)

ismn_aggr_ind = REPLACE3
simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = get_ismn_aggr_method(simple[ismn_aggr_ind],
                                        dominance_lc[ismn_aggr_ind],
                                        dominance_threshold)

# ['weighted_average', 'weighted_uncertainty']
for val in ['weighted_average']:
    grid_latlon, _, _ = \
        get_weighted_monthly_data(os.path.join(mg.path_intrim_out(), 
                                               'ismn_aggr'), 
                                  ismn_aggr_method, d, 'all')


    setup = val + '_' + str(year[0]) + '-' + str(year[-1]) + '_' + d + '_' + \
            ismn_aggr_method + '_' + cov_method
    f = os.path.join(mg.path_out(), 'dolce_cmip5_product', setup + '.nc')

    data = xr.open_dataset(f, decode_times=False)
    weighted_sm = data.sm.copy(deep=True)
    data.close()

    weighted_sm_at_data = get_weighted_sm_points(grid_latlon, 
                                                 weighted_sm, year)

    weighted_sm_at_data.to_csv(os.path.join(mg.path_out(), 
                                            'at_obs_dolce_cmip5',
                                            setup + '.csv'))

end = time.time()
print('The script finished in ' + ('%.4f' % ((end-start)/3600) ) + ' hours.')
