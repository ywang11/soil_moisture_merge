"""
2020/12/10
Plot the PDF of GSWP3.
"""
import xarray as xr
import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import itertools as it
import utils_management as mg
from utils_management.constants import depth, depth_cm
from misc.plot_utils import stipple, cmap_div
from matplotlib.cm import get_cmap


mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 6
mpl.rcParams['hatch.linewidth'] = 0.5
cmap = cmap_div()
mlist = ['-', '--', ':']
#slist = [get_cmap('tab20c').colors[i] for i in [0,1,2,4,5,6,8,9,10]]
slist = [get_cmap('tab20c').colors[i] for i in [0,4,8]]
lab = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
       'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
       'y', 'z', 'aa', 'bb', 'cc', 'dd', 'ee', 'ff', 'gg', 'hh',
       'ii', 'jj', 'kk', 'll', 'mm', 'nn', 'oo', 'pp', 'qq', 'rr',
       'ss', 'tt', 'uu', 'vv', 'ww', 'xx']


prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all']
prod_name_list = ['Mean ORS', 'OLC ORS', 'EC ORS', 'EC CMIP5', 'EC CMIP6',
                  'EC CMIP5+6', 'EC ALL']
season_list = ['Annual', 'DJF', 'JJA']
for season in season_list:
    fig, axes = plt.subplots(len(prod_list), 4, figsize = (6.5, 6.5),
                             sharex = True, sharey = True)
    fig.subplots_adjust(wspace = 0.06, hspace = 0.1)
    count = 0
    for pind, dind in it.product(range(len(prod_list)), range(len(depth))):
        prod = prod_list[pind]
        d = depth[dind]
        dcm = depth_cm[dind]
        ax = axes.flat[count]

        #h1 = [None] * len(season_list)
        #h2 = [None] * 3
        h = [None] * 9
        for sind, season in enumerate(season_list):
            hr = xr.open_dataset(os.path.join(mg.path_out(), 'validate',
                                              'corr_gswp3_' + prod + '_' + \
                                              d + '_' + season + '.nc'))
            for vind, var in enumerate(['pr', 'tas', 'rsds']):          
                temp = hr[var].where(hr[var + '_p'] <= 0.05).values.reshape(-1)
                temp = temp[~np.isnan(temp)]
                hist, bin_edges = np.histogram(temp, bins = 40,
                                               density = True)
                htemp, = ax.plot((bin_edges[1:] + bin_edges[:-1])/2,  hist,
                                 lw = 0.5, ls = mlist[sind],
                                 color = slist[vind])
                #if sind == 0:
                #    h2[vind] = htemp
                #if vind == 0:
                #    h1[sind] = htemp
                h[sind*3 + vind] = htemp
            hr.close()

        if pind == 0:
            ax.set_title(dcm.replace('-', u'\u2212').replace('cm', ' cm'))
        if pind != (len(prod_list) - 1):
            plt.setp(ax.get_xticklabels(), visible=False)
        if dind == 0:
            ax.set_ylabel(prod_name_list[pind])
        else:
            plt.setp(ax.get_yticklabels(), visible=False)

        ax.text(0.05, 0.88, '(' + lab[pind * len(depth) + dind] + ')',
                transform = ax.transAxes)
        ax.set_xlim([-1, 1])
        ax.set_xticks(np.arange(-0.8, 0.81, 0.4))

        count += 1

    #ax.legend(h1 + h2, season_list + ['pr', 'tas', 'rsds'],
    #          loc = (-2.5, -0.6), ncol = 6)
    ax.legend(h, [rr + ' ' + jj for rr in season_list for jj in ['pr', 'tas', 'rsds']],
              loc = (-2.5, -0.7), ncol = 5)
    fig.savefig(os.path.join(mg.path_out(), 'validate', 
                             'corr_gswp3_summary.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
