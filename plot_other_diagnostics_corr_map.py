"""
2019/10/04
ywang254@utk.edu

Plot the correlation map between the ESA-CCI, GLEAMv3.3a, ERA-Land, and
 the results.
"""
import xarray as xr
import os
from utils_management.constants import depth, depth_cm
import utils_management as mg
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from cartopy.util import add_cyclic_point

d = depth[0]
dcm = depth_cm[0]


for s in ['ESA-CCI', 'GLEAMv3.3a']:
    data = xr.open_dataset(os.path.join(mg.path_out(), 'other_diagnostics',
                                        'meanmedian_products',
                                        'corr_with_' + s + '_' + dcm + '.nc'))

    fig, ax = plt.subplots(subplot_kw = {'projection': ccrs.PlateCarree()},
                           figsize = (8,6))
    ax.set_extent([-180, 180, -70, 90])
    ax.coastlines()

    Z_cyc, x_cyc = add_cyclic_point(data.corr, data.lon)
    cf = ax.contourf(x_cyc, data.lat, Z_cyc, transform = ccrs.PlateCarree(), 
                     cmap = 'Spectral', levels = np.arange(0., 1.01, 0.1),
                     extend = 'both')
    cb = plt.colorbar(cf, ax = ax, shrink = 0.7, extend = 'both', 
                      boundaries = np.arange(0., 1.01, 0.1), pad = 0.1)

    ax.gridlines(draw_labels = True, xlocs = np.arange(-180, 181, 30),
                 ylocs = np.arange(-90, 91, 30))

    fig.savefig(os.path.join(mg.path_out(), 'other_diagnostics_plot', 
                             'corr_map_' + s + '_' + dcm + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)

    data.close()


# 'ERA-Land'
for dcm in depth_cm[:2]:
    data = xr.open_dataset(os.path.join(mg.path_out(), 'other_diagnostics',
                                        'meanmedian_products',
                                        'corr_with_ERA-Land_' + dcm + '.nc'))

    fig, ax = plt.subplots(subplot_kw = {'projection': ccrs.PlateCarree()},
                           figsize = (8,6))
    ax.set_extent([-180, 180, -70, 90])
    ax.coastlines()

    Z_cyc, x_cyc = add_cyclic_point(data.corr, data.lon)
    cf = ax.contourf(x_cyc, data.lat, Z_cyc, transform = ccrs.PlateCarree(), 
                     cmap = 'Spectral', levels = np.arange(0., 1.01, 0.1),
                     extend = 'both')
    cb = plt.colorbar(cf, ax = ax, shrink = 0.7, extend = 'both', 
                      boundaries = np.arange(0., 1.01, 0.1), pad = 0.1)

    ax.gridlines(draw_labels = True, xlocs = np.arange(-180, 181, 30),
                 ylocs = np.arange(-90, 91, 30))

    fig.savefig(os.path.join(mg.path_out(), 'other_diagnostics_plot', 
                             'corr_map_ERA-Land_' + dcm + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)

    data.close()


### DEBUG: 
### Dummy to check the ESA-CCI data availability.
"""
fig, ax = plt.subplots(subplot_kw = {'projection': ccrs.PlateCarree()},
                       figsize = (8,6))
ax.set_extent([-180, 180, -70, 90])
ax.coastlines()
Z_cyc, x_cyc = add_cyclic_point((~np.isnan(ref)).sum(axis = 0) > 3, data.lon)
cf = ax.contourf(x_cyc, data.lat, Z_cyc, transform = ccrs.PlateCarree(),
                 cmap = 'Spectral', vmin = 0., vmax = 0.8,
                 extend = 'both')
cb = plt.colorbar(cf, ax = ax, shrink = 0.7, extend = 'both')
ax.gridlines(draw_labels = True, xlocs = np.arange(-180, 181, 30),
             ylocs = np.arange(-90, 91, 30))
fig.savefig(os.path.join(mg.path_out(), 'other_diagnostics_plot',
                         'corr_map.png'), dpi = 600.)
"""
