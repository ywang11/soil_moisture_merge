"""
Compute the following for every DOLCE-weighted result:

1. Global map (averaged over time): NetCDF
2. Global average time series.
3. Latitude bands average time series.
4. Continental average time series.
5. SREX region average time series.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import year_longest, depth, depth_cm
from misc.dolce_utils import get_cov_method
from misc.ismn_utils import get_ismn_aggr_method
from misc.standard_diagnostics import standard_diagnostics
import os
import pandas as pd


year = year_longest


i = REPLACE1 # [0,1,2,3]
d = depth[i]

#cov = REPLACE2 # range(5)
#cov_method = get_cov_method(cov)
cov_method = 'ShrunkCovariance'

#ismn_aggr_ind = REPLACE3
#simple = [True, False, False]
#dominance_lc = [False, False, True]
#dominance_threshold = 40
#ismn_aggr_method = get_ismn_aggr_method(simple[ismn_aggr_ind],
#                                        dominance_lc[ismn_aggr_ind],
#                                        dominance_threshold)
ismn_aggr_method = 'lu_weighted'


no_merge = REPLACE2 # True, False


uncertainty = REPLACE3 # True, False

prod = 'REPLACE4' # 'all', 'lsm'


if no_merge:
    if uncertainty:
        fi = os.path.join(mg.path_out(), 'concat_no_merge_dolce_' + prod,
                          'concat_uncertainty_' + d + '_' + \
                          ismn_aggr_method + '_' + cov_method + '.nc')
    else:
        fi = os.path.join(mg.path_out(), 'concat_no_merge_dolce_' + prod,
                          'concat_average_' + d + '_' + ismn_aggr_method + \
                          '_' + cov_method + '.nc')
else:
    if uncertainty:
        fi = os.path.join(mg.path_out(), 'concat_dolce_' + prod, 
                          'concat_uncertainty_' + d + '_' + \
                          ismn_aggr_method + '_' + \
                          cov_method + '.nc')
    else:
        fi = os.path.join(mg.path_out(), 'concat_dolce_' + prod, 
                          'positive_average_' + d + '_' + ismn_aggr_method + \
                          '_' + cov_method + '.nc')


data = xr.open_dataset(fi, decode_times=False)

time = pd.date_range(str(year[0])+'-01-01', str(year[-1])+'-12-31',
                     freq = 'MS')


if uncertainty:
    values = data.sm_uncertainty.values
else:
    values = data.sm.values


if no_merge:
    if uncertainty:
        standard_diagnostics(values, time, data.lat, data.lon, 
                             os.path.join(mg.path_out(), 
                                          'standard_diagnostics_concat',
                                          'no_merge_dolce_' + prod),
                             'no_merge_dolce_uncertainty_' + \
                             ismn_aggr_method + '_' + \
                             cov_method + '_' + d)
    else:
        standard_diagnostics(values, time, data.lat, data.lon, 
                             os.path.join(mg.path_out(), 
                                          'standard_diagnostics_concat',
                                          'no_merge_dolce_' + prod),
                             'no_merge_dolce_average_' + \
                             ismn_aggr_method + '_' + \
                             cov_method + '_' + d)
else:
    if uncertainty:
        standard_diagnostics(values, time, data.lat, data.lon, 
                             os.path.join(mg.path_out(), 
                                          'standard_diagnostics_concat',
                                          'dolce_' + prod),
                             'dolce_uncertainty_' + ismn_aggr_method + '_' + \
                             cov_method + '_' + d)
    else:
        standard_diagnostics(values, time, data.lat, data.lon, 
                             os.path.join(mg.path_out(), 
                                          'standard_diagnostics_concat',
                                          'dolce_' + prod),
                             'dolce_average_' + ismn_aggr_method + '_' + \
                             cov_method + '_' + d)

data.close()
