"""
2020/05/19

ywang254@utk.edu

Plot the RMSE & Corr of the different weighted results,
 the range of LSM, CMIP5, CMIP6 models, and highlight the ESA-CCI, 
 GLEAMv3.3a, and the ERA-Land results in the LSM models.

Use the best emergent constraint and DOLCE weighting setups.

Use a subset of the products that are used in the D&A.
"""
import os
import sys
import pandas as pd
import numpy as np
from utils_management.constants import depth, depth_cm, year_longest, \
    year_shorter, year_shorter2, year_shortest, lsm_list, year_cmip5, \
    year_cmip6
import utils_management as mg
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import cm
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability, one_layer_availability
from misc.ismn_utils import get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
import itertools as it


# MODIFY
USonly = False # if True, plot the evaluation results for CONUS
if USonly:
    suffix = '_USonly'
else:
    suffix = ''

#
pr = 'val'
year = year_longest # Only plot the concatenated LSM and ALL products.
year_str = str(year[0]) + '-' + str(year[-1])
iam = 'lu_weighted'
em = 'year_month_anomaly_9grid'
cov = 'ShrunkCovariance'
land_mask = 'vanilla'
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'
cmap = cm.get_cmap('jet')
clist = [cmap((i+0.7)/7) for i in range(7)]
mlist = ['x', 'o', 's', 's', 's', 's', 's']
prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all']
prod_name = ['Mean ORS', 'OLC ORS', 'EC ORS', 'EC CMIP5', 'EC CMIP6',
             'EC CMIP5+6', 'EC ALL']

#
metric_list = ['Bias','RMSE', 'Corr']
unit_list = ['($m^3/m^3$)','($m^3/m^3$)','(-)']

#
mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 6

#
fig, axes = plt.subplots(nrows = 3, ncols = 4, figsize = (6.5, 6.),
                         sharex = True, sharey = False)
fig.subplots_adjust(hspace = 0.05, wspace = 0.)
for mind, dind in it.product(range(len(metric_list)), range(len(depth))):
    metric = metric_list[mind]
    d = depth[dind]
    dcm = depth_cm[dind]

    ax = axes[mind, dind]

    #
    metric_raw = {}
    metric_raw['lsm'] = pd.read_csv(os.path.join(mg.path_out(), 
                                                 'standard_metrics', 
                                                 'lsm_' + d + '_all' + 
                                                 suffix + '.csv'),
                                    index_col = [0,1], 
                                    header = [0,1]).loc[iam, (metric,pr)]
    metric_raw['cmip5'] = pd.read_csv(os.path.join(mg.path_out(), 
                                                   'standard_metrics',
                                                   'cmip5_' + d + \
                                                   suffix + '.csv'),
                                      index_col = [0,1], 
                                      header = [0,1]).loc[iam, (metric,pr)]
    metric_raw['cmip6'] = pd.read_csv(os.path.join(mg.path_out(),
                                                   'standard_metrics',
                                                   'cmip6_' + d + '.csv'),
                                      index_col = [0,1],
                                      header = [0,1]).loc[iam, (metric,pr)]

    #
    metric_prod = {}
    metric_prod[prod_list[0]] = pd.read_csv(os.path.join(mg.path_out(),
        'standard_metrics', 'meanmedian_lsm_' + d + '_' + year_str + \
        suffix + '.csv'), index_col = [0,1], header = [0,1]).loc[(iam,'mean'),
                                                                 (metric,pr)]
    metric_prod[prod_list[1]] = pd.read_csv(os.path.join(mg.path_out(),
        'standard_metrics', 'concat_dolce_lsm_' + d + '_' + year_str + \
        suffix + '.csv'), index_col = [0,1], header = [0,1]).loc[(iam,cov),
                                                                 (metric,pr)]
    for prod in ['em_lsm', 'em_all']:
        metric_prod[prod] = pd.read_csv(os.path.join(mg.path_out(), 
                                                     'standard_metrics',
                                                     'concat_' + prod + \
                                                     '_' + d + \
                                                     '_' + year_str + \
                                                     suffix + '.csv'),
                                        index_col = 0, 
                                        header = [0,1]).loc[iam, (metric,pr)]
    for prod in ['em_cmip5', 'em_cmip6', 'em_2cmip']:
        metric_prod[prod] = pd.read_csv(os.path.join(mg.path_out(),
                                                     'standard_metrics',
                                                     prod + '_' + d + '_' + \
                                                     year_str + \
                                                     suffix + '.csv'),
                                         index_col = [0,1],
                                         header = [0,1]).loc[(iam,em),
                                                             (metric,pr)]

    #######################################################################
    # Plot the individual source datasets.
    #######################################################################
    data_lsm_short = pd.read_csv(os.path.join(mg.path_out(),
                                              'standard_metrics',
                                              'lsm_' + d + '_all' + \
                                              suffix + '.csv'),
                                 index_col = [0,1],
                                 header = [0,1]).loc[iam, (metric,pr)]

    def plotter(ax, dl, pos, col):
        # ---- draw the boxplot. Outliers beyond range are not shown.
        hb = ax.boxplot(dl.reshape(-1,1), positions = [pos], 
                        widths = 0.4, whis = [0, 100], showfliers = False)
        #ax.text(pos-0.25, np.min(dl) * 0.6, 'n=' + str(dl.shape[0]),
        #        rotation = 90)
        hb['boxes'][0].set_color(col)
        hb['boxes'][0].set_color(col)
        return hb

    plotter(ax, metric_raw['lsm'].values, 1., 'k')
    plotter(ax, metric_raw['cmip5'].values, 2, 'k')
    plotter(ax, metric_raw['cmip6'].values, 3, 'k')

    print(metric, dcm)

    #######################################################################
    # Plot the products.
    #######################################################################
    h4 = []
    count = 0; count2 = 0
    for ind, prod in enumerate(prod_list):
        if ('lsm' in prod) | ('all' in prod):
            x = 1 + (count - 1.5) * 0.2
            count += 1
        elif 'cmip5' in prod:
            x = 2
        else:
            x = 3 + (count2 - 0.5) * 0.3
            count2 += 1
        print(metric_prod[prod])
        tmp, = ax.plot(x, metric_prod[prod], mlist[ind],
                       color = clist[ind], markersize = 4,
                       markerfacecolor = "None")
        h4.append(tmp)

    ax.set_xlim([0.5, 3.5])
    ax.set_xticks([1, 2, 3])
    ax.set_xticklabels(['ORS', 'CMIP5', 'CMIP6'])

    if mind == 0:
        ax.set_title(dcm.replace('-', u'\u2212').replace('cm', ' cm'))
        ax.set_ylim([-0.2, 0.2])
    elif mind == 1:
        ax.set_ylim([0.07, 0.2])
        ax.set_yticks(np.arange(0.08, 0.2, 0.02))
    else:
        ax.set_ylim([0.1, 0.8])
        ax.set_yticks(np.arange(0.2, 0.8, 0.1))
    if dind == 0:
        ax.set_ylabel(metric + ' ' + unit_list[mind])
    else:
        ax.tick_params('y', length = 0)
        ax.set_yticklabels([])
    ax.grid(True, axis = 'y')
    ax.text(0.05, 0.93, '(' + lab[mind * len(depth) + dind] + ')',
            transform = ax.transAxes)

ax.legend(h4, prod_name, loc = 'center left', ncol = 4,
          bbox_to_anchor = [-2.5, -0.3])

fig.savefig(os.path.join(mg.path_out(), 'standard_metrics', 
                         'plot_' + pr + '_all_' + iam + \
                         suffix + '.png'), dpi = 600.,
            bbox_inches = 'tight')
plt.close(fig)
