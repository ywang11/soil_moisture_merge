"""
20210713
ywang254@utk.edu

Create the SREX time series of the precipitation drivers.
"""
import regionmask as rmk
import matplotlib.pyplot as plt
import os
import pandas as pd
import xarray as xr
import numpy as np
import utils_management as mg
from utils_management.constants import depth, depth_cm, val_list, val_depth, \
    val2_list, val_depth, lsm_all, cmip5, met_to_lsm
from misc.analyze_utils import decode_month_since
from misc.cmip6_utils import *
from time import time


def avg_box(sm, box):
    sm = sm[:, (sm['lat'] >= box[0]) & (sm['lat'] <= box[1]), 
            :][:, :, (sm['lon'] >= box[2]) & (sm['lon'] <= box[3])]
    area_temp = area[(area['lat'] >= box[0]) & (area['lat'] <= box[1]),
                     :][:, (area['lon'] >= box[2]) & (area['lon'] <= box[3])]
    sm = (sm * area_temp).sum(dim = ['lat','lon']) / np.sum(area_temp)
    sm = pd.Series(data = sm.values, index = sm['time'].to_index())
    return sm


# Grid area
hr = xr.open_dataset(os.path.join(mg.path_data(), 'Global_Masks',
                                    'elm_half_degree_grid_negLon.nc'))
area = hr.area.copy(deep = True)
hr.close()


#
bounding_box = [0, 20, -5, 40]
met_list = sorted(met_to_lsm.keys())


# Drivers of the products
met_srex_all = pd.DataFrame(data = np.nan,
                            index = pd.date_range('1970-01-01', '2016-12-31', freq = 'MS'),
                            columns = met_list)
for met in met_list:
    path_met = mg.path_to_pr('vanilla')
    flist = [os.path.join(path_met[met] + str(yy) + '.nc') for yy in range(1970, 2017)]
    flist = [temp for temp in flist if os.path.exists(temp)]
    ylist = [int(temp.split('_')[-1][:4]) for temp in flist]

    hr = xr.open_mfdataset(flist, decode_times = False)
    var = hr['pr'].copy(deep = True)
    var['time'] = pd.date_range(str(ylist[0]) + '-01-01', str(ylist[-1]) + '-12-31', freq = 'MS')
    hr.close()

    met_srex_all.loc[:, met] = avg_box(var, bounding_box)
met_srex_all.to_csv(os.path.join(mg.path_root(), 'output_da',
                                 'prod_srex_precip_all.csv'))

