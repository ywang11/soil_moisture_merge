# -*- coding: utf-8 -*-
"""
Created on Wed May  1 21:04:14 2019

@author: ywang254

Calculate the RMSEs between between the merged emergent constraint results, 
 and the observed soil moisture.
"""
import os
import utils_management as mg
import pandas as pd
from utils_management.constants import depth, depth_cm, year_longest
from misc.ismn_utils import get_ismn_aggr_method, get_weighted_monthly_data, \
    get_weighted_monthly_data_USonly
from misc.analyze_utils import calc_stats
import numpy as np
import itertools as it
import time


# Select the time range to calculate the RMSE between observation and land
# surface models.
year = year_longest


# List the weighting methods that are used to generate the weighted
# averaged ISMN data.
simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = [get_ismn_aggr_method(simple[i], dominance_lc[i], 
                                         dominance_threshold) for i in \
                    range(len(simple))]

# List the precipitation datasets that are used in the emergent constraint.
met_obs = 'CRU_v4.03'


# List the methods to conduct the emergent constraint analysis.
em = 'year_month_anomaly_9grid'


start = time.time()
for USonly, no_merge, prod, i in it.product([True, False], [True, False], 
                                            ['lsm', 'all'], range(4)):
    d = depth[i]
    dcm = depth_cm[i]

    # Collector for the calculated metrics in time and space.
    metrics_collect = pd.DataFrame(data = np.nan,
                                   index = ismn_aggr_method,
        columns = pd.MultiIndex.from_product([['RMSE','Bias','uRMSE','Corr'],
                                              ['all','cal','val']],
                                             names = ['Metric', 'ISMN Set']))

    for iam, ismn_set in it.product(ismn_aggr_method, ['all', 'cal', 'val']):
        #######################################################################
        # Get the observed data.
        #######################################################################
        if USonly:
            grid_latlon, weighted_monthly_data, _ = \
                get_weighted_monthly_data_USonly( \
                    os.path.join(mg.path_intrim_out(),
                                 'ismn_aggr'), iam, d, ismn_set)
        else:
            grid_latlon, weighted_monthly_data, _ = \
                get_weighted_monthly_data(os.path.join(mg.path_intrim_out(), 
                                                       'ismn_aggr'), iam, d, 
                                          ismn_set)

        #######################################################################
        # Performance of the weighted product.
        #######################################################################
        if no_merge:
            data = pd.read_csv(os.path.join(mg.path_out(), 
                                            'at_obs_concat_em_' + prod,
                                            'no_merge_' + met_obs + '_' + \
                                            em + '_' + dcm + '_' + iam + \
                                            '.csv'),
                               index_col = 0, parse_dates = True)
        else:
            data = pd.read_csv(os.path.join(mg.path_out(),
                                            'at_obs_concat_em_' + prod,
                                            met_obs + '_' + em + '_' + \
                                            dcm + '_' + iam + '.csv'),
                               index_col = 0, parse_dates = True)
        data = data.loc[data.index <= weighted_monthly_data.index[-1], :]
        # ---- obtain values at the observed data points
        weighted_sm_at_data = data.loc[weighted_monthly_data.index, 
                                       weighted_monthly_data.columns]

        metrics_collect.loc[iam, ('RMSE',ismn_set)], \
            metrics_collect.loc[iam, ('Bias',ismn_set)], \
            metrics_collect.loc[iam, ('uRMSE',ismn_set)], \
            metrics_collect.loc[iam, ('Corr',ismn_set)] = \
            calc_stats(weighted_sm_at_data.values.reshape(-1), 
                       weighted_monthly_data.values.reshape(-1))

    if USonly:
        if no_merge:
            metrics_collect.to_csv(os.path.join(mg.path_out(), 
                                                'standard_metrics',
                                                'concat_no_merge_em_' + prod +\
                                                '_' + d + '_' + str(year[0]) +\
                                                '-' + str(year[-1]) + \
                                                '_USonly.csv'))
        else:
            metrics_collect.to_csv(os.path.join(mg.path_out(),
                                                'standard_metrics',
                                                'concat_em_' + prod + '_' + \
                                                d + '_' + str(year[0]) + \
                                                '-' + str(year[-1]) + \
                                                '_USonly.csv'))
    else:
        if no_merge:
            metrics_collect.to_csv(os.path.join(mg.path_out(),
                                                'standard_metrics',
                                                'concat_no_merge_em_' + prod +\
                                                '_' + d + '_' + str(year[0]) +\
                                                '-' + str(year[-1]) + '.csv'))
        else:
            metrics_collect.to_csv(os.path.join(mg.path_out(),
                                                'standard_metrics',
                                                'concat_em_' + prod + '_' + \
                                                d + '_' + str(year[0]) + \
                                                '-' + str(year[-1]) + '.csv'))
end = time.time()
print('The script finished in ' + ('%.4f' % ((end-start)/3600) ) + ' hours.')
