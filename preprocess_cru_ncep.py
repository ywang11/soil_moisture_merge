# -*- coding: utf-8 -*-
"""
Created on Sat May 25 14:01:27 2019

@author: ywang254

Average the downloaded CRU_NCEP7 precipitation to monthly time scale.
"""
import xarray as xr
import pandas as pd
import os
import utils_management as mg


for year in range(1950, 2017):

    data = xr.open_mfdataset('D:/Projects/2018 Soil Moisture/data/CRU_NCEP7/' + 
                             'clmforc.cruncep.V7.c2016.0.5d.Prec.' + \
                             str(year) + '-*.nc')

    # Convert from kg/m2/s to mm/day
    pr = data.PRECTmms.groupby('time.month').mean(dim='time') * 24 * 3600

    pr = pr.rename({'month': 'time'})
    pr['time'] = pd.date_range(str(year) + '-01-01', 
                               str(year) + '-12-31', freq='MS')

    encoding=dict(time=dict(units='days since 1950-01-01 00:00:00',
                            calendar='gregorian'))

    pr.to_dataset(name='pr').to_netcdf(os.path.join(mg.path_intrim_out(),
        'CRU_NCEP7_downsampled', str(year) + '.nc'), encoding=encoding)

    data.close()