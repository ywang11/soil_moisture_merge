# -*- coding: utf-8 -*-
"""
Created on Fri Apr 26 20:23:36 2019

@author: ywang254

Identify the land use of ISMN stations and the fraction of land use in the 
  grid cell that the ISMN stations are in.
"""
import numpy as np
import subprocess
import utils_management as mg
from utils_management.constants import depth, target_lat, target_lon
import xarray as xr
import os
import pandas as pd
import time


regen = False
if regen:
    # Run "ismn_metadata_20190416.py" to generate the metadata quality check.
    subprocess.call("ismn_metadata.py")
regen2 = False
if regen2:
    # Run "lu_aggr.py" to generate the required aggregated land use maps.
    subprocess.call("lu_aggr.py")


start = time.time()


###############################################################################
# Read the ISMN stations metadata.
###############################################################################
def read_ismn_metadata(depth, target_lat, target_lon):
    ismn_metadata = pd.read_csv(os.path.join(mg.path_intrim_out(),
                                             'ismn_metadata', 'metadata.csv'))
    ismn_metadata = ismn_metadata.loc[(ismn_metadata['Depth-Fit'] == \
                                       depth[0]) | \
                                      (ismn_metadata['Depth-Fit'] == \
                                       depth[1]) | \
                                      (ismn_metadata['Depth-Fit'] == \
                                       depth[2]) | \
                                      (ismn_metadata['Depth-Fit'] == \
                                       depth[3]), :]

    ismn_grids = {}
    for key in depth:
        uniq_ismn_latlon = np.unique(ismn_metadata.loc[ \
            ismn_metadata['Depth-Fit'] == key, ['Lat','Lon']], axis = 0)

        # the 0.5x0.5 target grid in which the ismn station resides
        is_in_grid = np.array([ \
            [target_lat[np.argmin(np.abs(target_lat-uniq_ismn_latlon[x,0]))],
             target_lon[np.argmin(np.abs(target_lon-uniq_ismn_latlon[x,1]))]] \
            for x in range(uniq_ismn_latlon.shape[0])])

        ismn_grids[key] = np.concatenate([uniq_ismn_latlon, 
                                          is_in_grid], axis=1)
    return ismn_grids, ismn_metadata

ismn_grids, ismn_metadata = read_ismn_metadata(depth, target_lat, target_lon)


###############################################################################
# Extract the land use separately for each soil depth and grid cell.
###############################################################################
for key in depth:
    path_out = os.path.join(mg.path_intrim_out(), 'ismn_lu', key)

    if os.path.exists(path_out) and os.path.isdir(path_out):
        if len(os.listdir(path_out) ) > 0:
            raise Exception(path_out + ' must be empty to write into it.')

    if not os.path.exists(path_out):
        os.makedirs(path_out)

    uniq_target_grids = np.unique(ismn_grids[key][:,2:], axis=0)

    for i in range(uniq_target_grids.shape[0]):
        lat_grid = uniq_target_grids[i,0]
        lon_grid = uniq_target_grids[i,1]


        # Record the grid-level land cover in MCD12C1 IGBP Classes 0-16.
        def get_grid_lu(lat, lon):            
            data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 
                                                'lu_aggr', 'mcd12c1_0.5.nc'))
            grid_lu = np.zeros(17)
            for j in range(17):
                grid_lu[j] = float(data.__xarray_dataarray_variable__.sel( \
                    {'class': j, 'lat': lat, 'lon': lon}, method = 'nearest'))
            data.close()
            return grid_lu
        grid_lu = get_grid_lu(lat_grid, lon_grid)


        latlon_stations = ismn_grids[key][ \
            (ismn_grids[key][:,2] == lat_grid) & \
            (ismn_grids[key][:,3] == lon_grid), :2]        


        stations_lu_collect = pd.DataFrame(data=-9999, index=[], 
                                           columns = ['Network', 'Station',
                                           'Lat', 'Lon', 'Depth-Actual',
                                           'Sensor', 'Land Cover', 
                                           'Climate Zone'])
        for j in range(latlon_stations.shape[0]):

            def get_stations(ismn_metadata, key, lat, lon):
                stations = ismn_metadata.loc[ \
                    (ismn_metadata['Depth-Fit'] == key) & \
                    (ismn_metadata['Lat'] == lat) & \
                    (ismn_metadata['Lon'] == lon), :].reset_index(drop = True)

                # ---- make sure that at least one of the stations has data
                #      during 1950-2016. skip the stations that do not have
                #      any data during hat period
                stations = stations.loc[stations.iloc[:,12:].sum(axis=1) > 0,
                                        :]
                return stations

            stations = get_stations(ismn_metadata, key, latlon_stations[j,0], 
                                    latlon_stations[j,1])
            if stations.shape[0] == 0:
                continue

            # Record the station-level land cover in MCD12C1 IGBP Classes 0-16.
            def get_station_lu(stations):
                data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 
                                       'lu_aggr', 'mcd12c1_0.05_major.nc'))
                stations_lu = pd.DataFrame(data=-9999, index=stations.index, 
                                           columns = ['Network', 'Station',
                                                      'Lat', 'Lon', 
                                                      'Depth-Actual',
                                                      'Sensor', 'Land Cover',
                                                      'Climate Zone'])
                stations_lu.loc[:, 'Network'] = stations['Network']
                stations_lu.loc[:, 'Station'] = stations['Station']
                stations_lu.loc[:, 'Lat'] = stations['Lat']
                stations_lu.loc[:, 'Lon'] = stations['Lon']
                stations_lu.loc[:, 'Depth-Actual'] = stations['Depth-Actual']
                stations_lu.loc[:, 'Sensor'] = stations['Sensor']
                for j in stations_lu.index:
                    stations_lu.loc[j, 'Land Cover'] = int( \
                        data.__xarray_dataarray_variable__.sel( \
                        lat = stations_lu.loc[j,'Lat'], 
                        lon = stations_lu.loc[j,'Lon'], method = 'nearest'))
                stations_lu.loc[:, 'Climate Zone'] = stations['Climate Zone']
                data.close()
                return stations_lu
            stations_lu_collect = pd.concat([stations_lu_collect, 
                                             get_station_lu(stations)], 
                                            axis=0)

        stations_lu_collect.reset_index(inplace=True, drop=True)

        # Write to file.
        def write_file(grid_lu, stations_lu, path_out):
            fout = open(os.path.join(path_out, 'grid_'+str(i)+'.txt'), 'a')
            # ---- write the geographical coordinates
            fout.write(str(lat_grid) + ',' + str(lon_grid)+'\n')
            # ---- write the land cover percentages of the grid cell
            fout.write('Grid\n')
            fout.write(','.join(['%.4f' % lu for lu in grid_lu]) + '\n')
            # ---- write the land cover class of each station
            fout.write(','.join(list(stations_lu.columns)) + '\n')
            for j in stations_lu.index:
                fout.write(stations_lu.loc[j, 'Network'] + ',')
                fout.write(stations_lu.loc[j, 'Station'] + ',')
                fout.write(str(stations_lu.loc[j, 'Lat']) + ',')
                fout.write(str(stations_lu.loc[j, 'Lon']) + ',')
                fout.write(stations_lu.loc[j, 'Depth-Actual'] + ',')
                fout.write(stations_lu.loc[j, 'Sensor'] + ',')
                fout.write(str(stations_lu.loc[j, 'Land Cover']) + ',')
                fout.write(str(stations_lu.loc[j, 'Climate Zone']) + '\n')
            fout.close()
        write_file(grid_lu, stations_lu_collect, path_out)


end = time.time()

# Time in hours.
print('The script finished in ' + ('%.4f' % ((end-start)/3600) ) + ' hours.')
