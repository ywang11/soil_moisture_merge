"""
2020/12/9
ywang254@utk.edu

Partial correlation with the GSWP3 precipitation, shortwave radiation,
 and surface temperature.
"""
import os
import numpy as np
import pandas as pd
import xarray as xr
from glob import glob
import pingouin as pg
import utils_management as mg
from utils_management.constants import depth, depth_cm, year_longest, \
    year_shorter, year_shorter2, year_shortest
from dateutil.relativedelta import relativedelta
import multiprocessing as mp


def decode_month_since(time):
    ref = pd.Timestamp(time.attrs['units'].split(' ')[2])
    start = ref + relativedelta(months = time.values[0])
    start = start.replace(day = 1, hour = 0, minute = 0, second = 0)
    return pd.date_range(start, periods = len(time), freq = 'MS')


def get_prod_list(mset, d, dcm):
    if mset == 'mean_lsm':
        flist = [os.path.join(mg.path_out(), 'meanmedian_lsm',
                              'mean_' + d + '_' + \
                              str(year_longest[0]) + '-' + \
                              str(year_longest[-1]) + '.nc')]
    elif mset == 'dolce_lsm':
        flist = [os.path.join(mg.path_out(), 'concat_dolce_lsm',
                              'positive_average_' + d + \
                              '_lu_weighted_ShrunkCovariance.nc')]
    elif (mset == 'em_all') | (mset == 'em_lsm'):
        flist = [os.path.join(mg.path_out(), 'concat_' + mset,
                              'positive_CRU_v4.03_year_month_anomaly_' + \
                              '9grid_' + dcm + '_predicted_' + \
                              str(year_longest[0]) + '-' + \
                              str(year_longest[-1]) + '.nc')]
    else:
        flist = [os.path.join(mg.path_out(), mset + '_corr',
                              'CRU_v4.03_year_month_positive_9grid_' + \
                              dcm + '_' + str(year_longest[0]) + \
                              '-' + str(year_longest[-1]), 'predicted.nc')]
    return flist


def get_prod(flist, yrng):
    hr = xr.open_mfdataset(flist, decode_times = False)
    if ('CRU_v4.03_year_month_positive_9grid_0-10cm_1950-2016' in flist[0]) |\
       ('CRU_v4.03_year_month_positive_9grid_10-30cm_1950-2016' in flist[0]) |\
       ('CRU_v4.03_year_month_positive_9grid_30-50cm_1950-2016' in flist[0]) |\
       ('CRU_v4.03_year_month_positive_9grid_50-100cm_1950-2016' in flist[0]):
        hr['time'].attrs['units'] = 'months since 1950-01-01'
    if 'month' in hr['time'].attrs['units']:
        tvec = decode_month_since(hr['time'])
    else:
        tvec = xr.decode_cf(hr)['time'].to_index()
    hr['time'] = tvec
    try:
        sm = hr['sm']
    except:
        sm = hr['predicted']
    sm = sm[(tvec.year >= yrng[0]) & (tvec.year <= yrng[-1]), : , :]
    hr.close()

    return sm


def get_gswp(varname, yrng):
    hr = xr.open_mfdataset(sorted(glob(os.path.join(mg.path_data(), 'GSWP3',
                                                    '*' + varname + '*.nc'))),
                           decode_times = True)
    var = hr[varname].copy(deep = True)
    # ---- re-order latitude
    var = var[:, ::-1, :]
    # subset the time
    var = var[(var['time'].to_index().year >= yrng[0]) & \
              (var['time'].to_index().year <= yrng[-1]), :, :]
    hr.close()
    var = var.resample(time = 'MS').mean()

    if varname == 'pr':
        var = var * 86400 # mm/day
    elif varname == 'tas':
        var = var - 273.15

    return var


def subset_season(var, season):
    if season == 'Annual':
        var_mean = var.groupby('time.year').mean()
    else:
        period = var['time'].to_index().to_period('Q-NOV')
        if season == 'DJF':
            period = period[:(-1)]
            var = var[:(-1), :, :]
            qt = 1
        elif season == 'MAM':
            qt = 2
        elif season == 'JJA':
            qt = 3
        else:
            qt = 4
        var_mean = var[period.quarter == qt, :, :].groupby('time.year').mean()
        if season == 'DJF':
            var_mean = var_mean[1:, :, :]
    return var_mean


def calc_pcr_inner(order, dict_args):
    #print(dict_args)
    if np.min(dict_args['data'].std()) < 1e-9:
        return np.nan, np.nan, order
    else:
        stats = pg.partial_corr(**dict_args)
        return stats.loc['pearson', 'r'], stats.loc['pearson', 'p-val'], order


def calc_pcr(var0, var_dict):
    """
    Must implement parallel because need to be calculated for each grid.

    https://pingouin-stats.org/generated/pingouin.partial_corr.html
    """
    names = sorted(var_dict.keys())
    tvec = var0['year']
    lat = var0['lat']
    lon = var0['lon']

    retain = np.where(~np.isnan(var0.values[0, :, :]).reshape(-1))[0]

    var0 = var0.values.reshape(-1, var0.shape[1]*var0.shape[2])
    var0 = var0[:, retain]
    var0 = np.array_split(var0, len(retain), axis = 1)

    for nn in names:
        temp = var_dict[nn].values.reshape(-1, 
                                           var_dict[nn].shape[1] * \
                                           var_dict[nn].shape[2])
        temp = temp[:, retain]
        var_dict[nn] = np.array_split(temp, len(retain), axis = 1)

    keep_all = [None] * len(retain)
    # re-order into pandas dataframe
    for rr in range(len(retain)):
        keep_all[rr] = pd.DataFrame(np.nan, 
                                    index = range(len(tvec)), 
                                    columns = ['Var0'] + names)
        ##print(rr)
        keep_all[rr]['Var0'] = var0[rr]
        for nn in names:
            ##print(nn)
            keep_all[rr][nn] = var_dict[nn][rr]
    del var0, var_dict

    for rr in range(len(retain)):
        if np.sum(np.isnan(keep_all[rr].values)) > 0:
            break

    #
    prc_collect = {}
    for nn in names:
        print(nn)
        p = mp.Pool(4)
        result = [p.apply_async(calc_pcr_inner, 
                                args = (order, 
                                        dict(data = df,
                                             x = 'Var0', y = nn, 
                                             covar = [nx for nx in names \
                                                      if nx != nn]))) \
                  for order, df in enumerate(keep_all)]
        p.close()
        p.join()

        prc_collect[nn] = np.full(len(lat) * len(lon), np.nan)
        prc_collect[nn + '_p'] = np.full(len(lat) * len(lon), np.nan)
        for ind, rr in enumerate(result):
            ##print(ind)
            r, pvalue, order = rr.get()
            prc_collect[nn][retain[order]] = r
            prc_collect[nn + '_p'][retain[order]] = pvalue

        prc_collect[nn] = (['lat','lon'],
                           prc_collect[nn].reshape(len(lat),
                                                   len(lon)))
        prc_collect[nn + '_p'] = (['lat','lon'], 
                                  prc_collect[nn + '_p'].reshape(len(lat),
                                                                 len(lon)))
    ##DEBUG
    ##nn = 'pr'
    ##for ind in range(len(retain)):
    ##    df = keep_all[ind]
    ##    calc_pcr_inner(order, dict(data = df, x = 'Var0', y = nn, 
    ##                               covar = [nx for nx in names \
    ##                                        if nx != nn]))

    #
    return xr.Dataset(prc_collect, 
                      coords = {'lat': lat, 'lon': lon})


#
yrng = range(1970, 2011) # overlap between GSWP3 and products.
prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5',
             'em_cmip6', 'em_2cmip', 'em_all']
season_list = ['Annual', 'DJF', 'MAM', 'JJA', 'SON']


#
##for mset in prod_list:
mset = prod_list[REPLACE1]
dind = REPLACE2
d = depth[dind]
dcm = depth_cm[dind]

pr = get_gswp('pr', yrng)
tas = get_gswp('tas', yrng)
rsds = get_gswp('rsds', yrng)

gswp_pr = {}
gswp_tas = {}
gswp_rsds = {}
for season in season_list:
    gswp_pr[season] = subset_season(pr, season)
    gswp_tas[season] = subset_season(tas, season)
    gswp_rsds[season] = subset_season(rsds, season)

#
flist = get_prod_list(mset, d, dcm)
prod = get_prod(flist, yrng)

for season in season_list:
    prod_mean = subset_season(prod, season)

    # scale the soil moisture values to be comparable to the 
    # other variables
    prod_mean = prod_mean * 100.

    pcr = calc_pcr(prod_mean,
                   {'pr': gswp_pr[season],
                    'tas': gswp_tas[season],
                    'rsds': gswp_rsds[season]})

    pcr.to_netcdf(os.path.join(mg.path_out(), 'validate',
                               'corr_gswp3_' + mset + '_' + d + \
                               '_' + season + '.nc'))
