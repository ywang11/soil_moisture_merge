"""
20200805
ywang254@utk.edu

Plot the merged products for global srex regions.

20200526
ywang254@utk.edu

Plot only the main regions.
"""
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
import matplotlib as mpl
import regionmask as rmk
import utils_management as mg
from utils_management.constants import depth, depth_cm, val_list, val_depth, \
    val2_list
from scipy import signal
from validate_gen_psd_plot_fewer import *


scaler = 1. # multiple the soil moisture values
# Read dataset
prod_set = pd.read_csv(os.path.join(mg.path_out(),'validate', 'prod_srex_all.csv'),
                       index_col = 0, parse_dates = True,
                       header = [0,1,2]) * scaler
source_set = pd.read_csv(os.path.join(mg.path_out(), 'validate',
                                      'source_srex_all.csv'),
                         index_col = 0, parse_dates = True,
                         header = [0,1,2]) * scaler


sid_list = [['1','2','4','5','6','8','9','10'], 
            ['11','12','14','16','17','18','19','20'],
            ['21','23','24','26']]
for ind_0 in range(3):
    fig = plt.figure(figsize = (6.5, 7))
    gs = gridspec.GridSpec(2, 4, hspace = 0.08, wspace = 0.08)

    for ind, sid in enumerate(sid_list[ind_0]):
        prod_by_depth = {}
        for dind, dcm in enumerate(depth_cm):
            prod_by_depth[dind] = prod_set.loc[:, (slice(None), dcm, str(sid))]
            prod_by_depth[dind].columns = prod_by_depth[dind].columns.droplevel([1,2])
            
        source_by_depth = {}
        for dind, dcm in enumerate(depth_cm):
            source_by_depth[dind] = source_set.loc[:, (slice(None), dcm, str(sid))]
            source_by_depth[dind].columns = source_by_depth[dind].columns.droplevel([1,2])
    
        gs_panel = gs[ind//4, np.mod(ind, 4)]
        h, hf, axes, sub_gs = small_panel(gs_panel, prod_by_depth, source_by_depth)
        axes[0].set_title(rmk.defined_regions.srex.names[int(sid)-1])
    
        for i in range(4):
            axes[i].text(0.02, 0.85, '(' + lab[ind+ind_0*8] + str(i) + ') ' + \
                         depth_cm[i].replace('-', u'\u2212').replace('cm',
                                                                     ' cm'),
                         transform = axes[i].transAxes)
        if (ind >= 4) or (ind_0 == 2):
            for i in range(4):
                plt.setp(axes[i].get_xticklabels(), visible=True)
            axes[3].set_xlabel('Period (year)')
        else:
            for i in range(4):
                plt.setp(axes[i].get_xticklabels(), visible=False)
    
        if np.mod(ind,4) == 0:
            axes[1].text(-0.3, 0., 'Power', horizontalalignment = 'center', rotation = 90,
                         transform = axes[1].transAxes)
        else:
            for i in range(4):
                plt.setp(axes[i].get_yticklabels(), visible=False)
    
        if (ind_0 == 2) & (ind == 0):
            axes[3].legend(h + [hf], prod_names + ['Source datasets'],
                           loc = (0.3, -0.9), ncol = 6)
    
    fig.savefig(os.path.join(mg.path_out(), 'validate',
                             'srex_psd_rest_' + str(ind_0) + '.png'), 
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
