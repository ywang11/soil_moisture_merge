"""
2020/05/19

ywang254@utk.edu

Draw the global maps of the emergent constraint slopes, when based on the range
 of land surface models, and the significance of the slopes.
"""
import numpy as np
import xarray as xr
import utils_management as mg
from utils_management.constants import depth, year_longest, year_shorter, \
    year_shorter2, year_shortest, depth_cm, target_lat, target_lon
import sys
import os
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
import cartopy.crs as ccrs
import itertools as it
from misc.plot_utils import cmap_gen
import multiprocessing as mp


met_obs = 'CRU_v4.03'
me = 'year_month_anomaly_9grid'
year = year_longest
year_str = str(year[0]) + '-' + str(year[-1])


year_str_list1 = [str(year_shorter[0]) + '-' + str(year_shorter[-1]),
                  str(year_shortest[0]) + '-' + str(year_shortest[-1]),
                  str(year_shorter2[0]) + '-' + str(year_shorter2[-1])]
year_used_list1 = [range(1950,1981), range(1981, 2011), range(2011, 2017)]


em_list = ['lsm', 'cmip5', 'cmip6', '2cmip', 'all']
em_name = ['ORS', 'CMIP5', 'CMIP6', 'CMIP5+6', 'ALL']


mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 6
mpl.rcParams['hatch.linewidth'] = 0.5
lab = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
       'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
       'y', 'z', 'aa', 'bb', 'cc', 'dd', 'ee', 'ff', 'gg', 'hh',
       'ii', 'jj', 'kk', 'll', 'mm', 'nn', 'oo', 'pp', 'qq', 'rr',
       'ss', 'tt', 'uu', 'vv', 'ww', 'xx']


cmap1 = cmap_gen('autumn', 'winter_r')
cmap2 = get_cmap('gist_gray_r')
levels2 = np.linspace(0, 1., 41)


for vname in ['p', 't']:
    if vname == 'p':
        levels1 = np.linspace(-0.04, 0.04, 41)
    else:
        levels1 = np.linspace(-0.04, 0.04, 41)

    fig, axes = plt.subplots(nrows = 2 * len(depth_cm), ncols = len(em_list),
                             figsize = (6.5, 6.),
                             subplot_kw = {'projection': ccrs.Miller()})
    fig.subplots_adjust(wspace = 0., hspace = 0.)
    count = 0
    for dind, mind in it.product(range(4), range(len(em_list))):
        dcm = depth_cm[dind]
        model = em_list[mind]

        #
        hr = xr.open_dataset(os.path.join(mg.path_out(), mg.path_out(),
                                          'summary_em_corr',
                                          model + '_' + dcm + '.nc'))
        corr = hr['corr_' + vname].values.copy()
        em_applied = hr['em_applied_' + vname \
        ].where(~np.isnan(corr)).values.copy()
        hr.close()

        # Each grid, 1st row, corr
        ax = axes[dind * 2, mind]
        ax.coastlines(lw = 0.5, color = 'grey')
        ax.set_extent([-180, 180, -60, 90])
        ax.outline_patch.set_visible(False)
        ax.spines['top'].set_visible(True)
        ax.spines['bottom'].set_visible(True)
        ax.spines['left'].set_visible(True)
        ax.spines['right'].set_visible(True)
        ax.spines['top'].set_lw(0.1)
        ax.spines['bottom'].set_lw(0.1)
        ax.spines['left'].set_lw(0.1)
        ax.spines['right'].set_lw(0.1)
    
        cf1 = ax.contourf(hr.lon.values, hr.lat.values, corr,
                          cmap = cmap1, transform = ccrs.PlateCarree(),
                          levels = levels1, extend = 'both')
        if dind == 0:
            ax.set_title(em_name[mind])
        if mind == 0:
            ax.text(-0.1, 0.5, 'Slope', transform = ax.transAxes,
                    verticalalignment = 'center', rotation = 90)
            ax.text(-0.2, 0., dcm.replace('-', u'\u2212').replace('cm',
                                                                  ' cm'),
                    transform = ax.transAxes,
                    verticalalignment = 'center', rotation = 90)
        ax.text(0.01, 0.85, '(' + lab[count] + ')',
                transform = ax.transAxes)
        count += 1

        # Each grid, 2nd row, percent applied em
        ax = axes[dind * 2 + 1, mind]
        ax.coastlines(lw = 0.5, color = 'grey')
        ax.set_extent([-180, 180, -60, 90])
        ax.outline_patch.set_visible(False)
        ax.spines['top'].set_visible(True)
        ax.spines['bottom'].set_visible(True)
        ax.spines['left'].set_visible(True)
        ax.spines['right'].set_visible(True)
        ax.spines['top'].set_lw(0.1)
        ax.spines['bottom'].set_lw(0.1)
        ax.spines['left'].set_lw(0.1)
        ax.spines['right'].set_lw(0.1)

        cf2 = ax.contourf(hr.lon.values, hr.lat.values, em_applied,
                          cmap = cmap2, transform = ccrs.PlateCarree(),
                          levels = levels2, extend = 'neither')
        if mind == 0:
            ax.text(-0.1, 0.5, 'Percentage', transform = ax.transAxes,
                    verticalalignment = 'center', rotation = 90)
        ax.text(0.01, 0.85, '(' + lab[count] + ')',
                transform = ax.transAxes)
        count += 1

    cax1 = fig.add_axes([0.13, 0.08, 0.36, 0.01])
    plt.colorbar(cf1, cax = cax1, orientation = 'horizontal',
                 boundaries = levels1, ticks = levels1[::8],
                 label = 'Slope')
    cax2 = fig.add_axes([0.54, 0.08, 0.36, 0.01])
    plt.colorbar(cf2, cax = cax2, orientation = 'horizontal',
                 boundaries = levels2, ticks = levels2[::8],
                 label = 'Percentage')

    fig.savefig(os.path.join(mg.path_out(), 'summary_em_corr',
                             vname + '.png'), dpi = 600.,
                bbox_inches = 'tight')
    plt.close(fig)
