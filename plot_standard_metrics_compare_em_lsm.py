"""
2019/07/23

ywang254@utk.edu

Compare the standard metrics between the different emergent constraint
approaches for the land surface models.
"""
import os
import sys
import pandas as pd
import numpy as np
from utils_management.constants import depth, year_longest, year_shorter, \
    year_shorter2, year_shortest
import utils_management as mg
import matplotlib.pyplot as plt
from misc.ismn_utils import get_ismn_aggr_method
import itertools as it

# MODIFY
USonly = False # True, False

if USonly:
    suffix = '_USonly'
else:
    suffix = ''

year_list = [year_longest, year_shorter, year_shorter2, year_shortest]


# Weighting methods
em_lsm_method = ['month_1grid', 'year_month_9grid',  
                 'year_mw_1grid', 'year_mw_9grid', 
                 'month_anomaly_1grid', 'year_month_anomaly_9grid',
                 'year_mw_anomaly_1grid', 'year_mw_anomaly_9grid']


# Separate plot for each metric.
for metric in ['RMSE', 'Bias', 'uRMSE', 'Corr']:
    fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(10,10),
                             sharex=True, sharey=True)
    for d_ind, d in enumerate(depth):
        # Collect the results.
        metric_collect = {}
        ##metric_concat_collect = {} // skip b.c. done elsewhere
        for em in em_lsm_method:
            metric_collect[em] = np.zeros([0, 3])
            for year in year_list:
                data = pd.read_csv(os.path.join(mg.path_out(),
                                                'standard_metrics', 
                                                'em_lsm_' + d + '_' + \
                                                str(year[0]) + '-' + \
                                                str(year[-1]) + suffix + \
                                                '.csv'), 
                                   header = [0,1], index_col = [0,1] \
                ).loc[(slice(None), em), (metric, slice(None))].values
                metric_collect[em] = np.concatenate([metric_collect[em],
                                                     data], axis = 0)
            ##year = year_longest
            ##metric_concat_collect[(em, pr)] = \
            ##    pd.read_csv(os.path.join(mg.path_out(), 'standard_metrics',
            ##                             'concat_em_lsm_' + d + '_' + \
            ##                             str(year[0]) + '-' + \
            ##                             str(year[-1]) + '.csv'), 
            ##                header = [0,1], index_col = [0,1,2] \
            ##    ).loc[(slice(None), em, pr), (metric, slice(None))].values

        # Plot the collected results.
        ax = axes.flat[d_ind]

        for em_ind, em in enumerate(em_lsm_method):
            bp = ax.boxplot(metric_collect[em].reshape(-1,1),
                            positions = [em_ind], 
                            patch_artist=True, whis = [5, 95])
            for element in ['boxes', 'whiskers', 'fliers', 'means', 
                            'caps']:
                plt.setp(bp[element], color='grey')
            plt.setp(bp['medians'], color = 'k')
            for patch in bp['boxes']:
                patch.set(facecolor = [0,0,0,0])

            ##ax.plot([em_ind+(pr_ind-1)/3] * 9,
            ##        metric_concat_collect[(em, pr)].reshape(-1), 'ob',
            ##        markersize = 3)

        ##if (metric == 'uRMSE') | (metric == 'RMSE'):
        ##    ax.set_ylim([0.07, 0.16]) # 0.5 - b.c. some in UDEL are too big.

        ax.set_xticks(range(len(em_lsm_method)))
        ax.set_xticklabels(em_lsm_method, rotation = 90.)
        ax.set_title('Depth: ' + d)
        ax.set_ylabel(metric)

    fig.savefig(os.path.join(mg.path_out(), 'standard_metrics', 
                             'plot_compare_em_lsm_' + metric + suffix + \
                             '.png'), dpi = 600., bbox_inches='tight')
    plt.close(fig)
