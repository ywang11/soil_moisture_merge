"""
2019/07/05

ywang254@utk.edu

Map the global mean and seasonal mean soil moisture for the DOLCE-weighted
 land surface models.
"""
from utils_management.constants import depth, year_longest, \
    year_shorter, year_shorter2, year_shortest
import utils_management as mg
from misc.ismn_utils import get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
from misc.plot_standard_diagnostics_utils import plot_map
import numpy as np
import os
import itertools as it


levels = np.linspace(0., 0.6, 15)
cmap = 'RdYlBu'


year_list = [year_longest, year_shorter, year_shorter2, year_shortest]


simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = [get_ismn_aggr_method(simple[iam], 
                                         dominance_lc[iam], 
                                         dominance_threshold) for iam \
                    in range(len(simple))]

cov_method = [get_cov_method(cov) for cov in range(5)]

for year, ismn, cov in it.product(year_list, ismn_aggr_method, 
                                  cov_method):
    plot_map(depth, os.path.join(mg.path_out(), 'standard_diagnostics_dolce',
                                 ismn + '_' + cov + '_weighted_average_' + \
                                 str(year[0]) + '-' + str(year[-1])), 
             os.path.join(mg.path_out(), 'standard_diagnostics_plot', 
                          'mean_dolce', ismn + '_' + cov + \
                          '_' + str(year[0]) + '-' + str(year[-1])), 'map',
             levels, cmap, False)
