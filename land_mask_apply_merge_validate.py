"""
2020/12/06

ywang254@utk.edu

Apply the common land mask on the interpolated, unmasked, after their
interpolation to 0.5 deg.

Need to run "land_mask_minimum_cmip6.py" "land_mask_minimum_obs_pr.py"
            "land_mask_minimum_cmip5.py" "land_mask_minimum_lsm.py"
            "land_mask_minimum_merge.py" first. The "_merge" is the final
            land mask.
"""
import sys
import os
import xarray as xr
import numpy as np
import pandas as pd
from glob import glob
import utils_management as mg
from utils_management.constants import lsm_all
import multiprocessing as mp


land_mask = 'vanilla' # 'e1m', 'MODIS', 'vanilla'


###############################################################################
# Read the merged land mask.
###############################################################################
data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'land_mask_minimum', 
                                    'merge_' + land_mask + '.nc'))
mask = data.mask.values.copy()
data.close()


###############################################################################
# Apply on the merged datasets.
###############################################################################
for m_v in ['GLEAMv3.3a', 'SMOS_L3', 'SMOS_L4', 'SMERGE_v2', 'SoMo']:
    file_list = glob(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                  'None', m_v, '*.nc'))
    for f in file_list:
        data = xr.open_dataset(f, decode_times = False)
        data2 = data.where(mask)
        data2.to_netcdf(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                     land_mask, m_v, 
                                     f.split('/')[-1]))
        data.close()
