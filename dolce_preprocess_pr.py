# -*- coding: utf-8 -*-
"""
Created on Sat May 25 15:21:16 2019

@author: ywang254

Extract the monthly precipitation from monthly CRU_NCEP7 dataset at the
ISMN observational data points.
"""
import xarray as xr
import numpy as np
import pandas as pd
import utils_management as mg
from utils_management.constants import depth
from misc.dolce_utils import get_weighted_monthly_data, get_ismn_aggr_method
import os
import time


# Choose the weighting method that was used to generate the weighted averaged
# ISMN data.
##from ismn_aggr_dolce import simple, dominance_lc, dominance_threshold
# ---- default to LU-weighted, not cut off by minimum pct represented 
#      land cover
simple = False # [True, False]
dominance_lc = True # [True, False]
dominance_threshold = 40
ismn_aggr_method = get_ismn_aggr_method(simple, dominance_lc,
                                        dominance_threshold)


start = time.time()


for d in depth:
    grid_latlon, weighted_monthly_data, available_year = \
        get_weighted_monthly_data(os.path.join(mg.path_intrim_out(), 
                                  'dolce_ismn_aggr'), ismn_aggr_method, d)

    def get_pr_points(grid_latlon, pr, year):
        pr_at_data = pd.DataFrame(data = np.nan, index = pd.to_datetime([ \
                                  str(y) + '-' + ('%02d' % m) + '-01' \
                                  for y in year for m in range(1,13)],
                                  format='%Y-%m-%d'),
                                  columns = grid_latlon.columns)

        # ---- create a mask
        gr_mask = np.zeros(pr.shape[1:], bool)
        gr_mask_label = np.array([['        '] * pr.shape[2]] * pr.shape[1])
        for gr in grid_latlon.columns:
            ind_x = int(np.argmin(np.abs(pr.lat - \
                        grid_latlon.loc['Lat', gr])))
            ind_y = int(np.argmin(np.abs(pr.lon - \
                        grid_latlon.loc['Lon', gr])))
            gr_mask[ind_x, ind_y] = True
            gr_mask_label[ind_x, ind_y] = gr

        gr_mask_label = gr_mask_label[gr_mask].reshape(-1)

        for t in range(pr.shape[0]):
            pr = pr.values[t, :, :]
            pr_at_data.loc[pr_at_data.index[t], gr_mask_label] = \
                pr[gr_mask].reshape(-1)

        return pr_at_data

    data = xr.open_mfdataset([os.path.join(mg.path_intrim_out(), 
        'CRU_NCEP7_downsampled', str(y) + '.nc') for y in \
        range(available_year[0], available_year[-1])])
    pr = data.pr
    pr_at_data = get_pr_points(grid_latlon, pr, available_year)
    pr_at_data = pr_at_data.loc[weighted_monthly_data.index, :]
    pr_at_data.to_csv(os.path.join(mg.path_intrim_out(), 
                                   'dolce_preprocess_pr', 
                                   'depth_' + d + '.csv'))
    data.close()


end = time.time()
print('The script finished in ' + ('%.4f' % ((end-start)/3600) ) + ' hours.')