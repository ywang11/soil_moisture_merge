"""
20191118
ywang254@utk.edu

Obtain the lag that has the maximum correlation.
"""
import utils_management as mg
from utils_management.constants import depth, depth_cm, target_lat, \
    target_lon, year_longest, year_shorter, year_shorter2, year_shortest, \
    lsm_list
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability
import os
import xarray as xr
import pandas as pd
import numpy as np
import itertools as it
import multiprocessing as mp


land_mask = 'vanilla'


##model = 'cmip5' # 'cmip5', 'cmip6'
##i = 0 # [0, 1, 2, 3]
##var = 'temperature' # 'temperature', 'precipitation'


def plotter(option):
    model, i, var = option
    d = depth[i]
    dcm = depth_cm[i]

    if model == 'lsm':
        model_list = lsm_list[(str(year_shortest[0]) + '-' + \
                               str(year_shortest[-1]), d)]
    elif model == 'cmip5':
        model_list = cmip5_availability(dcm, land_mask)
    elif model == 'cmip6':
        model_list = list(set(mrsol_availability(dcm, land_mask, 
                                                 'historical')) &\
                          set(mrsol_availability(dcm, land_mask, 'ssp585')))
        model_list = sorted([x for x in model_list if 'r1i1p1f1' in x])

    for month in range(12):
        max_lag = np.full([len(model_list), len(target_lat), len(target_lon)],
                          np.nan)

        for m_ind, m in enumerate(model_list):
            corr = np.full([12, len(target_lat), len(target_lon)], np.nan)
            for lag_ind, lag in enumerate(range(-6, 6)):
                data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 
                                                    'em_tas_lagged_corr_month', 
                                                    model + '_' + m + '_' + \
                                                    dcm + '_' + str(month) + \
                                                    '_' + str(lag) + '_' + \
                                                    var + '.nc'))
                corr[lag_ind, :, :] = data.corr.values
                data.close()
            temp = np.argmax(corr, axis = 0) - 6.
            temp[np.isnan(corr[0, :, :])] = np.nan
            max_lag[m_ind, :, :] = temp

        max_lag = xr.DataArray(max_lag, 
                               coords = {'model': range(len(model_list)),
                                         'lat': target_lat, 'lon': target_lon},
                               attrs = {'model_list': ','.join(model_list)},
                               dims = ['model', 'lat',
                                       'lon']).to_dataset(name = 'max_lag')

        max_lag.to_netcdf(os.path.join(mg.path_intrim_out(),
                                       'em_tas_lagged_corr_month',
                                       'max_lag_' + model + '_' + dcm + \
                                       '_' + str(month) + '_' + var + '.nc'))

pool = mp.Pool(4)
pool.map_async(plotter, list(it.product(['lsm', 'cmip5', 'cmip6'], range(4),
                                        ['temperature', 'precipitation'])))
pool.close()
pool.join()
