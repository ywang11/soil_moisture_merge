"""
2019/10/05

Plot the pairwise difference between the annual and seasonal climatology of 
 ESA-CCI, GLEAM, ERA-Land, Mean-All, Median-Products.
"""
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from cartopy.util import add_cyclic_point
import utils_management as mg
from utils_management.constants import depth_cm, depth, target_lat, target_lon
import os
import xarray as xr
from misc.plot_utils import cmap_gen
import statsmodels.api as sm
import numpy as np
import matplotlib as mpl


mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 6

year_str = '1950-2016'

i = 1
d = depth[i]
dcm = depth_cm[i]

season = 'annual' # 'annual', 'DJF', 'JJA', 'MAM', 'SON'

# Read and collect the datasets.
collection = {}
# ---- Median-Products
data = xr.open_dataset(os.path.join(mg.path_out(),
                                    'standard_diagnostics_meanmedian_products',
                                    'median_' + year_str + '_' + d + \
                                    '_g_map_' + season + '.nc'))
collection['Median-Products'] = data.g_map.values.copy()
data.close()
# ---- Mean-All
data = xr.open_dataset(os.path.join(mg.path_out(),
                                    'standard_diagnostics_meanmedian_all',
                                    'mean_' + year_str + '_' + d + \
                                    '_g_map_' + season + '.nc'))
collection['Mean-All'] = data.g_map.values.copy()
data.close()
# ---- ERA-Land
data = xr.open_dataset(os.path.join(mg.path_out(),
                                    'standard_diagnostics_lsm',
                                    'ERA-Land_' + d + '_g_map_' + \
                                    season + '.nc'))
collection['ERA-Land'] = data.g_map.values.copy()
data.close()

#
fig, axes = plt.subplots(nrows = 3, ncols = 3, figsize = (6.5, 6.5),
                         subplot_kw = {'projection': ccrs.Miller()})
for row, i in enumerate(['ERA-Land', 'Mean-All', 'Median-Products']):
    for col, j in enumerate(['ERA-Land', 'Mean-All', 'Median-Products']):
        ax = axes[row, col]

        if row <= col:
            ax.set_extent([-180, 180, -70, 90])
            ax.coastlines()
            if col == row:
                # plot the climatology of each product
                Z_cyc, x_cyc = add_cyclic_point(collection[i], target_lon)
                cmap = 'Spectral'
                levels = np.arange(0., 0.61, 0.1)
            else:
                # plot the difference (row - col)
                Z_cyc, x_cyc = add_cyclic_point(collection[j] - \
                                                collection[i], target_lon)
                cmap = cmap_gen('autumn', 'winter_r')
                levels = np.arange(-0.2, 0.21, 0.05)
            cf = ax.contourf(x_cyc, target_lat, Z_cyc, 
                             transform = ccrs.PlateCarree(), cmap = cmap,
                             levels = levels, extend = 'both')
            cb = plt.colorbar(cf, ax = ax, shrink = 0.9, extend = 'both',
                              boundaries = levels, anchor = (0.5, -0.1),
                              orientation = 'horizontal', pad = 0.05)
            cb.ax.set_xticklabels(cb.ax.get_xticklabels(), rotation=60)

            if (row == 0) & (col == 0):
                # ---- get the aspect for consistency setting later
                asp = ax.get_aspect()
        else:
            # plot the spatial correlation in climatology
            X = collection[j].reshape(-1)
            y = collection[i].reshape(-1)

            remove = np.isnan(X) | np.isnan(y)
            X = X[~remove]
            y = y[~remove]

            mod = sm.OLS(y, sm.add_constant(X))
            result = mod.fit()

            ax.plot(X, y, '.k', markersize = 0.1)
            ax.plot([np.min(X), np.max(X)],
                    [result.params[1] * np.min(X) + result.params[0],
                     result.params[1] * np.max(X) + result.params[0]], 
                    '-b')
            ax.text(0.4, 0.9, 'R$^2$ = %.2f' % result.rsquared,
                    transform = ax.transAxes)
            ax.set_xlim([0., 0.8])
            ax.set_ylim([0., 0.8])
            ax.set_aspect(0.9)
            if col == 0:
                ax.set_yticks(np.arange(0., 0.81, 0.2))
                ax.yaxis.set_tick_params(labelleft = True)
            if row == 4:
                ax.set_xticks(np.arange(0., 0.81, 0.2))
                ax.xaxis.set_tick_params(labelbottom = True)

        if row == 0:
            ax.set_title(j)
        if col == 0:
            ax.set_ylabel(i)

fig.savefig(os.path.join(mg.path_out(), 'other_diagnostics_plot',
                         'climatology_' + season + '_10-30cm.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)
