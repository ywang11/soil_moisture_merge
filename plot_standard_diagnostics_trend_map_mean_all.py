"""
2019/10/04
ywang254@utk.edu

Draw the global soil moisture trend map of the mean of the source datasets
 (lsm + cmip5 + cmip6), with hatched area indicating >66.7% of the created
 datasets agree on the sign of trend.
"""
import os
import pandas as pd
from utils_management.constants import depth, target_lat, target_lon, \
    lsm_list, depth_cm
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import utils_management as mg
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import itertools as it
from misc.plot_utils import plot_map_w_stipple
import matplotlib as mpl
print(mpl.__version__) # should be 2.x.y
from misc.plot_utils import cmap_gen


levels = np.linspace(-0.0005, 0.0005, 11)
cmap = cmap_gen('autumn', 'winter_r')

mpl.rcParams['hatch.linewidth'] = 0.2  # previous pdf hatch linewidth

land_mask = 'vanilla'

for i,d in enumerate(depth):
    dcm = depth_cm[i]

    # Source models
    year_str = '1950-2016'
    lsm = lsm_list[(year_str, d)]
    cmip5 = cmip5_availability(dcm, land_mask)
    cmip6_list_1 = mrsol_availability(dcm, land_mask, 'historical')
    cmip6_list_2 = mrsol_availability(dcm, land_mask, 'ssp585')
    cmip6_list_3 = one_layer_availability('pr', land_mask, 'historical')
    cmip6_list_4 = one_layer_availability('pr', land_mask, 'ssp585')
    cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) & \
                       set(cmip6_list_3) & set(cmip6_list_4) )
    cmip6_list = [x for x in cmip6_list if 'r1i1p1f1' in x]

    #
    for ss, ss2 in zip(['Annual', 'All', 'DJF', 'MAM', 'JJA', 'SON'],
                       ['annual', 'all', 'DJF', 'MAM', 'JJA', 'SON']):
        # Annual trend points.
        data = xr.open_dataset(os.path.join(mg.path_out(),
            'standard_diagnostics_meanmedian_all',
            'mean_1950-2016_' + d + '_g_map_trend_' + ss + '.nc'))
        product_trend = data['g_map_trend'].values.copy()
        data.close()

        # Loop through the source datasets and obtain the annual trend points.
        consistent_pct = np.zeros(product_trend.shape, dtype = int)
        for group, model in zip(['lsm', 'cmip5', 'cmip6'],
                                [lsm, cmip5, cmip6_list]):
            for l in model:
                if group == 'lsm':
                    data = xr.open_dataset(os.path.join(mg.path_out(), 
                        'standard_diagnostics_' + group, l + '_' + d + \
                        '_g_map_trend_' + ss + '.nc'))
                elif group == 'cmip5':
                    data = xr.open_dataset(os.path.join(mg.path_out(), 
                        'standard_diagnostics_' + group, l + '_' + dcm + \
                        '_g_map_trend_' + ss + '.nc'))
                else:
                    data = xr.open_dataset(os.path.join(mg.path_out(), 
                        'standard_diagnostics_' + group, 'mrsol_' + l + \
                        '_' + dcm + '_g_map_trend_' + ss + '.nc'))
                consistent_pct = consistent_pct + ((product_trend > 0) & \
                     (data['g_map_trend'].values.copy() > 0)) | \
                    ((product_trend < 0) & \
                     (data['g_map_trend'].values.copy() < 0))
                data.close()
        consistent_pct = consistent_pct / (len(lsm) + len(cmip5) + \
                                           len(cmip6_list))

        # Make the plot w/ stipples at >66.7% model agree.
        fig, ax = plt.subplots(subplot_kw = {'projection': ccrs.PlateCarree()},
                               figsize = (8,6))
        plot_map_w_stipple(ax, product_trend, 1 - consistent_pct > 0.666,
                           target_lat, target_lon, thresh = 0.05,
                           add_cyc = True,
                           contour_style = {'levels': levels, 'cmap': cmap,
                                            'extend': 'both'},
                           cbar_style = {'boundaries': levels, 'pad': 0.1})
        ax.set_extent([-180, 180, -80, 90])
        ax.gridlines(draw_labels = True, xlocs = np.arange(-180, 181, 30),
                     ylocs = np.arange(-90, 91, 30))
        fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                                 'trend_map_mean_all_' + ss2 + '_' + \
                                 d + '.png'), dpi = 600., 
                    bbox_inches = 'tight')
        plt.close(fig)
