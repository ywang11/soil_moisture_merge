"""
2019/06/24

ywang254@utk.edu

The emergent constraint model constantly throws errors because CRU 
precipitation are constant over the year. Identify the grids and months 
where:
(1) mean annual precip = 1.
(2) standard deviation < 1e-6

So that em_lsm_corr.py can skip these grid cells in month (all years) 
regressions that only involve CRU precipitation.

Also, identify the grids, and year-months where:
(3) standard deviation in the adjacent 9 grid cells < 1e-6

So that em_lsm_corr.py can skip these grid cells in year-months regression
that only involve CRU precipitation.
"""
import numpy as np
import pandas as pd
import xarray as xr
import utils_management as mg
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest
import os
import sys
if sys.platform == 'linux':
    os.environ['QT_QPA_PLATFORM']='offscreen'
import matplotlib.pyplot as plt
from matplotlib import colors
from matplotlib import cm
plt.switch_backend('agg') # solve the display problem
import cartopy.crs as ccrs
from misc.plot_utils import plot_map_w_pts
from misc.spatial_utils import extract_grid
import multiprocessing as mp
import time
import itertools as it


year = year_shorter # year_longest, year_shorter, year_shorter2, year_shortest
land_mask = 'SYNMAP' # the land mask used to filter CRU data


data = xr.open_mfdataset([os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                       land_mask, 'CRU_Merged',
                                       'CRU_Merged_pr_' + \
                                       str(y) + '.nc') for y in year],
                         decode_times=False)
data['time'] = pd.date_range(str(year[0])+'-01-01', 
                             str(year[-1])+'-12-31', freq='MS')
pr_mean = data.pr.groupby('time.month').mean(dim='time').values
pr_std = data.pr.groupby('time.month').std(dim='time').values
lon, lat = np.meshgrid(data.lon.values.copy(), data.lat.values.copy())


###############################################################################
# Plot the global precipitation with symbols where the mean = 1.
###############################################################################
# ---- save the identified data points 
f = open(os.path.join(mg.path_intrim_out(), 'cru_qc', 'mean_pr_pts_' + \
                      str(year[0])+'-'+str(year[-1])+'.txt'), 'w')
fig, axes = plt.subplots(nrows=4, ncols=3, figsize=(25,15),
                         subplot_kw = {'projection': ccrs.PlateCarree()})
for m in range(12):
    ax = axes.flat[m]

    # ---- where precipitation == 1
    mask = np.abs(pr_mean[m, :, :] - 1.) < 1e-6
    pts = pd.DataFrame(np.concatenate([lon[mask].reshape(-1,1), 
                                       lat[mask].reshape(-1,1)], axis=1), 
                       columns = ['Lon', 'Lat'])

    f.writelines('Month = ' + str(m) + '\n')
    f.writelines('Lon: ' + ','.join([str(x) for x in pts['Lon']]) + '\n')
    f.writelines('Lat: ' + ','.join([str(x) for x in pts['Lat']]) + '\n')


    upper_bound = np.nanmax(pr_mean)
    levels = np.exp(np.linspace(-3, np.log(upper_bound)*1.01, 
                                num=11))
    levels[0] = 0.
    norm = colors.BoundaryNorm(boundaries=levels, ncolors=256)    

    plot_map_w_pts(ax, pr_mean[m,:,:], data.lat.values, data.lon.values, 
                   pts, add_cyc=True, 
                   contour_style = {'cmap': 'plasma', 
                                    'levels': levels, 
                                    'vmin': 0., 'vmax': upper_bound,
                                    'norm': norm},
                   cbar_style = {'norm': norm, 'boundaries': levels,
                                 'ticks': levels},
                   pts_style = {'linestyle': '', 
                                'marker': 'o', 'markersize': 3, 
                                'markerfacecolor': '#00FF00', 
                                'markeredgecolor': '#00FF00'})
    ax.set_title('Month = ' + str(m))
fig.savefig(os.path.join(mg.path_intrim_out(), 'cru_qc', 
                         'mean_pr_'+str(year[0])+'-'+str(year[-1])+'.png'),
            dpi=600.)
f.close()


###############################################################################
# Plot the global standard deviation of precip with symbols where std = 0.
###############################################################################
# ---- save the identified data points 
f = open(os.path.join(mg.path_intrim_out(), 'cru_qc',
                      'std_pr_pts_'+str(year[0])+'-'+str(year[-1])+'.txt'),
         'w')
fig, axes = plt.subplots(nrows=4, ncols=3, figsize=(25,15),
                         subplot_kw = {'projection': ccrs.PlateCarree()})
for m in range(12):
    ax = axes.flat[m]

    # ---- locations where standard deviation = 0.
    mask = np.abs(pr_std[m, :, :]) < 1e-6
    pts = pd.DataFrame(np.concatenate([lon[mask].reshape(-1,1), 
                                       lat[mask].reshape(-1,1)], axis=1), 
                       columns = ['Lon', 'Lat'])

    f.writelines('Month = ' + str(m) + '\n')
    f.writelines('Lon: ' + ','.join([str(x) for x in pts['Lon']]) + '\n')
    f.writelines('Lat: ' + ','.join([str(x) for x in pts['Lat']]) + '\n')

    temp = pr_mean[m, :, :]
    temp = temp[mask].reshape(-1,1)
    f.writelines('pr_mean: ' + ','.join([('%.4f' % x) for x in temp]) + '\n')

    upper_bound = np.nanmax(pr_std)
    levels = np.exp(np.linspace(-3, np.log(upper_bound)*1.01, 
                                num=11))
    levels[0] = 0.
    norm = colors.BoundaryNorm(boundaries=levels, ncolors=256)    

    plot_map_w_pts(ax, pr_std[m,:,:], data.lat.values, data.lon.values,
                   pts, add_cyc=True, 
                   contour_style = {'cmap': 'plasma', 
                                    'levels': levels, 
                                    'vmin': 0., 'vmax': upper_bound,
                                    'norm': norm}, 
                   cbar_style = {'norm': norm, 'boundaries': levels,
                                 'ticks': levels}, 
                   pts_style = {'linestyle': '', 
                                'marker': 'o', 'markersize': 3, 
                                'markerfacecolor': '#00FF00', 
                                'markeredgecolor': '#00FF00'})
    ax.set_title('Month = ' + str(m))
fig.savefig(os.path.join(mg.path_intrim_out(), 'cru_qc', 
                         'std_pr_'+str(year[0])+'-'+str(year[-1])+'.png'), 
            dpi=600.)
f.close()


###############################################################################
# Create the NetCDF that records the standard deviation across nine grid cells.
###############################################################################
pr = data.pr.values

std9 = xr.DataArray(data = np.empty([len(data.time), len(data.lat), 
                                     len(data.lon)]), 
                    coords = {'time': data.time, 
                              'lat': data.lat, 'lon': data.lon}, 
                    dims = ['time', 'lat', 'lon'], name = 'std9')
std9[:,:,:] = np.nan

def para_fill(y):
    result = np.empty([len(data.lat.values), len(data.lon.values)])
    result[:,:] = np.nan
    for a, b in it.product(range(len(data.lat.values)), 
                           range(len(data.lon.values))):
        a_lat = data.lat.values[a]
        a_lon = data.lon.values[b]
        if not np.isnan(pr[y, a, b]):
            nearby = extract_grid(pr[y, :, :], data.lat.values, 
                                  data.lon.values, 
                                  a_lat, a_lon, 3)
            result[a,b] = np.std(nearby[~np.isnan(nearby)])
    return result


print("Number of processors: ", mp.cpu_count())


start = time.time()
pool = mp.Pool(mp.cpu_count())
##for y in range(pr.shape[0]):
##    std9[y, :, :] = para_fill(y)
results = pool.map(para_fill, list(range(pr.shape[0])))
end = time.time()
pool.close()
print(end - start)

for i in range(len(results)):
    std9[i,:,:] = results.pop(0)


std9.to_dataset(name = 'std9').to_netcdf( \
    os.path.join(mg.path_intrim_out(), 'cru_qc', 'std9_' + \
                 str(year[0]) + '-' + str(year[-1]) + '.nc') )

data.close()


###############################################################################
# Map the number of year-months in each grid that has std < 1e-6 around the
# nearest 9 grid cells.
###############################################################################
data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'cru_qc', 
                                    'std9_' + str(year[0]) + '-' + \
                                    str(year[-1]) + '.nc'))
std9 = data.std9

std9_count = (std9 < 1e-6).groupby('time.month').sum(axis=0)
mask = np.tile(np.isnan(std9[0,:,:]), (12,1,1))
std9_count = std9_count.where(~mask)

fig, axes = plt.subplots(nrows=4, ncols=3, figsize=(25,15), 
                         subplot_kw={'projection': ccrs.PlateCarree()})
for m in range(12):
    ax = axes.flat[m]
    ax.set_global()
    ax.coastlines()
    
    upper_bound = np.max(std9_count)
    levels = np.concatenate([np.array([0.]),
                             np.linspace(1e-6, upper_bound*1.01, 10)])
    cmap = cm.get_cmap('plasma_r', 11)
    ##cmap = colors.ListedColormap( \
    ##    np.concatenate([ np.array([0., 1., 0., 1.]).reshape(1,-1), 
    ##                     cmap.colors ], axis = 0))
    cmap = np.concatenate([ np.array([0., 1., 0., 1.]).reshape(1,-1), 
                            cmap.colors ], axis = 0)
    h = ax.contourf(std9_count.lon, std9_count.lat, std9_count[m,:,:], 
                    vmin = 0., vmax=upper_bound, 
                    levels = levels, colors = cmap, 
                    transform=ccrs.PlateCarree())
    cb = plt.colorbar(h, ax = ax, boundaries = levels, ticks = levels,
                      extend='both')

    ax.set_title('Month = ' + str(m))
fig.savefig(os.path.join(mg.path_intrim_out(), 'cru_qc', 
                         'std9_count_' + str(year[0]) + '-' + str(year[-1]) \
                         + '.png'), dpi = 600.)

data.close()
