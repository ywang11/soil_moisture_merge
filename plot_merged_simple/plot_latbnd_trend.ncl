load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"

;----------------------------------------------------------
;Draw the trend of soil moisture by latitude by model.
;----------------------------------------------------------

begin
  model = (/"CERA20C", "CFSR", "CLASS-CTEM-N", "CLM4", \
            "CLM4VIC", "CLM5.0", "ERA20C", "ERA-Interim", "ESA-CCI", \
            "GLDAS_CLM", "GLDAS_Noah2.0", "GLDAS_VIC", "GTEC", \
            "ISAM", "JSBACH", "JULES", "LPX-Bern", "ORCHIDEE", \
            "ORCHIDEE-CNP"/)
  depth = (/"0-10cm", "10-30cm", "30-50cm", "50-100cm"/)
  lat_bnds = (/ (/-60., -40./), (/-40., -20./), (/-20., 0./), \
                (/0., 20./), (/20., 40./), (/40., 60./) /)
  lat_bnd_names = (/"60S-40S", "40S-20S", "20S-0", "0-20N", \
                    "20N-40N", "40N-60N"/)

  ;----------------------------------------------------------------
  ; Global land mask
  ;----------------------------------------------------------------
  f = addfile("$SCRATCHDIR/Soil_Moisture/data/Global_Masks/" + \
              "elm_half_degree_grid_negLon.nc", "r")
  area = f->area
  landfrac = f->landfrac
  delete(f)

  ;----------------------------------------------------------------
  ; Obtain latitude trends and anomalies
  ;----------------------------------------------------------------
  do d = 0,dimsizes(depth)-1
    global_trend = new((/dimsizes(model),dimsizes(lat_bnd_names)/), double)
    global_trend@_FillValue = default_fillvalue("double")
    global_trend(:, :) = global_trend@_FillValue

    do m = 0,dimsizes(model)-1
      print(depth(d) + " " + model(m))

      flist = systemfunc("ls $SCRATCHDIR/Soil_Moisture/" + \
                         "intermediate/Interp_Merge/" + \
                         model(m) + "/" + model(m) + "_*_" + \
                         depth(d) + ".nc")
      if .not. ismissing(flist(0)) then
        f = addfiles(flist, "r")
        sm = f[:]->sm

        ntime = dimsizes(sm&time)
        do b = 0,dimsizes(lat_bnd_names)-1
          sm_lat_bnd = new(ntime, double)
          do t = 0,ntime-1
            ;;print(dimsizes(sm))
            ;;print(t)
            ;;print(lat_bnds(b,0))
            ;;print(lat_bnds(b,1))

            ;;printVarSummary(sm)
            ;;printVarSummary(area)
            ;;printVarSummary(landfrac)
            ;;printVarSummary(sm_lat_bnd)

            sm_lat_bnd(t) = \
                sum( sm(t,{lat_bnds(b,0):lat_bnds(b,1)},:) * \
                     area({lat_bnds(b,0):lat_bnds(b,1)},:) * \
                 landfrac({lat_bnds(b,0):lat_bnds(b,1)},:) ) / \
                sum( area({lat_bnds(b,0):lat_bnds(b,1)},:) * \
                 landfrac({lat_bnds(b,0):lat_bnds(b,1)},:) )
          end do
          ; Calculate the trend
          rc = regline(ispan(1,ntime,1), sm_lat_bnd)
          global_trend(m,b) = (/rc/)
          delete(t)
          delete(rc)
          delete(sm_lat_bnd)
        end do
        delete(ntime)
        delete(f)
        delete(sm)
      end if
      delete(flist)
    end do

    ;------------------------------------------------------------
    ; Draw the global trend
    ;------------------------------------------------------------
    res = True
    res@gsnMaximize = True
    res@vpWidthF = 0.8
    res@vpHeightF = 0.8

    res@tmYUseLeft           = True    ; Make right axis independent of left
    res@tmYLOn               = True    ; Turn on left tickmarks
    res@tmYROn               = False   ; Turn off right tickmarks
    res@tmXTOn               = False   ; Turn off top tickmarks
    res@tmYLLabelsOn         = True    ; Turn off left labels
    res@tmYRLabelsOn         = False   ; Turn off right labels
    res@tmYRMinorOn          = False   ; Turn off minor ticks on Y axis
    res@tmXBMode             = "Explicit" ; use tmXTValues to set tick marks
    res@tmXBValues           = ispan(1,dimsizes(lat_bnd_names),1)
    res@tmXBLabels           = lat_bnd_names

    res@gsnCenterStringFontHeightF = 0.02   ; Increase font height
    res@gsnCenterString      = "Soil Moisture Trend (m3/m3 per decade)"

    ; plot options for the trendline setup
    res@xyMarkLineModes     = "Lines"
    res@xyMarkers           = 16  ; choose type of marker
    res@xyMarkerSizeF       = 0.0075  ; Marker size (default 0.01)

    res@xyDashPatterns      = (/0,0,0,0, 1,1,1,1, 2,2,2,2, 3,3,3,3, \
                                4,4,4,4/)
    res@xyLineThicknesses   = 2
    res@xyLineColors        = (/"brown3","purple2","royalblue2","darkgreen", \
                                "brown3","purple2","royalblue2","darkgreen", \
                                "brown3","purple2","royalblue2","darkgreen", \
                                "brown3","purple2","royalblue2","darkgreen", \
                                "brown3","purple2","royalblue2","darkgreen"/)

    ;---Make legend smaller and move into plot
    res@pmLegendDisplayMode    = "Always"            ; turn on legend
    res@pmLegendSide           = "Right"             ; Change location of
    res@pmLegendParallelPosF   = .5                 ; move units up
    res@pmLegendOrthogonalPosF = .02                 ; move units right
    res@pmLegendWidthF         = 0.175               ; Change width and
    res@pmLegendHeightF        = 0.7                 ; height of legend.
    res@lgPerimOn              = True                ; turn off/on box around
    res@lgLabelFontHeightF     = .015                ; label font height
    res@xyExplicitLegendLabels = model

    wks = gsn_open_wks("png", "$SCRATCHDIR/Soil_Moisture/" + \
                       "intermediate/plot_interp_merged_simple/" + \
                       "annual_latbnd_trend_all_models_" + depth(d))
    ;;res@trYMinF             = -.0002
    ;;res@trYMaxF             =  .0002
    plot = gsn_csm_xy(wks, ispan(1,dimsizes(lat_bnd_names),1), \
                      global_trend * 10, res)
    delete(plot)
    delete(wks)

    delete(res)
    delete(global_trend)
  end do

  delete(area)
  delete(landfrac)
  delete(model)
  delete(depth)
  delete(lat_bnds)
  delete(lat_bnd_names)

end