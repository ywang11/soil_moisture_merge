# -*- coding: utf-8 -*-
"""
Created on Wed Apr 17 00:30:33 2019

@author: ywang254

Aggregate the ismn data to monthly time averages, and save to .csv file, with
  each column being one grid cell. No header.

The aggregated soil moisture data is the weighted average of any stations that
  has available data. Although over time, the weights and the available
  stations change, only a single final weight will be derived for the LSM data,
  so that the temporal non-homogeneity is not transferred. 

simple:
  if True, simply average the soil moisture from all stations within a grid.
  if False, weight-average the soil moisture of the stations within a grid
    by the relative percentages of their land covers. See the dominance_lc 
    option below.

dominance_lc: 
  if True, a grid cell is used in weighting only if the total percentage of
    land covers that match any of the stations exceed the dominance_threshold.
  if False, a grid cell is used in weighting regardless of the land cover of
    its stations, but the stations are still weighted-averaged by the
    relative percentages of their land covers.


"""
import utils_management as mg
from utils_management.constants import depth, year_longest
from misc.ismn_get_monthly import ismn_get_monthly
from misc.ismn_utils import get_ismn_aggr_method
import os
import pandas as pd
import numpy as np
import time


i = REPLACE1 # [0, 1, 2]
simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = get_ismn_aggr_method(simple[i], dominance_lc[i], 
                                        dominance_threshold)

start = time.time()

# Select the time range to recast the observed soil moisture data to
time_range = pd.date_range(start=str(year_longest[0])+'-01-01',
                           end=str(year_longest[-1])+'-12-31', freq='MS',
                           tz = 'UTC')

##for key in depth:
key = depth[REPLACE2]

# ---- debug stuff
problematic_grids = []

path_lu = os.path.join(mg.path_intrim_out(), 'ismn_lu', key)

## DEBUG: set to only [:3]
grids = os.listdir(path_lu) ## [:3]

weighted_monthly_data = pd.DataFrame(data = np.nan, 
    index = ['Lat', 'Lon'] + list(time_range.strftime('%Y-%m-%d')),
    columns = [x.split('.')[0] for x in grids])

for gr in grids:
    # ---- debug stuff
    skip_grid = False

    ###################################################################
    # Calculate the weight for each ISMN station based on the land use 
    # fraction.
    ###################################################################
    # Load land cover information in MCD12C1 IGBP Classes 0-16.
    def parse_lu(path_lu, gr):
        f = open(os.path.join(path_lu, gr), 'r')
        # ---- grid lat lon
        grid_coords = [float(x) for x in f.readline().split(',')]
        f.readline() # remove 'Grid' label
        # ---- grid land cover percentages
        grid_lu = [float(n) for n in f.readline().split(',')]
        f.close()
        # ---- station land covers at this grid
        stations_lu = pd.read_csv(os.path.join(path_lu, gr),
                                  skiprows = 3)
        return grid_coords, grid_lu, stations_lu
    grid_coords, grid_lu, stations_lu = parse_lu(path_lu, gr)

    weighted_monthly_data.loc['Lat', gr.split('.')[0]] = grid_coords[0]
    weighted_monthly_data.loc['Lon', gr.split('.')[0]] = grid_coords[1]

    def calc_station_lu_weights(stations_lu, simple, dominance_lc):
        if simple:
            stations_lu_weights = np.ones(stations_lu.shape[0],
                                          dtype=float) / \
                                  stations_lu.shape[0]
        else:
            unique_lu = stations_lu['Land Cover'].unique()
            unique_lu_pc = np.zeros(len(unique_lu), dtype=float)
            for i,lu in enumerate(unique_lu):
                unique_lu_pc[i] = grid_lu[lu]

            if dominance_lc:
                if sum(unique_lu_pc) < dominance_threshold:
                    return None

            unique_lu_pc = unique_lu_pc / sum(unique_lu_pc)

            stations_lu_weights = np.zeros(len(stations_lu), 
                                           dtype=float)
            for i,lu in enumerate(unique_lu):
                stations_lu_weights[stations_lu['Land Cover'] == lu] \
                    = unique_lu_pc[i] / sum(stations_lu['Land Cover'] \
                                            == lu)
        return stations_lu_weights

    stations_lu_weights = calc_station_lu_weights(stations_lu, simple, 
                                                  dominance_lc)
    if type(stations_lu_weights) == type(None):
        continue

    ###################################################################
    # Read the soil moisture time series for each ISMN station and 
    # aggregate to monthly time scale. 
    ###################################################################
    for i,p in stations_lu.iterrows():
        try:
            monthly_data = ismn_get_monthly( \
                os.path.join(mg.path_data(),
                'ISMN2', p['Network']), p['Station'], p['Depth-Actual'],
                p['Sensor'], time_range)
        except:
            problematic_grids.append(gr)
            skip_grid = True
            break

        # The station has no good data. Why did it get in?            
        if type(monthly_data) == type(None):
            skip_grid = True
            break

        if i==0:
            stations_monthly_data = monthly_data.to_frame(name=str(i))
        else:
            stations_monthly_data = pd.concat([stations_monthly_data, 
                monthly_data.to_frame(name=str(i))], join='outer', 
                axis=1)

    if skip_grid:
        continue

    stations_monthly_data.dropna(axis=0, how='all', inplace=True)
    stations_monthly_data.index = \
        stations_monthly_data.index.strftime('%Y-%m-%d')

    ###################################################################
    # Apply weight for each time step separately.
    ###################################################################
    for i,p in stations_monthly_data.iterrows():
        weighted_monthly_data.loc[i, gr.split('.')[0]] = sum( \
            p.values * stations_lu_weights ) / sum( \
            stations_lu_weights[~np.isnan(p.values)] )


# ---- compress the data by remove the months without any observations
weighted_monthly_data.dropna(axis=0, how='all', inplace=True)


fout = os.path.join(mg.path_intrim_out(), 'ismn_aggr', 
                    'weighted_monthly_data_' + ismn_aggr_method + \
                    '_' + key + '.csv')
weighted_monthly_data.to_csv(fout, index=True)


#######################################################################
#Use the first 8 years for validation.
#######################################################################
##cal = weighted_monthly_data.loc[weighted_monthly.index.year <= 2003, :]
##cal.to_csv(os.path.join(mg.path_intrim_out(), 'ismn_aggr', 
##                        'cal_weighted_monthly_data_' + \
##                        ismn_aggr_method + '_' + key + '.csv'))
##val = weighted_monthly_data.loc[weighted_monthly.index.year <= 2003, :]
##val.to_csv(os.path.join(mg.path_intrim_out(), 'ismn_aggr', 
##                        'val_weighted_monthly_data_' + \
##                        ismn_aggr_method + '_' + key + '.csv'))


if problematic_grids:
    print('!!! Attention at depth ' + key + 'm:')
    print(problematic_grids)


end = time.time()


# Time in hours.
# 20190509 benchmark on laptop:
# simple=True takes 11.2053 hours.
# dominance_lc=False takes  hours
# dominance_lc=True with dominance_threshold=40 takes slightly > 10 hours.
print('The script finished in ' + ('%.4f' % ((end-start)/3600) ) + ' hours.')
