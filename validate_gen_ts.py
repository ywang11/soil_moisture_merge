"""
2020/08/04
ywang254@utk.edu

Create the SREX time series of the soil moisture products, and the validation
 datasets.

2021/05/26
ywang254@utk.edu

Add the SREX time series for the source datasets.
"""
import regionmask as rmk
import matplotlib.pyplot as plt
import os
import pandas as pd
import xarray as xr
import numpy as np
import utils_management as mg
from utils_management.constants import depth, depth_cm, val_list, val_depth, \
    val2_list, val_depth, lsm_all, cmip5
from misc.analyze_utils import decode_month_since
from misc.cmip6_utils import *
from time import time


# Grid area
hr = xr.open_dataset(os.path.join(mg.path_data(), 'Global_Masks',
                                    'elm_half_degree_grid_negLon.nc'))
area = hr.area.copy(deep = True)
hr.close()


#
srex_mask = rmk.defined_regions.srex.mask(area)
def avg_srex(sm, which):
    """Average within SREX regions (all are over land)"""
    sm_srex = pd.DataFrame(data = np.nan,
                           index = sm['time'].to_index(),
                           columns = [str(sid) for sid in which])
    for sid in which:
        keep = (np.abs(srex_mask.values - sid) < 1e-6) & \
            (~np.isnan(sm.values[0,:,:]))
        sm_r = (sm.where(keep) * area.where(keep)).sum(dim = ['lat','lon']) \
            / np.sum(area.where(keep))
        sm_srex.loc[:, str(sid)] = sm_r.values
    return sm_srex


# Products: Global SREX regions
prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all']
prod_srex_all = pd.DataFrame(data = np.nan,
                             index = pd.date_range('1970-01-01', '2016-12-31',
                                                   freq = 'MS'),
                             columns = pd.MultiIndex.from_product([ \
    prod_list, depth_cm, ['Global'] + [str(sid) for sid in range(1,27)]]))
for prod in prod_list:
    for d, dcm in zip(depth, depth_cm):
        path_prod = mg.path_to_final_prod(prod, d, dcm)
        hr = xr.open_dataset(path_prod, decode_times = False)
        if 'em' in prod:
            sm = hr['predicted'].copy(deep = True)
        else:
            sm = hr['sm'].copy(deep = True)
        sm['time'] = pd.date_range('1950-01-01', '2016-12-31', freq = 'MS')
        sm = sm[sm['time'].to_index().year >= 1970, :, :]
        hr.close()
        prod_srex_all.loc[:, (prod, dcm, 'Global')] = \
            (sm * area).sum(dim = ['lat','lon']) / np.sum(area)
        temp = avg_srex(sm, range(1,27))
        for sid in range(1,27):
            prod_srex_all.loc[:, (prod, dcm, str(sid))] = temp.loc[:, str(sid)]
prod_srex_all.to_csv(os.path.join(mg.path_out(), 'validate',
                                  'prod_srex_all.csv'))


# Source datasets
cmip6_list = list(set(mrsol_availability('0-10cm', 'vanilla', 'historical')) &\
                  set(mrsol_availability('0-10cm', 'vanilla', 'ssp585')))
cmip6_list = sorted([x for x in cmip6_list if 'r1i1p1f1' in x])


prod_source_all = pd.DataFrame(data = np.nan,
                               index = pd.date_range('1970-01-01', '2016-12-31',
                                                     freq = 'MS'),
                               columns = pd.MultiIndex.from_product([lsm_all+cmip5+cmip6_list,
        depth_cm, ['Global'] + [str(sid) for sid in range(1,27)]]))
for d, dcm in zip(depth, depth_cm):
    for source, source_list in zip(['ORS', 'CMIP5', 'CMIP6'],
                                   [lsm_all, cmip5, cmip6_list]):
        for nn in source_list:
            start = time()

            if source == 'ORS':
                flist = sorted(glob(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                                 'vanilla', nn,
                                                 nn + '_*_' + dcm + '.nc')))
                ylist = [int(yy.split('/')[-1].split('_')[-2]) for yy in flist]
            elif source == 'CMIP5':
                flist = sorted(glob(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                                 'vanilla', source, nn,
                                                 'sm_*_' + dcm + '.nc')))
                ylist = [int(yy.split('/')[-1].split('_')[-2]) for yy in flist]
            elif source == 'CMIP6':
                flist = sorted(glob(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                                 'vanilla', source, nn,
                                                 'mrsol_*_' + dcm + '.nc')))
                ylist = [int(yy.split('/')[-1].split('_')[-2]) for yy in flist]

            if len(flist) == 0:
                continue

            hr = xr.open_mfdataset(flist, decode_times = False)
            sm = hr['sm'].copy(deep = True)
            sm['time'] = pd.date_range(str(ylist[0]) + '-01-01',
                                       str(ylist[-1]) + '-12-31', freq = 'MS')
            sm = sm[(sm['time'].to_index().year >= 1970) & \
                    (sm['time'].to_index().year <= 2016), :, :]
            hr.close()

            prod_source_all.loc[sm['time'].to_index(), (nn, dcm, 'Global')] = \
                ((sm * area).sum(dim = ['lat','lon']) / np.sum(area)).values
            temp = avg_srex(sm, range(1,27))
            for sid in range(1,27):
                prod_source_all.loc[sm['time'].to_index(),
                                    (nn, dcm, str(sid))] = temp.loc[:, str(sid)]

            print(time() - start)
prod_source_all.to_csv(os.path.join(mg.path_out(), 'validate',
                                    'source_srex_all.csv'))
