# -*- coding: utf-8 -*-
"""
Created on Fri Apr 26 20:24:05 2019

@author: ywang254

Average the vanilla land cover over time.
"""
import time
import glob
import os
from pyhdf.SD import SD, SDC
import pprint
import xarray as xr
import numpy as np
import collections as co
import warnings

import utils_management as mg

start = time.time()

for count, yy in enumerate(range(2001, 2017)):    
    file_name = glob.glob(os.path.join(mg.path_data(), 'LULC', 'mcd12c1', 
                          'MCD12C1.A'+str(yy)+'001.006.*.hdf'))[0]

    file = SD(file_name, SDC.READ)

    print(file.info())
    
    
    # Print the names of datasets in the file
    datasets_dic = file.datasets()

    for idx,sds in enumerate(datasets_dic.keys()):
        print(idx,sds)
    
    
    # Get a specific data
    sds_obj = file.select('Land_Cover_Type_1_Percent')
    
    data = sds_obj.get()
    print(data)
    
    # Get the SDS attributes
    pprint.pprint( sds_obj.attributes() )
    # =============================================================================
    # outcome of the above: 
    # {'Layer 0': 'water',
    #  'Layer 1': 'evergreen needleleaf forest',
    #  'Layer 10': 'grasslands',
    #  'Layer 11': 'permanent wetlands',
    #  'Layer 12': 'croplands',
    #  'Layer 13': 'urban and built-up',
    #  'Layer 14': 'cropland/natural vegetation mosaic',
    #  'Layer 15': 'snow and ice',
    #  'Layer 16': 'barren or sparsely vegetated',
    #  'Layer 2': 'evergreen broadleaf forest',
    #  'Layer 3': 'deciduous needleleaf forest',
    #  'Layer 4': 'deciduous broadleaf forest',
    #  'Layer 5': 'mixed forests',
    #  'Layer 6': 'closed shrubland',
    #  'Layer 7': 'open shrublands',
    #  'Layer 8': 'woody savannas',
    #  'Layer 9': 'savannas',
    #  '_FillValue': 255,
    #  'long_name': 'Land_Cover_Type_1_Percent',
    #  'units': 'percent in integers',
    #  'valid_range': [0, 100]}
    # =============================================================================
    
    # Convert 255 to NaN
    data = data.astype(np.float64)
    data[data == 255.] = np.nan
    
    # Re-order latitude S->N
    data = data[::-1, :, :]

    # Create xarray dataset
    # (convert to int because float causes problem in accurate indexing)
    lat_int = np.array(range(-89975, 89976, 50))
    lon_int = np.array(range(-179975, 179976, 50))
    xr_data = xr.DataArray(data = data, dims = ('lat', 'lon', 'class'), 
                           coords = {'lat': lat_int, 'lon': lon_int,
                                     'class': np.array(range(0,17))}, 
                           attrs = co.OrderedDict({'src': 'vanilla MCD12C1 IGBP Classification'}))
    ##xr_data = xr_data.where(xr_data == 255, np.nan)

    # Average over time and output to NetCDF
    if count == 0:
        xr_data_avg = xr_data.copy(deep=True)
    else:
        xr_data_avg = xr_data_avg + xr_data

    file.end()

xr_data_avg = xr_data_avg / count

xr_data_avg.to_netcdf(path = os.path.join(mg.path_intrim_out(), 
                                          'lu_aggr', 'mcd12c1_0.05.nc'))


# Dominant land cover type in each pixel
xr_data_maj = xr.DataArray(data = np.argmax(xr_data_avg.values, axis=2), 
                           dims = ('lat', 'lon'),
                           coords = {'lat': np.arange(-89.975, 89.976, 0.05),
                                     'lon': np.arange(-179.975, 179.976, 0.05)},
                           attrs = co.OrderedDict({'src': 'vanilla MCD12C1 IGBP Classification'}))
xr_data_maj.to_netcdf(path = os.path.join(mg.path_intrim_out(), 
                                          'lu_aggr', 'mcd12c1_0.05_major.nc'))


# Aggregate to 0.5^o
xr_data_aggr = xr.DataArray(data = np.empty([360, 720, 17]), 
                            dims = ('lat', 'lon', 'class'), 
                            coords = {'lat': np.arange(-89.75, 89.76, 0.5), 
                                      'lon': np.arange(-179.75, 179.76, 0.5),
                                      'class': np.array(range(0,17))}, 
                            attrs = co.OrderedDict({'src': 'vanilla MCD12C1 IGBP Classification'}))

debug = False
if debug:
    warnings.filterwarnings("error")

for x in xr_data_aggr.lat:
    for y in xr_data_aggr.lon:
        xr_data_aggr.loc[x, y, :] = np.nanmean( np.nanmean( \
            xr_data.loc[ np.array(range(int(1000*(x-0.25)),
                                        int(1000*(x+0.25)), 50)) + 25, 
                         np.array(range(int(1000*(y-0.25)), 
                                        int(1000*(y+0.25)), 50)) + 25, : ],
            axis=0), axis=0 )

warnings.filterwarnings("default")

xr_data_aggr.to_netcdf(path = os.path.join(mg.path_intrim_out(), 
                                           'lu_aggr', 'mcd12c1_0.5.nc'))

end = time.time()

print(end - start)
