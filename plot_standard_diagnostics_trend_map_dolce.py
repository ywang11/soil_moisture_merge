"""
Draw the global soil moisture trend map of the DOLCE-weighted results at the
interpolated 0.5 degrees resolution.
"""
import os
import pandas as pd
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth
import utils_management as mg
import xarray as xr
import numpy as np
from misc.ismn_utils import get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
from misc.plot_standard_diagnostics_utils import plot_trend_map
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import itertools as it
import multiprocessing as mp
from misc.plot_utils import cmap_gen


simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = [get_ismn_aggr_method(simple[iam],
                                         dominance_lc[iam],
                                         dominance_threshold) for iam \
                    in range(len(simple))]

cov_method = [get_cov_method(cov) for cov in range(5)]

# 'lsm', 'cmip5', 'cmip6'
model_set = ['2cmip']


# DEBUG
# for model, ismn, cov in it.product(model_set, ismn_aggr_method, cov_method):
def plotter(option):
    model, ismn, cov = option

    if model == 'lsm':
        year_list = [year_longest, year_shorter, year_shorter2, year_shortest]
    else:
        year_list = [year_longest]

    for year in year_list:
        prefix_in = os.path.join(mg.path_out(), 
                                 'standard_diagnostics_dolce_' + model,
                                 ismn + '_' + cov + '_weighted_average_' + \
                                 str(year[0]) + '-' + str(year[-1]))

        prefix_out = os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                                  'trend_map_dolce_' + model, 
                                  ismn + '_' + cov + '_' + str(year[0]) + \
                                  '-' + str(year[-1]))

        levels = np.linspace(-0.002, 0.002, 21)
        cmap = cmap_gen('autumn', 'winter_r')

        plot_trend_map(depth, prefix_in, prefix_out, levels, cmap)

pool = mp.Pool(min(6, mp.cpu_count()-1))
pool.map_async(plotter, list(it.product(model_set, ismn_aggr_method,
                                        cov_method)))
pool.close()
pool.join()
