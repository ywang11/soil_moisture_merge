"""
20190920
ywang254@utk.edu

Rescale the 1950-2010 and 1981-2010 DOLCE-LSM datasets to the dynamic range
 of the 1981-2016 dataset during their overlapping periods.

Method: linear CDF matching
  Liu et al. 2011 Trend-Preserving Blending of Passive and Active Microwave Soil Moisture Retrievals. DOI: 10.5194/hess-15-425-2011
  Liu et al. 2012 Trend-Preserving Blending of Passive and Active Microwave Soil Moisture Retrievals. DOI: 10.1016/j.rse.2012.03.014
"""
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest
