"""
20190923

ywang254@utk.edu

Compare the standard metrics between the un-merged 1950-2016 results, 
 the merged results, and the concatenated un-merged results.
"""
import os
import sys
import pandas as pd
import numpy as np
from utils_management.constants import depth, year_longest, year_shorter, \
    year_shorter2, year_shortest
import utils_management as mg
import matplotlib.pyplot as plt
from misc.ismn_utils import get_ismn_aggr_method
import itertools as it

# MODIFY
USonly = True
if USonly:
    suffix = '_USonly'
else:
    suffix = ''


year = year_longest
#met_obs = 'CRU_v4.03'
em = 'year_month_anomaly_9grid'


# Separate plot for each metric.
for metric in ['RMSE', 'Bias', 'uRMSE', 'Corr']:
    fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(10,10),
                             sharex=True, sharey=True)
    for d_ind, d in enumerate(depth):
        ax = axes.flat[d_ind]

        original = pd.read_csv(os.path.join(mg.path_out(),
                                            'standard_metrics',
                                            'em_lsm_' + d + '_' + \
                                            str(year[0]) + '-' + \
                                            str(year[-1]) + \
                                            suffix + '.csv'),
                               header = [0,1], index_col = [0,1] \
        ).loc[(slice(None), em),
              (metric, slice(None))].values.reshape(-1)

        concat = pd.read_csv(os.path.join(mg.path_out(),
                                          'standard_metrics',
                                          'concat_no_merge_em_lsm_' + \
                                          d + '_' + str(year[0]) + \
                                          '-' + str(year[-1]) + \
                                          suffix + '.csv'),
                             header = [0,1], index_col = 0 \
        ).loc[:, # (slice(None), em, pr)
              (metric, slice(None))].values.reshape(-1)

        merge = pd.read_csv(os.path.join(mg.path_out(), 
                                         'standard_metrics', 
                                         'concat_em_lsm_' + d + '_' + \
                                         str(year[0]) + '-' + \
                                         str(year[-1]) + suffix + '.csv'),
                            header = [0,1], index_col = 0 \
        ).loc[:, # (slice(None), em, pr)
              (metric, slice(None))].values.reshape(-1)

        h1 = ax.boxplot(original, ##positions = [em_ind - (pr_ind-1)/3],
                        positions = [0],
                        patch_artist = True)
        h2 = ax.boxplot(concat, ##positions = [em_ind],
                        positions = [1],
                        patch_artist = True)
        h3 = ax.boxplot(merge, ##positions = [em_ind + (pr_ind-1)/3],
                        positions = [2],
                        patch_artist = True)

        for element in ['boxes', 'whiskers', 'fliers', 'means',
                        'caps']:
            plt.setp(h1[element], color='r')
            plt.setp(h2[element], color='g')
            plt.setp(h3[element], color='b')
        h1['boxes'][0].set(facecolor = [0,0,0,0])
        h2['boxes'][0].set(facecolor = [0,0,0,0])
        h3['boxes'][0].set(facecolor = [0,0,0,0])

        ax.set_xticks(range(3))
        ax.set_xticklabels(['Original', 'Concat', 'Merged']) # rotation = 30.
        ax.set_title('Depth: ' + d)
        ax.set_ylabel(metric)

    #ax.legend([h1['boxes'][0], h2['boxes'][0], h3['boxes'][0]], 
    #          ['Original', 'Concat', 'Merged'],
    #          loc = 'lower center', bbox_to_anchor = (-1., -0.5))

    fig.savefig(os.path.join(mg.path_out(), 'standard_metrics',
                             'plot_compare_concat_em_lsm_' + metric + \
                             suffix + '.png'), dpi = 600., bbox_inches='tight')
    plt.close(fig)
