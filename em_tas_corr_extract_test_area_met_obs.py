# -*- coding: utf-8 -*-
"""
Created on Thu May 30 16:25:28 2019

@author: ywang254

Extract the soil moisture and constraints data for the following
 bounding box (middle-US): [-100, -90, 33, 43]
"""
import utils_management as mg
from utils_management.constants import met_to_lsm, year_longest, \
    lsm_list, depth, depth_cm, target_lat, target_lon
from misc.spatial_utils import extract_grid
import os
import xarray as xr
import pandas as pd
import numpy as np
import itertools as it
import time
import multiprocessing as mp


land_mask = 'vanilla'
bbox = [-100, -90, 33, 43]
grid = ['grid' + str(x) + str(y) for x in range(1,21) for y in range(1,21)]


# Target period of interest.
year = year_longest
year_str = str(year[0]) + '-' + str(year[-1])
period = pd.date_range(str(year[0])+'-01-01', str(year[-1])+'-12-31',
                       freq = 'MS')


# Prefix of the observed precipitation and temperature data.
met_obs = 'CRU_v4.03'
pr_path = mg.path_to_pr(land_mask)['CRU_v4.03']
tas_path = mg.path_to_tas(land_mask)['CRU_v4.03']


# Precipitation.
pr = pd.DataFrame(data = np.nan, index = period, columns = grid)
flist = [pr_path + str(y) + '.nc' for y in year]
data = xr.open_mfdataset(flist, decode_times = False)
pr.loc[:, :] = data.pr.values[:, 
                              (data.lat.values >= bbox[2]) & \
                              (data.lat.values <= bbox[3]), 
                              :][:, :,
                                 (target_lon >= bbox[0]) & \
                                 (target_lon <= bbox[1]) \
                              ].reshape(-1, len(grid))
data.close()
pr.to_csv(os.path.join(mg.path_intrim_out(), 
                       'em_tas_corr_extract_test_area', 
                       'pr_CRU_v4.03.csv'))

# Temperature.
tas = pd.DataFrame(data = np.nan, index = period, columns = grid)
flist = [tas_path + str(y) + '.nc' for y in year]
data = xr.open_mfdataset(flist, decode_times = False)
tas.loc[:, :] = data.tas.values[:, 
                                (data.lat.values >= bbox[2]) & \
                                (data.lat.values <= bbox[3]), 
                                :][:, :,
                                   (target_lon >= bbox[0]) & \
                                   (target_lon <= bbox[1]) \
                                ].reshape(-1, len(grid))
data.close()
tas.to_csv(os.path.join(mg.path_intrim_out(), 
                        'em_tas_corr_extract_test_area', 
                        'tas_CRU_v4.03.csv'))
