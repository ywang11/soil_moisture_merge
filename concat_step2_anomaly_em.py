"""
20190923
ywang254@utk.edu

Step 2 of the CDF-matching merging:
 Caculate the anomalies from the monthly climatology (1981-2010) of the 
 DOLCE-LSM products of different time periods.
"""
import xarray as xr
import os
import pandas as pd
import numpy as np
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth_cm, target_lat, target_lon
import utils_management as mg
from misc.ismn_utils import get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
import itertools as it
import multiprocessing as mp


met_obs = 'CRU_v4.03'
em = 'year_month_anomaly_9grid' # Need to modify the read script if not
                                # this method
prefix = ['predicted'] # , 'predicted_CI_lower', 'predicted_CI_upper']


args_list = []
for i, fx in it.product(range(len(depth_cm)), prefix):
    args_list.append(['lsm', i, fx])
    args_list.append(['all', i, fx])


def anomaly(args):
    model_set, i, fx = args

    dcm = depth_cm[i]

    for year in [year_shorter, year_shorter2, year_shortest]:
        year_str = str(year[0]) + '-' + str(year[-1])

        data = xr.open_mfdataset([os.path.join(mg.path_out(),
                                               'em_' + model_set + '_corr',
                                               met_obs + '_' + \
                                               em.replace('anomaly',
                                                          'restored') + \
                                               '_' + dcm + '_' + year_str,
                                               fx + '_' + str(yr) + \
                                               '.nc') for yr in year],
                                 decode_times = False, concat_dim = 'time')

        data['time'] = pd.date_range(str(year[0]) + '-01-01',
                                     str(year[-1]) + '-12-31', freq = 'MS')

        data[fx].load()

        data2 = xr.open_dataset(os.path.join(mg.path_out(), 
                                             'concat_em_' + model_set,
                                             'climatology_' + met_obs + '_' + \
                                             em + '_' + dcm + '_' + fx + \
                                             '_' + year_str + '.nc'),
                                decode_times = False)
        data2[fx].load()

        for m in range(12):
            data[fx].loc[data.time.indexes['time'].month == (m + 1),
                         :, :] -= data2[fx].values[m, :, :]

        data2.close()
        data.to_netcdf(os.path.join(mg.path_out(), 'concat_em_' + model_set,
                                    'anomaly_' + met_obs + '_' + em + '_' + \
                                    dcm + '_' + fx + '_' + year_str + '.nc'))
        data.close()


pool = mp.Pool(4)
pool.map_async(anomaly, args_list)
pool.close()
pool.join()
