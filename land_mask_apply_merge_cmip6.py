"""
2019/07/14

ywang254@utk.edu

Apply the common land mask on the interpolated, unmasked, after their
interpolation to 0.5 deg.

Need to run "land_mask_minimum_cmip6.py" 
            "land_mask_minimum_cmip5.py" "land_mask_minimum_lsm.py"
            "land_mask_minimum_merge.py" first. The "_merge" is the final
            land mask.
"""
import sys
import os
import xarray as xr
import numpy as np
import pandas as pd
from glob import glob
import utils_management as mg
from utils_management.constants import year_cmip6, depth_cm
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import multiprocessing as mp
import itertools as it


land_mask = 'vanilla' # 'e1m', 'MODIS', 'vanilla'


###############################################################################
# Read the merged land mask.
###############################################################################
data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'land_mask_minimum', 
                                    'merge_' + land_mask + '.nc'))
mask = data.mask.values.copy()
data.close()


###############################################################################
# Apply on the CMIP6 models.
###############################################################################
#expr = 'ssp585' # 'historical', 'ssp585'
#var = 'pr' # 'mrsol', 'mrsos', 'pr', 'tas'

for expr, var in it.product(['historical', 'ssp585'], ['pr', 'tas', 'mrsol']):
    if var == 'mrsol':
        model_version = np.unique(mrsol_availability(depth_cm[0], 'None', 
                                                     expr) + \
                                  mrsol_availability(depth_cm[1], 'None', 
                                                     expr) + \
                                  mrsol_availability(depth_cm[2], 'None',
                                                     expr) + \
                                  mrsol_availability(depth_cm[3], 'None',
                                                     expr))
    else:
        model_version = one_layer_availability(var, 'None', expr)
    
    ## Debug: check file existence
    ##for m_v in model_version:
    ##    file_list = glob(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
    ##                                  'None', 'CMIP6', m_v, 
    ##                                  var + '_' + expr + '*.nc'))
    ##    print(file_list[0])
    
    def apply_mask(m_v):
        file_list = glob(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                      'None', 'CMIP6', m_v, 
                                      var + '_' + expr + '*.nc'))
        print(file_list[0])
        for f in file_list:
            data = xr.open_dataset(f, decode_times = False)
            data2 = data.where(mask)
            data2.to_netcdf(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                         land_mask, 'CMIP6', m_v, 
                                         f.split('/')[-1]))
            data.close()
        return None
    
    pool = mp.Pool(mp.cpu_count() - 1)
    pool.map(apply_mask, model_version)
    pool.close()
    pool.join()
