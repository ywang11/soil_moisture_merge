"""
2019/09/06
ywang254@utk.edu

Check the model and ensemble members in the "data" folder and the 
  "intermediate" folder.

// Print to .csv
// Nominal resolution uses the average difference between latitudes/longitudes

Project, Experiment, Model, Nominal resolution, Variable, Depth, -
     Variant in "data", -
     Variant in "Interp_Merge/None", Variant in "Interp_Merge/vanilla"
"CMIP5", historical, , , "mrsol", "0-10cm", , 
"CMIP6", ssp585/rcp85, , , "mrsol", "10-30cm", ,
                           "mrsol", "30-50cm", ,
                           "mrsol", "50-100cm", ,
                           "pr", , ,
                           "tas", , ,
"""
import os
import pandas as pd
import numpy as np
import utils_management as mg
from utils_management.constants import depth_cm
from glob import glob
import xarray as xr


data = pd.DataFrame(data = np.nan, index = range(24),
                    columns = ['Project', 'Experiment', 'Model',
                               'Nominal resolution (lat)',
                               'Nominal resolution (lon)', 'Variable',
                               'Variant in .data.', 'Depth', 
                               'Variant in .intermediate.Interp_Merge.None',
                               'Variant in .intermediate.Interp_Merge.vanilla'])


count = 0
###############################################################################
# CMIP5
###############################################################################
proj = 'CMIP5'
########
# mrsol
########
var = 'mrlsl'
for expr in ['historical', 'rcp85']:
    flist0 = glob(os.path.join(mg.path_data(), proj, expr, var, 
                               var + '*' + expr + '_r1i1p1_*.nc'))
    model_list = list(np.unique([x.split('_')[-4] for x in flist0]))

    print(model_list)

    for m in model_list:
        flist1 = glob(os.path.join(mg.path_data(), proj, expr, var,
                                   var + '_Lmon_' + m + '_' + expr + \
                                   '_r1i1p1_*.nc'))

        if len(flist1) > 0:
            for dcm in depth_cm:
                data.loc[count, 'Project'] = proj
                data.loc[count, 'Experiment'] = expr
                data.loc[count, 'Model'] = m
    
                r = xr.open_dataset(flist1[0])
                data.loc[count, 'Nominal resolution (lat)'] = \
                    np.mean(r.lat.values[1:] - r.lat.values[:-1])
                data.loc[count, 'Nominal resolution (lon)'] = \
                    np.mean(r.lon.values[1:] - r.lon.values[:-1])
                r.close()
    
                data.loc[count, 'Variable'] = var
                data.loc[count, 'Variant in .data.'] = 'r1i1p1'

                data.loc[count, 'Depth'] = dcm

                for land_mask in ['None', 'vanilla']:
                    flist2 = glob(os.path.join(mg.path_intrim_out(), 
                                               'Interp_Merge', land_mask, proj,
                                               m, 'sm_' + expr + \
                                               '_r1i1p1_*_' + dcm + '.nc'))
                    if len(flist2) > 0:
                        data.loc[count, 'Variant in .intermediate.' + \
                                 'Interp_Merge.' + land_mask] = 'r1i1p1'
                count += 1
########
# pr, tas
########
for var in ['pr', 'tas']:
    for expr in ['historical', 'rcp85']:
        # (only check the models relevant to mrlsl)
        flist0 = glob(os.path.join(mg.path_data(), proj, expr, 'mrlsl', 
                                   'mrlsl*' + expr + '_r1i1p1_*.nc'))
        model_list = list(np.unique([x.split('_')[-4] for x in flist0]))

        for m in model_list:
            flist1 = glob(os.path.join(mg.path_data(), proj, expr, var,
                                       var + '_Amon_' + m + '_' + expr + \
                                       '_r1i1p1_*.nc'))

            if len(flist1) > 0:
                data.loc[count, 'Project'] = proj
                data.loc[count, 'Experiment'] = expr
                data.loc[count, 'Model'] = m

                r = xr.open_dataset(flist1[0])
                data.loc[count, 'Nominal resolution (lat)'] = \
                    np.mean(r.lat.values[1:] - r.lat.values[:-1])
                data.loc[count, 'Nominal resolution (lon)'] = \
                    np.mean(r.lon.values[1:] - r.lon.values[:-1])
                r.close()

                data.loc[count, 'Variable'] = var
                data.loc[count, 'Variant in .data.'] = 'r1i1p1'

                for land_mask in ['None', 'vanilla']:
                    flist2 = glob(os.path.join(mg.path_intrim_out(), 
                                               'Interp_Merge', land_mask, proj,
                                               m, var + '_' + expr + \
                                               '_r1i1p1_*.nc'))
                    if len(flist2) > 0:
                        data.loc[count, 'Variant in .intermediate.' + \
                                 'Interp_Merge.' + land_mask] = 'r1i1p1'
                count += 1


###############################################################################
# CMIP6
###############################################################################
proj = 'CMIP6'
########
# mrsol
########
var = 'mrsol'
for expr in ['historical', 'ssp585']:
    prefix = os.path.join(mg.path_data(), proj, expr, var)
    model_list = [x for x in os.listdir(prefix) if \
                  os.path.isdir(os.path.join(prefix, x))]
    for m in model_list:
        flist1 = glob(os.path.join(mg.path_data(), proj, expr, var, m,
                                   var + '*' + m + '_' + expr + \
                                   '*r1i1p1f1*.nc'))

        if len(flist1) > 0:
            for dcm in depth_cm:
                data.loc[count, 'Project'] = proj
                data.loc[count, 'Experiment'] = expr
                data.loc[count, 'Model'] = m

                r = xr.open_dataset(flist1[0])
                data.loc[count, 'Nominal resolution (lat)'] = \
                    np.mean(r.lat.values[1:] - r.lat.values[:-1])
                data.loc[count, 'Nominal resolution (lon)'] = \
                    np.mean(r.lon.values[1:] - r.lon.values[:-1])
                r.close()

                data.loc[count, 'Variable'] = var

                list_variant = list(np.unique([x.split('_')[-3] \
                                               for x in flist1]))
                data.loc[count, 'Variant in .data.'] = '; '.join(list_variant)

                data.loc[count, 'Depth'] = dcm

                for land_mask in ['None', 'vanilla']:
                    list_variant2 = []
                    for v in list_variant:
                        flist2 = glob(os.path.join(mg.path_intrim_out(), 
                                                   'Interp_Merge', land_mask,
                                                   proj, m + '_' + v,
                                                   var + '_*_' + dcm + '.nc'))
                        if len(flist2) > 0:
                            list_variant2.append(v)
                    data.loc[count, 'Variant in .intermediate.' + \
                             'Interp_Merge.' + land_mask] = \
                             '; '.join(list_variant2)
                count += 1
########
# pr, tas
########
for var in ['pr', 'tas']:
    for expr in ['historical', 'ssp585']:
        # (only check the models relevant to mrsol)
        prefix = os.path.join(mg.path_data(), proj, expr, 'mrsol')
        model_list = [x for x in os.listdir(prefix) if \
                      os.path.isdir(os.path.join(prefix, x))]
        for m in model_list:
            flist1 = glob(os.path.join(mg.path_data(), proj, expr, var, m,
                                       var + '*' + m + '_' + expr + \
                                       '*r1i1p1f1*.nc'))

            if len(flist1) > 0:
                data.loc[count, 'Project'] = proj
                data.loc[count, 'Experiment'] = expr
                data.loc[count, 'Model'] = m

                r = xr.open_dataset(flist1[0])
                data.loc[count, 'Nominal resolution (lat)'] = \
                    np.mean(r.lat.values[1:] - r.lat.values[:-1])
                data.loc[count, 'Nominal resolution (lon)'] = \
                    np.mean(r.lon.values[1:] - r.lon.values[:-1])
                r.close()

                data.loc[count, 'Variable'] = var

                list_variant = list(np.unique([x.split('_')[-3] \
                                               for x in flist1]))
                data.loc[count, 'Variant in .data.'] = '; '.join(list_variant)

                for land_mask in ['None', 'vanilla']:
                    list_variant2 = []
                    for v in list_variant:
                        flist2 = glob(os.path.join(mg.path_intrim_out(), 
                                                   'Interp_Merge', land_mask,
                                                   proj, m + '_' + v,
                                                   var + '_*.nc'))
                        if len(flist2) > 0:
                            list_variant2.append(v)
                    data.loc[count, 'Variant in .intermediate.' + \
                             'Interp_Merge.' + land_mask] = \
                             '; '.join(list_variant2)
                count += 1


data.to_csv(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                         'check_model_interpolated.csv'))
