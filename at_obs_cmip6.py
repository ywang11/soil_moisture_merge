# -*- coding: utf-8 -*-
"""
Created on Thu May 16 19:19:40 2019

@author: ywang254

Because these take a long time to produce, extract the soil moisture from 
the individual CMIP6 models.
"""
import utils_management as mg
from utils_management.constants import depth, depth_cm, year_longest, \
    year_cmip6
import os
import xarray as xr
from misc.ismn_utils import get_ismn_aggr_method, get_weighted_monthly_data
from misc.dolce_utils import get_cov_method, get_weighted_sm_points
from misc.cmip6_utils import mrsol_availability, one_layer_availability
from glob import glob
import pandas as pd
import time
import multiprocessing as mp


start = time.time()

land_mask = 'vanilla' # The land mask applied on the land surface models.

# Choose the weighting method that was used to generate the weighted averaged
# ISMN data.
# ---- default to LU-weighted, not cut off by minimum pct represented 
#      land cover
ismn_aggr_ind = REPLACE1 # 0, 1, 2
simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = get_ismn_aggr_method(simple[ismn_aggr_ind],
                                        dominance_lc[ismn_aggr_ind],
                                        dominance_threshold)

##for i,d in enumerate(depth):
i = REPLACE2
d = depth[i]
dcm = depth_cm[i]

grid_latlon, _, _ = get_weighted_monthly_data(os.path.join( \
    mg.path_intrim_out(), 'ismn_aggr'), ismn_aggr_method, d, 'all')


# Relevant CMIP6 files (emergent constraint).
# ---- the models that have precipitation.
cmip6_list_1 = mrsol_availability(dcm, land_mask, 'historical')
cmip6_list_2 = mrsol_availability(dcm, land_mask, 'ssp585')
# ---- further subset to the given depth.
cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) )
# ---- further subset to the first ensemble member.
cmip6_list = [x for x in cmip6_list if 'r1i1p1f1' in x]


def extract(l):
    # Choose the time frame to conduct the calculation. But selecting the
    # longest year already encompasses all the years.
    file1 = [os.path.join(mg.path_intrim_out(), 'Interp_Merge', 
                          land_mask, 'CMIP6', l, 'mrsol_historical_' + \
                          str(y) + '_' + dcm + '.nc') for y in year_cmip6]
    file2 = [os.path.join(mg.path_intrim_out(), 'Interp_Merge', 
                          land_mask, 'CMIP6', l, 'mrsol_ssp585_' + \
                          str(y) + '_' + dcm + '.nc') for y in \
             list(set(year_longest) - set(year_cmip6))]

    if not file1 or not file2:
        raise('Missing file at model ' + l)

    data = xr.open_mfdataset(file1 + file2, decode_times=False)
    sm = data.sm.copy(deep=True)
    data.close()
    # ---- obtain values at the observed data points (do not subset
    #      the dates).
    sm_at_data = get_weighted_sm_points(grid_latlon, sm, year_longest)
    sm_at_data.to_csv(os.path.join(mg.path_intrim_out(), 'at_obs_cmip6', 
                                   ismn_aggr_method + '_' + \
                                   l + '_' + d + '.csv'))


pool = mp.Pool(mp.cpu_count())
pool.map_async(extract, cmip6_list)

pool.close()
pool.join()

end = time.time()
print('The script finished in ' + ('%.4f' % ((end-start)/3600) ) + ' hours.')
