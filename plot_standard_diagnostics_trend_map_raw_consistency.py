"""
20200109

Period of trend: 1981-2010

Plot
1. The percentage agreement on signs. Only plot >50% agreements. Use blues to
   indicate agreement on positive sign, and reds to indicate agreement on 
   negative sign. 
2. \frac{\mu}{\sigma} of the trend, which is the inverse of the coefficient
   of variation. Since std is always positive, negative 1/CV implies
   negative trend, and positive 1/CV implies positive trend. Under Gaussian
   assumption, \frac{\mu}{\sigma} exceeding 0.95, 1.64 or 2.57 imples 66%,
   90%, or 99% confidence in \mu being significantly different from zero.
"""
from scipy.stats import variation
from utils_management.constants import year_shortest, depth, lsm_list, \
    depth_cm, target_lat, target_lon, lsm_reanalysis, lsm_mstmip, lsm_trendy
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability
import numpy as np
import os
import itertools as it
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from cartopy.util import add_cyclic_point
from matplotlib import cm, colors
import multiprocessing as mp
import xarray as xr
import utils_management as mg


model_set_list = ['lsm', 'cmip5', 'cmip6', 'lsm_reanalysis', 'lsm_mstmip',
                  'lsm_trendy']
land_mask = 'vanilla'
season_list = ['Annual', 'DJF', 'MAM', 'JJA', 'SON']


for model_set in model_set_list:
    ###########################################################################
    # Collect the data.
    ###########################################################################
    pos_in_0 = {}
    for i in range(4):
        d = depth[i]
        dcm = depth_cm[i]

        if model_set == 'lsm':
            model_list = lsm_list[(str(year_shortest[0]) + '-' + \
                                   str(year_shortest[-1]), d)]
        elif model_set == 'cmip5':
            model_list = cmip5_availability(dcm, land_mask)
        elif model_set == 'cmip6':
            cmip6_list_1 = mrsol_availability(dcm, land_mask, 'historical')
            cmip6_list_2 = mrsol_availability(dcm, land_mask, 'ssp585')
            cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) )
            model_list = [x for x in cmip6_list if 'r1i1p1f1' in x]
        elif model_set == 'lsm_reanalysis':
            model_list = list(set(lsm_list[(str(year_shortest[0]) + '-' + \
                                            str(year_shortest[-1]), d)]) & \
                              set(lsm_reanalysis))
        elif model_set == 'lsm_mstmip':
            model_list = list(set(lsm_list[(str(year_shortest[0]) + '-' + \
                                            str(year_shortest[-1]), d)]) & \
                              set(lsm_mstmip))
        elif model_set == 'lsm_trendy':
            model_list = list(set(lsm_list[(str(year_shortest[0]) + '-' + \
                                            str(year_shortest[-1]), d)]) & \
                              set(lsm_trendy))

        if i == 0:
            overall_trend = np.full([len(season_list), len(depth),
                                     len(model_list),
                                     len(target_lat), len(target_lon)], np.nan)
            model_list0 = model_list
            pos_in_0[i] = list(range(len(model_list)))
        else:
            pos_in_0[i] = []
            for elem in model_list:
                pos_in_0[i].append(model_list0.index(elem))


    ###########################################################################
    # Collect data.
    ###########################################################################
    for ss_ind, ss in enumerate(season_list):
        for i in range(4):
            d = depth[i]
            dcm = depth_cm[i]
            for p in pos_in_0[i]:
                if 'lsm' in model_set:
                    data = xr.open_dataset(os.path.join(mg.path_out(), 
                        'standard_diagnostics_' + model_set.split('_')[0],
                        model_list0[p] + '_' + d + '_g_map_trend_' \
                                                        + ss + '.nc'))
                elif model_set == 'cmip5':
                    data = xr.open_dataset(os.path.join(mg.path_out(), 
                        'standard_diagnostics_' + model_set.split('_')[0],
                        model_list0[p] + '_' + dcm + '_g_map_trend_' \
                                                        + ss + '.nc'))
                else:
                    data = xr.open_dataset(os.path.join(mg.path_out(), 
                        'standard_diagnostics_' + model_set.split('_')[0],
                        'mrsol_' + model_list0[p] + '_' + dcm + \
                        '_g_map_trend_' + ss + '.nc'))
                overall_trend[ss_ind, i, p, 
                              :, :] = data['g_map_trend'].values.copy()
                data.close()


    ###########################################################################
    # 1. Percentage agreement on signs.
    ###########################################################################
    ################
    # (1) Calculate.
    ################
    eps = 1e-10
    pct_pos = 100. * np.ma.mean(np.ma.masked_invalid(overall_trend) > eps, 
                                axis = 2)
    pct_neg = 100. * np.ma.mean(np.ma.masked_invalid(overall_trend) < -eps, 
                                axis = 2)
    # (shrunk by 50 to make the colorbar easier)
    pct_all = np.ma.where(pct_pos > (50. + eps), pct_pos - 50., 0.)
    pct_all = np.ma.where(pct_neg > (50. + eps), - (pct_neg - 50.), pct_all)

    np.ma.set_fill_value(pct_all, np.nan)
    pct_all = np.ma.filled(pct_all)

    ################
    # (2) Plot.
    ################
    levels = np.linspace(-50., 50., 11)
    fig, axes = plt.subplots(nrows = len(depth), ncols = len(season_list),
                             subplot_kw = {'projection': ccrs.PlateCarree()},
                             figsize = (25,10))
    fig.subplots_adjust(hspace = 0., wspace = 0.01)
    for i, ss_ind in it.product(range(4), range(len(season_list))):
        ax = axes[i, ss_ind]

        # Skip some depths if too few models.
        tot = len(pos_in_0[i])
        if tot == 1:
            ax.text(0.1, 0.5, 'Only ' + model_list0[pos_in_0[i][0]] + \
                    ' exists. Skipping', transform = ax.transAxes)
            continue
        elif tot == 0:
            ax.text(0.1, 0.5, 'No data. Skipping.', transform = ax.transAxes)
            continue

        val = pct_all[ss_ind, i, :, :].copy()
        ax.coastlines()
        ax.set_extent([-180, 180, -60., 90])

        # Do not add cyclic because sometimes cause segmentation fault.
        cf = ax.contourf(target_lon, target_lat, val, cmap = 'RdYlBu',
                         levels = levels)
        if ss_ind == 0:
            ax.text(-0.03, 0.55, depth[i], va='bottom', ha='center',
                    rotation='vertical', rotation_mode='anchor',
                    transform=ax.transAxes)
        if i == 0:
            ax.set_title(season_list[ss_ind] + ' n = ' + str(tot))
        else:
            ax.set_title('n = ' + str(tot))
    cax = fig.add_axes([0.91, 0.1, 0.01, 0.8])
    cb = fig.colorbar(cf, cax = cax, boundaries = levels)
    cb.set_ticks(levels)
    cb.set_ticklabels([('%d' % (np.abs(l) + 50)) for l in levels])
    cb.ax.set_ylabel('Sign agreement (%): Reds - negative, Blues - positive.')
    fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                             'trend_map_raw_consistency',
                             'pct_sign_agreement_' + model_set + '.png'),
                dpi = 600.)
    plt.close(fig)


    ###########################################################################
    # 2. CV of trend.
    ###########################################################################
    ################
    # (1) Calculate.
    ################
    inv_cv = 1/variation(overall_trend, axis = 2, nan_policy = 'omit')

    ################
    # (2) Plot.
    ################
    levels = [-2.57, -1.64, -0.95, -0.5, 0., 0.5, 0.95, 1.64, 2.57]
    cmap = cm.get_cmap('jet_r')
    temp = cmap(np.linspace(0., 1., len(levels) + 5))
    cmap2 = colors.ListedColormap(np.concatenate([temp[:5,:],
                                                  temp[7,:][np.newaxis,:],
                                                  temp[9:,:]], axis = 0))

    fig, axes = plt.subplots(nrows = len(depth), ncols = len(season_list),
                             subplot_kw = {'projection': ccrs.PlateCarree()},
                             figsize = (25,10))
    fig.subplots_adjust(hspace = 0., wspace = 0.01)
    for i, ss_ind in it.product(range(4), range(len(season_list))):
        ax = axes[i, ss_ind]

        # Skip some depths if too few models.
        tot = len(pos_in_0[i])
        if tot == 1:
            ax.text(0.1, 0.5, 'Only ' + model_list0[pos_in_0[i][0]] + \
                    ' exists. Skipping', transform = ax.transAxes)
            continue
        elif tot == 0:
            ax.text(0.1, 0.5, 'No data. Skipping.', transform = ax.transAxes)
            continue

        ax.coastlines()
        ax.set_extent([-180, 180, -60., 90])
        pct_cyc, lon_cyc = add_cyclic_point(inv_cv[ss_ind, i, :, :],
                                            coord = target_lon)
        cf = ax.contourf(lon_cyc, target_lat, pct_cyc, 
                         cmap = cmap2,
                         levels = levels, extend = 'both')
        if ss_ind == 0:
            ax.text(-0.03, 0.55, depth[i], va='bottom', ha='center',
                    rotation='vertical', rotation_mode='anchor',
                    transform=ax.transAxes)
        if i == 0:
            ax.set_title(season_list[ss_ind] + ' n = ' + str(tot))
        else:
            ax.set_title('n = ' + str(tot))
    cax = fig.add_axes([0.91, 0.1, 0.01, 0.8])
    cb = fig.colorbar(cf, cax = cax, boundaries = levels)
    cb.set_ticks(levels)
    ##cb.set_ticklabels(['', '', '99%', '90%', '66%', '0', '66%', '90%', 
    ##                   '99%', '', ''])
    cb.ax.set_ylabel('Mean Trend/Std. Trend')
    fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                             'trend_map_raw_consistency',
                             'frac_mean_std_' + model_set + '.png'),
                dpi = 600.)
    plt.close(fig)
