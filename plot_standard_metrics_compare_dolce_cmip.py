"""
2019/07/23

ywang254@utk.edu

Compare the standard metrics between the different ways to calculate the 
covariance matrix for DOLCE weighting.
"""
import os
import sys
import pandas as pd
import numpy as np
from utils_management.constants import depth, year_longest
import utils_management as mg
import matplotlib.pyplot as plt
from misc.ismn_utils import get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
import itertools as it

# MODIFY
USonly = True
model_set = 'cmip6'

if USonly:
    suffix = '_USonly'
else:
    suffix = ''

# Covariance methods
cov_method = [get_cov_method(i) for i in range(5)]

#
year = year_longest

# Separate plot for each metric.
for metric in ['RMSE', 'Bias', 'uRMSE', 'Corr']:
    fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(10,10),
                             sharex=True, sharey=True)
    for d_ind, d in enumerate(depth):
        # Collect the results.
        metric_collect = {}
        for cov in cov_method:
            metric_collect[cov] = pd.read_csv(os.path.join(mg.path_out(),
                'standard_metrics', 'dolce_' + model_set + '_' + \
                d + '_' + str(year[0]) + '-' + str(year[-1]) + \
                suffix + '.csv'), header = [0,1], index_col = [0,1] \
            ).loc[(slice(None), cov), (metric, slice(None))].values

        # Plot the collected results.
        ax = axes.flat[d_ind]

        bp = [None for i in range(len(cov_method))]
        for cov_ind, cov in enumerate(cov_method):
            bp[cov_ind] = ax.boxplot(metric_collect[cov].reshape(-1,1),
                                     positions = [cov_ind])
        ax.set_xticks(range(len(cov_method)))
        ax.set_xticklabels(cov_method, rotation = 30.)
        ax.set_title('Depth: ' + d)
        ax.set_ylabel(metric)

    fig.savefig(os.path.join(mg.path_out(), 'standard_metrics', 
                             'plot_compare_dolce_' + model_set + '_' + \
                             metric + suffix + '.png'), dpi = 600,
                bbox_inches='tight')
    plt.close(fig)
