"""
2019/07/23

ywang254@utk.edu

Write the RMSE, Bias, uRMSE, and Corr of the ESA-CCI, GLEAMv3.3a, and the
 ERA-Land results, the mean & range of the LSM+CMIP5+CMIP6 models, and the
 DOLCE-products. Only the validation period.
"""
import os
import sys
import pandas as pd
import numpy as np
from utils_management.constants import depth, depth_cm, year_longest, \
    year_shorter, year_shorter2, year_shortest, lsm_list, year_cmip5, \
    year_cmip6
import utils_management as mg
import matplotlib.pyplot as plt
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability, one_layer_availability
from misc.ismn_utils import get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
import itertools as it


# Setup options
land_mask = 'vanilla'
pr = 'val'
year = year_longest
year_str = str(year[0]) + '-' + str(year[-1])
iam = 'lu_weighted'
cov = 'ShrunkCovariance'


###############################################################################
# Loops to make the table.
###############################################################################
metric_list = ['RMSE', 'Bias', 'uRMSE', 'Corr']
for metric, USonly in it.product(metric_list, [True, False]):
    if USonly:
        suffix = '_USonly'
    else:
        suffix = ''

    data = pd.DataFrame(data = np.nan, 
                        index = ['ESA-CCI', 'GLEAM v3.3a', 'ERA-Land', 
                                 'Mean-All', 'Mean-All-min', 'Mean-All-max',
                                 'Median-Products'], columns = depth)

    for count, d in enumerate(depth):
        # ESA CCI, GLEAM, ERA-Land.
        if (d == '0.00-0.10'):
            temp = pd.read_csv(os.path.join(mg.path_out(),
                                            'standard_metrics',
                                            'lsm_' + d + '_1981-2016' + \
                                            suffix + '.csv'),
                               index_col = [0,1], header = [0,1])
            data.loc['ESA-CCI', d] = temp.loc[(iam, 'ESA-CCI'), (metric, pr)]
            data.loc['GLEAM v3.3a', d] = temp.loc[(iam, 'GLEAMv3.3a'),
                                                  (metric, pr)]
        if (d == '0.00-0.10') | (d == '0.10-0.30'):
            temp = pd.read_csv(os.path.join(mg.path_out(),
                                            'standard_metrics',
                                            'lsm_' + d + '_1981-2010' + \
                                            suffix + '.csv'),
                               index_col = [0,1], header = [0,1])
            data.loc['ERA-Land', d] = temp.loc[(iam, 'ERA-Land'), (metric, pr)]

        # All the models.
        temp = pd.read_csv(os.path.join(mg.path_out(), 'standard_metrics', 
                                        'meanmedian_all_' + d + '_' + \
                                        year_str + suffix + '.csv'),
                           index_col = [0,1], header = [0,1])
        data.loc['Mean-All', d] = temp.loc[(iam, 'mean'), (metric, pr)]

        # Read the LSM, CMIP5, and CMIP6 data and take the max & min.
        temp_lsm = pd.read_csv(os.path.join(mg.path_out(),
                                            'standard_metrics',
                                            'lsm_' + d + '_' + year_str + \
                                            suffix + '.csv'),
                               index_col = [0,1], 
                               header = [0,1]).loc[(iam, slice(None)),
                                                   (metric, pr)]
        temp_cmip5 = pd.read_csv(os.path.join(mg.path_out(),
                                              'standard_metrics',
                                              'cmip5_' + d + suffix + '.csv'),
                                 index_col = [0,1], 
                                 header = [0,1]).loc[(iam, slice(None)),
                                                     (metric, pr)]
        temp_cmip6 = pd.read_csv(os.path.join(mg.path_out(),
                                              'standard_metrics',
                                              'cmip6_' + d + '.csv'),
                                 index_col = [0,1],
                                 header = [0,1]).loc[(iam, slice(None)),
                                                     (metric, pr)]
        data.loc['Mean-All-min', d] = np.min([np.min(temp_lsm),
                                              np.min(temp_cmip5),
                                              np.min(temp_cmip6)])
        data.loc['Mean-All-max', d] = np.max([np.max(temp_lsm),
                                              np.max(temp_cmip5),
                                              np.max(temp_cmip6)])

        # Read the final OLC-EC product.
        data.loc['Median-Products', 
                 d] = pd.read_csv(os.path.join(mg.path_out(),
                                               'standard_metrics',
                                               'meanmedian_products_' + d + \
                                               '_' + year_str + suffix + \
                                               '.csv'), index_col = [0,1],
                                  header = [0,1]).loc[(iam, 'median'),
                                                      (metric, pr)]

    data.to_csv(os.path.join(mg.path_out(), 'table_standard_metrics_all_' + \
                             metric + suffix + '.csv'))
