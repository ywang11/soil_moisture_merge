"""
2019/08/05
ywang254@utk.edu

Plot the time series of global mean annual mean soil moisture of the 
 DOLCE results, with the upper CI and lower CI.
Show the GLEAMv3.3 and ERA-Land time series of global mean annual mean
 soil moisture for comparison.
Compare between the 1950-2016 simple product (which has fewer models), 
 concatenated product no merge, and merged product.
"""
import matplotlib.pyplot as plt
import utils_management as mg
from utils_management.constants import year_longest, depth, depth_cm
from misc.plot_utils import plot_ts_shade, plot_ts_trend
import pandas as pd
import os
import numpy as np


# MODIFY
iam = 'lu_weighted'
cov_method = 'ShrunkCovariance'
year = year_longest


#
year_str = str(year[0]) + '-' + str(year[-1])


#
global_ts_all_depth = {}
for i,d in enumerate(depth):
    dcm = depth_cm[i]

    # (1) 1950-2016 continuous dataset with fewer models
    simple = pd.read_csv(os.path.join(mg.path_out(),
                                      'standard_diagnostics_dolce_lsm',
                                      iam + '_' + cov_method + \
                                      '_weighted_average_' + year_str + '_' + \
                                      d + '_g_ts.csv'), index_col = 0, 
                         parse_dates = True).iloc[:, 0]

    simple_uncertainty = pd.read_csv(os.path.join(mg.path_out(),
        'standard_diagnostics_dolce_lsm', iam + '_' + cov_method + \
        '_weighted_uncertainty_' + year_str + '_' + d + '_g_ts.csv'),
        index_col = 0, parse_dates = True).iloc[:, 0]


    # (2) Concatenated dataset, no merge
    concat = pd.read_csv(os.path.join(mg.path_out(), 
                                      'standard_diagnostics_concat',
                                      'no_merge_dolce', 
                                      'no_merge_dolce_average_' + iam + '_' + \
                                      cov_method + '_' + d + '_g_ts.csv'),
                         index_col = 0, parse_dates = True).iloc[:, 0]
    concat_uncertainty = pd.read_csv(os.path.join(mg.path_out(), 
                                     'standard_diagnostics_concat',
                                     'no_merge_dolce',
                                     'no_merge_dolce_uncertainty_' + iam + \
                                     '_' + cov_method + '_' + d + '_g_ts.csv'),
                                     index_col = 0, 
                                     parse_dates = True).iloc[:, 0]


    # (3)
    merge = pd.read_csv(os.path.join(mg.path_out(), 
                                     'standard_diagnostics_concat', 'dolce',
                                     'dolce_average_' + iam + '_' + \
                                     cov_method + '_' + d + '_g_ts.csv'),
                        index_col = 0, parse_dates = True).iloc[:, 0]
    """
    merge_uncertainty = pd.read_csv(os.path.join(mg.path_out(), 
                                                 'standard_diagnostics_concat',
                                                 'dolce',
                                                 'dolce_uncertainty_' + \
                                                 iam + '_' + cov_method + \
                                                 d + '_g_ts.csv'),
                                    parse_dates = True).iloc[:, 0]
    """

    #
    simple = simple.groupby(simple.index.year).mean()
    simple_uncertainty = simple_uncertainty.groupby( \
        simple_uncertainty.index.year).mean()
    concat = concat.groupby(concat.index.year).mean()
    concat_uncertainty = concat_uncertainty.groupby( \
        concat_uncertainty.index.year).mean()
    merge = merge.groupby(merge.index.year).mean()
    """
    merge_uncertainty = merge_uncertainty.groupby(merge.index.year).mean()
    """

    # Gather the data
    global_ts_all_depth[d] = {}

    global_ts = {}
    global_ts['mean'] = simple
    global_ts['min'] = global_ts['mean'] - 2*simple_uncertainty # 95% CI
    global_ts['max'] = global_ts['mean'] + 2*simple_uncertainty # 95% CI
    global_ts_all_depth[d]['simple'] = global_ts

    global_ts = {}
    global_ts['mean'] = concat
    global_ts['min'] = global_ts['mean'] - 2*concat_uncertainty # 95% CI
    global_ts['max'] = global_ts['mean'] + 2*concat_uncertainty # 95% CI
    global_ts_all_depth[d]['concat'] = global_ts

    global_ts = {}
    global_ts['mean'] = merge
    global_ts['min'] = global_ts['mean'] - 0 ###2*merge_uncertainty # 95% CI
    global_ts['max'] = global_ts['mean'] + 0 ###2*merge_uncertainty # 95% CI
    global_ts_all_depth[d]['merge'] = global_ts


# 
fig, axes = plt.subplots(nrows = len(depth_cm), ncols = 2, sharex = True, 
                         sharey = False, figsize = (10, 6.5))
fig.subplots_adjust(hspace = 0., wspace = 0.2)

# (1) Soil Moisture
for i,d in enumerate(depth):
    dcm = depth_cm[i]

    ax = axes[i, 0]
    h1 = plot_ts_shade(ax, global_ts_all_depth[d]['simple']['mean'].index, 
                       global_ts_all_depth[d]['simple'], ts_col = 'b')
    h2 = plot_ts_shade(ax, global_ts_all_depth[d]['concat']['mean'].index, 
                       global_ts_all_depth[d]['concat'], ts_col = 'r')
    h3 = plot_ts_shade(ax, global_ts_all_depth[d]['merge']['mean'].index, 
                       global_ts_all_depth[d]['merge'], ts_col = 'g')

    if i == 0:
        ax.set_title('Soil Moisture (m$^3$/m$^3$)')
    ax.set_xlim([year[0], year[-1]])
    if i == len(depth) - 1:
        ax.set_xlabel('Year')
    ax.set_ylabel(dcm) # '(m$^3$/m$^3$)'
    ax.set_ylim([0., 0.5])
    ax.set_yticks(np.linspace(0.1, 0.4, 5))

ax.legend([h1, h2, h3], ['1950-2016', 'Concat', 'Merged'],
          ncol = 3, loc = 'lower right', 
          bbox_to_anchor = (1.5, -0.7))

# (2) Soil Moisture Anomaly
for i,d in enumerate(depth):
    dcm = depth_cm[i]

    ax = axes[i, 1]
    h1, = ax.plot(global_ts_all_depth[d]['simple']['mean'].index, 
                  global_ts_all_depth[d]['simple']['mean'] - \
                  global_ts_all_depth[d]['simple']['mean'].mean(), '-b', 
                  linewidth = 2, zorder = 3)
    h2, = ax.plot(global_ts_all_depth[d]['concat']['mean'].index, 
                  global_ts_all_depth[d]['concat']['mean'] - \
                  global_ts_all_depth[d]['concat']['mean'].mean(), '-r',
                  linewidth = 2, zorder = 3)
    h3, = ax.plot(global_ts_all_depth[d]['merge']['mean'].index,
                  global_ts_all_depth[d]['merge']['mean'].values - \
                  global_ts_all_depth[d]['merge']['mean'].mean(), '-g',
                  linewidth = 2, zorder = 3)

    ax.set_title('Anomaly (m$^3$/m$^3$)')
    ax.set_xlim([year[0], year[-1]])
    ax.set_xlabel('Year')
    ax.set_ylim([-0.005, 0.005])
    ax.set_yticks(np.linspace(-0.004, 0.004, 5))

fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                         'ts_annual', 'compare_concat_dolce_lsm_' + \
                         iam + '_' + cov_method + '_' + year_str + '.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)
