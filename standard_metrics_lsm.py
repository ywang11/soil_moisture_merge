# -*- coding: utf-8 -*-
"""
Created on Wed May  1 21:04:14 2019

@author: ywang254

Calculate the metrics between between the individual land surface models, and
the observed soil moisture.
"""
import os
import utils_management as mg
import pandas as pd
from utils_management.constants import depth, depth_cm, lsm_list, \
    year_longest, year_shorter, year_shorter2, year_shortest
from misc.ismn_utils import get_ismn_aggr_method, get_weighted_monthly_data, \
    get_weighted_monthly_data_USonly
from misc.analyze_utils import calc_stats
import numpy as np
import itertools as it
import time


# MODIFY:
# Whether the metric is limited to the performance in CONUS.
USonly = True


# List the weighting methods that are used to generate the weighted
# averaged ISMN data.
simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = [get_ismn_aggr_method(simple[i], dominance_lc[i], 
                                         dominance_threshold) for i in \
                    range(len(simple))]


year_list = [year_longest, year_shorter, year_shorter2, year_shortest]


start = time.time()
for i,year in it.product(range(len(depth)), year_list):
    d = depth[i]
    dcm = depth_cm[i]

    # Check existence. The different ismn methods should not affect which
    # land surface models are available.
    lsm = lsm_list[(str(year[0]) + '-' + str(year[-1]), d)]

    # Collector for the calculated metrics in time and space.
    metrics_collect = pd.DataFrame(data = np.nan,
        index = pd.MultiIndex.from_product([ismn_aggr_method, lsm], 
                                           names = ['ISMN Aggr', 'LSM']), 
        columns = pd.MultiIndex.from_product([['RMSE','Bias','uRMSE','Corr'],
                                              ['all','cal','val']],
                                             names = ['Metric', 'ISMN Set']))

    for iam, l, ismn_set in it.product(ismn_aggr_method, lsm,
                                       ['all', 'cal', 'val']):
        #######################################################################
        # Get the observed data.
        #######################################################################
        if USonly:
            grid_latlon, weighted_monthly_data, _ = \
                get_weighted_monthly_data_USonly(os.path.join( \
                    mg.path_intrim_out(), 'ismn_aggr'), iam, d, ismn_set)
        else:
            grid_latlon, weighted_monthly_data, _ = \
                get_weighted_monthly_data(os.path.join(mg.path_intrim_out(), 
                                                       'ismn_aggr'), iam, d, 
                                          ismn_set)

        #######################################################################
        # Performance of the weighted product.
        #######################################################################
        data = pd.read_csv(os.path.join(mg.path_intrim_out(), 'at_obs_lsm',
                                        iam + '_' + l + '_' + d + '.csv'), 
                           index_col = 0, parse_dates = True)
        # ---- subset to the year
        data = data.loc[(data.index >= str(year[0])+'-01-01') & \
                        (data.index <= str(year[-1])+'-12-31'), :]

        # ---- obtain values at the observed data points
        weighted_sm_at_data = data.loc[weighted_monthly_data.index, 
                                       weighted_monthly_data.columns]

        metrics_collect.loc[(iam, l), ('RMSE',ismn_set)], \
            metrics_collect.loc[(iam, l), ('Bias',ismn_set)], \
            metrics_collect.loc[(iam, l), ('uRMSE',ismn_set)], \
            metrics_collect.loc[(iam, l), ('Corr',ismn_set)] = \
            calc_stats(weighted_sm_at_data.values.reshape(-1), 
                       weighted_monthly_data.values.reshape(-1))

    if USonly:
        metrics_collect.to_csv(os.path.join(mg.path_out(),
                                            'standard_metrics', 
                                            'lsm_' + d + '_' + \
                                            str(year[0]) + '-' + \
                                            str(year[-1]) + '_USonly_' + \
                                            iam + '_' + ismn_set + '.csv'))
    else:
        metrics_collect.to_csv(os.path.join(mg.path_out(),
                                            'standard_metrics', 
                                            'lsm_' + d + '_' + \
                                            str(year[0]) + '-' + \
                                            str(year[-1]) + '_' + \
                                            iam + '_' + ismn_set + '.csv'))
end = time.time()
#
print('The script finished in ' + ('%.4f' % ((end-start)/3600) ) + ' hours.')
