"""
Compute the following for every CMIP6 model:

1. Global map (averaged over time): NetCDF
2. Global average time series.
3. Latitude bands average time series.
4. Continental average time series.
5. SREX region average time series.
"""
import os
import pandas as pd
import xarray as xr
import utils_management as mg
from utils_management.constants import year_longest, year_cmip6, depth_cm
from misc.cmip6_utils import mrsol_availability, one_layer_availability
from misc.standard_diagnostics import standard_diagnostics
import multiprocessing as mp


i = REPLACE # [0, 1, 2, 3]
dcm = depth_cm[i]

land_mask = 'vanilla'


# CMIP6 that are suitable for this depth and time period
# CMIP6 relevant to this soil layer.
# ---- the models that have precipitation.
cmip6_list_1 = mrsol_availability(dcm, land_mask, 'historical')
cmip6_list_2 = mrsol_availability(dcm, land_mask, 'ssp585')
# ---- further subset to the given depth.
cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) )
# ---- further subset to the first ensemble member.
cmip6 = [x for x in cmip6_list if 'r1i1p1f1' in x]


def calc(model):
    file1 = [os.path.join(mg.path_intrim_out(), 'Interp_Merge', land_mask, 
                          'CMIP6', model, 'mrsol_historical_' + \
                          str(y) + '_' + dcm + '.nc') for y in year_cmip6]
    file2 = [os.path.join(mg.path_intrim_out(), 'Interp_Merge', land_mask, 
                          'CMIP6', model, 'mrsol_ssp585_' + \
                          str(y) + '_' + dcm + '.nc') \
             for y in list(set(year_longest) - set(year_cmip6))]
    data = xr.open_mfdataset(file1 + file2, decode_times=False)

    time = pd.date_range(str(year_longest[0])+'-01-01', 
                         str(year_longest[-1])+'-12-31',
                         freq = 'MS')

    standard_diagnostics(data.sm.values, time, data.lat, data.lon, 
                         os.path.join(mg.path_out(), 
                                      'standard_diagnostics_cmip6'),
                         'mrsol_' + model + '_' + dcm)
    data.close()


#for model in cmip6:
pool = mp.Pool(mp.cpu_count())
pool.map_async(calc, cmip6)
pool.close()
pool.join()
