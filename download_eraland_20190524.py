# -*- coding: utf-8 -*-
"""
Created on Fri May 24 13:40:54 2019

@author: ywang254
"""
from ecmwfapi import ECMWFDataServer
import utils_management as mg
import os
from time import sleep
import sys


if sys.platform == 'linux':
    os.system('export ECMWF_API_URL="https://api.ecmwf.int/v1"')
    os.system('export ECMWF_API_KEY="f93636d89d23e3990244d2486f6fdbcb"')
    os.system('export ECMWF_API_EMAIL="ywang254@utk.edu"')


def retrieve(yy):
    server = ECMWFDataServer(url="https://api.ecmwf.int/v1",
                             key="f93636d89d23e3990244d2486f6fdbcb",
                             email="ywang254@utk.edu")
    server.retrieve({
        "class": "ei",
        "dataset": "interim_land",
        "date": str(yy)+"-01-01/to/"+str(yy)+"-12-31",
        "expver": "2",
        "levtype": "sfc",
        "param": "39.128/40.128/41.128/42.128",
        "stream": "oper",
        "time": "00:00:00/06:00:00/12:00:00/18:00:00",
        "type": "an",
        "target": os.path.join(mg.path_data(), "Reanalysis", "ERA-Land", 
                               str(yy)+".gr"),
    })


for yy in range(1979, 2011):
    retrieve(yy)
    sleep(1)
