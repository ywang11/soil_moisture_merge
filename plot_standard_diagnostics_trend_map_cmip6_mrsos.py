"""
Draw the global soil moisture trend map of the CMIP6 models at the 
interpolated 0.5 degrees resolution.
"""
import os
import pandas as pd
from utils_management.constants import year_cmip6, cmip6_list
import utils_management as mg
import xarray as xr
import numpy as np
from misc.plot_utils import plot_map_w_stipple
import matplotlib.pyplot as plt
import cartopy.crs as ccrs


year = year_cmip6 # [year_cmip5, year_cmip6]

land_mask = 'SYNMAP'

act = 'CMIP'
expr = 'historical'
var = 'mrsos'
tid = 'Lmon'

model_version = cmip6_list(act, expr, var, tid)


# A subset of 3-4 models
##model_version = model_version[(REPLACE*3):(REPLACE*3+3)]


# Graph setup.
levels = np.linspace(-5e-4, 5e-4, 10)
cmap = 'RdYlBu'



for m_v in model_version:
    ##print(m_v)

    prefix = var + '_' + expr + '_' + m_v + '_' + str(year[0]) + '-' + \
             str(year[-1])

    data = xr.open_dataset(os.path.join(mg.path_out(),
                                        'standard_diagnostics_cmip6', 
                                        prefix + '_g_map_trend_Annual.nc'))

    # Plot the trend with stippled significance
    fig, ax = plt.subplots(subplot_kw = {'projection': \
                                         ccrs.PlateCarree()},
                           figsize = (8,6))
    plot_map_w_stipple(ax, data['g_map_trend'].values,
                       data['g_p_values'].values, 
                       data.lat, data.lon, thresh = 0.05, add_cyc = True, 
                       contour_style = {'levels': levels, 'cmap': cmap, 
                                        'extend': 'both'}, 
                       cbar_style = {'ticks': levels})
    fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                             'trend_cmip6_mrsos', prefix + '.png'), dpi = 600.)
    data.close()
    plt.close(fig)
