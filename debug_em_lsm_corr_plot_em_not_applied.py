# -*- coding: utf-8 -*-
"""
2019/09/15
ywang254@utk.edu

Plot where emergent constraint was not applied due to small variability in
  pr_subset. Compare to the actual NaN's in the saved outputs of 
  "em_lsm_corr.py".
"""
import utils_management as mg
from utils_management.constants import pr_to_lsm, year_longest, \
    year_shorter, year_shorter2, year_shortest, lsm_list, depth, depth_cm, \
    target_lat, target_lon
import os
import xarray as xr
import pandas as pd
import numpy as np
import itertools as it
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from misc.plot_utils import plot_sm_map
import multiprocessing as mp


def plotter(args):
    year, dcm, opt, pr_obs_name = args

    if opt[0] == 'year_month':
        levels = range(0, len(years), 10)
    else:
        levels = [0,1]

    ###########################################################################
    # Read the output.
    ###########################################################################
    data = xr.open_dataset(os.path.join(mg.path_out(), 'em_lsm_corr', 
                                        pr_obs_name + '_' + \
                                        opt[0] + '_' + dcm,
                                        'em_not_applied_' + \
                                        str(year[0]) + '-' + \
                                        str(year[-1]) + '_' + \
                                        str(opt[-1]) + '.nc'))
    # 1 - not applied due to insufficient variability in precipitation.
    em_not_applied_1 = data.em_not_applied.values == 1
    # 2 - not applied due to insignificant p-values.
    em_not_applied_2 = data.em_not_applied.values == 2
    data.close()


    ###########################################################################
    # Plot the output. 
    ###########################################################################
    fig, axes = plt.subplots(nrows = 4, ncols = 3, figsize = (25, 15),
                             subplot_kw = {'projection': ccrs.PlateCarree()})
    for i in range(12):
        ax = axes.flat[i]
        if opt[0] == 'year_month':
            ax, cf, cb = plot_sm_map(ax, 
                                     np.sum(em_not_applied_1[i::12, :, :], 
                                            axis = 0),
                                     target_lat, target_lon, add_cyc = True,
                                     levels = levels, 
                                     cmap = 'Reds')
        else:
            ax, cf, cb = plot_sm_map(ax, em_not_applied_1[i, :, :],
                                     target_lat, target_lon, add_cyc = True,
                                     levels = levels, 
                                     cmap = 'Reds')
    fig.savefig(os.path.join(mg.path_out(), 'debug_em_lsm_corr_coef', 
                             'type1_' + pr_obs_name + '_' + opt[0] + '_' + \
                             opt[1] + '_' + dcm + '_' + str(year[0]) + '-' + \
                             str(year[-1]) + '.png'), dpi = 600., 
                bbox_inches = 'tight')
    plt.close(fig)


    fig, axes = plt.subplots(nrows = 4, ncols = 3, figsize = (25, 15),
                             subplot_kw = {'projection': ccrs.PlateCarree()})
    for i in range(12):
        ax = axes.flat[i]
        if opt[0] == 'year_month':
            ax, cf, cb = plot_sm_map(ax, 
                                     np.sum(em_not_applied_2[i::12, :, :], 
                                            axis = 0),
                                     target_lat, target_lon, add_cyc = True,
                                     levels = levels, 
                                     cmap = 'Blues')
        else:
            ax, cf, cb = plot_sm_map(ax, em_not_applied_2[i, :, :],
                                     target_lat, target_lon, add_cyc = True,
                                     levels = levels,
                                     cmap = 'Blues')
    fig.savefig(os.path.join(mg.path_out(), 'debug_em_lsm_corr_coef', 
                             'type2_' + pr_obs_name + '_' + opt[0] + '_' + \
                             opt[1] + '_' + dcm + '_' + str(year[0]) + '-' + \
                             str(year[-1]) + '.png'), dpi = 600.,
                bbox_inches = 'tight')
    plt.close(fig)


##for year, dcm, opt, pr_obs_name in \
##    it.product([year_longest, year_shorter, year_shorter2, year_shortest],
##               depth_cm, [('month', '1grid'), ('month_anomaly', '1grid'),
##                          ('year_month', '9grid')], 
##               ['CRU_v4.03', 'GPCC', 'UDEL']):
##    plotter(year, dcm, opt, pr_obs_name)

iterator = list(it.product([year_longest, year_shorter, year_shorter2, 
                            year_shortest], depth_cm, 
                           [('month', '1grid'), ('month_anomaly', '1grid'),
                            ('year_month', '9grid')], 
                           ['CRU_v4.03'])) # 'CRU_v4.03', 'GPCC', 'UDEL'

pool = mp.Pool(4)
pool.map_async(plotter, iterator)
pool.close()
pool.join()
