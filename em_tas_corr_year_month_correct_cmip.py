"""
2020/01/10

Catch & correct negative soil moisture in the concatenated or produced
 emergent constraint soil moisture values.
Not including lsm & all, because these should correct concatenated values.
"""
import xarray as xr
import os
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth, depth_cm
import utils_management as mg
import numpy as np
import itertools as it
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import pandas as pd
from misc.em_utils import rm_oor


year_str = str(year_longest[0]) + '-' + str(year_longest[-1])
period = pd.date_range(str(year_longest[0]) + '-01-01',
                       str(year_longest[-1]) + '-12-31', freq = 'MS')


###############################################################################
# Setup of the emergent constraint method.
###############################################################################
i = REPLACE1 #[0, 1, 2, 3]
d = depth[i]
dcm = depth_cm[i]

model = 'REPLACE2' # 'cmip5', 'cmip6', '2cmip'

opt = 'REPLACE3' # 'year_month_anomaly_9grid', 'year_month_anomaly_1grid'
                 # 'year_month_9grid', 'year_month_1grid'


###############################################################################
# Read the restored emergent constraint-based soil moisture. Find the 
# negative values. Plot. Set the negative values to 1e-16, and the values > 1.
# to 1 - 1e-16.
###############################################################################
for prefix in ['predicted']: # , 'predicted_CI_lower', 'predicted_CI_upper'
    data = xr.open_mfdataset([os.path.join(mg.path_out(),
                                           'em_' + model + '_corr',
                                           'CRU_v4.03_' + \
                                           opt.replace('anomaly', 'restored') \
                                           + '_' + dcm + '_' + year_str,
                                           prefix + '_' + str(y) + '.nc') \
                              for y in year_longest], decode_times = False,
                             concat_dim = 'time')
    data[prefix].load()

    rm_oor(data[prefix], os.path.join(mg.path_out(), 'em_' + model + '_corr',
                                      'CRU_v4.03_' + opt.replace('anomaly',
                                                                 'positive')\
                                      + '_' + dcm + '_' + year_str),
           '', prefix, period)

    data.close()
