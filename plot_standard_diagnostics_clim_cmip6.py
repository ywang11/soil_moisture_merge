"""
2019/07/05

ywang254@utk.edu

Map the global mean and seasonal mean soil moisture for the individual
  CMIP5 models.
"""
from utils_management.constants import year_longest, depth_cm
import utils_management as mg
from misc.cmip6_utils import mrsol_availability, one_layer_availability
from misc.plot_standard_diagnostics_utils import plot_map
import numpy as np
import os
import multiprocessing as mp


levels = np.linspace(0., 0.6, 15)
cmap = 'coolwarm_r'
year = year_longest


land_mask = 'vanilla'

cmip6 = []
for dcm in depth_cm:
    # ---- the models that have precipitation.
    cmip6_list_1 = mrsol_availability(dcm, land_mask, 'historical')
    cmip6_list_2 = mrsol_availability(dcm, land_mask, 'ssp585')
    cmip6_list_3 = one_layer_availability('pr', land_mask, 'historical')
    cmip6_list_4 = one_layer_availability('pr', land_mask, 'ssp585')
    # ---- further subset to the given depth.
    cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) & \
                       set(cmip6_list_3) & set(cmip6_list_4) )
    # ---- further subset to the first ensemble member.
    cmip6_list = [x for x in cmip6_list if 'r1i1p1f1' in x]
    cmip6.extend(cmip6_list)
cmip6 = np.unique(cmip6)

def plotter(l):
    plot_map(depth_cm, os.path.join(mg.path_out(), 
                                    'standard_diagnostics_cmip6', 
                                    'mrsol_' + l), 
             os.path.join(mg.path_out(), 'standard_diagnostics_plot', 
                          'mean_cmip6', l), 'map', levels, cmap)

# debug
##plotter(cmip5[0])

pool = mp.Pool(mp.cpu_count() - 1)
pool.map_async(plotter, cmip6)
pool.close()
pool.join()
