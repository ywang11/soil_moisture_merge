# -*- coding: utf-8 -*-
"""
20190918
ywang254@utk.edu

Mean/Median of the raw model datasets.
"""
import xarray as xr
import os
import utils_management as mg
from utils_management.constants import depth, depth_cm, year_cmip6, \
    year_cmip5, year_longest, year_shorter, year_shorter2, year_shortest, \
    lsm_list, target_lat, target_lon
import pandas as pd
import numpy as np
from misc.cmip6_utils import mrsol_availability, one_layer_availability
from misc.cmip5_availability import cmip5_availability
import itertools as it
import multiprocessing as mp


year_list = [year_longest, year_shorter, year_shorter2, year_shortest]
land_mask = 'vanilla'


#for operation, year, i in it.product(['mean', 'median'],
#                                     year_list, range(4)): 
def calc(option):
    operation, year, i = option
    d = depth[i]
    dcm = depth_cm[i]

    period = pd.date_range(str(year[0]) + '-01-01', str(year[-1]) + '-12-31',
                           freq = 'MS')

    # Relevant models.
    lsm_model = lsm_list[(str(year[0])+'-'+str(year[-1]), d)]
    cmip5_model = cmip5_availability(dcm, land_mask)

    cmip6_list_1 = mrsol_availability(dcm, land_mask, 'historical')
    cmip6_list_2 = mrsol_availability(dcm, land_mask, 'ssp585')
    cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) )
    cmip6_model = [x for x in cmip6_list if 'r1i1p1f1' in x]


    # Final median values.
    collect_years = np.empty([12*len(year), len(target_lat), len(target_lon)])

    for y in year:
        collection = np.empty([12, len(target_lat), len(target_lon), 
                               len(lsm_model + cmip5_model + cmip6_model)])

        #######################################################################
        # LSM part.
        #######################################################################
        for j,l in enumerate(lsm_model):
            data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 
                                                'Interp_Merge', land_mask, l,
                                                l + '_' + str(y) + '_' + \
                                                depth_cm[i] + '.nc'),
                                   decode_times = False)
            collection[:, :, :, j] = data.sm.values.copy()
            data.close()

        #######################################################################
        # CMIP5 part.
        #######################################################################
        for j,l in enumerate(cmip5_model, len(lsm_model)):
            if y <= year_cmip5[-1]:
                scn = 'historical'
            else:
                scn = 'rcp85'
            data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 
                                                'Interp_Merge', land_mask, 
                                                'CMIP5', l, 'sm_' + scn + \
                                                '_r1i1p1_' + str(y) + '_' + \
                                                dcm + '.nc'),
                                   decode_times = False)
            collection[:, :, :, j] = data.sm.values.copy()
            data.close()

        #######################################################################
        # CMIP6 part.
        #######################################################################
        for j,l in enumerate(cmip6_model, len(lsm_model) + len(cmip5_model)):
            if y <= year_cmip6[-1]:
                scn = 'historical'
            else:
                scn = 'ssp585'
            data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 
                                                'Interp_Merge', land_mask, 
                                                'CMIP6', l, 'mrsol_' + scn + \
                                                '_' + str(y) + '_' + dcm + \
                                                '.nc'), decode_times = False)
            collection[:, :, :, j] = data.sm.values.copy()
            data.close()

        #
        if operation == 'mean':
            model_merged = np.nanmean(collection, axis = 3)
        else:
            model_merged = np.nanmedian(collection, axis = 3)

        del collection # free up memory

        #
        collect_years[(12 * (y - year[0])):(12 * (y - year[0] + 1)),
                      :, :] = model_merged

    #
    collect_year2 = xr.DataArray(collect_years, 
                                 coords = {'time': period, 'lat': target_lat,
                                           'lon': target_lon},
                                 dims = ['time', 'lat', 'lon'])
    del collect_years # free up memory

    #
    collect_year2.attrs['Source Models'] = ' '.join(lsm_model + cmip5_model + \
                                                    cmip6_model)

    collect_year2.to_dataset(name='sm').to_netcdf( \
        os.path.join(mg.path_out(), 'meanmedian_all',
                     operation + '_' + d + '_' + str(year[0]) + '-' + \
                     str(year[-1]) + '.nc'))


p = mp.Pool(min(mp.cpu_count() - 1, 16))
p.map_async(calc, list(it.product(['mean', 'median'], year_list, range(4))))
p.close()
p.join()
