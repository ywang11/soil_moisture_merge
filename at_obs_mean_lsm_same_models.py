# -*- coding: utf-8 -*-
"""
Created on Thu May 16 19:19:40 2019

@author: ywang254

Extract the soil moisture from the unweighted mean of the CMIP5 models.
"""
import utils_management as mg
from utils_management.constants import depth, depth_cm, year_longest
import os
import xarray as xr
from misc.ismn_utils import get_weighted_monthly_data, \
    get_ismn_aggr_method
from misc.dolce_utils import get_weighted_sm_points
from glob import glob
import pandas as pd
import time
import itertools as it


start = time.time()


year = year_longest


simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = [get_ismn_aggr_method(simple[ind], 
                                         dominance_lc[ind], 
                                         dominance_threshold) for \
                    ind in range(len(simple))]


for i, iam in it.product(range(len(depth)), ismn_aggr_method):

    dcm = depth_cm[i]
    d = depth[i]

    grid_latlon, _, _ = get_weighted_monthly_data(os.path.join( \
        mg.path_intrim_out(), 'ismn_aggr'), iam, d, 'all')

    f = os.path.join(mg.path_out(), 'meanmedian_lsm',
                     'same_models_average_' + d + '_' + str(year[0]) + '-' + \
                     str(year[-1]) + '.nc')
    data = xr.open_dataset(f, decode_times=False)
    sm = data.sm.copy(deep=True)
    data.close()

    # ---- obtain values at the observed data points (do not subset
    #      the dates).
    sm_at_data = get_weighted_sm_points(grid_latlon, sm, year)
    sm_at_data.to_csv(os.path.join(mg.path_out(), 'at_obs_meanmedian_lsm', \
                                   'same_models_' + iam + '_' + d + '_' + \
                                   str(year[0]) + '-' + str(year[-1]) + \
                                   '.csv'))

end = time.time()
print('The script finished in ' + ('%.4f' % ((end-start)/3600) ) + ' hours.')
