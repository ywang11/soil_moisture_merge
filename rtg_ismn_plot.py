import utils_management as mg
import os
import itertools as it
import pandas as pd
import numpy as np
from utils_management.constants import *


prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6', 'em_2cmip',
             'em_all']
time_ranges = [pd.date_range('1970-01-01', '1980-12-31', freq = 'MS'),
               pd.date_range('1981-01-01', '2010-12-31', freq = 'MS'),
               pd.date_range('2011-01-01', '2016-12-31', freq = 'MS')]


wx_sig = pd.DataFrame(np.nan,
                      index = pd.MultiIndex.from_product([prod_list, depth_cm]),
                      columns = pd.MultiIndex.from_product([['ntotal', 'percentage'],
                                                            range(1,len(time_ranges))]))
fl_sig = pd.DataFrame(np.nan,
                      index = pd.MultiIndex.from_product([prod_list, depth_cm]),
                      columns = pd.MultiIndex.from_product([['ntotal', 'percentage'],
                                                            range(1,len(time_ranges))]))
for prod, dcm in it.product(prod_list, depth_cm):
    data = pd.read_csv(os.path.join(mg.path_out(), 'homogeneity_test',
                                    'pval_' + dcm + '_' + prod + '.csv'),
                       index_col = 0, header = [0,1])
    for period in range(1, len(time_ranges)):
        wx_sig.loc[(prod, dcm), ('ntotal', period)] = \
            (~np.isnan(data.loc[:, ('wilcoxon', period)])).sum()
        wx_sig.loc[(prod, dcm), ('percentage', period)] = \
            np.nanmean(data.loc[:, ('wilcoxon', period)].values <= 0.05)
        fl_sig.loc[(prod, dcm), ('ntotal', period)] = \
            (~np.isnan(data.loc[:, ('fligner', period)])).sum()
        fl_sig.loc[(prod, dcm), ('percentage', period)] = \
            np.nanmean(data.loc[:, ('fligner', period)].values <= 0.05)

wx_sig.to_csv(os.path.join(mg.path_out(), 'homogeneity_test', 'wx_sig.csv'))
fl_sig.to_csv(os.path.join(mg.path_out(), 'homogeneity_test', 'fl_sig.csv'))
