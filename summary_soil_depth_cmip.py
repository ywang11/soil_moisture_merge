"""
Extract the soil depth variable from the CMIP5 and CMIP6 models.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import year_longest, year_cmip5, depth_cm
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import os
import pandas as pd
from glob import glob
import numpy as np


land_mask = 'vanilla'


#########
# CMIP5
#########
cmip5_depth_list = {}

cmip5 = cmip5_availability('0-10cm', land_mask)
for model in cmip5:
    flist = glob(os.path.join(mg.path_data(), 'CMIP5', 'historical',
                              'mrlsl', '*_' + model + '_*.nc'))

    hr = xr.open_dataset(flist[0])

    # Convert to cm
    depth_bnds = hr['depth_bnds'].values * 100

    dbc = []
    for i in range(depth_bnds.shape[0]):
        a = ('%.3f' % depth_bnds[i,0]).rstrip('0')
        if a[-1] == '.':
            a = a[:-1]
        b = ('%.3f' % depth_bnds[i,1]).rstrip('0')
        if b[-1] == '.':
            b = b[:-1]

        dbc.append(a + '-' + b + 'cm')
    cmip5_depth_list[model] = dbc
    hr.close()


#########
# CMIP6
#########
cmip6_depth_list = {}

cmip6_list_1 = mrsol_availability('0-10cm', land_mask, 'historical')
cmip6_list_2 = mrsol_availability('0-10cm', land_mask, 'ssp585')
cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) )
cmip6 = [x.split('_')[0] for x in cmip6_list if 'r1i1p1f1' in x]

for model in cmip6:
    flist = glob(os.path.join(mg.path_data(), 'CMIP6', 'historical',
                              'mrsol', model, '*_' + model + '_*.nc'))

    hr = xr.open_dataset(flist[0])

    # Convert to cm
    if (model == 'CNRM-CM6-1') | (model == 'CNRM-ESM2-1'):
        depth_bnds = hr['sdepth_bnds'].values * 100
    elif (model == 'IPSL-CM6A-LR'):
        depth_bnds = np.array([[0., 0.001], [0.001, 0.004], [0.004, 0.01], \
                               [0.01, 0.022], [0.022, 0.045], [0.045, 0.092], \
                               [0.092, 0.186], [0.186, 0.374], [0.374, 0.749],\
                               [0.749, 1.499], [1.499, 1.999]]) * 100
    else:
        depth_bnds = hr['depth_bnds'].values * 100

    dbc = []
    for i in range(depth_bnds.shape[0]):
        a = ('%.3f' % depth_bnds[i,0]).rstrip('0')
        if a[-1] == '.':
            a = a[:-1]
        b = ('%.3f' % depth_bnds[i,1]).rstrip('0')
        if b[-1] == '.':
            b = b[:-1]

        dbc.append(a + '-' + b + 'cm')
    cmip6_depth_list[model] = dbc
    hr.close()

#########
# Print
#########
print(cmip5_depth_list)
print(cmip6_depth_list)
