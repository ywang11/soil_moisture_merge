"""
20190920
ywang254@utk.edu

Illustrate the emergent constraint method
"""
import numpy as np
import matplotlib.pyplot as plt
import utils_management as mg
import os
import statsmodels.api as sm
import math

np.random.seed(9999)


x = np.random.rand(8) * 10
y = x * 3 + np.random.rand(8) * 9


fig, ax = plt.subplots(figsize = (4, 4))

ax.plot(x, y, 'o', markersize = 10)

model = sm.OLS(y, sm.add_constant(x))
results = model.fit()
ax.plot([0,12], results.params[0] + \
                np.array([0,12]) * results.params[1], '-b')

ax.axvline(6.5, 0, 1, linewidth = 5, color = 'k')

ax.hlines(results.params[0] + 6.5 * results.params[1],
          0, 6.5, color = 'grey', linestyle = ':', linewidth = 3)

ax.set_xlabel('Emergent Constraint')
ax.set_ylabel('Unobservable Variable')

ax.text(7, 15.5, 'Observed EC', color = 'k',
        fontdict = {'fontweight': 'bold'})
ax.text(1, 27, 'Constrained \nUnobservable \nVariable', color = 'grey', 
        fontdict = {'fontweight': 'bold'})
ax.text(7, 22, 'Emergent \nRelationship', color = 'b', 
        rotation = math.atan(0.6) / math.pi * 180)


ax.set_xlim([0., 11])

fig.savefig(os.path.join(mg.path_out(), 'illustrate_ec.png'), dpi = 600.,
            bbox_inches = 'tight')
