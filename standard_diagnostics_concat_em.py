"""
20190925
ywang254@utk.edu

Compute the following for the merged emergent constraint product.

1. Global map (averaged over time): NetCDF
2. Global average time series.
3. Latitude bands average time series.
4. Continental average time series.
5. SREX region average time series.
"""
import xarray as xr
import utils_management as mg
from misc.standard_diagnostics import standard_diagnostics
import os
import pandas as pd
from utils_management.constants import year_longest, depth, depth_cm


me = 'year_month_anomaly_9grid'

i = REPLACE1 # [0,1,2,3]
d = depth[i]
dcm = depth_cm[i]

met_obs = 'CRU_v4.03'

no_merge = REPLACE2 # True, False

prod = 'REPLACE3'

iam = 'lu_weighted' # keep this fixed

year = year_longest


for prefix in ['predicted']:
    if no_merge:
        fi = os.path.join(mg.path_out(), 'concat_no_merge_em_' + prod,
                          'concat_' + met_obs + '_' + me.replace('anomaly',
                                                                 'positive') \
                          + '_' + dcm + '_' + prefix + '.nc')
    else:
        fi = os.path.join(mg.path_out(), 'concat_em_' + prod,
                          'positive_' + met_obs + '_' + me + '_' + dcm + \
                          '_' + prefix + '_' + str(year[0]) + '-' + \
                          str(year[-1]) + '.nc')

    data = xr.open_dataset(fi, decode_times=False)

    time = pd.date_range(str(year[0])+'-01-01', str(year[-1])+'-12-31',
                         freq = 'MS')

    # year_depth should always be together
    if no_merge:
        standard_diagnostics(data[prefix].values, time, data.lat, data.lon, 
                             os.path.join(mg.path_out(), 
                                          'standard_diagnostics_concat',
                                          'no_merge_em_' + prod),
                             'no_merge_' + met_obs + '_' + me + '_' + \
                             prefix + '_' + d)
    else:
        standard_diagnostics(data[prefix].values, time, data.lat, data.lon, 
                             os.path.join(mg.path_out(), 
                                          'standard_diagnostics_concat',
                                          'em_' + prod),
                             met_obs + '_' + me + '_' + prefix + '_' + d)
    data.close()
