"""
20190923
ywang254@utk.edu

Step 4 of the CDF-matching merging:
 Add back the seasonality.
"""
import xarray as xr
import os
import pandas as pd
import numpy as np
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth, target_lat, target_lon
import utils_management as mg
from misc.ismn_utils import get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
from misc.concat_utils import piecewise_cdf_create, piecewise_cdf_apply
import itertools as it
import multiprocessing as mp


simple = [True, False, False]
lu_weighted = [False, False, True]
lu_threshold = 40
ismn_aggr_method = [get_ismn_aggr_method(simple[i], lu_weighted[i],
                                         lu_threshold) for i in range(3)]
cov_method = [get_cov_method(i) for i in range(5)]
prod_list = ['lsm', 'all']


args_list = []
for d, iam, cov, prod in it.product(depth, ismn_aggr_method, cov_method,
                                    prod_list):
    args_list.append([d, iam, cov, prod])


def restore(args):
    d, iam, cov, prod = args

    for year in [year_shorter, year_shortest]:
        data = xr.open_dataset(os.path.join(mg.path_out(),
                                            'concat_dolce_' + prod,
                                            'scaled_' + str(year[0]) + '-' + \
                                            str(year[-1]) + '_' + d + '_' + \
                                            iam + '_' + cov + '.nc'),
                               decode_times = False)
        data['time'] = pd.date_range(str(year[0]) + '-01-01',
                                     str(year[-1]) + '-12-31', freq = 'MS')

        # Use the reference dataset, i.e. 1981-2016 climatology.
        data2 = xr.open_dataset(os.path.join(mg.path_out(),
                                             'concat_dolce_' + prod,
                                             'climatology_1981-2016_' + \
                                             d + '_' + iam + '_' + cov + \
                                             '.nc'),
                                decode_times = False)

        print(str(year[0]) + '-' + str(year[-1]))

        for m in range(1,13):
            data.sm.loc[data.time.indexes['time'].month == m,
                        :, :] += data2.sm.loc[m, :, :]

        data2.close()
        data.to_netcdf(os.path.join(mg.path_out(), 'concat_dolce_' + prod,
                                    'restored_' + str(year[0]) + '-' + \
                                    str(year[-1]) + '_' + d + '_' + \
                                    iam + '_' + cov + '.nc'))

pool = mp.Pool(4)
pool.map_async(restore, args_list)
pool.close()
pool.join()

#restore(args_list[0])
