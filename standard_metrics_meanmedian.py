# -*- coding: utf-8 -*-
"""
Created on Wed May  1 21:04:14 2019

@author: ywang254

Calculate the RMSEs between between the mean and median soil moisture results,
and the observed soil moisture. 
"""
import os
import utils_management as mg
import pandas as pd
from utils_management.constants import depth, depth_cm, \
    year_longest, year_shorter, year_shorter2, year_shortest
from misc.ismn_utils import get_ismn_aggr_method, get_weighted_monthly_data, \
    get_weighted_monthly_data_USonly
from misc.analyze_utils import calc_stats
import numpy as np
import itertools as it
import time


prefix = ['mean', 'median']


# List the weighting methods that are used to generate the weighted
# averaged ISMN data.
simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = [get_ismn_aggr_method(simple[i], dominance_lc[i], 
                                         dominance_threshold) for i in \
                    range(len(simple))]


start = time.time()
for prod, i, USonly in it.product(['lsm', 'cmip5', 'cmip6', '2cmip', 'all'],
                                  range(len(depth)), [True, False]):

    # Select the time range to calculate the RMSE between observation and land
    # surface models.
    if (prod == 'lsm') | (prod == 'all'):
        year_list = [year_longest, year_shorter, year_shorter2, year_shortest]
    else:
        year_list = [year_longest]

    time_range_list = []
    for year in year_list:
        time_range_list.append(pd.date_range(start=str(year[0])+'-01-01',
                                             end=str(year[-1])+'-12-31',
                                             freq='MS'))

    for year in year_list:
        d = depth[i]
        dcm = depth_cm[i]
    
        # Collector for the calculated metrics in time and space.
        metrics_collect = pd.DataFrame(data = np.nan,
            index = pd.MultiIndex.from_product([ismn_aggr_method, prefix], 
                                               names = ['ISMN Aggr', 'Kind']),
            columns = pd.MultiIndex.from_product([['RMSE','Bias','uRMSE',
                                                   'Corr'],
                                                  ['all','cal','val']],
                                                 names = ['Metric',
                                                          'ISMN Set']))
    
        for iam, kind, ismn_set in it.product(ismn_aggr_method, prefix,
                                              ['all', 'cal', 'val']):
            ###################################################################
            # Get the observed data.
            ###################################################################
            if USonly:
                grid_latlon, weighted_monthly_data, _ = \
                    get_weighted_monthly_data_USonly( \
                        os.path.join(mg.path_intrim_out(),
                                     'ismn_aggr'), iam, d, ismn_set)
            else:
                grid_latlon, weighted_monthly_data, _ = \
                    get_weighted_monthly_data(os.path.join( \
                        mg.path_intrim_out(), 'ismn_aggr'), iam, d, 
                                              ismn_set)
    
            # ---- subset to the same time period.
            weighted_monthly_data = weighted_monthly_data.loc[ \
                (weighted_monthly_data.index >= pd.to_datetime(str(year[0]) +\
                                                               '-01-01')) & \
                (weighted_monthly_data.index <= pd.to_datetime(str(year[-1]) +\
                                                               '-12-31')), :]
    
            ###################################################################
            # Performance of the weighted product.
            ###################################################################
            data = pd.read_csv(os.path.join(mg.path_out(),
                                            'at_obs_meanmedian_' + prod,
                                            kind + '_' + d + '_' + \
                                            str(year[0]) + '-' + \
                                            str(year[-1]) + '_' + iam + \
                                            '.csv'),
                               index_col = 0, parse_dates = True)
            # ---- obtain values at the observed data points
            weighted_sm_at_data = data.loc[weighted_monthly_data.index, 
                                           weighted_monthly_data.columns]
    
            metrics_collect.loc[(iam, kind), ('RMSE',ismn_set)], \
                metrics_collect.loc[(iam, kind), ('Bias',ismn_set)], \
                metrics_collect.loc[(iam, kind), ('uRMSE',ismn_set)], \
                metrics_collect.loc[(iam, kind), ('Corr',ismn_set)] = \
                calc_stats(weighted_sm_at_data.values.reshape(-1), 
                           weighted_monthly_data.values.reshape(-1))
    
        if USonly:
            metrics_collect.to_csv(os.path.join(mg.path_out(),
                                                'standard_metrics', 
                                                'meanmedian_' + prod + '_' + \
                                                d + '_' + str(year[0]) + \
                                                '-' + str(year[-1]) + \
                                                '_USonly.csv'))
        else:
            metrics_collect.to_csv(os.path.join(mg.path_out(),
                                                'standard_metrics', 
                                                'meanmedian_' + prod + '_' + \
                                                d + '_' + str(year[0]) + \
                                                '-' + str(year[-1]) + '.csv'))
end = time.time()
# The script finished in 0.0288 hours.
print('The script finished in ' + ('%.4f' % ((end-start)/3600) ) + ' hours.')
