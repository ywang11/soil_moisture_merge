"""
2019/07/03

Create 0.5 deg NetCDF file of global countries, and then continents. 
"""
import regionmask
import xarray as xr
import numpy as np
import sys
import os

if sys.platform == 'linux':
    os.environ['QT_QPA_PLATFORM']='offscreen'
import matplotlib.pyplot as plt
plt.switch_backend('agg') # solve the display problem

import time
import utils_management as mg
import pandas as pd


## define resolution in degree
lon_res = 0.5 ## 1.875
lat_res = 0.5 ## 1.875

## specify resolutions
##lon = np.arange(-180, 180, lon_res)
##lat = np.arange(-90, 90, lat_res)
lon = np.arange(-179.75, 179.76, lon_res)
lat = np.arange(-89.75, 89.76, lat_res)


###############################################################################
# Country level
###############################################################################
## create dataset
ds = xr.Dataset(coords={'lon': lon, 'lat': lat,})

## define the srex regions
region = regionmask.defined_regions.natural_earth.countries_110.mask(ds)

region[:,:] = np.where(np.isnan(region), -9999, region)

region = region.astype(int)
region.attrs = {'_FillValue': -9999}

ds = region.to_dataset()

## attach attribute information
abbrevs = regionmask.defined_regions.natural_earth.countries_110.abbrevs
names = regionmask.defined_regions.natural_earth.countries_110.names
numbers = regionmask.defined_regions.natural_earth.countries_110.numbers

description = 'Region ID - Abbrevation - Longname; '

for i in range(len(numbers)):
    description += str(numbers[i]) + ' - ' + str(abbrevs[i]) + ' - ' + \
                   str(names[i]) + '; '

ds.attrs['title'] = 'Natural Earth Countries Mask'
ds.attrs['history'] = 'Created on ' + time.ctime()
ds.attrs['creator_email'] = 'ywang254@utk.edu'

ds.attrs['projection'] = 'lonlat'
ds.attrs['geospatial_lat_resolution'] = str(lat_res)+' deg'
ds.attrs['geospatial_lon_resolution'] = str(lon_res)+' deg'
ds.attrs['geospatial_lat_min'] = -89.75
ds.attrs['geospatial_lat_max'] = 89.75
ds.attrs['geospatial_lon_min'] = -179.75
ds.attrs['geospatial_lon_max'] = 179.75

ds.attrs['comment'] = description

ds.to_netcdf(os.path.join(mg.path_intrim_out(), 
                          'natural_earth_countries_mask.nc'))

## make plot showing different regions
plt.figure()
regionmask.defined_regions.natural_earth.countries_110.plot(label=False)
plt.title('Global Countries')


###############################################################################
# Continent level
###############################################################################
mapper = pd.read_csv(os.path.join(mg.path_data(), 
                                  'natural_earth_continent.csv'))


region = -9999 * np.ones([len(lat), len(lon)], dtype=int)

for ind,p in mapper.iterrows():
    region[ds['region'].values == int(p['Number'])] = \
        int(p['Continent_Number'])


ds2 = xr.DataArray(data = region,
                   dims = ['lat', 'lon'],
                   coords={'lon': lon, 'lat': lat,},
                   attrs = {'_FillValue': -9999}).to_dataset(name='region')


description = 'Continent ID - Abbrevation; '
continents = ['AF', 'AN', 'AS', 'EU', 'NA', 'OC', 'SA']

for i in range(1, len(continents)+1):
    description += str(i) + ' - ' + str(continents[i-1]) + '; '

ds2.attrs['title'] = 'Natural Earth Countries Mask'
ds2.attrs['history'] = 'Created on ' + time.ctime()
ds2.attrs['creator_email'] = 'ywang254@utk.edu'

ds2.attrs['projection'] = 'lonlat'
ds2.attrs['geospatial_lat_resolution'] = str(lat_res)+' deg'
ds2.attrs['geospatial_lon_resolution'] = str(lon_res)+' deg'
ds2.attrs['geospatial_lat_min'] = -89.75
ds2.attrs['geospatial_lat_max'] = 89.75
ds2.attrs['geospatial_lon_min'] = -179.75
ds2.attrs['geospatial_lon_max'] = 179.75

ds2.attrs['comment'] = description


ds2.to_netcdf(os.path.join(mg.path_intrim_out(), 
                           'natural_earth_continent_mask.nc'))
