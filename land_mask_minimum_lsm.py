"""
2019/07/14

ywang254@utk.edu

Calculate the common land mask of the interpolated land surface models
and their corresponding precipitation dataset after their interpolation to
0.5oC.
"""
import sys
import os
import xarray as xr
import numpy as np
import pandas as pd
from glob import glob
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import utils_management as mg


path_merge = mg.path_intrim_out() + '/Interp_Merge/'


lsm_all = ['CERA20C', 'CLASS-CTEM-N', 'CLM4', 'CLM4VIC',
           'CLM5.0', 'ERA20C', 'ERA-Interim', 'ERA5', 'ERA-Land', ##'ESA-CCI',
           'GLDAS_Noah2.0', 'GLEAMv3.3a', 'ISAM','JULES', 'JSBACH', 
           'LPX-Bern', 'ORCHIDEE', 'ORCHIDEE-CNP', 'SiBCASA']
pr_all  = ['CRU_v4.03', 'CRU_v3.20', 'CRU_v3.26', 'CERA20C_pr',
           'ERA20C_pr', 'ERA-Interim_pr', 'ERA5_pr', 'GLDAS_Noah2.0_pr']
tas_all  = ['CRU_v4.03', 'CRU_v3.20', 'CRU_v3.26', 'CERA20C_tas',
            'ERA20C_tas', 'ERA-Interim_tas', 'ERA5_tas', 'GLDAS_Noah2.0_tas']


target_lat = np.arange(-89.75, 89.76, 0.5)
target_lon = np.arange(-179.75, 179.76, 0.5)


mask_all = np.zeros([len(lsm_all) + len(pr_all) + len(tas_all), 360, 720],
                    dtype = int)
for model_ind, model in enumerate(lsm_all):
    path_model = os.path.join(path_merge, 'None', model)
    file_list = glob(path_model + '/' + model + '*.nc')
    f = os.path.join(os.path.join(path_model, file_list[0]))
    data = xr.open_dataset(f, decode_times = False)
    mask_all[model_ind, :, :] = ~np.isnan(data.sm[0,:,:])
    data.close()
for model_ind, model in enumerate(pr_all, len(lsm_all)):
    path_model = os.path.join(path_merge, 'None', model)
    if 'CRU' in model:
        file_list = glob(path_model + '/pr_*.nc')
    else:
        file_list = glob(path_model + '/' + model + '*.nc')
    f = os.path.join(os.path.join(path_model, file_list[0]))
    data = xr.open_dataset(f, decode_times = False)
    mask_all[model_ind, :, :] = ~np.isnan(data.pr[0,:,:])
    data.close()
for model_ind, model in enumerate(tas_all, len(lsm_all) + len(pr_all)):
    path_model = os.path.join(path_merge, 'None', model)
    if 'CRU' in model:
        file_list = glob(path_model + '/tas*.nc')
    else:
        file_list = glob(path_model + '/' + model + '*.nc')
    f = os.path.join(os.path.join(path_model, file_list[0]))
    data = xr.open_dataset(f, decode_times = False)
    mask_all[model_ind, :, :] = ~np.isnan(data.tas[0,:,:])
    data.close()

# Find the minimum mask, i.e. where all the mask == 1
mask_check = np.all(mask_all == 1, axis=0)


# Save the minimum mask to file.
mask_check = xr.DataArray(mask_check, dims = ['lat', 'lon'], 
                          coords = {'lat': target_lat, 'lon': target_lon})
mask_check.to_dataset(name = 'mask' \
                  ).to_netcdf(os.path.join(mg.path_intrim_out(), 'land_mask_minimum', 'lsm.nc'))


# Accompanying graph.
fig, ax = plt.subplots(figsize = (12, 12), 
                       subplot_kw = {'projection': ccrs.PlateCarree()})
ax.coastlines(color = 'red')
ax.gridlines(draw_labels = True)
mask_check.plot(cmap = 'Greys')
fig.savefig(os.path.join(mg.path_intrim_out(), 'land_mask_minimum', 
                         'lsm.png'), bbox_inches = 'tight',
            dpi = 600.)
plt.close(fig)
