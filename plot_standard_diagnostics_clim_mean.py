"""
2020/12/25
ywang254@utk.edu

Calculate the global mean soil moisture of each soil layer for each dataset.
"""
from utils_management.constants import lsm_all, year_longest, year_shorter, \
    year_shorter2, year_shortest, depth, cmip5, depth_cm
import utils_management as mg
from misc.plot_standard_diagnostics_utils import plot_map
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import numpy as np
import os
import itertools as it
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib as mpl
from misc.plot_utils import heatmap, annotate_heatmap


def get_cmip6():
    land_mask = 'vanilla'
    cmip6 = []
    for dcm in depth_cm:
        # ---- the models that have precipitation.
        cmip6_list_1 = mrsol_availability(dcm, land_mask, 'historical')
        cmip6_list_2 = mrsol_availability(dcm, land_mask, 'ssp585')
        cmip6_list_3 = one_layer_availability('pr', land_mask, 'historical')
        cmip6_list_4 = one_layer_availability('pr', land_mask, 'ssp585')
        # ---- further subset to the given depth.
        cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) & \
                           set(cmip6_list_3) & set(cmip6_list_4) )
        # ---- further subset to the first ensemble member.
        cmip6_list = [x for x in cmip6_list if 'r1i1p1f1' in x]
        cmip6.extend(cmip6_list)
    cmip6 = list(np.unique(cmip6))
    return cmip6


def avg_clim(fpath):
    if os.path.exists(fpath):
        hr = xr.open_dataset(fpath)
        avg = np.nanmean(hr['g_map'].values)
        hr.close()
        return avg
    else:
        return np.nan


def im_annotated(ax, df):
    im = ax.imshow(df.values, aspect = 0.5, cmap = 'bone_r')
    ax.set_xticks(np.arange(df.shape[1]))
    ax.set_yticks(np.arange(df.shape[0]))
    ax.set_xticklabels(df.columns)
    ax.set_yticklabels(df.index)
    plt.setp(ax.get_xticklabels(), rotation=90, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    for i in range(df.shape[0]):
        for j in range(df.shape[1]):
            text = ax.text(j, i, '%.3f' % df.values[i, j],
                           ha="center", va="center", color="w")
    return im, ax


#
cmip6 = get_cmip6()


create_data = True
if create_data:
    data = pd.DataFrame(np.nan, index = lsm_all + cmip5 + cmip6,
                        columns = depth_cm)
    for dind, d in enumerate(depth):
        dcm = depth_cm[dind]
    
        for l in lsm_all:
            data.loc[l, dcm] = avg_clim(os.path.join(mg.path_out(),
                                        'standard_diagnostics_lsm',
                                                     l + '_' + d + \
                                                     '_g_map_annual.nc'))
        
        for l in cmip5:
            data.loc[l, dcm] = avg_clim(os.path.join(mg.path_out(),
                                        'standard_diagnostics_cmip5',
                                                     l + '_' + dcm + \
                                                     '_g_map_annual.nc'))
        
        for l in cmip6:
            data.loc[l, dcm] = avg_clim(os.path.join(mg.path_out(),
                                        'standard_diagnostics_cmip6',
                                                     'mrsol_' + l + '_' + \
                                                     dcm + \
                                                     '_g_map_annual.nc'))
    data.to_csv(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                             'clim_mean.csv'))
else:
    data = pd.read_csv(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                                    'clim_mean.csv'),
                       index_col = 0)


#
lsm_data = data.loc[sorted(lsm_all), :]
cmip5_data = data.loc[sorted(cmip5), :]
cmip6_data = data.loc[sorted(cmip6), :]
cmip6_data.index = [x.replace('_r1i1p1f1', '') for x in cmip6_data.index]

mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 6
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'

fig = plt.figure(figsize = (6.5, 5))
axes = np.empty(3, dtype = object)
axes[0] = fig.add_axes([0., 0.667, 1., 0.33])
axes[1] = fig.add_axes([0., 0.333, 1., 0.33])
axes[2] = fig.add_axes([0., 0., .4737, 0.33])

for ind, df in enumerate([lsm_data, cmip5_data, cmip6_data]):
    ax = axes[ind]

    im = heatmap(df.values.T, df.columns, df.index, ax=ax,
                 cbar=False, cmap="YlGn",
                 cbarlabel='', aspect = 0.5,
                 vmin = 0., vmax = 0.5)
    texts = annotate_heatmap(im, df.values.T, valfmt="{x:.3f}",
                             threshold = 0.4)

    if ind == 0:
        ax.set_title('(' + lab[ind] + ') ORS')
    elif ind == 1:
        ax.set_title('(' + lab[ind] + ') CMIP5')
    else:
        ax.set_title('(' + lab[ind] + ') CMIP6')
cax = fig.add_axes([0.5, 0.2, 0.5, 0.01])
plt.colorbar(im, cax = cax, orientation = 'horizontal')
fig.savefig(os.path.join(mg.path_out(),
                         'standard_diagnostics_plot',
                         'clim_mean.png'), dpi = 600.,
            bbox_inches = 'tight')
plt.close(fig)
