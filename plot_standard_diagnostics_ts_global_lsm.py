"""
2019/08/05
ywang254@utk.edu

Plot the time series of global mean annual mean soil moisture of the 
 land surface models.
"""
import matplotlib.pyplot as plt
import utils_management as mg
from utils_management.constants import year_longest, depth, depth_cm, \
    lsm_all, lsm_reanalysis, lsm_satellite, lsm_offline, lsm_mstmip, lsm_trendy
from misc.plot_utils import plot_ts_shade, plot_ts_trend
from misc.standard_diagnostics import seasonal_avg
import pandas as pd
import os
import numpy as np
import itertools as it
import multiprocessing as mp
from matplotlib.cm import get_cmap


year = year_longest

cmap = get_cmap('nipy_spectral')

#
global_ts_all_depth = {}
for i,d in enumerate(depth):
    dcm = depth_cm[i]
    for l in lsm_all:
        fname = os.path.join(mg.path_out(), 'standard_diagnostics_lsm',
                             l + '_' + d + '_g_ts.csv')
        if not os.path.exists(fname):
            continue
        temp = pd.read_csv(fname, index_col = 0, parse_dates = True).iloc[:,0]
        global_ts_all_depth[(d, l)] = seasonal_avg(temp)


#
def plotter(option):
    subset, subset_name = option
    clist = [cmap((ind+0.5)/len(subset)) for ind in range(len(subset))]

    for s in ['annual', 'DJF', 'MAM', 'JJA', 'SON']:
        fig, axes = plt.subplots(nrows = len(depth_cm), ncols = 2, 
                                 sharex = True, 
                                 sharey = False, figsize = (10, 6.5))
        fig.subplots_adjust(hspace = 0., wspace = 0.2)

        # (1) Soil Moisture
        for i,d in enumerate(depth):
            dcm = depth_cm[i]

            ax = axes[i, 0]
            h = [None] * len(subset)
            for ind, l in enumerate(subset):
                if not (d,l) in global_ts_all_depth.keys():
                    continue

                h[ind], = ax.plot(global_ts_all_depth[(d,l)].index, 
                                  global_ts_all_depth[(d,l)].loc[:, s], 
                                  color = clist[ind])

            if (i == 0):
                ax.set_title('Soil Moisture (m$^3$/m$^3$)')

            ax.set_xlim([year[0], year[-1]])
            ax.set_xlabel('Year')
            ax.set_ylabel(dcm)
            ax.set_ylim([0., 0.5])
            ax.set_yticks(np.linspace(0.1, 0.4, 5))

            if i == 0:
                ax.legend(h, subset, loc = [0.3, -4],
                          ncol = min(4, len(subset)))

        # (2) Soil Moisture Anomaly
        for i,d in enumerate(depth):
            dcm = depth_cm[i]

            ax = axes[i, 1]
            h = [None] * len(subset)
            for ind, l in enumerate(subset):
                if not (d,l) in global_ts_all_depth.keys():
                    continue
                h[ind], = ax.plot(global_ts_all_depth[(d,l)].index, 
                                  global_ts_all_depth[(d,l)].loc[:, s] - \
                                  global_ts_all_depth[(d,l)].loc[:, 
                                                                 s].mean(), 
                                  color = clist[ind])

            if (i == 0):
                ax.set_title('Anomaly (m$^3$/m$^3$)')

            ax.set_xlim([year[0], year[-1]])
            ax.set_xlabel('Year')
            ax.set_ylim([-0.01, 0.01])
            ax.set_yticks(np.linspace(-0.008, 0.008, 5))

        fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot', 
                                 'ts_global', 'lsm', s + '_' + \
                                 subset_name + '.png'),
                    dpi = 600., bbox_inches = 'tight')
        plt.close(fig)


#pool = mp.Pool(mp.cpu_count() - 2)
#pool.map_async(plotter, lsm_all)
#pool.close()
#pool.join()

plotter([lsm_reanalysis, 'lsm_reanalysis'])
plotter([['GLEAMv3.3a'], 'GLEAMv3.3a'])
plotter([lsm_mstmip, 'lsm_mstmip'])
plotter([lsm_trendy, 'lsm_trendy'])
