# -*- coding: utf-8 -*-
"""
Created on Wed May  1 21:04:14 2019

@author: ywang254

Calculate the metrics between between the individual land surface models, and
the observed soil moisture.
"""
import os
import utils_management as mg
import pandas as pd
from utils_management.constants import depth, depth_cm
from misc.ismn_utils import get_ismn_aggr_method, get_weighted_monthly_data, \
    get_weighted_monthly_data_USonly
from misc.analyze_utils import calc_stats
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import numpy as np
import itertools as it
import time


# MODIFY
# Whether the metric is limited to the performance in CONUS.
USonly = True


# List the weighting methods that are used to generate the weighted
# averaged ISMN data.
simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = [get_ismn_aggr_method(simple[i], dominance_lc[i], 
                                         dominance_threshold) for i in \
                    range(len(simple))]

land_mask = 'vanilla'

start = time.time()
for i,d in enumerate(depth):
    dcm = depth_cm[i]

    # Check existence.
    # ---- the models that have precipitation.
    cmip6_list_1 = mrsol_availability(dcm, land_mask, 'historical')
    cmip6_list_2 = mrsol_availability(dcm, land_mask, 'ssp585')
    # ---- further subset to the given depth.
    cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) )
    # ---- further subset to the first ensemble member.
    cmip6_list = [x for x in cmip6_list if 'r1i1p1f1' in x]

    # Collector for the calculated metrics in time and space.
    metrics_collect = pd.DataFrame(data = np.nan,
        index = pd.MultiIndex.from_product([ismn_aggr_method, cmip6_list], 
                                           names = ['ISMN Aggr', 'CMIP6']),
        columns = pd.MultiIndex.from_product([['RMSE','Bias','uRMSE','Corr'],
                                              ['all','cal','val']],
                                             names = ['Metric', 'ISMN Set']))

    for iam, l, ismn_set in it.product(ismn_aggr_method, cmip6_list,
                                       ['all', 'cal', 'val']):
        #######################################################################
        # Get the observed data.
        #######################################################################
        if USonly:
            grid_latlon, weighted_monthly_data, _ = \
                get_weighted_monthly_data_USonly( \
                    os.path.join(mg.path_intrim_out(),
                                 'ismn_aggr'), iam, d, ismn_set)
        else:
            grid_latlon, weighted_monthly_data, _ = \
                get_weighted_monthly_data(os.path.join(mg.path_intrim_out(), 
                                                       'ismn_aggr'), iam, d, 
                                          ismn_set)

        #######################################################################
        # Performance of the weighted product.
        #######################################################################
        data = pd.read_csv(os.path.join(mg.path_intrim_out(), 
                                        'at_obs_cmip6',
                                        iam + '_' + l + '_' + d + '.csv'), 
                           index_col = 0, parse_dates = True)
        # ---- obtain values at the observed data points
        weighted_sm_at_data = data.loc[weighted_monthly_data.index, 
                                       weighted_monthly_data.columns]

        metrics_collect.loc[(iam, l), ('RMSE',ismn_set)], \
            metrics_collect.loc[(iam, l), ('Bias',ismn_set)], \
            metrics_collect.loc[(iam, l), ('uRMSE',ismn_set)], \
            metrics_collect.loc[(iam, l), ('Corr',ismn_set)] = \
            calc_stats(weighted_sm_at_data.values.reshape(-1), 
                       weighted_monthly_data.values.reshape(-1))

    if USonly:
        metrics_collect.to_csv(os.path.join(mg.path_out(),
                                            'standard_metrics', 
                                            'cmip6_' + d + '_USonly.csv'))
    else:
        metrics_collect.to_csv(os.path.join(mg.path_out(),
                                            'standard_metrics', 
                                            'cmip6_' + d + '.csv'))
end = time.time()
# The script finished in 5.2126 hours.
print('The script finished in ' + ('%.4f' % ((end-start)/3600) ) + ' hours.')
