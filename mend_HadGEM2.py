"""
2019/07/17

Concatenate RCP85 2005/12 to the last year of historical simulation of the
HadGEM2-CC and HadGEM2-ES models, because their historical simulations 
end at 2005/11.
"""
import utils_management as mg
import os
import xarray as xr
import shutil


path_hist = os.path.join(mg.path_data(), 'CMIP5', 'historical')
path_rcp85 = os.path.join(mg.path_data(), 'CMIP5', 'rcp85')


part1 = [os.path.join(path_hist, 'mrlsl',
                      'mrlsl_Lmon_HadGEM2-CC_historical_r1i1p1_' + \
                      '198412-200511.nc'),
         os.path.join(path_hist, 'pr',
                      'pr_Amon_HadGEM2-CC_historical_r1i1p1_198412-200511.nc'),
         os.path.join(path_hist, 'mrlsl',
                      'mrlsl_Lmon_HadGEM2-ES_historical_r1i1p1_' + \
                      '198412-200511.nc'),
         os.path.join(path_hist, 'pr',
                      'pr_Amon_HadGEM2-ES_historical_r1i1p1_198412-200511.nc')]
part2 = [os.path.join(path_rcp85, 'mrlsl',
                      'mrlsl_Lmon_HadGEM2-CC_rcp85_r1i1p1_200512-203011.nc'),
         os.path.join(path_rcp85, 'pr',
                      'pr_Amon_HadGEM2-CC_rcp85_r1i1p1_200512-203011.nc'),
         os.path.join(path_rcp85, 'mrlsl',
                      'mrlsl_Lmon_HadGEM2-ES_rcp85_r1i1p1_200512-203011.nc'),
         os.path.join(path_rcp85, 'pr',
                      'pr_Amon_HadGEM2-ES_rcp85_r1i1p1_200512-203011.nc')]


for p1_ind, p1 in enumerate(part1):
    p2 = part2[p1_ind]

    # Read the parts
    data1 = xr.open_dataset(p1)
    data2 = xr.open_dataset(p2)

    # Concatenate the first month of data2 to data1
    if 'mrlsl' in p1:
        name = 'mrlsl'
        data3 = xr.concat([data1.mrlsl, data2.mrlsl[:1,:,:]], 
                          dim = 'time')
    else:
        name = 'pr'
        data3 = xr.concat([data1.pr, data2.pr[:1,:,:]], 
                          dim = 'time')

    # Ensure that the time coordinate match.
    # Save with force overwrite & CF time
    data3.to_dataset(name = name \
    ).to_netcdf(p1.replace('200511','200512'), mode = 'w',
                unlimited_dims = 'time', 
                encoding = {'time': data1.time.encoding})

    # Remove the suffix of the original file, so that interpolation scripts
    # will not recognize it.
    shutil.move(p1, p1.replace('.nc','_bak'))

    data1.close()
    data2.close()
