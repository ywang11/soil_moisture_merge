# -*- coding: utf-8 -*-
"""
Created on Tue Apr 16 12:59:05 2019

@author: ywang254

Display the number of good monthly soil moisture points in each grid a global
map of 0.5 degrees. If multiple stations exist in the same grid, use the
overlapping time period between them.
"""
import numpy as np
import subprocess
from utils_management.constants import depth, depth_cm, lu_names
import utils_management as mg
import matplotlib as mpl
import os
import pandas as pd
import cartopy
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from matplotlib import cm


lat_grid = np.arange(-89.75, 89.75, 0.5)
lon_grid = np.arange(-179.75, 179.75, 0.5)

lat_parallel = np.arange(-90., 90., 0.5)
lon_parallel = np.arange(-180., 180., 0.5)

n_months = np.zeros([len(lat_grid), len(lon_grid)])

# Use the aggregated soil moisture data to determine the number of months
# that have soil moisture data for calibration and validation.
# Sum all depths.
for i,d in enumerate(depth):
    data = pd.read_csv(os.path.join(mg.path_intrim_out(), 'ismn_aggr',
                                    'weighted_monthly_data_lu_weighted_' + \
                                    d + '.csv'), index_col = 0)
    for ind, p in data.iteritems():
        which_lat = np.argmin(np.abs(lat_grid - p['Lat']))
        which_lon = np.argmin(np.abs(lon_grid - p['Lon']))
        n_months[which_lat,which_lon] += sum(p.iloc[2:] > 0)

n_months[n_months == 0.] = np.nan

##fig, axes = plt.subplots(nrows = 2, ncols = 3, figsize=(12,10), 
##                         subplot_kw = {'projection': \
##                                       cartopy.crs.PlateCarree()})


mpl.rcParams['font.size'] = 6
mpl.rcParams['axes.titlesize'] = 6

lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'

fig = plt.figure(figsize = (6.5, 6.5))
axes = [None] * 8
# 0.33 + 0.67 - (1 - 0.5) * 3/4
axes[0] = fig.add_axes([0., 0.625, 0.5, 0.375],
                       projection = cartopy.crs.PlateCarree())
axes[1] = fig.add_axes([0., 0.276, 0.32, 0.39],
                       projection = cartopy.crs.PlateCarree())
axes[2] = fig.add_axes([0.39, 0.286, 0.3, 0.38],
                       projection = cartopy.crs.PlateCarree())
axes[3] = fig.add_axes([0.57, 0.775, 0.36, 0.172],
                       projection = cartopy.crs.PlateCarree())
axes[4] = fig.add_axes([0.57, 0.645, 0.3, 0.0975],
                       projection = cartopy.crs.PlateCarree())
axes[5] = fig.add_axes([0.7, 0.312, 0.32, 0.277],
                       projection = cartopy.crs.PlateCarree())
axes[6] = fig.add_axes([0., 0.136, 1, 0.125])
axes[7] = fig.add_axes([0., 0., 1, 0.125])
for ax_ind, ax in enumerate(axes[:-2]):
    ax.coastlines(lw = 0.5, color='grey')
    #ax.set_yticks(np.arange(-80., 90., 20))
    #ax.set_xticks(np.arange(-180., 180., 20))

    h = ax.pcolormesh(lon_grid, lat_grid, n_months, vmin=1, vmax=500, 
                      transform=cartopy.crs.PlateCarree(), 
                      cmap=plt.cm.get_cmap('jet'))

    if ax_ind == 0:
        ax.set_extent([-150, -60, 25, 65])
        ax.set_title('North America')
    elif ax_ind == 1:
        ax.set_extent([80, 140, 0., 55])
        ax.set_title('Asia')
    elif ax_ind == 2:
        ax.set_extent([-10, 40, 35, 70])
        ax.set_title('Europe')
    elif ax_ind == 3:
        ax.set_extent([-20, 40, 0, 20])
        ax.set_title('Africa')
    elif ax_ind == 4:
        ax.set_extent([140, 160, -40, -30])
        ax.set_title('Oceania')
    elif ax_ind == 5:
        ax.set_extent([-80, -60, -58, -20])
        ax.set_title('South America')

    gl = ax.gridlines(crs=cartopy.crs.PlateCarree(), linewidth=2,
                      color='black', lw = 0,
                      linestyle='--', draw_labels=True)
    gl.xlabels_top = False
    gl.ylabels_right = False
    if ax_ind == 3:
        gl.xlocator = mticker.FixedLocator(range(-180, 181, 10))
        gl.ylocator = mticker.FixedLocator(range(-90, 91, 10))
    elif ax_ind == 4:
        gl.xlocator = mticker.FixedLocator(range(-180, 181, 5))
        gl.ylocator = mticker.FixedLocator(range(-90, 91, 5))
    elif ax_ind == 5:
        gl.xlocator = mticker.FixedLocator(range(-180, 181, 5))
        gl.ylocator = mticker.FixedLocator(range(-90, 91, 5))
    else:
        gl.xlocator = mticker.FixedLocator(range(-180, 181, 20))
        gl.ylocator = mticker.FixedLocator(range(-90, 91, 10))
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'color': 'k', 'weight': 'normal'}

    ax.text(-0.05, 1.06, '(' + lab[ax_ind] + ')',
            transform = ax.transAxes)
# add colorbar
cbar_ax = fig.add_axes([0.97, 0.35, 0.01, 0.6])
cbar = fig.colorbar(h, cax=cbar_ax)
#cbar.ax.tick_params(labelsize=12)
cbar.set_label('Number of Monthly Observations')


#
ismn_land_use_freq = pd.read_csv(os.path.join(mg.path_intrim_out(),
                                              'ismn_summary',
                                              'ismn_land_use_freq.csv'),
                                 index_col = 0)
data = ismn_land_use_freq.loc[['MODIS'] + ['all_lu_weighted_' + d + \
                                           '_weighted' for d in depth], :]
# ----- drop permanent snow and ice
data.drop('15', axis = 1, inplace = True)
# ----- rename Barren to 15
data.columns = [str(x) for x in range(16)]

cmap = cm.get_cmap('hsv')
clist = [cmap((i+0.5)/(len(depth))) for i in range(len(depth))]
x = np.arange(1, 16)
width = 0.1
# ---- Bottom part
ax = axes[7]
ax.bar(x, data.loc['MODIS', :].values[1:], width, label = 'MODIS',
       fc = [0.5, 0.5, 0.5])
for lind, label in enumerate(data.index[1:], 1):
    # Skip ocean
    ax.bar(x + 1.5 * lind * width, data.loc[label, :].values[1:], width,
           label = depth_cm[lind-1], fc = clist[lind-1])
ax.spines['top'].set_visible(False)
ax.set_ylim([0., 0.08])
ax.set_xticks(np.arange(0.8, 16.8))
ax.set_xticks(np.arange(1.3, 16.3, 1), minor = True)
ax.set_xlim([0.8, 15.8])
xticks = []
for i in range(1,16):
    if i == 15:
        tmp = ['Barren']
    else:
        tmp = lu_names[i].split(' ')
    xticks.append( tmp[0] + '\n' + ' '.join(tmp[1:]) )
ax.set_xticklabels([])
ax.tick_params('x', which = 'minor', length = 0)
ax.set_xticklabels(xticks, rotation = 90, minor = True)
# ---- Top part
ax = axes[6]
ax.bar(x, data.loc['MODIS', :].values[1:], width, label = 'MODIS',
       fc = [0.5, 0.5, 0.5])
for lind, label in enumerate(data.index[1:], 1):
    # Skip ocean
    ax.bar(x + 1.5 * lind * width, data.loc[label, :].values[1:], width,
           label = depth_cm[lind-1], fc = clist[lind-1])
ax.set_ylim([0.25, 0.45])
ax.spines['bottom'].set_visible(False)
ax.set_xticklabels([])
ax.set_xticks([])
ax.set_xlim([0.8, 15.8])
ax.text(0., 1.08, '(' + lab[6] + ')', transform = ax.transAxes)

# ---- Separator
d = .015  # how big to make the diagonal lines in axes coordinates
# arguments to pass to plot, just so we don't keep repeating them
ax = axes[6]
kwargs = dict(transform=ax.transAxes, color='k', clip_on=False, lw = 0.5)
ax.plot((-d, +d), (-d, +d), **kwargs)        # top-left diagonal
ax.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal
ax = axes[7]
kwargs.update(transform=ax.transAxes)  # switch to the bottom axes
ax.plot((-d, +d), (1 - d, 1 + d), **kwargs)  # bottom-left diagonal
ax.plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs)  # bottom-right diagonal
axes[6].legend(loc = 'upper left', ncol = 3)


#
fig.savefig(os.path.join(mg.path_intrim_out(), 'ismn_metadata', 
                         'ismn_display_map_lu.png'), bbox_inches = 'tight',
            dpi = 600.)

