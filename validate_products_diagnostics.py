"""
2020/12/10
ywang254@utk.edu

Climatology, Seasonality (STD), Trend (with significance), Anomalies (std)
 of the merged products at each depth.
"""
import os
import xarray as xr
import pandas as pd
import numpy as np
import utils_management as mg
from utils_management.constants import depth, depth_cm
import itertools as it
from misc.standard_diagnostics2 import standard_diagnostics2


yrng = range(1970, 2017)

prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all']

name = prod_list[REPLACE1]
dind = REPLACE2

d = depth[dind]
dcm = depth_cm[dind]
standard_diagnostics2(name, d, dcm, yrng)
