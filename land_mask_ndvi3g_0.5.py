"""
20191120
Plot the interpolated NDVI3g data after interpolation and mask out the values
 of NDVI < 0.125
"""
import utils_management as mg
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import cartopy.crs as ccrs
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import os
import pandas as pd
import numpy as np


data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'land_mask_minimum',
                                    'ndvi_climatology_0.5.nc'))
var = data['ndvi'].copy(deep = True)
data.close()


###############################################################################
# Plot the global climatology.
###############################################################################
# Create the figure, get the panel (ax)
fig, ax = plt.subplots(figsize = (10, 6), 
                       subplot_kw = {'projection': ccrs.PlateCarree()})


# Options - Change here
cmap = 'Spectral'
levels = np.linspace(-.3, 1.0, 14)
map_extent = [-180, 180, -60, 90]
grid_on = True # True, False


# Generic module: var - some xr.DataArray
ax.coastlines()
ax.set_extent(map_extent)
h = ax.contourf(var.lon, var.lat, var, cmap = cmap, levels = levels)
plt.colorbar(h, ax = ax, boundaries = levels, shrink = 0.7)
if grid_on:
    gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True,
                      linewidth=1, color='gray', alpha=0.5, linestyle='--')
    gl.xlabels_top = False
    gl.ylabels_right = False
    gl.xlocator = mticker.FixedLocator(np.arange(-180, 181, 20.))
    gl.ylocator = mticker.FixedLocator(np.arange(-90., 91., 10.))
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'color': 'black', 'weight': 'bold', 'size': 10}
    gl.ylabel_style = {'color': 'black', 'weight': 'bold', 'size': 10}


fig.savefig(os.path.join(mg.path_intrim_out(), 'land_mask_minimum',
                         'ndvi_climatology_0.5.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)



###############################################################################
# Create and plot the land mask.
###############################################################################
thres = '0.125'
var_mask = var >= float(thres)


var_mask.to_dataset(name = 'mask').to_netcdf(os.path.join( \
    mg.path_intrim_out(), 'land_mask_minimum', 'ndvi_mask_0.5_' + \
    thres + '.nc'))


# Create the figure, get the panel (ax)
fig, ax = plt.subplots(figsize = (10, 6), 
                       subplot_kw = {'projection': ccrs.PlateCarree()})


# Options - Change here
cmap = 'Greys'
levels = np.arange(-0.5, 1.6, 1.)
map_extent = [-180, 180, -60, 90]
grid_on = True # True, False


# Generic module: var - some xr.DataArray
ax.coastlines()
ax.set_extent(map_extent)
h = ax.contourf(var_mask.lon, var_mask.lat, var_mask, cmap = cmap, 
                levels = levels)
plt.colorbar(h, ax = ax, boundaries = levels,
             ticks = 0.5 * (levels[1:] + levels[:-1]), shrink = 0.7)
if grid_on:
    gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True,
                      linewidth=1, color='gray', alpha=0.5, linestyle='--')
    gl.xlabels_top = False
    gl.ylabels_right = False
    gl.xlocator = mticker.FixedLocator(np.arange(-180, 181, 20.))
    gl.ylocator = mticker.FixedLocator(np.arange(-90., 91., 10.))
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlabel_style = {'color': 'black', 'weight': 'bold', 'size': 10}
    gl.ylabel_style = {'color': 'black', 'weight': 'bold', 'size': 10}


fig.savefig(os.path.join(mg.path_intrim_out(), 'land_mask_minimum',
                         'ndvi_mask_0.5_' + thres + '.png'),
            dpi = 600., bbox_inches = 'tight')
plt.close(fig)
