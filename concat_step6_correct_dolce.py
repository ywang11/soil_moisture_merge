"""
2020/01/17

Remove the < 0. and > 1. soil moisture values from the restored values.
"""
import xarray as xr
import os
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth, depth_cm
import utils_management as mg
import numpy as np
import itertools as it
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import pandas as pd
from misc.ismn_utils import get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
from misc.em_utils import rm_oor
import multiprocessing as mp


simple = [True, False, False]
lu_weighted = [False, False, True]
lu_threshold = 40
ismn_aggr_method = [get_ismn_aggr_method(simple[i], lu_weighted[i],
                                         lu_threshold) for i in range(3)]
cov_method = [get_cov_method(i) for i in range(5)]

year_str = str(year_longest[0]) + '-' + str(year_longest[-1])
period = pd.date_range(str(year_longest[0]) + '-01-01',
                       str(year_longest[-1]) + '-12-31', freq = 'MS')

prod_list = ['lsm', 'all']


def calc(option):
    i, prod, iam, cov = option

    d = depth[i]
    dcm = depth_cm[i]
    data = xr.open_dataset(os.path.join(mg.path_out(),
                                        'concat_dolce_' + prod,
                                        'concat_average_' + d + '_' + \
                                        iam + '_' + cov + '.nc'), \
                           decode_times = False)
    data['sm'].load()

    rm_oor(data['sm'], os.path.join(mg.path_out(), 'concat_dolce_' + prod),
           'positive_', 'average_' + d + '_' + iam + '_' + cov + '', period)
    data.close()


p = mp.Pool(4)
p.map_async(calc, list(it.product(range(4), prod_list, ismn_aggr_method,
                                  cov_method)))
p.close()
p.join()

#calc([0, prod_list[0], ismn_aggr_method[0], cov_method[0]])
