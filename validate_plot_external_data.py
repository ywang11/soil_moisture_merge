"""
Draw global time series and climatology maps of the validation datasets.
"""
import numpy as np
import os
import utils_management as mg
import pandas as pd
import xarray as xr
import matplotlib.pyplot as plt
from matplotlib import gridspec
import cartopy.crs as ccrs
import regionmask


name = 'ESSMRA' # 'SMERGE_v2', 'scPDSI', 'ESSMRA', 'GLEAMv3.3a'
if name == 'GLEAMv3.3a':
    path_in = os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                           'None', name, name + '_*0-100cm.nc')
else:
    path_in = os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                           'None', name, name + '_*.nc')
path_out = os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                        'None', name, name + '.png')
varname = 'sm'


def diagnosis(name, path_int, path_out, varname):
    def decode_month_since(time):
        start = time.attrs['units'].split(' ')[2]
        return pd.date_range(start, periods = len(time), freq = '1M')

    hr = xr.open_mfdataset(path_in, decode_times = False)
    var = hr[varname].copy(deep = True)
    if 'month' in hr['time'].attrs['units']:
        var['time'] = decode_month_since(hr['time'])
    else:
        var['time'] = xr.decode_cf(hr)
    hr.close()
    
    
    fig = plt.figure(figsize = (6.5, 8))
    gs = gridspec.GridSpec(2, 1, hspace = 0.2, height_ratios = [0.8, 1.2])
    
    # Time series
    ax = plt.subplot(gs[0])
    ax.plot(var['time'].to_index(), var.mean(dim = ['lat', 'lon']).values)
    ax.set_title(name + ' time series')
    
    # Map
    ax = plt.subplot(gs[1], projection = ccrs.PlateCarree())
    ax.coastlines()
    ax.gridlines()
    cf = ax.contourf(var.lon, var.lat, var.mean(dim = 'time'),
                     cmap = 'Spectral')

    #
    mask = regionmask.defined_regions.srex.mask(var[0,:,:])
    low = mask.min()
    high = mask.max()
    levels = np.arange(low - 0.5, high + 1)
    mask.plot.contourf(ax = ax, transform=ccrs.PlateCarree(),
                       levels = levels, add_colorbar = False,
                       alpha = 0.2, cmap = 'viridis')

    plt.colorbar(cf, ax = ax, orientation = 'horizontal', 
                 pad = 0.05)
    ax.set_title(name + ' climatology')
    
    fig.savefig(path_out, dpi = 600., bbox_inches = 'tight')
    plt.close(fig)

diagnosis(name, path_in, path_out, varname)
