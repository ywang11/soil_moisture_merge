# -*- coding: utf-8 -*-
"""
Created on Thu May  2 15:57:49 2019

@author: ywang254

Source: 
  Hobeichi et al. - 2018 - Derived Optimal Linear Combination 
    Evapotranspiration (DOLCE) A Global Gridded Synthesis Estimate

Use the weights derived from "lsm_weights_dolce.py" to calculate the final
  weighted spatiotemporal uncertainty of the land surface models.
"""
import os
import utils_management as mg
import pandas as pd
from utils_management.constants import depth, depth_cm, year_longest, \
    year_shorter, year_shorter2, year_shortest, lsm_list, target_lat, \
    target_lon
from misc.ismn_utils import get_weighted_monthly_data, get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
import xarray as xr
import numpy as np
import subprocess


# Choose the weighting method that was used to generate the weighted averaged
# ISMN data.
##from ismn_aggr_dolce import simple, dominance_lc, dominance_threshold
ismn_aggr_ind = REPLACE1
simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = get_ismn_aggr_method(simple[ismn_aggr_ind], 
                                        dominance_lc[ismn_aggr_ind],
                                        dominance_threshold)

year = REPLACE2 # [year_longest, year_shorter, year_shorter2, year_shortest]


# pick the covariance function based on which the weights are calculated
cov_estimer = REPLACE3 ## [0,1,2,3,4]
method = get_cov_method(cov_estimer)

remove_mean = True

land_mask = 'vanilla'

i = REPLACE4
d = depth[i]


###############################################################################
# Preliminary data
###############################################################################
# observed soil moisture
grid_latlon, weighted_monthly_data, _ = \
    get_weighted_monthly_data(os.path.join(mg.path_intrim_out(), 
                                           'ismn_aggr'),
                              ismn_aggr_method, d, opt='cal')
# subset to year_range
weighted_monthly_data = weighted_monthly_data.loc[ \
    (weighted_monthly_data.index >= np.datetime64(str(year[0])+'-01-01')) & \
    (weighted_monthly_data.index <= np.datetime64(str(year[-1])+'-12-31')),:]


# weighted soil moisture at observational data points
weighted_sm_at_data = pd.read_csv(os.path.join(mg.path_out(),
                                               'at_obs_dolce_lsm',
                                               'weighted_average_' +\
                                               str(year[0]) + '-' +\
                                               str(year[-1]) + '_' + d + '_' +\
                                               ismn_aggr_method + '_' +\
                                               method + '.csv'),
                                  index_col = 0, parse_dates = True)
weighted_sm_at_data = weighted_sm_at_data.loc[weighted_monthly_data.index, :]

# soil moisture of the individual land surface models at the 
#      observational data locations
lsm = lsm_list[(str(year[0])+'-'+str(year[-1]), d)]
lsm_sm_at_data = {}
for l in lsm:
    lsm_sm_at_data[l] = pd.read_csv(os.path.join( \
        mg.path_intrim_out(), 'at_obs_lsm', ismn_aggr_method + '_' + l + \
        '_' + d + '.csv'), index_col = 0, parse_dates = True)
    lsm_sm_at_data[l] = lsm_sm_at_data[l].loc[weighted_monthly_data.index,:]

#
lsm_mean_at_data = pd.read_csv(os.path.join(mg.path_out(), \
    'at_obs_meanmedian_lsm', 'mean_' + d + '_' + str(year[0]) + '-' + \
    str(year[-1]) + '_' + ismn_aggr_method + '.csv'),
    index_col = 0, parse_dates = True)
lsm_mean_at_data = lsm_mean_at_data.loc[weighted_monthly_data.index, :]


# average of the land surface models at all data points
data = xr.open_dataset(os.path.join(mg.path_out(), 
                                    'meanmedian_lsm',
                                    'mean_' + d + '_' + str(year[0]) + \
                                    '-' + str(year[-1]) + '.nc'),
                       decode_times = False)
mean_sm = data.sm.copy(deep = True)
data.close()

# weighted average results
data = xr.open_dataset(os.path.join(mg.path_out(), 'dolce_lsm_product',
                                    'weighted_average_' + str(year[0]) + \
                                    '-' + str(year[-1]) + '_' + d + '_' + \
                                    ismn_aggr_method + '_' + method + '.nc'),
                       decode_times = False)
weighted_sm = data.sm.copy(deep = True)
data.close()



###############################################################################
# Calculate the uncertainty window.
###############################################################################
#******************************************************************************
# Parameters
#******************************************************************************
# ---- w: weights
w_file = os.path.join(mg.path_intrim_out(), 'dolce_lsm_weights',
                      'weights_' + ismn_aggr_method + '_' + \
                      str(year[0]) + '-' + str(year[-1]) + '_' + method + \
                      '_' + d)
if remove_mean:
    w_file = w_file + '_remove_mean.csv'
else:
    w_file = w_file + '.csv'

weights0 = pd.read_csv(w_file, index_col = 0).loc[:,'Weight']
weights_list = list(weights0.index)
weights = weights0.values

if 'ESA-CCI' in weights_list:
    ind = np.where(['ESA-CCI' == x for x in weights_list])[0][0]
    
    weights_no_esa = weights0.values.copy().astype(np.float64)
    weights_no_esa[ind] = 0.
    weights_no_esa = weights_no_esa / np.sum(weights_no_esa)

    # ---- spatial points where no ESA-CCI data
    data = xr.open_mfdataset([os.path.join(mg.path_intrim_out(), 
                                           'Interp_Merge', land_mask,
                                           'ESA-CCI',
                                           'ESA-CCI_' + str(x) + '_' + \
                                           depth_cm[i] + '.nc') \
                              for x in year], decode_times = False)
    sm_nan = np.isnan(data.sm.values.copy())
    data.close()


# ---- K: number of merged original products
K = len(weights)
if 'ESA-CCI' in weights_list:
    K_no_esa = K - 1


# ---- J: nubmer of observational points
J = np.sum(~(np.isnan(weighted_monthly_data.values.reshape(-1))))


# ---- alpha: adjustment parameter
if np.min(weights) >= 0:
    alpha = 1
else:
    alpha =  1 - K * np.min(weights)
if 'ESA-CCI' in weights_list:
    if np.min(weights_no_esa) >= 0:
        alpha_no_esa = 1
    else:
        alpha_no_esa = 1 - K_no_esa * np.min(weights_no_esa)


# ---- adjusted weight vector
w_tilda = (weights + (alpha - 1) / K) / alpha
if 'ESA-CCI' in weights_list:
    # Note that w_tilda is non-zero
    w_tilda_no_esa = (weights_no_esa + \
                      (alpha_no_esa - 1) / K_no_esa) / alpha_no_esa


# ---- the discrepancy of the weighted data from observation at
#      observed locations
se_sq = np.nansum(np.power((weighted_sm_at_data - \
                            weighted_monthly_data).values.reshape(-1),
                           2)) / (J-1)

# ---- the beta parameter
beta_denom = np.zeros(len(weights), np.float64)
for k,l in enumerate(weights_list):
    beta_denom_inner = lsm_mean_at_data.values.reshape(-1) + \
        alpha * (lsm_sm_at_data[l].values.reshape(-1) - \
                 lsm_mean_at_data.values.reshape(-1)) - \
        weighted_sm_at_data.values.reshape(-1)
    beta_denom[k] = np.nanmean(np.power(beta_denom_inner, 2)) * w_tilda[k]
beta = np.sqrt(se_sq / np.sum(beta_denom))


if 'ESA-CCI' in weights_list:
    beta_denom_no_esa = np.zeros(len(weights_no_esa), np.float64)
    for k,l in enumerate(weights_list):
        beta_denom_inner = lsm_mean_at_data.values.reshape(-1) + \
            alpha_no_esa * (lsm_sm_at_data[l].values.reshape(-1) - \
                            lsm_mean_at_data.values.reshape(-1)) - \
            weighted_sm_at_data.values.reshape(-1)
        beta_denom_no_esa[k] = np.nanmean(np.power(beta_denom_inner,2)) * \
            w_tilda_no_esa[k]
    beta_no_esa = np.sqrt(se_sq / np.sum(beta_denom_no_esa))


# ---- calculate the weighted uncertainty
for k,l in enumerate(weights_list):
    # ---- reload the land surface model data
    data = xr.open_mfdataset([os.path.join(mg.path_intrim_out(), 
                                           'Interp_Merge', land_mask, l, 
                                           l + '_' + str(x) + '_' + \
                                           depth_cm[i] + '.nc') \
                              for x in year],
                             decode_times = False)
    sm = data.sm.copy(deep = True).load()

    ## Combine unuseful eq.
    #lsm_sm_adjusted = weighted_sm + beta * (mean_sm + alpha * \
    #    (sm - mean_sm) - weighted_sm)
    #lsm_sm_adjusted = lsm_sm_adjusted - weighted_sm
    lsm_sm_adjusted = beta * (mean_sm + alpha * \
                              (sm - mean_sm) - weighted_sm)
    if 'ESA-CCI' in weights_list:
        lsm_sm_adjusted_no_esa = beta_no_esa * (mean_sm + alpha_no_esa * \
                                                (sm - mean_sm) - weighted_sm)
    data.close()

    if l == 'ESA-CCI':
        lsm_sm_adusted = lsm_sm_adjusted.where(~( sm_nan & \
                                                  ~np.isnan(mean_sm) ), 0.)
        lsm_sm_adjusted_no_esa = \
            lsm_sm_adjusted_no_esa.where(~( sm_nan & \
                                            ~np.isnan(mean_sm) ), 0.)

    # ---- calculate the to-sum
    temp_uncertainty = w_tilda[k] * np.power(lsm_sm_adjusted, 2)
    if 'ESA-CCI' in weights_list:
        temp_uncertainty = temp_uncertainty.where( ~sm_nan,
            w_tilda_no_esa[k] * np.power(lsm_sm_adjusted_no_esa, 2) )

    if k==0:
        weighted_uncertainty = temp_uncertainty
    else:
        weighted_uncertainty += temp_uncertainty

# ---- convert from variance to standard deviation
weighted_uncertainty = np.sqrt(weighted_uncertainty)

wsufile = os.path.join(mg.path_out(), 'dolce_lsm_product',
                       'weighted_uncertainty_' + str(year[0]) + '-' + \
                       str(year[-1]) + '_' + d + '_' + \
                       ismn_aggr_method + '_' + method + '.nc')
weighted_uncertainty.to_dataset(name = 'sm_uncertainty').to_netcdf(wsufile)
