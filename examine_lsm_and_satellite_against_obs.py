# -*- coding: utf-8 -*-
"""
Created on Mon May 20 18:15:26 2019

@author: ywang254

Calculate evaluation metrics by climate zone, land use, and season.
"""
import utils_management as mg
from utils_management.constants import depth, lsm_list, year_longest, \
    clim_zone, lu_names    
from misc.analyze_utils import calc_stats
from misc.dolce_utils import get_ismn_aggr_method, get_weighted_monthly_data, \
    get_cov_method
import pandas as pd
import time
import numpy as np
import os


start = time.time()


# Choose the weighting method that was used to generate the weighted averaged
# ISMN data.
##from ismn_aggr_dolce import simple, dominance_lc, dominance_threshold
# ---- default to LU-weighted, not cut off by minimum pct represented 
#      land cover
simple = False # [True, False]
dominance_lc = False # [True, False]
dominance_threshold = 40
ismn_aggr_method = get_ismn_aggr_method(simple, dominance_lc, dominance_threshold)


# Pick the covariance function based on which the weights are calculated.
# ---- default to the simple empirical estimator
cov_estimer = 0
method = get_cov_method(cov_estimer)


year = year_longest
time_range = pd.date_range(start=str(year[0])+'-01-01',
                           end=str(year[-1])+'-12-31', freq='MS')


season = ['DJF', 'MAM', 'JJA', 'SON']


for i,d in enumerate(depth):

    # Load the observed soil moisture.
    grid_latlon, weighted_monthly_data, available_year = \
        get_weighted_monthly_data(os.path.join(mg.path_intrim_out(), 
                                  'ismn_aggr_dolce'), ismn_aggr_method, d)

    if i==0:
        gridded_datasets = ['weighted', 'average', 'median', 'satellite'] + \
                           lsm_list[(str(year[0])+'-'+str(year[-1]), d)]
    else:
        gridded_datasets = ['weighted', 'average', 'median'] + \
                           lsm_list[(str(year[0])+'-'+str(year[-1]), d)]

    ###########################################################################
    # Obtain labels for climate zone and major land.
    ###########################################################################
    def get_grid_labels(grid_names):
        grid_labels = pd.DataFrame(data = ' '*200, index = grid_latlon.columns, 
            columns = ['Climate Zone', 'Major Land Cover'])
        for gr in grid_names:
            # Obtain the climate zone and major land cover (excluding water
            # bodies) over this grid.
            def parse_grid_info(ddd, gr):
                fpath = os.path.join(mg.path_intrim_out(), 'ismn_lu', ddd, 
                                     gr + '.txt')
                f = open(fpath, 'r')
                # ---- skip grid information and 'Grid' label
                f.readline()
                f.readline()
                # ---- grid land cover percentages
                grid_lu = [float(n) for n in f.readline().split(',')]
                # ---- major land cover of the grid
                major_lu = lu_names[np.argmax(grid_lu[1:])+1]
                f.close()
                # ---- station land covers at this grid
                stations_lu = pd.read_csv(fpath, skiprows = 3)
                # ---- get the climate zone from the first station
                cz = stations_lu.iloc[0, -1]
                return cz, major_lu
            cz, major_lu = parse_grid_info(d, gr)
            grid_labels.loc[gr, 'Climate Zone'] = cz
            grid_labels.loc[gr, 'Major Land Cover'] = major_lu
        return grid_labels
    grid_labels = get_grid_labels(grid_latlon.columns)

    ###########################################################################
    # Create and fill the collectors to find out the metrics for each grid.
    ###########################################################################
    rmse_collector = pd.DataFrame(data = np.nan, index = grid_latlon.columns, 
                                  columns = pd.MultiIndex.from_product([ \
                                  gridded_datasets, season]))
    bias_collector = rmse_collector.copy()
    urmse_collector = rmse_collector.copy()
    corr_collector = rmse_collector.copy()

    for ds in gridded_datasets:

        # Load the grid-scale simulated soil moisture of ds.
        weighted_sm_at_data = pd.read_csv(os.path.join(mg.path_out(), 
            'postprocess_grid_at_obs', ds+'_'+d+'.csv'), index_col=0)

        for gr in grid_latlon.columns:

            # Obtain the climate zone and major land cover (excluding water
            # bodies) over this grid.
            def parse_grid_info(ddd, gr):
                fpath = os.path.join(mg.path_intrim_out(), 'ismn_lu', ddd, 
                                     gr + '.txt')
                f = open(fpath, 'r')
                # ---- skip grid information and 'Grid' label
                f.readline()
                f.readline()
                # ---- grid land cover percentages
                grid_lu = [float(n) for n in f.readline().split(',')]
                # ---- major land cover of the grid
                major_lu = lu_names[np.argmax(grid_lu[1:])+1]
                f.close()
                # ---- station land covers at this grid
                stations_lu = pd.read_csv(fpath, skiprows = 3)
                # ---- get the climate zone from the first station
                cz = stations_lu.iloc[0, -1]
                return cz, major_lu
            cz, major_lu = parse_grid_info(d, gr)


            # Calculate the matrix for each season
            for s in season:
                if s=='DJF':
                    temp = (weighted_monthly_data.index.month == 12) | \
                           (weighted_monthly_data.index.month == 1) | \
                           (weighted_monthly_data.index.month == 2)
                elif s=='MAM':
                    temp = (weighted_monthly_data.index.month == 3) | \
                           (weighted_monthly_data.index.month == 4) | \
                           (weighted_monthly_data.index.month == 5)
                elif s=='JJA':
                    temp = (weighted_monthly_data.index.month == 6) | \
                           (weighted_monthly_data.index.month == 7) | \
                           (weighted_monthly_data.index.month == 8)
                else:
                    temp = (weighted_monthly_data.index.month == 9) | \
                           (weighted_monthly_data.index.month == 10) | \
                           (weighted_monthly_data.index.month == 11)

                x = weighted_monthly_data.loc[temp, gr].values
                y = weighted_sm_at_data.loc[temp, gr].values
                non_nan = ~(np.isnan(x) | np.isnan(y))

                if sum(non_nan) < 3:
                    bias_collector.loc[gr, (ds,s)] = np.mean(y[non_nan] - \
                                                             x[non_nan])
                else:
                    rmse_collector.loc[gr, (ds,s)], \
                    bias_collector.loc[gr, (ds,s)], \
                    urmse_collector.loc[gr, (ds,s)], \
                    corr_collector.loc[gr, (ds,s)] = \
                        calc_stats(y[non_nan], x[non_nan])

    ###########################################################################
    # Save the collectors. Also make the summary by climate zone, land cover,
    # and season.
    ###########################################################################
    rmse_collector.to_csv(os.path.join(mg.path_out(), 
        'examine_lsm_and_satellite_against_obs', 'rmse_collecter_'+d+'.csv'))
    bias_collector.to_csv(os.path.join(mg.path_out(), 
        'examine_lsm_and_satellite_against_obs', 'bias_collecter_'+d+'.csv'))    
    urmse_collector.to_csv(os.path.join(mg.path_out(), 
        'examine_lsm_and_satellite_against_obs', 'urmse_collecter_'+d+'.csv'))
    corr_collector.to_csv(os.path.join(mg.path_out(), 
        'examine_lsm_and_satellite_against_obs', 'corr_collector_'+d+'.csv'))


    metrics = pd.DataFrame(data = np.nan, index = pd.MultiIndex.from_product( \
                           [clim_zone, lu_names.values(), season], 
                           names = ['Climate Zone', 'Major Land Cover', 'Season']), 
                           columns = pd.MultiIndex.from_product( \
                           [gridded_datasets, 
                            ['Max','Median','Min','Mean'],
                            ['RMSE','Bias','uRMSE','Corr']]))
    for ind,p in metrics.iterrows():
        temp = grid_labels.index[(grid_labels['Climate Zone'] == ind[0]) & \
                                 (grid_labels['Major Land Cover'] == ind[1])]

        if len(temp) == 0:
            continue

        for ds in gridded_datasets:
            metrics.loc[ind, (ds, 'Max','RMSE')] = np.nanmax( \
                rmse_collector.loc[temp, (ds, ind[2])])
            metrics.loc[ind, (ds, 'Median','RMSE')] = np.nanmedian( \
                rmse_collector.loc[temp, (ds, ind[2])])
            metrics.loc[ind, (ds, 'Min','RMSE')] = np.nanmin( \
                rmse_collector.loc[temp, (ds, ind[2])])
            metrics.loc[ind, (ds, 'Mean','RMSE')] = np.nanmean( \
                rmse_collector.loc[temp, (ds, ind[2])])

            metrics.loc[ind, (ds, 'Max','Bias')] = np.nanmax( \
                bias_collector.loc[temp, (ds, ind[2])])
            metrics.loc[ind, (ds, 'Median','Bias')] = np.nanmedian( \
                bias_collector.loc[temp, (ds, ind[2])])
            metrics.loc[ind, (ds, 'Min','Bias')] = np.nanmin( \
                bias_collector.loc[temp, (ds, ind[2])])
            metrics.loc[ind, (ds, 'Mean','Bias')] = np.nanmean( \
                bias_collector.loc[temp, (ds, ind[2])])

            metrics.loc[ind, (ds, 'Max','uRMSE')] = np.nanmax( \
                urmse_collector.loc[temp, (ds, ind[2])])
            metrics.loc[ind, (ds, 'Median','uRMSE')] = np.nanmedian( \
                urmse_collector.loc[temp, (ds, ind[2])])
            metrics.loc[ind, (ds, 'Min','uRMSE')] = np.nanmin( \
                urmse_collector.loc[temp, (ds, ind[2])])
            metrics.loc[ind, (ds, 'Mean','uRMSE')] = np.nanmean( \
                urmse_collector.loc[temp, (ds, ind[2])])

            metrics.loc[ind, (ds, 'Max','Corr')] = np.nanmax( \
                corr_collector.loc[temp, (ds, ind[2])])
            metrics.loc[ind, (ds, 'Median','Corr')] = np.nanmedian( \
                corr_collector.loc[temp, (ds, ind[2])])
            metrics.loc[ind, (ds, 'Min','Corr')] = np.nanmin( \
                corr_collector.loc[temp, (ds, ind[2])])
            metrics.loc[ind, (ds, 'Mean','Corr')] = np.nanmean( \
                corr_collector.loc[temp, (ds, ind[2])])

    metrics.to_csv(os.path.join(mg.path_out(), 
        'analyze_lsm_and_satellite_against_obs', 'metrics_' + d + '.csv'))


end = time.time()
print('The script finished in ' + ('%.4f' % ((end-start)/3600) ) + ' hours.')