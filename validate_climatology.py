"""
2020/12/06

Climatology of the products, evaluation datasets, raw datasets.
"""
import os
import xarray as xr
import pandas as pd
import utils_management as mg
from utils_management.constants import depth, depth_cm, lsm_list, \
    year_longest, year_shorter2, year_shorter, year_shortest
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability, one_layer_availability
from glob import glob
import itertools as it
from dateutil.relativedelta import relativedelta


def decode_month_since(time):
    ref = pd.Timestamp(time.attrs['units'].split(' ')[2])
    start = ref + relativedelta(months = time.values[0])
    start = start.replace(day = 1, hour = 0, minute = 0, second = 0)
    return pd.date_range(start, periods = len(time), freq = 'MS')


def get_climatology(flist, yrng):
    hr = xr.open_mfdataset(flist, decode_times = False)
    if ('CRU_v4.03_year_month_positive_9grid_0-10cm_1950-2016' in flist[0]) |\
       ('CRU_v4.03_year_month_positive_9grid_10-30cm_1950-2016' in flist[0]) |\
       ('CRU_v4.03_year_month_positive_9grid_30-50cm_1950-2016' in flist[0]) |\
       ('CRU_v4.03_year_month_positive_9grid_50-100cm_1950-2016' in flist[0]):
        hr['time'].attrs['units'] = 'months since 1950-01-01'
    if 'month' in hr['time'].attrs['units']:
        tvec = decode_month_since(hr['time'])
    else:
        tvec = xr.decode_cf(hr)['time'].to_index()
    try:
        sm = hr['sm']
    except:
        sm = hr['predicted']
    sm_mean = sm[(tvec.year >= yrng[0]) & \
                 (tvec.year <= yrng[-1]), : , :].mean(dim = 'time')
    hr.close()

    return sm_mean


land_mask = 'vanilla'

year_list = {'SMOS_L3': range(2010, 2017), 'SMOS_L4': range(2010, 2017),
             'SMERGE_v2': range(1981, 2017), # for compatibility with shorter2
             'GLEAMv3.3a': range(1981, 2017), # for compatibility with shorter2
             'SoMo': range(2000,2017)}

# 'GLEAMv3.3a', 'SMOS_L3', 'SMOS_L4', 'SMERGE_v2', 'SoMo',
#             'SMOS_L4'
for name in []:
    if name == 'SoMo':
        for layer in ['0-10cm', '10-30cm', '30-50cm']:
            flist = sorted(glob(os.path.join(mg.path_intrim_out(),
                                             'Interp_Merge',
                                             land_mask, name,
                                             name + '_*_' + layer + '.nc')))
            sm_mean = get_climatology(flist, year_list[name])
            sm_mean.to_dataset(name = 'sm' \
            ).to_netcdf(os.path.join(mg.path_out(), 'validate', 'climatology',
                                     name + '_' + layer + '.nc'))
    else:
        if name == 'GLEAMv3.3a':
            flist = sorted(glob(os.path.join(mg.path_intrim_out(),
                                             'Interp_Merge',
                                             land_mask, name,
                                             name + '_*_0-100cm.nc')))
            sm_mean = get_climatology(flist, year_list[name])
        elif name == 'SMOS_L3':
            # ascending orbit
            flist = sorted(glob(os.path.join(mg.path_intrim_out(),
                                             'Interp_Merge',
                                             land_mask, name,
                                             name + '_*_CLF3MA.nc')))
            sm_mean_a = get_climatology(flist, year_list[name])
            #
            flist = sorted(glob(os.path.join(mg.path_intrim_out(),
                                             'Interp_Merge',
                                             land_mask, name,
                                             name + '_*_CLF3MD.nc')))
            sm_mean_d = get_climatology(flist, year_list[name])
            #
            sm_mean = (sm_mean_a + sm_mean_d) / 2
        elif (name == 'SMOS_L4') | (name == 'SMERGE_v2'):
            flist = sorted(glob(os.path.join(mg.path_intrim_out(),
                                             'Interp_Merge',
                                             land_mask, name, name + '*.nc')))
            sm_mean = get_climatology(flist, year_list[name])

        if name == 'GLEAMv3.3a':
            sm_mean.to_dataset(name = 'sm' \
            ).to_netcdf(os.path.join(mg.path_out(), 'validate',
                                     'climatology', name + '_0-100cm.nc'))
        else:
            sm_mean.to_dataset(name = 'sm' \
            ).to_netcdf(os.path.join(mg.path_out(), 'validate',
                                     'climatology', name + '.nc'))


#
for dind, d in enumerate(depth):
    dcm = depth_cm[dind]
    lsm_temp = lsm_list[(str(year_shorter2[0]) + '-' + \
                         str(year_shorter2[-1]), d)]
    cmip5_temp = cmip5_availability(dcm, land_mask)
    cmip6_temp = sorted(set(mrsol_availability(dcm, land_mask, 'historical')) \
                        & set(mrsol_availability(dcm, land_mask, 'ssp585')))

    if dind == 0:
        lsm = lsm_temp
        cmip5 = cmip5_temp
        cmip6 = cmip6_temp
    else:
        lsm = sorted(set(lsm) & set(lsm_temp))
        cmip5 = sorted(set(cmip5) & set(cmip5_temp))
        cmip6 = sorted(set(cmip6) & set(cmip6_temp))


# range(2010, 2017), 
year_list_list = [range(1981, 2017), range(2000, 2017)]
# 'lsm', 'cmip5', 'cmip6', 'mean_lsm', 'dolce_lsm', 'em_lsm',
#             'em_cmip5', 'em_cmip6', 'em_2cmip', 'em_all'
for mset in ['em_lsm', 'em_cmip5', 'em_cmip6', 'em_2cmip', 'em_all']:
    if mset == 'lsm':
        model_list = lsm
    elif mset == 'cmip5':
        model_list = cmip5
    elif mset == 'cmip6':
        model_list = cmip6
    else:
        model_list = [mset]

    for model, yind in it.product(model_list, range(len(year_list_list))):
        for dind, dcm in enumerate(depth_cm):
            d = depth[dind]
            year = year_list_list[yind]
            year_str = str(year[0]) + '-' + str(year[-1])

            if mset == 'lsm':
                flist = sorted(glob(os.path.join(mg.path_intrim_out(),
                                                 'Interp_Merge', land_mask,
                                                 model, model + \
                                                 '_*_' + dcm + '.nc')))
            elif mset == 'cmip5':
                flist = [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                      land_mask, 'CMIP5', model,
                                      'sm_historical_r1i1p1_' + \
                                      str(yy) + '_' + dcm + '.nc') \
                         for yy in range(year[0], 2006)] + \
                        [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                      land_mask, 'CMIP5', model,
                                      'sm_rcp85_r1i1p1_' + str(yy) +  '_' + \
                                      dcm + '.nc') \
                         for yy in range(2006, year[-1] + 1)]
            elif mset == 'cmip6':
                flist = [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                      land_mask, 'CMIP6', model,
                                      'mrsol_historical_' + str(yy) + '_' + \
                                      dcm + '.nc') \
                         for yy in range(year[0], 2015)] + \
                        [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                      land_mask, 'CMIP6', model,
                                      'mrsol_ssp585_' + str(yy) + '_' + \
                                      dcm + '.nc') \
                         for yy in range(2015, year[-1] + 1)]
            elif mset == 'mean_lsm':
                flist = [os.path.join(mg.path_out(),
                                      'meanmedian_lsm',
                                      'mean_' + d + '_' + \
                                      str(year_longest[0]) + '-' + \
                                      str(year_longest[-1]) + '.nc')]
            elif mset == 'dolce_lsm':
                flist = [os.path.join(mg.path_out(),
                                      'concat_dolce_lsm',
                                      'positive_average_' + d + \
                                      '_lu_weighted_ShrunkCovariance.nc')]
            elif (mset == 'em_all') | (mset == 'em_lsm'):
                flist = [os.path.join(mg.path_out(),
                                      'concat_' + mset,
                                      'positive_CRU_v4.03' + \
                                      '_year_month_anomaly_9grid_' + \
                                      dcm + '_predicted_' + \
                                      str(year_longest[0]) + '-' + \
                                      str(year_longest[-1]) + '.nc')]
            else:
                flist = [os.path.join(mg.path_out(),
                                      model + '_corr',
                                      'CRU_v4.03_year_month_positive_' + \
                                      '9grid_' + dcm + '_' + \
                                      str(year_longest[0]) + \
                                      '-' + str(year_longest[-1]),
                                      'predicted.nc')]

            sm_mean = get_climatology(flist, year)

            if dcm != '50-100cm':
                sm_mean.to_dataset(name = 'sm' \
                ).to_netcdf(os.path.join(mg.path_out(), 'validate',
                                         'climatology', mset + '_' + \
                                         model + '_' + dcm + '_' + year_str + \
                                         '.nc'))

            if dind == 0:
                sm_mean_tot = sm_mean * 0.1
            elif (dind == 1) | (dind == 2):
                sm_mean_tot += sm_mean * 0.2
            else:
                sm_mean_tot += sm_mean * 0.5
                sm_mean_tot.to_dataset(name = 'sm' \
                ).to_netcdf(os.path.join(mg.path_out(), 'validate',
                                         'climatology',
                                         mset + '_' + model + \
                                         '_0-100cm_' + \
                                         year_str + '.nc'))

            if dind == 0:
                sm_mean_tot2 = sm_mean * 0.1
            elif dind == 1:
                sm_mean_tot2 += sm_mean * 0.2
            elif dind == 2:
                sm_mean_tot2 += sm_mean * 0.1
                sm_mean_tot2 = sm_mean_tot2 / 0.4
                sm_mean_tot2.to_dataset(name = 'sm' \
                ).to_netcdf(os.path.join(mg.path_out(), 'validate',
                                         'climatology',
                                         mset + '_' + model + \
                                         '_0-40cm_' + year_str + '.nc'))
