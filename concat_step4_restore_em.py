"""
20190923
ywang254@utk.edu

Step 4 of the CDF-matching merging:
 Add back the seasonality.
"""
import xarray as xr
import os
import pandas as pd
import numpy as np
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth_cm, target_lat, target_lon
import utils_management as mg
from misc.ismn_utils import get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
from misc.concat_utils import piecewise_cdf_create, piecewise_cdf_apply
import itertools as it
import multiprocessing as mp


met_obs = 'CRU_v4.03'
em = 'year_month_anomaly_9grid' # Need to modify the read script if not
                                # this method
prefix = ['predicted']
prod_list = ['lsm', 'all']


args_list = []
for i, fx, prod in it.product(range(len(depth_cm)), prefix, prod_list):
    args_list.append([i, fx, prod])


def restore(args):
#for args in [args_list[0]]:
    i, fx, prod = args
    dcm = depth_cm[i]

    for year in [year_shorter, year_shortest]:
        year_str = str(year[0]) + '-' + str(year[-1])

        data = xr.open_dataset(os.path.join(mg.path_out(), 'concat_em_' + \
                                            prod, 'scaled_' + met_obs + \
                                            '_' + em + '_' + dcm + '_' + \
                                            fx + '_' + year_str + '.nc'),
                               decode_times = False)
        data['time'] = pd.date_range(str(year[0]) + '-01-01',
                                     str(year[-1]) + '-12-31', freq = 'MS')

        # Fix to the reference climatology (1981-2016 dataset).
        data2 = xr.open_dataset(os.path.join(mg.path_out(),
                                             'concat_em_' + prod, 
                                             'climatology_' + 
                                             met_obs + '_' + em + '_' + dcm + \
                                             '_' + fx + '_1981-2016.nc'),
                                decode_times = False)

        for m in range(12):
            data[fx].loc[data.time.indexes['time'].month == (m + 1),
                         :, :] += data2[fx].values[m, :, :].copy()

        data2.close()
        data.to_netcdf(os.path.join(mg.path_out(), 'concat_em_' + prod,
                                    'restored_' + met_obs + '_' + em + '_' + \
                                    dcm + '_' + fx + '_' + year_str + '.nc'))


pool = mp.Pool(3)
pool.map_async(restore, args_list)
pool.close()
pool.join()
