"""
2019/08/05
ywang254@utk.edu

Plot the time series of global mean annual mean soil moisture of the 
 emergent constraint results, with the upper CI and lower CI.
Show the GLEAMv3.3 and ERA-Land time series of global mean annual mean
 soil moisture for comparison.
"""
import matplotlib.pyplot as plt
import utils_management as mg
from utils_management.constants import year_cmip5, year_longest, \
    year_shorter, year_shorter2, year_shortest, depth, depth_cm
from misc.plot_utils import plot_ts_shade, plot_ts_trend
import pandas as pd
import os
import numpy as np
import multiprocessing as mp
import itertools as it


pr_obs = 'CRU_v4.03'


#
def plotter(option):
    year, method = option
    global_ts_all_depth = {}
    for i,d in enumerate(depth):
        dcm = depth_cm[i]
    
        #
        year_str = str(year[0]) + '-' + str(year[-1])
        prefix_CI_lower = os.path.join(mg.path_out(), 
                                       'standard_diagnostics_em_' + model_set,
                                       pr_obs + '_predicted_CI_lower_' + \
                                       method + \
                                       '_' + year_str + '_' + d + '_g_ts.csv')
        prefix_CI_upper = os.path.join(mg.path_out(), 
                                       'standard_diagnostics_em_' + model_set,
                                       pr_obs + '_predicted_CI_upper_' + \
                                       method + \
                                       '_' + year_str + '_' + d + '_g_ts.csv')
        prefix_point = os.path.join(mg.path_out(), 
                                    'standard_diagnostics_em_' + model_set,
                                    pr_obs + '_predicted_' + method + \
                                    '_' + year_str + '_' + d + '_g_ts.csv')
    
        # Emergent constraint result
        global_ts = {}
        global_ts['min'] = pd.read_csv(prefix_CI_lower, index_col = 0, 
                                       parse_dates = True).iloc[:, 0]
        global_ts['max'] = pd.read_csv(prefix_CI_upper, index_col = 0, 
                                       parse_dates = True).iloc[:, 0]
        global_ts['mean'] = pd.read_csv(prefix_point, index_col = 0,
                                        parse_dates = True).iloc[:, 0]
        # ---- convert to annual results.
        global_ts = pd.DataFrame.from_dict(global_ts)
        global_ts = global_ts.groupby(global_ts.index.year).mean()
        # ---- convert to anomalies by removing the climatology of point 
        #      estimate.
        ##global_ts = global_ts - global_ts['mean'].mean()
    
        #
        if 'month_anomaly' in method:
            global_ts['min'] = global_ts['mean'].mean() + global_ts['min']
            global_ts['max'] = global_ts['mean'].mean() + global_ts['max']
    
        global_ts_all_depth[d] = global_ts
    
    
    # GLEAM v3.3a is only available for 0-10cm.
    gleam = pd.read_csv(os.path.join(mg.path_out(), 
                                     'standard_diagnostics_lsm', 
                                     'GLEAMv3.3a_0.00-0.10_g_ts.csv'),
                        index_col = 0, parse_dates = True)
    gleam = gleam.groupby(gleam.index.year).mean()
    
    # ERA-Land is only available for the two shallow depths.
    eraland_all_depth = {}
    for i,d in enumerate(['0.00-0.10', '0.10-0.30']):
        eraland_all_depth[d] = pd.read_csv(os.path.join(mg.path_out(),
                                                'standard_diagnostics_lsm',
                                                        'ERA-Land_' + \
                                                        d + '_g_ts.csv'),
                                           index_col = 0, parse_dates = True)
        eraland_all_depth[d] = eraland_all_depth[d \
        ].groupby(eraland_all_depth[d].index.year).mean()
    
    
    # 
    fig, axes = plt.subplots(nrows = len(depth_cm), ncols = 2, sharex = True, 
                             sharey = False, figsize = (10, 6.5))
    fig.subplots_adjust(hspace = 0., wspace = 0.2)
    
    # (1) Soil Moisture
    for i,d in enumerate(depth):
        dcm = depth_cm[i]
    
        ax = axes[i, 0]
        h1 = plot_ts_shade(ax, global_ts_all_depth[d].index, 
                           global_ts_all_depth[d], ts_col = 'b')
        if i == 0:
            h2, = ax.plot(gleam.index, gleam.values.reshape(-1), '-r', 
                          linewidth = 2, zorder = 3)
        if (i == 0) | (i == 1):
            h3, = ax.plot(eraland_all_depth[d].index, eraland_all_depth[d],
                          '-k', linewidth = 2, zorder = 3)
        if (i == 0):
            ax.set_title('Soil Moisture (m$^3$/m$^3$)')
    
        if i == len(depth)-1:
            ax.legend([h1, h2, h3], [title, 'GLEAMv3.3a', 'ERA-Land'], 
                      ncol = 3, loc = 'lower right', 
                      bbox_to_anchor = (1.5, -0.7))
    
        ax.set_xlim([year[0], year[-1]])
        ax.set_xlabel('Year')
        ax.set_ylabel(dcm) # '(m$^3$/m$^3$)'
        ax.set_ylim([0., 0.5])
        ax.set_yticks(np.linspace(0.1, 0.4, 5))
    
    # (2) Soil Moisture Anomaly
    for i,d in enumerate(depth):
        dcm = depth_cm[i]
    
        ax = axes[i, 1]
        h1, = ax.plot(global_ts_all_depth[d].index, 
                      global_ts_all_depth[d]['mean'] - \
                      global_ts_all_depth[d]['mean'].mean(), '-b', 
                      linewidth = 2, zorder = 3)
        if i == 0:
            h2, = ax.plot(gleam.index, gleam['global_mean'] - \
                          gleam['global_mean'].mean(), '-r', 
                          linewidth = 2, zorder = 3)
        if (i == 0) | (i == 1):
            h3, = ax.plot(eraland_all_depth[d].index, 
                          eraland_all_depth[d]['global_mean'] - \
                          eraland_all_depth[d]['global_mean'].mean(), '-k', 
                          linewidth = 2, zorder = 3)
        if (i == 0):
            ax.set_title('Anomaly (m$^3$/m$^3$)')
    
        ax.set_xlim([year[0], year[-1]])
        ax.set_xlabel('Year')
        ax.set_ylim([-0.005, 0.005])
        ax.set_yticks(np.linspace(-0.004, 0.004, 5))
    
    fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                             'ts_annual',
                             'em_' + model_set + '_' + pr_obs + '_' + method +\
                             '_' + year_str + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)


# 'lsm', 'cmip5', 'cmip6', '2cmip', 
for model_set in ['all']:

    if model_set == 'lsm':
        year_list = [year_longest, year_shorter, year_shorter2, year_shortest]
        method = ['year_month_9grid', 'year_month_anomaly_9grid']
    elif model_set == 'all':
        year_list = [year_longest, year_shorter, year_shorter2, year_shortest]
        method = ['year_month_9grid', 'year_month_anomaly_9grid']
        # 'year_month_1grid', 'year_month_anomaly_1grid',
    else:
        year_list = [year_longest]
        method = ['year_month_9grid', 'year_month_anomaly_9grid']
        # 'year_month_1grid', 'year_month_anomaly_1grid',

    if model_set == 'lsm': 
        title = 'EC-ORS'
    elif model_set == 'cmip5':
        title = 'EC-CMIP5'
    elif model_set == 'cmip6':
        title = 'EC-CMIP6'
    elif model_set == '2cmip':
        title = 'EC-CMIP5+6'
    else:
        title = 'EC-ALL'

    #p = mp.Pool(4)
    #p.map_async(plotter, list(it.product(year_list, method)))
    #p.close()
    #p.join()
    for year, meth in it.product(year_list, method):
        plotter([year, meth])
