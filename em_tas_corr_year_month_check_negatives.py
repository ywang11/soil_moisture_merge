"""
20191218
ywang254@utk.edu

Find the negative soil moisture in the year_month emergent constraint data.
"""
import xarray as xr
import os
import utils_management as mg
from utils_management.constants import depth_cm, year_longest
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import numpy as np
import itertools as it
import pandas as pd


year_str = str(year_longest[0]) + '-' + str(year_longest[-1])
period = pd.date_range(str(year_longest[0]) + '-01-01',
                       str(year_longest[-1]) + '-12-31', freq = 'MS')


# 'lsm',
for dcm, model in it.product(depth_cm, ['cmip5', 'cmip6']):
    if model == 'lsm':
        best = 'year_month_anomaly_9grid'
        data = xr.open_dataset(os.path.join(mg.path_out(), 'concat_em_lsm',
                                            'concat_CRU_v4.03_' + best + '_' +\
                                            dcm + '_predicted_' + year_str + \
                                            '.nc'), decode_times = False)
    else:
        best = 'year_month_anomaly_9grid'.replace('anomaly', 'restored')
        data = xr.open_mfdataset([os.path.join(mg.path_out(),
                                               'em_' + model + '_corr',
                                               'CRU_v4.03_' + best + '_' + \
                                               dcm + '_' + year_str,
                                               'predicted_' + str(y) + '.nc') \
                                  for y in year_longest], decode_times = False,
                                 concat_dim = 'time')
    sm_is_negative = data['predicted'].values.copy() < 0.
    data.close()


    #
    ts_negative = np.sum(np.sum(sm_is_negative, axis = 2), axis = 1)
    map_negative = np.sum(sm_is_negative, axis = 0)


    #
    fig = plt.figure(figsize = (12, 12))
    ax = fig.add_subplot(211)
    ax.plot(period.year.values + (period.month.values-0.5)/12, ts_negative)
    ax.set_xticks(period.year[::60])
    ax.set_title('# negatives per time step')

    ax = fig.add_subplot(212, projection = ccrs.PlateCarree())
    ax.coastlines()
    ax.set_extent([-180, 180, -60, 90])
    h = ax.contourf(data.lon.values, data.lat.values, map_negative,
                    corner_mask = False)
    plt.colorbar(h, ax = ax, shrink = 0.7)

    fig.savefig(os.path.join(mg.path_out(), 'em_best_check_negatives',
                             model + '_' + dcm + '.png'), dpi = 600.)
    plt.close(fig)
