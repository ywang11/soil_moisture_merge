# -*- coding: utf-8 -*-
"""
Created on Fri Feb 22 20:45:15 2019

@author: ywang254
"""
import utils_management as mg
import os
import xarray as xr

path_gleam = os.path.join(mg.path_data(), 'GLEAM', 'v3.2a')

for yy in range(1980, 2018):
    # Root zone
    ds = xr.open_dataset(os.path.join(path_gleam, str(yy), 
                                      'SMroot_' + str(yy) + '_GLEAM_v3.2a.nc'))
    ds.close()


    # Surface (0-10cm)
    ds = xr.open_dataset(os.path.join(path_gleam, str(yy), 
                                      'SMsurf_' + str(yy) + '_GLEAM_v3.2a.nc'))
    
    ds.close()
