"""
2019/07/14

ywang254@utk.edu

Calculate the common land mask of the interpolated CMIP5 models and their
precipitation variables, after interpolation to 0.5oC. 
"""
import sys
import os
import xarray as xr
import numpy as np
import pandas as pd
from glob import glob
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import utils_management as mg


path_merge = os.path.join(mg.path_intrim_out(), 'Interp_Merge')


cmip5_all = ['ACCESS1-0','ACCESS1-3','CanESM2','CNRM-CM5',
             'FGOALS-s2', 'GFDL-CM3','GFDL-ESM2G','GFDL-ESM2M',
             'GISS-E2-H-CC','GISS-E2-H','GISS-E2-R-CC','GISS-E2-R',
             'HadGEM2-ES','HadGEM2-CC','inmcm4','MIROC5',
             'MIROC-ESM','MIROC-ESM-CHEM','NorESM1-ME']

target_lat = np.arange(-89.75, 89.76, 0.5)
target_lon = np.arange(-179.75, 179.76, 0.5)


mask_all = np.zeros([len(cmip5_all)*3, 360, 720], dtype = int)
for model_ind, model in enumerate(cmip5_all):
    path_model = os.path.join(path_merge, 'None', 'CMIP5', model)
    file_list = glob(path_model + '/sm_historical_*.nc')
    f = os.path.join(os.path.join(path_model, file_list[0]))
    data = xr.open_dataset(f, decode_times = False)
    mask_all[model_ind, :, :] = ~np.isnan(data.sm[0,:,:])
    data.close()
for model_ind, model in enumerate(cmip5_all, len(cmip5_all)):
    path_model = os.path.join(path_merge, 'None', 'CMIP5', model)
    file_list = glob(path_model + '/pr_historical_*.nc')
    f = os.path.join(os.path.join(path_model, file_list[0]))
    data = xr.open_dataset(f, decode_times = False)
    mask_all[model_ind, :, :] = ~np.isnan(data.pr[0,:,:])
    data.close()
for model_ind, model in enumerate(cmip5_all, 2 * len(cmip5_all)):
    path_model = os.path.join(path_merge, 'None', 'CMIP5', model)
    file_list = glob(path_model + '/tas_historical_*.nc')
    f = os.path.join(os.path.join(path_model, file_list[0]))
    data = xr.open_dataset(f, decode_times = False)
    mask_all[model_ind, :, :] = ~np.isnan(data.tas[0,:,:])
    data.close()


# Find the minimum mask, i.e. where all the mask == 1
mask_check = np.all(mask_all == 1, axis=0)


# Save the minimum mask to file.
mask_check = xr.DataArray(mask_check, dims = ['lat', 'lon'], 
                          coords = {'lat': target_lat, 'lon': target_lon})
mask_check.to_dataset(name = 'mask' \
).to_netcdf(os.path.join(mg.path_intrim_out(), 'land_mask_minimum', 
                         'cmip5.nc'))


# Accompanying graph.
fig, ax = plt.subplots(figsize = (12, 12), 
                       subplot_kw = {'projection': ccrs.PlateCarree()})
ax.coastlines(color = 'red')
ax.gridlines(draw_labels = True)
mask_check.plot(cmap = 'Greys')
fig.savefig(os.path.join(mg.path_intrim_out(), 'land_mask_minimum',
                         'cmip5.png'), bbox_inches = 'tight', dpi = 600.)
plt.close(fig)
