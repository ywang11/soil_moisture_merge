"""
Plot the Target & Taylor diagrams to compare the
 (1) Bias, RMSE, and uRMSE
 (2) SD-ratio, uRMSE, and Corr
 of the Mean, Median, the best method of DOLCE, the best method of EM-LSM, 
 and the individual land surface models/reanalysis/satellite data using the
 validation-period ISMN observations aggregated using land cover-weighted. 
The purpose is to understand which of the original datasets are causing 
 good/bad performance.
"""
import os
import numpy as np
import pandas as pd
from misc.plot_utils import plot_convenient_target, plot_convenient_taylor
import utils_management as mg
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth, lsm_list
import itertools as it
from misc.ismn_utils import get_weighted_monthly_data
import matplotlib.pyplot as plt
from matplotlib import cm


###############################################################################
# Separate plots for the following factors and depth.
###############################################################################
year_list = [year_longest, year_shorter, year_shorter2, year_shortest]


###############################################################################
# Set up some controlling factors.
###############################################################################
pr = 'val' # different periods are similar
ismn_aggr_method = 'lu_weighted' # different aggregation methods are similar


###############################################################################
# Weighting method setups: use the best that are identified in the
# "standard_metrics_compare_*.py"
###############################################################################
# Emergent constraint
em_lsm_best = 'month_1grid'
# em_cmip5_best = ['month_1grid', 'month_anomaly_1grid',
#                  'year_month_1grid', 'year_month_9grid']

# DOLCE
dolce_cov_best = 'ShrunkCovariance'


###############################################################################
# Plot loop.
###############################################################################
for year, d in it.product(year_list, depth):
    lsm = lsm_list[(str(year[0])+'-'+str(year[-1]), d)]


    ###########################################################################
    # Read observed soil moisture:
    ###########################################################################
    grid_latlon, weighted_monthly_data, \
        _ = get_weighted_monthly_data(os.path.join(mg.path_intrim_out(), 
                                                   'ismn_aggr'),
                                      ismn_aggr_method, d, opt = pr)


    ###########################################################################
    # Read modeled/weighted data:
    ###########################################################################
    # ---- all the land surface models.
    data_lsm = {}
    for l in lsm:
        data_lsm[l] = pd.read_csv(os.path.join(mg.path_intrim_out(),
                                               'at_obs_lsm',
                                               ismn_aggr_method + '_' + \
                                               l + '_' + d + '.csv'),
                                  index_col = 0, parse_dates = True \
        ).loc[weighted_monthly_data.index, :]

    # ---- emergent constraint
    data_em_lsm = pd.read_csv(os.path.join(mg.path_out(), 'at_obs_em_lsm',
                                           em_lsm_best + '_' + d + '_' + \
                                           str(year[0]) + '-' + \
                                           str(year[-1]) + '_' + \
                                           ismn_aggr_method + '_' + pr + \
                                           '.csv'), index_col = 0, \
    parse_dates = True).loc[weighted_monthly_data.index, :]

    # ---- DOLCE
    data_dolce = pd.read_csv(os.path.join(mg.path_out(), 'at_obs_dolce',
                                          'weighted_average_' +\
                                          str(year[0]) + '-' + str(year[-1]) +\
                                          '_' + d + '_' + ismn_aggr_method +\
                                          '_' + dolce_cov_best + '_' + \
                                          pr + '.csv'), index_col = 0, \
    parse_dates = True).loc[weighted_monthly_data.index, :]

    # ---- mean
    data_mean = pd.read_csv(os.path.join(mg.path_out(), 'at_obs_meanmedian',
                                         'average_' + d + '_' + str(year[0]) +\
                                         '-' + str(year[-1]) + '_' + \
                                         ismn_aggr_method + '_' + pr + '.csv'),
                            index_col = 0, parse_dates = True \
    ).loc[weighted_monthly_data.index, :]

    # ---- median
    data_median = pd.read_csv(os.path.join(mg.path_out(), 'at_obs_meanmedian',
                                           'median_' + d + '_' + \
                                           str(year[0]) + '-' + str(year[-1]) \
                                           + '_' + ismn_aggr_method + '_' + \
                                           pr + '.csv'), index_col = 0, \
    parse_dates = True).loc[weighted_monthly_data.index, :]


    ###########################################################################
    # Plot the individual land surface models and the other datasets.
    ###########################################################################
    # Colorization setup.
    cmap = cm.get_cmap('gist_ncar')
    col_set = {}
    col_set['lsm'] = [cmap((x+0.5)/len(lsm) * 0.8) for x in range(len(lsm))]
    col_set['em_lsm'] = [cmap(0.83)]
    col_set['dolce'] = [cmap(0.88)]
    col_set['mean'] = [cmap(0.93)]
    col_set['median'] = [cmap(0.98)]

    # Dataset setup.
    obs = weighted_monthly_data.values.reshape(-1)
    sim = np.empty([len(obs), len(lsm) + 4])
    for l_ind, l in enumerate(lsm):
        sim[:, l_ind] = data_lsm[l].values.reshape(-1)
    sim[:, l_ind+1] = data_em_lsm.values.reshape(-1)
    sim[:, l_ind+2] = data_dolce.values.reshape(-1)
    sim[:, l_ind+3] = data_mean.values.reshape(-1)
    sim[:, l_ind+4] = data_median.values.reshape(-1)

    retain = ~( np.isnan(obs) | np.isnan(sim).any(axis=1) )
    sim = sim[retain, :]
    obs = obs[retain]

    # Target diagram.
    ticks = np.round(np.arange(-0.1, 0.11, 0.02), 2)
    ## xtickLabelPos = np.arange(-0.1, 0.11, 0.02),
    ## ytickLabelPos = np.arange(-0.1, 0.11, 0.02),
    fig, ax = plt.subplots(figsize = (12, 12))
    plot_convenient_target(ax, obs, sim, col_set['lsm'] + col_set['em_lsm'] +\
                           col_set['dolce'] + col_set['mean'] + \
                           col_set['median'], lsm + ['EM-LSM','DOLCE','Mean',
                                                     'Median'], 
                           markerLabelColor = 'b', circles = ticks, 
                           ticks = ticks, axismax = 0.12, markerSize = 15)
    fig.savefig(os.path.join(mg.path_out(), 'standard_metrics', 
                             'plot_target_' + str(year[0]) + '-' + \
                             str(year[-1]) + '_' + d + '.png'), dpi = 600.,
                bbox_inches = 'tight')
    plt.close(fig)


    # Taylor diagram.
    fig, ax = plt.subplots(figsize = (12, 12))
    plot_convenient_taylor(ax, obs, sim, ['k'] + col_set['lsm'] + \
                           col_set['em_lsm'] +\
                           col_set['dolce'] + col_set['mean'] + \
                           col_set['median'], ['Obs'] + lsm + \
                           ['EM-LSM','DOLCE','Mean', 'Median'], 
                           axismax = 0.13, tickCOR = np.arange(0.,1.1,0.2),
                           tickRMS = [0.,0.07,0.14], markerSize = 15)
    fig.savefig(os.path.join(mg.path_out(), 'standard_metrics', 
                             'plot_taylor_' + str(year[0]) + '-' + \
                             str(year[-1]) + '_' + d + '.png'), dpi = 600.,
                bbox_inches = 'tight')
    plt.close(fig)
