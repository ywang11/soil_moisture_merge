"""
Compute the following for the mean of the CMIP5 & CMIP6 models.

1. Global map (averaged over time): NetCDF
2. Global average time series.
3. Latitude bands average time series.
4. Continental average time series.
5. SREX region average time series.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import depth, year_cmip5, year_cmip6
from misc.standard_diagnostics import standard_diagnostics
import os
import pandas as pd




for i, d in enumerate(depth):
    for b in ['cmip5', 'cmip6']:
        x = os.path.join(mg.path_out(), b + '_unweighted_mean', 
                         'average_' + d + '.nc')
        data = xr.open_dataset(x, decode_times=False)

        if b == 'cmip5':
            year = year_cmip5
        else:
            year = year_cmip6
        time = pd.date_range(str(year[0])+'-01-01', str(year[-1])+'-12-31',
                             freq = 'MS')

        standard_diagnostics(data.sm.values, time, data.lat, data.lon, 
                             os.path.join(mg.path_out(), 
                                'standard_diagnostics_mean_cmip5cmip6'),
                             b + '_' + d)

    data.close()
