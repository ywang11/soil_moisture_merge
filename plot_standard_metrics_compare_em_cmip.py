"""
2019/07/23

ywang254@utk.edu

Compare the standard metrics between the different emergent constraint
approaches for the CMIP5/CMIP6 models.
"""
import os
import sys
import pandas as pd
import numpy as np
from utils_management.constants import depth, year_longest
import utils_management as mg
import matplotlib.pyplot as plt
from misc.ismn_utils import get_ismn_aggr_method
import itertools as it


# MODIFY
USonly = True # True, False
model_set = 'cmip5' # cmip5, cmip6

if USonly:
    suffix = '_USonly'
else:
    suffix = ''

year = year_longest

# Weighting methods
em_cmip_method = ['month_1grid', 'year_month_1grid', 'year_month_9grid',
                  'year_mw_1grid', 'year_mw_9grid',
                  'month_anomaly_1grid','year_month_anomaly_1grid',
                  'year_month_anomaly_9grid',
                  'year_mw_anomaly_1grid', 'year_mw_anomaly_9grid']


# Separate plot for each metric.
for metric in ['RMSE', 'Bias', 'uRMSE', 'Corr']:
    fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(10,10),
                             sharex=True, sharey=True)
    for d_ind, d in enumerate(depth):
        # Collect the results.
        metric_collect = {}
        for em in em_cmip_method:
            metric_collect[em] = pd.read_csv(os.path.join(mg.path_out(), 
                'standard_metrics', 'em_' + model_set + '_' + d + '_' + \
                str(year[0]) + '-' + str(year[-1]) + suffix + '.csv'),
            header = [0,1], index_col = [0,1] \
            ).loc[(slice(None), em), (metric, slice(None))].values
        # Plot the collected results.
        ax = axes.flat[d_ind]

        for em_ind, em in enumerate(em_cmip_method):
            bp = ax.boxplot(metric_collect[em].reshape(-1,1),
                            positions = [em_ind], patch_artist=True, 
                            whis = [5, 95])
            for element in ['boxes', 'whiskers', 'fliers', 'means', 
                            'caps']:
                plt.setp(bp[element], color='grey')
            plt.setp(bp['medians'], color = 'k')
            for patch in bp['boxes']:
                patch.set(facecolor = [0,0,0,0])

        ##if (metric == 'uRMSE') | (metric == 'RMSE'):
        ##    ax.set_ylim([0.07, 0.16]) # some UDEL are too big

        ax.set_xticks(range(len(em_cmip_method)))
        ax.set_xticklabels(em_cmip_method, rotation = 90.)
        ax.set_title('Depth: ' + d)
        ax.set_ylabel(metric)

    fig.savefig(os.path.join(mg.path_out(), 'standard_metrics', 
                             'plot_compare_em_' + model_set + '_' + \
                             metric + suffix + '.png'),
                dpi = 600., bbox_inches='tight')
    plt.close(fig)
