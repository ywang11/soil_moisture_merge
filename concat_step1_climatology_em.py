"""
20190917
ywang254@utk.edu

Step 1 of the CDF-matching merging:
 Calculate the monthly climatology (1981-2010) of the DOLCE-LSM products of 
 different time periods.
"""
import xarray as xr
import os
import pandas as pd
import numpy as np
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth_cm, target_lat, target_lon
import utils_management as mg
from misc.ismn_utils import get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
import itertools as it
import multiprocessing as mp


met_obs = 'CRU_v4.03'
em = 'year_month_anomaly_9grid' # best performing setup
prefix = ['predicted']


args_list = []
for i, fx in it.product(range(len(depth_cm)), prefix):
    args_list.append(['lsm', i, fx])
    args_list.append(['all', i, fx])


def clim(args):
    model_set, i, fx = args

    dcm = depth_cm[i]

    time_range = pd.date_range('1981-01-01', '2010-12-31', freq = 'MS')

    for year in [year_shorter, year_shorter2, year_shortest]:
        year_str = str(year[0]) + '-' + str(year[-1])

        data = xr.open_mfdataset([os.path.join(mg.path_out(),
                                               'em_' + model_set + '_corr',
                                               met_obs + '_' + \
                                               em.replace('anomaly',
                                                          'restored') + \
                                               '_' + dcm + '_' + year_str,
                                               fx + '_' + str(yr) + \
                                               '.nc') for yr in year],
                                 decode_times = False, concat_dim = 'time')

        data['time'] = pd.date_range(str(year[0]) + '-01-01', 
                                     str(year[-1]) + '-12-31', freq = 'MS')

        sm = data[fx].loc[(data.time.indexes['time'] >= time_range[0]) & \
                          (data.time.indexes['time'] <= time_range[-1]),
                          :, :].copy(deep = True)

        clim = sm.groupby('time.month').mean('time')

        data.close()

        print(fx)

        clim.to_dataset(name = fx).to_netcdf(os.path.join( \
            mg.path_out(), 'concat_em_' + model_set,
            'climatology_' + met_obs + '_' + em + '_' + \
            dcm + '_' + fx + '_' + year_str + '.nc'))


pool = mp.Pool(4)
pool.map_async(clim, args_list)
pool.close()
pool.join()
