"""
2020/01/17

Remove the < 0. and > 1. soil moisture values from the restored values.
"""
import xarray as xr
import os
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth, depth_cm
import utils_management as mg
import numpy as np
import itertools as it
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import pandas as pd
from misc.em_utils import rm_oor
import multiprocessing as mp


year_str = str(year_longest[0]) + '-' + str(year_longest[-1])
period = pd.date_range(str(year_longest[0]) + '-01-01',
                       str(year_longest[-1]) + '-12-31', freq = 'MS')
prod_list = ['lsm', 'all']
best = 'year_month_anomaly_9grid'


#for i, prod, prefix in it.product(range(4), prod_list,
#                                  ['predicted']):
def calc(option):
    i, prod, prefix = option

    d = depth[i]
    dcm = depth_cm[i]
    data = xr.open_dataset(os.path.join(mg.path_out(),
                                        'concat_em_' + prod,
                                        'concat_CRU_v4.03_' + best + \
                                        '_' + dcm + '_' + prefix + \
                                        '_' + year_str + '.nc'), \
                           decode_times = False)
    data[prefix].load()

    rm_oor(data[prefix], os.path.join(mg.path_out(), 'concat_em_' + prod),
           'positive_CRU_v4.03_' + best + '_' + dcm, 
           prefix + '_' + year_str, period)
    data.close()


p = mp.Pool(4)
p.map_async(calc, list(it.product(range(4), prod_list, 
                                  ['predicted'])))
p.close()
p.join()
