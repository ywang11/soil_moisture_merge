"""
2019/01/07
ywang254@utk.edu

Plot the monthly climatology and trend (w/ sig.) of latitudial mean 
 soil moisture, 1950-2016. No reference to ERA-Interim/GLEAM

Each panel becomes latitude by depth.
"""
import pandas as pd
from utils_management.constants import depth, depth_cm, year_longest, \
    year_shortest
import utils_management as mg
import itertools as it
import os
import numpy as np
from misc.plot_utils import stipple, cmap_gen
import matplotlib.pyplot as plt
import matplotlib as mpl


# Time period to calculate the climatology and trend on: 1950-2016.
year = year_longest
year_str = str(year[0]) + '-' + str(year[-1])
year_str2 = str(year_shortest[0]) + '-' + str(year_shortest[-1])
time_range = pd.date_range(start = str(year[0])+'-01-01',
                           end = str(year[-1])+'-12-31', freq = 'MS')

             
#data_list = [x+'_'+y for x in ['mean', 'dolce', 'em'] \
#             for y in ['lsm', 'cmip5', 'cmip6', '2cmip', 'all']]
data_list = ['mean_lsm', 'dolce_lsm',
             'em_lsm', 'em_cmip5', 'em_cmip6', 'em_2cmip', 'em_all']

#data_name = [x+'_'+y for x in ['Mean','OLC','EC'] \
#             for y in ['ORS', 'CMIP5', 'CMIP6', 'CMIP5+6', 'ALL']]
data_name = ['Mean-ORS', 'OLC-ORS', 'EC-ORS', 'EC-CMIP5', 'EC-CMIP6',
             'EC-2CMIP', 'EC-ALL']


lat_median = np.arange(78.75, -54.76, -0.5)
levels_clim = np.linspace(0.05, 0.46, 41)
levels_trend = np.linspace(-2., 2., 41)
cmap = 'Spectral'
cmap_anomaly = cmap_gen('autumn', 'winter_r')
months = [str(i) for i in range(1,13)]

mpl.rcParams['axes.titlesize'] = 10
for which in ['clim', 'trend']:
    fig, axes = plt.subplots(nrows = 6, ncols = len(data_list) * 2, 
                             figsize = (20, 18))
    #fig.subplots_adjust(hspace = 0.01, wspace = 0.01)

    if which == 'clim':
        axes[0, len(data_list)].text(0, -46, # -32,
                                     'Soil Moisture (m$^3$/m$^3$)',
                                     fontsize = 16)
    else:
        axes[0, len(data_list)].text(0, -46, # -32, 
                                     'Trend (10$^{-4}$ m$^3$/m$^3$/year)',
                                     fontsize = 16)

    for month in range(1, 13):
        for dind, data in enumerate(data_list):
            ax = axes.flat[(month-1)*len(data_list) + dind]

            collect = np.full([len(lat_median), len(depth)], 0.)
            collect_p = np.full([len(lat_median), len(depth)], 1.)

            for i,d in enumerate(depth):
                #if (data == 'dolce_all') & (i == 0):
                #    continue

                dcm = depth_cm[i]

                if which == 'clim':
                    collect[:,i] = pd.read_csv(os.path.join(mg.path_out(), 
                        'other_diagnostics', data, 'bylat_clim_' + d + '_' + \
                        year_str2 + '.csv'), index_col = 0 \
                    ).dropna(axis = 1, how = 'all').T.loc[::-1, str(month)]
                else:
                    collect[:,i] = pd.read_csv(os.path.join(mg.path_out(),
                        'other_diagnostics', data, 'bylat_trend_' + d + '_' + \
                        year_str + '.csv'), index_col = 0 \
                    ).dropna(axis = 0, how = 'all').loc[::-1, str(month)]

                    collect_p[:,i] = pd.read_csv(os.path.join(mg.path_out(), 
                        'other_diagnostics', data, 'bylat_trend_p_' + d + \
                        '_' + year_str + '.csv'), index_col = 0 \
                    ).dropna(axis = 0, how = 'all').loc[::-1, str(month)]

            if which == 'clim':
                h = ax.imshow(collect, aspect = 0.04,
                              vmin = levels_clim[0],
                              vmax = levels_clim[-1], cmap = cmap)
            else:
                h = ax.imshow(collect * 1e4, aspect = 0.04,
                              vmin = levels_trend[0],
                              vmax = levels_trend[-1], cmap = cmap_anomaly)
                stipple(ax, range(len(lat_median)), range(len(depth)),
                        collect_p)

            ax.set_ylabel(data_name[dind], labelpad = 0.01)
            if dind == (len(data_list) // 2):
                ax.set_title('Month ' + str(month))

            if (np.mod(month,2) == 1) & (dind == 0):
                ax.set_yticks(range(0, len(lat_median), 40))
                ax.set_yticklabels(['%.2f' % f for f in lat_median[::40]])
            else:
                ax.set_yticks([])
                ax.set_yticklabels([])
            ax.set_xticks(range(0, len(depth)))
            if (month == 11) | (month == 12):
                ax.set_xticklabels(range(1, len(depth)+1))
            else:
                ax.set_xticklabels([])

    if which == 'clim':
        cax = fig.add_axes([0.1, 0.05, 0.8, 0.02])
        fig.colorbar(h, cax = cax, boundaries = levels_clim,
                     orientation = 'horizontal')
    else:
        cax = fig.add_axes([0.1, 0.05, 0.8, 0.02])
        fig.colorbar(h, cax = cax, boundaries = levels_trend,
                     orientation = 'horizontal')

    fig.savefig(os.path.join(mg.path_out(), 'other_diagnostics_plot',
                             'bylat', 'bydepth_' + which + '.png'), dpi = 600.)
    plt.close(fig)
