"""
Compute the following for every emergent constraint product.

1. Global maps (averaged over time, and by season over time).
2. Global trends (all months, annual mean, seasonal mean).
3. Global average time series.
4. Latitude bands average time series.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth, depth_cm
from misc.standard_diagnostics import standard_diagnostics
import os
import pandas as pd


year = REPLACE1 # year_longest, year_shorter, year_shorter2, year_shortest
year_str = str(year[0]) + '-' + str(year[-1])
period = pd.date_range(str(year[0])+'-01-01', str(year[-1])+'-12-31',
                       freq = 'MS')

i = REPLACE2 # [0,1,2,3]
d = depth[i]
dcm = depth_cm[i]

model = 'REPLACE3' # 'lsm', 'cmip5', 'cmip6', '2cmip', 'all'

opt = 'REPLACE4' # ['year_month_9grid', 'year_month_1grid',
                 #  'year_month_anomaly_9grid', 'year_month_anomaly_1grid']

for prefix in ['predicted']:
    if ('anomaly' in opt) & (prefix == 'predicted'):
        fi = os.path.join(mg.path_out(), 'em_' + model + '_corr',
                          'CRU_v4.03_' + \
                          opt.replace('anomaly', 'positive') + '_' + \
                          dcm + '_' + year_str, prefix + '.nc')
    else:
        fi = [os.path.join(mg.path_out(), 'em_' + model + '_corr',
                           'CRU_v4.03_' + opt + '_' + dcm + '_' + year_str,
                           prefix + '_' + str(y) + '.nc') for y in year]

    data = xr.open_mfdataset(fi, decode_times=False, concat_dim = 'time')

    # year_depth should always be together
    standard_diagnostics(data[prefix].values, period, 
                         data.lat, data.lon, 
                         os.path.join(mg.path_out(),
                                      'standard_diagnostics_em_' + model),
                         'CRU_v4.03_' + prefix + \
                         '_' + opt + '_' + year_str + '_' + d)

    data.close()
