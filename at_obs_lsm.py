# -*- coding: utf-8 -*-
"""
Created on Thu May 16 19:19:40 2019

@author: ywang254

Because these take a long time to produce, extract the soil moisture from 
the individual land surface models, and from the ESA-CCI satellite data.
"""
import utils_management as mg
from utils_management.constants import depth, depth_cm, lsm_all, \
    year_longest, year_shortest
import os
import xarray as xr
from misc.ismn_utils import get_ismn_aggr_method, get_weighted_monthly_data
from misc.dolce_utils import get_cov_method, get_weighted_sm_points
from glob import glob
import pandas as pd
import time
import multiprocessing as mp


start = time.time()

land_mask = 'vanilla' # The land mask applied on the land surface models.

# Choose the weighting method that was used to generate the weighted averaged
# ISMN data.
# ---- default to LU-weighted, not cut off by minimum pct represented 
#      land cover
ismn_aggr_ind = REPLACE1 # 0, 1, 2
simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = get_ismn_aggr_method(simple[ismn_aggr_ind],
                                        dominance_lc[ismn_aggr_ind],
                                        dominance_threshold)

##for i,d in enumerate(depth):
i = REPLACE2
d = depth[i]

grid_latlon, _, _ = get_weighted_monthly_data(os.path.join( \
    mg.path_intrim_out(), 'ismn_aggr'), ismn_aggr_method, d, 'all')


def extract(l):
    # Choose the time frame to conduct the calculation. But selecting the
    # longest year already encompasses all the years.
    file_all = [os.path.join(mg.path_intrim_out(), 'Interp_Merge', 
                             land_mask, l, l + '_' + str(y) + '_' + \
                             depth_cm[i] + '.nc') for y in year_longest]

    file_list = [x for x in file_all if os.path.exists(x)]
    year = [year_longest[y] for y in range(len(year_longest)) if \
            os.path.exists(file_all[y])]

    print(file_list[0])

    if not file_list:
        #continue
        return None

    data = xr.open_mfdataset(file_list, decode_times=False)
    sm = data.sm.copy(deep=True)
    data.close()
    # ---- obtain values at the observed data points (do not subset
    #      the dates).
    sm_at_data = get_weighted_sm_points(grid_latlon, sm, year)
    sm_at_data.to_csv(os.path.join(mg.path_intrim_out(), 
                                   'at_obs_lsm', ismn_aggr_method + '_' + \
                                   l + '_' + d + '.csv'))


##for l in lsm_all:
##    extract(l)
pool = mp.Pool(mp.cpu_count())
pool.map_async(extract, lsm_all)
pool.close()
pool.join()
##extract('CLM4')

end = time.time()
print('The script finished in ' + ('%.4f' % ((end-start)/3600) ) + ' hours.')
