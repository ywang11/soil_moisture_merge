# -*- coding: utf-8 -*-
"""
Created on Thu May 30 16:25:28 2019

@author: ywang254

Extract the soil moisture and constraints data for the following
 bounding box (middle-US): [-100, -90, 33, 43]
"""
import utils_management as mg
from utils_management.constants import met_to_lsm, year_longest, \
    year_shorter, year_shorter2, year_shortest, lsm_list, depth, depth_cm, \
    target_lat, target_lon, year_cmip5, year_cmip6
from misc.spatial_utils import extract_grid
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import os
import xarray as xr
import pandas as pd
import numpy as np
import itertools as it
import time
import multiprocessing as mp


land_mask = 'vanilla'
bbox = [-100, -90, 33, 43]
grid = ['grid' + str(x) + '_' + str(y) \
        for x in range(1,21) for y in range(1,21)]

###############################################################################
# Settings
###############################################################################
# Target period of interest.
year = REPLACE1 # [year_longest, year_shorter, year_shorter2, year_shortest]
year_str = str(year[0]) + '-' + str(year[-1])
period = pd.date_range(str(year[0])+'-01-01', str(year[-1])+'-12-31',
                       freq = 'MS')

# Soil layer of interest.
i = REPLACE2 # [0, 1, 2, 3]
d = depth[i]
dcm = depth_cm[i]

# Land surface models relevant to this soil layer and target period.
model = 'REPLACE3' # 'lsm', 'cmip5', 'cmip6'
if model == 'lsm':
    model_list = lsm_list[(str(year[0])+'-'+str(year[-1]), d)]
elif model == 'cmip5':
    model_list = cmip5_availability(dcm, land_mask)
elif model == 'cmip6':
    model_list = list(set(mrsol_availability(dcm, land_mask, 'historical')) &\
                      set(mrsol_availability(dcm, land_mask, 'ssp585')))
    model_list = sorted([x for x in model_list if 'r1i1p1f1' in x])


###############################################################################
# Preparations
###############################################################################
# Prefix of the precipitation and temperature data that are to be used in
# the regression.
if model == 'lsm':
    met_list = []
    pr_path_list = [] 
    tas_path_list = []

    temp1 = mg.path_to_pr(land_mask)
    temp2 = mg.path_to_tas(land_mask)
    for met, value in met_to_lsm.items():
        for l in model_list:
            if l in value:
                met_list.append(met)
                pr_path_list.append(temp1[met])
                tas_path_list.append(temp2[met])
    met_list, ind = np.unique(met_list, return_index = True)
    met_list = list(met_list)
    pr_path_list = [pr_path_list[x] for x in ind]
    tas_path_list = [tas_path_list[x] for x in ind]
else:
    met_list = model_list
    pr_path_list = []
    tas_path_list = []
    for m in model_list:
        pr_path_list.append(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                         land_mask, model.upper(), m, 'pr_'))
        tas_path_list.append(os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                          land_mask, model.upper(), m, 'tas_'))


###############################################################################
# Read and write the regression data.
###############################################################################
for p_ind, p in enumerate(met_list):
    ################
    # Soil moisture.
    ################
    if model == 'lsm':
        mlist = list(set(met_to_lsm[p]) & set(model_list))
    else:
        mlist = [model_list[p_ind]]
    sm = pd.DataFrame(data = np.nan, index = period, columns = grid)
    for m_ind, m in enumerate(mlist):
        if model == 'lsm':
            flist = [os.path.join(mg.path_intrim_out(), 'Interp_Merge', 
                                  land_mask, m, m + '_' + str(y) + '_' + \
                                  dcm + '.nc') for y in year]
        elif model == 'cmip5':
            f_hist = [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                   land_mask, model.upper(), m,
                                   'sm_historical_r1i1p1_' + \
                                   str(y) + '_' + dcm + '.nc') \
                      for y in year_cmip5]
            f_85 = [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                 land_mask, model.upper(), m, 'sm_rcp85_' +\
                                 'r1i1p1_' + str(y) + '_' + dcm + '.nc') \
                    for y in range(year_cmip5[-1]+1, year[-1]+1)]
            flist = f_hist + f_85
        elif model == 'cmip6':
            f_hist = [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                   land_mask, model.upper(), m, 
                                   'mrsol_historical_' + str(y) + '_' + dcm + \
                                   '.nc') for y in year_cmip6]
            f_85 = [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                                 land_mask, model.upper(), m, 
                                 'mrsol_ssp585_' + \
                                 str(y) + '_' + dcm + '.nc') \
                    for y in range(year_cmip6[-1]+1, year[-1]+1)]
            flist = f_hist + f_85

        data = xr.open_mfdataset(flist, decode_times = False)
        if m_ind == 0:
            sm.loc[:, :] = data.sm.values[:, 
                                          (data.lat.values >= bbox[2]) & \
                                          (data.lat.values <= bbox[3]), 
                                          :][:, :, 
                                             (target_lon >= bbox[0]) & \
                                             (target_lon <= bbox[1]), 
                                          ].reshape(-1, len(grid)).copy()
        else:
            sm += data.sm.values[:, 
                                 (data.lat.values >= bbox[2]) & \
                                 (data.lat.values <= bbox[3]), 
                                 :][:, :,
                                    (target_lon >= bbox[0]) & \
                                    (target_lon <= bbox[1]) \
                                 ].reshape(-1, len(grid)).copy()
        data.close()
    sm /= len(mlist)
    sm.to_csv(os.path.join(mg.path_intrim_out(), 
                           'em_tas_corr_extract_test_area',
                           'sm_' + year_str + '_' + dcm + '_' + \
                           model + '_' + p + '.csv'))

    ################
    # Precipitation.
    ################
    pr = pd.DataFrame(data = np.nan, index = period, columns = grid)
    if model == 'lsm':
        flist = [pr_path_list[p_ind] + str(y) + '.nc' for y in year]
    elif model == 'cmip5':
        f_hist = [pr_path_list[p_ind] + 'historical_r1i1p1_' + str(y) + '.nc' \
                  for y in year_cmip5]
        f_85 = [pr_path_list[p_ind] + 'rcp85_r1i1p1_' + str(y) + '.nc' \
                for y in range(year_cmip5[-1]+1, year[-1]+1)]
        flist = f_hist + f_85
    elif model == 'cmip6':
        f_hist = [pr_path_list[p_ind] + 'historical_' + str(y) + '.nc' \
                  for y in year_cmip6]
        f_85 = [pr_path_list[p_ind] + 'ssp585_' + str(y) + '.nc' \
                for y in range(year_cmip6[-1]+1, year[-1]+1)]
        flist = f_hist + f_85

    data = xr.open_mfdataset(flist, decode_times = False)
    pr.loc[:, :] = data.pr.values[:, 
                                 (data.lat.values >= bbox[2]) & \
                                 (data.lat.values <= bbox[3]), 
                                 :][:, :,
                                    (target_lon >= bbox[0]) & \
                                    (target_lon <= bbox[1]) \
                                 ].reshape(-1, len(grid))

    data.close()
    pr.to_csv(os.path.join(mg.path_intrim_out(), 
                           'em_tas_corr_extract_test_area', 
                           'pr_' + year_str + '_' + dcm + '_' + \
                           model + '_' + p + '.csv'))


    ################
    # Temperature.
    ################
    tas = pd.DataFrame(data = np.nan, index = period, columns = grid)
    if model == 'lsm':
        flist = [tas_path_list[p_ind] + str(y) + '.nc' for y in year]
    elif model == 'cmip5':
        f_hist = [tas_path_list[p_ind] + 'historical_r1i1p1_' + \
                  str(y) + '.nc' for y in year_cmip5]
        f_85 = [tas_path_list[p_ind] + 'rcp85_r1i1p1_' + str(y) + '.nc' \
                for y in range(year_cmip5[-1]+1, year[-1]+1)]
        flist = f_hist + f_85
    elif model == 'cmip6':
        f_hist = [tas_path_list[p_ind] + 'historical_' + str(y) + '.nc' \
                  for y in year_cmip6]
        f_85 = [tas_path_list[p_ind] + 'ssp585_' + str(y) + '.nc' \
                for y in range(year_cmip6[-1]+1, year[-1]+1)]
        flist = f_hist + f_85

    data = xr.open_mfdataset(flist, decode_times = False)
    tas.loc[:, :] = data.tas.values[:, 
                                    (data.lat.values >= bbox[2]) & \
                                    (data.lat.values <= bbox[3]), 
                                    :][:, :,
                                       (target_lon >= bbox[0]) & \
                                       (target_lon <= bbox[1]) \
                                    ].reshape(-1, len(grid))
    data.close()
    tas.to_csv(os.path.join(mg.path_intrim_out(), 
                            'em_tas_corr_extract_test_area', 
                            'tas_' + year_str + '_' + dcm + '_' + \
                            model + '_' + p + '.csv'))
