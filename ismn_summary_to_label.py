"""
Obtain the land use and continent of the ISMN stations.
# No SREX region.
"""
from misc.ismn_utils import get_weighted_monthly_data, get_ismn_aggr_method
from utils_management.constants import depth, target_lat, target_lon
import utils_management as mg
import os
import itertools as it
import xarray as xr
import numpy.ma as ma
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors


###############################################################################
# Preliminary discrete datasets.
###############################################################################
# Load land cover base dataset.
data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 'lu_aggr',
                                    'mcd12c1_0.05_major.nc'))
land_use_modis = data.__xarray_dataarray_variable__.copy(deep = True).load()
data.close()


# Load continent dataset.
data = xr.open_dataset(os.path.join(mg.path_intrim_out(), 
                                    'natural_earth_continent_mask.nc'))
continent_mask = data.region.copy(deep = True).load()
continent_mask = continent_mask.where(~np.isnan(continent_mask.values), 999)
data.close()


###############################################################################
# Identify the land cover, SREX region, and continent at each ISMN station.
###############################################################################
simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = [get_ismn_aggr_method(simple[iam], dominance_lc[iam],
                                         dominance_threshold) \
                    for iam in range(len(simple))]


#
for opt, iam, d in it.product(['cal','val','all'], ismn_aggr_method, depth):
    grid_latlon, weighted_monthly_data, _ = get_weighted_monthly_data( \
        os.path.join(mg.path_intrim_out(), 'ismn_aggr'), iam, d, opt)


    # Creat pandas dataframe.
    ismn_summary = pd.DataFrame(data = np.nan, 
                                index = grid_latlon.columns, 
                                columns = ['Lat', 'Lon', 'MODIS Land Use',
                                           'Continent'])
    ismn_summary.loc[:, 'Lat'] = grid_latlon.loc['Lat', :]
    ismn_summary.loc[:, 'Lon'] = grid_latlon.loc['Lon', :]


    # Fill the pandas dataframe row-by-row.
    for c in weighted_monthly_data.columns:
        if sum(~np.isnan(weighted_monthly_data[c].values)) > 0:
            ismn_summary.loc[c, 'MODIS Land Use'] = \
                land_use_modis.sel(lat = grid_latlon.loc['Lat',c],
                                   lon = grid_latlon.loc['Lon',c],
                                   method = 'nearest')
            ismn_summary.loc[c, 'Continent'] = \
                continent_mask.sel(lat = grid_latlon.loc['Lat',c],
                                   lon = grid_latlon.loc['Lon',c],
                                   method = 'nearest')

    # Save to file.
    ismn_summary.to_csv(os.path.join(mg.path_intrim_out(), 'ismn_summary',
                                     'ismn_landuse_continent_' + d + \
                                     '_' + iam + '_' + opt + '.csv'))
