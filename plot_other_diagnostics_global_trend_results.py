"""
2019/08/06
ywang254@utk.edu

Make the x-y plots of the monthly trend of the mean soil moisture of 
 LSM + CMIP5 + CMIP6, and of the OLC-Products product. For the same period
 as the reference.
"""
import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import utils_management as mg
from utils_management.constants import depth, depth_cm, lsm_list
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import os
import numpy as np

mpl.rcParams['font.size'] = 7
land_mask = 'vanilla'

figsize = (8, 8)

year_str = '1950-2016'

fig, axes = plt.subplots(nrows = 4, ncols = 2, figsize = figsize, 
                         sharex = True, sharey = True)
fig.subplots_adjust(wspace = 0)

# (1) Mean-All
for d_ind, d in enumerate(depth):
    dcm = depth_cm[d_ind]
    ax = axes[d_ind,0]
    data = pd.read_csv(os.path.join(mg.path_out(), 'other_diagnostics',
                                'meanmedian_all',
                                'trend_mean_' + year_str + \
                                '_' + depth[0] + '.csv'), index_col = 0)
    for i,c in enumerate(data.columns):
        if data.loc['p_value', c] < 0.05:
            b1 = ax.bar(i, data.loc['trend', c], color = 'k', edgecolor = 'k')
        else:
            b2 = ax.bar(i, data.loc['trend', c], color = 'w', edgecolor = 'k')

    ########################################
    # Display the percentage of source datasets that agree on the sign of trend
    ########################################
    #
    lsm = lsm_list[(year_str, d)]
    #
    cmip5 = cmip5_availability(dcm, land_mask)
    #
    cmip6_list_1 = mrsol_availability(dcm, land_mask, 'historical')
    cmip6_list_2 = mrsol_availability(dcm, land_mask, 'ssp585')
    cmip6_list_3 = one_layer_availability('pr', land_mask, 'historical')
    cmip6_list_4 = one_layer_availability('pr', land_mask, 'ssp585')
    # ---- further subset to the given depth.
    cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) & \
                       set(cmip6_list_3) & set(cmip6_list_4) )
    # ---- further subset to the first ensemble member.
    cmip6_list = [x for x in cmip6_list if 'r1i1p1f1' in x]
    #
    data_src = pd.DataFrame(data = np.nan, columns = data.columns, 
                            index = lsm + cmip5 + cmip6_list)
    #
    for l in lsm_list[(year_str, d)]:
        if l == 'ESA-CCI':
            continue # Skip because no reliable global mean trend.
        temp = pd.read_csv(os.path.join(mg.path_out(), 'other_diagnostics',
                                        'lsm', 'trend_' + l + '_' + \
                                        year_str + '_' + d + '.csv'), 
                           index_col = 0)
        data_src.loc[l, :] = temp.loc['trend', :]
    for f, cc in zip(['cmip5', 'cmip6'], [cmip5, cmip6_list]):
        for c in cc:
            temp = pd.read_csv(os.path.join(mg.path_out(), 'other_diagnostics',
                                            f, 'trend_' + c + '_' + \
                                            year_str + '_' + d + '.csv'),
                               index_col = 0)
            data_src.loc[c, :] = temp.loc['trend', :]
    #
    for i,c in enumerate(data_src.columns):
        if data.loc['trend', c] < 0:
            pct_agree = np.sum(data_src[c] < 0) / data_src.shape[0] * 100
            ax.text(i - 0.5, data.loc['trend', c] - 0.00003,
                    '%.1f' % pct_agree, fontdict = {'fontsize': 6})
        else:
            pct_agree = np.sum(data_src[c] > 0) / data_src.shape[0] * 100
            ax.text(i - 0.5, data.loc['trend', c] + 0.00003,
                    '%.1f' % pct_agree, fontdict = {'fontsize': 6})

    ax.set_xticks(range(len(data.columns)))
    ax.set_xticklabels(data.columns)
    ax.set_ylabel('Trend (m$^3$/m$^3$/year)')
    ax.set_ylim([-0.0003, 0.0001])
    ax.set_title('Mean-All ' + dcm)
    if d_ind == 3:
        ax.set_xlabel('Month')

# Legend
ax.legend([b1, b2], ['p < 0.05', r'p $\geq$ 0.05'], loc = 'lower center',
          bbox_to_anchor = (1, -0.5), ncol = 2)


# (2) OLC-Products
for d_ind, d in enumerate(depth):
    dcm = depth_cm[d_ind]

    data = pd.read_csv(os.path.join(mg.path_out(), 'other_diagnostics',
                                    'dolce_products', 
                                    'trend_' + year_str + \
                                    '_' + d + '.csv'), index_col = 0)
    ax = axes[d_ind, 1]
    for i,c in enumerate(data.columns):
        if data.loc['p_value', c] < 0.05:
            b1 = ax.bar(i, data.loc['trend', c], color = 'k', edgecolor = 'k')
        else:
            b2 = ax.bar(i, data.loc['trend', c], color = 'w', edgecolor = 'k')
    ax.set_xticks(range(len(data.columns)))
    ax.set_xticklabels(data.columns)

    if d_ind == 3:
        ax.set_xlabel('Month')
    ax.set_title('OLC-Products ' + dcm)

fig.savefig(os.path.join(mg.path_out(), 'other_diagnostics_plot', 
                         'global_trend_reference_result.png'), dpi = 600.,
            bbox_inches = 'tight')
