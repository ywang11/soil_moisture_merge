"""
2020/05/21

ywang254@utk.edu

Plot the global latitudinal trend in soil moisture of the selected products.
1981-2010 for compatibility with GLEAM and ERA-Land.
"""
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
import cartopy.crs as ccrs
import utils_management as mg
from utils_management.constants import depth_cm, depth, target_lat, \
    target_lon, year_longest
import os
import pandas as pd
import xarray as xr
from misc.plot_utils import cmap_gen
import statsmodels.api as sm
import numpy as np
import itertools as it
from scipy.stats import pearsonr


year = year_longest
iam = 'lu_weighted'
cov = 'ShrunkCovariance'
met = 'CRU_v4.03'
me = 'year_month_anomaly_9grid'


prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all']
prod_names = ['Mean ORS', 'OLC ORS', 'EC ORS', 'EC CMIP5', 'EC CMIP6',
              'EC CMIP5+6', 'EC ALL']


levels = np.linspace(-0.001, 0.001, 21)
cmap = cmap_gen('autumn', 'winter_r')
mpl.rcParams['font.size'] = 5
mpl.rcParams['axes.titlesize'] = 5
mpl.rcParams['hatch.linewidth'] = 0.5


month = list('JFMAMJJASOND')
which_x = [str(i) for i in range(1,13)]
which_y = [('%.1f' % x) + '-' + ('%.1f' % (x+0.5)) for x in \
           np.arange(-55., 78.6, 0.5)]
which_ytick = np.where([np.mod(x, 20) < 1e-6 for x in \
                        np.arange(-55.0, 78.6, 0.5)])[0]
which_yticklabels = []
for x in np.arange(-55., 78.6, 0.5):
    if np.mod(x, 20) < 1e-6:
        if x > 0:
            which_yticklabels.append(str(int(x)) + '$^o$N')
        elif np.abs(x) < 1e-6:
            which_yticklabels.append('0$^o$')
        else:
            which_yticklabels.append(str(int(x))[1:] + '$^o$S')


fig, axes = plt.subplots(nrows = len(prod_list), ncols = 5,
                         figsize = (6.5,7),
                         sharex = True, sharey = True)
fig.subplots_adjust(hspace = 0.01, wspace = 0.)
for pind, prod in enumerate(prod_list):
    ###########################################################################
    # (1)/(2) 1981-2010 trend, 0-10cm/10-30cm
    ###########################################################################
    coef_collect = {}
    for i in range(2):
        d = depth[i]
        dcm = depth_cm[i]
    
        ax = axes[pind, i]
        ax.spines['top'].set_visible(True)
        ax.spines['bottom'].set_visible(True)
        ax.spines['left'].set_visible(True)
        ax.spines['right'].set_visible(True)
        ax.spines['top'].set_lw(0.5)
        ax.spines['bottom'].set_lw(0.5)
        ax.spines['left'].set_lw(0.5)
        ax.spines['right'].set_lw(0.5)
        ax.set_aspect(0.033)

        fname = os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                             'bylat_trend', prod + '_' + d + '_1981-2010.csv')
        coef = pd.read_csv(fname, index_col = 0).loc[which_y, which_x]
        coef_collect[dcm] = coef

        cf1 = ax.contourf(range(coef.shape[1]), range(coef.shape[0]),
                          coef.values, cmap = cmap, levels = levels,
                          extend = 'both')
        if pind == 0:
            ax.set_title('Trend\n' + dcm)
        if pind == (len(prod_list)-1):
            ax.set_xticks(np.linspace(0.3, 11.7, 12))
            ax.set_xticklabels(month)
        else:
            ax.tick_params('x', length = 0)
        if i == 0:
            ax.text(-0.43, 0.5, prod_names[pind], verticalalignment='center',
                    rotation = 90, transform = ax.transAxes)
            ax.set_yticks(which_ytick)
            ax.set_yticklabels(which_yticklabels)
        else:
            ax.tick_params('y', length = 0)
        ax.set_xlim([0, coef.shape[1]-1])
        ax.set_ylim([0, coef.shape[0]-1])

    ###########################################################################
    # (3)/(4)/(5) Difference from GLEAM/ERA-Land with hatch for the difference
    #             being within 95% CI
    ###########################################################################
    for rind, ref, d, dcm in zip(range(3),
                                 ['GLEAMv3.3a', 'ERA-Land', 'ERA-Land'],
                                 ['0.00-0.10','0.00-0.10','0.10-0.30'],
                                 ['0-10cm','0-10cm','10-30cm']):
        ax = axes[pind, 2 + rind]
        ax.spines['top'].set_visible(True)
        ax.spines['bottom'].set_visible(True)
        ax.spines['left'].set_visible(True)
        ax.spines['right'].set_visible(True)
        ax.spines['top'].set_lw(0.5)
        ax.spines['bottom'].set_lw(0.5)
        ax.spines['left'].set_lw(0.5)
        ax.spines['right'].set_lw(0.5)
        ax.set_aspect(0.033)

        fname = os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                             'bylat_trend', ref + '_' + d + '_1981-2010.csv')
        coef_obs = pd.read_csv(fname, index_col = 0).loc[which_y, which_x]

        cf3 = ax.contourf(range(coef_obs.shape[1]), range(coef_obs.shape[0]),
                          coef_collect[dcm].values - coef_obs.values,
                          cmap = cmap,
                          levels = levels, extend = 'both')

        # ---- Pearson correlation between sm and obs
        x = coef_obs.values.reshape(-1)
        y = coef_collect[dcm].values.reshape(-1)
        temp = ~(np.isnan(x) | np.isnan(y))
        r, pval = pearsonr(x[temp], y[temp])
        ax.text(0.02, 0.85, r'$\rho$=' + ('%.3f' % r),
                transform = ax.transAxes)

        if pind == 0:
            ax.set_title('Product - ' + ref + '\n' + dcm)
        if pind == (len(prod_list)-1):
            ax.set_xticks(np.linspace(0.3, 11.7, 12))
            ax.set_xticklabels(month)
        else:
            ax.tick_params('x', length = 0)
        ax.tick_params('y', length = 0)
        ax.set_xlim([0, coef.shape[1]-1])
        ax.set_ylim([0, coef.shape[0]-1])

cax = fig.add_axes([0.1, 0.06, 0.8, 0.01])
plt.colorbar(cf1, cax = cax, boundaries = levels, orientation = 'horizontal',
             ticks = levels[::2])

fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                         'bylat_trend_all.png'),
            dpi = 600, bbox_inches = 'tight')
plt.close()
