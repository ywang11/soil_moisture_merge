"""
2019/07/24

ywang254@utk.edu

Draw the global maps of the percentage of years that are constrained in
 year_month setup of the em method, by temperature, precipitation, or either.
"""
import numpy as np
import xarray as xr
import utils_management as mg
from utils_management.constants import depth, year_longest, year_shorter, \
    year_shorter2, year_shortest, depth_cm, target_lat, target_lon
import sys
import os
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from misc.plot_utils import plot_sm_map, plot_map_w_stipple
import itertools as it
from misc.plot_utils import cmap_gen
import multiprocessing as mp
from glob import glob


cmap = 'Spectral'
level = np.linspace(-0.05, 1.05, 12)


# MODIFY
met_obs = 'CRU_v4.03'

#for model_set, i in it.product(['lsm', 'cmip5', 'cmip6'], range(4)):
def plotter(option):
    model_set, i = option

    if model_set == 'lsm':
        year_list = [year_longest, year_shorter, year_shorter2, year_shortest]
        em_method = ['year_mw_9grid']
                    #['year_month_anomaly_9grid', 'month_anomaly_1grid', 
                    # 'year_mw_anomaly_1grid', 
                    # 'year_mw_9grid', 'year_mw_anomaly_9grid']
    else:
        year_list = [year_longest]
        em_method = ['year_mw_9grid']
                    #['year_month_anomaly_9grid', 'month_anomaly_1grid',
                    # 'year_mw_anomaly_1grid', 
                    # 'year_mw_9grid', 'year_mw_anomaly_9grid']

    d = depth[i]
    dcm = depth_cm[i]

    for year, me in it.product(year_list, em_method):
        year_str = str(year[0]) + '-' + str(year[-1])
    
        if 'year_month' in me:
            flist = sorted(glob(os.path.join(mg.path_out(), 
                                             'em_' + model_set + '_corr',
                                             met_obs + '_' + me + '_' + dcm + \
                                             '_' + year_str,
                                             'em_not_applied*.nc')))
            data = xr.open_mfdataset(flist, concat_dim = 'time')
            em_not_applied = data.em_not_applied.values.copy()
            data.close()
        elif 'year_mw' in me:
            em_not_applied = np.full([2, len(year)*12, len(target_lat),
                                      len(target_lon)], np.nan)
            for m in range(12):
                data = xr.open_dataset(os.path.join(mg.path_out(), 'em_' + 
                                                    model_set + '_corr',
                                                    met_obs + '_' + me + '_' +\
                                                    dcm + '_' + year_str,
                                                    'em_not_applied_' + \
                                                    str(m) + '.nc'))
                em_not_applied[:, m::12, :, 
                               :] = data.em_not_applied.values.copy()
                data.close()
        else:
            em_not_applied = np.full([2, 12, len(target_lat),
                                      len(target_lon)], np.nan)
            for m in range(12):
                data = xr.open_dataset(os.path.join(mg.path_out(), 'em_' + \
                                                    model_set + '_corr', 
                                                    met_obs + '_' + me + '_' +\
                                                    dcm + '_' + year_str,
                                                    'em_not_applied_' + \
                                                    str(m) + '.nc'))
                em_not_applied[:, m, :, :] = data.em_not_applied.values.copy()
                data.close()


        # Make a plot of the corr. for precipitation and temperature
        # for each month. Only the significant correlations are counted.
        # 1 - precipitation, 2 - temperature
        for reg, rname in zip([1,2,3], ['precipitation', 'temperature', 
                                        'both']): 
            fig, axes = plt.subplots(nrows = 4, ncols = 3, 
                                     figsize = (25,15), 
                                     subplot_kw = {'projection': \
                                                   ccrs.PlateCarree()})
            for ax_ind, ax in enumerate(axes.flat):
                # Convert to percentage of years that have significant 
                # regression coefficients (em_not_applied == 0)
                if reg < 3:
                    pct_em_applied = np.mean(np.abs(em_not_applied[reg-1, 
                        ax_ind::12, :, :] - 0.) < 1e-3, axis = 0)
                else:
                    pct_em_applied = np.mean(np.max(np.abs( \
                        em_not_applied[:, ax_ind::12, :, :] - 0.) < 1e-3,
                                                    axis = 0), axis = 0)

                pct_em_applied[np.isnan(em_not_applied[0, 0, :, :])] = np.nan
    
                ax.coastlines()
                ax.set_extent([-180, 180, -60, 90])
    
                h = ax.contourf(data.lon.values, data.lat.values, 
                                pct_em_applied, levels = level, 
                                cmap = cmap)
                plt.colorbar(h, ax = ax, boundaries = level,
                             ticks = np.linspace(0., 1., 11))
                ax.set_title('Month = ' + str(ax_ind))
            fig.savefig(os.path.join(mg.path_out(), 
                                     'plot_em_significance', 
                                     'em_not_applied_summary', model_set,
                                     rname + '_' + met_obs + '_' + \
                                     me + '_' + dcm + '_' + \
                                     year_str + '.png'),
                        dpi=600., bbox_inches = 'tight')
            plt.close(fig)

pool = mp.Pool(4)
pool.map_async(plotter, list(it.product(['lsm', 'cmip5', 'cmip6'], range(4))))
pool.close()
pool.join()
