# -*- coding: utf-8 -*-
"""
Created on Wed May  1 10:20:40 2019

@author: ywang254

Source: 
  Hobeichi et al. - 2018 - Derived Optimal Linear Combination 
    Evapotranspiration (DOLCE) A Global Gridded Synthesis Estimate

Apply DOLCE-based weighting on the OLS and emergent constraint products:
  - land surface model & reanalysis & satellite
  - cmip5
  - cmip6

Get the products' soil moisture at the observed grid cells from the netcdf
 files.
Calculate the errors between between each product and the grid-aggregated
 observations.
Calculate the weights for each of the product from the error covariance
 matrix (A): w = A^(-1) * 1 / (1^T * A^(-1) * 1)

Note:
  Use scikit-learn covariance estimation because presumably more accurate
    than the pandas function. 
  https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.cov.html

  Test different covariance estimators for final performance on the root-mean-
  squared-error of the merged soil moisture: 
    (0) the ordinary estimator - 
        sklearn.covariance.EmpiricalCovariance
            https://scikit-learn.org/stable/modules/generated/sklearn.covariance.EmpiricalCovariance.html
    (1) the regularized estimator, use default shrink parameter - 
        sklearn.covariance.ShrunkCovariance
            https://scikit-learn.org/stable/modules/generated/sklearn.covariance.ShrunkCovariance.html#sklearn.covariance.ShrunkCovariance
    (2) a robust estimator suited for unimodally distributed variables - 
        sklearn.covariance.MinCovDet
            https://scikit-learn.org/stable/modules/generated/sklearn.covariance.MinCovDet.html#sklearn.covariance.MinCovDet
    (3) additional regularized estimator 1 - 
        sklearn.covariance.LedoitWolf
            https://scikit-learn.org/stable/modules/generated/sklearn.covariance.LedoitWolf.html
    (4) additional regularized estimator 2 - 
        sklearn.covariance.OAS
            https://scikit-learn.org/stable/modules/generated/sklearn.covariance.OAS.html

  Removal of the mean soil moisture from the products' soil moisture is
only at the dates and locations that have observations.
"""
import os
import utils_management as mg
import pandas as pd
from utils_management.constants import depth, depth_cm, year_longest
from misc.dolce_utils import get_cov_method
from misc.ismn_utils import get_weighted_monthly_data, get_ismn_aggr_method
import numpy as np
import sklearn.covariance as sk_cov
import time


###############################################################################
# Setup the methods to prepare the observation method, and to compare the
# results.
###############################################################################
# Choose the weighting method that was used to generate the weighted averaged
# ISMN data.
ismn_aggr_ind = REPLACE # 0, 1, 2
simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = get_ismn_aggr_method(simple[ismn_aggr_ind],
                                        dominance_lc[ismn_aggr_ind],
                                        dominance_threshold)

# Remove the long-term average from the source datasets or not.
remove_mean = True # this is consistent with the original paper

# Test of different covariance estimators: [0, 1, 2, 3, 4]
cov_estimer = [0, 1, 2, 3, 4]

# The source products to merge. Note: the lsm uses the concated product.
dolce_method = {'dolce_lsm': 'ShrunkCovariance', # ismn_aggr_method follows
                'dolce_cmip5': 'ShrunkCovariance', # this script.
                'dolce_cmip6': 'ShrunkCovariance'}
# Concat is bad. Use 1950-2016.
em_method = {'em_lsm': 'year_month_anomaly_9grid',
             'em_cmip5': 'year_month_anomaly_9grid',
             'em_cmip6': 'year_month_anomaly_9grid'}

year = year_longest
time_range = pd.date_range(start=str(min(year))+'-01-01',
                           end=str(max(year))+'-12-31', freq='MS')
year_str = str(year[0]) + '-' + str(year[-1])


start = time.time()
for i,d in enumerate(depth):
    dcm = depth_cm[i]

    #######################################################################
    # Load the observational data, calibration period.
    #######################################################################
    _, weighted_monthly_data, _ = \
        get_weighted_monthly_data(os.path.join(mg.path_intrim_out(), 
                                               'ismn_aggr'), 
                                  ismn_aggr_method, d, opt = 'cal')

    # Remove the long-term mean from the weighted monthly data.
    if remove_mean:
        weighted_monthly_data = weighted_monthly_data - \
                                weighted_monthly_data.mean(axis = 0)

    # Subset the weighted monthly data to the time_range.
    weighted_monthly_data = weighted_monthly_data.loc[ \
        (weighted_monthly_data.index >= time_range[0]) & \
        (weighted_monthly_data.index <= time_range[-1]), :]


    #######################################################################
    # Load the difference between the products and observation.
    #######################################################################
    lsm_err = pd.DataFrame(data = np.nan,
                           index = range(weighted_monthly_data.shape[0] * \
                                         weighted_monthly_data.shape[1]),
                           columns = list(dolce_method.keys()) + \
                                     list(em_method.keys()))
    for l in dolce_method.keys():
        if l == 'dolce_lsm':
            lsm_monthly_sm = pd.read_csv(os.path.join(mg.path_out(), 
                'at_obs_concat_' + l, d + '_' + ismn_aggr_method + '_' + \
                dolce_method[l] + '.csv'),
                index_col = 0, parse_dates = True)
        else:
            lsm_monthly_sm = pd.read_csv(os.path.join(mg.path_out(), 
                'at_obs_' + l, 'weighted_average_' + year_str + '_' + \
                d + '_' + ismn_aggr_method + '_' + dolce_method[l] + '.csv'),
                index_col = 0, parse_dates = True)

        # Subset the emergent constrait data to time_range.
        lsm_monthly_sm = lsm_monthly_sm.loc[weighted_monthly_data.index, :]

        # Mask the month_sm data to only where weighted_monthly_data
        # has values.
        lsm_monthly_sm[np.isnan(weighted_monthly_data.values)] = np.nan

        # Remove the long-term mean. The long-term mean is only 
        # calculated on the days that correspond to the grid-scale observation.
        if remove_mean:
            lsm_monthly_sm = lsm_monthly_sm - lsm_monthly_sm.mean(axis = 0)

        # The masking of non-calibration values is automatic.
        lsm_err.loc[:,l] = (lsm_monthly_sm - \
                            weighted_monthly_data).values.reshape(-1)
    for l in em_method.keys():
        if l == 'em_lsm':
            lsm_monthly_sm = pd.read_csv(os.path.join(mg.path_out(),
                'at_obs_concat_' + l, 'CRU_v4.03_' + em_method[l] + '_' + \
                dcm + '_' + ismn_aggr_method + '.csv'), index_col = 0,
                                         parse_dates = True)
        else:
            lsm_monthly_sm = pd.read_csv(os.path.join(mg.path_out(), 
                'at_obs_' + l, 'CRU_v4.03_' + em_method[l] + '_' + d + '_' + \
                year_str + '_' + ismn_aggr_method + '.csv'), index_col = 0,
                                         parse_dates = True)

        # Subset the emergent constrait data to time_range.
        lsm_monthly_sm = lsm_monthly_sm.loc[weighted_monthly_data.index, :]

        # Mask the month_sm data to only where weighted_monthly_data
        # has values.
        lsm_monthly_sm[np.isnan(weighted_monthly_data.values)] = np.nan

        # Remove the long-term mean. The long-term mean is only 
        # calculated on the days that correspond to the grid-scale observation.
        if remove_mean:
            lsm_monthly_sm = lsm_monthly_sm - lsm_monthly_sm.mean(axis = 0)

        # The masking of non-calibration values is automatic.
        lsm_err.loc[:,l] = (lsm_monthly_sm - \
                            weighted_monthly_data).values.reshape(-1)


    #######################################################################
    # Estimate the error covariance matrix between the land surface models.
    #######################################################################
    # ---- drop nan's in the errors of the individual land surface models
    lsm_err.dropna(axis=0, how='any', inplace=True)

    for cov in cov_estimer:
        if cov == 0:
            cov_func = sk_cov.EmpiricalCovariance(store_precision = True)
        elif cov == 1:
            cov_func = sk_cov.ShrunkCovariance(store_precision = True, 
                                               shrinkage = 0.1)
        elif cov == 2:
            cov_func = sk_cov.MinCovDet(store_precision = True, 
                                        random_state = 999)
        elif cov == 3:
            cov_func = sk_cov.LedoitWolf(store_precision = True, 
                                         block_size = 1000)
        elif cov == 4:
            cov_func = sk_cov.OAS(store_precision = True)
        else:
            raise Exception('Unrecognized option cov_estimer = ' + \
                            str(cov))

        A = cov_func.fit(lsm_err.values)

        ###################################################################
        # Calculate the weights and write to .csv file.
        ###################################################################
        temp = np.dot(A.precision_, np.ones(lsm_err.shape[1]))
        w = temp / sum(temp)

        method = get_cov_method(cov)

        suffix = ismn_aggr_method + '_' + year_str + '_' + method + '_' + d
        if remove_mean: 
            suffix = suffix + '_remove_mean'

        pd.DataFrame(w, index = lsm_err.columns,
                     columns=['Weight']).to_csv( \
            os.path.join(mg.path_intrim_out(), 'dolce_products_weights', 
                         'weights_' + suffix + '.csv') )


end = time.time()
print('The script finished in ' + ('%.4f' % ((end-start)/3600) ) + ' hours.')
