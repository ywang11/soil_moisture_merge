"""
20200520

ywang254@utk.edu

For each depth, annual/season, anomaly/absolute values, plot in 9 panels:
  - mean_lsm, with original models' envelop
  - concat_dolce_lsm with uncertainty (lu_weighted, ShrunkCovariance)
  - em_lsm with uncertainty (CRU_v4.03, year_month_anomaly_9grid)
  - em_cmip5 with uncertainty (CRU_v4.03, year_month_anomaly_9grid)
  - em_cmip6 with uncertainty (CRU_v4.03, year_month_anomaly_9grid)
  - em_2cmip with uncertainty (CRU_v4.03, year_month_anomaly_9grid)
  - em_all with uncertainty (CRU_v4.03, year_month_anomaly_9grid)
"""
from utils_management.constants import year_longest, depth, depth_cm, \
    lsm_list, year_cmip5, year_cmip6
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability, one_layer_availability
import utils_management as mg
import numpy as np
import xarray as xr
import pandas as pd
import os
import sys
import matplotlib.pyplot as plt
from matplotlib import cm
from misc.plot_utils import plot_ts_trend, plot_ts_shade
import itertools as it
import multiprocessing as mp
import matplotlib as mpl
from scipy.stats import linregress


year_str = str(year_longest[0]) + '-' + str(year_longest[-1])
period = pd.date_range(str(year_longest[0])+'-01-01', 
                       str(year_longest[-1])+'-12-31', freq = 'MS')
land_mask = 'vanilla'


prod_list = ['mean_lsm', 'dolce_lsm', 'em_lsm', 'em_cmip5', 'em_cmip6',
             'em_2cmip', 'em_all']
prod_name = ['Mean ORS', 'OLC ORS', 'EC ORS', 'EC CMIP5', 'EC CMIP6',
             'EC CMIP5+6', 'EC ALL']


season_list = ['Annual', 'DJF', 'MAM', 'JJA', 'SON']


anomaly = True


def seasonal(data_frame, season, anomaly):
    if isinstance(season, str):
        if season == 'DJF':
            data_frame = data_frame.loc[data_frame.index.quarter == 1, :]
        elif season == 'MAM':
            data_frame = data_frame.loc[data_frame.index.quarter == 2, :]
        elif season == 'JJA':
            data_frame = data_frame.loc[data_frame.index.quarter == 3, :]
        elif season == 'SON':
            data_frame = data_frame.loc[data_frame.index.quarter == 4, :]
        else:
            data_frame = data_frame
    else:
        # Month
        data_frame = data_frame.loc[data_frame.index.month == season, :]
    data_frame = data_frame.groupby(data_frame.index.year).mean()
    if anomaly:
        data_frame = data_frame - data_frame.mean(axis = 0)
    return data_frame


def plot_ts_trend(ax, time, ts, txt_x, txt_y):
    """
    Plot the time series with trend.
    """
    h1, = ax.plot(time, ts, 'k-', lw = 0.5)
    slope, intercept, _, _, _ = linregress(range(30),
                                           ts[(time >= 1981) & \
                                              (time <= 2010)])
    h2, = ax.plot(range(1981,2011),
                  np.arange(0, 30) * slope + intercept, '-k', lw = 0.5)
    return h1, h2, slope


mpl.rcParams['font.size'] = 5
mpl.rcParams['axes.titlesize'] = 5
fig, axes = plt.subplots(nrows = len(prod_list) * 2,
                         ncols = len(season_list),
                         sharex = True, sharey = True, figsize = (6.5, 7.5))
fig.subplots_adjust(wspace = 0., hspace = 0.)
for sind, season in enumerate(season_list):
    for i in range(2):
        d = depth[i]
        dcm = depth_cm[i]

        ##########################
        # Reference datasets
        ##########################        
        if i == 0:
            data_gleam = pd.read_csv(os.path.join(mg.path_out(),
                                                  'standard_diagnostics_lsm',
                                                  'GLEAMv3.3a_' + d + \
                                                  '_g_ts.csv'),
                                     index_col = 0, parse_dates = True)
            data_gleam = data_gleam.loc[(data_gleam.index.year >= 1950) & \
                                        (data_gleam.index.year <= 2016), :]
            data_gleam = seasonal(data_gleam, season, anomaly).iloc[:, 0]
            data_gleam = data_gleam.loc[1981:2016]

            slope_gleam, _, _, _, _ = linregress(range(30),
                data_gleam.loc[1981:2010].values)
        if (i == 0) | (i == 1):
            data_era = pd.read_csv(os.path.join(mg.path_out(),
                                                'standard_diagnostics_lsm',
                                                'ERA-Land_' + d + '_g_ts.csv'),
                                   index_col = 0, parse_dates = True)
            data_era = data_era.loc[(data_era.index.year >= 1950) & \
                                    (data_era.index.year <= 2016), :]
            data_era = seasonal(data_era, season, anomaly).iloc[:, 0]
            slope_era, _, _, _, _ = linregress(range(30),
                data_era.loc[1981:2010].values)


        ##########################
        # Products
        ##########################
        collect_mean = pd.DataFrame(np.nan, index = range(1950, 2017),
                                    columns = prod_list)
        for prod in prod_list:
            if prod == 'mean_lsm':
                data = pd.read_csv(os.path.join(mg.path_out(),
                    'standard_diagnostics_meanmedian_lsm',
                    'mean_1950-2016_' + d + '_g_ts.csv'), index_col = 0,
                                   parse_dates = True)
            elif prod == 'dolce_lsm':
                data = pd.read_csv(os.path.join(mg.path_out(),
                    'standard_diagnostics_concat', prod,
                    'dolce_average_lu_weighted_ShrunkCovariance_' + d + \
                                                '_g_ts.csv'),
                                   index_col = 0, parse_dates = True)
            elif (prod == 'em_lsm') | (prod == 'em_all'):
                data = pd.read_csv(os.path.join(mg.path_out(),
                    'standard_diagnostics_concat', prod, 'CRU_v4.03_' + \
                    'year_month_anomaly_9grid_predicted_' + d + \
                    '_g_ts.csv'), index_col = 0, parse_dates = True)
            else:
                data = pd.read_csv(os.path.join(mg.path_out(),
                    'standard_diagnostics_' + prod, 'CRU_v4.03_' + \
                    'predicted_year_month_anomaly_9grid_1950-2016_' + d + \
                    '_g_ts.csv'), index_col = 0, parse_dates = True)

            temp0 = seasonal(data, season, False)
            collect_mean.loc[:, prod] = temp0
            if anomaly:
                collect_mean.loc[:, prod] -= np.mean(collect_mean.loc[:, prod])


        ##########################
        # Plot the result.
        ##########################
        for c_ind, c in enumerate(collect_mean.columns):
            ax = axes[c_ind + len(prod_list)*i, sind]
            ax.spines['top'].set_visible(True)
            ax.spines['bottom'].set_visible(True)
            ax.spines['left'].set_visible(True)
            ax.spines['right'].set_visible(True)
            ax.spines['top'].set_lw(0.5)
            ax.spines['bottom'].set_lw(0.5)
            ax.spines['left'].set_lw(0.5)
            ax.spines['right'].set_lw(0.5)

            h1, _, slope = plot_ts_trend(ax, collect_mean.index,
                                          collect_mean[c], 0.1, 0.1)
            #ax.text(0.01, 0.9, '{:2e}'.format(slope),
            #        transform = ax.transAxes)
            if slope > 0:
                ax.text(0.05, 0.82, '+', transform = ax.transAxes,
                        fontsize = 7)
            else:
                ax.text(0.05, 0.82, '-', transform = ax.transAxes,
                        fontsize = 7)
            if i == 0:
                h3, = ax.plot(data_gleam.index, data_gleam, 'b', lw = 0.5)
                #ax.text(0.1, 0.9, '{:2e}'.format(slope_gleam), color = 'b',
                #        transform = ax.transAxes)
                if slope_gleam > 0:
                    ax.text(0.14, 0.82, '+', color = 'b',
                            transform = ax.transAxes, fontsize = 7)
                else:
                    ax.text(0.14, 0.82, '-', color = 'b',
                            transform = ax.transAxes, fontsize = 7)
            if (i == 0) | (i == 1):
                h4, = ax.plot(data_era.index, data_era, 'r', lw = 0.5)
                #ax.text(0.2, 0.9, '{:2e}'.format(slope_era), color = 'r',
                #        transform = ax.transAxes)
                if slope_era > 0:
                    ax.text(0.23, 0.82, '+', color = 'r',
                            transform = ax.transAxes, fontsize = 7)
                else:
                    ax.text(0.23, 0.82, '-', color = 'r',
                            transform = ax.transAxes, fontsize = 7)
            if sind == 0:
                if c_ind == 3:
                    ax.set_ylabel(dcm + '\n' + prod_name[c_ind])
                else:
                    ax.set_ylabel(dcm + '\n' + prod_name[c_ind])
            if (c_ind == 0) & (i == 0):
                ax.set_title(season)
            if sind > 0:
                ax.tick_params('y', length = 0)
ax.legend([h1, h3, h4], ['Product', 'GLEAMv3.3a', 'ERA-Land'], ncol = 3,
          bbox_to_anchor = (-1.5,-0.5))
fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                         'global_compare2.png'), dpi = 600.,
            bbox_inches = 'tight')
