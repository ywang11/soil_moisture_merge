"""
2020/12/8
ywang254@utk.edu

US drought 1985-1992
Australian millenium drought 2002-2009

Spinoni et al. (2019) A new global database of meteorological drought events from 1951 to 2016. Journal of Hydrology: Regional Studies. 22: 100593.

The soil moisture and scPDSI values are anomalies relative to 1971-2014,
 the overlapping time period between the two datasets.
"""
import os
import pandas as pd
import numpy as np
import xarray as xr
import utils_management as mg
from utils_management.constants import year_longest, depth, depth_cm
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.gridspec import GridSpec, GridSpecFromSubplotSpec
from misc.plot_utils import cmap_div
import cartopy.crs as ccrs
from scipy.stats import spearmanr


#
def get_flist(name, d, dcm):
    if name == 'scPDSI':
        flist = [os.path.join(mg.path_intrim_out(), 'Interp_Merge',
                              'vanilla', 'scPDSI', 'scPDSI.nc')]
    elif name == 'mean_lsm':
        flist = [os.path.join(mg.path_out(), 'meanmedian_lsm',
                              'mean_' + d + '_' + \
                              str(year_longest[0]) + '-' + \
                              str(year_longest[-1]) + '.nc')]
    elif name == 'dolce_lsm':
        flist = [os.path.join(mg.path_out(), 'concat_dolce_lsm',
                              'positive_average_' + d + \
                              '_lu_weighted_ShrunkCovariance.nc')]
    elif (name == 'em_all') | (name == 'em_lsm'):
        flist = [os.path.join(mg.path_out(), 'concat_' + name,
                              'positive_CRU_v4.03' + \
                              '_year_month_anomaly_9grid_' + \
                              dcm + '_predicted_' + \
                              str(year_longest[0]) + '-' + \
                              str(year_longest[-1]) + '.nc')]
    else:
        flist = [os.path.join(mg.path_out(), name + '_corr',
                              'CRU_v4.03_year_month_positive_' + \
                              '9grid_' + dcm + '_' + \
                              str(year_longest[0]) + \
                              '-' + str(year_longest[-1]),
                              'predicted.nc')]
    return flist

        
def decode_month_since(time):
    ref = pd.Timestamp(time.attrs['units'].split(' ')[2])
    start = ref + pd.Timedelta(time.values[0], unit = 'M')
    start = start.replace(day = 1, hour = 0, minute = 0, second = 0)
    return pd.date_range(start, periods = len(time), freq = 'MS')


def get_data(name, bbox, byrng, d = None, dcm = None):
    flist = get_flist(name, d, dcm)
    hr = xr.open_mfdataset(flist, decode_times = False)
    if ('CRU_v4.03_year_month_positive_9grid_0-10cm_1950-2016' \
        in flist[0]) |\
       ('CRU_v4.03_year_month_positive_9grid_10-30cm_1950-2016' \
        in flist[0]) |\
       ('CRU_v4.03_year_month_positive_9grid_30-50cm_1950-2016' \
        in flist[0]) |\
       ('CRU_v4.03_year_month_positive_9grid_50-100cm_1950-2016' \
        in flist[0]):
        hr['time'].attrs['units'] = 'months since 1950-01-01'
    if 'month' in hr['time'].attrs['units']:
        tvec = decode_month_since(hr['time'])
    else:
        tvec = xr.decode_cf(hr)['time'].to_index()
    try:
        sm = hr['sm']
    except:
        sm = hr['predicted']
    sm['time'] = tvec
    # ---- remove 1970-2016 climatology
    sm = sm - sm[(tvec.year >= 1970) & (tvec.year <= 2016), :,
                :].mean(dim = 'time')
    sm = sm[(tvec.year >= byrng[0]) & (tvec.year <= byrng[-1]), : , :]
    sm = sm[:, (hr['lat'] >= bbox[1]) & (hr['lat'] <= bbox[-1]), :]
    sm = sm[:, :, (hr['lon'] >= bbox[0]) & (hr['lon'] <= bbox[2])]
    sm = sm.groupby('time.year').mean()
    hr.close()

    return sm


def plot_row(row_axes, data, levels, show_year = False):
    cmap = cmap_div()
    for ind, ax in enumerate(row_axes):
        ax.coastlines(lw = 0.5)
        cf = ax.contourf(data['lon'], data['lat'],
                         data.values[ind, :, :], cmap = cmap_div(),
                         levels = levels,
                         transform = ccrs.PlateCarree(),
                         extend = 'both')
        if show_year:
            ax.set_title(data['year'].values[ind])
    return cf


# lon1, lat1, lon2, lat2
bounding_box = {'US': [-125.3,24.7,-66.3,49.6],
                'Australia': [110.41,-39.58,157.21,-10.23]}
bounding_year = {'US': range(1985, 1993),
                 'Australia': range(2002, 2010)}
prod_list = ['mean_lsm', 'em_lsm', 'dolce_lsm', 'em_cmip5',
             'em_cmip6', 'em_2cmip', 'em_all']
prod_name_list = ['Mean ORS', 'OLC ORS', 'EC ORS', 'EC CMIP5',
                  'EC CMIP6', 'EC CMIP5+6', 'EC ALL']
lab = 'abcdefghijklmnopqrstuvwxyzαβγδεζηθικλμνξøπρςτυφχψω'


# MODIFY
d = '0.00-1.00' # '0.00-0.10', '0.00-1.00'
dcm = '0-100cm' # '0-10cm', '0-100cm'


mpl.rcParams['font.size'] = 5.5
mpl.rcParams['axes.titlesize'] = 5.5
for region in ['US', 'Australia']:
    if region == 'US':
        proj = ccrs.AzimuthalEquidistant(central_longitude = -96.0,
                                         central_latitude = 35.)
    else:
        proj = ccrs.PlateCarree()

    fig = plt.figure(figsize = (6.5, 6))
    gs = GridSpec(2, 1, height_ratios = [1.3, 7])
    sub_gs1 = GridSpecFromSubplotSpec(1, 8, gs[0])
    sub_gs2 = GridSpecFromSubplotSpec(7, 8, gs[1])
    axes = np.empty([8, 8], dtype = object)
    for ii in range(8):
        axes[0, ii] = fig.add_subplot(sub_gs1[ii], projection = proj)
        for jj in range(7):
            axes[jj+1, ii] = fig.add_subplot(sub_gs2[jj,ii],
                                             projection = proj)

    #
    data0 = get_data('scPDSI',
                    bounding_box[region], bounding_year[region])
    cf = plot_row(axes[0, :], data0,
                  np.linspace(-0.4,0.4,41), show_year = True)
    axes[0,0].text(-0.25, 0.25, 'scPDSI',
                   transform = axes[0,0].transAxes, rotation = 90)
    cax = fig.add_axes([0.15, 0.77, 0.7, 0.01])
    plt.colorbar(cf, cax = cax, orientation = 'horizontal')

    #
    for ii in range(1, len(prod_list) + 1):
        if d == '0.00-0.10':
            data = get_data(prod_list[ii-1],
                            bounding_box[region], bounding_year[region],
                            d, dcm)
        else:
            for dind2, d2 in enumerate(depth):
                dcm = depth_cm[dind2]
                data = get_data(prod_list[ii-1],
                                bounding_box[region],
                                bounding_year[region], d2, dcm)
                if dind2 == 0:
                    data_tot = data * 0.1
                else:
                    thickness = float(d.split('-')[1]) - \
                        float(d.split('-')[0])
                    data_tot += data * thickness
            data = data_tot
            del data_tot

        cf = plot_row(axes[ii,:], data,
                      np.linspace(-0.05, 0.05, 41),
                      show_year = False)
        axes[ii,0].text(-0.25, 0.3, prod_name_list[ii-1],
                        transform = axes[ii,0].transAxes,
                        rotation = 90, verticalalignment = 'center')

        for ind in range(data.shape[0]):
            x = data0.values[ind, :, :].reshape(-1)
            y = data.values[ind, :, :].reshape(-1)
            temp = ~(np.isnan(x) | np.isnan(y))
            x = x[temp]
            y = y[temp]
            corr, pvalue = spearmanr(x, y, axis = 0)
            text = '%.3f' % corr
            if pvalue <= 0.05:
                text += '*'
            axes[ii,ind].text(0.6, 1.1, text,
                              transform = axes[ii,ind].transAxes,
                              horizontalalignment = 'center')

    for ii in range(8):
        for jj in range(8):
            axes[ii,jj].text(-0.05, 1.1, '(' + lab[ii] + str(jj) + ')',
                             transform = axes[ii,jj].transAxes)
    cax = fig.add_axes([0.15, 0.08, 0.7, 0.01])
    plt.colorbar(cf, cax = cax, orientation = 'horizontal')

    #
    fig.savefig(os.path.join(mg.path_out(), 'validate',
                             'drought_events_' + region + '_' + \
                             dcm + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)
