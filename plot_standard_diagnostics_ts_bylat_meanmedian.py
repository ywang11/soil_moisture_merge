"""
2019/08/05
ywang254@utk.edu

Plot the time series of latitudinal mean soil moisture of the
 mean and median results. Show the GLEAMv3.3 and ERA-Land time series of 
 global mean annual mean soil moisture for comparison.
"""
import matplotlib.pyplot as plt
import utils_management as mg
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth, depth_cm, lsm_list
from misc.plot_utils import plot_ts_shade, plot_ts_trend, cmap_gen
from misc.standard_diagnostics import seasonal_avg2
from misc.cmip5_availability import cmip5_availability
from misc.cmip6_utils import mrsol_availability
import pandas as pd
import os
import numpy as np
import itertools as it


# MODIFY
model_set = '2cmip' # 'lsm', 'cmip5', 'cmip6', '2cmip', 'all'

if model_set == 'lsm': 
    title = 'ORS'
elif model_set == 'cmip5':
    title = 'CMIP5'
elif model_set == 'cmip6':
    title = 'CMIP6'
elif model_set == '2cmip':
    title = 'CMIP5 + CMIP6'
else:
    title = 'ORS + CMIP5 + CMIP6'

if (model_set == 'lsm') | (model_set == 'all'):
    year_list = [year_longest, year_shorter, year_shorter2, year_shortest]
else:
    year_list = [year_longest]


# Plotting function.
def plotter(dict_, fig_name, year, dict_anomaly = None):
    lat_median = np.arange(78.75, -54.76, -0.5)
    levels = np.linspace(0., 0.6, 31)
    levels_anomaly = np.linspace(-0.02, 0.02, 21)
    cmap = 'Spectral'
    cmap_anomaly = cmap_gen('autumn', 'winter_r')

    fig, axes = plt.subplots(nrows = 5, ncols = 2, sharex = True,
                             sharey = False, figsize = (10, 6.5))
    fig.subplots_adjust(hspace = 0., wspace = 0.35)
    axes[0, 0].set_title('Soil Moisture (m$^3$/m$^3$)')
    axes[0, 1].set_title('Anomaly (m$^3$/m$^3$)')

    for ind, season in enumerate(['annual', 'DJF', 'MAM', 'JJA', 'SON']):
        val = dict_[season].iloc[:, ::-1].T

        # (1) Soil moisture
        ax = axes[ind, 0]
        h1 = ax.contourf(val.columns, lat_median, val, levels = levels,
                         cmap = cmap, extend = 'both')
        ax.set_yticks([-50, 0, 50])
        ax.set_yticklabels(['50S','0','50N'])
        ax.set_xlim([year[0], year[-1]])
        ax.set_ylabel(season)

        # (2) Soil moisture anomaly
        ax = axes[ind, 1]
        if dict_anomaly == None:
            h2 = ax.contourf(val.columns, lat_median,
                             val - val.mean(axis = 1).values.reshape(-1,1),
                             levels = levels_anomaly, cmap = cmap_anomaly, 
                             extend = 'both')
        else:
            val2 = dict_anomaly[season].iloc[:, ::-1].T
            h2 = ax.contourf(val.columns, lat_median, val2, 
                             levels = levels_anomaly,
                             cmap = cmap_anomaly, extend = 'both')
        ax.set_yticks([-50, 0, 50])
        #ax.set_yticklabels(['50S','0','50N'])
        ax.set_yticklabels([])
        ax.set_xlim([year[0], year[-1]])

    cax = fig.add_axes([0.47, 0.1, 0.02, 0.8])
    fig.colorbar(h1, cax = cax, boundaries = levels)

    cax = fig.add_axes([0.92, 0.1, 0.02, 0.8])
    fig.colorbar(h2, cax = cax, boundaries = levels_anomaly)

    fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot', 
                             'ts_bylat', 'meanmedian', fig_name + '_' + \
                             model_set + '_' + year_str + '_' + d + '.png'),
                dpi = 600., bbox_inches = 'tight')
    plt.close(fig)


###############################################################################
for i in range(4):
    d = depth[i]
    dcm = depth_cm[i]

    ###########################################################################
    # Mean/median soil moisture.
    ###########################################################################
    global_ts_all = {'mean': {}, 'median': {}}
    for year in year_list:
        d = depth[i]
        dcm = depth_cm[i]
        year_str = str(year[0]) + '-' + str(year[-1])
    
        #
        mean = pd.read_csv(os.path.join(mg.path_out(), 
                                        'standard_diagnostics_meanmedian_' + \
                                        model_set, 'mean_' + year_str + '_' \
                                        + d + '_g_lat_ts.csv'), index_col = 0, 
                           parse_dates = True).dropna(axis = 1, how = 'all')
        median = pd.read_csv(os.path.join(mg.path_out(), 
                                          'standard_diagnostics_meanmedian_' \
                                          + model_set, 
                                          'median_' + year_str + '_' + d + \
                                          '_g_lat_ts.csv'), index_col = 0,
                             parse_dates = True).dropna(axis = 1, how = 'all')
    
        global_ts_all['mean'][year_str] = seasonal_avg2(mean)
        global_ts_all['median'][year_str] = seasonal_avg2(median)
        
    ###########################################################################
    # Reference data.
    ###########################################################################
    # GLEAM v3.3a is only available for 0-10cm.
    if (d == '0.00-0.10'):
        gleam = pd.read_csv(os.path.join(mg.path_out(),
                                         'standard_diagnostics_lsm', 
                                         'GLEAMv3.3a_0.00-0.10_g_lat_ts.csv'),
                            index_col = 0, 
                            parse_dates = True).dropna(axis = 1, how = 'all')
        gleam_ts_all = seasonal_avg2(gleam)

    # ERA-Land is only available for the two shallow depths.
    eraland_ts_all = {}
    if (d == '0.00-0.10') | (d == '0.10-0.30'):
        temp = pd.read_csv(os.path.join(mg.path_out(),
                                        'standard_diagnostics_lsm',
                                        'ERA-Land_' + d + '_g_lat_ts.csv'),
                           index_col = 0, 
                           parse_dates = True).dropna(axis = 1, how = 'all')
        eraland_ts_all[d] = seasonal_avg2(temp)
    
    ###########################################################################
    # Build 5% to 95% intervals of the absolute values and the anomalies of 
    # raw data.
    ###########################################################################
    land_mask = 'vanilla'
    raw_ts_all = {}
    raw_ts_all_anomaly = {}
    for year in year_list:
        d = depth[i]
        dcm = depth_cm[i]
        year_str = str(year[0]) + '-' + str(year[-1])
    
        temp_all = {'annual': {}, 'DJF': {}, 'MAM': {}, 'JJA': {}, 'SON': {}}
        if model_set == 'lsm':
            model_list = lsm_list[(year_str, d)]
            for m in model_list:
                temp = seasonal_avg2(pd.read_csv(os.path.join(mg.path_out(),
                    'standard_diagnostics_' + model_set, m + '_' + d + \
                    '_g_lat_ts.csv'), index_col = 0, 
                    parse_dates = True).dropna(axis = 1, how = 'all'))
                for k in temp.keys():
                    temp_all[k][m] = temp[k].loc[year, :]
                    temp_all[k][m] = temp_all[k][m].unstack()
        elif model_set == 'cmip5':
            model_list = cmip5_availability(dcm, land_mask)
            for m in model_list:
                temp = seasonal_avg2(pd.read_csv(os.path.join(mg.path_out(),
                    'standard_diagnostics_' + model_set, m + '_' + dcm + \
                    '_g_lat_ts.csv'), index_col = 0, 
                    parse_dates = True).dropna(axis = 1, how = 'all'))
                for k in temp.keys():
                    temp_all[k][m] = temp[k].loc[year, :]
                    temp_all[k][m] = temp_all[k][m].unstack()
        elif model_set == 'cmip6':
            cmip6_list_1 = mrsol_availability(dcm, land_mask, 'historical')
            cmip6_list_2 = mrsol_availability(dcm, land_mask, 'ssp585')
            cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) )
            model_list = [x for x in cmip6_list if 'r1i1p1f1' in x]
            for m in model_list:
                temp = seasonal_avg2(pd.read_csv(os.path.join(mg.path_out(),
                    'standard_diagnostics_' + model_set, 'mrsol_' + \
                    m + '_' + dcm + '_g_lat_ts.csv'),  index_col = 0, 
                    parse_dates = True).dropna(axis = 1, how = 'all'))
                for k in temp.keys():
                    temp_all[k][m] = temp[k].loc[year, :]
                    temp_all[k][m] = temp_all[k][m].unstack()
        elif model_set == '2cmip':
            #
            model_list = cmip5_availability(dcm, land_mask)
            for m in model_list:
                temp = seasonal_avg2(pd.read_csv(os.path.join(mg.path_out(),
                    'standard_diagnostics_cmip5', m + '_' + dcm + \
                    '_g_lat_ts.csv'), index_col = 0, 
                    parse_dates = True).dropna(axis = 1, how = 'all'))
                for k in temp.keys():
                    temp_all[k][m] = temp[k].loc[year, :]
                    temp_all[k][m] = temp_all[k][m].unstack()
            #
            cmip6_list_1 = mrsol_availability(dcm, land_mask, 'historical')
            cmip6_list_2 = mrsol_availability(dcm, land_mask, 'ssp585')
            cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) )
            model_list = [x for x in cmip6_list if 'r1i1p1f1' in x]
            for m in model_list:
                temp = seasonal_avg2(pd.read_csv(os.path.join(mg.path_out(),
                    'standard_diagnostics_cmip6', 'mrsol_' + \
                    m + '_' + dcm + '_g_lat_ts.csv'),  index_col = 0, 
                    parse_dates = True).dropna(axis = 1, how = 'all'))
                for k in temp.keys():
                    temp_all[k][m] = temp[k].loc[year, :]
                    temp_all[k][m] = temp_all[k][m].unstack()
        elif model_set == 'all':
            #
            model_list = lsm_list[(year_str, d)]
            for m in model_list:
                temp = seasonal_avg2(pd.read_csv(os.path.join(mg.path_out(),
                    'standard_diagnostics_lsm', m + '_' + d + \
                    '_g_lat_ts.csv'), index_col = 0, 
                    parse_dates = True).dropna(axis = 1, how = 'all'))
                for k in temp.keys():
                    temp_all[k][m] = temp[k].loc[year, :]
                    temp_all[k][m] = temp_all[k][m].unstack()
            #
            model_list = cmip5_availability(dcm, land_mask)
            for m in model_list:
                temp = seasonal_avg2(pd.read_csv(os.path.join(mg.path_out(),
                    'standard_diagnostics_cmip5', m + '_' + dcm + \
                    '_g_lat_ts.csv'), index_col = 0, 
                    parse_dates = True).dropna(axis = 1, how = 'all'))
                for k in temp.keys():
                    temp_all[k][m] = temp[k].loc[year, :]
                    temp_all[k][m] = temp_all[k][m].unstack()
            #
            cmip6_list_1 = mrsol_availability(dcm, land_mask, 'historical')
            cmip6_list_2 = mrsol_availability(dcm, land_mask, 'ssp585')
            cmip6_list = list( set(cmip6_list_1) & set(cmip6_list_2) )
            model_list = [x for x in cmip6_list if 'r1i1p1f1' in x]
            for m in model_list:
                temp = seasonal_avg2(pd.read_csv(os.path.join(mg.path_out(),
                    'standard_diagnostics_cmip6', 'mrsol_' + \
                    m + '_' + dcm + '_g_lat_ts.csv'),  index_col = 0, 
                    parse_dates = True).dropna(axis = 1, how = 'all'))
                for k in temp.keys():
                    temp_all[k][m] = temp[k].loc[year, :]
                    temp_all[k][m] = temp_all[k][m].unstack()
    
    
        for k in temp_all.keys():
            temp_all[k] = pd.DataFrame(temp_all[k])
    
        # absolute values
        raw_ts_all[year_str] = {}
        raw_ts_all[year_str]['min'] = {}
        raw_ts_all[year_str]['max'] = {}
    
        # anomalies
        raw_ts_all_anomaly[year_str] = {}
        raw_ts_all_anomaly[year_str]['min'] = {}
        raw_ts_all_anomaly[year_str]['max'] = {}
    
        for k in temp_all.keys():
            temp = temp_all[k]
    
            raw_ts_all[year_str]['min'][k] = \
                pd.Series(np.nanpercentile(temp, 2.5, axis = 1), 
                          index = temp.index).unstack().T
            raw_ts_all[year_str]['max'][k] = \
                pd.Series(np.nanpercentile(temp, 97.5, axis = 1),
                          index = temp.index).unstack().T
    
            temp2 = temp.reorder_levels([1, 0]).unstack()
            temp2 = temp2 - temp2.mean(axis = 0)
            temp2 = temp2.stack(dropna = False).reorder_levels([1,0])
    
            raw_ts_all_anomaly[year_str]['min'][k] = \
                pd.Series(np.nanpercentile(temp2, 2.5, axis = 1),
                          index = temp2.index).unstack().T
            raw_ts_all_anomaly[year_str]['max'][k] = \
                pd.Series(np.nanpercentile(temp2, 97.5, axis = 1),
                          index = temp2.index).unstack().T

    for year in year_list:
        year_str = str(year[0]) + '-' + str(year[-1])

        for stat in ['mean', 'median']:
            plotter(global_ts_all[stat][year_str], stat, year)

        for stat in ['min', 'max']:
            plotter(raw_ts_all[year_str][stat], stat, year,
                    raw_ts_all_anomaly[year_str][stat])

        if d == '0.00-0.10':
            plotter(gleam_ts_all, 'GLEAMv3.3a', year)
            plotter(eraland_ts_all[d], 'ERA-Land', year)
        elif d == '0.10-0.30':
            plotter(eraland_ts_all[d], 'ERA-Land', year)
