"""
2020/08/08
ywang254@utk.edu

Aggregate SMAP to monthly.
"""
import os
import h5py
import xarray as xr
import utils_management as mg
from glob import glob
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import cartopy.crs as ccrs

# ['2015' + ('%02d' % x) for x in range(4,13)] + \
month_list = ['2016' + ('%02d' % x) for x in range(1,13)]

for mon in month_list:
    flist = sorted(glob(os.path.join(mg.path_data(), 'SMAP_L4',
                                     'SMAP_L4_SM_gph_' + mon + '*.h5')))

    # Temp!
    flist = flist[:3]

    for find, f in enumerate(flist):
        hf = h5py.File(f, 'r')

        x1 = hf['Geophysical_Data']['sm_surface']
        x2 = hf['Geophysical_Data']['sm_rootzone']

        if find == 0:
            lat = hf['cell_lat'][()][:, 0]
            lon = hf['cell_lon'][()][0, :]
            sm_sf_collect = xr.DataArray(data = np.nan,
                coords = {'time': pd.DatetimeIndex([mon + '01']),
                          'lat': lat, 'lon': lon},
                dims = ['time', 'lat', 'lon'],
                attrs = {'_FillValue': x1.attrs['_FillValue'],
                         'long_name': x1.attrs['long_name'].decode('UTF-8'),
                         'units': x1.attrs['units'].decode('UTF-8'),
                         'valid_max': x1.attrs['valid_max'],
                         'valid_min': x1.attrs['valid_min']})
            sm_rz_collect = xr.DataArray(data = np.nan,
                coords = {'time': pd.DatetimeIndex([mon + '01']),
                          'lat': lat, 'lon': lon},
                dims = ['time', 'lat', 'lon'],
                attrs = {'_FillValue': x2.attrs['_FillValue'],
                         'long_name': x2.attrs['long_name'].decode('UTF-8'),
                         'units': x2.attrs['units'].decode('UTF-8'),
                         'valid_max': x2.attrs['valid_max'],
                         'valid_min': x2.attrs['valid_min']})

            sm_sf_collect[0, :, :] = x1[()]
            sm_rz_collect[0, :, :] = x2[()]
        else:
            sm_sf_collect[0, :, :] += x1[()]
            sm_rz_collect[0, :, :] += x2[()]

        hf.close()

    sm_sf_collect /= len(flist)
    sm_rz_collect /= len(flist)

    sm_sf_collect.to_dataset(name = 'sm').to_netcdf(os.path.join( \
        mg.path_intrim_out(), 'SMAP_L4', 'SMAP_L4_0-5cm_' + mon + '.nc'))
    sm_rz_collect.to_dataset(name = 'sm').to_netcdf(os.path.join( \
        mg.path_intrim_out(), 'SMAP_L4', 'SMAP_L4_0-100cm_' + mon + '.nc'))

    sm_sf_collect = sm_sf_collect.where(sm_sf_collect > -1)
    sm_rz_collect = sm_rz_collect.where(sm_sf_collect > -1)

    fig, axes = plt.subplots(2, 1, figsize = (6.5, 6.5),
                             subplot_kw = {'projection': ccrs.Miller()})

    min_val = min(np.nanmin(sm_sf_collect.values),
                  np.nanmin(sm_sf_collect.values))
    max_val = max(np.nanmax(sm_rz_collect.values),
                  np.nanmax(sm_rz_collect.values))

    ax = axes[0]
    cf = ax.contourf(sm_sf_collect.lon, sm_sf_collect.lat,
                     sm_sf_collect[0,:,:], cmap = 'Spectral',
                     vmin = min_val, vmax = max_val)
    plt.colorbar(cf, ax = ax, shrink = 0.7)
    ax.set_title('Surface')

    ax = axes[1]
    cf = ax.contourf(sm_rz_collect.lon, sm_rz_collect.lat,
                     sm_rz_collect[0,:,:], cmap = 'Spectral',
                     vmin = min_val, vmax = max_val)
    plt.colorbar(cf, ax = ax, shrink = 0.7)
    ax.set_title('Root Zone')

    fig.savefig(os.path.join(mg.path_intrim_out(), 'SMAP_L4',
                             'SMAP_L4_' + mon + '.png'), dpi = 600.,
                bbox_inches = 'tight')
    plt.close(fig)
