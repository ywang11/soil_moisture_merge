"""
2019/08/05

ywang254@utk.edu

Plot the min, max, mean of the climatology and trend of the CMIP6 models.
For the trend, also plot the number of significant positive & negative trends.
"""
import xarray as xr
import utils_management as mg
from utils_management.constants import year_cmip6
import matplotlib.pyplot as plt
import numpy as np
import cartopy.crs as ccrs
from misc.plot_utils import plot_map_w_stipple, plot_sm_map
import os


expr = 'hist-nat' # 'historical', 'hist-nat', 'hist-GHG'
var = 'mrsos'

year = year_cmip6


# Climatology
data = xr.open_dataset(os.path.join(mg.path_out(),
                                    'standard_diagnostics_cmip6',
                                    var + '_' + expr + '_' + \
                                    str(year_cmip6[0]) + '-' + \
                                    str(year_cmip6[-1]) + '_summary_mean.nc'))

fig, axes = plt.subplots(nrows = 3, ncols = 1, figsize = (12, 16),
                         subplot_kw = {'projection': ccrs.PlateCarree()})

for ax, stat in zip(axes.flat, ['mean', 'max', 'min']):
    plot_sm_map(ax, data[stat], data.lat, data.lon, add_cyc = True,
                levels = np.linspace(0., 0.60, 15))
    ax.set_title(stat)
fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                         'summary_cmip6', 'climatolgy_' + var + '_' + \
                         expr + '.png'), dpi=600., bbox_inches = 'tight')
plt.close(fig)

data.close()


# Trend
levels = np.linspace(-0.001, 0.001, 11)
cmap = 'RdYlBu'

data = xr.open_dataset(os.path.join(mg.path_out(),
                                    'standard_diagnostics_cmip6',
                                    var + '_' + expr + '_' + \
                                    str(year_cmip6[0]) + '-' + \
                                    str(year_cmip6[-1]) + '_summary_trend.nc'))

# ---- summary statistics
fig, axes = plt.subplots(nrows = 3, ncols = 1, figsize = (12, 16),
                         subplot_kw = {'projection': ccrs.PlateCarree()})

for ax, stat in zip(axes.flat, ['mean', 'max', 'min']):
    if stat == 'mean':
        plot_map_w_stipple(ax, data[stat], 
                           ~((data['pct_sig_pos'] > 0.4) | \
                             (data['pct_sig_neg'] > 0.4)), data.lat, data.lon, 
                           thresh = 0.05, add_cyc = True, 
                           contour_style = {'levels': levels, 'cmap': cmap,
                                            'extend': 'both'},
                           cbar_style = {'ticks': levels})
    else:
        plot_sm_map(ax, data[stat], data.lat, data.lon, add_cyc = True,
                    levels = levels, cmap = cmap)
    ax.set_title(stat)
    ax.set_ylim([-60., 90.])
fig.savefig(os.path.join(mg.path_out(), 'standard_diagnostics_plot',
                         'summary_cmip6', 'trend_' + var + '_' + \
                         expr + '.png'), dpi=600., bbox_inches='tight')
plt.close(fig)

data.close()
