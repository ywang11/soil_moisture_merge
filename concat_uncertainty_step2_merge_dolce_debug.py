import numpy as np
import os
import xarray as xr
import matplotlib.pyplot as plt
import cartopy.crs as ccrs


prod = 'lsm'


fig, axes = plt.subplots(nrows = 2, ncols = 2, figsize = (10, 6),
                         subplot_kw = {'projection': ccrs.PlateCarree()})

ax = axes.flat[0]
hr = xr.open_dataset(os.environ['PROJDIR'] + '/Soil_Moisture/' + \
                     'output_product/concat_dolce_' + prod + '/' + \
                     'concat_uncertainty_0.00-0.10_lu_weighted_' + \
                     'ShrunkCovariance.nc')
cf = ax.contourf(hr.lon.values, hr.lat.values,
                 np.mean(hr['sm_uncertainty'].values, axis = 0),
                 cmap = 'Spectral')
plt.colorbar(cf, ax = ax)
hr.close()
ax.set_title('Merged')


for yind, ystr in enumerate(['1950-2010', '1981-2010', '1981-2016']):
    ax = axes.flat[yind + 1]

    hr = xr.open_dataset(os.environ['PROJDIR'] + '/Soil_Moisture/' + \
                         'output_product/dolce_' + prod + '_product/' + \
                         'weighted_uncertainty_' + ystr + '_0.00-0.10_' + \
                         'lu_weighted_ShrunkCovariance.nc',
                         decode_times = False)
    cf = ax.contourf(hr.lon.values, hr.lat.values,
                     np.mean(hr['sm_uncertainty'].values, axis = 0),
                     cmap = 'Spectral')
    plt.colorbar(cf, ax = ax)
    hr.close()

    ax.set_title(ystr)


fig.savefig(os.environ['PROJDIR'] + '/Soil_Moisture/output_product/' + \
            'temp_' + prod + '.png', dpi = 600.)
plt.close(fig)
