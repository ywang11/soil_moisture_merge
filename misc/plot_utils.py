# -*- coding: utf-8 -*-
"""
Created on Tue May 14 10:01:21 2019

@author: ywang254
"""
import numpy as np
import os
import sys
if sys.platform == 'linux':
    os.environ['QT_QPA_PLATFORM']='offscreen'
import matplotlib.pyplot as plt
plt.switch_backend('agg') # solve the display problem
from matplotlib import colors
from matplotlib import cm
from cartopy.util import add_cyclic_point
import cartopy.crs as ccrs
import skill_metrics as SM
import matplotlib as mpl
import statsmodels.regression.linear_model as lm
from statsmodels.regression.linear_model import OLS
from statsmodels.tools.tools import add_constant
from statsmodels.sandbox.regression.predstd import wls_prediction_std
from scipy.stats import pearsonr
from .format_text import ppf, ppp


def cmap_gen(cmap_1, cmap_2):
    """
    Generate a new colormap by stacking two color maps. 
    """
    c1 = cm.get_cmap(cmap_1, 128)
    c2 = cm.get_cmap(cmap_2, 128)
    new1 = c1(np.linspace(0, 1, 128))
    new2 = c2(np.linspace(0, 1, 128))
    newcmp = colors.ListedColormap(np.concatenate([new1, new2], axis = 0))
    return newcmp


def cmap_div():
    c = cm.get_cmap('gist_ncar_r', 256)
    new1 = c(np.linspace(0.1, 0.5, 128))
    new2 = c(np.linspace(0.6, 1., 128))
    newcmp = colors.ListedColormap(np.concatenate([new1, new2], axis = 0))
    return newcmp


def stipple(ax, lat, lon, p_values, thresh=0.05, transform=None,
            hatch = '//////'):
    """ 
    Stipple points using plt.scatter for values below thresh. 
    """ 
    xData, yData = np.meshgrid(lon, lat)
    ax.contourf(xData, yData, p_values < thresh, 1,
                hatches = ['', hatch], transform = transform,
                alpha = 0, zorder = 3)


def plot_ts_trend(ax, time, ts, txt_x, txt_y,
                  ts_kw = {'color': 'k', 'marker': '.', 'markersize': 3.}, 
                  ln_kw = {'color': 'k', 'linestyle': '-', 'linewidth': 1.5}):
    """
    Plot the time series with trend.
    """
    h1, = ax.plot(time, ts, **ts_kw)

    model = lm.OLS(ts, add_constant(np.arange(0, len(time))))
    result = model.fit()
    params_CI = result.conf_int()

    """
    ax.text(txt_x, txt_y + 0.025, 'Slope = %.2E (%.2E, %.2E)'  % \
            (result.params[1], params_CI[1,0], params_CI[1,1]), 
            transform = ax.transAxes, color = ln_kw['color'],
            fontsize = 8)
    ax.text(txt_x, txt_y - 0.025, 'Intercept = %.2E (%.2E, %.2E)' % \
            (result.params[0], params_CI[0,0], params_CI[0,1]),
            transform = ax.transAxes, color = ln_kw['color'],
            fontsize = 8)
    """

    slope = result.params[1]
    intercept = result.params[0]

    h2, = ax.plot(time, np.arange(0, len(time)) * slope + intercept, 
                  **ln_kw)

    if intercept < 0:
        symbol = '-'
        intercept = -intercept
    else:
        symbol = '+'
    format_str = 'y = %.6f * x ' + symbol + ' %.6f (%.3f, %.3f)'

    ax.text(txt_x, txt_y, format_str % \
            (slope, intercept, result.pvalues[1], result.pvalues[0]),
            transform = ax.transAxes, color = ln_kw['color'],
            fontdict = {'fontsize': 8, 'family' : 'serif',
                        'weight': 'bold'})
    ##print(np.arange(0, len(time)) * slope + intercept)
    return h1, h2


def plot_ts_shade(ax, time, ts = {'min': np.array([]), 'mean': np.array([]),
                                  'max': np.array([])}, ts_label = '',
                  ts_col = 'red'):
    """
    Plot a shaded ensemble.
    """
    hl, = ax.plot(time, ts['mean'], '-', color = ts_col, linewidth = 2,
                  label = ts_label)
    ax.plot(time, ts['min'], '--', color = ts_col, linewidth = 1)
    ax.plot(time, ts['max'], '--', color = ts_col, linewidth = 1)
    ax.fill_between(time, ts['min'], ts['max'], 
                    where = ts['max'] > ts['min'],
                    facecolor = ts_col, alpha = 0.2)
    hfill, = ax.fill(np.nan, np.nan, ts_col, alpha = 0.2)
    return hl, hfill


def plot_ts_shade2(ax, time, ts = {'bottom': np.array([]), 
                                   'lower': np.array([]),
                                   'mean': np.array([]),
                                   'upper': np.array([]),
                                   'top': np.array([])},
                   ts_label = '', ts_col = 'red'):
    """
    Plot a shaded ensemble.
    """
    hl, = ax.plot(time, ts['mean'], '-', color = ts_col, linewidth = 2,
                  label = ts_label)
    h2 = ax.fill_between(time, ts['bottom'], ts['top'],
                         where = ts['top'] > ts['bottom'],
                         facecolor = ts_col, alpha = 0.2)
    h3 = ax.fill_between(time, ts['lower'], ts['upper'],
                         where = ts['upper'] > ts['lower'],
                         facecolor = ts_col, alpha = 0.5)
    return hl, h2, h3


def plot_sm_map(ax, sm_array, lat, lon, add_cyc = False, 
                levels = np.arange(0., 0.6, 0.02), cmap = 'RdYlBu',
                cbar_style = {}):
    """
    Plot global map of soil moisture (sm_array is 2D numpy array)
    """
    # ax.set_global()
    ax.set_extent([-180, 180, -70, 90])
    ax.coastlines()
    ##ax.gridlines(draw_labels = True, xlocs = np.arange(-180, 181, 60), 
    ##             ylocs = np.arange(-90, 91, 60))

    if add_cyc:
        Z_cyc, x_cyc = add_cyclic_point(sm_array, lon)
    else:
        Z_cyc = sm_array
        x_cyc = lon
    cf = ax.contourf(x_cyc, lat, Z_cyc, levels = levels, extend = 'both',
                     cmap = cmap)
    if not cbar_style:
        return ax, cf
    else:
        cb = plt.colorbar(cf, ax=ax, shrink=0.7, pad = 0.1,
                          boundaries = levels, ticks = levels)
        return ax, cf, cb


def plot_pr_map(ax, pr_array, lat, lon, add_cyc=False):
    """
    Plot global map of precipitation (mm/day) (pr_array is 2D numpy array)
    """
    ax.set_global()
    ax.coastlines()

    if add_cyc:
        Z_cyc, x_cyc = add_cyclic_point(pr_array, lon)
    else:
        Z_cyc = pr_array
        x_cyc = lon
    levels = np.power(np.arange(0., np.power(15.,0.5)+1e-6, 
                                np.power(15.,0.4) / 20), 2.5)
    cf = ax.contourf(x_cyc, lat, Z_cyc,
                     levels = levels,
                     cmap = 'RdYlBu', vmin = 0., vmax = 15.,
                     norm = colors.PowerNorm(gamma = 0.4))
    cb = plt.colorbar(cf, ax=ax, shrink=0.5, extend='both')
# =============================================================================
#         ax = axes[1]
#         ax.set_global()
#         Z_cyc, x_cyc = add_cyclic_point(p_values, data.lon)
#         cf2 = ax.contourf(x_cyc, data.lat, Z_cyc, levels= \
#                           np.arange(.0, 0.0001, .000005), 
#                           transform=ccrs.PlateCarree(),
#                           cmap = 'RdYlBu', vmin=0., vmax=0.0001)
#        plt.colorbar(cf2, ax=ax, shrink=0.5, extend='both')
# =============================================================================
    cb.set_ticks(levels[::2])
    cb.ax.set_yticklabels([('%.2f' % x) for x in levels[::2]])
    return ax, cf, cb


def plot_map_w_pts(ax, data, lat, lon, pts, add_cyc=False, 
                   contour_style = {}, cbar_style = {}, pts_style = {}):
    """
    Plot global map with certain areas shaded.
    """
    ax.set_global()
    ax.coastlines()
    
    if add_cyc:
        Z_cyc, x_cyc = add_cyclic_point(data, lon)
    else:
        Z_cyc = data
        x_cyc = lon
    
    # **kwargs examples:
    # levels= np.arange(-0.0004, 0.00041, 0.00002)
    # cmap = 'PiYG'
    # vmin= -0.0004
    # vmax= 0.0004
    cf = ax.contourf(x_cyc, lat, Z_cyc, transform=ccrs.PlateCarree(),
                     **contour_style)
    cb = plt.colorbar(cf, ax=ax, shrink=0.5, extend='both', **cbar_style)
    # ---- plot the pts: ['Lat, 'Lon']
    # **kwargs examples:
    # marker = 'o', markersize = 12,
    # markerfacecolor = '#00FF00', markeredgecolor = 'k'
    ax.plot(pts['Lon'], pts['Lat'], **pts_style)
    return ax, cf, cb


def plot_map_w_stipple(ax, data, p_values, lat, lon, thresh, add_cyc=False, 
                       contour_style = {}, cbar_style = {}):
    """
    Plot global map with certain areas shaded.
    """
    ax.set_extent([-180, 180, -70, 90])
    ax.coastlines()

    if add_cyc:
        Z_cyc, x_cyc = add_cyclic_point(data, lon)
    else:
        Z_cyc = data
        x_cyc = lon

    # **kwargs examples:
    # levels= np.arange(-0.0004, 0.00041, 0.00002)
    # cmap = 'PiYG'
    # vmin= -0.0004
    # vmax= 0.0004
    cf = ax.contourf(x_cyc, lat, Z_cyc, transform=ccrs.PlateCarree(),
                     **contour_style)
    # ---- two-sided t-test at p = 0.05; need to ensure p = two-sided.
    stipple(ax, lat, lon, p_values, thresh=thresh, 
            transform=ccrs.PlateCarree())

    if cbar_style:
        cb = plt.colorbar(cf, ax=ax, shrink=0.7, extend='both', **cbar_style)
        return ax, cf, cb
    else:
        return ax, cf


def plot_trend_w_sig(ax, trends, p_values, lat, lon, thresh=0.05, 
                     add_cyc=False):
    """
    Plot global trend with significance (at p <= thresh)
    """
    # ---- two-sided t-test at p = 0.05, thus thresh = 0.025 on each side

    ##[np.nanquantile(trends, p) for p in np.arange(.05, 1., .05)]
    ax, cf, cb = plot_map_w_stipple(ax, trends, p_values, lat, lon, thresh, 
                                    add_cyc, level = np.arange(-0.0004,
                                                               0.00041, 
                                                               0.00002), 
                                    cmap = 'PiYG', vmin= -0.0004,
                                    vmax= 0.0004)
    return ax, cf, cb


def plot_convenient_target(ax, obs, sim, color_list, label_list, **kwargs):
    """
    Given two numpy arrays (1-D or 2-D, in which case the 1st dimension
    is taken as the time dimension and must match), plot the Target diagram.

    https://github.com/PeterRochford/SkillMetrics/wiki

    **kwargs pass into SM.taylor_diagram() options other than 'markerfacecolor'
     and 'markerLabel'
    """
    if len(obs.shape) == 1:
        if len(sim.shape) == 1:
            target_stats = SM.target_statistics(sim, obs)
            Bias = target_stats['bias']
            uRMSE = target_stats['crmsd']
            RMSE = target_stats['rmsd']
        else:
            Bias = np.empty(sim.shape[1])
            uRMSE = np.empty(sim.shape[1])
            RMSE = np.empty(sim.shape[1])
            for i in range(sim.shape[1]):
                target_stats = SM.target_statistics(sim[:,i], obs)
                Bias[i] = target_stats['bias']
                uRMSE[i] = target_stats['crmsd']
                RMSE[i] = target_stats['rmsd']
    else:
        if not ((obs.shape[0] == sim.shape[0]) & \
                (obs.shape[1] == sim.shape[1])):
            raise('Mismatch in the dimensions of obs and sim')
        Bias = np.empty(sim.shape[1])
        uRMSE = np.empty(sim.shape[1])
        RMSE = np.empty(sim.shape[1])
        for i in range(sim.shape[1]):
            target_stats = SM.target_statistics(sim[:,i], obs[:,i])
            Bias[i] = target_stats['bias']
            uRMSE[i] = target_stats['crmsd']
            RMSE[i] = target_stats['rmsd']

    print(uRMSE)

    plt.sca(ax)
    if (len(obs.shape) == 1) & (len(sim.shape) == 1):
        SM.target_diagram(Bias, uRMSE, RMSE, markercolor = color_list, 
                          markerLabel = [label_list], **kwargs)
    else:
        for i in range(sim.shape[1]):
            if i == 0:
                SM.target_diagram(Bias[i], uRMSE[i], RMSE[i],
                                  markercolor = color_list[i],
                                  markerLabel = [label_list[i]], **kwargs)
            else:
                SM.target_diagram(Bias[i], uRMSE[i], RMSE[i],
                                  markercolor = color_list[i],
                                  markerLabel = [label_list[i]],
                                  overlay = 'on', **kwargs)


def plot_convenient_taylor(ax, obs, sim, color_list, label_list, **kwargs):
    """
    Given two numpy arrays (1-D or 2-D, in which case the 1st dimension
    is taken as the time dimension and must match), plot the Taylor diagram.

    https://github.com/PeterRochford/SkillMetrics/wiki

    **kwargs pass into SM.taylor_diagram() options other than 'markerfacecolor'
     and 'markerLabel'
    """
    if len(obs.shape) == 1:
        if len(sim.shape) == 1:
            taylor_stats = SM.taylor_statistics(sim, obs)
            SD = taylor_stats['sdev']
            uRMSE = taylor_stats['crmsd']
            Corr = taylor_stats['ccoef']
        else:
            SD = np.empty(1 + sim.shape[1])
            uRMSE = np.empty(1 + sim.shape[1])
            Corr = np.empty(1 + sim.shape[1])
            for i in range(sim.shape[1]):
                taylor_stats = SM.taylor_statistics(sim[:,i], obs)
                ##print(taylor_stats)
                if i == 0:
                    SD[0] = taylor_stats['sdev'][0]
                    uRMSE[0] = taylor_stats['crmsd'][0]
                    Corr[0] = taylor_stats['ccoef'][0]
                SD[i+1] = taylor_stats['sdev'][1]
                uRMSE[i+1] = taylor_stats['crmsd'][1]
                Corr[i+1] = taylor_stats['ccoef'][1]
    else:
        SD = np.empty(1 + sim.shape[1])
        uRMSE = np.empty(1 + sim.shape[1])
        Corr = np.empty(1 + sim.shape[1])
        for i in range(sim.shape[1]):
            taylor_stats = SM.taylor_statistics(sim[:,i], obs[:,i])
            if i == 0:
                SD[0] = taylor_stats['sdev'][0]
                uRMSE[0] = taylor_stats['crmsd'][0]
                Corr[0] = taylor_stats['ccoef'][0]
            SD[i] = taylor_stats['sdev'][1]
            uRMSE[i] = taylor_stats['crmsd'][1]
            Corr[i] = taylor_stats['ccoef'][1]

    plt.sca(ax)
    if (len(obs.shape) == 1) & (len(sim.shape) == 1):
        SM.taylor_diagram(SD, uRMSE, Corr, markercolor = color_list, 
                          markerLabel = [label_list], markerObs = 's', 
                          titleOBS = label_list[0], rmsLabelFormat = '0:.2f',
                          **kwargs)
    else:
        for i in range(1, sim.shape[1]+1):
            if i == 1:
                SM.taylor_diagram(SD[[0,i]], uRMSE[[0,i]], Corr[[0,i]],
                                  colOBS = color_list[0],
                                  markercolor = color_list[i],
                                  markerLabel = [label_list[0], 
                                                 label_list[i]], 
                                  markerObs = 's', titleOBS = label_list[0],
                                  rmsLabelFormat = '0:.2f',
                                  **kwargs)
            else:
                SM.taylor_diagram(SD[[0,i]], uRMSE[[0,i]], Corr[[0,i]],
                                  colOBS = color_list[0],
                                  markercolor = color_list[i],
                                  markerLabel = [label_list[0], 
                                                 label_list[i]],
                                  overlay = 'on', markerObs = 's', 
                                  rmsLabelFormat = '0:.2f',
                                  titleOBS = label_list[0], **kwargs)


def scatter_hatch(ax, mask_array, **kwargs):
    lon2d, lat2d = np.meshgrid(mask_array['lon'].values,
                               mask_array['lat'].values)
    lon2d[~mask_array] = np.nan
    lat2d[~mask_array] = np.nan
    temp = ~(np.isnan(lon2d) | np.isnan(lat2d))
    x = lon2d[temp]
    y = lat2d[temp]
    h = ax.scatter(x, y, transform = ccrs.PlateCarree(), **kwargs)

    return h


def heatmap(data, row_labels, col_labels, ax=None, cbar=False,
            cbar_kw={}, cbarlabel="", **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Parameters
    ----------
    data
        A 2D numpy array of shape (N, M).
    row_labels
        A list or array of length N with the labels for the rows.
    col_labels
        A list or array of length M with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    cbar_kw
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    cbarlabel
        The label for the colorbar.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)

    # Create colorbar
    if cbar:
        cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
        cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(col_labels)
    ax.set_yticklabels(row_labels)

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=True, bottom=False,
                   labeltop=True, labelbottom=False)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=-30, ha="right",
             rotation_mode="anchor")

    # Turn spines off and create white grid.
    for edge, spine in ax.spines.items():
        
        spine.set_visible(False)

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)

    if cbar:
        return im, cbar
    else:
        return im


def annotate_heatmap(im, data=None, valfmt="{x:.2f}",
                     textcolors=["black", "white"],
                     threshold=None, **textkw):
    """
    A function to annotate a heatmap.

    Parameters
    ----------
    im
        The AxesImage to be labeled.
    data
        Data used to annotate.  If None, the image's data is used.  Optional.
    valfmt
        The format of the annotations inside the heatmap.  This should either
        use the string format method, e.g. "$ {x:.2f}", or be a
        `matplotlib.ticker.Formatter`.  Optional.
    textcolors
        A list or array of two color specifications.  The first is used for
        values below a threshold, the second for those above.  Optional.
    threshold
        Value in data units according to which the colors from textcolors are
        applied.  If None (the default) uses the middle of the colormap as
        separation.  Optional.
    **kwargs
        All other arguments are forwarded to each call to `text` used to create
        the text labels.
    """

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max())/2.

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = mpl.ticker.StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            kw.update(color=textcolors[int(im.norm(data[i, j]) > threshold)])
            if ~np.isnan(data[i,j]):
                text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
            else:
                text = im.axes.text(j, i, '', **kw)
            texts.append(text)

    return texts


def ax_regress(ax, x, vector, 
               display = 'equation',
               pos_xy = [0.1, 0.9],
               args_pt = {'ls': '-'},
               args_ln = {'color': 'k'},
               args_ci = {'color': 'k', 'alpha': 0.2},
               args_tx = {'color': 'k'}):
    """
    Plot the time series with trend.
    Parameters
    ----------
    ax: matplotlib.pyplot.axis
    x: 1-d array
        The x-values in the regression.
    vector: 1-d array
        The y-values in the regression.
    display: None or str
        If None, does not display the regression equation.
        If 'equation', display the regression equation.
        If 'pearson', display the Pearson correlation.
    pos_xy: [float, float]
        The position to place the annotation in the normalized axis unit.
    args_pt, args_ln, args_ci, args_txt: dict
        Keyword arguments to be passed into the scatter plot, regression
        line, confidence interval for the regression line, and annotation
        text plotting functions.
    """
    temp = (~np.isnan(vector)) & (~np.isnan(x))
    x = x[temp]
    vector = vector[temp]

    ax.plot(x, vector, **args_pt)

    reg = OLS(vector, add_constant(x)).fit()
    ax.plot(x, x * reg.params[1] + reg.params[0], **args_ln)

    _, predict_ci_low, predict_ci_upp = wls_prediction_std(reg, \
        exog = reg.model.exog, weights = np.ones(len(reg.model.exog)))
    x_ind = np.argsort(x)
    ax.fill_between(x[x_ind], predict_ci_low[x_ind], 
                    predict_ci_upp[x_ind], interpolate = True,
                    **args_ci)

    if display == 'equation':
        ax.text(pos_xy[0], pos_xy[1],
                ppf(reg.params[1], reg.params[0],
                    reg.pvalues[1], reg.pvalues[0]),
                transform = ax.transAxes, **args_tx)
    elif display == 'pearson':
        r, pval = pearsonr(x, vector)
        ax.text(pos_xy[0], pos_xy[1],
                ('%.3f' % r) + ppp(pval),
                transform = ax.transAxes, **args_tx)
