import pandas as pd
import sys
import os

if sys.platform == 'win32':
    path = 'D:/Projects/2018 Soil Moisture/intermediate'
elif sys.platform == 'linux':
    #path = '/lustre/haven/user/ywang254/Soil_Moisture/intermediate'
    path = os.path.join(os.environ['PROJDIR'], 'Soil_Moisture',
                        'intermediate')


def cmip5_availability(dcm, land_mask):
    """
    Given the depth (0-10cm, 10-30cm, 30-50cm, 50-100cm), compute the 
    list of CMIP5 models that are available for this depth.
    """
    cmip5_availability = pd.read_csv(os.path.join(path, 'Interp_Merge',
                                                  land_mask, 
                                                  'intersect_cmip5_table.csv'),
                                     index_col = 0)

    return list(cmip5_availability.columns[cmip5_availability.loc[dcm,
                                                                  :].values])
