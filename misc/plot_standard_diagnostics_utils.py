import numpy as np
import pandas as pd
import xarray as xr
import os
import sys
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from .plot_utils import plot_sm_map, plot_map_w_stipple
import statsmodels.regression.linear_model as lm
from statsmodels.tools import add_constant


def plot_map(depth, prefix_in, path_out, levels, cmap, diff = False, 
             year = None):
    fig, axes = plt.subplots(nrows = 4, ncols = 5,
                             subplot_kw = {'projection': ccrs.PlateCarree()},
                             figsize = (25,10))
    fig.subplots_adjust(hspace = 0., wspace = 0.01)

    for i,d in enumerate(depth):
        annual_file = os.path.join(prefix_in + d + '_g_map_annual.nc')
        print(annual_file)
        if not os.path.exists(annual_file):
            print('... does not exist for this depth')
            continue

        data = xr.open_dataset(annual_file)
        values = data.g_map.values.copy()
        data.close()

        if diff:
            # Requires the year variable to be set.
            if '_' == prefix_in[-1]:
                data0 = xr.open_dataset(os.path.join(mg.path_out(), 
                    'standard_diagnostics_meanmedian_' + \
                    prefix[:-1].split('_')[-1], 'mean_' + \
                    str(year[0]) + '-' + str(year[-1]) + '_' + d + \
                    '_g_map_annual.nc'))
            else:
                data0 = xr.open_dataset(os.path.join(mg.path_out(), 
                    'standard_diagnostics_meanmedian_' + \
                    prefix.split('_')[-1], 'mean_' + \
                    str(year[0]) + '-' + str(year[-1]) + '_' + d + \
                    '_g_map_annual.nc'))
            values = values - data0.g_map.values.copy()
            data0.close()
        ax = axes[i,0]
        ax, cf = plot_sm_map(ax, values, data.lat, data.lon, True, 
                             cmap = 'Spectral')
        ax.text(-0.03, 0.55, d, va='bottom', ha='center',
                rotation='vertical', rotation_mode='anchor',
                transform=ax.transAxes)
        if i == 0:
            ax.set_title('Annual')

        seasons = ['DJF', 'MAM', 'JJA', 'SON']
        for ss_ind, ss in enumerate(seasons, 1):
            data = xr.open_dataset(os.path.join(prefix_in + d + \
                                                '_g_map_' + ss + '.nc'))
            values = data.g_map.values.copy()
            data.close()

            if diff:
                # Requires the year variable to be set.
                if '_' == prefix_in[-1]:
                    data0 = xr.open_dataset(os.path.join(mg.path_out(), 
                        'standard_diagnostics_meanmedian_' + \
                        prefix[:-1].split('_')[-1], 'mean_' + \
                        str(year[0]) + '-' + str(year[-1]) + '_' + d + \
                        '_g_map_' + ss + '.nc'))
                else:
                    data0 = xr.open_dataset(os.path.join(mg.path_out(), 
                        'standard_diagnostics_meanmedian_' + \
                        prefix.split('_')[-1], 'mean_' + \
                        str(year[0]) + '-' + str(year[-1]) + '_' + d + \
                        '_g_map_' + ss + '.nc'))
                values = values - data0.g_map.values.copy()
                data0.close()

            ax = axes[i,ss_ind]
            ax, cf = plot_sm_map(ax, values, data.lat, data.lon, True,
                                 cmap = 'Spectral')
            if i == 0:
                ax.set_title(ss) # pad = 20

        cax = fig.add_axes([0.91, 0.1, 0.01, 0.8])
        cbar = fig.colorbar(cf, cax=cax, boundaries = levels)
        cbar.ax.set_ylabel('Soil Moisture (m$^3$/m$^3$)')

        fig.savefig(path_out, dpi=600., bbox_inches = 'tight')
        plt.close(fig)


def plot_trend_map(depth, prefix_in, prefix_out, levels, cmap):
    fig, axes = plt.subplots(nrows = 4, ncols = 5, 
                             subplot_kw = {'projection': ccrs.PlateCarree()},
                             figsize = (25,10))
    fig.subplots_adjust(hspace = 0., wspace = 0.01)

    for i,d in enumerate(depth):
        annual_file = os.path.join(prefix_in + d + '_g_map_trend' + \
                                   '_Annual.nc')
        print(annual_file)
        if not os.path.exists(annual_file):
            print('... does not exist for this depth')
            continue

        # Annual trend points.
        ax = axes[i, 0]
        data = xr.open_dataset(annual_file)
        ax, cf = plot_map_w_stipple(ax, data['g_map_trend'].values,
                                    data['g_p_values'].values, data.lat,
                                    data.lon, thresh = 0.05, add_cyc = True, 
                                    contour_style = {'levels': levels, 
                                                     'cmap': cmap,
                                                     'extend': 'both'})
        ax.text(-0.03, 0.55, d, va='bottom', ha='center',
                rotation='vertical', rotation_mode='anchor',
                transform=ax.transAxes)
        if i == 0:
            ax.set_title('Annual')
        ##ax.gridlines(draw_labels = True, xlocs = np.arange(-180, 181, 30),
        ##             ylocs = np.arange(-90, 91, 30))
        data.close()

        #
        seasons = ['DJF', 'MAM', 'JJA', 'SON']
        for ss_ind, ss in enumerate(seasons, 1):
            ax = axes[i, ss_ind]
            data = xr.open_dataset(os.path.join(prefix_in + d + \
                                                '_g_map_trend_' + ss + '.nc'))
            plot_map_w_stipple(ax, data['g_map_trend'].values,
                               data['g_p_values'].values, data.lat,
                               data.lon, thresh = 0.05, add_cyc = True,
                               contour_style = {'levels': levels,
                                                'cmap': cmap,
                                                'extend': 'both'})
            #ax.gridlines(draw_labels = True, xlocs = np.arange(-180, 181, 30),
            #             ylocs = np.arange(-90, 91, 30))
            if i == 0:
                ax.set_title(ss) # pad = 20
            data.close()

    cax = fig.add_axes([0.91, 0.1, 0.01, 0.8])
    cbar = fig.colorbar(cf, cax=cax, boundaries = levels)
    cbar.ax.set_ylabel('Trend (m$^3$/m$^3$/year)')

    fig.savefig(prefix_out + '_map_trend.png', dpi = 600., 
                bbox_inches = 'tight')
    plt.close(fig)


def plot_ts_trend(depth, year, path_out, 
                  prefix_in = {'lsm': [], 'em': [], 'dolce': []},
                  prefix_out = {'lsm': [], 'em': [], 'dolce': []},
                  kind = ['ts', 'lat_ts', 'srex_ts', 'continent_ts']):

    slope = pd.DataFrame(data = np.nan, index = prefix_out['lsm'] + \
                         prefix_out['em'] + prefix_out['dolce'], 
                         columns = [])

    for l_ind, l in enumerate(prefix_in['lsm'] + prefix_in['em'] + \
                              prefix_in['dolce']):
        data = pd.read_csv(l + '_' + depth + '_g_' + \
                           kind + '.csv', index_col = 0, parse_dates = True)
        for c in data.columns:
            model = lm.OLS(data[c].values,
                           add_constant(np.arange(0., data.shape[0])))
            result = model.fit()
            params_CI = result.conf_int()

            if params_CI[1,0] * params_CI[1,1] < 0:
                slope.loc[slope.index[l_ind], c] = 0.
            else:
                slope.loc[slope.index[l_ind], c] = result.params[1]

    # m^3/m^3 per 10 years
    slope = slope * 10.

    fig, ax = plt.subplots()
    # ---- boxplot for the individual land surface models
    bp = ax.boxplot(slope.loc[prefix_out['lsm'],:].T, 
                    sym = 'b+', notch=False, patch_artist = True,
                    positions = np.arange(0., slope.shape[1]),
                    whis = 'range', whiskerprops={'color': 'b'},
                    capprops = {'color': 'b'})
    for box in bp['boxes']:
        box.set(color = '#beaed4', linewidth = 2)
        box.set(facecolor = '#7fc97f')
    # ---- dots for the individual land surface models/emergent constraint
    #      result/DOLCE result
    for c_ind, c in enumerate(slope.columns):
        h = []
        mkr = ['x', 'o', 'o']
        col = ['k', 'r', 'b']
        for me_ind, me in enumerate(['lsm', 'em', 'dolce']):
            h1 = ax.scatter(c_ind * np.ones(len(prefix_out[me])) - 0.1,
                            slope.loc[prefix_out[me], c], 
                            marker = mkr[me_ind], c = col[me_ind], 
                            s = 10, zorder = 3)
            h.append(h1)

    ax.set_xticklabels(slope.columns, rotation = 90)
    ax.set_title('Trend: m$^3$/m$^3$ per 10 years')
    ax.legend([bp['boxes'][0]] + h, ['Range LSM', 'Land Surface Model',
                                     'Emergent Constraint', 'DOLCE'])
    fig.savefig(os.path.join(path_out, 'standard_diagnostics_plot',
                             'trend_' + kind + '_' + str(year[0]) + '-' + \
                             str(year[-1]) + '_' + depth + '.png'), 
                bbox_inches = 'tight', dpi = 600.)
