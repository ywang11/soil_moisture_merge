# -*- coding: utf-8 -*-
"""
Created on Thu May 23 23:07:33 2019

@author: ywang254
"""
import pandas as pd
import numpy as np
import xarray as xr
import os
from sklearn import tree
from sklearn import svm
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import cross_val_score
import itertools as it


def parse_grid_czlu(fpath, ddd, gr, lu_names):
    """
    Obtain the climate zone and major land cover (including water bodies) 
    over this grid.
    """
    f = open(fpath, 'r')
    # ---- skip grid information and 'Grid' label
    f.readline()
    f.readline()
    # ---- grid land cover percentages
    grid_lu = [float(n) for n in f.readline().split(',')]
    # ---- major land cover of the grid
    major_lu = lu_names[np.argmax(grid_lu)]
    f.close()
    # ---- station land covers at this grid
    stations_lu = pd.read_csv(fpath, skiprows = 3)
    # ---- get the climate zone from the first station
    cz = stations_lu.iloc[0, -1]
    return cz, major_lu


def get_pr_cruncep(fpath, grid_latlon, year):
    """
    Obtain the CRU NCEP7 precipitation over this grid. But does not seem to
    have high predictive power.

    https://rda.ucar.edu/datasets/ds314.3/

    "The CRUNCEP is a combination of two existing datasets; the CRU TS3.2
     0.5 X 0.5 monthly data covering the period 1901 to 2002 and the NCEP 
     reanalysis 2.5 X 2.5 degree 6-hourly data covering the period 1948 to 
     2016."
    """
    pr = pd.DataFrame(data=np.nan,
                      index=pd.to_datetime([str(y) + '-' + ('%02d' % m) + \
                            '-01' for y in year for m in range(1,13)],
                            format='%Y-%m-%d'), 
                      columns=grid_latlon.columns)

    # Make the longitude all positive to be compatible with CRU_NCEP
    temp_latlon = grid_latlon.copy()
    temp_latlon.loc['Lon', temp_latlon.loc['Lon', :] < 0] = \
            temp_latlon.loc['Lon', temp_latlon.loc['Lon', :] < 0] + 360.

    data = xr.open_dataset(os.path.join(fpath, '1950.cn'), 
                           decode_times = False)
    gr_mask = np.zeros((len(data.lat), len(data.lon)), bool)
    gr_mask_label = np.array([['        '] * len(data.lon)] * len(data.lat))
    for gr in grid_latlon.columns:
        ind_x = int(np.argmin(np.abs(data.lat - temp_latlon.loc['Lat', gr])))
        ind_y = int(np.argmin(np.abs(data.lon - temp_latlon.loc['Lon', gr])))
        gr_mask[ind_x, ind_y] = True
        gr_mask_label[ind_x, ind_y] = gr
    data.close()

    gr_mask = np.where(gr_mask.reshape(-1))[0]
    gr_mask_label = gr_mask_label.reshape(-1)[gr_mask]

    # ---- implement the mask
    for y in year:
        data = xr.open_dataset(os.path.join(fpath, str(y)+'.nc'), 
                               decode_times = False)
        pr_values = data.pr.values.reshape(12,-1)
        pr.loc[pd.to_datetime([str(y) + '-' + ('%02d' % m) + \
               '-01' for m in range(1,13)], format='%Y-%m-%d'),
               gr_mask_label] = pr_values[:, gr_mask]
        data.close()
    return pr


def get_pr_cru(fpath, grid_latlon, year):
    """
    Obtain the CRU v4.03 monthly precipitation over this grid.
    """
    pr = pd.DataFrame(data=np.nan,
                      index=pd.to_datetime([str(y) + '-' + ('%02d' % m) + \
                            '-01' for y in year for m in range(1,13)],
                            format='%Y-%m-%d'), 
                      columns=grid_latlon.columns)

    data = xr.open_dataset(os.path.join(fpath,
                           'cru_ts4.03.1951.1960.pre.dat.nc'), 
                           decode_times = False)
    gr_mask = np.zeros((len(data.lat), len(data.lon)), bool)
    gr_mask_label = np.array([['        '] * len(data.lon)] * len(data.lat))
    for gr in grid_latlon.columns:
        ind_x = int(np.argmin(np.abs(data.lat - grid_latlon.loc['Lat', gr])))
        ind_y = int(np.argmin(np.abs(data.lon - grid_latlon.loc['Lon', gr])))
        gr_mask[ind_x, ind_y] = True
        gr_mask_label[ind_x, ind_y] = gr
    data.close()

    gr_mask = gr_mask.reshape(-1)
    gr_mask_label = gr_mask_label.reshape(-1)[gr_mask]

    # ---- implement the mask
    for ten_year in ['1941.1950', '1951.1960', '1961.1970', '1971.1980',
                     '1981.1990', '1991.2000', '2001.2010', '2011.2018']:
        start = int(ten_year.split('.')[0])
        end = int(ten_year.split('.')[-1])

        if (end < year[0]) | (start > year[-1]):
            continue

        data = xr.open_dataset(os.path.join(fpath, 'cru_ts4.03.' + \
                               str(ten_year) + '.pre.dat.nc'))
        data['time'] = pd.date_range(ten_year.split('.')[0] + '-01-01', 
                                     ten_year.split('.')[-1]+'-12-31', 
                                     freq='MS')
        overlap = pr.index[(pr.index >= data['time'].values[0]) & \
                           (pr.index <= data['time'].values[-1])]
        overlap2 = [x in overlap for x in data['time'].values]
        pr_values = data.pre.values.reshape(12*(end - start + 1), -1)
        pr.loc[overlap, gr_mask_label] = pr_values[overlap2, 
                                                   :][:, gr_mask].copy()
        data.close()
    return pr


def get_pr_cru_global(fpath, time_range):
    data = xr.open_mfdataset([os.path.join(fpath,
                                           'cru_ts4.03.' + str(x) + \
                                           '.pre.dat.nc') for x in [ \
                                        '1941.1950', '1951.1960', '1961.1970',
                                        '1971.1980', '1981.1990', '1991.2000',
                                        '2001.2010', '2011.2018']])
    pre = data.pre[(data.time >= time_range[0].to_datetime64()) & \
                   (data.time <= time_range[-1].to_datetime64()), :, :].copy()
    data.close()

    return pre ## pre2


def get_ta_cru(fpath, grid_latlon, year):
    """
    Obtain the CRU v4.03 monthly temperature over this grid.
    """
    ta = pd.DataFrame(data=np.nan,
                      index=pd.to_datetime([str(y) + '-' + ('%02d' % m) + \
                            '-01' for y in year for m in range(1,13)],
                            format='%Y-%m-%d'),
                      columns=grid_latlon.columns)

    data = xr.open_dataset(os.path.join(fpath,
                                        'cru_ts4.03.1951.1960.tmp.dat.nc'),
                           decode_times = False)
    gr_mask = np.zeros((len(data.lat), len(data.lon)), bool)
    gr_mask_label = np.array([['        '] * len(data.lon)] * len(data.lat))
    for gr in grid_latlon.columns:
        ind_x = int(np.argmin(np.abs(data.lat - grid_latlon.loc['Lat', gr])))
        ind_y = int(np.argmin(np.abs(data.lon - grid_latlon.loc['Lon', gr])))
        gr_mask[ind_x, ind_y] = True
        gr_mask_label[ind_x, ind_y] = gr
    data.close()

    gr_mask = gr_mask.reshape(-1)
    gr_mask_label = gr_mask_label.reshape(-1)[gr_mask]

    # ---- implement the mask
    for ten_year in ['1941.1950', '1951.1960', '1961.1970', '1971.1980',
                     '1981.1990', '1991.2000', '2001.2010', '2011.2018']:
        start = int(ten_year.split('.')[0])
        end = int(ten_year.split('.')[-1])

        if (end < year[0]) | (start > year[-1]):
            continue

        data = xr.open_dataset(os.path.join(fpath, 'cru_ts4.03.' + \
                                            str(ten_year) + '.tmp.dat.nc'))
        data['time'] = pd.date_range(ten_year.split('.')[0] + '-01-01', 
                                     ten_year.split('.')[-1] + '-12-31',
                                     freq='MS')
        overlap = ta.index[(ta.index >= data['time'].values[0]) & \
                           (ta.index <= data['time'].values[-1])]
        overlap2 = [x in overlap for x in data['time'].values]
        ta_values = data.tmp.values.reshape(12*(end - start + 1), -1)
        ta.loc[overlap, gr_mask_label] = ta_values[overlap2,
                                                   :][:, gr_mask].copy()
        data.close()
    return ta


def get_ta_cru_global(fpath, time_range):
    data = xr.open_mfdataset([os.path.join(fpath,
                                           'cru_ts4.03.' + str(x) + \
                                           '.tmp.dat.nc') for x in [ \
                                        '1941.1950', '1951.1960', '1961.1970',
                                        '1971.1980', '1981.1990', '1991.2000',
                                        '2001.2010', '2011.2018']])

    tmp = data.tmp[(data.time >= time_range[0].to_datetime64()) & \
                   (data.time <= time_range[-1].to_datetime64()), :, :].copy()
    data.close()
    return tmp


def get_cz_global(fpath):
    """
    Obtain the global Koeppen climate zone.
    """
    ##fpath = os.path.join(mg.path_data(), 'Koeppen-Geiger-ASCII.txt')

    data = pd.read_csv(fpath, sep = '\s+')

    data2_core = np.empty([360, 720], dtype = '<U10')
    data2_core[:, :] = '              '
    data2 = xr.DataArray(data = data2_core, dims = ['lat','lon'],
                         coords = {'lat': np.arange(-89.75, 89.76, 0.5),
                                   'lon': np.arange(-179.75, 179.76, 0.5)})
    for ind, p in data.iterrows():
        data2.loc[dict(lat = p['Lat'], lon = p['Lon'])] = p['Cls']
    return data2


def get_lu_global(fpath):
    """
    Obtain the land cover type. 
    """
    ##fpath = os.path.join(mg.path_intrim_out(), 'lu_aggr', 'mcd12c1_0.5.nc')
    data = xr.open_dataset(fpath)
    lu = np.argmax(data.__xarray_dataarray_variable__, axis = 2)
    data.close()
    return lu


def obtain_season(datetime_index):
    season = pd.Series('       ', index=datetime_index)
    season.loc[(datetime_index.month == 12) | \
               (datetime_index.month == 1) | \
               (datetime_index.month == 2)] = 'DJF'
    season.loc[(datetime_index.month == 3) | \
               (datetime_index.month == 4) | \
               (datetime_index.month == 5)] = 'MAM'
    season.loc[(datetime_index.month == 6) | \
               (datetime_index.month == 7) | \
               (datetime_index.month == 8)] = 'JJA'
    season.loc[(datetime_index.month == 9) | \
               (datetime_index.month == 10) | \
               (datetime_index.month == 11)] = 'SON'
    return season


def convert_categorical(a_vector):
    """
    Convert the categorical predictors.
    """
    a_list = np.unique(a_vector)
    converted = np.zeros((len(a_vector), len(a_list)-1))
    for ind_x, x in enumerate(a_list[:-1]):
        converted[np.array(a_vector)==x, ind_x] = 1
    return a_list, converted


def model_error_regression(sm_sites, cz_sites, lu_sites, pr_sites,
                           ta_sites, latlon_sites, xv = False, use_pr = False,
                           use_ta = False, regressor = ['CART', 'RF', 'SVM']):

    ##sm_sites = simulated_monthly_diff[l]
    ##cz_sites = grid_info['Climate Zone']
    ##lu_sites = grid_info['Major Land Cover']
    ##pr_sites = pr
    ##latlon_sites = grid_latlon
    ##xv = True
    """
    Carry out the regression analysis between the soil moisture's errors in
    the model, and the predictors data (climate zone, land use, season).
    """
    # Expand the climate zone vector.
    cz_sites = pd.concat([cz_sites] * sm_sites.shape[0], axis=1,
                         ignore_index=True).T
    cz_sites.index = sm_sites.index
    ##print(cz_sites.shape)

    # Expand the land use vector.
    lu_sites = pd.concat([lu_sites] * sm_sites.shape[0], axis=1, 
                         ignore_index=True).T
    lu_sites.index = sm_sites.index
    ##print(lu_sites.shape)

    # Obtain the season of the months.
    sm_sites.index = pd.to_datetime(sm_sites.index, format='%Y-%m-%d')
    ss_sites = pd.concat([obtain_season(sm_sites.index)] * \
                          len(sm_sites.columns), axis = 1)
    ss_sites.columns = sm_sites.columns
    ##print(ss_sites.shape)

    # Indicator for western/eastern hemisphere ### northern & southern 
    # hemisphere. Did not have much result.
    ##he_sites = pd.DataFrame(data = np.repeat( \
    ##    (latlon_sites.loc['Lon', :].values > 0).reshape(1,-1), 
    ##     len(sm_sites.index), axis=0), index = sm_sites.index,
    ##    columns = latlon_sites.columns)
    ##he_sites = he_sites.loc[:, sm_sites.columns]

    #
    pr_sites = pr_sites.loc[sm_sites.index, sm_sites.columns]
    ### ---- log-transformation
    ##pr_sites = np.log(pr_sites + 1.)
    # ---- normalize by mean and standard deviation
    pr_sites = (pr_sites - pr_sites.mean(axis=0)) / pr_sites.std(axis=0)
    ##print(pr_sites.shape)

    #
    ta_sites = ta_sites.loc[sm_sites.index, sm_sites.columns]
    # ---- normalize by mean and standard deviation
    ta_sites = (ta_sites - ta_sites.mean(axis=0)) / ta_sites.std(axis=0)
        

    # Prepare the data. In order to keep things comparable, use precipitation
    # to filter the data, even when precipitation is not a predictor.
    y = sm_sites.values.reshape(-1)
    X_cat = np.concatenate((cz_sites.values.reshape(-1,1),
                            lu_sites.values.reshape(-1,1),
                            ss_sites.values.reshape(-1,1)), axis=1)
    # he_sites.values.reshape(-1,1)
    X_num = np.concatenate((pr_sites.values.reshape(-1,1), 
                            ta_sites.values.reshape(-1,1)), axis=1)

    retain = (~np.isnan(y)) & (~(np.isnan(X_num).any(axis=1)))

    X_cat = X_cat[retain, :]
    X_num = X_num[retain, :]

    y = y[retain]

    # Convert the categorical predictors.
    cz_list, cz_converted = convert_categorical(list(X_cat[:,0]))
    lu_list, lu_converted = convert_categorical(list(X_cat[:,1]))
    ss_list, ss_converted = convert_categorical(list(X_cat[:,2]))

    # Re-prepare the data
    X_transformed = np.concatenate((cz_converted, lu_converted,
                                    ss_converted), axis=1)
    if use_pr:
        X_transformed = np.concatenate((X_transformed, 
                                        X_num[:,0].reshape(-1,1)), axis=1)
    if use_ta:
        X_transformed = np.concatenate((X_transformed, 
                                        X_num[:,1].reshape(-1,1)), axis=1)

    # Conduct the regression
    if regressor == 'CART': 
        rc = tree.DecisionTreeRegressor()
    elif regressor == 'RF':
        rc = RandomForestRegressor(n_estimators = 100, random_state = 999)
    elif regressor == 'SVM':
        rc = svm.SVR()
    else:
        raise Exception('Unrecognized regressor :' + regressor)

    if xv:
        # Preliminary analysis: conduct cross-validation.
        score = cross_val_score(rc, X_transformed, y, cv=5, 
                                scoring='neg_mean_squared_error')
        # Convert to root mean square error.
        rmse = np.sqrt(-score)
        y_std = np.std(y)
        return rmse, y_std, y
    else:
        # Conduct the regression.
        rc = rc.fit(X_transformed,y)
        return rc, cz_list, lu_list, ss_list


def model_error_prediction_local(rc, cz_sites, lu_sites, pr_sites,
                                 ta_sites, latlon_sites, cz_original, 
                                 lu_original, use_pr, use_ta):
    """
    Use rc and predictors data (climate zone, land use, season) to predict 
    the soil moisture's errors at site level.
    The input ta & pr must be previously normalized using the calibration
    data.
    """
    # Expand the climate zone vector into categorical matrix.
    # ---- match the position of cz_original
    cz_sites_expand_1 = np.zeros([len(cz_sites.index),
                                  len(cz_original)], dtype = float)
    for cz_ind, cz in enumerate(cz_original):
        temp = np.where(cz_sites == cz)[0]
        cz_sites_expand_1[temp, cz_ind] = 1.
    # ---- if not in cz_original, fill with np.nan, i.e. no prediction
    extra = np.where((cz_sites_expand_1 < 1e-6).all(axis=1))[0]
    cz_sites_expand_1[extra, :] = np.nan
    # ---- drop the last column to remove multi-colinearity
    cz_sites_expand_1 = cz_sites_expand_1[:, :-1]
    # Expand the climate zone vector into the time dimension.
    cz_sites_expand_2 = np.tile(cz_sites_expand_1, 
                                [len(pr_sites.index), 1, 1])


    # Expand the land use vector into categorical matrix.
    # ---- match the position of lu_original
    lu_sites_expand_1 = np.zeros([len(lu_sites.index),
                                  len(lu_original)], dtype = float)
    lu_sites_expand_1[:, :] = 0.
    for lu_ind, lu in enumerate(lu_original):
        temp = np.where(lu_sites == lu)[0]
        lu_sites_expand_1[temp, lu_ind] = 1.
    # ---- if not in lu_original, fill with np.nan, i.e. no prediction
    extra = np.where((lu_sites_expand_1 < 1e-6).all(axis=1))[0]
    lu_sites_expand_1[extra, :] = np.nan
    # ---- drop the last column to remove multi-colinearity
    lu_sites_expand_1 = lu_sites_expand_1[:, :-1]
    # Expand the land use vector into the time dimension.
    lu_sites_expand_2 = np.tile(lu_sites_expand_1, 
                                [len(pr_sites.index), 1, 1])


    # Obtain the season of the months. Note: the pr_sites & ta_sites's indices
    # must be in the DatetimeIndex format.
    pr_sites.index = pd.to_datetime(pr_sites.index, format='%Y-%m-%d')
    ss_sites = pd.concat([obtain_season(pr_sites.index)] * \
                         len(pr_sites.columns), axis = 1)
    ss_sites.columns = pr_sites.columns

    ss_sites_expand = np.zeros([len(ss_sites.index), 
                                len(ss_sites.columns), 3], dtype = float)
    for ss_ind, ss in enumerate(['DJF', 'MAM', 'JJA']):
        ss_sites_expand[:, :, ss_ind] = ss_sites.values == ss


    # Prepare the data. In order to keep things comparable, use precipitation
    # to filter the data, even when precipitation is not a predictor.
    X_cat = np.concatenate((cz_sites_expand_2.reshape(-1,len(cz_original)-1),
                            lu_sites_expand_2.reshape(-1,len(lu_original)-1),
                            ss_sites_expand.reshape(-1,3)), axis=1)
    X_num = np.concatenate((pr_sites.values.reshape(-1,1),
                            ta_sites.values.reshape(-1,1)), axis=1)


    # Prepare a container for the predicted value: 
    y_full = np.empty(X_cat.shape[0], float)
    y_full[:] = np.nan

    retain = ~(np.isnan(X_cat).any(axis=1) | (np.isnan(X_num).any(axis=1)))

    X_cat = X_cat[retain, :]
    X_num = X_num[retain, :]

    # Re-prepare the data
    X = X_cat
    if use_pr:
        X = np.concatenate((X, X_num[:,0].reshape(-1,1)), axis=1)
    if use_ta:
        X = np.concatenate((X, X_num[:,1].reshape(-1,1)), axis=1)

    # Conduct the prediction & carry back to the whole vector
    y_full[retain] = rc.predict(X)

    # Cast into DataFrame
    y_full = pd.DataFrame(y_full.reshape(-1, len(cz_sites.index)), 
                          index = pr_sites.index, 
                          columns = cz_sites.index)

    return y_full


def model_error_prediction_global_clim(rc, cz_global, lu_global, time_range,
                                       cz_original, lu_original, lu_names):
    """
    Use the fitted sklearn regression model (rc) to predict global 
    Carry out the regression analysis between the soil moisture's errors in
    the model, and the predictors data (climate zone, land use, season).

    Input type: xr.DataArray
    Dimensions: cz_global & lu_global: (lat, lon)
                pr_global & ta_global: (time, lat, lon)
    """

    #
    predict_core = np.empty([len(time_range), cz_global.lat.shape[0],
                             cz_global.lon.shape[0]])
    predict_core[:, :, :] = np.nan

    #
    lu_ids = dict([(lu_names[k],k) for k in lu_names.keys()])

    for cz, lu in it.product(cz_original, lu_original):
        #
        subset = (cz_global == cz) & (lu_global == lu_ids[lu])

        if subset.values.sum() < 1e-6:
            continue

        # single prediction for the same combination of cz and lu
        cz_site = np.zeros([len(time_range), len(cz_original)-1])
        cz_site[:, cz == np.array(cz_original[:-1])] = 1

        lu_site = np.zeros([len(time_range), len(lu_original)-1])
        lu_site[:, lu == np.array(lu_original[:-1])] = 1

        ss_name = obtain_season(time_range)
        ss_site = np.zeros([len(time_range), 3])
        for ss_ind, ss in enumerate(['DJF', 'MAM', 'JJA']):
            ss_site[ss_name == ss, ss_ind] = 1.

        X = np.concatenate([cz_site, lu_site, ss_site], axis = 1)

        # Conduct the prediction & carry back to the whole vector
        y = rc.predict(X)

        predict_core = np.where(subset, 
                                np.tile(y.reshape(-1,1,1), 
                                        (1, cz_global.lat.shape[0], 
                                         cz_global.lon.shape[0])),
                                predict_core)

    predict = xr.DataArray(data = predict_core,
                           dims = ['time', 'lat', 'lon'],
                           coords = {'time': time_range,
                                     'lat': cz_global.lat,
                                     'lon': cz_global.lon})

    return predict
