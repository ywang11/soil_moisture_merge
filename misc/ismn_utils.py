# -*- coding: utf-8 -*-
"""
Created on Tue Jul 16 12:00:24 2019

@author: ywang254
"""
import pandas as pd
import numpy as np
import os


# Function to get the weighted monthly data from the output of "ismn_aggr.py".
def get_weighted_monthly_data(path_monthly_data, ismn_aggr_method, depth,
                              opt = 'all'):
    if (opt == 'all'):
        ismn_file = os.path.join(path_monthly_data, 
                                 'weighted_monthly_data_' + \
                                 ismn_aggr_method + '_' + depth + '.csv')
    else:
        ismn_file = os.path.join(path_monthly_data, 
                                 opt + '_weighted_monthly_data_' + \
                                 ismn_aggr_method + '_' + depth + '.csv')
    grid_latlon = pd.read_csv(ismn_file,  index_col = 0, nrows = 2)
    weighted_monthly_data = pd.read_csv(ismn_file, 
                                        index_col = 0).iloc[2:,:]
    weighted_monthly_data.index = pd.to_datetime( \
        weighted_monthly_data.index, format = '%Y-%m-%d')

    # remove the grids that have straight NaN's in opt = 'all'
    if (opt == 'all'):
        temp = np.sum(np.isnan(weighted_monthly_data.values),
                      axis = 0) == weighted_monthly_data.shape[0]
    else:
        temp_0 = pd.read_csv(os.path.join(path_monthly_data, 
                                          'weighted_monthly_data_' + \
                                          ismn_aggr_method + '_' + depth + \
                                          '.csv'), index_col = 0).iloc[2:,:]
        temp = np.sum(np.isnan(temp_0.values), 
                      axis = 0) == weighted_monthly_data.shape[0]
    grid_latlon = grid_latlon.loc[:, ~temp]
    weighted_monthly_data = weighted_monthly_data.loc[:, ~temp]

    available_year = np.unique(weighted_monthly_data.index.year)
    return grid_latlon, weighted_monthly_data, available_year


# Function to get the weighted monthly data for the grids in US.
# Bounding box: -125.62,24.35,-66.56,49.43
def get_weighted_monthly_data_USonly(path_monthly_data, ismn_aggr_method,
                                     depth, opt = 'all'):
    if (opt == 'all'):
        ismn_file = os.path.join(path_monthly_data, 
                                 'weighted_monthly_data_' + \
                                 ismn_aggr_method + '_' + depth + '.csv')
    else:
        ismn_file = os.path.join(path_monthly_data, 
                                 opt + '_weighted_monthly_data_' + \
                                 ismn_aggr_method + '_' + depth + '.csv')
    grid_latlon = pd.read_csv(ismn_file,  index_col = 0, nrows = 2)
    weighted_monthly_data = pd.read_csv(ismn_file, 
                                        index_col = 0).iloc[2:,:]
    weighted_monthly_data.index = pd.to_datetime( \
        weighted_monthly_data.index, format = '%Y-%m-%d')

    # remove the grids that have straight NaN's in opt = 'all'
    if (opt == 'all'):
        temp = np.sum(np.isnan(weighted_monthly_data.values),
                      axis = 0) == weighted_monthly_data.shape[0]
    else:
        temp_0 = pd.read_csv(os.path.join(path_monthly_data, 
                                          'weighted_monthly_data_' + \
                                          ismn_aggr_method + '_' + depth + \
                                          '.csv'), index_col = 0).iloc[2:,:]
        temp = np.sum(np.isnan(temp_0.values), 
                      axis = 0) == weighted_monthly_data.shape[0]

    # limit to the grids that are in the US
    grid_US = (grid_latlon.loc['Lat', :] <= 49.43) & \
              (grid_latlon.loc['Lat', :] >= 24.35) & \
              (grid_latlon.loc['Lon', :] >= -125.62) & \
              (grid_latlon.loc['Lon', :] <= -66.56)

    grid_latlon = grid_latlon.loc[:, (~temp) & grid_US]
    weighted_monthly_data = weighted_monthly_data.loc[:, (~temp) & grid_US]

    available_year = np.unique(weighted_monthly_data.index.year)
    return grid_latlon, weighted_monthly_data, available_year


# Functions to generate file names for different weighting setups.
def get_ismn_aggr_method(simple, dominance_lc, dominance_threshold):
    if simple:
        ismn_aggr_method = 'simple'
    else:
        if dominance_lc:
            ismn_aggr_method = 'lu_weighted_above_' + str(dominance_threshold)
        else:
            ismn_aggr_method = 'lu_weighted'
    return ismn_aggr_method
