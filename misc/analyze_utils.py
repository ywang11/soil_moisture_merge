# -*- coding: utf-8 -*-
"""
Created on Tue May 14 09:30:38 2019

@author: ywang254
"""
import pandas as pd
import numpy as np
from scipy import stats
##from sklearn.linear_model import LinearRegression
import sys
import os
if sys.platform == 'linux':
    os.environ['QT_QPA_PLATFORM']='offscreen'
import statsmodels.api as sm
from statsmodels.sandbox.regression.predstd import wls_prediction_std
#from .spatial_utils import get_pt_in_continent
##import multiprocessing as mp
##from threading import Thread
##import time

def decode_month_since(time):
    ref = pd.Timestamp(time.attrs['units'].split(' ')[2])
    start = ref + pd.Timedelta(time.values[0], unit = 'M')
    start = start.replace(day = 1, hour = 0, minute = 0, second = 0)
    return pd.date_range(start, periods = len(time), freq = 'MS')


def calc_global_mean(sm_data):
    """
    Calculate global spatial averaged time series from (time, lat, lon) data.
    """
    global_ts = sm_data.mean(dim=['lat','lon'])

    global_annual = global_ts.groupby('time.year').mean(dim='time')
    
    global_mean = pd.DataFrame(data=np.nan, 
                               index=global_annual.year,
                               columns=['Annual', 'DJF', 'MAM', 'JJA',
                                        'SON'])

    global_mean.loc[:, 'Annual'] = global_annual.values.copy()
    for s in ['DJF', 'MAM', 'JJA', 'SON']:
        global_s = global_ts.where(global_ts['time.season']==s).groupby( \
                                   'time.year').mean(dim='time')
        global_mean.loc[:, s] = global_s.values.copy()

    return global_mean


def calc_p_value(reg, X, y):
    """
    Manual function to calculate the significance of a sklearn regression
    object, because regressor.stats.coef_pval uses covariance matrix between
    the predictors to weight the original data and thus gives different values.
    ##p_values[retain] = stats.coef_pval(reg, X, y)[vals3.shape[1]:]
    # skip the intercept significance
    """
    X_stacked = np.repeat(X, y.shape[1], axis=1)
    b0_stacked = np.repeat(reg.intercept_.reshape(1,-1), len(X), axis=0)
    b1_stacked = np.repeat(reg.coef_.reshape(1,-1), len(X), axis=0)
    se = np.sqrt(np.sum(np.power(y - b0_stacked - \
                 b1_stacked*X_stacked, 2), axis=0) / (len(X)-2)) / \
                 np.sqrt(np.sum(np.power(X - np.mean(X),2)))
    pp = stats.t.cdf(reg.coef_.reshape(-1) / se, len(X)-2)
    p_values = pp.copy()
    p_values[pp > 0.5] = 1 - pp[pp > 0.5]
    return p_values


def calc_ts_trend(X, y):
    mod = sm.OLS(y, sm.add_constant(X))
    reg = mod.fit()
    slope = reg.params[1]
    intercept = reg.params[0]
    p_value = reg.pvalues[1]
    return slope, intercept, p_value


def calc_seasonal_trend(sm_data, season = ['All', 'Annual', 'DJF', 'MAM', 
                                           'JJA', 'SON']):
    # Convert to time series of annual or seasonal averages
    if season=='All':
        # ---- Skip the 1st year to be consistent with DJF.
        season_annual = sm_data[12:, :, :]

        # Extract the numpy array of the seasonal values
        year = np.arange(0., (len(season_annual.time)-1)/12 + 1e-6, 1/12.)
    elif season=='Annual':
        # ---- Skip the 1st year to be consistent with DJF.
        sm_data = sm_data[12:, :, :]
        season_annual = sm_data.groupby('time.year').mean(dim='time',
                                                          skipna = True)
        # do not calculate the annual mean, if <3 observations
        seasonal_annual_count = (~np.isnan(sm_data) \
        ).groupby('time.year').sum(dim = 'time')
        season_annual = season_annual.where(seasonal_annual_count >= 3, 
                                            np.nan)

        # Extract the numpy array of the seasonal values
        year = season_annual.year.values
    else:
        # ---- Skip the 1st year to be consistent with DJF.
        period = sm_data['time'].to_index().to_period(freq = 'Q-NOV')
        if season == 'DJF':
            qtr = 1
        elif season == 'MAM':
            qtr = 2
        elif season == 'JJA':
            qtr = 3
        else:
            qtr = 4
        sm_data = sm_data[period.quarter == qtr, :, :]
        sm_data['time'] = period[period.quarter == 1].year
        season_annual = sm_data.groupby('time').mean(dim = 'time', skipna=True)

        # do not calculate the seasonal mean, if < 1 observation
        seasonal_annual_count = (~np.isnan(sm_data) \
        ).groupby('time').sum(dim = 'time')
        season_annual = season_annual.where(seasonal_annual_count >= 1, 
                                            np.nan)

        if season == 'DJF':
            season_annual = season_annual[1:-1, :, :]
        else:
            season_annual = season_annual[1:, :, :]

        season_annual = season_annual.rename({'time': 'year'})

        # Extract the numpy array of the seasonal values
        year = season_annual.year.values

    vals = season_annual.values
    vals2 = vals.reshape(len(year), -1)

    # ---- keep only the land data points
    #      for ESA-CCI, which have data gaps: the grid is only used
    #      to derive trend if it has >= 3 data points over the years.
    def calc_reg(ids, ide):
        trends = np.empty(ide - ids)
        trends[:] = np.nan
        intercepts = np.empty(ide - ids)
        intercepts[:] = np.nan
        p_values = np.empty(ide - ids)
        p_values[:] = np.nan
        for val_ind in range(ids, ide):
            X = year
            y = vals2[:, val_ind]
            retain = ~np.isnan(y)
            if sum(retain) < 3:
                continue
            else:
                X = X[retain]
                y = y[retain]
            # Because need significance, use sklearn to allow multiple jobs
            ##rc = LinearRegression(n_jobs = 10)
            ##reg = rc.fit(X[retain], y[retain])
            ##trends[val_ind] = reg.coef_.reshape(-1)
            ##intercepts[val_ind] = reg.intercept_.reshape(-1)
            ### Manual calculation of the p values of the slope
            ##p_values[val_ind] = calc_p_value(reg, X, y)

            # The sklearn implementation is actually faster. But use this for
            # reliable calculation of p-values.
            mod = sm.OLS(y, sm.add_constant(X))
            reg = mod.fit()
            trends[val_ind - ids] = reg.params[1]
            intercepts[val_ind - ids] = reg.params[0]
            p_values[val_ind - ids] = reg.pvalues[1] # 2-tailed p-value
        return ids, trends, intercepts, p_values

    ##start = time.time()
    # Trial 1: vanilla for loop.
    trends = np.empty(vals2.shape[1])
    trends[:] = np.nan
    intercepts = np.empty(vals2.shape[1])
    intercepts[:] = np.nan
    p_values = np.empty(vals2.shape[1])
    p_values[:] = np.nan
    for ind_start in range(0, vals2.shape[1], 100):
        ind_end = ind_start + 100
        _, trends[ind_start:ind_end], intercepts[ind_start:ind_end], \
            p_values[ind_start:ind_end] = calc_reg(ind_start, ind_end)
    # Trial 2: threading does not increase speed at all.
    ##threads = [Thread(target = calc_reg, 
    ##                  args = (ind_start, ind_start + 100)) \
    ##           for ind_start in range(0, 2000, 100)]
    ##[t.start() for t in threads]
    ##[t.join() for t in threads]
    # Trial 3: multiprocessing still does not improve the speed.
    ##p = mp.Pool(processes = 10)
    ##results = [p.apply(calc_reg, args=(t, t+10)) for t in range(0,1000,10)]
    ##p.close()
    ##endd = time.time()
    ##print(endd - start)

    trends = trends.reshape(vals.shape[1], vals.shape[2])
    intercepts = trends.reshape(vals.shape[1], vals.shape[2])
    p_values = p_values.reshape(vals.shape[1], vals.shape[2])

    return trends, p_values, intercepts


def calc_stats(sim, obs):
    """
    Given two numpy vectors, calculate a few general statistics.
    """
    rmse = np.sqrt(np.nanmean(np.power(sim - obs, 2)))
    bias = np.nanmean(sim - obs)
    urmse = np.sqrt(np.nanmean(np.power( \
                    sim-obs - np.nanmean(sim-obs), 2)))
    if np.sum((~np.isnan(obs)) & (~np.isnan(sim))) >= 3:
        corr =  np.corrcoef(obs[(~np.isnan(obs)) & (~np.isnan(sim))], 
                            sim[(~np.isnan(obs)) & (~np.isnan(sim))])[1,0]
    else:
        corr = np.nan

    return rmse, bias, urmse, corr


def calc_evaluation(sm_values, weighted_monthly_data, grid_latlon):
    sm_values2 = sm_values.loc[weighted_monthly_data.index,
                               weighted_monthly_data.columns]

    rmse = pd.DataFrame(data=np.nan, 
                        columns=['annual'] + [str(m) for m in range(1,13)],
                        index=['global','Africa','Europe','Asia','Oceania',
                               'North America', 'South America'])
    bias = rmse.copy()
    urmse = rmse.copy()
    corr = corr.copy()
    # ---- global annual statistics
    sim = sm_values2.values.reshape(-1)
    obs = weighted_monthly_data.values.reshape(-1)
    rmse.loc['global','annual'], bias.loc['global','annual'], \
    urmse.loc['global','annual'], corr.loc['global','annual'] = \
        calc_stats(sim, obs)
    # ---- global monthly statistics
    for m in range(1,13):
        sim = sm_values2.loc[sm_values2.index.month == m, :].values.reshape(-1)
        obs = weighted_monthly_data.loc[weighted_monthly_data.index.month \
                                        == m, :].values.reshape(-1)
        rmse.loc['global', str(m)], bias.loc['global', str(m)], \
        urmse.loc['global', str(m)], corr.loc['global', str(m)] = \
            calc_stats(sim, obs)
    # ---- continental statistics
    cont = get_pt_in_continent(grid_latlon.values.T)
    for c in ['Africa','Europe','Asia','Oceania', 'North America',
              'South America']:
        subset = (cont == c)
        # ---- annual
        sim = sm_values2.loc[:, subset].values.reshape(-1)
        obs = weighted_monthly_data.loc[:, subset].values.reshape(-1)
        rmse.loc[c,'annual'], bias.loc[c,'annual'], \
        urmse.loc[c,'annual'], corr.loc[c,'annual'] = calc_stats(sim, obs)
        # ---- monthly
        for m in range(1,13):
            sim = sm_values2.loc[sm_values2.index.month == m, 
                                 subset].values.reshape(-1)
            obs = weighted_monthly_data.loc[weighted_monthly_data.index.month \
                                            == m, subset].values.reshape(-1)
        rmse.loc[c,'annual'], bias.loc[c,'annual'], \
        urmse.loc[c,'annual'], corr.loc[c,'annual'] = calc_stats(sim, obs)

    return rmse, bias, urmse, corr
