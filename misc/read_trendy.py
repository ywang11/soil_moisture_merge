# -*- coding: utf-8 -*-
"""
Created on Fri Feb 22 16:58:53 2019

@author: ywang254
"""
import xarray as xr
import os


def CABLEPOP(path_parent):
    for i in range(1,7):
        ds = xr.open_dataset(os.path.join(path_parent, 'CABLE-POP', 'S3', 
                                          'CABLE-POP_S3_msl'+str(i)+'.nc'), 
                             decode_times=False)
        ds['msl'+str(i)]
        ds.close()


def CLM50(path_parent):
    ds = xr.open_dataset(os.path.join(path_parent, 'CLM5.0', 'S3', 
                                      'CLM5.0_S3_msl.nc'), 
                         decode_times=False)
    ds.mrso
    ds.close()


def ISAM(path_parent):
    ds = xr.open_dataset(os.path.join(path_parent, 'ISAM', 'S3',
                                      'ISAM_S3_msl.nc'), 
                         decode_times=False)
    ds.msl
    ds.close()


def JOHANNES(path_parent):
    ds = xr.open_dataset(os.path.join(path_parent, 'JOHANNES', 
                                      'trendyv7_S3_mrso_1950-2017.nc'), 
                         decode_times=False)
    ds.mrso
    ds.close()


def JSBACH(path_parent):
    ds = xr.open_dataset(os.path.join(path_parent, 'JSBACH', 'S3',
                                      'JSBACH_S3_msl.nc'),
                         decode_times=False)
    ds.msl
    ds.close()


def JULES(path_parent):
    ds = xr.open_dataset(os.path.join(path_parent, 'JULES',
                                      'JULES_S3_msl.nc'), decode_times=False)
    ds.msl
    ds.close()


def LPJ(path_parent):
    for i in ['1', '2']:
        ds = xr.open_dataset(os.path.join(path_parent, 'LPJ', 'S3', 
                                          'LPJ_S3_msl'+i+'.nc'),
                             decode_times=False)
        ds['msl'+i]
        ds.close()

def LPJGUESS(path_parent):
    for i in ['upper', 'lower']:
        ds = xr.open_dataset(os.path.join(path_parent, 'LPJ-GUESS', 'S3', 
                                          'LPJ-GUESS_S3_mrso_'+i+'.nc'),
                             decode_times=False)
        ds['mrso_'+i]
        ds.close()


def LPXBern(path_parent):
    ds = xr.open_dataset(os.path.join(path_parent, 'LPX-Bern', 'S3', 
                                      'LPX_S3_msl.nc'),
                         decode_times=False)
    ds.msl[0,:,:,:]
    ds.close()


def OCN(path_parent):
    ds = xr.open_dataset(os.path.join(path_parent, 'OCN', 'S3', 
                                      'OCN_S3_mrso.nc'),
                         decode_times=False)
    ds.mrso
    ds.close()


def ORCHIDEE(path_parent):
    ds = xr.open_dataset(os.path.join(path_parent, 'ORCHIDEE', 'S3', 
                                      'ORCHIDEE_S3_msl.nc'),
                         decode_times=False)
    ds.msl
    ds.close()


def ORCHIDEECNP(path_parent):
    ds = xr.open_dataset(os.path.join(path_parent, 'ORCHIDEE-CNP', 'S3_new', 
                                      'ORCHIDEE-CNP_S3_msl.nc'),
                         decode_times=False)
    ds.msl
    ds.close()


def SDGVM(path_parent):
    ds = xr.open_dataset(os.path.join(path_parent, 'SDGVM', 'S3', 
                                      'SDGVM_S3_mrso.nc'),
                         decode_times=False)
    ds.mrso
    ds.close()


def VISIT(path_parent):
    ds = xr.open_dataset(os.path.join(path_parent, 'VISIT', 'msl', 
                                      'VISIT_S3_msl.nc'),
                         decode_times=False)
    ds.msl
    ds.close()
