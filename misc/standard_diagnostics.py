from .spatial_utils import get_lat_avg
from .analyze_utils import calc_seasonal_trend
import numpy as np
import sys
import os
import xarray as xr
import pandas as pd
from glob import glob


if sys.platform == 'linux':
    path_mask = os.path.join(os.environ['PROJDIR'], 'Soil_Moisture', 'data',
                             'Global_Masks', 'elm_half_degree_grid_negLon.nc')
    path_intrim = os.path.join(os.environ['PROJDIR'], 'Soil_Moisture',
                               'intermediate')
    path_srex = os.path.join(os.environ['HOME'], 'Git',
                             'IPCC-SREX_regions_netCDF',
                             'SREX_regions_mask_0.5x0.5.nc')
else:
    path_mask = 'D:/Projects/2018 Soil Moisture/data/' + \
                'Global_Masks/elm_half_degree_grid_negLon.nc'
    path_intrim = 'D:/Projects/2018 Soil Moisture/intermediate'
    path_srex = 'C:/Users/ywang254/Documents/Git/IPCC-SREX_regions_netCDF/' + \
        'SREX_regions_mask_0.5x0.5.nc'


def standard_diagnostics(array, time, lat, lon, path_out, prefix):
    # Assumed dimension of array: time, lat, lon.
    #
    # Computed results: 
    # 1. Global maps (averaged over time, and by season over time).
    # 2. Global trends (all months, annual mean, seasonal mean).
    # 3. Global average time series.
    # 4. Latitude bands average time series.

    # (not necessary anymore)
    ### 5. Continental average time series.
    ### 6. SREX region average time series.

    # km^2 of land in each grid cell.
    data = xr.open_dataset(path_mask)
    land_area = data.area.values * data.landfrac.values
    data.close()

    # ---- pre-multiply because this will be used multiple times
    array2 = np.empty_like(array)
    for t in range(array.shape[0]):
        array2[t,:,:] = array[t,:,:] * land_area

    # ---- make sure that the ocean grid cells match between array2 and
    #      land_area
    #      ESA-CCI has "land area" that varies over time because it has
    #      different missing values. Therefore, makd land area a time-
    #      varying variable.
    land_area = np.tile(land_area, (array2.shape[0], 1, 1))
    for la in range(land_area.shape[0]):
        land_area[la,:,:] = np.where(np.isnan(array2[la,:,:]), np.nan, 
                                     land_area[la,:,:])

    standard_period = pd.date_range('1981-01-01', '2010-12-31', freq = 'MS')
    array3 = array[(time >= standard_period[0]) & \
                   (time <= standard_period[-1]), :, :]

    # 1. Climatology of the standard time period.
    g_map = np.nanmean(array3, axis=0)
    # ---- remove grids with insufficient data.
    g_map[np.sum(~np.isnan(array3), axis = 0) < 3] = np.nan

    xr.DataArray(data=g_map, dims=['lat', 'lon'],
                 coords={'lat':lat, 'lon':lon}).to_dataset(name='g_map' \
                 ).to_netcdf(os.path.join(path_out, prefix + \
                                          '_g_map_annual.nc'))

    for s1, s2 in zip(['DJF', 'MAM', 'JJA', 'SON'], 
                      [[12, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]):
        temp = np.logical_or( np.logical_or(standard_period.month == s2[0],
                                            standard_period.month == s2[1]),
                              standard_period.month == s2[2] )

        temp4 = array3[temp, :, :]
        g_map_season = np.nanmean(temp4, axis=0)
        # ---- remove grids with insufficient data
        g_map_season[np.sum(~np.isnan(temp4), axis = 0) < 3] = np.nan

        xr.DataArray(data=g_map_season, dims=['lat', 'lon'],
                     coords={'lat':lat, 'lon':lon} \
        ).to_dataset(name='g_map' \
        ).to_netcdf(os.path.join(path_out, prefix + '_g_map_' + s1 + '.nc'))

    # 2. Trend of the input time period.
    sm_data = xr.DataArray(array3, coords={'time':standard_period, 
                                           'lat':lat, 'lon':lon},
                           dims = ['time', 'lat', 'lon'])
    for s in ['All', 'Annual', 'DJF', 'MAM', 'JJA', 'SON']:
        g_map_trend, g_p_values, g_intercepts = \
            calc_seasonal_trend(sm_data, s)
        xr.Dataset({'g_map_trend': (['lat','lon'], g_map_trend),
                    'g_p_values': (['lat','lon'], g_p_values), 
                    'g_intercepts': (['lat','lon'], g_intercepts)},
                   coords = {'lon':lon, 'lat':lat} \
        ).to_netcdf(os.path.join(path_out, prefix + '_g_map_trend_' + \
                                 s +'.nc'))

    # 3.
    g_ts = np.nansum(array2, axis=(1,2)) / np.nansum(land_area, axis=(1,2))

    pd.DataFrame(data=g_ts.reshape(-1,1), index=time,
                 columns=['global_mean']).to_csv(os.path.join(path_out,
                                                              prefix + \
                                                              '_g_ts.csv'))

    # 4. 
    lat_bnds = np.concatenate([np.arange(-90., 89.6, 0.5).reshape(-1,1),
                               np.arange(-89.5, 90.1, 0.5).reshape(-1,1)], 
                              axis = 1)

    g_lat_ts = np.empty([array2.shape[0], lat_bnds.shape[0]])
    g_lat_ts[:, :] = np.nan
    for lb in range(lat_bnds.shape[0]):
        g_lat_ts[:, lb] = get_lat_avg(array2, lat, lat_bnds[lb,:]) / \
                          get_lat_avg(land_area, lat, lat_bnds[lb,:])

    pd.DataFrame(data=g_lat_ts, index=time, 
                 columns=[('%.1f' % lat_bnds[lb,0]) + '-' + \
                          ('%.1f' % lat_bnds[lb,1]) for lb in \
                          range(lat_bnds.shape[0])]
    ).to_csv(os.path.join(path_out, prefix + '_g_lat_ts.csv'))

#    # 5.
#    data = xr.open_dataset(os.path.join(path_intrim, 
#                                        'natural_earth_continent_mask.nc'))
#    # ---- ID and Abbreviation:
#    levels = data.comment.split('; ')
#    ids = [int(x.split(' - ')[0]) for x in levels[1:-1]]
#    abbrev = [x.split(' - ')[1] for x in levels[1:-1]]
#
#    mask = data.region.values
#
#    g_continent_ts = np.empty([array2.shape[0], len(ids)])
#    g_continent_ts[:,:] = np.nan
#    for t in range(array2.shape[0]):
#        for c_ind, c in enumerate(ids):
#            temp = array2[t, :, :]
#            temp2 = mask == c
#            temp3 = land_area[t, :, :]
#            g_continent_ts[t, c_ind] = np.nansum(temp[temp2]) / \
#                                       np.nansum(temp3[temp2])
#    data.close()
#
#    pd.DataFrame(data = g_continent_ts, index = time,
#                 columns = abbrev \
#    ).to_csv(os.path.join(path_out, prefix + '_g_continent_ts.csv'))
#
#    # 6.
#    data = xr.open_dataset(path_srex)
#
#    g_srex_ts = np.empty([array2.shape[0], 26])
#    g_srex_ts[:,:] = np.nan
#    for t in range(array2.shape[0]):
#        for s in range(1, 27):
#            temp = array2[t, :, :]
#            temp2 = np.abs(data.region.values - s) < 1e-6
#            temp3 = land_area[t, :, :]
#            g_srex_ts[t, s-1] = np.nansum(temp[temp2]) / \
#                                np.nansum(temp3[temp2])
#
#    pd.DataFrame(data = g_srex_ts, index = time,
#                 columns = ['ALA', 'CGI', 'WNA', 'CNA', 'ENA', 'CAM', 
#                            'AMZ', 'NEB', 'WSA', 'SSA', 'NEU', 'CEU', 'MED',
#                            'SAH', 'WAF', 'EAF', 'SAF', 'NAS', 'WAS', 'CAS', 
#                            'TIB', 'EAS', 'SAS', 'SEA', 'NAU', 'SAU']
#    ).to_csv(os.path.join(path_out, prefix + '_g_srex_ts.csv'))
#
#    data.close()


def get_summary_cmip5(path_out, target_lat, target_lon, dcm, 
                      kind = {'mean', 'trend'}):
    """
    The mean, min, max of the mean/trend of global soil moisture of CMIP5
    models.
    """
    if kind == 'mean':
        model_list = glob(os.path.join(path_out,
                                       'standard_diagnostics_cmip5',
                                       '*' + dcm + '_g_map_annual.nc'))
    else:
        model_list = glob(os.path.join(path_out,
                                       'standard_diagnostics_cmip5',
                                       '*' + dcm + '_g_map_trend_Annual.nc'))

    collect_data = np.empty([len(model_list), len(target_lat),
                             len(target_lon)])
    collect_data[:,:,:] = np.nan


    if kind == 'trend':
        collect_pvalue = np.empty([len(model_list), len(target_lat),
                                   len(target_lon)])
        collect_pvalue[:,:,:] = np.nan
    else:
        collect_pvalue = None


    for model_ind, model_path in enumerate(model_list):
        if kind == 'mean':
            var_name = 'g_map'
        elif kind == 'trend':
            var_name = ['g_map_trend', 'g_p_values']

        data = xr.open_dataset(model_path)

        if kind == 'mean':
            collect_data[model_ind, :, :] = data[var_name].values.copy()
        else:
            # Average over all trends, instead of the significant trends only.
            # data[var_name[0]].where(data[var_name[1]] <= 0.05)
            collect_data[model_ind, :, :] = data[var_name[0]].values.copy()
            collect_pvalue[model_ind, :, :] = data[var_name[1]].values.copy()

        data.close()

    collect_mean = np.nanmean(collect_data, axis=0)
    collect_max = np.nanmax(collect_data, axis=0)
    collect_min = np.nanmin(collect_data, axis=0)
    return collect_mean, collect_max, collect_min, collect_data, \
        collect_pvalue


def get_summary_cmip6(path_out, model_version, prefix, target_lat,
                      target_lon, year, kind = {'mean', 'trend'}):
    """
    The mean, min, max of the mean/trend of global soil moisture of CMIP6
    models.
    """
    collect_data = np.empty([len(model_version), len(target_lat),
                             len(target_lon)])
    collect_data[:,:,:] = np.nan

    if kind == 'trend':
        collect_pvalue = np.empty([len(model_version), len(target_lat),
                                   len(target_lon)])
        collect_pvalue[:,:,:] = np.nan
    else:
        collect_pvalue = None

    for model_ind, model in enumerate(model_version):
        if kind == 'mean':
            suffix = '_g_map_annual.nc'
            var_name = 'g_map'
        elif kind == 'trend':
            suffix = '_g_map_trend_Annual.nc'
            var_name = ['g_map_trend', 'g_p_values']

        data = xr.open_dataset(os.path.join(path_out,
                                            'standard_diagnostics_cmip6',
                                            prefix + '_' + model + '_' + \
                                            str(year[0]) + '-' + \
                                            str(year[-1]) + suffix))

        if kind == 'mean':
            collect_data[model_ind, :, :] = data[var_name].values.copy()
        else:
            # Average over all trends, instead of the significant trends only.
            # data[var_name[0]].where(data[var_name[1]] <= 0.05)
            collect_data[model_ind, :, :] = data[var_name[0]].values.copy()
            collect_pvalue[model_ind, :, :] = data[var_name[1]].values.copy()

        data.close()

    collect_mean = np.nanmean(collect_data, axis=0)
    collect_max = np.nanmax(collect_data, axis=0)
    collect_min = np.nanmin(collect_data, axis=0)

    return collect_mean, collect_max, collect_min, collect_data, \
        collect_pvalue



def seasonal_avg(pd_series):
    """
    Calculate the seasonal average of a pandas data series.
    """
    result = {}
    result['annual'] = pd_series.groupby(pd_series.index.year).mean()

    temp = pd_series.index.to_period(freq = 'Q-NOV')
    temp2 = pd_series.groupby(temp).mean()

    # Assuming the time series starts from Jan and ends in Dec: 
    # set the seasonal average of 2 months to NaN, and remove the
    # last season (only contains a December)
    temp2.iloc[0] = np.nan
    temp2 = temp2.iloc[:-1]

    for qtr, season in enumerate(['DJF', 'MAM', 'JJA', 'SON'], 1):
        result[season] = temp2.loc[temp2.index.quarter == qtr]
        result[season].index = result[season].index.year

    return pd.DataFrame(result)


def seasonal_avg2(pd_frame):
    """
    Calculate the seasonal average of a pandas data frame.
    """
    result = {}
    result['annual'] = pd_frame.groupby(pd_frame.index.year).mean()

    temp = pd_frame.index.to_period(freq = 'Q-NOV')
    temp2 = pd_frame.groupby(temp).mean()

    # Assuming the time series starts from Jan and ends in Dec: 
    # set the seasonal average of 2 months to NaN, and remove the
    # last season (only contains a December)
    temp2.iloc[0] = np.nan
    temp2 = temp2.iloc[:-1]

    for qtr, season in enumerate(['DJF', 'MAM', 'JJA', 'SON'], 1):
        result[season] = temp2.loc[temp2.index.quarter == qtr]
        result[season].index = result[season].index.year

    return result
