# -*- coding: utf-8 -*-
"""
Created on 2019/06/06

@author: ywang254@utk.edu

Various convenience functions for the emergent constraint scripts.
Not for general purpose. 

2020/12/25
Change to saving prstd instead of CI intervals.
"""
import numpy as np
import os
import sys
if sys.platform == 'linux':
    os.environ['QT_QPA_PLATFORM']='offscreen'
import matplotlib.pyplot as plt
plt.switch_backend('agg') # solve the display problem
import cartopy.crs as ccrs
import statsmodels.api as sm
from statsmodels.sandbox.regression.predstd import wls_prediction_std
import itertools as it
import xarray as xr
from .spatial_utils import get_lat_avg, extract_grid


def mask_cru_qc_month(path_qc, year):
    """
    Create a 12 months * 2D mask based on the grid cells whose inter-annual
    variance are too small (<1e-6).
    """
    lat_0 = np.arange(-89.75, 89.76, 0.5)
    lon_0 = np.arange(-179.75, 179.76, 0.5)
    lon_mesh, lat_mesh = np.meshgrid(lon_0, lat_0)
    mask = np.empty([12, len(lat_0), len(lon_0)])
    mask[:, :, :] = False
    f = open(os.path.join(path_qc, 'std_pr_pts_' + str(year[0]) + '-' + \
                          str(year[-1]) + '.txt'), 'r')
    while True:
        line = f.readline()
        if not line:
            break
        if 'Month' in line:
            m = int(line.split(' = ')[1])
            lon = f.readline().split(': ')[1].split(',')
            lat = f.readline().split(': ')[1].split(',')

            # Because the 0.5 deg grid is very regular, can easily convert to 
            # index.
            lon_ind = [int((float(x)+179.75)*2) for x in lon]
            lat_ind = [int((float(x)+89.75)*2) for x in lat]

            # Set where the problematic lon & lat to zero.
            m1 = mask[m, :, :].copy()
            m2 = mask[m, :, :].copy()
            m1[:, lon_ind] = True
            m2[lat_ind, :] = True
            mask[m, :, :] = np.logical_and(m1, m2)
    f.close()
    return mask


def mask_cru_qc_year_month(path_qc, year):
    """
    Create a 56 years * 2D mask based on the grid cells whose standard
    deviation between the nearby 9 grid cells are too low (<1e-6).
    """
    data = xr.open_dataset(os.path.join(path_qc, 'std9_' + str(year[0]) + \
                                        '-' + str(year[-1]) + '.nc'))
    mask = (data.std9.values < 1e-6).copy()
    data.close()
    return mask


def em_latbnd_yearmonth_plot(lat_bnds, pr_relevant, lsm_relevant,
                             pr_values, sm_values, pr_coords, sm_coords, year,
                             time, marker, color, sep_legend, to_folder):
    """
    Plot the emergent constraint relationship between latitude-band averaged
     precipitation and soil moisture, for each year and month.

    Input:
    - lat_bnds is a n*2 array containing the latitude bands.
    - pr_relevant list the precipitation datasets.
    - lsm_relevant is a dictionary that lists the land surface model (or 
      climate model) datasets that correspond to each precipitation dataset.
    - pr_values and sm_values are dictionaries that contain one numpy array
      under each dataset name.
    - pr_coords and sm_coords are dictionaries that contain the latitude 
      and longitude coordinates for each dataset.
    - year is the list of years to include in the plotting.
    - time is the time coordinate shared by all datasets. It should be a
      pandas DatetimeIndex.
    - marker and color are dictionaries that specify the marker and color
      for the symbol of each precipitation and land surface model dataset.
    - sep_legend indicates whether (True) the precipitation datasets are 
      indicated by markers ad the soil moisture datasets by colors, or 
      (False) the precipitation and soil moisture datasets are the same, and
      indicated by markers and colors jointly.
    - to_folder indicates which folder to save the graphs.
    """
    lsm = list(sm_values.keys())
    fig, axes = plt.subplots(nrows=4, ncols=3, sharex=True,
                             sharey = True, figsize=(12,12))
    for i,y in it.product(range(lat_bnds.shape[0]), year):
        print('lat_bnds ' + str(i))
        print('year ' + str(y))

        for m in range(12):
            ax = axes.flat[m]
            ax.cla()
            for pr in pr_relevant:
                # Find the subset mean of precipitation data.
                pr_lat_bnd = get_lat_avg( np.squeeze( \
                        pr_values[pr][(time.year == y) & \
                                      (time.month == m+1), :, :], axis=0),
                                          pr_coords[pr][0],
                                          lat_bnds[i,:] )
                for l in lsm_relevant[pr]:
                    l_lat_bnd = get_lat_avg( np.squeeze( \
                        sm_values[l][(time.year == y) & (time.month == m+1),
                                     :, :], axis=0), sm_coords[l][0],
                                             lat_bnds[i,:] )

                    ax.plot(pr_lat_bnd, l_lat_bnd, marker[pr], color = \
                            color[l], markersize=10)
            ax.set_xlabel('Pr (mm/day)')
            ax.set_ylabel(r'$\theta$ (m$^3$/m$^3$)')
            ax.set_title('Month ' + str(m+1))
        # Dummy legend
        hall = []
        if sep_legend:
            for pr in pr_relevant:
                hl, = plt.plot(np.nan, np.nan, marker[pr],
                               markerfacecolor = 'w',
                               markeredgecolor = 'k')
                hall.append(hl)
            for l in lsm:
                hl, = plt.plot(np.nan, np.nan, 's', color = color[l])
                hall.append(hl)
            ax.legend(hall, pr_relevant + lsm, ncol = 4,
                      bbox_to_anchor = (-0.3, -0.5))
        else:
            for pr in pr_relevant:
                hl, = plt.plot(np.nan, np.nan, marker[pr], color = color[pr])
                hall.append(hl)
            ax.legend(hall, pr_relevant, ncol = 4,
                      bbox_to_anchor = (-0.3, -0.5))
        fig.savefig(os.path.join(to_folder, 
                                 'latbnds_'+('%02d' % lat_bnds[i,0]) + '-' + \
                                 ('%02d' % lat_bnds[i,1]) + '_' + \
                                 'range_'+str(year[0])+'-'+str(year[-1])+ \
                                 '_' +str(y) + '.png'), dpi=600.,
                    bbox_inches = 'tight')
    plt.close(fig)


def em_latbnd_month_plot(lat_bnds, pr_relevant, lsm_relevant,
                         pr_values, sm_values, pr_coords, sm_coords, year,
                         time, marker, color, sep_legend, to_folder):
    """
    Plot the emergent constraint relationship between latitude-band averaged
     precipitation and soil moisture, for each month, pooled for all years.

    Input:
    - lat_bnds is a n*2 array containing the latitude bands.
    - pr_relevant list the precipitation datasets.
    - lsm_relevant is a dictionary that lists the land surface model (or 
      climate model) datasets that correspond to each precipitation dataset.
    - pr_values and sm_values are dictionaries that contain one numpy array
      under each dataset name.
    - pr_coords and sm_coords are dictionaries that contain the latitude, 
      and longitude coordinates for each dataset.
    - year is the list of years to include in the plotting.
    - time is the time coordinate shared by all datasets. It should be a
      pandas DatetimeIndex.
    - marker and color are dictionaries that specify the marker and color
      for the symbol of each precipitation and land surface model dataset.
    - sep_legend indicates whether (True) the precipitation datasets are 
      indicated by markers ad the soil moisture datasets by colors, or 
      (False) the precipitation and soil moisture datasets are the same, and
      indicated by markers and colors jointly.
    - to_folder indicates which folder to save the graphs.    
    """
    lsm = list(sm_values.keys())

    ##print(lsm)

    fig, axes = plt.subplots(nrows=4, ncols=3, sharex=True,
                             sharey = True, figsize=(12,12))
    for i in range(lat_bnds.shape[0]):
        ##print('lat_bnds ' + str(i))

        for m in range(12):
            ax = axes.flat[m]
            ax.cla()
            for pr in pr_relevant:
                # Find the subset mean of precipitation data.
                pr_lat_bnd = get_lat_avg( \
                    pr_values[pr][(time.month == m+1), :, :],
                                          pr_coords[pr][0], lat_bnds[i,:] )
                for l in lsm_relevant[pr]:
                    l_lat_bnd = get_lat_avg( \
                        sm_values[l][(time.month == m+1), :, :],
                                             sm_coords[l][0],
                                             lat_bnds[i,:] )
                    ax.plot(pr_lat_bnd, l_lat_bnd, marker[pr], color = \
                            color[l], markersize=2)
            ax.set_xlabel('Pr (mm/day)')
            ax.set_ylabel(r'$\theta$ (m$^3$/m$^3$)')
            ax.set_title('Month ' + str(m+1))
        # Dummy legend
        hall = []
        if sep_legend:
            for pr in pr_relevant:
                hl, = plt.plot(np.nan, np.nan, marker[pr],
                               markerfacecolor = 'w',
                               markeredgecolor = 'k')
                hall.append(hl)
            for l in lsm:
                hl, = plt.plot(np.nan, np.nan, 's', color = color[l])
                hall.append(hl)
            ax.legend(hall, pr_relevant + lsm, ncol = 4,
                      bbox_to_anchor = (-0.3, -0.5))
        else:
            for pr in pr_relevant:
                hl, = plt.plot(np.nan, np.nan, marker[pr], color = color[pr])
                hall.append(hl)
            ax.legend(hall, pr_relevant, ncol = 4,
                      bbox_to_anchor = (-0.3, -0.5))
        fig.savefig(os.path.join(to_folder,
                                 'latbnds_'+('%02d' % lat_bnds[i,0]) + '-' + \
                                 ('%02d' % lat_bnds[i,1]) + '_range_' + \
                                 str(year[0])+'-'+str(year[-1])+'.png'),
                    dpi=600., bbox_inches = 'tight')
    plt.close(fig)


def em_gridbox_yearmonth_plot(grid_box, n_sq_gr, pr_relevant, lsm_relevant,
                              pr_values, sm_values, pr_coords, sm_coords, year,
                              time, marker, color, sep_legend, to_folder):
    """
    Plot the emergent constraint relationship between latitude-band averaged
     precipitation and soil moisture, for each month, pooled for all years.

    Input:
    - grid_box is a n*2 array containing the lat & lon of the grid boxes of
      interest.
    - n_sq_gr is the sqrt of the number of nearest grids to extract.
    - pr_relevant list the precipitation datasets.
    - lsm_relevant is a dictionary that lists the land surface model (or 
      climate model) datasets that correspond to each precipitation dataset.
    - pr_values and sm_values are dictionaries that contain one numpy array
      under each dataset name.
    - pr_coords and sm_coords are dictionaries that contain the latitude, 
      and longitude coordinates for each dataset.
    - year is the list of years to include in the plotting.
    - time is the time coordinate shared by all datasets. It should be a
      pandas DatetimeIndex.
    - marker and color are dictionaries that specify the marker and color
      for the symbol of each precipitation and land surface model dataset.
    - sep_legend indicates whether (True) the precipitation datasets are 
      indicated by markers ad the soil moisture datasets by colors, or 
      (False) the precipitation and soil moisture datasets are the same, and
      indicated by markers and colors jointly.
    - to_folder indicates which folder to save the graphs.    
    """
    lsm = list(sm_values.keys())
    fig, axes = plt.subplots(nrows=4, ncols=3, sharex=True,
                             sharey = True, figsize=(12,12))
    for i,y in it.product(range(grid_box.shape[0]), year):
        print('lat_bnds ' + str(i))
        print('year ' + str(y))

        for m in range(12):
            ax = axes.flat[m]
            ax.cla()
            for pr in pr_relevant:
                # Find the subset mean of precipitation data.
                pr_grid = extract_grid( np.squeeze( \
                    pr_values[pr][(time.year == y) & \
                                  (time.month == m+1), :, :], axis=0),
                                        pr_coords[pr][0],
                                        pr_coords[pr][1],
                                        grid_box[i,0], grid_box[i,1],
                                        n_sq_gr )
                for l in lsm_relevant[pr]:
                    l_grid = extract_grid( np.squeeze( \
                        sm_values[l][(time.year == y) & \
                                     (time.month == m+1),
                                     :, :], axis=0),
                                           sm_coords[l][0],
                                           sm_coords[l][1],
                                           grid_box[i,0], grid_box[i,1],
                                           n_sq_gr )
                    ax.plot(pr_grid, l_grid, marker[pr], color = color[l],
                            markersize=8)
            ax.set_xlabel('Pr (mm/day)')
            ax.set_ylabel(r'$\theta$ (m$^3$/m$^3$)')
            ax.set_title('Month ' + str(m+1))
        # Dummy legend
        hall = []
        if sep_legend:
            for pr in pr_relevant:
                hl, = plt.plot(np.nan, np.nan, marker[pr],
                               markerfacecolor = 'w',
                               markeredgecolor = 'k')
                hall.append(hl)
            for l in lsm:
                hl, = plt.plot(np.nan, np.nan, 's', color = color[l])
                hall.append(hl)
            ax.legend(hall, pr_relevant + lsm, ncol = 4,
                      bbox_to_anchor = (-0.3, -0.5))
        else:
            for pr in pr_relevant:
                hl, = plt.plot(np.nan, np.nan, marker[pr], color = color[pr])
                hall.append(hl)
            ax.legend(hall, pr_relevant, ncol = 4,
                      bbox_to_anchor = (-0.3, -0.5))
        fig.savefig(os.path.join(to_folder, 
                                 'gridbox' + str(i) + '_' + \
                                 str(n_sq_gr * n_sq_gr) + 'grid_' + \
                                 'range_'+str(year[0])+'-'+str(year[-1])+ \
                                 '_' +str(y) + '.png'), dpi=600.,
                    bbox_inches = 'tight')
    plt.close(fig)


def em_gridbox_month_plot(grid_box, n_sq_gr, pr_relevant, lsm_relevant,
                          pr_values, sm_values, pr_coords, sm_coords, year,
                          time, marker, color, sep_legend, to_folder):
    """
    Plot the emergent constraint relationship between latitude-band averaged
     precipitation and soil moisture, for each month, pooled for all years.

    Input:
    - grid_box is a n*2 array containing the lat & lon of the grid boxes of
      interest.
    - n_sq_gr is the sqrt of the number of nearest grids to extract.
    - pr_relevant list the precipitation datasets.
    - lsm_relevant is a dictionary that lists the land surface model (or 
      climate model) datasets that correspond to each precipitation dataset.
    - pr_values and sm_values are dictionaries that contain one numpy array
      under each dataset name.
    - pr_coords and sm_coords are dictionaries that contain the latitude, 
      and longitude coordinates for each dataset.
    - year is the list of years to include in the plotting.
    - time is the time coordinate shared by all datasets. It should be a
      pandas DatetimeIndex.
    - marker and color are dictionaries that specify the marker and color
      for the symbol of each precipitation and land surface model dataset.
    - sep_legend indicates whether (True) the precipitation datasets are 
      indicated by markers ad the soil moisture datasets by colors, or 
      (False) the precipitation and soil moisture datasets are the same, and
      indicated by markers and colors jointly.
    - to_folder indicates which folder to save the graphs.
    """
    lsm = list(sm_values.keys())
    fig, axes = plt.subplots(nrows=4, ncols=3, sharex=True, sharey = True,
                             figsize=(12,12))
    for i in range(grid_box.shape[0]):
        print('lat_bnds ' + str(i))

        for m in range(12):
            ax = axes.flat[m]
            ax.cla()
            for pr in pr_relevant:
                # Find the subset mean of precipitation data.
                pr_grid = extract_grid( \
                    pr_values[pr][(time.month == m+1), :, :],
                                        pr_coords[pr][0], pr_coords[pr][1],
                                        grid_box[i,0], grid_box[i,1],
                                        n_sq_gr )
                for l in lsm_relevant[pr]:
                    l_grid = extract_grid( \
                        sm_values[l][(time.month == m+1), :, :],
                                           sm_coords[l][0],
                                           sm_coords[l][1],
                                           grid_box[i,0], grid_box[i,1],
                                           n_sq_gr )
                    ax.plot(pr_grid, l_grid, marker[pr],
                            color = color[l], markersize=2)
                ax.set_xlabel('Pr (mm/day)')
                ax.set_ylabel(r'$\theta$ (m$^3$/m$^3$)')
                ax.set_title('Month ' + str(m+1))
        hall = []
        if sep_legend:
            for pr in pr_relevant:
                hl, = plt.plot(np.nan, np.nan, marker[pr],
                               markerfacecolor = 'w',
                               markeredgecolor = 'k')
                hall.append(hl)
            for l in lsm:
                hl, = plt.plot(np.nan, np.nan, 's', color = color[l])
                hall.append(hl)
            ax.legend(hall, pr_relevant + lsm, ncol = 4,
                      bbox_to_anchor = (-0.3, -0.5))
        else:
            for pr in pr_relevant:
                hl, = plt.plot(np.nan, np.nan, marker[pr], color = color[pr])
                hall.append(hl)
            ax.legend(hall, pr_relevant, ncol = 4,
                      bbox_to_anchor = (-0.3, -0.5))

        fig.savefig(os.path.join(to_folder,
                                 'gridbox' + str(i) + '_' + \
                                 str(n_sq_gr * n_sq_gr) + 'grid_' + \
                                 'range_' + str(year[0]) + '-' + \
                                 str(year[-1])+'.png'), 
                    dpi=600., bbox_inches = 'tight')
        plt.close(fig)


def save_yearmonth_corr(corr, corr_p, corr_CI_lower, corr_CI_upper,
                        intercept, intercept_p, intercept_CI_lower,
                        intercept_CI_upper, predicted, 
                        predicted_CI_lower, predicted_CI_upper,
                        em_not_applied, time, target_lat, target_lon, year,
                        n_sq_gr, path_folder):
    """
    Save the regression results when the correlation are calculated for 
    each month in each year.
    """
    xr.DataArray(data = corr, coords = {'time': time, 'lat': target_lat,
                                        'lon': target_lon},
                 dims = ['time', 'lat', 'lon']).to_dataset(name='corr' \
            ).to_netcdf(os.path.join(path_folder, 'corr_' +
                                     str(year[0])+'-'+str(year[-1]) + \
                                     '_' + str(n_sq_gr*n_sq_gr) + 'grid.nc'))
    xr.DataArray(data = corr_p, coords = {'time': time, 'lat': target_lat,
                                          'lon': target_lon},
                 dims = ['time', 'lat', 'lon']).to_dataset(name='corr_p' \
            ).to_netcdf(os.path.join(path_folder, 'corr_p_' +
                                     str(year[0])+'-'+str(year[-1]) + \
                                     '_' + str(n_sq_gr*n_sq_gr) + 'grid.nc'))
    xr.DataArray(data = corr_CI_lower, coords = {'time': time,
                                                 'lat': target_lat,
                                                 'lon': target_lon},
                 dims = ['time', 'lat', 'lon']).to_dataset( \
                 name='corr_CI_lower').to_netcdf( \
                 os.path.join(path_folder, 'corr_CI_lower_' + \
                              str(year[0])+'-'+str(year[-1]) + \
                              '_' + str(n_sq_gr*n_sq_gr) + 'grid.nc'))
    xr.DataArray(data = corr_CI_upper, coords = {'time': time,
                                                 'lat': target_lat,
                                                 'lon': target_lon},
                 dims = ['time', 'lat', 'lon']).to_dataset( \
                 name='corr_CI_upper').to_netcdf( \
                 os.path.join(path_folder, 'corr_CI_upper_' + \
                              str(year[0])+'-'+str(year[-1]) + \
                              '_' + str(n_sq_gr*n_sq_gr) + 'grid.nc'))


    xr.DataArray(data = intercept, coords = {'time': time, 'lat': target_lat,
                                             'lon': target_lon},
                 dims = ['time', 'lat', 'lon']).to_dataset(name='intercept' \
            ).to_netcdf(os.path.join(path_folder, 'intercept_' +
                                     str(year[0])+'-'+str(year[-1]) + \
                                     '_' + str(n_sq_gr*n_sq_gr) + 'grid.nc'))
    xr.DataArray(data = intercept_p, coords = {'time': time, 'lat': target_lat,
                                               'lon': target_lon},
                 dims = ['time', 'lat', 'lon']).to_dataset(name='intercept_p' \
            ).to_netcdf(os.path.join(path_folder, 'intercept_p_' +
                                     str(year[0])+'-'+str(year[-1]) + \
                                     '_' + str(n_sq_gr*n_sq_gr) + 'grid.nc'))
    xr.DataArray(data = intercept_CI_lower, coords = {'time': time,
                                                      'lat': target_lat,
                                                      'lon': target_lon},
                 dims = ['time', 'lat', 'lon']).to_dataset( \
                 name='intercept_CI_lower').to_netcdf( \
                 os.path.join(path_folder, 'intercept_CI_lower_' + \
                              str(year[0])+'-'+str(year[-1]) + \
                              '_' + str(n_sq_gr*n_sq_gr) + 'grid.nc'))
    xr.DataArray(data = intercept_CI_upper, coords = {'time': time,
                                                      'lat': target_lat,
                                                      'lon': target_lon},
                 dims = ['time', 'lat', 'lon']).to_dataset( \
                 name='intercept_CI_upper').to_netcdf( \
                 os.path.join(path_folder, 'intercept_CI_upper_' + \
                              str(year[0])+'-'+str(year[-1]) + \
                              '_' + str(n_sq_gr*n_sq_gr) + 'grid.nc'))


    xr.DataArray(data = predicted, coords = {'time': time, 'lat': target_lat,
                                             'lon': target_lon},
                 dims = ['time', 'lat', 'lon']).to_dataset(name='predicted' \
            ).to_netcdf(os.path.join(path_folder, 'predicted_' +
                                     str(year[0])+'-'+str(year[-1]) + \
                                     '_' + str(n_sq_gr*n_sq_gr) + 'grid.nc'))
    xr.DataArray(data = predicted_CI_lower, coords = {'time': time,
                                                      'lat': target_lat,
                                                      'lon': target_lon},
                 dims = ['time', 'lat', 'lon']).to_dataset( \
                 name='predicted_CI_lower').to_netcdf( \
                 os.path.join(path_folder, 'predicted_CI_lower_' + \
                              str(year[0])+'-'+str(year[-1]) + \
                              '_' + str(n_sq_gr*n_sq_gr) + 'grid.nc'))
    xr.DataArray(data = predicted_CI_upper, coords = {'time': time,
                                                      'lat': target_lat,
                                                      'lon': target_lon},
                 dims = ['time', 'lat', 'lon']).to_dataset( \
                 name='predicted_CI_upper').to_netcdf( \
                 os.path.join(path_folder, 'predicted_CI_upper_' + \
                              str(year[0])+'-'+str(year[-1]) + \
                              '_' + str(n_sq_gr*n_sq_gr) + 'grid.nc'))


    xr.DataArray(data = em_not_applied, coords = {'time': time,
                                                  'lat': target_lat,
                                                  'lon': target_lon},
                 dims = ['time', 'lat', 'lon']).to_dataset( \
                 name='em_not_applied').to_netcdf( \
                 os.path.join(path_folder, 'em_not_applied_' +
                              str(year[0])+'-'+str(year[-1]) + \
                              '_' + str(n_sq_gr*n_sq_gr) + 'grid.nc'))


def save_month_corr(corr, corr_p, corr_CI_lower, corr_CI_upper,
                    intercept, intercept_p, intercept_CI_lower,
                    intercept_CI_upper, predicted,
                    predicted_CI_lower, predicted_CI_upper,
                    em_not_applied, time, target_lat, target_lon, year,
                    n_sq_gr, path_folder):
    """
    Save the regression results when the correlation are calculated for 
    each month for all years.
    """
    xr.DataArray(data = corr, coords = {'month': range(1,13), 
                                        'lat': target_lat, 'lon': target_lon},
                 dims = ['month', 'lat', 'lon']).to_dataset(name = 'corr' \
                ).to_netcdf(os.path.join(path_folder, 'corr_' +
                                         str(year[0])+'-'+str(year[-1]) + \
                                         '_' + str(n_sq_gr*n_sq_gr) + \
                                         'grid.nc'))
    xr.DataArray(data = corr_p, coords = {'month': range(1,13),
                                          'lat':target_lat, 'lon':target_lon},
                 dims = ['month', 'lat', 'lon']).to_dataset(name = 'corr_p' \
                ).to_netcdf(os.path.join(path_folder, 'corr_p_' +
                                         str(year[0])+'-'+str(year[-1]) + \
                                         '_' + str(n_sq_gr*n_sq_gr) + \
                                         'grid.nc'))
    xr.DataArray(data = corr_CI_lower, coords = {'month': range(1,13), 
                                                 'lat': target_lat,
                                                 'lon': target_lon},
                 dims = ['month', 'lat', 'lon']).to_dataset( \
                 name = 'corr_CI_lower').to_netcdf( \
                 os.path.join(path_folder, 'corr_CI_lower_' + \
                              str(year[0])+'-'+str(year[-1]) + \
                              '_' + str(n_sq_gr*n_sq_gr) + 'grid.nc'))
    xr.DataArray(data = corr_CI_upper, coords = {'month': range(1,13),
                                                 'lat': target_lat,
                                                 'lon': target_lon},
                 dims = ['month', 'lat', 'lon']).to_dataset( \
                 name = 'corr_CI_upper').to_netcdf( \
                 os.path.join(path_folder, 'corr_CI_upper_' + \
                              str(year[0])+'-'+str(year[-1]) + \
                              '_' + str(n_sq_gr*n_sq_gr) + 'grid.nc'))


    xr.DataArray(data = intercept, coords = {'month': range(1,13),
                                             'lat': target_lat,
                                             'lon': target_lon},
                 dims = ['month','lat','lon']).to_dataset(name = 'intercept' \
                ).to_netcdf(os.path.join(path_folder, 'intercept_' +
                                         str(year[0])+'-'+str(year[-1]) + \
                                         '_' + str(n_sq_gr*n_sq_gr) + \
                                         'grid.nc'))
    xr.DataArray(data = intercept_p, coords = {'month': range(1,13),
                                               'lat':target_lat,
                                               'lon':target_lon},
                 dims = ['month', 'lat', 'lon'] \
                ).to_dataset(name = 'intercept_p' \
                ).to_netcdf(os.path.join(path_folder, 'intercept_p_' +
                                         str(year[0])+'-'+str(year[-1]) + \
                                         '_' + str(n_sq_gr*n_sq_gr) + \
                                         'grid.nc'))
    xr.DataArray(data = intercept_CI_lower, coords = {'month': range(1,13),
                                                      'lat': target_lat,
                                                      'lon': target_lon},
                 dims = ['month', 'lat', 'lon']).to_dataset( \
                 name = 'intercept_CI_lower').to_netcdf( \
                 os.path.join(path_folder, 'intercept_CI_lower_' + \
                              str(year[0])+'-'+str(year[-1]) + \
                              '_' + str(n_sq_gr*n_sq_gr) + 'grid.nc'))
    xr.DataArray(data = intercept_CI_upper, coords = {'month': range(1,13),
                                                      'lat': target_lat,
                                                      'lon': target_lon},
                 dims = ['month', 'lat', 'lon']).to_dataset( \
                 name = 'intercept_CI_upper').to_netcdf( \
                 os.path.join(path_folder, 'intercept_CI_upper_' + \
                              str(year[0])+'-'+str(year[-1]) + \
                              '_' + str(n_sq_gr*n_sq_gr) + 'grid.nc'))


    xr.DataArray(data = predicted, coords = {'time': time, 'lat': target_lat,
                                             'lon': target_lon},
                 dims = ['time', 'lat', 'lon']).to_dataset(name='predicted' \
                ).to_netcdf(os.path.join(path_folder, 'predicted_' +
                                         str(year[0])+'-'+str(year[-1]) + \
                                         '_' + str(n_sq_gr*n_sq_gr) + \
                                         'grid.nc'))
    xr.DataArray(data = predicted_CI_lower, coords = {'time': time,
                                                      'lat': target_lat,
                                                      'lon': target_lon},
                 dims = ['time', 'lat', 'lon']).to_dataset( \
                 name='predicted_CI_lower').to_netcdf( \
                 os.path.join(path_folder, 'predicted_CI_lower_' + \
                              str(year[0])+'-'+str(year[-1]) + \
                              '_' + str(n_sq_gr*n_sq_gr) + 'grid.nc'))
    xr.DataArray(data = predicted_CI_upper, coords = {'time': time,
                                                      'lat': target_lat,
                                                      'lon': target_lon},
                 dims = ['time', 'lat', 'lon']).to_dataset( \
                 name='predicted_CI_upper').to_netcdf( \
                 os.path.join(path_folder, 'predicted_CI_upper_' + \
                              str(year[0])+'-'+str(year[-1]) + \
                              '_' + str(n_sq_gr*n_sq_gr) + 'grid.nc'))


    xr.DataArray(data = em_not_applied, coords = {'month': range(1,13), 
                                                  'lat': target_lat,
                                                  'lon': target_lon},
                 dims = ['month', 'lat', 'lon']).to_dataset( \
                 name = 'em_not_applied').to_netcdf( \
                 os.path.join(path_folder, 'em_not_applied_' +
                              str(year[0])+'-'+str(year[-1]) + '_' + \
                              str(n_sq_gr*n_sq_gr) + 'grid.nc'))


def rm_oor(data_array, path, prefix, suffix, period):
    """
    Remove soil moisture values < 0. (set to 1e-16) or > 1. (set to 1-1e-16).
    """
    negative = data_array < 0.
    too_big = data_array > 1.

    abnormal = xr.Dataset({'negative': negative, 'too_big': too_big})

    if (len(prefix) > 0) and (prefix[-1] != '_'):
        prefix = prefix + '_'

    abnormal.to_netcdf(os.path.join(path, 
                                    prefix + 'abnormal_' + suffix + '.nc'))

    for x in ['negative', 'too_big']:
        if x == 'negative':
            ts_x = negative.sum(dim = 'lat').sum(dim = 'lon').values
            map_x = negative.sum(dim = 'time').values
        else:
            ts_x = too_big.sum(dim = 'lat').sum(dim = 'lon').values
            map_x = too_big.sum(dim = 'time').values

        fig = plt.figure(figsize = (12, 12))
        ax = fig.add_subplot(211)
        ax.plot(period.year.values + (period.month.values-0.5)/12, ts_x)
        ax.set_xticks(period.year[::60])
        ax.set_title('# ' + x + ' per time step')

        ax = fig.add_subplot(212, projection = ccrs.PlateCarree())
        ax.coastlines()
        ax.set_extent([-180, 180, -60, 90])
        h = ax.contourf(data_array.lon.values, data_array.lat.values, map_x,
                        corner_mask = False)
        plt.colorbar(h, ax = ax, shrink = 0.7)

        fig.savefig(os.path.join(path, 
                                 prefix + x + '_' + suffix + '.png'), 
                    dpi = 600.)
        plt.close(fig)

    corrected = np.where(data_array.values < 0., 1e-16, data_array.values)
    corrected = np.where(corrected > 1., 1 - 1e-16, corrected)

    if not prefix:
        xr.DataArray(corrected, coords = {'time': data_array.time,
                                          'lat': data_array.lat, 
                                          'lon': data_array.lon},
        dims = ['time', 'lat', 'lon']).to_dataset(name = data_array.name \
        ).to_netcdf(os.path.join(path, suffix + '.nc'))
    else:
        xr.DataArray(corrected, coords = {'time': data_array.time,
                                          'lat': data_array.lat, 
                                          'lon': data_array.lon},
        dims = ['time', 'lat', 'lon']).to_dataset(name = data_array.name \
        ).to_netcdf(os.path.join(path, prefix + suffix + '.nc'))
