from .analyze_utils import calc_ts_trend
import pandas as pd
import numpy as np


def get_global_trend(path_prefix, depth, time_range):
    """
    Given prefix to the standard diagnostics by-latitude time series, 
    calculate the trend over time_range for the global mean.
    """
    data = pd.read_csv(path_prefix + '_' + depth + '_g_ts.csv',
                       index_col = 0, parse_dates = True).loc[:, 'global_mean']
    ts = data.loc[time_range] # Subset to the required time range.

    trend = pd.DataFrame(data = np.nan, index = ['trend', 'p_value'],
                         columns = ['annual'] + \
                         [str(x) for x in range(1,13)])

    # Annual time series, calculate trend
    ts_annual = ts.groupby(ts.index.year).mean()
    slope, intercept, p_value = \
        calc_ts_trend(range(len(ts_annual)), ts_annual.values)

    ##if p_value < 0.05:
    ##    trend.loc[lat, 'annual'] = slope
    trend.loc['trend', 'annual'] = slope
    trend.loc['p_value', 'annual'] = p_value

    # Monthly time series, calculate trend
    for m in range(1,13):
        ts_m = ts.loc[ts.index.month == m]
        slope, intercept, p_value = \
            calc_ts_trend(range(len(ts_m)), ts_m.values)

        ##if p_value < 0.05:
        ##    trend.loc[lat, str(m)] = slope
        trend.loc['trend', str(m)] = slope
        trend.loc['p_value', str(m)] = p_value

    return trend


def get_bylat_climatology(path_prefix, depth, time_range):
    """
    Given prefix to the standard diagnostics by-latitude time series, 
    calculate the climatology over time_range for each latitude band.
    """
    data = pd.read_csv(path_prefix + '_' + depth + '_g_lat_ts.csv',
                       index_col = 0, parse_dates = True)
    data = data.loc[time_range, :] # Subset to the required time range.

    clim = pd.DataFrame(data = np.nan, columns = data.columns,
                        index = ['annual'] + \
                        [str(x) for x in range(1,13)])

    # Annual time series, calculate mean
    clim.loc['annual', :] = data.mean(axis = 0)

    # Monthly time series, calculate mean
    for m in range(1,13):
        clim.loc[str(m), :] = data.loc[data.index.month == m].mean(axis = 0)

    return clim


def get_bylat_trend(path_prefix, depth, time_range):
    """
    Given prefix to the standard diagnostics by-latitude time series, 
    calculate the trend over time_range for each latitude band.
    """
    data = pd.read_csv(path_prefix + '_' + depth + '_g_lat_ts.csv',
                       index_col = 0, parse_dates = True)
    data = data.loc[time_range, :] # Subset to the required time range.

    trend = pd.DataFrame(data = np.nan, index = data.columns,
                         columns = ['annual'] + \
                         [str(x) for x in range(1,13)])
    trend_p = pd.DataFrame(data = np.nan, index = data.columns,
                           columns = ['annual'] + \
                           [str(x) for x in range(1,13)])

    for lat in data.columns:
        ts = data.loc[:, lat]

        # Annual time series, calculate trend
        ts_annual = ts.groupby(ts.index.year).mean()
        slope, intercept, p_value = \
            calc_ts_trend(range(len(ts_annual)), ts_annual.values)

        ##if p_value < 0.05:
        ##    trend.loc[lat, 'annual'] = slope
        trend.loc[lat, 'annual'] = slope
        trend_p.loc[lat, 'annual'] = p_value

        # Monthly time series, calculate trend
        for m in range(1,13):
            ts_m = ts.loc[ts.index.month == m]
            slope, intercept, p_value = \
                calc_ts_trend(range(len(ts_m)), ts_m.values)

            ##if p_value < 0.05:
            ##    trend.loc[lat, str(m)] = slope
            trend.loc[lat, str(m)] = slope
            trend_p.loc[lat, str(m)] = p_value

    return trend, trend_p
