"""
20190730
ywang254@utk.edu
"""
import os
import sys
from glob import glob
import numpy as np

"""
def build_cori_path(doc):
    ##Construct the path to CMIP6 soil moisture files using the json dictionary
    ##of one file from esgf search. Works on cori path structure.
    path_cmip6 = '/global/cscratch1/sd/cmip6/CMIP6/'
    file_folder = os.path.join(path_cmip6, doc['activity_id'][0],
                               doc['institution_id'][0], doc['source_id'][0],
                               doc['experiment_id'][0], doc['member_id'][0],
                               doc['table_id'][0], doc['variable'][0],
                               doc['grid_label'][0], 'v' + doc['version'])
    return file_folder
"""


def mrsol_availability(dcm, land_mask, expr):
    """
    Find the availability of interpolated soil moisture at given depth, 
    return the list of ModelName_EnsembleID.
    """
    if sys.platform == 'win32':
        path = 'D:/Projects/2018 Soil Moisture'
    elif sys.platform == 'linux':
        if 'cori' in os.environ['HOSTNAME']:
            path = os.path.join(os.environ['SCRATCH'], 'Soil_Moisture')
        else:
            #path = '/lustre/haven/user/ywang254/Soil_Moisture'
            path = os.path.join(os.environ['PROJDIR'], 'Soil_Moisture')


    folders = os.listdir(os.path.join(path, 'intermediate', 'Interp_Merge',
                                      land_mask, 'CMIP6'))
    folder2 = []
    for f in folders:
        f_list = glob(os.path.join(path, 'intermediate', 'Interp_Merge',
                                   land_mask, 'CMIP6', f, 
                                   'mrsol_' + expr + '_*_' + dcm + '.nc'))
        if len(f_list) > 0:
            folder2.append(f)

    return folder2


def one_layer_availability(var, land_mask, expr):
    """
    Find the availability of one-layer variables (pr, tas, evspsbl, mrsos)
    return the list of ModelName_EnsembleID.
    """
    if sys.platform == 'win32':
        path = 'D:/Projects/2018 Soil Moisture'
    elif sys.platform == 'linux':
        if 'cori' in os.environ['HOSTNAME']:
            path = os.path.join(os.environ['SCRATCH'], 'Soil_Moisture')
        else:
            #path = '/lustre/haven/user/ywang254/Soil_Moisture'
            path = os.path.join(os.environ['PROJDIR'], 'Soil_Moisture')

    folders = os.listdir(os.path.join(path, 'intermediate', 'Interp_Merge',
                                      land_mask, 'CMIP6'))
    folder2 = []
    for f in folders:
        f_list = glob(os.path.join(path, 'intermediate', 'Interp_Merge',
                                   land_mask, 'CMIP6', f, 
                                   var + '_' + expr + '_*.nc'))
        if len(f_list) > 0:
            folder2.append(f)

    return folder2
