import numpy as np
import sys
import os
import xarray as xr
import pandas as pd
from glob import glob
from dateutil.relativedelta import relativedelta
import multiprocessing as mp
from scipy.stats import linregress


if sys.platform == 'win32':
    path = 'D:/Projects/2018 Soil Moisture'
elif sys.platform == 'linux':
    if 'cori' in os.environ['HOSTNAME']:
        path = os.path.join(os.environ['SCRATCH'], 'Soil_Moisture')
    else:
        path = os.path.join(os.environ['PROJDIR'], 'Soil_Moisture')
path_out = os.path.join(path, 'output_product')


def decode_month_since(time):
    ref = pd.Timestamp(time.attrs['units'].split(' ')[2])
    start = ref + relativedelta(months = time.values[0])
    start = start.replace(day = 1, hour = 0, minute = 0, second = 0)
    return pd.date_range(start, periods = len(time), freq = 'MS')


def get_prod_list(mset, d, dcm):
    year_longest = range(1950, 2017)

    if mset == 'mean_lsm':
        flist = [os.path.join(path_out, 'meanmedian_lsm',
                              'mean_' + d + '_' + \
                              str(year_longest[0]) + '-' + \
                              str(year_longest[-1]) + '.nc')]
    elif mset == 'dolce_lsm':
        flist = [os.path.join(path_out, 'concat_dolce_lsm',
                              'positive_average_' + d + \
                              '_lu_weighted_ShrunkCovariance.nc')]
    elif (mset == 'em_all') | (mset == 'em_lsm'):
        flist = [os.path.join(path_out, 'concat_' + mset,
                              'positive_CRU_v4.03_year_month_anomaly_' + \
                              '9grid_' + dcm + '_predicted_' + \
                              str(year_longest[0]) + '-' + \
                              str(year_longest[-1]) + '.nc')]
    else:
        flist = [os.path.join(path_out, mset + '_corr',
                              'CRU_v4.03_year_month_positive_9grid_' + \
                              dcm + '_' + str(year_longest[0]) + \
                              '-' + str(year_longest[-1]), 'predicted.nc')]
    return flist


def get_sm(flist, yrng):
    hr = xr.open_mfdataset(flist, decode_times = False)
    if ('CRU_v4.03_year_month_positive_9grid_0-10cm_1950-2016' in flist[0]) |\
       ('CRU_v4.03_year_month_positive_9grid_10-30cm_1950-2016' in flist[0]) |\
       ('CRU_v4.03_year_month_positive_9grid_30-50cm_1950-2016' in flist[0]) |\
       ('CRU_v4.03_year_month_positive_9grid_50-100cm_1950-2016' in flist[0]):
        hr['time'].attrs['units'] = 'months since 1950-01-01'
    try:
        sm = hr['sm']
    except:
        sm = hr['predicted']
    if 'month' in sm['time'].attrs['units']:
        tvec = decode_month_since(sm['time'])
    else:
        tvec = xr.decode_cf(hr)['time'].to_index()
    sm = sm[(tvec.year >= yrng[0]) & (tvec.year <= yrng[-1]), :, :]
    sm['time'] = tvec[(tvec.year >= yrng[0]) & (tvec.year <= yrng[-1])]
    hr.close()
    return sm


def calc_trend(vector, order):
    x = np.arange( len(vector) )
    y = vector
    temp = ~(np.isnan(x) | np.isnan(y))
    x = x[temp]
    y = y[temp]
    slope, _, _, slope_p, _ = linregress(x, y)
    return [order, slope, slope_p]


def get_trend(sm):
    #start = time()

    lat = sm['lat']
    lon = sm['lon']
    sm = sm.values

    retain = np.where((~np.isnan(np.nanmean(sm, axis = 0))).reshape(-1))[0]
    sm_temp = np.array_split(sm.reshape(-1, sm.shape[1]*sm.shape[2] \
    )[:, retain], len(retain), axis = 1)

    p = mp.Pool(4)
    result = [p.apply_async(calc_trend, args = (smt.reshape(-1), order)) \
              for order,smt in enumerate(sm_temp)]
    p.close()
    p.join()

    result = [rr.get() for rr in result]

    trend = np.full(sm.shape[1] * sm.shape[2], np.nan)
    trend_p = np.full(sm.shape[1] * sm.shape[2], np.nan)
    for rr in range(len(result)):
        trend[retain[result[rr][0]]] = result[rr][1]
        trend_p[retain[result[rr][0]]] = result[rr][2]

    trend = xr.DataArray(trend.reshape(sm.shape[1], sm.shape[2]),
                         dims = ['lat', 'lon'],
                         coords = {'lat': lat, 'lon': lon})
    trend_p = xr.DataArray(trend_p.reshape(sm.shape[1], sm.shape[2]),
                           dims = ['lat', 'lon'],
                           coords = {'lat': lat, 'lon': lon})
    #end = time()
    #print(end - start)

    return trend, trend_p


def get_anomaly(sm, sm_seasonal, sm_trend):
    sm_anomalies = np.full(sm.shape, np.nan)
    for tt in range(sm.shape[0]):
        ##print(sm['time'].to_index())
        month = sm['time'].to_index().month[tt]
        ##print(month)
        sm_anomalies[tt, :, :] = sm.values[tt, :, :] - \
            sm_seasonal[month - 1, :, :] - \
            sm_trend * (tt - (sm.shape[0]-1)/2)
    sm_anomalies = xr.DataArray(sm_anomalies, dims = ['time', 'lat', 'lon'],
                                coords = {'time': sm['time'],
                                          'lat': sm['lat'],
                                          'lon': sm['lon']})
    return sm_anomalies


def get_anomaly2(sm, sm_seasonal):
    sm_anomalies = np.full(sm.shape, np.nan)
    for tt in range(sm.shape[0]):
        month = sm['time'].to_index().month[tt]
        sm_anomalies[tt, :, :] = sm.values[tt, :, :] - \
            sm_seasonal[month - 1, :, :]
    sm_anomalies = xr.DataArray(sm_anomalies, dims = ['time', 'lat', 'lon'],
                                coords = {'time': sm['time'],
                                          'lat': sm['lat'],
                                          'lon': sm['lon']})
    return sm_anomalies


def standard_diagnostics2(name, d, dcm, yrng):
    flist = get_prod_list(name, d, dcm)

    sm = get_sm(flist, yrng)

    # seasonality
    sm_seasonal = sm.groupby('time.month').mean()

    # trend
    sm_trend, sm_trend_p = get_trend(sm)

    # anomalies - no removal of trend
    sm_anomalies = get_anomaly(sm, sm_seasonal, sm_trend)
    ##sm_anomalies = get_anomaly2(sm, sm_seasonal)

    sm_anomalies_std = sm_anomalies.std(dim = 'time')

    xr.Dataset({'seasonal': (('month','lat','lon'), sm_seasonal),
                'trend': (('lat','lon'), sm_trend),
                'trend_p': (('lat','lon'), sm_trend_p),
                'anomalies_std': (('lat','lon'), sm_anomalies_std)},
               coords = {'month': sm_seasonal['month'],
                         'lat': sm_seasonal['lat'],
                         'lon': sm_seasonal['lon']},
               ).to_netcdf(os.path.join(path, 'output_product',
                                        'standard_diagnostics2', 
                                        name + '_' + dcm + '_' + \
                                        str(yrng[0]) + '-' + \
                                        str(yrng[-1]) + '.nc'))
