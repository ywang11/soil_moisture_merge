# -*- coding: utf-8 -*-
"""
Created on Tue May  7 20:40:55 2019

@author: ywang254

Obtain monthly time series from ISMN station
"""
from ismn.interface import ISMN_Interface
from .ismn_availability import ismn_availability
import numpy as np


def ismn_get_monthly(network_path, station, depth_actual, sensor, 
                     time_range):

    ISMN_reader = ISMN_Interface(network_path)

    if (station == 2.1) | (station == '2.1'):
        station = '2.10' # manual correction for a HOBE station
    if (station == 1.1) | (station == '1.1'):
        station = '1.10' # manual correction for another HOBE station

    station_obj = ISMN_reader.get_station(str(station)) # note: some station names are float-like

    # get the soil moisture data and flags
    d = [float(x) for x in depth_actual.split('-')]
    temp = (np.abs(station_obj.depth_from - d[0]) < 0.01) & \
        (np.abs(station_obj.depth_to - d[1]) < 0.01)
    if sum(temp) > 0:
        d_from = station_obj.depth_from[temp][0]
        d_to = station_obj.depth_to[temp][0]
    else:
        d_from = d[0]
        d_to = d[-1]

    time_series = station_obj.read_variable('soil moisture', 
                                            depth_from=d_from, depth_to=d_to, sensor=sensor)
    
    # subset to the time_range
    time_series.data = time_series.data.loc[ \
        (time_series.data.index >= time_range[0]) & \
        (time_series.data.index <= time_range[-1]), :]

    # get the monthly time series
    _, monthly_data = ismn_availability(time_series.data,
                                        aggregate = True)
    
    return monthly_data
