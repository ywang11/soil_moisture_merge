# -*- coding: utf-8 -*-
"""
Created on Wed May  8 20:34:09 2019

@author: ywang254

Various convenience functions for the weighting scripts.
Not for general purpose. 
"""
import numpy as np
import xarray as xr
import pandas as pd
import os


# Function to get the soil moisture of individual grid cells from the
# weighted product.
def get_weighted_sm_points(grid_latlon, weighted_sm, year):
    weighted_sm_at_data = pd.DataFrame(data=np.nan, 
        index=pd.to_datetime([str(y) + '-' + ('%02d' % m) + '-01' \
                              for y in year for m in range(1,13)],
                             format='%Y-%m-%d'),
        columns = grid_latlon.columns)

    # ---- create a mask
    gr_mask = np.zeros(weighted_sm.shape[1:], bool)
    gr_mask_label = np.array([['        '] * weighted_sm.shape[2]] * \
                             weighted_sm.shape[1])
    for gr in grid_latlon.columns:
        ind_x = int(np.argmin(np.abs(weighted_sm.lat - \
                    grid_latlon.loc['Lat', gr])))
        ind_y = int(np.argmin(np.abs(weighted_sm.lon - \
                    grid_latlon.loc['Lon', gr])))
        gr_mask[ind_x, ind_y] = True
        gr_mask_label[ind_x, ind_y] = gr

    gr_mask_label = gr_mask_label[gr_mask].reshape(-1)

    for t in range(weighted_sm.shape[0]):
        sm_values = weighted_sm.values[t, :, :]
        weighted_sm_at_data.loc[weighted_sm_at_data.index[t],
                                gr_mask_label] = \
            sm_values[gr_mask].reshape(-1)
    return weighted_sm_at_data


# Function to get the soil moisture from land surface models at individual
# grid cells.
def get_lsm_sm_points(grid_latlon, prefix, year, suffix):    
    lsm_sm = pd.DataFrame(data=np.nan, 
                          index=pd.to_datetime([str(y) + '-' + ('%02d' % m) + \
                                '-01' for y in year for m in range(1,13)],
                                format='%Y-%m-%d'), 
                          columns=grid_latlon.columns)
                          
    # Need to make grid_latlon.columns and gr_mask match!!!

    # ---- create a mask
    data = xr.open_dataset(os.path.join(prefix + str(year[0]) + suffix + \
                           '.nc'), decode_times = False)
    gr_mask = np.zeros((len(data.lat), len(data.lon)), bool)
    gr_mask_label = np.array([['        '] * len(data.lon)] * len(data.lat))
    for gr in grid_latlon.columns:
        ind_x = int(np.argmin(np.abs(data.lat - grid_latlon.loc['Lat', gr])))
        ind_y = int(np.argmin(np.abs(data.lon - grid_latlon.loc['Lon', gr])))
        ##print(str(ind_x) + '_' + str(ind_y))
        gr_mask[ind_x, ind_y] = True
        gr_mask_label[ind_x, ind_y] = gr
    data.close()
    gr_mask = np.where(gr_mask.reshape(-1))[0]
    gr_mask_label = gr_mask_label.reshape(-1)[gr_mask]

    # ---- implement the mask
    for y in year:
        data = xr.open_dataset(os.path.join(prefix + str(y) + suffix + \
                               '.nc'), decode_times = False)
        sm_values = data.sm.values.reshape(12,-1)
        lsm_sm.loc[pd.to_datetime([str(y) + '-' + ('%02d' % m) + \
                   '-01' for m in range(1,13)], format='%Y-%m-%d'),
                   gr_mask_label] = sm_values[:, gr_mask]
        data.close()

    return lsm_sm


def get_cov_method(cov):
    if cov == 0:
        method = 'EmpiricalCovariance'
    elif cov == 1:
        method = 'ShrunkCovariance'
    elif cov == 2:
        method = 'MinCovDet'
    elif cov == 3:
        method = 'LedoitWolf'
    elif cov == 4:
        method = 'OAS'
    return method
