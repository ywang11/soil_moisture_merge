import os
import pandas as pd
import numpy as np
import time
import xarray as xr
from datetime import datetime
import itertools as it
from scipy.stats import pearsonr, linregress, ranksums, fligner


def calc_q_test(matched, year_of_break):
    target = matched['target'].values
    stations = matched.drop('target', axis = 1).values
    period_a = matched.index.year < year_of_break
    period_b = matched.index.year >= year_of_break

    # (1) fill the values of missing stations, if there are overlaps, and the station
    #     has correlation > 0.8, and p-value < 0.01.
    def _fillnan(stations):
        has_nan = np.any(np.isnan(stations), axis = 0)
        stations_filled = stations.copy()
    
        nsuitable = np.full([stations_filled.shape[1],
                             stations_filled.shape[1]], 0, dtype = int)
        for i in range(stations_filled.shape[1]):
            for j in range(i+1, stations_filled.shape[1]):
                x = stations[:,i]
                y = stations[:,j]
                temp = ~np.isnan(x) & ~np.isnan(y)
                x = x[temp]
                y = y[temp]
                if len(x) > 3:
                    coef, pval = pearsonr(x, y)
                    ##print(coef, pval)
                    if (coef >= 0.8) & (pval <= 0.01):
                        nsuitable[j, i] = sum(np.isnan(stations[:,i]) & ~np.isnan(stations[:,j]))
                        nsuitable[i, j] = sum(np.isnan(stations[:,j]) & ~np.isnan(stations[:,i]))
    
        for i in range(stations_filled.shape[1]):
            if has_nan[i] & (max(nsuitable[:,i]) > 0):
                isuitable = np.argmax(nsuitable[:,i])
                # fill using linear regression
                x = stations[:,isuitable]
                y = stations[:,i]
                temp = ~np.isnan(x) & ~np.isnan(y)
                x = x[temp]
                y = y[temp]
                res = linregress(x, y)
                stations_filled[:,i] = np.where(np.isnan(stations[:,i]),
                                                res.slope * stations[:,isuitable] + \
                                                res.intercept, stations[:,i])
        return stations_filled
    stations_filled = _fillnan(stations)

    # (2) calculate the q values.
    def _calcq(target, stations_filled):
        temp = np.any(np.isnan(stations_filled), axis = 1)
        target = target[~temp]
        stations_filled = stations_filled[~temp]

        beta = np.full(stations_filled.shape[1], 0.)
        cval = np.full(stations_filled.shape[1], 0.)     
        for i in range(stations_filled.shape[1]):
            y = target
            x = stations_filled[:,i]
            temp2 = ~np.isnan(x) & ~np.isnan(y)
            if sum(temp2) >= 3:
                x = x[temp2]
                y = y[temp2]
                res = linregress(x, y)
                if res.pvalue <= 0.05:
                    beta[i] = res.slope
                    cval[i] = res.intercept

        vval = np.power(beta, 2)
        qval_x = target - ((np.matmul(stations_filled,
                                      (vval * beta).reshape(-1,1)) + \
                            sum(vval * cval)) / np.sum(vval)).reshape(-1)
        qval = np.full(len(temp), np.nan)
        qval[~temp] = qval_x

        return qval

    qval_a = _calcq(target[period_a], stations_filled[period_a,:])
    qval_a = qval_a[~np.isnan(qval_a)]
    qval_b = _calcq(target[period_b], stations_filled[period_b,:])
    qval_b = qval_b[~np.isnan(qval_b)]

    # (3) perform the test
    if (len(qval_a) > 3) & (len(qval_b) > 3):
        _, wx_pval = ranksums(qval_a, qval_b)
        _, fl_pval = fligner(qval_a, qval_b)
    else:
        wx_pval = np.nan
        fl_pval = np.nan
    return wx_pval, fl_pval
