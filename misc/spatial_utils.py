#from osgeo import ogr
from IPython import embed
import sys
from rasterio import features
from affine import Affine
import numpy as np


def get_lat_avg(matrix, lat, lat_rng):
    """
    Calculate the latitudinal average of a matrix.
    """
    if len(matrix.shape) == 2:
        return np.nanmean(matrix[(lat >= lat_rng[0]) & (lat <= lat_rng[1]), :])
    elif len(matrix.shape) == 3:
        return np.nanmean(np.nanmean(matrix[:, (lat >= lat_rng[0]) & \
                                            (lat <= lat_rng[1]), :],
                                     axis=2), axis=1)
    else:
        raise Exception('Matrix has too many dimensions')


def extract_grid(array, lat, lon, a_lat, a_lon, n_sq):
    """
    Extract the nearest n_sq * n_sq grids to (a_lat, a_lon).
    """
    rowmin = np.argmin(np.abs(lat - a_lat))
    colmin = np.argmin(np.abs(lon - a_lon))

    ##DEBUG
    ##print(rowmin)
    ##print(colmin)
    ##print(np.abs(lat - a_lat))
    ##print(np.abs(lon - a_lon))

    n_gr = n_sq * n_sq

    half_n_sq = int( (n_sq-1) / 2 )

    if len(array.shape) == 2:
        subset = array[max(0,rowmin-half_n_sq): \
                       min(len(lat), rowmin+half_n_sq+1),
                       max(0,colmin-half_n_sq): \
                       min(len(lon), colmin+half_n_sq+1)]

        # Edge cells: fill with NaN's
        n_fill = subset.shape[0] * subset.shape[1]
        if (n_fill < n_gr):
            temp = np.empty(n_gr)
            temp[:n_fill] = subset.reshape(-1)
            temp[n_fill:] = np.nan
            return temp
        else:
            return subset.reshape(-1)

    elif len(array.shape) == 3:
        ##print(max(0,colmin-half_n_sq))
        ##print(min(len(lon), colmin+half_n_sq+1))
        subset = array[:,
                       max(0,rowmin-half_n_sq): \
                       min(len(lat), rowmin+half_n_sq+1),
                       max(0,colmin-half_n_sq): \
                       min(len(lon), colmin+half_n_sq+1)]
        # Edge cells: fill with NaN's
        n_fill = subset.shape[1] * subset.shape[2]
        ##print(n_fill)
        if (n_fill < n_gr):
            temp = np.empty([subset.shape[0], n_gr])
            temp[:, :n_fill] = subset.reshape(subset.shape[0], n_fill)
            temp[:, n_fill:] = np.nan
            return temp
        else:
            return subset.reshape(subset.shape[0], n_gr)

    else:
        raise Exception('Too many dimensions (' + str(len(array.shape))+').')


def get_pt_in_continent(latlon_array):
    """
    Given an array of lat, lon pairs, and path to a shapefile, check whether
    each pair is within the polygon of interest.
    """
    shp_file = os.path.join(os.environ['PROJDIR'], 'Soil_Moisture', 'data',
                            'World_Continents', 'continents', 'continent.shp')
    field_name = 'CONTINENT'
    
    drv = ogr.GetDriverByName('ESRI Shapefile') #We will load a shape file
    ds_in = drv.Open(shp_file)    #Get the contents of the shape file
    lyr_in = ds_in.GetLayer(0)    #Get the shape file's first layer

    #Put the title of the field you are interested in here
    idx_reg = lyr_in.GetLayerDefn().GetFieldIndex(field_name)

    #If the latitude/longitude we're going to use is not in the projection
    #of the shapefile, then we will get erroneous results.
    #The following assumes that the latitude longitude is in WGS84
    #This is identified by the number "4326", as in "EPSG:4326"
    #We will create a transformation between this and the shapefile's
    #project, whatever it may be
    geo_ref = lyr_in.GetSpatialRef()
    point_ref=ogr.osr.SpatialReference()
    point_ref.ImportFromEPSG(4326)
    ctran=ogr.osr.CoordinateTransformation(point_ref,geo_ref)

    def check(lat, lon):
        #Transform incoming longitude/latitude to the shapefile's projection
        [lon,lat,z]=ctran.TransformPoint(lon,lat)

        #Create a point
        pt = ogr.Geometry(ogr.wkbPoint)
        pt.SetPoint_2D(0, lon, lat)

        #Set up a spatial filter such that the only features we see when we
        #loop through "lyr_in" are those which overlap the point defined above
        lyr_in.SetSpatialFilter(pt)

        #Loop through the overlapped features and get the field of
        #interest
        feat = []
        for feat_in in lyr_in:
            ##print(lon, lat, feat_in.GetFieldAsString(idx_reg))
            feat.append(feat_in.GetFieldAsString(idx_reg))
        return feat[0]

    cont = []
    for i in range(latlon_array.shape[0]):
        cont.append(check(latlon_array[i,0], latlon_array[i,1]))
    return np.array(cont)


def add_shape_coord_from_data_array(xr_da, shp_path, coord_name):
    """
    Achtung! Requires "geo_env" to be activated.

    https://stackoverflow.com/questions/51398563/python-mask-netcdf-data-using-shapefile

    Create a new coord for the xr_da indicating whether or not it 
    is inside the shapefile

    Creates a new coord - "coord_name" which will have integer values
    used to subset xr_da for plotting / analysis/

    Usage:
    -----
    precip_da = add_shape_coord_from_data_array(precip_da, 
    "awash.shp", "awash")
    awash_da = precip_da.where(precip_da.awash==0, other=np.nan)
    """

    def transform_from_latlon(lat, lon):
        """
        Input 1D array of lat / lon and output an Affine transformation
        """
        lat = np.asarray(lat)
        lon = np.asarray(lon)
        trans = Affine.translation(lon[0], lat[0])
        scale = Affine.scale(lon[1] - lon[0], lat[1] - lat[0])
        return trans * scale

    def rasterize(shapes, coords, latitude='latitude', longitude='longitude',
                  fill=np.nan, **kwargs):
        """
        Rasterize a list of (geometry, fill_value) tuples onto the given
        xray coordinates. This only works for 1d latitude and longitude
        arrays.

        usage:
        -----
        1. read shapefile to geopandas.GeoDataFrame
        `states = gpd.read_file(shp_dir+shp_file)`
        2. encode the different shapefiles that capture those lat-lons as 
        different numbers i.e. 0.0, 1.0 ... and otherwise np.nan
        `shapes = (zip(states.geometry, range(len(states))))`
        3. Assign this to a new coord in your original xarray.DataArray
        `ds['states'] = rasterize(shapes, ds.coords, longitude='X', 
        latitude='Y')`

        arguments:
        ---------
        : **kwargs (dict): passed to `rasterio.rasterize` function

        attrs:
        -----
        :transform (affine.Affine): how to translate from latlon to ...?
        :raster (numpy.ndarray): use rasterio.features.rasterize fill the 
         values outside the .shp file with np.nan
        :spatial_coords (dict): dictionary of {"X":xr.DataArray, "Y":
         xr.DataArray()} with "X", "Y" as keys, and xr.DataArray as values

        returns:
        -------
        :(xr.DataArray): DataArray with `values` of nan for points outside
         shapefile and coords `Y` = latitude, 'X' = longitude.
        """
        transform = transform_from_latlon(coords[latitude], coords[longitude])
        out_shape = (len(coords[latitude]), len(coords[longitude]))
        raster = features.rasterize(shapes, out_shape=out_shape,
                                    fill=fill, transform=transform,
                                    dtype=float, **kwargs)
        spatial_coords = {latitude: coords[latitude], 
                          longitude: coords[longitude]}
        return xr.DataArray(raster, coords=spatial_coords, 
                            dims=(latitude, longitude))


    # 1. read in shapefile
    shp_gpd = gpd.read_file(shp_path)

    # 2. create a list of tuples (shapely.geometry, id)
    #    this allows for many different polygons within a .shp file 
    #    (e.g. States of US)
    shapes = [(shape, n) for n, shape in enumerate(shp_gpd.geometry)]

    # 3. create a new coord in the xr_da which will be set to the id in 
    # `shapes`
    xr_da[coord_name] = rasterize(shapes, xr_da.coords, 
                                  longitude='longitude',
                                  latitude='latitude')
    return xr_da
