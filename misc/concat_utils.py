import numpy as np


def piecewise_cdf_create(x, ref):
    pct_x = np.percentile(x, [0, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 95,
                              100])
    pct_ref = np.percentile(ref, [0, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 
                                  95, 100])

    slope = np.diff(pct_ref) / np.diff(pct_x)
    threshold = pct_x[:-1]
    intercept = pct_ref[:-1]

    return slope, threshold, intercept


def piecewise_cdf_apply(slope, threshold, intercept, anomalies):
    # Identify which percentile applies to the anomalies.
    which_percentile = np.zeros(anomalies.shape, dtype = int)

    for i in range(len(slope)):
        # With each increment, fewer True values are in id_bool.
        id_bool = threshold[i] <= anomalies
        ##print(sum(id_bool))
        if sum(id_bool):
            which_percentile[id_bool] = i

    # Apply the linear transformation.
    for i in range(len(slope)):
        anomalies[which_percentile == i] = \
            (anomalies[which_percentile == i] - threshold[i]) * slope[i] + \
            intercept[i]

    return anomalies
