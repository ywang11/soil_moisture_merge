# -*- coding: utf-8 -*-
"""
2019/04/04
ywang254@utk.edu

Given ISMN station time series data, determine the temporal availability 
  according to the following requirements:
    ("Hobeichi et al. 2018. Derived Optimal Linear Combination 
     Evapotranspiration (DOLCE) A Global Gridded Synthesis Estimate")
    (2.2) omit the daily soil moisture if <50% hourly soil moisture is 
          observed on that day
    (2.3) omit the monthly soil moisture average if <15 daily soil moisture
          values exist
    (3) not flagged as problematic in ISMN

Return:
  A pandas series that indicates the availability of each month between
    start time and end time.
  If aggregate = True, then also return aggregated monthly average in the
    another pandas series. But if no data is available, return None.

2019/08/09
ywang254@utk.edu

Less stringent criteria on missing data: min. obs. month >= 4, 
                                         min. obs. day >= 1.
"""
import pandas as pd
import numpy as np
import calendar


def ismn_availability(time_series_data, aggregate = False):

    # Limit the data to hours that have 'G' (good) or 'M' (missing information)
    # i.e. not flagged as problematic in ISMN. 
    time_series_data = time_series_data.loc[ \
                       (time_series_data['soil moisture_flag'] == 'G') | \
                       (time_series_data['soil moisture_flag'] == 'M'), :]

    if len(time_series_data) == 0:
        if aggregate:
            return None, None
        else:
            return None

    time_series_index = time_series_data.index

    def round_to_month(pandas_time, start = True):
        if start:
            tag = 1
        else:
            # month end
            _, tag = calendar.monthrange(pandas_time.year, pandas_time.month)
        return pd.datetime(pandas_time.year, pandas_time.month, tag)

    def round_to_day(pandas_time):
        return pd.datetime(pandas_time.year, pandas_time.month,
                           pandas_time.day)

    start_time = time_series_index[0]
    end_time = time_series_index[-1]

    # Calculate the number of hours available on each day between
    # [start_time, end_time]
    days = pd.date_range(start=round_to_month(start_time), 
                         end=round_to_month(end_time, False), freq='D')
    days_avail_num = np.array([0] * len(days))
    for i in time_series_index:
        days_avail_num[days==round_to_day(i)] += 1
    ## (set a day to unavailable if the number of available hours is <12)
    # 2019/08/09
    ## days_avail_num[days_avail_num < 12] = 0
    days_avail_num[days_avail_num < 1] = 0
    days_avail_num = pd.Series(data = days_avail_num, index = days)

    # Calculate the number of days available in each month between
    # [start_time, end_time]
    months = pd.date_range(start=round_to_month(start_time), 
                           end=round_to_month(end_time), freq='MS')
    months_avail_num = days_avail_num.groupby([lambda x: x.year, 
                                               lambda x: x.month]).sum()
    # 2019/08/09
    ##months_avail = months_avail_num >= 15
    months_avail = months_avail_num >= 1
    months_avail.index = months

    if aggregate:
        if not sum(months_avail):
            return months_avail, None
        else:
            months_data_temp = time_series_data['soil moisture' \
                ].groupby([lambda x: x.year, lambda x: x.month]).mean()
            months_data_temp.index = [pd.datetime(x, y, 1) for x,y in \
                months_data_temp.index.values]

            months_data = pd.Series(data=np.nan, index=months)
            months_data.loc[months[months_avail]] = \
                months_data_temp.loc[months[months_avail]]

            return months_avail, months_data
    else:
        return months_avail
