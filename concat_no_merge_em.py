"""
20190923
ywang254@utk.edu

Concatenate the four time periods of the EM-LSM products to produce a 
 consecutive product.
"""
import xarray as xr
import os
import pandas as pd
import numpy as np
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth_cm, target_lat, target_lon
import utils_management as mg
import itertools as it
import multiprocessing as mp
from misc.em_utils import rm_oor


met_obs = 'CRU_v4.03'
prefix = ['predicted', 'predicted_std']
em = 'year_month_anomaly_9grid' # best performing setup


args_list = []
for i, fx in it.product(range(len(depth_cm)), prefix):
    args_list.append(['lsm', i, fx])
    args_list.append(['all', i, fx])


def concat(args):
    model_set, i, fx = args

    dcm = depth_cm[i]

    time_range = pd.date_range(str(year_longest[0]) + '-01-01',
                               str(year_longest[-1]) + '-12-31', freq = 'MS')

    concat = xr.DataArray(data = np.empty([len(time_range), 
                                           len(target_lat), len(target_lon)]),
                          dims = ['time', 'lat', 'lon'],
                          coords = {'time': time_range, 
                                    'lat': target_lat, 'lon': target_lon},
                          name = fx)

    time_segment = [pd.date_range('1950-01-01', '1980-12-31', freq = 'MS'),
                    pd.date_range('1981-01-01', '2010-12-31', freq = 'MS'),
                    pd.date_range('2011-01-01', '2016-12-31', freq = 'MS')]

    for year, ts in zip([year_shorter, year_shortest, year_shorter2], 
                        time_segment):
        year_str = str(year[0]) + '-' + str(year[-1])

        data = xr.open_mfdataset([os.path.join(mg.path_out(),
                                               'em_' + model_set + '_corr',
                                               met_obs + '_' + \
                                               em.replace('anomaly', 
                                                          'restored')\
                                               + '_' + dcm + '_' + year_str,
                                               fx + '_' + str(y) + '.nc') \
                                  for y in year],
                                 decode_times = False, concat_dim = 'time')
        data['time'] = pd.date_range(str(year[0]) + '-01-01',
                                     str(year[-1]) + '-12-31', freq = 'MS')
        concat.loc[ts, :, :] = data[fx].loc[ts, :, :].copy(deep = True).load()
        data.close()

    print(model_set + ' ' + dcm + ' ' + fx + ' ' + em)
    concat.to_dataset(name = fx).to_netcdf(os.path.join(mg.path_out(),
        'concat_no_merge_em_' + model_set, 'concat_' + met_obs + '_' + \
        em.replace('anomaly', 'restored') + '_' + dcm + '_' + fx + '.nc'))

    # Remove the negative and too positive values.
    rm_oor(concat, os.path.join(mg.path_out(), 
                                'concat_no_merge_em_' + model_set), 
           'concat_' + met_obs + '_' + em.replace('anomaly', 'positive') + \
           '_' + dcm, fx, time_range)


pool = mp.Pool(6)
pool.map_async(concat, args_list)
pool.close()
pool.join()
