# -*- coding: utf-8 -*-
"""
Created on Thu May 16 19:19:40 2019

@author: ywang254

Extract the soil moisture from the concatenated merged soil moisture data.
"""
import utils_management as mg
from utils_management.constants import depth, depth_cm, year_longest
import os
import xarray as xr
from misc.ismn_utils import get_weighted_monthly_data, get_ismn_aggr_method
from misc.dolce_utils import get_weighted_sm_points
from glob import glob
import pandas as pd
import time
import itertools as it
import multiprocessing as mp


# Choose the time frame to conduct the calculation.
met_obs = 'CRU_v4.03'
me = 'year_month_anomaly_9grid' # Need to modify the read script if not
                                # this method
land_mask = 'vanilla'
year = year_longest


# Choose the set of observational data points to compare to.
simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = [get_ismn_aggr_method(simple[iam], 
                                         dominance_lc[iam], 
                                         dominance_threshold) \
                    for iam in range(3)]
prod_list = ['lsm', 'all']

# Extract the results at the observational data points.
def extract(option):
    no_merge, iam, i, prod = option
##for no_merge, iam, i, prod in it.product([True], ismn_aggr_method,
##                                         range(len(depth)), prod_list):
    dcm = depth_cm[i]
    d = depth[i]

    grid_latlon, _, _ = get_weighted_monthly_data(os.path.join( \
        mg.path_intrim_out(), 'ismn_aggr'), iam, d, 'all')

    if no_merge:
        f = os.path.join(mg.path_out(), 'concat_no_merge_em_' + prod,
                         'concat_' + met_obs + '_' + \
                         me.replace('anomaly', 'positive') + '_' + dcm + \
                         '_predicted.nc')
    else:
        f = os.path.join(mg.path_out(), 'concat_em_' + prod,
                         'positive_' + met_obs + '_' + me + '_' + dcm + \
                         '_predicted_' + str(year[0]) + '-' + \
                         str(year[-1]) + '.nc')

    data = xr.open_dataset(f, decode_times=False)
    sm = data.predicted.copy(deep=True)
    data.close()

    print(f)


    # ---- obtain values at the observed data points (do not subset
    #      the dates).
    sm_at_data = get_weighted_sm_points(grid_latlon, sm, year)
    if no_merge:
        sm_at_data.to_csv(os.path.join(mg.path_out(), 
                                       'at_obs_concat_em_' + prod, \
                                       'no_merge_' + met_obs + '_' + me + '_' \
                                       + dcm + '_' + iam + '.csv'))
    else:
        sm_at_data.to_csv(os.path.join(mg.path_out(),
                                       'at_obs_concat_em_' + prod, \
                                       met_obs + '_' + me + '_' + dcm + \
                                       '_' + iam + '.csv'))


pool = mp.Pool(4)
pool.map_async(extract, list(it.product([True, False], ismn_aggr_method,
                                        range(len(depth)), prod_list)))
pool.close()
pool.join()
