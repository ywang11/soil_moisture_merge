"""
20190923
ywang254@utk.edu

Step 2 of the CDF-matching merging:
 Caculate the anomalies from the monthly climatology (1981-2010) of the 
 DOLCE-LSM products of different time periods.
"""
import xarray as xr
import os
import pandas as pd
import numpy as np
from utils_management.constants import year_longest, year_shorter, \
    year_shorter2, year_shortest, depth, target_lat, target_lon
import utils_management as mg
from misc.ismn_utils import get_ismn_aggr_method
from misc.dolce_utils import get_cov_method
import itertools as it


simple = [True, False, False]
lu_weighted = [False, False, True]
lu_threshold = 40
ismn_aggr_method = [get_ismn_aggr_method(simple[i], lu_weighted[i],
                                         lu_threshold) for i in range(3)]
cov_method = [get_cov_method(i) for i in range(5)]
prod_list = ['lsm', 'all']

for prod, d, iam, cov in it.product(prod_list, depth, ismn_aggr_method, 
                                    cov_method):
    for year in [year_shorter, year_shorter2, year_shortest]:
        data = xr.open_dataset(os.path.join(mg.path_out(),
                                            'dolce_' + prod + '_product',
                                            'weighted_average_' + \
                                            str(year[0]) + '-' + \
                                            str(year[-1]) + '_' + d + '_' + \
                                            iam + '_' + cov + '.nc'),
                               decode_times = False)
        data['time'] = pd.date_range(str(year[0]) + '-01-01',
                                     str(year[-1]) + '-12-31', freq = 'MS')

        data2 = xr.open_dataset(os.path.join(mg.path_out(),
                                             'concat_dolce_' + prod, 
                                             'climatology_' + \
                                             str(year[0]) + '-' + \
                                             str(year[-1]) + '_' + d + '_' + \
                                             iam + '_' + cov + '.nc'),
                                decode_times = False)

        for m in range(1,13):
            data.sm.loc[data.time.indexes['time'].month == m, 
                        :, :] -= data2.sm.loc[m, :, :]

        data2.close()
        data.to_netcdf(os.path.join(mg.path_out(), 'concat_dolce_' + prod,
                                    'anomaly_' + str(year[0]) + '-' + \
                                    str(year[-1]) + '_' + d + '_' + \
                                    iam + '_' + cov + '.nc'))
