"""
2019/08/16
ywang254@utk.edu

Merge the global predictions of the CMIP6 models with the
"month_anomaly" predictions of the emergent constraint method.

Derivation:

Let s_{i,j,*} = mean soil moisture at grid/station i, of model j
    e_{i,j,*} = mean soil moisture error at grid/station i, of model j
    o_{i,*} = mean actual soil moisture at grid/station i

They have the relationship o{i,*} = s_{i,j,*} - e_{i,j,*}. Therefore, 
 given the estimate \hat{e}_{i,j,*} \forall{i} in space, one can estimate
 \hat{o}_{i,*} \forall{i} in space. Further average the estimates derived
 from all the models j, one obtains a single estimate of the climatology
 of soil moisture. 

Then, add this estimated climatology back to the emergent constraint-derived
 soil moisture anomalies to obtain estimated absolute soil moisture. Where
 this estimated climatology is unavailable, use the mean climatology.
"""
import xarray as xr
from utils_management.constants import year_longest, depth, depth_cm
from misc.cmip5_availability import cmip5_availability
from misc.ismn_utils import get_ismn_aggr_method
import os
import utils_management as mg
import numpy as np


###############################################################################
# Setup of the emergent constraint method.
###############################################################################
year = year_longest

i = REPLACE1 #[0, 1, 2, 3]
d = depth[i]
dcm = depth_cm[i]

em_method = ['month_anomaly', '1grid']

pr_obs = 'REPLACE2' # ['CRU_v4.03', 'GPCC', 'UDEL']


###############################################################################
# Load the mean climatology - separate between months.
###############################################################################
data = xr.open_dataset(os.path.join(mg.path_out(), 'meanmedian_cmip6',
                                    'average_' + d + '_' + str(year[0]) + \
                                    '-' + str(year[-1]) + '.nc'),
                       decode_times = False)
mean_climatology = np.empty([12, len(data.lat), len(data.lon)])
mean_climatology[:, :, :] = np.nan
for m in range(12):
    mean_climatology[m, :, :] = data.sm[m::12, :, :].mean(dim='time')
data.close()


###############################################################################
# Read the emergent constraint-based soil moisture anomaly.
###############################################################################
data = xr.open_dataset(os.path.join(mg.path_out(), 'em_cmip6_corr', 
                                    pr_obs + '_' + em_method[0] + '_' + dcm,
                                    'predicted_' + str(year[0]) + '-' + \
                                    str(year[-1]) + '_' + em_method[1] + \
                                    '.nc'))
em_sm = data.predicted.copy(deep = True)
data.close()


###############################################################################
# Add the climatology to the anomalies.
###############################################################################
fout = os.path.join(mg.path_out(), 'error_cmip6_apply_global_mean',
                    pr_obs + '_' + em_method[0] + '_'+ dcm)

if not os.path.exists(fout):
    os.mkdir(fout)

for m in range(12):
    em_sm[m::12, :, :] = em_sm[m::12, :, :] + mean_climatology[m, :, :]
em_sm.to_dataset(name = 'predicted').to_netcdf(os.path.join(fout,
    'monthly_absolute_' + str(year[0]) + '-' + \
    str(year[-1]) + '_' + em_method[1] + '.nc'))
