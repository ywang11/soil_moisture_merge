"""
2019/08/06
ywang254@utk.edu

Make the x-y plots of the monthly trend of GLEAMv3.3a and ERA-Land.
"""
import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import utils_management as mg
from utils_management.constants import depth
import os

mpl.rcParams['font.size'] = 7

figsize = (8, 3)

year_str = '1981-2010'

fig, axes = plt.subplots(nrows = 2, ncols = 2, figsize = figsize, 
                         sharex = True, sharey = True)
fig.subplots_adjust(wspace = 0)

# (1) GLEAMv3.3a
ax = axes[0,0]
data = pd.read_csv(os.path.join(mg.path_out(), 'other_diagnostics',
                                'lsm', 'trend_GLEAMv3.3a_' + year_str + \
                                '_' + depth[0] + '.csv'), index_col = 0)
for i,c in enumerate(data.columns):
    if data.loc['p_value', c] < 0.05:
        b1 = ax.bar(i, data.loc['trend', c], color = 'k', edgecolor = 'k')
    else:
        b2 = ax.bar(i, data.loc['trend', c], color = 'w', edgecolor = 'k')
ax.set_xticks(range(len(data.columns)))
ax.set_xticklabels(data.columns)
ax.set_xlabel('Month')
ax.set_ylabel('Trend (m$^3$/m$^3$/year)')
ax.set_ylim([-0.0003, 0.0001])
ax.set_title('GLEAM v3.3a 0-10cm')
# ---- ensure that x ticks are visible
ax.xaxis.set_tick_params(labelbottom=True)

# Legend
ax.legend([b1, b2], ['p < 0.05', r'p $\geq$ 0.05'], loc = 'lower center',
          bbox_to_anchor = (0.5, -1), ncol = 2)
axes[1,0].axis('off')


# (2) ERA-Land
for d_ind, d in enumerate(depth[:2]):
    data = pd.read_csv(os.path.join(mg.path_out(), 'other_diagnostics',
                                    'lsm', 'trend_ERA-Land_' + year_str + \
                                    '_' + d + '.csv'), index_col = 0)
    ax = axes[d_ind, 1]
    for i,c in enumerate(data.columns):
        if data.loc['p_value', c] < 0.05:
            b1 = ax.bar(i, data.loc['trend', c], color = 'k', edgecolor = 'k')
        else:
            b2 = ax.bar(i, data.loc['trend', c], color = 'w', edgecolor = 'k')
    ax.set_xticks(range(len(data.columns)))
    ax.set_xticklabels(data.columns)

    if d_ind == 1:
        ax.set_ylabel('Trend (m$^3$/m$^3$/year)')
        ax.set_xlabel('Month')
        ax.set_title('ERA-Land 10-30cm')
    else:
        ax.set_title('ERA-Land 0-10cm')

fig.savefig(os.path.join(mg.path_out(), 'other_diagnostics_plot', 
                         'global_trend_reference.png'), dpi = 600.,
            bbox_inches = 'tight')
