# -*- coding: utf-8 -*-
"""
2019/09/15
ywang254@utk.edu

Summarize the precipitation datasets for each setup of the emergent
  constraint method. Compare to the actual NaN's in the saved outputs 
  of "em_lsm_corr.py".
"""
import utils_management as mg
from utils_management.constants import pr_to_lsm, year_longest, \
    year_shorter, year_shorter2, year_shortest, lsm_list, depth, depth_cm, \
    target_lat, target_lon
from misc.spatial_utils import extract_grid
import os
import xarray as xr
import pandas as pd
import numpy as np
import itertools as it
import time
from joblib import Parallel, delayed


###############################################################################
# The precipitation data relevant to the land surface models in this setup.
###############################################################################
pr_relevant = {}


# Precipitation dataset is irrelevant: ['CRU_v4.03', 'GPCC', 'UDEL']
# Method is irrelevant: [('month', '1grid'), ('month_anomaly', '1grid'),
#                        ('year_month', '9grid')]


for year, i in it.product([year_longest, year_shorter, year_shorter2, 
                           year_shortest], range(len(depth_cm))):
    d = depth[i]
    dcm = depth_cm[i]

    year_str = str(year[0]) + '-' + str(year[-1])

    # Land surface models relevant to this soil layer and target period.
    lsm = lsm_list[(str(year[0])+'-'+str(year[-1]), d)]

    pr_temp = []
    for pr in pr_to_lsm.keys():
        temp = set(pr_to_lsm[pr]) & set(lsm)
        if bool(temp):
            pr_temp.append(pr)
    pr_relevant[(year_str, dcm)] = pr_temp
