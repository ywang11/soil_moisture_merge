"""
2019/07/31
ywang254@utk.edu

Parse the CMIP6 json files from the download interfaces.
Create symlink to the common scratch space CMIP6 files in order to harmonize
folder structure.
"""
import utils_management as mg
from misc.cmip6_utils import build_cori_path
import json
import os
import pandas as pd
import numpy as np
from glob import glob


# Read my required experiment and variable set.
need = pd.read_csv('cmip6_my_act_var_expr_tid.csv')
activity_list = list(need['Activity'])
varname_list = list(need['Variable'])
expr_list = list(need['Experiment'])
table_list = list(need['Table ID'])


for act, var, expr, table in zip(activity_list, varname_list,
                                 expr_list, table_list):
    # Create the list of files & path.

    """
    sm_path_list = []  # Total water content of soil layer, kg m-2

    for i in range(1,3):
        jname = os.path.join(mg.path_data(), 'CMIP6',
                             act + '_' + var + '_' + expr + '_' + table + \
                             '_page'+str(i)+'.json')

        if not os.path.exists(jname):
            continue

        js1 = open(jname)
        js1_str = js1.read()
        # list of dicts of data
        js1_data = json.loads(js1_str)['response']['docs']
        for j in js1_data:
            sm_path_list.append(build_cori_path(j))
        js1.close()

    # Remove the non-existent/empty paths.
    sm_path_list = [x for x in sm_path_list if os.path.exists(x)]
    """

    # (The existence or completeness of CMIP6 files is not guaranteed.)
    sm_path_list = glob(os.environ['CMIP6PATH'] + '/' + act + '/*/*/' + \
                        expr + '/*/' + table + '/' + var + '/*/*')


    # Write the paths to txt file.
    path_file = os.path.join(mg.path_intrim_out(), 'cori_list',
                             act + '_' + var + '_' + expr + '_' + table + \
                             '.csv')
    f = open(path_file, 'w')
    f.write('Model,Version,Ensemble,Experiment,Path\n')
    for i in sm_path_list:
        temp = i.split('/')
        f.write(temp[8] + ',')
        f.write(temp[-1] + ',')
        f.write(temp[10] + ',')
        f.write(temp[9] + ',')
        f.write(i+'\n')
    f.close()


    # Re-write the txt file to only keep the latest version.
    data = pd.read_csv(path_file)
    reduced = data[['Model', 'Ensemble', 'Experiment']].drop_duplicates()
    if reduced.shape[0] < data.shape[0]:
        f = open(path_file, 'w')
        f.write('Model,Ensemble,Experiment,Path\n')
        for ind,p in reduced.iterrows():
            temp = data.loc[(data['Model'] == p['Model']) & \
                            (data['Ensemble'] == p['Ensemble']) & \
                            (data['Experiment'] == p['Experiment']), :]
            if temp.shape[0] > 1:
                version_number = [float(x[1:]) for x in temp['Version']]
                print(p['Model'] + '_' + p['Ensemble'] + '_' + p['Experiment'])
                print(version_number)
                retain = np.argmax(version_number)
            else:
                retain = 0
            temp = temp.iloc[retain, :]
            f.write(temp['Model'] + ',' + temp['Ensemble'] + ','+ \
                    temp['Experiment'] + ',' + temp['Path'] + '\n')
        f.close()
    else:
        data.loc[:, ['Model','Ensemble','Experiment','Path'] \
        ].to_csv(path_file, index = False)


    # Make the symbolic links.
    data = pd.read_csv(path_file)
    for ind,p in data.iterrows():
        path_temp = os.path.join(mg.path_data(), 'CMIP6',
                                 p['Experiment'], var, p['Model'])
        if not os.path.exists(path_temp):
            os.mkdir(path_temp)

        for f in os.listdir(p['Path']):
            if not '.nc' in f:
                # Skip non-nc files
                print(os.path.join(p['Path'], f))
                os.system('rm ' + os.path.join(p['Path'], f))
            else:
                os.system('ln -sf ' + os.path.join(p['Path'], f) + ' ' + \
                          os.path.join(path_temp, f))
