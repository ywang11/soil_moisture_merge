"""
2019/06/12
ywang254@utk.edu

Plot the global maps of interpolated soil moisture at each depth (if exist).
"""
import xarray as xr
import sys
import os
import matplotlib.pyplot as plt
from utils_management.paths import path_intrim_out
from utils_management.constants import depth_cm
from misc.cmip6_utils import mrsol_availability
import cartopy.crs as ccrs
from glob import glob


land_mask = 'vanilla' # 'None', 'vanilla'


cmip6_1 = mrsol_availability('0-10cm', land_mask, 'historical')
cmip6_2 = mrsol_availability('10-30cm', land_mask, 'historical')
cmip6_3 = mrsol_availability('30-50cm', land_mask, 'historical')
cmip6_4 = mrsol_availability('50-100cm', land_mask, 'historical')

cmip6 = list(set(cmip6_1) | set(cmip6_2) | set(cmip6_3) | set(cmip6_4))

for model in cmip6:
    fig, axes = plt.subplots(nrows = 2, ncols = 2, figsize = (20, 12),
                             subplot_kw = {'projection': ccrs.PlateCarree()})
    for d in range(4):
        filename = sorted(glob(os.path.join(path_intrim_out(), 'Interp_Merge',
                                            land_mask, 'CMIP6', model,
                                            'mrsol_historical_*' + \
                                            depth_cm[d] + '.nc')))
        if len(filename) == 0:
            continue

        data = xr.open_mfdataset(filename, decode_times = False,
                                 concat_dim = 'time')
        sm_data = data['sm'].mean(dim='time').copy(deep=True).load()
        data.close()

        ax = axes.flat[d]
        ax.coastlines()
        h = ax.contourf(sm_data.lon.values, sm_data.lat.values,
                        sm_data.values)
        plt.colorbar(h, ax = ax, shrink = 0.7)
        ax.set_title('Soil moisture ' + depth_cm[d] + ' (m$^3$/m$^3$)')
    fig.savefig(os.path.join(path_intrim_out(), 'Interp_Merge_plot', 
                             land_mask, 'cmip6', 
                             'global_map_' + model + '.png'),
                dpi = 600.)
    plt.close(fig)
