# -*- coding: utf-8 -*-
"""
Created on Thu May 16 19:19:40 2019

@author: ywang254

Extract the soil moisture from the concatenated merged
 DOLCE-weighted soil moisture data.
"""
import utils_management as mg
from utils_management.constants import depth, year_longest
import os
import xarray as xr
from misc.ismn_utils import get_weighted_monthly_data, \
    get_ismn_aggr_method
from misc.dolce_utils import get_cov_method, get_weighted_sm_points
from glob import glob
import pandas as pd
import time
import itertools as it


cov_method = [get_cov_method(c) for c in range(5)]

simple = [True, False, False]
dominance_lc = [False, False, True]
dominance_threshold = 40
ismn_aggr_method = [get_ismn_aggr_method(simple[iam], dominance_lc[iam],
                                         dominance_threshold) \
                    for iam in range(len(simple))]


for i, cov, ismn_aggr, no_merge, prod in \
    it.product(range(4), cov_method, ismn_aggr_method, 
               [True, False], ['lsm', 'all']):
    d = depth[i]

    grid_latlon, _, _ = get_weighted_monthly_data(os.path.join( \
            mg.path_intrim_out(), 'ismn_aggr'), ismn_aggr, d, 'all')

    if no_merge:
        f = os.path.join(mg.path_out(), 'concat_no_merge_dolce_' + prod,
                         'concat_average_' + d + '_' + ismn_aggr + \
                         '_' + cov + '.nc')
    else:
        f = os.path.join(mg.path_out(), 'concat_dolce_' + prod, 
                         'positive_average_' + d + '_' + ismn_aggr + \
                         '_' + cov + '.nc')

    data = xr.open_dataset(f, decode_times=False)
    weighted_sm = data.sm.copy(deep=True)
    data.close()

    year = year_longest
    weighted_sm_at_data = get_weighted_sm_points(grid_latlon,
                                                 weighted_sm, year)

    if no_merge:
        weighted_sm_at_data.to_csv(os.path.join(mg.path_out(), 
                                                'at_obs_concat_dolce_' + \
                                                prod, 'no_merge_' + d + '_' + \
                                                ismn_aggr + '_' + \
                                                cov + '.csv'))
    else:
        weighted_sm_at_data.to_csv(os.path.join(mg.path_out(), 
                                                'at_obs_concat_dolce_' + prod,
                                                d + '_' + ismn_aggr + '_' + \
                                                cov + '.csv'))
