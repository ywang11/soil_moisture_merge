# -*- coding: utf-8 -*-
"""
Created on Tue Apr 30 13:47:31 2019

@author: ywang254
"""
import numpy as np
import sys
import os
import pandas as pd
from glob import glob


# Soil layer depths to interpolate to.
depth = ['0.00-0.10', '0.10-0.30', '0.30-0.50', '0.50-1.00']
depth_cm = ['0-10cm', '10-30cm', '30-50cm', '50-100cm']
depth_new = ['0.00-0.10', '0.00-1.00']
depth_cm_new = ['0-10cm', '0-100cm']


# Land surface models participating in the interpolation.
## CABLE-POP does not work
# ---- Need to add ERA-Land
# ---- the top soil of MERRA-Land is 2cm, which is too shallow for us.
# Excluded models: CLM4.0 (30-50cm and 50-100cm layers), GTEC, 
#                  CFSR, GLDAS_CLM, GLDAS_VIC
lsm_all = ['CERA20C', 'CLASS-CTEM-N', 'CLM4', 'CLM4VIC', 
           'CLM5.0', 'ERA20C', 'ERA-Interim', 'ERA5', 'ERA-Land', 'ESA-CCI',
           'GLDAS_Noah2.0', 'GLEAMv3.3a', 'ISAM', 'JSBACH', 'JULES',
           'LPX-Bern', 'ORCHIDEE', 'ORCHIDEE-CNP', 'SiBCASA']
lsm_reanalysis = ['CERA20C', 'ERA5', 'ERA-Land', 'ERA20C', 'ERA-Interim',
                  'GLDAS_Noah2.0']
lsm_satellite = ['ESA-CCI', 'GLEAMv3.3a']
lsm_offline = ['CLASS-CTEM-N', 'CLM4', 'CLM4VIC', 'CLM5.0', 'ISAM',
               'JSBACH', 'JULES', 'LPX-Bern', 'ORCHIDEE', 'ORCHIDEE-CNP',
               'SiBCASA']
lsm_mstmip = ['CLASS-CTEM-N', 'CLM4', 'CLM4VIC', 'SiBCASA']
lsm_trendy = ['CLM5.0', 'ISAM', 'JSBACH', 'JULES', 'LPX-Bern', 'ORCHIDEE',
              'ORCHIDEE-CNP']


lsm_list = {('1950-2016','0.00-0.10'):['CLM5.0','ISAM','JSBACH','JULES',
                                       'LPX-Bern','ORCHIDEE','ORCHIDEE-CNP'],
            ('1950-2010','0.00-0.10'):['CERA20C','CLASS-CTEM-N','CLM4',
                                       'CLM4VIC','CLM5.0', 'ERA20C',
                                       'GLDAS_Noah2.0','ISAM','JSBACH',
                                       'JULES','LPX-Bern','ORCHIDEE',
                                       'ORCHIDEE-CNP','SiBCASA'],
            ('1981-2010','0.00-0.10'):['CERA20C','CLASS-CTEM-N','CLM4',
                                       'CLM4VIC','CLM5.0','ERA20C',
                                       'ERA5','ERA-Land','ERA-Interim',
                                       'ESA-CCI','GLDAS_Noah2.0','ISAM',
                                       'JSBACH','JULES','LPX-Bern','ORCHIDEE',
                                       'ORCHIDEE-CNP','SiBCASA','GLEAMv3.3a'],
            ('1981-2016','0.00-0.10'):['CLM5.0','ERA5','ESA-CCI',
                                       'GLEAMv3.3a','ISAM','JSBACH','JULES',
                                       'LPX-Bern','ORCHIDEE',
                                       'ORCHIDEE-CNP','ERA-Interim'], 
            ('1950-2016','0.10-0.30'):['CLM5.0','ISAM','JULES','LPX-Bern',
                                       'ORCHIDEE','ORCHIDEE-CNP'],
            ('1950-2010','0.10-0.30'):['CERA20C','CLM4','CLM4VIC','CLM5.0',
                                       'GLDAS_Noah2.0','ISAM','ERA20C',
                                       'JULES','LPX-Bern','ORCHIDEE',
                                       'ORCHIDEE-CNP','SiBCASA'],
            ('1981-2010','0.10-0.30'):['CERA20C','CLM4','CLM4VIC','CLM5.0',
                                       'ERA20C','ERA5','ERA-Interim',
                                       'ERA-Land','GLDAS_Noah2.0',
                                       'ISAM','JULES','LPX-Bern','ORCHIDEE',
                                       'ORCHIDEE-CNP','SiBCASA'],
            ('1981-2016','0.10-0.30'):['CLM5.0','ERA5','ERA-Interim',
                                       'ISAM', 'JULES','LPX-Bern','ORCHIDEE',
                                       'ORCHIDEE-CNP'],
            ('1950-2016','0.30-0.50'):['CLM5.0','ISAM','JSBACH','LPX-Bern',
                                       'ORCHIDEE','ORCHIDEE-CNP'],
            ('1950-2010','0.30-0.50'):['CLASS-CTEM-N','CLM4VIC',
                                       'CLM5.0','GLDAS_Noah2.0','ISAM',
                                       'JSBACH','LPX-Bern','ORCHIDEE',
                                       'ORCHIDEE-CNP','SiBCASA'],
            ('1981-2010','0.30-0.50'):['CLASS-CTEM-N','CLM4VIC',
                                       'CLM5.0','GLDAS_Noah2.0',
                                       'ISAM','JSBACH','LPX-Bern',
                                       'ORCHIDEE','ORCHIDEE-CNP','SiBCASA'],
            ('1981-2016','0.30-0.50'):['CLM5.0','ISAM','JSBACH',
                                       'LPX-Bern','ORCHIDEE','ORCHIDEE-CNP'],
            ('1950-2016','0.50-1.00'):['CLM5.0','ISAM','JULES','LPX-Bern',
                                       'ORCHIDEE','ORCHIDEE-CNP'],
            ('1950-2010','0.50-1.00'):['CLM4VIC','CLM5.0',
                                       'GLDAS_Noah2.0','ISAM','JULES',
                                       'LPX-Bern','ORCHIDEE','ORCHIDEE-CNP',
                                       'SiBCASA'],
            ('1981-2010','0.50-1.00'):['CLM4VIC','CLM5.0',
                                       'GLDAS_Noah2.0','ISAM','JULES',
                                       'LPX-Bern','ORCHIDEE','ORCHIDEE-CNP',
                                       'SiBCASA'],
            ('1981-2016','0.50-1.00'):['CLM5.0','ISAM','JULES',
                                       'LPX-Bern','ORCHIDEE','ORCHIDEE-CNP'] \
}


# Years that the land surface models are interpolated to.
year_longest = range(1950, 2017)
year_shorter = range(1950, 2011)
year_shorter2 = range(1981, 2017)
year_shortest = range(1981, 2011)

# Historical years in CMIP5 models.
year_cmip5 = range(1950,2006)

# Historical years in CMIP6 models.
year_cmip6 = range(1950,2015)

# Target lat and lon.
target_lat = np.arange(-89.75, 89.76, .5)
target_lon = np.arange(-179.75, 179.76, .5)


# Climate zones occupied by the ISMN stations.
clim_zone = ['Af', 'Am', 'Aw', 'BSh', 'BSk', 'BWh', 'BWk', 'Cfa', 'Cfb', 
             'Csa', 'Csb', 'Cwa', 'Cwb', 'Dfa', 'Dfb', 'Dfc', 'Dsa', 'Dsb',
             'Dsc', 'Dwa', 'Dwb', 'Dwc', 'EF', 'ET', 'Unknown', 'W']


# IGBP land use classes.
lu_names = {0: 'Water Bodies', 1: 'Evergreen Needleleaf Forests', 
            2: 'Evergreen Broadleaf Forests', 
            3: 'Deciduous Needleleaf Forests', 
            4: 'Deciduous Broadleaf Forests', 
            5: 'Mixed Forests', 6: 'Closed Shrublands', 
            7: 'Open Shrublands', 8: 'Woody Savannas', 
            9: 'Savannas', 10: 'Grasslands', 
            11: 'Permanent Wetlands', 12: 'Croplands', 
            13: 'Urban and Built-up Lands', 
            14: 'Cropland/Natural Vegetation Mosaics', 
            15: 'Permanent Snow and Ice', 
            16: 'Barren', 255: 'Unclassified'}


# The driver or corresponding meteorological data of the land surface
# models, satellite data, and reanalysis.
met_to_lsm = {'CRU_v4.03': ['ESA-CCI', 'GLEAMv3.3a'],
              'CRU_v3.20': ['CLASS-CTEM-N', 'CLM4', 'CLM4VIC', 'SiBCASA'],
              'CRU_v3.26': ['CLM5.0', 'ISAM', 'JSBACH', 'JULES', 'LPX-Bern',
                            'ORCHIDEE', 'ORCHIDEE-CNP'],
              'GLDAS_Noah2.0': ['GLDAS_Noah2.0'],
              'ERA-Interim': ['ERA-Interim', 'ERA-Land'],
              'ERA5': ['ERA5'],
              'ERA20C': ['ERA20C'],
              'CERA20C': ['CERA20C']}
lsm_to_met = dict([(jj,ii) for ii in met_to_lsm.keys() for jj in met_to_lsm[ii]])


# CMIP5 models:
# required variables: mrlsl for [piControl, not any more], historical, rcp85
#                     pr for historical, rcp85
#               FGOALS-s2 does not have piControl for soil moisture,
#               FGOALS-g2 does not have historical precipitation.
#               CNRM-CM5-2 does not have rcp85 precipitation.
cmip5 = ['ACCESS1-0','ACCESS1-3','CanESM2','CNRM-CM5',
         'FGOALS-s2', 'GFDL-CM3','GFDL-ESM2G','GFDL-ESM2M','GISS-E2-H-CC',
         'GISS-E2-H','GISS-E2-R-CC','GISS-E2-R','HadGEM2-ES','HadGEM2-CC',
         'inmcm4','MIROC5','MIROC-ESM','MIROC-ESM-CHEM','NorESM1-ME']


# CMIP6 models
def cmip6_list(expr, var):
    """
    Find the list of models generated by "cmip6_parse_create_symlink.py",
    including the ensemble id's. Add the models that are not downloaded.
    """
    if sys.platform == 'win32':
        path = 'D:/Projects/2018 Soil Moisture'
    elif sys.platform == 'linux':
        if 'cori' in os.environ['HOSTNAME']:
            path = os.path.join(os.environ['SCRATCH'], 'Soil_Moisture')
        else:
            path = '/lustre/haven/user/ywang254/Soil_Moisture'

    # List of models.
    path_var = os.path.join(path, 'data', 'CMIP6', expr, var)
    model = [f for f in os.listdir(path_var) if \
             os.path.isdir(os.path.join(path_var, f))]

    # List of ensemble members in each model.
    cmip6_list = {}
    for m in model:
        ##print(os.listdir(os.path.join(path_var, m)))
        temp = [x.split('/')[-1].split('_')[4] for x in \
                glob(os.path.join(path_var, m, '*.nc'))]
        cmip6_list[m] = list(np.unique(temp))

    # Drop incomplete folders
    if (var == 'mrsol'):
        if (expr == 'hist-GHG'):
            cmip6_list.pop('CESM2')
            cmip6_list.pop('NorESM2-LM')
        elif (expr == 'hist-nat'):
            cmip6_list.pop('CESM2')
        elif (expr == 'piControl'):
            cmip6_list.pop('GISS-E2-1-G-CC')
            cmip6_list['GISS-E2-1-G'].remove('r102i1p1f1')
    if (var == 'evspsbl') & (expr == 'historical'):
        cmip6_list['CanESM5'].remove('r15i1p1f1')
        cmip6_list['E3SM-1-0'].remove('r1i1p1f1')
        cmip6_list['E3SM-1-0'].remove('r2i1p1f1')
        cmip6_list['E3SM-1-0'].remove('r3i1p1f1')
        cmip6_list['E3SM-1-0'].remove('r4i1p1f1')
        cmip6_list['E3SM-1-0'].remove('r5i1p1f1')
        cmip6_list['EC-Earth3'].remove('r21i1p1f1')
        cmip6_list['EC-Earth3'].remove('r3i1p1f1')
        cmip6_list['EC-Earth3'].remove('r6i1p1f1')
        cmip6_list['EC-Earth3'].remove('r7i1p1f1')
        cmip6_list['GFDL-ESM4'].remove('r1i1p1f1')
    if (var == 'mrsos') & (expr == 'historical'):
        cmip6_list['EC-Earth3'].remove('EC-Earth3')
        cmip6_list['EC-Earth3'].remove('r3i1p1f1')
        cmip6_list['EC-Earth3'].remove('r21i1p1f1')
        cmip6_list['EC-Earth3'].remove('r2i1p1f1')
        cmip6_list['EC-Earth3'].remove('r6i1p1f1')
        cmip6_list['EC-Earth3'].remove('r7i1p1f1')

    cmip6 = [x + '_' + y for x in cmip6_list.keys() for y in cmip6_list[x]]

    return cmip6


# Key to the SYNMAP land cover data
synmap_legend = {0: 'Water',
                 1: 'Evergreen - needle - trees',
                 2: 'Deciduous - needle - trees', 
                 3: 'Mixed - needle - trees', 
                 4: 'Evergreen - broadleaf - trees', 
                 5: 'Deciduous - broadleaf - trees', 
                 6: 'Mixed - broadleaf - trees',
                 7: 'Evergreen - mixed - trees',
                 9: 'Mixed - trees', 
                 10: 'Evergreen - needle - trees and shrubs',
                 11: 'Deciduous - needle - trees and shrubs', 
                 13: 'Evergreen - broadleaf - trees and shrubs', 
                 14: 'Deciduous - broadleaf - trees and shrubs', 
                 15: 'Mixed - broadleaf - trees and shrubs',
                 17: 'Deciduous - mixed - trees and shrubs', 
                 18: 'Mixed - trees and shrubs', 
                 19: 'Evergreen - needle - trees and grasses', 
                 20: 'Deciduous - needle - trees and grasses', 
                 22: 'Evergreen - broadleaf - trees and grasses', 
                 23: 'Deciduous - broadleaf - trees and grasses', 
                 24: 'Mixed - broadleaf - trees and grasses', 
                 27: 'Mixed - trees and grasses', 
                 37: 'Shrubs', 
                 38: 'Shrubs and grasses', 
                 40: 'Shrubs and barren', 
                 41: 'Grasses', 
                 43: 'Grasses and barren', 
                 45: 'Barren', 
                 47: 'Snow and ice'}


# Key to the MODIS land cover data (same as lu_names)
modis_legend = {0: 'Water',
                1: 'Evergreen Needleleaf Forests',
                2: 'Evergreen Broadleaf Forests',
                3: 'Deciduous Needleleaf Forests',
                4: 'Deciduous Broadleaf Forests',
                5: 'Mixed Forests',
                6: 'Closed Shrublands',
                7: 'Open Shrublands',
                8: 'Woody Savannas',
                9: 'Savannas',
                10: 'Grasslands', 
                11: 'Permanent Wetlands', 
                12: 'Croplands', 
                13: 'Urban and Built-up Lands', 
                14: 'Cropland/Natural Vegetation Mosaics',
                15: 'Permanent Snow and Ice', 
                16: 'Barren', 
                255: 'Unclassified'}


# Key to the SREX region
srex_legend = {1: ('ALA', 'Alaska/N.W. Canada'),
               2: ('CGI', 'Canada/Greenl./Icel.'), 
               3: ('WNA', 'W. North America'),
               4: ('CNA', 'C. North America'), 
               5: ('ENA', 'E. North America'),
               6: ('CAM', 'Central America/Mexico'),
               7: ('AMZ', 'Amazon'),
               8: ('NEB', 'N.E. Brazil'),
               9: ('WSA', 'Coast South America'),
               10: ('SSA', 'S.E. South America'),
               11: ('NEU', 'N. Europe'),
               12: ('CEU', 'C. Europe'),
               13: ('MED', 'S. Europe/Mediterranean'),
               14: ('SAH', 'Sahara'),
               15: ('WAF', 'W. Africa'), 
               16: ('EAF', 'E. Africa'), 
               17: ('SAF', 'S. Africa'), 
               18: ('NAS', 'N. Asia'), 
               19: ('WAS', 'W. Asia'), 
               20: ('CAS', 'C. Asia'), 
               21: ('TIB', 'Tibetan Plateau'), 
               22: ('EAS', 'E. Asia'), 
               23: ('SAS', 'S. Asia'),
               24: ('SEA', 'S.E. Asia'), 
               25: ('NAU', 'N. Australia'), 
               26: ('SAU', 'S. Australia/New Zealand'), 
               999: ('NaN', 'No SREX Region')}


# Key to the continent mask produced by "continent_mask.py"
continent_legend = {1: 'Africa',
                    2: 'Antarctica',
                    3: 'Asia', 
                    4: 'Europe',
                    5: 'North America',
                    6: 'Oceania',
                    7: 'South America', 
                    999: 'No Continent'}


# Validation dataset & depth
val_list = ['ESSMRA', 'GLEAMv3.3a', 'SMERGE_v2', 'scPDSI']
val2_list = ['ERA5', 'GLDAS_Noah2.0']
val_depth = {'ESSMRA': ['0-3cm'], 'GLEAMv3.3a': ['0-10cm', '0-100cm'],
             'SMERGE_v2': ['0-40cm'], 'scPDSI': [''], 
             'ERA5': ['0-10cm', '10-30cm'],
             'GLDAS_Noah2.0': ['0-10cm', '10-30cm', '30-50cm', '50-100cm']}
