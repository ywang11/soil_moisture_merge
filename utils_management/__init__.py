# -*- coding: utf-8 -*-
"""
Created on Sat Dec  8 12:18:11 2018

@author: ywang254
"""

__all__ = ['constants', 'path_root', 'path_data', 'path_intrim_out', 
           'path_out', 'path_to_pr', 'path_to_tas', 'path_to_cmip6',
           'path_to_srex', 'path_to_final_prod']

from .paths import path_root, path_data, path_intrim_out, path_out, \
    path_to_pr, path_to_tas, path_to_cmip6, path_to_srex, \
    path_to_final_prod
