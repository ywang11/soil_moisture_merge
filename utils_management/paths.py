# -*- coding: utf-8 -*-
"""
Created on Sat Dec  8 12:13:38 2018

@author: ywang254

Return the root path to all data and output files, and other paths within the
root path.
"""
import os
import sys
import numpy as np
from glob import glob
from .constants import cmip6_list


def path_root():
    if sys.platform == 'win32':
        path = 'D:/Projects/2018 Soil Moisture'
    elif sys.platform == 'linux':
        if 'cori' in os.environ['HOSTNAME']:
            path = os.path.join(os.environ['SCRATCH'], 'Soil_Moisture')
        else:
            path = os.path.join(os.environ['PROJDIR'], 'Soil_Moisture')
    return path

def path_data():
    return os.path.join(path_root(), 'data')

def path_intrim_out():
    return os.path.join(path_root(), 'intermediate')

def path_out():
    return os.path.join(path_root(), 'output_product')

def path_to_pr(land_mask):
    """
    Directory to the precipitation drivers of the land surface models, 
    the observed precipitation, or the reanalysis precipitation.
    """
    pdt = path_data()
    pit = path_intrim_out()

    ppr = {'CRU_v4.03_data': [os.path.join(pdt, 'CRU_v4.03',
                                           'cru_ts4.03.' + x + \
                                           '.pre.dat.nc') for x in \
                              ['1941.1950','1951.1960','1961.1970', \
                               '1971.1980','1981.1990','1991.2000', \
                               '2001.2010','2011.2018']],
           'CRU_v3.20_data' : [os.path.join(pdt, 'CRU_v3.20',
                                            'cru_ts3.20.' + x + \
                                            '.pre.dat.nc') for x in \
                               ['1941.1950','1951.1960','1961.1970', \
                                '1971.1980','1981.1990','1991.2000', \
                                '2001.2010','2011.2011']],
           'CRU_v3.26_data': os.path.join(pdt, 'CRU_v3.26',
                                          'cru_ts3.26.1901.2017.pre.dat.nc'),
           'CRU_v3.20': os.path.join(pit, 'Interp_Merge', land_mask, 
                                     'CRU_v3.20', 'pr_'),
           'CRU_v3.26': os.path.join(pit, 'Interp_Merge', land_mask, 
                                     'CRU_v3.26', 'pr_'),
           'CRU_v4.03': os.path.join(pit, 'Interp_Merge', land_mask, 
                                     'CRU_v4.03', 'pr_'),
           'GLDAS_Noah2.0': os.path.join(pit, 'Interp_Merge',
                                         land_mask, 'GLDAS_Noah2.0_pr',
                                         'GLDAS_Noah2.0_pr_'),
           'ERA-Interim': os.path.join(pit, 'Interp_Merge',
                                       land_mask, 'ERA-Interim_pr',
                                       'ERA-Interim_pr_'),
           'ERA20C': os.path.join(pit, 'Interp_Merge',
                                  land_mask, 'ERA20C_pr', 'ERA20C_pr_'),
           'CERA20C': os.path.join(pit, 'Interp_Merge',
                                   land_mask, 'CERA20C_pr', 
                                   'CERA20C_pr_'),
           'ERA5': os.path.join(pit, 'Interp_Merge', land_mask, 'ERA5_pr',
                                'ERA5_pr_')}
    return ppr

def path_to_tas(land_mask):
    """
    Directory to the temperature drivers of the land surface models, 
    the observed temperature, or the reanalysis temperature.
    """
    pdt = path_data()
    pit = path_intrim_out()

    ppr = {'CRU_v4.03_data': [os.path.join(pdt, 'CRU_v4.03',
                                           'cru_ts4.03.' + x + \
                                           '.tmp.dat.nc') for x in \
                              ['1941.1950','1951.1960','1961.1970', \
                               '1971.1980','1981.1990','1991.2000', \
                               '2001.2010','2011.2018']],
           'CRU_v3.20_data' : [os.path.join(pdt, 'CRU_v3.20',
                                            'cru_ts3.20.' + x + \
                                            '.tmp.dat.nc') for x in \
                               ['1941.1950','1951.1960','1961.1970', \
                                '1971.1980','1981.1990','1991.2000', \
                                '2001.2010','2011.2011']],
           'CRU_v3.26_data': os.path.join(pdt, 'CRU_v3.26',
                                          'cru_ts3.26.1901.2017.tmp.dat.nc'),
           'CRU_v3.20': os.path.join(pit, 'Interp_Merge', land_mask, 
                                     'CRU_v3.20', 'tas_'),
           'CRU_v3.26': os.path.join(pit, 'Interp_Merge', land_mask, 
                                     'CRU_v3.26', 'tas_'),
           'CRU_v4.03': os.path.join(pit, 'Interp_Merge', land_mask, 
                                     'CRU_v4.03', 'tas_'),
           'GLDAS_Noah2.0': os.path.join(pit, 'Interp_Merge',
                                         land_mask, 'GLDAS_Noah2.0_tas',
                                         'GLDAS_Noah2.0_tas_'),
           'ERA-Interim': os.path.join(pit, 'Interp_Merge',
                                       land_mask, 'ERA-Interim_tas',
                                       'ERA-Interim_tas_'),
           'ERA20C': os.path.join(pit, 'Interp_Merge',
                                  land_mask, 'ERA20C_tas', 'ERA20C_tas_'),
           'CERA20C': os.path.join(pit, 'Interp_Merge', land_mask, 
                                   'CERA20C_tas', 'CERA20C_tas_'),
           'ERA5': os.path.join(pit, 'Interp_Merge', land_mask, 'ERA5_tas',
                                'ERA5_tas_')}
    return ppr

def path_to_cmip6(expr, var):
    """
    Directory to all the existing directories that have CMIP6 data. 
    """
    cmip6 = cmip6_list(expr, var)

    model = np.unique([x.split('_')[0] for x in cmip6])
    model_path = {}
    for m in model:
        model_path[m] = {}

        # get unique version numbers
        version_number = [x.split('_')[1] for x in \
                          [y for y in cmip6 if m in y]]
        for v in version_number:
            model_path[m][v] = glob(os.path.join(path_data(), 'CMIP6', expr,
                                                 var, m, var + '*' + m + \
                                                 '_' + expr + '_' + v + \
                                                 '*.nc'))
    return model_path

def path_to_srex():
    if sys.platform == 'linux':
        return '/nics/c/home/ywang254/Git/IPCC-SREX_regions_netCDF/SREX_regions_mask_0.5x0.5.nc'
    else:
        return 'C:/Users/ywang254/Documents/Git/IPCC-SREX_regions_netCDF/SREX_regions_mask_0.5x0.5.nc'


def path_to_final_prod(prod, d, dcm):
    if prod in ['mean_lsm', 'mean_cmip5', 'mean_cmip6', 'mean_2cmip',
                'mean_all']:
        path_prod = os.path.join(path_out(), 'meanmedian_' + \
                                 prod.split('_')[1],
                                 'mean_' + d + '_1950-2016.nc')
    elif prod in ['dolce_lsm', 'dolce_all']:
        path_prod = os.path.join(path_out(), 'concat_' + prod,
                                 'positive_average_' + d + \
                                 '_lu_weighted_ShrunkCovariance.nc')
    elif prod in ['dolce_cmip5', 'dolce_cmip6', 'dolce_2cmip']:
        path_prod = os.path.join(path_out(), prod + '_product',
                                 'weighted_average_1950-2016_' + d + \
                                 '_lu_weighted_ShrunkCovariance.nc')
    elif prod in ['em_lsm', 'em_all']:
        path_prod = os.path.join(path_out(), 'concat_' + prod,
                                 'positive_CRU_v4.03_' + \
                                 'year_month_anomaly_9grid_' + dcm + \
                                 '_predicted_1950-2016.nc')
    elif prod in ['em_cmip5', 'em_cmip6', 'em_2cmip']:
        path_prod = os.path.join(path_out(), prod + '_corr',
                                 'CRU_v4.03_year_month_positive_9grid_' + \
                                 dcm + '_1950-2016', 'predicted.nc')
    return path_prod
